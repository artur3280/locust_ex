package tasks;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.myzhan.locust4j.AbstractTask;
import com.github.myzhan.locust4j.Locust;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;

public class FirstTask extends AbstractTask {
    private int weight;
    private String name;

    public FirstTask(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void execute() throws Exception {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
        Map<String, String> timePeriod = RandomData.getTimePeriod(20, formatter);
        RestUtilities restUtilities = new RestUtilities();
        PropertyWorker propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));
        reqSpec.log().all();
        ResponseSpecification resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createPathParam(reqSpec, "id", "Mle6iTGN8ko1OizFOfoj");
        restUtilities.createQueryParam(reqSpec, "start", timePeriod.get("minusDays"));
        restUtilities.createQueryParam(reqSpec, "end", timePeriod.get("current"));

        Response response = restUtilities.getResponse(reqSpec, "get");

        Locust.getInstance().recordSuccess("http", getName(), response.getTime(), 1);

    }
}
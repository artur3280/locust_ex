package tasks;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.inventory.InventoryResponse;
import com.hrs.cc.api.models.patients.Patients;
import com.hrs.cc.api.models.patients.metrics_data.Metrics;
import com.hrs.cc.api.models.patients.metrics_data.PatientMetricsData;
import com.hrs.cc.api.models.patients.patient_list.PatientList;
import com.hrs.cc.api.models.patients.patient_list.Profile;
import com.hrs.cc.api.models.patients.patient_notes.PatientNote;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class RestAssistant {
    private static RequestSpecification reqSpec;
    private static ResponseSpecification resPec;
    private static RestUtilities restUtilities;
    private static Map<String, String> userData;
    public static String newPatientId;
    private static final String TOKEN_PATH = "/token.json";
    private static PropertyWorker propertyWorker;

    static Profile getPatientByName(String firstName, String lastName) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createQueryParam(reqSpec, "status[]", "Activated");
        restUtilities.createQueryParam(reqSpec, "status[]", "Pre-Activated");
        Response response = restUtilities.getResponse(reqSpec, "get");
        Patients patients = response.as(Patients.class);


        PatientList p = patients.getPatientlist().stream().filter(
                patient -> patient.get(0).getProfile().getName().getFirst().equals(firstName) &&
                        patient.get(0).getProfile().getName().getLast().equals(lastName)).collect(Collectors.toList()).get(0).get(0);
        if (p != null) {
            return p.getProfile();
        }
        return null;
    }

    static com.hrs.cc.api.models.patients.profile.Profile getPatientById(String id) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
        Map<String, String> timePeriod = RandomData.getTimePeriod(20, formatter);
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createPathParam(reqSpec, "id", id);
        restUtilities.createQueryParam(reqSpec, "start", timePeriod.get("minusDays"));
        restUtilities.createQueryParam(reqSpec, "end", timePeriod.get("current"));

        Response response = restUtilities.getResponse(reqSpec, "get");
        ArrayList profile = response.jsonPath().get("profile");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(profile.get(0), com.hrs.cc.api.models.patients.profile.Profile.class);
    }

    static void setModulesToPatientById(String patientId) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);


        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.MODULE_INFO));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        String bodyReq = "{\n" +
                "    \"moduleInfo\": {\n" +
                "        \"modules\": [\n" +
                "            \"healthy\",\n" +
                "            \"mainthree\",\n" +
                "            \"chf\",\n" +
                "            \"copd\",\n" +
                "            \"diabetes\",\n" +
                "            \"activity\",\n" +
                "            \"weight\",\n" +
                "            \"temperature\",\n" +
                "            \"pulseox\",\n" +
                "            \"bloodpressure\",\n" +
                "            \"medication\",\n" +
                "            \"survey\",\n" +
                "            \"glucose\",\n" +
                "            \"woundimaging\"\n" +
                "        ],\n" +
                "        \"activityreminder\": {\n" +
                "            \"time\": 20,\n" +
                "            \"window\": \"-\"\n" +
                "        },\n" +
                "        \"surveyreminder\": {\n" +
                "            \"window\": \"60\",\n" +
                "            \"time\": 600\n" +
                "        },\n" +
                "        \"weightreminder\": {\n" +
                "            \"window\": \"60\",\n" +
                "            \"time\": 540\n" +
                "        },\n" +
                "        \"medicationreminders\": [\n" +
                "            {\n" +
                "                \"medication\": \"SULFAPYRIDINE\",\n" +
                "                \"dose\": \"120 mg x1 Oral\",\n" +
                "                \"schedule\": {\n" +
                "                    \"essential\": \"undefined\",\n" +
                "                    \"type\": \"everyday\",\n" +
                "                    \"instruction\": \"test special instruction\"\n" +
                "                },\n" +
                "                \"times\": \"PRN\",\n" +
                "                \"time\": \"PRN\",\n" +
                "                \"window\": \"30\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"medication\": \"PERCORTEN\",\n" +
                "                \"dose\": \"122 ml x1 Oral\",\n" +
                "                \"schedule\": {\n" +
                "                    \"essential\": \"undefined\",\n" +
                "                    \"type\": \"everyday\",\n" +
                "                    \"instruction\": \"test spec instruction\"\n" +
                "                },\n" +
                "                \"times\": \"PRN\",\n" +
                "                \"time\": \"PRN\",\n" +
                "                \"window\": \"30\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"medication\": \"METANDREN\",\n" +
                "                \"dose\": \"1 tblsp x2 Oral\",\n" +
                "                \"schedule\": {\n" +
                "                    \"essential\": \"undefined\",\n" +
                "                    \"type\": \"everyday\",\n" +
                "                    \"instruction\": \"test spec instruction\"\n" +
                "                },\n" +
                "                \"times\": \"PRN\",\n" +
                "                \"time\": \"PRN\",\n" +
                "                \"window\": \"30\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}";

        reqSpec.body(bodyReq);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
        if (response.statusCode() == 200) {
            System.out.println("Set metrics to patient: " + patientId);
        } else {
            System.out.println("Something wrong! Please check!");
        }
    }

    static void setRisksToPatientById(String patientId) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.RISK));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        String bodyReq = "{\n" +
                "    \"settings\": {\n" +
                "        \"riskdata\": [\n" +
                "            {\n" +
                "                \"type\": \"activity_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastolicgreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 100\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_heartrategreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 150\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"glucose_bloodsugargreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 120\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"medication_missedessential\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"pulseox_heartrategreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": 80\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"pulseox_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"survey_checkanswers\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"survey_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"temperature_temperaturegreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 100\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"survey_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_decreasexlbsfromstart\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 120\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_decreasexlbsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 10\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_decreasexlbsfromstart\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 90\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastoliclessthan\",\n" +
                "                \"alerttype\": \"highrisk\",\n" +
                "                \"value\": \"40\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_systolicgreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": \"200\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"pulseox_spo2lessthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": \"90\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastolicgreaterthan\",\n" +
                "                \"alerttype\": \"highrisk\",\n" +
                "                \"value\": \"160\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastolicgreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": \"140\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}";

        reqSpec.body(bodyReq);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
        if (response.statusCode() == 200) {
            System.out.println("Set risks to patient: " + patientId);
        } else {
            System.out.println("Something wrong! Please check!");
        }
    }

    static Metrics getPatientMetricsData(String patientId) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Map<String, String> date = restUtilities.getTimePeriod(10);
        restUtilities.createQueryParam(reqSpec, "start", date.get("current").replace("-", ""));
        restUtilities.createQueryParam(reqSpec, "end", date.get("current").replace("-", ""));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "get"
        );

        return Arrays.asList(response.as(PatientMetricsData[].class)).get(0).getMetrics();
    }


    static List<PatientNote> getGetNotesByPatientId(String patientId) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.NOTES));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT + EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Response response = restUtilities.getResponse(restUtilities.createPathParam(reqSpec, "id", patientId), "get");
        return Arrays.asList(response.as(PatientNote[].class));
    }


    static Integer tsToSec8601(String timestamp) {
        if (timestamp == null) return null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            Date dt = sdf.parse(timestamp);
            long epoch = dt.getTime();
            return (int) (epoch / 1000);
        } catch (ParseException e) {
            return null;
        }
    }

    static String getPatientId() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createQueryParam(reqSpec, "status[]", "Activated");
        restUtilities.createQueryParam(reqSpec, "status[]", "Pre-Activated");
        Response response = restUtilities.getResponse(reqSpec, "get");
        Patients patients = response.as(Patients.class);


        String p = patients.getPatientlist().stream().filter(
                patient -> (patient.get(0).getProfile().getName().getFirst().contains("test") ||
                        patient.get(0).getProfile().getName().getLast().contains("test")) &&
                        patient.get(0).getProfile().getClinician() != null &&
                        patient.get(0).getProfile().getClinician().getName().equals(Auth.FIRST_NAME + " " + Auth.LAST_NAME) &&
                        patient.get(0).getDay() > 5).collect(Collectors.toList()).get(0).get(0).getProfile().getHrsid();


        return p;
    }

    static InventoryResponse getDeviceId() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.INVENTORY));

        resPec = restUtilities.getResponseSpecification();


        restUtilities.setContentType(ContentType.JSON);

        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response devices = restUtilities.getResponse(reqSpec, "get");
        List<InventoryResponse> inventoryList = Arrays.asList(devices.as(InventoryResponse[].class));
        return inventoryList.stream().filter(i ->
                !i.getName().equals("HRSTAB10498") &&
                        !i.getId().equals("HRSTAB10498")).collect(Collectors.toList()).get(0);


    }
}

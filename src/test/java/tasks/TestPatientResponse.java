package tasks;



import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.GenderTypes;
import com.hrs.cc.api.constans.Modules;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.patients.metrics_data.Profile;
import com.hrs.cc.api.models.patients.patient_metric.metric_weight.WeightMetric;
import com.hrs.cc.api.models.patients.patient_metric.response_after_add.MetricksResponse;
import com.hrs.cc.api.models.patients.patient_status.PatientStatus;
import com.hrs.cc.api.models.requests.add_new_patient.CustomAtributs;
import com.hrs.cc.api.models.requests.add_new_patient.Name;
import com.hrs.cc.api.models.requests.add_new_patient.NewPatient;
import com.hrs.cc.api.models.requests.set_patient_metricks.*;
import com.hrs.cc.api.models.requests.set_patient_metricks.activity.SetActivity;
import com.hrs.cc.api.models.requests.set_patient_metricks.blood_pressure.SetBloodPressure;
import com.hrs.cc.api.models.requests.set_patient_metricks.glucose.SetGlucose;
import com.hrs.cc.api.models.requests.set_patient_metricks.pulse_ox.SetPulseOx;
import com.hrs.cc.api.models.requests.set_patient_metricks.temperature.SetTemperature;
import com.hrs.cc.api.models.requests.set_patient_metricks.weight.SetWeight;
import com.hrs.cc.api.models.requests.set_quick_note.QuickNote;
import com.hrs.cc.api.models.requests.settings_patient.CustomNewAtributs;
import com.hrs.cc.api.models.requests.settings_patient.SettingPatient;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class TestPatientResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private static NewPatient patientData;
    private String newPatientId;
    private static Map<String, Integer> createdMetrics = new HashMap<>();
    private PropertyWorker propertyWorker;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENT));
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        restUtilities.removeQueryParam(reqSpec, "status[]");
        reqSpec.body("");
    }

    @Test
    public void testAddNewPatient() {
        patientData = new NewPatient();

        List<CustomAtributs> customAtributs = new ArrayList<>();

        CustomAtributs first = new CustomAtributs();
        first.setId(1);
        first.setName("Test custom attributes");
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated("2017-08-31T11:39:00+0000");
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomAtributs two = new CustomAtributs();
        two.setId(2);
        two.setName("Nickname");
        two.setType("text");
        two.setRequired(false);
        two.setPatientsetup(true);
        two.set_class("col-xs-6");
        two.setEmrTracked(false);
        two.setLastUpdated("2017-09-25T14:22:07+0000");
        two.set$$hashKey("object:2416");
        two.setValue("testnick");

        CustomAtributs three = new CustomAtributs();
        three.setId(4);
        three.setName("Custom Text Field");
        three.setType("text");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-12");
        three.setEmrTracked(false);
        three.setLastUpdated("2017-10-16T14:44:33+0000");
        three.set$$hashKey("object:2417");
        three.setValue("test custom field");

        customAtributs.add(first);
        customAtributs.add(two);
        customAtributs.add(three);
        patientData.setCustomAtributsList(customAtributs);

        Name name = new Name("test".concat(restUtilities.getRandomString())
                , "test".concat(restUtilities.getRandomString()),
                "test".concat(restUtilities.getRandomString()));
        patientData.setName(name);

        patientData.setDob(restUtilities.getTimePeriod(1).get("current"));
        patientData.setGender("F");
        patientData.setPhone("3423890430284902");
        patientData.setPid(restUtilities.getRandomString());
        reqSpec.body(patientData);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();

        Response response = restUtilities.getResponse(reqSpec, "post");

        JsonPath jsonPath = response.jsonPath();
        newPatientId = jsonPath.get("hrsid");
        assertNotNull(jsonPath.get("hrsid"));
        assertEquals(jsonPath.get("status"), "ok");
    }

    @Test(priority = 6)
    public void testAddAlreadyExistsPatient() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENT));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(500)
        );

        patientData = new NewPatient();

        List<CustomAtributs> customAtributs = new ArrayList<>();

        CustomAtributs first = new CustomAtributs();
        first.setId(1);
        first.setName("Test custom attributes");
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated("2017-08-31T11:39:00+0000");
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomAtributs two = new CustomAtributs();
        two.setId(2);
        two.setName("Nickname");
        two.setType("text");
        two.setRequired(false);
        two.setPatientsetup(true);
        two.set_class("col-xs-6");
        two.setEmrTracked(false);
        two.setLastUpdated("2017-09-25T14:22:07+0000");
        two.set$$hashKey("object:2416");
        two.setValue("1");

        CustomAtributs three = new CustomAtributs();
        three.setId(4);
        three.setName("Custom Text Field");
        three.setType("text");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-12");
        three.setEmrTracked(false);
        three.setLastUpdated("2017-10-16T14:44:33+0000");
        three.set$$hashKey("object:2417");
        three.setValue("1");

        customAtributs.add(first);
        customAtributs.add(two);
        customAtributs.add(three);
        patientData.setCustomAtributsList(customAtributs);

        Name name = new Name();
        name.setLast("Integration");
        name.setFirst("UserApp");
        name.setMiddle("");
        patientData.setName(name);

        patientData.setDob("01/01/1990");
        patientData.setGender("F");
        patientData.setPhone("091019990000111");
        patientData.setPid("testAppIntegration");
        reqSpec.body(patientData);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("message"), "Patient ID already exists");
    }

    @Test(dependsOnMethods = "testAddNewPatient")
    public void testSetMetricsToPatient() {
        NewMetricks newMetrick = new NewMetricks();
        ModuleInfo info = new ModuleInfo();

        List<String> modulesList = new ArrayList<>();
        modulesList.add(Modules.HELTHY);
        modulesList.add(Modules.CHF);
        modulesList.add(Modules.COPD);
        modulesList.add(Modules.DIABETES);
        modulesList.add(Modules.ACTIVITY);
        modulesList.add(Modules.WEIGHT);
        modulesList.add(Modules.TEMPERATURE);
        modulesList.add(Modules.BLOOD_PRESSURE);
        modulesList.add(Modules.MEDICATION);
        modulesList.add(Modules.SURVEY);
        modulesList.add(Modules.PULSE_OX);
        modulesList.add(Modules.GLUCOSE);
        modulesList.add(Modules.WOUND_IMAGING);
        info.setModules(modulesList);
        info.setActivityreminder(new Activityreminder());
        info.setSurveyreminder(new Surveyreminder());
        info.setWeightreminder(new Weightreminder());

        List<Medicationreminder> medicationreminderList = new ArrayList<>();
        Medicationreminder medicationreminder = new Medicationreminder();
        medicationreminder.setMedication("test medication ".concat(restUtilities.getRandomString()));
        medicationreminder.setDose("1 mg x1 Oral");
        medicationreminder.setSchedule(new Schedule("true",
                "everyday",
                restUtilities.getTimePeriod(25).get("plusDays"),
                "Test instruction: ".concat(restUtilities.getRandomString())));
        medicationreminderList.add(medicationreminder);

        Medicationreminder everyXdaysReminder = new Medicationreminder();
        everyXdaysReminder.setMedication("LIQUAEMIN SODIUM");
        everyXdaysReminder.setDose("12 mg x1 Oral");
        Schedule scheduleEveryXdaysReminder = new Schedule();
        scheduleEveryXdaysReminder.setEssential("true");
        scheduleEveryXdaysReminder.setType("everyxdays");
        scheduleEveryXdaysReminder.setX(restUtilities.getRandomInt(1, 7).toString());
        scheduleEveryXdaysReminder.setStartday(String.valueOf(restUtilities.getCurrentTimeStamp()));
        scheduleEveryXdaysReminder.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleEveryXdaysReminder.setExpiration(restUtilities.getTimePeriod(25).get("plusDays"));
        everyXdaysReminder.setSchedule(scheduleEveryXdaysReminder);
        everyXdaysReminder.setTime("540");
        everyXdaysReminder.setTimes("9:00AM");
        everyXdaysReminder.setWindow("60");
        medicationreminderList.add(everyXdaysReminder);

        Medicationreminder customDaysRemider = new Medicationreminder();
        customDaysRemider.setMedication("ALCOHOL 10% AND DEXTROSE 5%");
        customDaysRemider.setDose("200 mg x2 Oral");
        Schedule scheduleCustomDaysRemider = new Schedule();
        scheduleCustomDaysRemider.setEssential("true");
        scheduleCustomDaysRemider.setType("custom");
        scheduleCustomDaysRemider.setSchedule(Modules.customMedicationsDays);
        scheduleCustomDaysRemider.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleCustomDaysRemider.setExpiration(restUtilities.getTimePeriod(25).get("plusDays"));
        customDaysRemider.setSchedule(scheduleCustomDaysRemider);
        customDaysRemider.setTime("540");
        customDaysRemider.setTimes("9:00AM");
        customDaysRemider.setWindow("60");
        medicationreminderList.add(customDaysRemider);

        info.setMedicationreminders(medicationreminderList);
        newMetrick.setModuleInfo(info);

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENTS))
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token"))));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.MODULE_INFO));
        newReqSpec.body(newMetrick);

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");

        MetricksResponse metricksResponse = response.as(MetricksResponse.class);

        assertEquals(metricksResponse.getStatus(), "ok");
        assertNotNull(metricksResponse.getAuth());
        assertNotNull(metricksResponse.getAuth().getToken());

        assertNotNull(metricksResponse.getInstalldata());
        assertNotNull(metricksResponse.getInstalldata().getUpdatedisplay());
        assertNotNull(metricksResponse.getInstalldata().getModules());
        assertEquals(metricksResponse.getInstalldata().getModules(), newMetrick.getModuleInfo().getModules());

        assertNotNull(metricksResponse.getInstalldata().getActivityreminder());
        assertEquals(metricksResponse.getInstalldata().getActivityreminder().getTime()
                , newMetrick.getModuleInfo().getActivityreminder().getTime().toString());
        assertEquals(metricksResponse.getInstalldata().getActivityreminder().getWindow()
                , newMetrick.getModuleInfo().getActivityreminder().getWindow());

        assertNotNull(metricksResponse.getInstalldata().getSurveyreminder());
        assertEquals(metricksResponse.getInstalldata().getSurveyreminder().getTime()
                , newMetrick.getModuleInfo().getSurveyreminder().getTime().toString());
        assertEquals(metricksResponse.getInstalldata().getSurveyreminder().getWindow()
                , newMetrick.getModuleInfo().getSurveyreminder().getWindow());

        assertNotNull(metricksResponse.getInstalldata().getTemperaturereminders());
        assertNotNull(metricksResponse.getInstalldata().getTemperaturereminders().getStatus());
        assertEquals(metricksResponse.getInstalldata().getTemperaturereminders().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getWeightreminder());
        assertEquals(metricksResponse.getInstalldata().getWeightreminder().getTime()
                , newMetrick.getModuleInfo().getWeightreminder().getTime().toString());
        assertEquals(metricksResponse.getInstalldata().getWeightreminder().getWindow()
                , newMetrick.getModuleInfo().getWeightreminder().getWindow());

        assertNotNull(metricksResponse.getInstalldata().getWoundimagingreminders());
        assertNotNull(metricksResponse.getInstalldata().getWoundimagingreminders().getStatus());
        assertEquals(metricksResponse.getInstalldata().getWoundimagingreminders().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getMedicationreminders());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getMedication()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getMedication());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getDose()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getDose());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getType()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getType());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getExpiration()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getExpiration());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getEssential()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getEssential());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getInstruction()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getInstruction());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getTime()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getTime());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getWindow()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getWindow());

        assertNotNull(metricksResponse.getInstalldata().getGlucosereminder());
        assertNotNull(metricksResponse.getInstalldata().getGlucosereminder().getStatus());
        assertEquals(metricksResponse.getInstalldata().getGlucosereminder().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getPulseoxreminders());
        assertNotNull(metricksResponse.getInstalldata().getPulseoxreminders().getStatus());
        assertEquals(metricksResponse.getInstalldata().getPulseoxreminders().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getBloodpressurereminders());
        assertNotNull(metricksResponse.getInstalldata().getBloodpressurereminders().getStatus());
        assertEquals(metricksResponse.getInstalldata().getBloodpressurereminders().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getActivationhistory());
        assertNotNull(metricksResponse.getInstalldata().getActivationhistory().get(0).getStatus());
        assertEquals(metricksResponse.getInstalldata().getActivationhistory().get(0).getStatus(), "preactivate");

        List<com.hrs.cc.api.models.patients.patient_metric.response_after_add.response.Medicationreminder> everyXdaysMedications =
                metricksResponse.getInstalldata().getMedicationreminders().stream().filter(
                        m -> m.getSchedule().getType().equals("everyxdays")).collect(Collectors.toList());

        List<com.hrs.cc.api.models.patients.patient_metric.response_after_add.response.Medicationreminder> custoMedications =
                metricksResponse.getInstalldata().getMedicationreminders().stream().filter(
                        m -> m.getSchedule().getType().equals("custom")).collect(Collectors.toList());


        custoMedications.forEach(customMed -> {
            assertNotNull(customMed.getMedication());
            assertNotNull(customMed.getDose());
            assertNotNull(customMed.getTime());
            assertNotNull(customMed.getWindow());
            assertNotNull(customMed.getSchedule());
            assertNotNull(customMed.getSchedule().getType());
            assertNotNull(customMed.getSchedule().getSchedule());
            assertNotNull(customMed.getSchedule().getExpiration());
            assertNotNull(customMed.getSchedule().getEssential());
            assertNotNull(customMed.getSchedule().getInstruction());

            assertEquals(customMed.getMedication(), customDaysRemider.getMedication());
            assertEquals(customMed.getDose(), customDaysRemider.getDose());
            assertEquals(customMed.getTime(), customDaysRemider.getTime());
            assertEquals(customMed.getWindow(), customDaysRemider.getWindow());
            assertEquals(customMed.getSchedule().getType(), customDaysRemider.getSchedule().getType());
            assertEquals(customMed.getSchedule().getSchedule(), customDaysRemider.getSchedule().getSchedule());
            assertEquals(customMed.getSchedule().getEssential(), customDaysRemider.getSchedule().getEssential());
            assertEquals(customMed.getSchedule().getExpiration(), customDaysRemider.getSchedule().getExpiration());
            assertEquals(customMed.getSchedule().getInstruction(), customDaysRemider.getSchedule().getInstruction());
        });

        everyXdaysMedications.forEach(everyMed -> {
            assertNotNull(everyMed.getMedication());
            assertNotNull(everyMed.getDose());
            assertNotNull(everyMed.getTime());
            assertNotNull(everyMed.getWindow());
            assertNotNull(everyMed.getSchedule());
            assertNotNull(everyMed.getSchedule().getType());
            assertNotNull(everyMed.getSchedule().getX());
            assertNotNull(everyMed.getSchedule().getStartday());
            assertNotNull(everyMed.getSchedule().getEssential());
            assertNotNull(everyMed.getSchedule().getExpiration());
            assertNotNull(everyMed.getSchedule().getInstruction());

            assertEquals(everyMed.getMedication(), everyXdaysReminder.getMedication());
            assertEquals(everyMed.getDose(), everyXdaysReminder.getDose());
            assertEquals(everyMed.getTime(), everyXdaysReminder.getTime());
            assertEquals(everyMed.getWindow(), everyXdaysReminder.getWindow());
            assertEquals(everyMed.getSchedule().getType(), everyXdaysReminder.getSchedule().getType());
            assertEquals(everyMed.getSchedule().getX(), everyXdaysReminder.getSchedule().getX());
            assertEquals(everyMed.getSchedule().getStartday(), everyXdaysReminder.getSchedule().getStartday());
            assertEquals(everyMed.getSchedule().getEssential(), everyXdaysReminder.getSchedule().getEssential());
            assertEquals(everyMed.getSchedule().getExpiration(), everyXdaysReminder.getSchedule().getExpiration());
            assertEquals(everyMed.getSchedule().getInstruction(), everyXdaysReminder.getSchedule().getInstruction());
        });

    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testGetPatientActivationStatus() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.ACTIVATIONS));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "get");
        PatientStatus status = Arrays.asList(response.as(PatientStatus[].class)).get(0);
        assertEquals(status.getStatus(), "preactivate");
        assertNotNull(status.getLastUpdated().getDate());
        assertNotNull(status.getLastUpdated().getTimezone());
        assertNotNull(status.getLastUpdated().getTimezoneType());
        assertEquals(response.getStatusCode(), 200);
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetQuickNote() {
        QuickNote quickNote = new QuickNote();
        quickNote.setQuicknote(new com.hrs.cc.api.models.requests.set_quick_note.Note("test quick note "
                .concat(restUtilities.getRandomString())));
        reqSpec.body(quickNote);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("quicknote"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetWeightMetric() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.body(new SetWeight("test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(50, 100)));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        createdMetrics.put("weight", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetWeightMetric"}, priority = 2)
    public void testEditWeightMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "weight");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("rtime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("metric", createdMetrics.get("weight").toString());
        params.put("weight", restUtilities.getRandomInt(50, 150).toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"
            , "testSetWeightMetric", "testEditWeightMetric"}, priority = 5)
    public void testDeleteWeightMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "activity");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("weight").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetActivityMetric() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(new SetActivity("test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(0, 1000)));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        createdMetrics.put("activity", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetActivityMetric"}, priority = 2)
    public void testEditActivityMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "activity");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("rtime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("metric", createdMetrics.get("activity").toString());
        params.put("duration", restUtilities.getRandomInt(0, 1000).toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetActivityMetric", "testEditActivityMetric"}, priority = 5)
    public void testDeleteActivityMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "activity");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("activity").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetBloodPressureMetric() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(new SetBloodPressure(
                "test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(0, 100),
                restUtilities.getRandomInt(0, 100),
                restUtilities.getRandomInt(0, 1000)));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        createdMetrics.put("blood", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetBloodPressureMetric"}, priority = 2)
    public void testEditBloodPressureMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "bloodpressure");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("metric", createdMetrics.get("blood").toString());
        params.put("systolic", restUtilities.getRandomInt(0, 100).toString());
        params.put("diastolic", restUtilities.getRandomInt(0, 100).toString());
        params.put("heartrate", restUtilities.getRandomInt(0, 1000).toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetBloodPressureMetric", "testEditBloodPressureMetric"}, priority = 5)
    public void testDeleteBloodPressureMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "bloodpressure");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("blood").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetGlucoseMetric() {

        SetGlucose glucose = new SetGlucose();
        glucose.setReason("test reason ".concat(restUtilities.getRandomString()));
        glucose.setFtime(restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        Integer data = restUtilities.getRandomInt(0, 1000);
        glucose.setReading(data);
        glucose.setGlucose(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(glucose);

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        createdMetrics.put("glucose", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetGlucoseMetric"}, priority = 2)
    public void testEditGlucoseMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "glucose");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("metric", createdMetrics.get("glucose").toString());
        params.put("reading", restUtilities.getRandomInt(0, 1000).toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetGlucoseMetric", "testEditGlucoseMetric"}, priority = 5)
    public void testDeleteGlucoseMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "glucose");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("glucose").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetPulseOXMetric() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(new SetPulseOx(
                "test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(0, 100),
                restUtilities.getRandomInt(0, 100)));
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        createdMetrics.put("pulseox", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetPulseOXMetric"}, priority = 2)
    public void testEditPulseOXMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "pulseox");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("rtime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("metric", createdMetrics.get("pulseox").toString());
        params.put("spo2", restUtilities.getRandomInt(0, 100).toString());
        params.put("heartrate", restUtilities.getRandomInt(0, 100).toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetPulseOXMetric", "testEditPulseOXMetric"}, priority = 5)
    public void testDeletePulseOXMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "pulseox");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("pulseox").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetTemperatureMetric() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(new SetTemperature(
                "test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(0, 1000)));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        createdMetrics.put("temperature", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetTemperatureMetric"}, priority = 2)
    public void testEditTemperatureMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "temperature");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("temperature").toString());
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("temperature", restUtilities.getRandomInt(0, 100).toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetTemperatureMetric", "testEditTemperatureMetric"}, priority = 5)
    public void testDeleteTemperatureMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "temperature");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("temperature").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 3)
    public void testGetPatientBaselineWeight() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(new SetWeight("test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(50, 100)));

        Response newWeight = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = newWeight.jsonPath();
        assertEquals(path.get("status"), "ok");


        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT))
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")))
        );

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);

        restUtilities.createQueryParam(newReqSpec, "type", "weight");
        restUtilities.createQueryParam(newReqSpec, "criteria", "first");

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(newReqSpec, "id", newPatientId), "get");

        WeightMetric weightMetric = response.as(WeightMetric.class);
        assertNotNull(weightMetric.getId());
        assertEquals(weightMetric.getPatient(), newPatientId);
        assertNotNull(weightMetric.getClinician());
        assertNotNull(weightMetric.getType());
        assertNotNull(weightMetric.getWeight());
        assertNotNull(weightMetric.getReminder());
        assertNotNull(weightMetric.getFinished());
        assertNotNull(weightMetric.getTime());
        assertNotNull(weightMetric.getTime().getDate());
        assertNotNull(weightMetric.getTime().getTimezone());
        assertNotNull(weightMetric.getTime().getTimezoneType());
        assertNotNull(weightMetric.getStatus());
        assertNotNull(weightMetric.getReason());
        assertNotNull(weightMetric.getLastUpdated());
        assertNotNull(weightMetric.getLastUpdated().getDate());
        assertNotNull(weightMetric.getLastUpdated().getTimezone());
        assertNotNull(weightMetric.getLastUpdated().getTimezoneType());
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetQuickNote",
            "testSetActivityMetric", "testSetBloodPressureMetric",
            "testSetGlucoseMetric", "testSetPulseOXMetric", "testSetTemperatureMetric"}, priority = 1)
    public void testGetPatientProfile() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "get");
        Profile profile = response.as(Profile.class);

        assertNotNull(profile.getClinician());
        assertNotNull(profile.getClinician().getName());
        assertNotNull(profile.getClinician().getHrsid());
        assertNotNull(profile.getExtension());
//        assertEquals(profile.getGender(), patientData.getGender());
        assertNotNull(profile.getReadmission());
        assertNotNull(profile.getEdvisit());
        assertNotNull(profile.getTestpatient());
        assertEquals(profile.getPid(), patientData.getPid());
        assertEquals(profile.getLanguage(), "english");
        assertEquals(profile.getHrsid(), newPatientId);
        assertNotNull(profile.getStartdate());
        assertNotNull(profile.getLastmoduleupdate());
        assertNotNull(profile.getVolume());
        assertEquals(profile.getPhone(), patientData.getPhone());
        assertEquals(profile.getDob(), patientData.getDob());
        assertEquals(profile.getName().getMiddle(), patientData.getName().getMiddle());
        assertEquals(profile.getName().getLast(), patientData.getName().getLast());
        assertEquals(profile.getName().getFirst(), patientData.getName().getFirst());
        assertNotNull(profile.getEnvphone());
        assertNotNull(profile.getConditions());
        assertNotNull(profile.getAudioreminders());
        assertEquals(profile.getStatus(), "activated");

        assertNotNull(profile.getPcv().getActive());
        assertNotNull(profile.getPcv().getPhone());

//        profile.getPatientInfoCustomAttributes().forEach(attribute -> {
//            assertNotNull(attribute.getId());
//            assertNotNull(attribute.getName());
//            assertNotNull(attribute.getType());
//            assertNotNull(attribute.getRequired());
//            assertNotNull(attribute.getPatientsetup());
//            assertNotNull(attribute.get_class());
//            assertNotNull(attribute.getEmrTracked());
//            assertNotNull(attribute.getLastUpdated());
//        });
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetGenderUnknownToPatientSettings() {

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT)));


        SettingPatient settingPatient = new SettingPatient();
        List<CustomNewAtributs> custom = new ArrayList<>();

        CustomNewAtributs first = new CustomNewAtributs();
        first.setId(restUtilities.getRandomInt(1, 100));
        first.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomNewAtributs two = new CustomNewAtributs();
        two.setId(restUtilities.getRandomInt(1, 100));
        two.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        two.setType("boolean");
        two.setRequired(true);
        two.setPatientsetup(true);
        two.set_class("col-xs-3");
        two.setEmrTracked(false);
        two.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        two.set$$hashKey("object:2415");
        two.setValue("true");

        CustomNewAtributs three = new CustomNewAtributs();
        three.setId(restUtilities.getRandomInt(1, 100));
        three.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        three.setType("boolean");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-3");
        three.setEmrTracked(false);
        three.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        three.set$$hashKey("object:2415");
        three.setValue("true");

        custom.add(first);
        custom.add(two);
        custom.add(three);
        settingPatient.setPATIENTINFO_CUSTOMATTRIBUTES(custom);
        settingPatient.setSubgroup("test subgroup");

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        settingPatient.setLasthospitalization(RandomData.getTimePeriod(5, dateFormat).get("minusDays"));
        settingPatient.setGender(GenderTypes.UNKNOWN);
        settingPatient.setAlternatefirstname(restUtilities.getRandomString());
        settingPatient.setAlternatelastname(restUtilities.getRandomString());
        settingPatient.setAlternatetelephone(restUtilities.getRandomString());
        newReqSpec.body(settingPatient);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertEquals(response.statusCode(), 200);

        com.hrs.cc.api.models.patients.profile.Profile profile = RestAssistant.getPatientById(newPatientId);
        assertEquals(profile.getGender(), GenderTypes.UNKNOWN);

    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetGenderUnknownToPatientSettings"}, priority = 1)
    public void testSetGenderOtherToPatientSettings() {
        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT)));


        SettingPatient settingPatient = new SettingPatient();
        List<CustomNewAtributs> custom = new ArrayList<>();

        CustomNewAtributs first = new CustomNewAtributs();
        first.setId(restUtilities.getRandomInt(1, 100));
        first.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomNewAtributs two = new CustomNewAtributs();
        two.setId(restUtilities.getRandomInt(1, 100));
        two.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        two.setType("boolean");
        two.setRequired(true);
        two.setPatientsetup(true);
        two.set_class("col-xs-3");
        two.setEmrTracked(false);
        two.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        two.set$$hashKey("object:2415");
        two.setValue("true");

        CustomNewAtributs three = new CustomNewAtributs();
        three.setId(restUtilities.getRandomInt(1, 100));
        three.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        three.setType("boolean");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-3");
        three.setEmrTracked(false);
        three.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        three.set$$hashKey("object:2415");
        three.setValue("true");

        custom.add(first);
        custom.add(two);
        custom.add(three);
        settingPatient.setPATIENTINFO_CUSTOMATTRIBUTES(custom);
        settingPatient.setSubgroup("test subgroup");

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        settingPatient.setLasthospitalization(RandomData.getTimePeriod(5, dateFormat).get("minusDays"));
        settingPatient.setGender(GenderTypes.OTHER);
        settingPatient.setAlternatefirstname(restUtilities.getRandomString());
        settingPatient.setAlternatelastname(restUtilities.getRandomString());
        settingPatient.setAlternatetelephone(restUtilities.getRandomString());
        newReqSpec.body(settingPatient);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertEquals(response.statusCode(), 200);

        com.hrs.cc.api.models.patients.profile.Profile profile = RestAssistant.getPatientById(newPatientId);
        assertEquals(profile.getGender(), GenderTypes.OTHER);

    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient",
            "testSetGenderUnknownToPatientSettings", "testSetGenderOtherToPatientSettings"}, priority = 1)
    public void testSetGenderMaleToPatientSettings() {
        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT)));


        SettingPatient settingPatient = new SettingPatient();
        List<CustomNewAtributs> custom = new ArrayList<>();

        CustomNewAtributs first = new CustomNewAtributs();
        first.setId(restUtilities.getRandomInt(1, 100));
        first.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomNewAtributs two = new CustomNewAtributs();
        two.setId(restUtilities.getRandomInt(1, 100));
        two.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        two.setType("boolean");
        two.setRequired(true);
        two.setPatientsetup(true);
        two.set_class("col-xs-3");
        two.setEmrTracked(false);
        two.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        two.set$$hashKey("object:2415");
        two.setValue("true");

        CustomNewAtributs three = new CustomNewAtributs();
        three.setId(restUtilities.getRandomInt(1, 100));
        three.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        three.setType("boolean");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-3");
        three.setEmrTracked(false);
        three.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        three.set$$hashKey("object:2415");
        three.setValue("true");

        custom.add(first);
        custom.add(two);
        custom.add(three);
        settingPatient.setPATIENTINFO_CUSTOMATTRIBUTES(custom);
        settingPatient.setSubgroup("test subgroup");

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        settingPatient.setLasthospitalization(RandomData.getTimePeriod(5, dateFormat).get("minusDays"));
        settingPatient.setGender(GenderTypes.MALE);
        settingPatient.setAlternatefirstname(restUtilities.getRandomString());
        settingPatient.setAlternatelastname(restUtilities.getRandomString());
        settingPatient.setAlternatetelephone(restUtilities.getRandomString());
        newReqSpec.body(settingPatient);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertEquals(response.statusCode(), 200);

        com.hrs.cc.api.models.patients.profile.Profile profile = RestAssistant.getPatientById(newPatientId);
        assertEquals(profile.getGender(), GenderTypes.MALE);

    }

    @Test(dependsOnMethods = {"testAddNewPatient",
            "testSetMetricsToPatient",
            "testSetGenderUnknownToPatientSettings",
            "testSetGenderOtherToPatientSettings",
            "testSetGenderMaleToPatientSettings"}, priority = 1)
    public void testSetEmptyGenderToPatientSettings() {
        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT)));

        SettingPatient settingPatient = new SettingPatient();
        List<CustomNewAtributs> custom = new ArrayList<>();

        CustomNewAtributs first = new CustomNewAtributs();
        first.setId(restUtilities.getRandomInt(1, 100));
        first.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomNewAtributs two = new CustomNewAtributs();
        two.setId(restUtilities.getRandomInt(1, 100));
        two.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        two.setType("boolean");
        two.setRequired(true);
        two.setPatientsetup(true);
        two.set_class("col-xs-3");
        two.setEmrTracked(false);
        two.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        two.set$$hashKey("object:2415");
        two.setValue("true");

        CustomNewAtributs three = new CustomNewAtributs();
        three.setId(restUtilities.getRandomInt(1, 100));
        three.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        three.setType("boolean");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-3");
        three.setEmrTracked(false);
        three.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        three.set$$hashKey("object:2415");
        three.setValue("true");

        custom.add(first);
        custom.add(two);
        custom.add(three);
        settingPatient.setPATIENTINFO_CUSTOMATTRIBUTES(custom);
        settingPatient.setSubgroup("test subgroup");

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        settingPatient.setLasthospitalization(RandomData.getTimePeriod(5, dateFormat).get("minusDays"));
        settingPatient.setGender(GenderTypes.EMPTY);
        settingPatient.setAlternatefirstname(restUtilities.getRandomString());
        settingPatient.setAlternatelastname(restUtilities.getRandomString());
        settingPatient.setAlternatetelephone(restUtilities.getRandomString());
        newReqSpec.body(settingPatient);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertEquals(response.statusCode(), 200);

        com.hrs.cc.api.models.patients.profile.Profile profile = RestAssistant.getPatientById(newPatientId);
        assertEquals(profile.getGender(), GenderTypes.EMPTY);

    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient",
            "testSetGenderUnknownToPatientSettings", "testSetGenderOtherToPatientSettings",
            "testSetGenderMaleToPatientSettings", "testSetEmptyGenderToPatientSettings"}, priority = 1)
    public void testSetPatientSettings() {

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT)));


        SettingPatient settingPatient = new SettingPatient();
        List<CustomNewAtributs> custom = new ArrayList<>();

        CustomNewAtributs first = new CustomNewAtributs();
        first.setId(restUtilities.getRandomInt(1, 100));
        first.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomNewAtributs two = new CustomNewAtributs();
        two.setId(restUtilities.getRandomInt(1, 100));
        two.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        two.setType("boolean");
        two.setRequired(true);
        two.setPatientsetup(true);
        two.set_class("col-xs-3");
        two.setEmrTracked(false);
        two.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        two.set$$hashKey("object:2415");
        two.setValue("true");

        CustomNewAtributs three = new CustomNewAtributs();
        three.setId(restUtilities.getRandomInt(1, 100));
        three.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        three.setType("boolean");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-3");
        three.setEmrTracked(false);
        three.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        three.set$$hashKey("object:2415");
        three.setValue("true");

        custom.add(first);
        custom.add(two);
        custom.add(three);
        settingPatient.setPATIENTINFO_CUSTOMATTRIBUTES(custom);
        settingPatient.setSubgroup("test subgroup");

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        settingPatient.setLasthospitalization(RandomData.getTimePeriod(5, dateFormat).get("minusDays"));
        settingPatient.setGender(GenderTypes.FEMALE);
        settingPatient.setAlternatefirstname(restUtilities.getRandomString());
        settingPatient.setAlternatelastname(restUtilities.getRandomString());
        settingPatient.setAlternatetelephone(restUtilities.getRandomString());
        newReqSpec.body(settingPatient);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertEquals(response.statusCode(), 200);

        com.hrs.cc.api.models.patients.profile.Profile profile = RestAssistant.getPatientById(newPatientId);
        assertEquals(profile.getGender(), GenderTypes.FEMALE);
    }

}
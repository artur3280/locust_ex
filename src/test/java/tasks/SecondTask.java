package tasks;

import com.github.myzhan.locust4j.AbstractTask;
import com.github.myzhan.locust4j.Locust;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.inventory.InventoryResponse;
import core.PropertyWorker;
import core.rest.RestUtilities;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SecondTask extends AbstractTask {
    private int weight;
    private String name;

    public SecondTask(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void execute() throws Exception {
        RestUtilities restUtilities = new RestUtilities();
        PropertyWorker propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.INVENTORY));
        reqSpec.log().all();
        ResponseSpecification resPec = restUtilities.getResponseSpecification();


        restUtilities.setContentType(ContentType.JSON);

        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");

        Locust.getInstance().recordSuccess("http", getName(), response.getTime(), 1);
    }


}

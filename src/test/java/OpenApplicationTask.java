import com.github.myzhan.locust4j.AbstractTask;
import com.github.myzhan.locust4j.Locust;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class OpenApplicationTask extends AbstractTask {
    private int weight;

    @Override
    public int getWeight() {
        return weight;
    }


    @Override
    public String getName() {
        return "Open application task";
    }

    public OpenApplicationTask(int weight){
        this.weight = weight;
    }

    @Override
    public void execute() {
        try {

            Response response = given().get("http://blazedemo.com");
            Locust.getInstance().recordSuccess("http", getName(), response.getTime(), 1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
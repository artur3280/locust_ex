import com.github.myzhan.locust4j.Locust;
import tasks.FirstTask;
import tasks.SecondTask;

public class Main {

    ///https://www.blazemeter.com/blog/locust-performance-testing-using-java-and-kotlin/
    ///https://docs.locust.io/en/stable/installation.html#installing-locust-on-macos
    public static void main(String[] args) {


        Locust locust = Locust.getInstance();
        locust.setMasterHost("127.0.0.1");
        locust.setMasterPort(5557); //some free port to run the Locust slave

       /* locust.run(
                new FindFlightsTask(1),
                new OpenApplicationTask(1)
        ); // <- You custom performance tasks should be here*/


        locust.run(
                new FirstTask(5, "Get patient data"),
                new SecondTask(5, "Get inventory")
        ); // <- You custom performance tasks should be here

    }
}

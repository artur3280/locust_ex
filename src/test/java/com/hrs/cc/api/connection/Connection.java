package com.hrs.cc.api.connection;


import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.account_data.Account;
import com.hrs.cc.api.models.integratioins_app.clinician_auth_token.ClinicianAuthToken;
import core.PropertyWorker;
import core.rest.RestUtilities;
import core.rest.auth.schemes.AuthScheme;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class Connection {
    private static RequestSpecification reqSpec;
    private static ResponseSpecification resSpec;
    private static RestUtilities restUtilities = new RestUtilities();
    private static Response response;
    private static final String TOKEN_PATH = "/token.json";
    public static final String AUTH_PROP = "./auth.properties";
    private static PropertyWorker propertyWorker;

    public static RequestSpecification UserAuth() {
        propertyWorker = new PropertyWorker(AUTH_PROP);
        restUtilities.setBaseUri(Path.BASE_URI);
        RequestSpecification requestSpecification = restUtilities.getRequestSpecificationInBody(Auth.userAuthData());
        requestSpecification.basePath("");
        requestSpecification.cookies(getUserData());
        requestSpecification.contentType(ContentType.JSON);
        Integer statusCode = authChecker(requestSpecification).getStatusCode();
        try {
            if (!(new File(AUTH_PROP).exists()) || statusCode.equals(401)) {
                System.out.println("Old token has expired! Generate new Auth token. Current status:" + statusCode);
                restUtilities.setBaseUri(Path.BASE_URI);
                reqSpec = restUtilities.getRequestSpecificationInBody(Auth.userAuthData());
                reqSpec.basePath(Path.LOGIN);
                restUtilities.setContentType(ContentType.JSON);
                resSpec = restUtilities.getResponseSpecification();
                response = restUtilities.getResponse(reqSpec, "post");
                propertyWorker.setProperty("cc2", response.getCookies().get("cc2"));
                propertyWorker.setProperty("PHPSESSID", response.getCookies().get("PHPSESSID"));
            }
        } catch (Exception e) {
            System.out.println("User does not authorised! Please check! ");
        }
        return reqSpec;
    }

    public static RequestSpecification ClinicianAuthIntegration() {
        propertyWorker = new PropertyWorker(AUTH_PROP);

        try {

            if (!new File(AUTH_PROP).exists() ||
                    getClinicianToken().get("clinician_intg_token") == null ||
                    getIntegrationStatusCode().equals(401)) {

                restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
                reqSpec = restUtilities.getRequestSpecificationInBody(Auth.userAuthDataIntegration());
                reqSpec.basePath(Path.TOKENS_INTEGRATION);
                restUtilities.setContentType(ContentType.JSON);
                restUtilities.setEndPoint();
                resSpec = restUtilities.getResponseSpecification();
                response = restUtilities.getResponse(reqSpec, "post");
                ClinicianAuthToken authToken = response.as(ClinicianAuthToken.class);
                propertyWorker.setProperty("clinician_intg_token", authToken.getData().getToken());

            }
        } catch (Exception e) {
            System.out.println("User does not authorised! Please check! ");
        }
        return reqSpec;
    }

//    static void AdminAuth(){
//        RestUtilities restUtilities = new RestUtilities();
//        RequestSpecification reqSpec;
//
//        restUtilities.setBaseUri("https://admin.hrsqa.com");
//        reqSpec = restUtilities.getRequestSpecification();
//        reqSpec.header("Authorization", AuthScheme.basicScheme("admin","LaCroix").generateAuthToken());
//        reqSpec.log().all();
//        restUtilities.setContentType(ContentType.JSON);
//        restUtilities.setEndPoint();
//
//        ResponseSpecification resSpec = restUtilities.getResponseSpecification(
//                restUtilities.createResponseBuilder()
//                        .expectStatusCode(302).expectContentType(ContentType.fromContentType("text/html")));
//
//        response = restUtilities.getResponse(reqSpec, "post");
//        System.out.println(response.getCookies());
//
//        response.prettyPrint();
//    }


    public static Response adminAuthGeneral(){
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_ADMIN_V2);

        restUtilities.setEndPoint(EndPoints.ADMIN_LOGIN);

        Map<String, String> data = new HashMap<>();
        data.put("request", "login");
        data.put("u", "olga_admin");
        data.put("p", "root");
        reqSpec.header("authorization", AuthScheme.basicScheme("admin","LaCroix").generateAuthToken());
        reqSpec.formParams(data);
        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
//        reqSpec.log().all();

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200).expectContentType(ContentType.fromContentType("text/html")));


        return restUtilities.getResponse(reqSpec, "post");
    }

    public static String getTokenId() {
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.cookies(getUserData());
        resSpec = restUtilities.getResponseSpecification();
        Response response = restUtilities.getResponse(reqSpec, "get");
        Account accountData = response.as(Account.class);
        return accountData.getFirebase().getAuthentication();
    }

    public static void setDataToFile(Response response) throws IOException {

        FileWriter fileWriter =
                new FileWriter(new File("").getCanonicalPath() + TOKEN_PATH);
        JSONObject object = new JSONObject();
        object.put("cc2", response.getCookies().get("cc2"));
        object.put("PHPSESSID", response.getCookies().get("PHPSESSID"));
        fileWriter.write(object.toJSONString());
        fileWriter.flush();

    }

    public static Map<String, String> getUserData() {
        Map<String, String> data = new HashMap<>();
        try {
            data.put("cc2", propertyWorker.getProperty("cc2"));
            data.put("PHPSESSID", propertyWorker.getProperty("PHPSESSID"));
        } catch (Exception e) {
            System.out.println("No such file for get data!");
        }
        return data;
    }

    public static Map<String, String> getClinicianToken() {
        Map<String, String> data = new HashMap<>();
        try {
            data.put("clinician_intg_token", propertyWorker.getProperty("clinician_intg_token"));
        } catch (Exception e) {
            System.out.println("No such file for get data!");
        }
        return data;
    }


    public static Response authChecker(RequestSpecification reqSpec) {
        return given().spec(reqSpec).request(Method.GET, "");
    }

    private static Integer getIntegrationStatusCode() {
        RequestSpecification requestSpecification = restUtilities.getRequestSpecificationInBody();
        requestSpecification.baseUri(Path.BASE_URI_APP_INTEGRATION);
        requestSpecification.basePath(Path.API_V2.concat(EndPoints.REPORT_STATUSES));
        requestSpecification.header("Authorization", "Bearer ".concat(getClinicianToken().get("clinician_intg_token")));
        requestSpecification.contentType(ContentType.JSON);
        Integer status = authChecker(requestSpecification).statusCode();
        if(status.equals(401))
            System.out.println("Old token has expired! Generate new Auth token. Current status:" + status);
        return status;
    }

}



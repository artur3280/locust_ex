package com.hrs.cc.api.constans;

public enum EnvModules {
    ACTIVITY("Activity"),
    BLOOD_PRESSURE("Blood Pressure"),
    GLUCOSE("Glucose"),
    MEDICATION("Medication"),
    PULSE_OX("Pulse Ox"),
    SURVEY("Survey"),
    TEMPERATURE("Temperature"),
    WEIGHT("Weight"),
    WOUND_IMAGING("Wound Imaging"),
    PATIENTCONNECT_VOICE("PatientConnect Voice"),
    STETHOSCOPE("Stethoscope");

    private String mId;

    EnvModules(String i) {
        this.mId = i;

    }

    public String get() {
        return mId;
    }
    }

package com.hrs.cc.api.constans;

import core.rest.configs.Configurations;

import java.util.HashMap;
import java.util.Map;

public class Auth {
    public static final String USER_NAME = Configurations.getConfigParam("user_name_hrs");
    public static final String PASSWORD = Configurations.getConfigParam("password_hrs");
    public static final String FIRST_NAME = "Olga";
    public static final String LAST_NAME = "Gnezdyonova";
    public static final String MIDDLE_NAME = "";
    public static final String TIME_ZONE = "America/New_York";
    public static final String EMAIL = "olga.gnezdyonova@gmail.com";
    public static final String TELEPHONE = "7329897390";
    public static final String TYPE = "credentials";
//    public static final String PATIENT_FIRST_NAME_INTG = "UserApp";
//    public static final String PATIENT_LAST_NAME_INTG = "Integration";
//    public static final String PATIENT_PID = "testAppIntegration";
    public static final String PATIENT_FIRST_NAME_INTG = "User2App";
    public static final String PATIENT_LAST_NAME_INTG = "Integ2ration";
    public static final String PATIENT_PID = "testAppIntegrationReserve";
    public static final String PATIENT_MIDDLE_NAME_INTG = "Api";
    public static final String CONFIGURATION_FILE = "./config.encode";

    public static Map<String, String> userAuthData() {
        Map<String, String> userData = new HashMap<>();
        userData.put("type", "clinician");
        userData.put("user", USER_NAME);
        userData.put("pass", PASSWORD);
        return userData;
    }


    public static String userAuthDataIntegration() {

        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + Auth.USER_NAME + "\"" + ",\n" +
                "        \"password\": " + "\"" + Auth.PASSWORD + "\"" + ",\n" +
                "        \"type\":" + "\"" + Auth.TYPE + "\"" + "\n" +
                "    }\n" +
                "}";
        return dataU;
    }


}

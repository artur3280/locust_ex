package com.hrs.cc.api.constans;

public class PatientStatuses {
    public static final String PAUSE = "paused";
    public static final String DEACTIVATED = "deactivated";
    public static final String ACTIVATED = "activated";
    public static final String DISCHARGED = "discharged";

}

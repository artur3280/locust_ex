package com.hrs.cc.api.constans;

import core.rest.configs.Configurations;

public class Path {
    public static String BASE_URI = "https://gateway.hrsalpha.com";
    public static String BASE_URI_APP_INTEGRATION = "https://gateway.hrsalpha.com";
    public static String BASE_ADMIN_URI_APP_INTEGRATION = "https://admin.hrsalpha.com";
    public static final String LOGIN = "/login";
    public static final String TOKENS = "/tokens";
    public static final String ENVIRONMENT = "/environment";
    public static final String PATIENTS = "/patients";
    public static final String WI = "/wi";
    public static final String PATIENT = "/patient";
    public static final String CLINICIAN = "/clinician";
    public static final String DASHBOARD = "/dashboard";
    public static final String INVENTORY = "/inventory";
    public static final String NOTES = "/notes";
    public static final String REPORT_STATUSES = "/report-statuses";
    public static final String REPORTS = "/reports";
    public static final String TOKENS_INTEGRATION = "/tokens";
    public static final String API_V2 = "/apiv2";
    public static final String CHAT = "/chat";
    public static final String API_V1 = "/v1";
    public static final String API_ADMIN_V2 = "/v2";
    public static final String NOTIFICATIONS = "/notifications";
    public static final String VIDEO_CALL = "/video-calls";
    public static final String VIDEO_TOKENS_CALL = "/video-call-tokens";

    public static void setBaseUrl(String[] url){
        try {
            Path.BASE_URI = "https://gateway.".concat(url[0]).concat(".com");
            Path.BASE_URI_APP_INTEGRATION = "https://gateway.".concat(url[0]).concat(".com");
            Path.BASE_ADMIN_URI_APP_INTEGRATION = "https://admin.".concat(url[0]).concat(".com");
            System.out.println(Path.BASE_URI);
            System.out.println(Path.BASE_ADMIN_URI_APP_INTEGRATION);
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Using default url ");
        }

    }

}

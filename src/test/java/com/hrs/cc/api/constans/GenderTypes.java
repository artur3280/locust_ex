package com.hrs.cc.api.constans;



public class GenderTypes {
    public static final String UNKNOWN = "U";
    public static final String MALE = "M";
    public static final String FEMALE = "F";
    public static final String OTHER = "X";
    public static final String EMPTY = "";
}

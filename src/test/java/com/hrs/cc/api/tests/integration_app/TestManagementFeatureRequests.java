package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.account_data.Account;
import com.hrs.cc.api.models.integratioins_app.clinician_auth_token.ClinicianAuthToken;
import com.hrs.cc.api.models.requests.add_new_clinicain.Data;
import com.hrs.cc.api.models.requests.add_new_clinicain.NewClinician;
import com.hrs.cc.api.models.requests.add_new_clinicain.Profile;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestManagementFeatureRequests extends Assert {
    private ObjectMapper mapper = new ObjectMapper();
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private PropertyWorker propertyWorker;
    private static String moderatorWestToken;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        moderatorWestToken = RestAssistant.authModeratorWest("olga_west", "olga1983");
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath("");
    }

    @AfterClass
    public void removeObj() {
        mapper = null;
        reqSpec = null;
        resPec = null;
        restUtilities = null;
        queryParam = null;
        userData = null;
        propertyWorker = null;
        System.gc();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
    }


    @Test
    public void testCreateNewClinicianWhoSubgroupsAreSubsetOfTheModeratorSubgroups() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(moderatorWestToken));

        NewClinician clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        profile.setRights("Clinician");
        ArrayList sub = new ArrayList();
        sub.add("west coast");
        profile.setSubgroups(sub);

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);

        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.statusCode(), 200);

        /*Auth created clinician*/
        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        assertNotNull(authToken.getData().getToken(), "token is null");
        assertNotNull(authToken.getData().getRefresh(), "refresh token is null");
        String clinicianToken = authToken.getData().getToken();

        /*Get info about clinician*/
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(clinicianToken));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);

        /*Remove clinician*/
        RestAssistant.removeClinician(account.getUser(), moderatorWestToken);
    }

    @Test
    public void testCreateNewClinicianWhoSubgroupsFalOutsideOfTheModeratorSubgroups() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(403));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(moderatorWestToken));

        NewClinician clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        profile.setRights("Clinician");
        ArrayList sub = new ArrayList();
        sub.add("east coast");
        profile.setSubgroups(sub);

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);

        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.statusCode(), 403);
    }

    @Test
    public void testCreateNewClinicianWhoHasNoSubgroupsInPayload() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(403));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(moderatorWestToken));

        NewClinician clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        profile.setRights("Clinician");
        ArrayList sub = new ArrayList();
//        sub.add("east coast");
        profile.setSubgroups(sub);

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);

        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.statusCode(), 403);
    }

    @Test
    public void testCreateNewClinicianWithSubgroupsWhenTheModeratorWithoutSubgroups() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        NewClinician clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        profile.setRights("Clinician");
        ArrayList sub = new ArrayList();
        sub.add("west coast");
        profile.setSubgroups(sub);

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);

        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.statusCode(), 200);

        /*Auth created clinician*/
        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        assertNotNull(authToken.getData().getToken(), "token is null");
        assertNotNull(authToken.getData().getRefresh(), "refresh token is null");
        String clinicianToken = authToken.getData().getToken();

        /*Get info about clinician*/
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(clinicianToken));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);

        /*Remove clinician*/
        RestAssistant.removeClinician(account.getUser());
    }

    @Test
    public void testCreateNewClinicianWithOutSubgroupsWhenTheModeratorWithoutSubgroups() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        NewClinician clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        profile.setRights("Clinician");
        ArrayList sub = new ArrayList();
//        sub.add("west coast");
        profile.setSubgroups(sub);

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);

        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.statusCode(), 200);

        /*Auth created clinician*/
        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        assertNotNull(authToken.getData().getToken(), "token is null");
        assertNotNull(authToken.getData().getRefresh(), "refresh token is null");
        String clinicianToken = authToken.getData().getToken();

        /*Get info about clinician*/
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(clinicianToken));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);

        /*Remove clinician*/
        RestAssistant.removeClinician(account.getUser());
    }

    @Test
    public void testAddingSubgroupToClinicianIfTheModeratorIsNotPartOf () {
        String clinicianId = RestAssistant.addClinician("west coast", moderatorWestToken);

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(403));

        NewClinician clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        ArrayList sub = new ArrayList();
        sub.add("east coast");
        profile.setSubgroups(sub);

        data.setProfile(profile);
        clinician.setData(data);

        reqSpec.body(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);
        reqSpec.header("Authorization", "Bearer ".concat(moderatorWestToken));

        Response response = restUtilities.getResponse(reqSpec, "patch");
        assertEquals(response.statusCode(), 403);

        RestAssistant.removeClinician(clinicianId, moderatorWestToken);
    }

    @Test
    public void testRemovingAllSubgroupsSoThatTheClinicianWillResultInHavingNoSubgroups () {
        String clinicianId = RestAssistant.addClinician("west coast", moderatorWestToken);

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(403));

        NewClinician clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        ArrayList sub = new ArrayList();
//        sub.add("east coast");
        profile.setSubgroups(sub);

        data.setProfile(profile);
        clinician.setData(data);

        reqSpec.body(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);
        reqSpec.header("Authorization", "Bearer ".concat(moderatorWestToken));

        Response response = restUtilities.getResponse(reqSpec, "patch");
        assertEquals(response.statusCode(), 403);

        RestAssistant.removeClinician(clinicianId, moderatorWestToken);
    }

    @Test
    public void testAddingSubgroupIfClinicianOutsideOfTheModeratorSubgroups () {
        String clinicianId = RestAssistant.addClinician("east coast");

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(403));

        NewClinician clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        ArrayList sub = new ArrayList();
        sub.add("west coast");
        profile.setSubgroups(sub);

        data.setProfile(profile);
        clinician.setData(data);

        reqSpec.body(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);
        reqSpec.header("Authorization", "Bearer ".concat(moderatorWestToken));

        Response response = restUtilities.getResponse(reqSpec, "patch");
        assertEquals(response.statusCode(), 403);

        RestAssistant.removeClinician(clinicianId);
    }

    @Test
    public void testRemovingTheOutsideSubgroupFromClinicianIfTheModeratorIsNotPartOf () {
        String clinicianId = RestAssistant.addClinician("east coast");

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(403));

        NewClinician clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        ArrayList sub = new ArrayList();
//        sub.add("west coast");
        profile.setSubgroups(sub);

        data.setProfile(profile);
        clinician.setData(data);

        reqSpec.body(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);
        reqSpec.header("Authorization", "Bearer ".concat(moderatorWestToken));

        Response response = restUtilities.getResponse(reqSpec, "patch");
        assertEquals(response.statusCode(), 403);

        RestAssistant.removeClinician(clinicianId);
    }

    @Test
    public void testAddingSubgroupIfClinicianWhenModeratorWithoutSubgroups () {
        String clinicianId = RestAssistant.addClinician("east coast");

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(204));

        NewClinician clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        ArrayList sub = new ArrayList();
        sub.add("west coast");
        profile.setSubgroups(sub);

        data.setProfile(profile);
        clinician.setData(data);

        reqSpec.body(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "patch");
        assertEquals(response.statusCode(), 204);

        RestAssistant.removeClinician(clinicianId);
    }

    @Test
    public void testRemovingTheSubgroupFromClinicianIfTheModeratorWithoutSubgroup () {
        String clinicianId = RestAssistant.addClinician("east coast");

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(204));

        NewClinician clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        ArrayList sub = new ArrayList();
//        sub.add("west coast");
        profile.setSubgroups(sub);

        data.setProfile(profile);
        clinician.setData(data);

        reqSpec.body(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "patch");
        assertEquals(response.statusCode(), 204);

        RestAssistant.removeClinician(clinicianId);
    }

    @Test
    public void testDeleteTheClinicianWhoSubgroupsAreSubsetOfTheModeratorSubgroups () {
        String clinicianId = RestAssistant.addClinician("west coast");

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(204));


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);
        reqSpec.header("Authorization", "Bearer ".concat(moderatorWestToken));

        Response response = restUtilities.getResponse(reqSpec, "delete");
        assertEquals(response.statusCode(), 204);
    }

    @Test
    public void testDeleteTheClinicianWhoSubgroupsFallOutsideTheModeratorSubgroups () {
        String clinicianId = RestAssistant.addClinician("east coast");

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(403));


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);
        reqSpec.header("Authorization", "Bearer ".concat(moderatorWestToken));

        Response response = restUtilities.getResponse(reqSpec, "delete");
        assertEquals(response.statusCode(), 403);
        RestAssistant.removeClinician(clinicianId);
    }

    @Test
    public void testDeleteTheClinicianWithNoSubgroups () {
        ArrayList<String> sub = new ArrayList<>();
//        String clinicianId = RestAssistant.addClinician(sub);
        String clinicianId = RestAssistant.getEnabledClinicians().getData().stream().filter(
                c->c.getProfile().getSubgroup().equals("")).findFirst().get().getId();

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(403));


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);
        reqSpec.header("Authorization", "Bearer ".concat(moderatorWestToken));

        Response response = restUtilities.getResponse(reqSpec, "delete");
        assertEquals(response.statusCode(), 403);
        RestAssistant.removeClinician(clinicianId);
    }

    @Test
    public void testModeratorWithoutSubgroupDeletingTheClinicianWithNoSubgroups () {
        ArrayList<String> sub = new ArrayList<>();
        String clinicianId = RestAssistant.addClinician(sub);

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(204));


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "delete");
        assertEquals(response.statusCode(), 204);
        RestAssistant.removeClinician(clinicianId);
    }

    @Test
    public void testModeratorWithoutSubgroupDeletingTheClinicianWithSubgroups () {
        ArrayList<String> sub = new ArrayList<>();
        sub.add("east coast");
        String clinicianId = RestAssistant.addClinician(sub);

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(204));


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "delete");
        assertEquals(response.statusCode(), 204);
        RestAssistant.removeClinician(clinicianId);
    }


}
package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.*;
import com.hrs.cc.api.models.patients.Patients;
import com.hrs.cc.api.models.patients.patient_list.*;
import com.hrs.cc.api.models.patients.patient_metric.PatientMetric;
import com.hrs.cc.api.models.patients.patient_metric.response_after_add.MetricksResponse;
import com.hrs.cc.api.models.requests.set_patient_metricks.Schedule;
import com.hrs.cc.api.models.requests.set_patient_metricks.*;
import core.PropertyWorker;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestGatewayRequests extends Assert {
    private ObjectMapper mapper = new ObjectMapper();
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private PropertyWorker propertyWorker;
    private static List<List<PatientList>> patientsList;


    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        Logger.debugging(false);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.API_V2);
    }

    @AfterClass
    public void removeObj() {
        mapper = null;
        reqSpec = null;
        resPec = null;
        restUtilities = null;
        queryParam = null;
        userData = null;
        propertyWorker = null;
        System.gc();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        restUtilities.removeQueryParam(reqSpec, "filter[patient]");
        restUtilities.removeQueryParam(reqSpec, "start");
        restUtilities.removeQueryParam(reqSpec, "end");
        queryParam.clear();
    }

    @Test
    public void testGateWayPatientListResponse() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS);
        restUtilities.createQueryParam(reqSpec,"status[]", "Activated");
        Response response = restUtilities.getResponse(reqSpec, "get");
        Patients patients = response.as(Patients.class);
        patientsList = patients.getPatientlist().stream().filter(
                patient -> !patient.get(0).getProfile().getName().getFirst().
                        equals(Auth.PATIENT_FIRST_NAME_INTG) &&
                        !patient.get(0).getProfile().getName().getLast().
                        equals(Auth.PATIENT_LAST_NAME_INTG) &&
                        patient.get(0).getProfile().getClinician() != null &&
                        patient.get(0).getProfile().getName().getLast().contains("test") &&
                        patient.get(0).getProfile().getClinician().getName().
                                equals(Auth.FIRST_NAME + " " + Auth.LAST_NAME) &&
//                        patient.get(0).getDay() >= 10 &&
                        patient.get(0).getProfile().getStatus().equals("activated")).collect(Collectors.toList());

        patients.getPatientlist().forEach(patientLists -> {
            assertNotNull(patientLists);
            assertNotNull(patientLists.get(0).getProfile(), "Profile field is null");
            assertNotNull(patientLists.get(0).getAdditionalProperties(), "AdditionalProperties field is null");
            assertNotNull(patientLists.get(0).getDay(), "Day field is null");
            assertNotNull(patientLists.get(0).getRisk(), "Risk field is null");
            assertNotNull(patientLists.get(0).getTracking(), "Tracking field is null");
            assertNotNull(patientLists.get(0).getTs(), "Ts field is null");
            try {
                assertNotNull(patientLists.get(0).getProfile().getClinician());
            } catch (AssertionError e) {
                System.out.println("Patient ".concat(patientLists.get(0).getProfile().getHrsid())
                        .concat(" does not attached to clinicians!! Please check!"));
            }
            try {
                assertNotNull(patientLists.get(0).getProfile().getExtension());
            } catch (AssertionError e) {
                System.out.println("Patient ".concat(patientLists.get(0).getProfile().getHrsid())
                        .concat(" does not have Extension!! Please check!"));
            }
            assertNotNull(patientLists.get(0).getProfile().getGender(), "Gender field is null");
            assertNotNull(patientLists.get(0).getProfile().getReadmission(), "Readmission field is null");
            assertNotNull(patientLists.get(0).getProfile().getEdvisit(), "Edvisit field is null");
            assertNotNull(patientLists.get(0).getProfile().getTestpatient(), "Testpatient field is null");
            assertNotNull(patientLists.get(0).getProfile().getPid(), "Pid field is null");
            assertNotNull(patientLists.get(0).getProfile().getHrsid(), "Hrsid field is null");
            assertNotNull(patientLists.get(0).getProfile().getStartdate(), "Startdate field is null");
            assertNotNull(patientLists.get(0).getProfile().getLastmoduleupdate(), "Lastmoduleupdate field is null");
            assertNotNull(patientLists.get(0).getProfile().getVolume(), "Volume field is null");
            assertNotNull(patientLists.get(0).getProfile().getName(), "Name field is null");
            assertNotNull(patientLists.get(0).getProfile().getConditions(), "Conditions field is null");
            assertNotNull(patientLists.get(0).getProfile().getConditions(), "Conditions field is null");
            assertNotNull(patientLists.get(0).getProfile().getAudioreminders(), "Audioreminders field is null");
            assertNotNull(patientLists.get(0).getProfile().getStatus(), "Status field is null");
            assertNotNull(patientLists.get(0).getProfile().getReviewed(), "Reviewed field is null");

            Risk detailPatient;
            try {
                detailPatient = mapper.convertValue(
                        patientLists.get(0).getRisk().get(0), Risk.class);
                detailPatient.getDetails().forEach(detail -> {
                    assertNotNull(detail, "Detail obj is null");
                    assertNotNull(detail.getMetricIds(), "MetricIds field is null");
                    assertNotNull(detail.getText(), "Text field is null");
                    assertNotNull(detail.getMetrics(), "Metrics field is null");
                    assertNotNull(detail.getType(), "Type field is null");
                });
            } catch (NullPointerException | IndexOutOfBoundsException e) {
                System.out.println("Patient does not have detail field!");
            }

            Metrics metricsPatient = null;
            try {
                metricsPatient = mapper.convertValue(patientLists.get(0).getMetrics(), Metrics.class);
                metricsPatient.getSurvey().getQuestions().forEach(question -> {
                    assertNotNull(question.getSchedule(), "Schedule field is null");
                    assertNotNull(question.getSchedule().getType(), "Schedule.Type field is null");
                    assertNotNull(question.getQuestion(), "Question field is null");
                    assertNotNull(question.getId(), "Id field is null");
                    assertNotNull(question.getTs(), "Ts field is null");
                });
            } catch (NullPointerException | IllegalArgumentException e) {
                System.out.println("The patient does not have metrics");
            }

            try {
                Medication medicationsPatient = mapper.convertValue(metricsPatient.getMedications(), Medication.class);
                medicationsPatient.getMedications().forEach(medicationItem -> {
                    assertNotNull(medicationItem.getSchedule(), "Schedule field is null");
                    assertNotNull(medicationItem.getSchedule().getType(), "Schedule.Type field is null");
                    assertNotNull(medicationItem.getDose(), "Dose field is null");
                    assertNotNull(medicationItem.getTime(), "Time field is null");
                    assertNotNull(medicationItem.getMed(), "Med field is null");
                });
            } catch (NullPointerException | IllegalArgumentException e) {
                System.out.println("The medication of patient is empty!");
            }

            assertNotNull(patientLists.get(0).getTracking(), "Tracking field is null");
            assertNotNull(patientLists.get(0).getTracking().getData(), "Tracking.Data field is null");
            assertNotNull(patientLists.get(0).getTracking().getData().getMedlist(), "Tracking.Data.Medlist field is null");
            assertNotNull(patientLists.get(0).getTracking().getData().getTracking(), "Tracking.Data.Tracking field is null");
            assertNotNull(patientLists.get(0).getTracking().getData().getVideo(), "Tracking.Data.Video field is null");
            assertNotNull(patientLists.get(0).getTracking().getData().getQuiz(), "Tracking.Data.Quiz field is null");
            assertNotNull(patientLists.get(0).getTracking().getData().getReminders(), "Tracking.Data.Reminders field is null");
            assertNotNull(patientLists.get(0).getDay(), "Day field is null");
            assertNotNull(patientLists.get(0).getTs(), "Ts field is null");
        });

        restUtilities.removeQueryParam(reqSpec, "status[]");
    }

    @Test(dependsOnMethods = {"testGateWayPatientListResponse"})
    public void testGateWayPatientByIdResponse(){
        Map<String, String> time = restUtilities.getTimePeriod(10);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.configDecode(reqSpec,true);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid());
        queryParam.put("start", time.get("current").replace("-", ""));
        queryParam.put("end", time.get("plusDays").replace("-", ""));


        Response response = restUtilities.getResponse(reqSpec, "get");
        ArrayList profile = response.jsonPath().get("profile");
        ArrayList metrics = response.jsonPath().get("metrics");
        ArrayList tracking = response.jsonPath().get("tracking");

        Profile patientProfile = mapper.convertValue(profile.get(0), Profile.class);

        assertEquals(patientsList.get(0).get(0).getProfile().getClinician().getHrsid(),
                patientProfile.getClinician().getHrsid(), "Hrs id not eql");
        assertEquals(patientsList.get(0).get(0).getProfile().getSubgroup(),
                patientProfile.getSubgroup(), "subgroup not eql");
        assertEquals(patientsList.get(0).get(0).getProfile().getName(),
                patientProfile.getName(), "name is null");

        Metrics patientMetrics = mapper.convertValue(metrics.get(0), Metrics.class);
        assertNotNull(patientMetrics.getSurvey(), "Survey field is null");
        assertNotNull(patientMetrics.getSurvey().getQuestions(), "Survey.Questions field is null");
        assertNotNull(patientMetrics.getSurvey().getQuestions().get(0).getSchedule(), "Survey.Questions.Schedule field is null");
        assertNotNull(patientMetrics.getSurvey().getQuestions().get(0).getSchedule().getType(), "Survey.Questions.Schedule.Type field is null");
        assertNotNull(patientMetrics.getSurvey().getQuestions().get(0).getQuestion(), "Survey.Questions.Question field is null");
        assertNotNull(patientMetrics.getSurvey().getQuestions().get(0).getToday(), "Survey.Questions.Today field is null");
        assertNotNull(patientMetrics.getSurvey().getQuestions().get(0).getId(), "Survey.Questions.Id field is null");
        assertNotNull(patientMetrics.getSurvey().getQuestions().get(0).getTs(), "Survey.Questions.Ts field is null");

        Tracking trackingPatient = mapper.convertValue(tracking.get(0), Tracking.class);
        assertNotNull(trackingPatient.getData().getMedlist(), "Medlist field is null");
        assertNotNull(trackingPatient.getData().getQuiz().toString(), "Quiz field is null");
        assertNotNull(trackingPatient.getData().getReminders().toString(), "Reminders field is null");
        assertNotNull(trackingPatient.getData().getTracking().toString(), "Tracking field is null");
        assertNotNull(trackingPatient.getData().getVideo().toString(), "Video field is null");
    }

    @Test(dependsOnMethods = {"testGateWayPatientListResponse"})
    public void testGateWayPatientMetricResponse(){
        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI_APP_INTEGRATION)
                        .setBasePath(Path.API_V2));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT.concat(EndPoints.PATIENT_METRIC));
        restUtilities.createQueryParam(newReqSpec, "type", "weight");
        restUtilities.createQueryParam(newReqSpec, "criteria", "first");

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(
                        newReqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()),
                        "get");

        PatientMetric patientMetric = response.as(PatientMetric.class);
        try {
            assertNotNull(patientMetric.getId());
        } catch (AssertionError e) {
            System.out.println("Object response does not have metric id. Please check!");
        }
        try {
            assertNotNull(patientMetric.getPatient());
        } catch (AssertionError e) {
            System.out.println("Does not have patient!");
        }
        try {
            assertNotNull(patientMetric.getClinician());
        } catch (AssertionError e) {
            System.out.println("Does not have clinician!");
        }
        try {
            assertNotNull(patientMetric.getType());
        } catch (AssertionError e) {
            System.out.println("type field is null");
        }
        try {
            assertNotNull(patientMetric.getWeight());
        } catch (AssertionError e) {
            System.out.println("weight field is null");
        }
        try {
            assertNotNull(patientMetric.getReminder());
        } catch (AssertionError e) {
            System.out.println("reminder field is null");
        }
        try {
            assertNotNull(patientMetric.getFinished());
        } catch (AssertionError e) {
            System.out.println("finished field is null");
        }
        try {
            assertNotNull(patientMetric.getTime());
        } catch (AssertionError e) {
            System.out.println("time field is null");
        }
        try {
            assertNotNull(patientMetric.getTime().getDate());
        } catch (Exception e) {
            System.out.println("date field is null");
        }
        try {
            assertNotNull(patientMetric.getTime().getTimezoneType());
        } catch (NullPointerException e) {
            System.out.println("time zone type field is null");
        }
        try {
            assertNotNull(patientMetric.getTime().getTimezone());
        } catch (NullPointerException e) {
            System.out.println("timezone field is null");
        }
        try {
            assertNotNull(patientMetric.getStatus());
        } catch (AssertionError e) {
            System.out.println("status field is null");
        }
        try {
            assertNotNull(patientMetric.getLastUpdated());
        } catch (AssertionError e) {
            System.out.println("last updated field is null");
        }
        try {
            assertNotNull(patientMetric.getLastUpdated().getDate());
        } catch (NullPointerException e) {
            System.out.println("date field is null");
        }
        try {
            assertNotNull(patientMetric.getLastUpdated().getTimezone());
        } catch (NullPointerException e) {
            System.out.println("last update timezone field is null");
        }
        try {
            assertNotNull(patientMetric.getLastUpdated().getTimezoneType());
        } catch (NullPointerException e) {
            System.out.println("last update timezone type field is null");
        }
    }

    @Test(dependsOnMethods = {"testGateWayPatientListResponse"}, priority = 6)
    public void testGateWayPausePatient(){
        queryParam.put("patient", patientsList.get(1).get(0).getProfile().getHrsid());
        queryParam.put("note", "reason for pausing ".concat(restUtilities.getRandomString()));
        queryParam.put("edvisit", "true");
        queryParam.put("readmitted", "true");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.PAUSE));
        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id",patientsList.get(0).get(0).getProfile().getHrsid()), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));
        assertNotNull(path.get("noteid"));

        Profile patientProfile = mapper.convertValue(
                RestAssistant.getPatientById(patientsList.get(0).get(0).getProfile().getHrsid()), Profile.class);

        assertEquals(patientProfile.getStatus(), PatientStatuses.PAUSE, "Status not pause");
    }

    @Test(dependsOnMethods = {"testGateWayPatientListResponse", "testGateWayPausePatient"}, priority = 6)
    public void testGateWayDeactivatePatient(){
        queryParam.put("patient", patientsList.get(1).get(0).getProfile().getHrsid());
        queryParam.put("note", "reason for deactivate ".concat(restUtilities.getRandomString()));
        queryParam.put("edvisit", "true");
        queryParam.put("readmitted", "true");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.DEACTIVATE));
        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id",patientsList.get(0).get(0).getProfile().getHrsid()), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));
        assertNotNull(path.get("noteid"));

        Profile patientProfile = mapper.convertValue(
                RestAssistant.getPatientById(patientsList.get(0).get(0).getProfile().getHrsid()), Profile.class);

        assertEquals(patientProfile.getStatus(), PatientStatuses.DEACTIVATED, "Status not deactivated");
    }

    @Test(dependsOnMethods = {
            "testGateWayPatientListResponse",
            "testGateWayPausePatient",
            "testGateWayDeactivatePatient"}, priority = 6)
    public void testGateWayReactivatePatient(){
        queryParam.put("patient", patientsList.get(1).get(0).getProfile().getHrsid());
        queryParam.put("note", "reason for deactivate ".concat(restUtilities.getRandomString()));
        queryParam.put("edvisit", "true");
        queryParam.put("readmitted", "true");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.REACTIVATE));
        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id",patientsList.get(0).get(0).getProfile().getHrsid()), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "activated");
        assertNotNull(path.get("auth"));
        assertNotNull(path.get("noteid"));

        Profile patientProfile = mapper.convertValue(
                RestAssistant.getPatientById(patientsList.get(0).get(0).getProfile().getHrsid()), Profile.class);

        assertEquals(patientProfile.getStatus(), PatientStatuses.ACTIVATED, "Status not activated");
    }

    @Test(dependsOnMethods = {
            "testGateWayPatientListResponse",
            "testGateWayPausePatient",
            "testGateWayDeactivatePatient",
            "testGateWayReactivatePatient"}, priority = 6)
    public void testGateWayDischargePatient(){
        queryParam.put("patient", patientsList.get(1).get(0).getProfile().getHrsid());
        queryParam.put("date", restUtilities.getTimePeriod(10).get("plusDays"));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.DISCHARGE));
        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id",patientsList.get(0).get(0).getProfile().getHrsid()), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));

        Profile patientProfile = mapper.convertValue(
                RestAssistant.getPatientById(patientsList.get(0).get(0).getProfile().getHrsid()), Profile.class);

        assertEquals(patientProfile.getStatus(), PatientStatuses.DISCHARGED, "Status not discharge");
    }

    @Test(dependsOnMethods = {
            "testGateWayPatientListResponse",
            "testGateWayPausePatient",
            "testGateWayDeactivatePatient",
            "testGateWayReactivatePatient",
            "testGateWayDischargePatient"}, priority = 6)
    public void testGateWayRemoveDischargePatient(){
        queryParam.put("patient", patientsList.get(1).get(0).getProfile().getHrsid());
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.DISCHARGE));
        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id",patientsList.get(0).get(0).getProfile().getHrsid()), "delete");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));

        Profile patientProfile = mapper.convertValue(
                RestAssistant.getPatientById(patientsList.get(0).get(0).getProfile().getHrsid()), Profile.class);

        assertEquals(patientProfile.getStatus(), PatientStatuses.ACTIVATED, "Status not activated");
    }


    @Test(dependsOnMethods = {"testGateWayPatientListResponse"})
    public void testSetGateWayMetricsByPatientId() {
        NewMetricks newMetrick = new NewMetricks();
        ModuleInfo info = new ModuleInfo();

        List<String> modulesList = new ArrayList<>();
        modulesList.add(Modules.HELTHY);
        modulesList.add(Modules.CHF);
        modulesList.add(Modules.COPD);
        modulesList.add(Modules.DIABETES);
        modulesList.add(Modules.ACTIVITY);
        modulesList.add(Modules.WEIGHT);
        modulesList.add(Modules.TEMPERATURE);
        modulesList.add(Modules.BLOOD_PRESSURE);
        modulesList.add(Modules.MEDICATION);
        modulesList.add(Modules.SURVEY);
        modulesList.add(Modules.PULSE_OX);
        modulesList.add(Modules.GLUCOSE);
        modulesList.add(Modules.WOUND_IMAGING);
        modulesList.add(Modules.PATIENT_CONNECT_VOICE);
        modulesList.add(Modules.STETHOSCOPE);
        modulesList.add(Modules.STEPS);
        info.setModules(modulesList);
        info.setActivityreminder(new Activityreminder());
        info.setSurveyreminder(new Surveyreminder());
        info.setWeightreminder(new Weightreminder());

        List<Medicationreminder> medicationreminderList = new ArrayList<>();
        Medicationreminder medicationreminder = new Medicationreminder();
        medicationreminder.setMedication("test medication ".concat(restUtilities.getRandomString()));
        medicationreminder.setDose("1 mg x1 Oral");
        medicationreminder.setSchedule(new Schedule("true",
                "everyday",
                restUtilities.getTimePeriod(5).get("current"),
                "Test instruction: ".concat(restUtilities.getRandomString())));
        medicationreminderList.add(medicationreminder);
        info.setMedicationreminders(medicationreminderList);
        newMetrick.setModuleInfo(info);

        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.MODULE_INFO));
        reqSpec.body(newMetrick);

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id",patientsList.get(0).get(0).getProfile().getHrsid()), "post");

        MetricksResponse metricksResponse = response.as(MetricksResponse.class);
        assertEquals(metricksResponse.getStatus(), "ok", "status is not ok");
        assertNotNull(metricksResponse.getAuth(), "Auth field is null");
        assertNotNull(metricksResponse.getAuth().getToken(), "Token field is null");

        assertNotNull(metricksResponse.getInstalldata(), "installdata field is null");
        assertNotNull(metricksResponse.getInstalldata().getUpdatedisplay(), "Updatedisplay field is null");
        assertNotNull(metricksResponse.getInstalldata().getModules(), "Modules field is null");
        assertEquals(metricksResponse.getInstalldata().getModules(), newMetrick.getModuleInfo().getModules(), "modules not eql");

        assertNotNull(metricksResponse.getInstalldata().getActivityreminder(), "Activity reminder field is null");
        assertEquals(metricksResponse.getInstalldata().getActivityreminder().getTime()
                , newMetrick.getModuleInfo().getActivityreminder().getTime(), "activity reminder time not eql");
        assertEquals(metricksResponse.getInstalldata().getActivityreminder().getWindow()
                , newMetrick.getModuleInfo().getActivityreminder().getWindow(), "activity reminder window not eql");

        assertNotNull(metricksResponse.getInstalldata().getSurveyreminder(), "Surveyreminder field is null");
        assertEquals(metricksResponse.getInstalldata().getSurveyreminder().getTime()
                , newMetrick.getModuleInfo().getSurveyreminder().getTime(), "survey reminder time not eql");
        assertEquals(metricksResponse.getInstalldata().getSurveyreminder().getWindow()
                , newMetrick.getModuleInfo().getSurveyreminder().getWindow(), "survey reminder window not eql");

        assertNotNull(metricksResponse.getInstalldata().getTemperaturereminders(), "Temperaturereminders field is null");
        assertNotNull(metricksResponse.getInstalldata().getTemperaturereminders().getStatus(), "Temperaturereminders.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getTemperaturereminders().getStatus(), "active", "status field is not eql");

        assertNotNull(metricksResponse.getInstalldata().getWeightreminder(), "Weightreminder field is null");
        assertEquals(metricksResponse.getInstalldata().getWeightreminder().getTime()
                , newMetrick.getModuleInfo().getWeightreminder().getTime(), "weight reminder time not eql");
        assertEquals(metricksResponse.getInstalldata().getWeightreminder().getWindow()
                , newMetrick.getModuleInfo().getWeightreminder().getWindow(), "weight reminder window not eql");

        assertNotNull(metricksResponse.getInstalldata().getWoundimagingreminders(), "Woundimagingreminders field is null");
        assertNotNull(metricksResponse.getInstalldata().getWoundimagingreminders().getStatus(), "Woundimagingreminders.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getWoundimagingreminders().getStatus(), "active", "Woundimagingreminders status not active");

        assertNotNull(metricksResponse.getInstalldata().getMedicationreminders(), "Medicationreminders field is null");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getMedication()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getMedication(), "Medication field not eql");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getDose()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getDose(), "Dose field not eql");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getType()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getType(), "Schedule.Type field not eql");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getExpiration()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getExpiration(), "Schedule.Expiration field not eql");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getEssential()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getEssential(), "Schedule.Essential field not eql");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getInstruction()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getInstruction(), "Schedule.Instruction field not eql");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getTime()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getTime(), "Time field not eql");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getWindow()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getWindow(), "Window field not eql");

        assertNotNull(metricksResponse.getInstalldata().getGlucosereminder(), "Glucosereminder field is null");
        assertNotNull(metricksResponse.getInstalldata().getGlucosereminder().getStatus(), "Glucosereminder.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getGlucosereminder().getStatus(), "active", "glucose reminder status not active");

        assertNotNull(metricksResponse.getInstalldata().getPulseoxreminders(), "Pulseoxreminders field is null");
        assertNotNull(metricksResponse.getInstalldata().getPulseoxreminders().getStatus(), "Pulseoxreminders.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getPulseoxreminders().getStatus(), "active", "pulseOx status not active");

        assertNotNull(metricksResponse.getInstalldata().getBloodpressurereminders(), "Bloodpressurereminders field is null");
        assertNotNull(metricksResponse.getInstalldata().getBloodpressurereminders().getStatus(), "Bloodpressurereminders.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getBloodpressurereminders().getStatus(), "active", "Bloodpressure status not active");

        assertNotNull(metricksResponse.getInstalldata().getActivationhistory(), "Activationhistory field is null");
        assertNotNull(metricksResponse.getInstalldata().getActivationhistory().get(0).getStatus(), "Activationhistory.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getActivationhistory().get(0).getStatus(), "preactivate", "activation history status not preactivate");

    }

    @Test(dependsOnMethods = {"testGateWayPatientListResponse"})
    public void testGetGateWayPatientSurveyDetailsForToday() {
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(
                EndPoints.PATIENTS.concat(
                        EndPoints.ID).concat(
                                EndPoints.SYRVEY_DETAILS).concat(
                                        EndPoints.DATE));

        restUtilities.createPathParam(reqSpec, "date", restUtilities.getTimePeriod(2).get("current"));
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "get");

        com.hrs.cc.api.models.patients.survey.Survey survey = response.as(com.hrs.cc.api.models.patients.survey.Survey.class);
        assertNotNull(survey.getGroups());
        survey.getGroups().forEach(group -> {
            assertNotNull(group.getName(), "group.getName field is null");
            assertNotNull(group.getQuestionids(), "group.getQuestionids field is null");
            assertNotNull(group.getScoretype(), "group.getScoretype field is null");
            assertNotNull(group.getScoredisplay(), "group.getScoredisplay field is null");
            group.getScoredisplay().forEach(scoreDisplay -> {
                assertNotNull(scoreDisplay.getMax(), "scoreDisplay.getMax field is null");
                assertNotNull(scoreDisplay.getMin(), "scoreDisplay.getMin field is null");
                assertNotNull(scoreDisplay.getText(), "scoreDisplay.getText field is null");
            });
        });
        assertNotNull(survey.getQuestions(), "survey.getQuestions field is null");
        survey.getQuestions().forEach(question -> {
            assertNotNull(question.getSchedule(), "question.getSchedule field is null");
            assertNotNull(question.getSchedule().getType(), "question.getSchedule().getType field is null");
            assertNotNull(question.getMedalert(), "question.getMedalert field is null");
            assertNotNull(question.getHighalert(), "question.getHighalert field is null");
            assertNotNull(question.getCategory(), "question.getCategory field is null");
            assertNotNull(question.getQuestion(), "question.getQuestion field is null");
            assertNotNull(question.getId(), "question.getId field is null");
            assertNotNull(question.getAnswertype(), "question.getAnswertype field is null");
        });

    }
}
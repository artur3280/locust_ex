package com.hrs.cc.api.tests.account;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.account_data.Account;
import com.hrs.cc.api.models.integratioins_app.patient_status.PatientState;
import com.hrs.cc.api.models.patients.syrvey.PatientSurveys;
import core.PropertyWorker;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class testAccountData extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private ObjectMapper mapper = new ObjectMapper();
    private static Account accountData;
    private static Map<java.lang.String, java.lang.String> queryParam = new HashMap<>();
    private PropertyWorker propertyWorker;
    private String moderatorId;
    private JsonPath pathV1;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2);
        Logger.debugging(false);
        resPec = restUtilities.getResponseSpecification();
    }


    @BeforeMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        reqSpec.body("");
    }

    @Test
    public void testAccountUserResponse() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtilities.getResponse(reqSpec, "get");
        accountData = response.as(Account.class);
        assertFalse(accountData.getUsertype().isEmpty(), "User type is empty");
        moderatorId = accountData.getUser();
        assertFalse(accountData.getUser().isEmpty(), "User field is empty");
        assertFalse(accountData.getToken().isEmpty(), "Token field is empty");
        assertFalse(accountData.getEnv().isEmpty(), "Env field is empty");
        assertFalse(accountData.getRoles().isEmpty(), "Roles field is empty");
        assertTrue(accountData.getUsername().isEmpty(), "User name field is empty");
        assertFalse(accountData.getFirstname().isEmpty(), "First name is empty");
        assertFalse(accountData.getLastname().isEmpty(), "Last name is empty");
        assertFalse(accountData.getExp().toString().isEmpty(), "Exp field is empty");
        assertFalse(accountData.getTimeZone().isEmpty(), "Timezone field is empty");
        try {
            assertFalse(accountData.getFirebase().toString().isEmpty(), "Firebase obj is empty");
            assertFalse(accountData.getFirebase().getAuthentication().isEmpty(), "Authentication field is empty");
            assertFalse(accountData.getFirebase().getApplication().toString().isEmpty(), "Application field is empty");
            assertFalse(accountData.getFirebase().getApplication().getApiKey().isEmpty(), "ApiKey field is empty");
            assertFalse(accountData.getFirebase().getApplication().getAuthDomain().isEmpty(), "AuthDomain field is empty");
            assertFalse(accountData.getFirebase().getApplication().getDatabaseURL().isEmpty(), "DatabaseURL field is empty");
            assertFalse(accountData.getFirebase().getApplication().getProjectId().isEmpty(), "ProjectId field is empty");
            assertFalse(accountData.getFirebase().getApplication().getStorageBucket().isEmpty(), "StorageBucket field is empty");
            assertFalse(accountData.getFirebase().getApplication().getMessagingSenderId().isEmpty(), "MessagingSenderId field is empty");
        } catch (AssertionError e) {
            System.out.println("Account does not have firebase field!");
        }

        assertEquals(accountData.getUsertype(), "clinician", "User type does not match expected");
        assertEquals(accountData.getUser(), "XM6WgC58jhoJrxhkAykH", "User id does not match expected");
        assertFalse(accountData.getToken().isEmpty(), "Token field is empty");
        assertEquals(accountData.getEnv(), "QATestingZone", "Env does not match expected");
        assertEquals(accountData.getRoles(), Arrays.asList("ROLE_API",
                "ROLE_CLINICIAN_RO",
                "ROLE_CLINICIAN",
                "ROLE_CLINICIAN_MODERATOR"), "Some roles does not match expected");

        if (!accountData.getFirstname().equals("")) {
            assertEquals(accountData.getFirstname(), Auth.FIRST_NAME, "First name does not match expected");
        }
        if (!accountData.getLastname().equals("")) {
            assertEquals(accountData.getLastname(), Auth.LAST_NAME, "Last name does not match expected");
        }
        assertEquals(accountData.getTimeZone(), Auth.TIME_ZONE, "Timezone does not match expected");
    }

    @Test(dependsOnMethods = {"testAccountUserResponse"}, priority = 2)
    public void testV1GetUserInformationById() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V1);

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));

        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.pathParam("id", moderatorId);
        Response response = restUtilities.getResponse(reqSpec, "get");
        pathV1 = response.jsonPath();
        assertNotNull(pathV1.get("data.id"), "Id field is null");
        assertNotNull(pathV1.get("data.resourceType"), "resourceType field is null");
        assertNotNull(pathV1.get("data.attributes"), "attributes field is null");
        assertNotNull(pathV1.get("data.attributes.type"), "type field is null");
        assertNotNull(pathV1.get("data.attributes.environment"), "environment field is null");
        assertNotNull(pathV1.get("data.attributes.profile"), "profile field is null");
    }

    @Test(dependsOnMethods = {"testAccountUserResponse"}, priority = 2)
    public void testGetUserInformationById() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));

        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.pathParam("id", moderatorId);
        Response response = restUtilities.getResponse(reqSpec, "get");
        JsonPath jsonPath = response.jsonPath();
        assertNotNull(jsonPath.get("data.id"), "Id field is empty");
        assertNotNull(jsonPath.get("data.type"), "Type field is empty");
        assertEquals(jsonPath.get("data.type"), "clinician", "Type does not match expected");
        assertNotNull(jsonPath.get("data.environment"), "environment field is empty");
    }


    @Test(priority = 1)
    public void testUserPasswordResets() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.RESET_PASSWORD);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, Object> reqData = new HashMap<>();
        Map<String, String> data = new HashMap<>();
        data.put("username", "test user name".concat(restUtilities.getRandomString(6)));
        data.put("email", restUtilities.getRandomString(6).concat("@gmail.com"));
        reqData.put("data", data);

        reqSpec.body(reqData);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);

    }

    @Test(dependsOnMethods = {"testUserPasswordResets"}, priority = 1)
    public void testNegativeUserPasswordResets() {

        ResponseSpecification responseSpecification = restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(500)
        );


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.RESET_PASSWORD);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, Object> reqData = new HashMap<>();
        Map<String, String> data = new HashMap<>();
        data.put("username", "");
        data.put("email", "");
        reqData.put("data", data);

        reqSpec.body(reqData);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 500);
        JsonPath jsonPath = response.jsonPath();
        assertEquals(jsonPath.get("error"), "Failed to provide a username and password", "The error has been modified.");
    }
}

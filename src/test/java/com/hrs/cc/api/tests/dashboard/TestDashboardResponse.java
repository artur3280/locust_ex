package com.hrs.cc.api.tests.dashboard;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.environment.Environment;
import com.hrs.cc.api.models.patients.dashboard_list.DashboardPatientList;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.PropertyWorker;
import core.rest.RestUtilities;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class TestDashboardResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private ObjectMapper mapper = new ObjectMapper();
    private static Environment environment;
    private static Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private PropertyWorker propertyWorker;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.DASHBOARD));
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
    }


    @BeforeMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec,queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
        reqSpec.body("");
    }

    @Test
    public void testDashboardResponse() {
        LocalDate localDate = LocalDate.now();
        restUtilities.setContentType(ContentType.JSON);
        queryParam.put("y", String.valueOf(localDate.getYear()));
        queryParam.put("m", String.valueOf(localDate.getMonthValue()));
        queryParam.put("d", String.valueOf(localDate.getDayOfMonth()));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createQueryParam(reqSpec, queryParam), "get");
        DashboardPatientList patientList = response.as(DashboardPatientList.class);

        assertNotNull(patientList.getPatientlist(), "Patient list field is null");

        for (int i = 0; i < 10; i++) {
            assertNotNull(patientList.getPatientlist().get(i).getDate(), "getDate filed is null");
            assertNotNull(patientList.getPatientlist().get(i).getDays(), "getDays filed is null");
            assertNotNull(patientList.getPatientlist().get(i).getHrsid(), "getHrsid filed is null");
            assertNotNull(patientList.getPatientlist().get(i).getPid(), "getPid filed is null");
            assertNotNull(patientList.getPatientlist().get(i).getStatus(), "getStatus filed is null");
            try {
                assertNotNull(patientList.getPatientlist().get(i).getConditions(), "assertNotNull(patientList.getPatientlist().get(i).getConditions filed is null");
            }catch (AssertionError error){
                System.out.println("Patient "+ patientList.getPatientlist().get(i).getPid()+" does not have any conditions!");
            }
            assertNotNull(patientList.getPatientlist().get(i).getSubgroup(), "getSubgroup filed is null");
            assertNotNull(patientList.getPatientlist().get(i).getConnectedclinician(), "getConnectedclinician filed is null");
            assertNotNull(patientList.getPatientlist().get(i).getConnectedclinician().getName(), "getConnectedclinician().getName filed is null");
            assertNotNull(patientList.getPatientlist().get(i).getConnectedclinician().getId(), "getConnectedclinician().getId filed is null");
            assertNotNull(patientList.getPatientlist().get(i).getFname(), "getFname filed is null");
            assertNotNull(patientList.getPatientlist().get(i).getLname(), "getLname filed is null");

            patientList.getPatientlist().get(i).getReviewedlist().forEach((review) -> {
                assertNotNull(review.getId(), "getId filed is null");
                assertNotNull(review.getName(), "getName filed is null");
                assertNotNull(review.getGroup(), "getGroup filed is null");
                assertNotNull(review.getTime(), "getTime filed is null");
            });

            assertNotNull(patientList.getPatientlist().get(i).getRisk(), "getRisk filed is null");
            assertNotNull(patientList.getPatientlist().get(i).getHighrisktoday(), "getHighrisktoday filed is null");
            assertNotNull(patientList.getPatientlist().get(i).getHighriskyesterday(), "getHighriskyesterday filed is null");

            assertNotNull(patientList. getPatientlist().get(i).getSteps().getActive(), "getActive filed is null");
            assertNotNull(patientList. getPatientlist().get(i).getSteps().getTs(), "getTs filed is null");
            assertNotNull(patientList. getPatientlist().get(i).getSteps().getCount(), "getCount filed is null");
            try {
//
//                if (patientList.getPatientlist().get(i).getRiskDetails()!=null) {
//                    patientList.getPatientlist().get(i).getRiskDetails().getDetails().forEach((detail -> {
//                        assertNotNull(detail.getType());
//                        assertNotNull(detail.getText());
//                        assertNotNull(detail.getMetrics());
//                        assertNotNull(detail.getMetricIds());
//                    }));
//                }
//
//                if (patientList.getPatientlist().get(i).getRiskDetails()!=null) {
//                    assertNotNull(patientList.getPatientlist().get(i).getRiskDetails().getModules());
//                }
                assertNotNull(patientList.getPatientlist().get(i).getActivity().getDuration(), "getDuration field is null");
                assertNotNull(patientList.getPatientlist().get(i).getActivity(), "getActivity field is null");
                assertNotNull(patientList.getPatientlist().get(i).getActivity().getActive(), "getActive field is null");
                assertNotNull(patientList.getPatientlist().get(i).getActivity().getTs(), "getTs field is null");
                assertNotNull(patientList.getPatientlist().get(i).getBp().getActive(), "getActive field is null");
                assertNotNull(patientList.getPatientlist().get(i).getBp(), "getBp field is null");
                assertNotNull(patientList.getPatientlist().get(i).getBp().getSys(), "getSys field is null");
                assertNotNull(patientList.getPatientlist().get(i).getBp().getDia(), "getDia field is null");
                assertNotNull(patientList.getPatientlist().get(i).getBp().getHr(), "getHr field is null");
                assertNotNull(patientList.getPatientlist().get(i).getBp().getTs(), "getTs field is null");
                assertNotNull(patientList.getPatientlist().get(i).getWeight(), "getWeight field is null");
                assertNotNull(patientList.getPatientlist().get(i).getWeight().getActive(), "getWeight().getActive field is null");
                assertNotNull(patientList.getPatientlist().get(i).getWeight().getWeight(), "getWeight().getWeight field is null");
                assertNotNull(patientList.getPatientlist().get(i).getWeight().getTs(), "getWeight().getTs field is null");

                assertNotNull(patientList.getPatientlist().get(i).getMeds(), "getMeds field is null");
                assertNotNull(patientList.getPatientlist().get(i).getMeds().getActive(), "getMeds().getActive field is null");


                assertNotNull(patientList.getPatientlist().get(i).getGlucose(), "getGlucose field is null");
                assertNotNull(patientList.getPatientlist().get(i).getGlucose().getActive(), "getGlucose().getActive field is null");

                assertNotNull(patientList.getPatientlist().get(i).getPulseox(), "getPulseox field is null");
                assertNotNull(patientList.getPatientlist().get(i).getPulseox().getActive(), "getPulseox().getActive field is null");

                assertNotNull(patientList.getPatientlist().get(i).getSurvey(), "getSurvey field is null");
                assertNotNull(patientList.getPatientlist().get(i).getSurvey().getActive(), "getSurvey().getActive field is null");


                assertNotNull(patientList.getPatientlist().get(i).getTemperature(), "getTemperature field is null");
                assertNotNull(patientList.getPatientlist().get(i).getTemperature().getActive(), "getTemperature().getActive field is null");

                assertNotNull(patientList.getPatientlist().get(i).getWoundimaging(), "getWoundimaging field is null");
                assertNotNull(patientList.getPatientlist().get(i).getWoundimaging().getActive(), "getWoundimaging().getActive field is null");
            } catch (AssertionError e) {
                System.out.println("Field is empty!");
            }
        }
        queryParam.clear();
    }
}

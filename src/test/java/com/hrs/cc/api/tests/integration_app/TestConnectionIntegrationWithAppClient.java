package com.hrs.cc.api.tests.integration_app;

import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.integratioins_app.clinician_auth_token.ClinicianAuthToken;
import com.hrs.cc.api.models.requests.clinician_auth_integration.ClinicianData;
import com.hrs.cc.api.models.requests.clinician_auth_integration.Data;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class TestConnectionIntegrationWithAppClient extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.TOKENS_INTEGRATION);
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
        reqSpec.body("");
    }

    @Test
    public void testLoginClinicianOnApp() {
        ClinicianData clinicianData = new ClinicianData();
        Data data = new Data();
        data.setUsername(Auth.USER_NAME);
        data.setPassword(Auth.PASSWORD);
        data.setType(Auth.TYPE);
        clinicianData.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.body(clinicianData);

        Response response = restUtilities.getResponse(reqSpec, "post");
        ClinicianAuthToken authToken = response.as(ClinicianAuthToken.class);

        assertNotNull(authToken.getData(), "Data field is null");
        assertNotNull(authToken.getData().getToken(), "data.token field is null");
        assertNotNull(authToken.getData().getRefresh(), "data.refresh field is null");
        assertFalse(authToken.getData().getToken().isEmpty(), "data.token field is empty");
        assertFalse(authToken.getData().getRefresh().isEmpty(), "data.refresh field is empty");
    }

    @Test
    public void testNegativeLoginClinicianOnApp() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(422)
        );

        ClinicianData clinicianData = new ClinicianData();
        Data data = new Data();
        data.setUsername(Auth.USER_NAME.concat(restUtilities.getRandomString(4)));
        data.setPassword(Auth.PASSWORD.concat(restUtilities.getRandomString(4)));
        data.setType(Auth.TYPE);
        clinicianData.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.body(clinicianData);

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath jsonPath = response.jsonPath();
        assertEquals(jsonPath.get("message"), "user not found" ,"Messages not eqls");

    }
}

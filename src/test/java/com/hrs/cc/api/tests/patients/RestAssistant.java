package com.hrs.cc.api.tests.patients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.EnvFlags;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.environment.settings.EnvSettings;
import com.hrs.cc.api.models.environment.settings.Setting;
import com.hrs.cc.api.models.integratioins_app.user_by_type_in_env.UsersByTypeInEnv;
import com.hrs.cc.api.models.inventory.InventoryResponse;
import com.hrs.cc.api.models.patients.Patients;
import com.hrs.cc.api.models.patients.education.Education;
import com.hrs.cc.api.models.patients.patient_list.PatientList;
import com.hrs.cc.api.models.patients.patient_notes.PatientNote;

import com.hrs.cc.api.models.patients.profile.Profile;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import core.rest.auth.schemes.AuthScheme;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class RestAssistant {
    private static RequestSpecification reqSpec;
    private static ResponseSpecification resPec;
    private static RestUtilities restUtilities;
    private static Map<String, String> userData;
    public static String newPatientId;
    private static final String TOKEN_PATH = "/token.json";
    private static PropertyWorker propertyWorker;
    private static EnvSettings settings;

    static String getPatientByName(String firstName, String lastName) {
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        try {
            if (propertyWorker.getProperty("patient_hrs_id") == null) throw new NullPointerException("id is null");
            return propertyWorker.getProperty("patient_hrs_id");

        } catch (NullPointerException e) {
            restUtilities = new RestUtilities();

            restUtilities.setBaseUri(Path.BASE_URI);
            reqSpec = restUtilities.getRequestSpecification();
            reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

            resPec = restUtilities.getResponseSpecification();

            restUtilities.setContentType(ContentType.JSON);
            restUtilities.setEndPoint();
            reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
            restUtilities.createQueryParams(reqSpec, "status[]",
                    "Activated",
                    "Pre-Activated",
                    "Paused",
                    "Deactivated",
                    "Discharged");
            Response response = restUtilities.getResponse(reqSpec, "get");
            Patients patients = response.as(Patients.class);


            PatientList p = patients.getPatientlist().stream().filter(
                    patient -> patient.get(0).getProfile().getName().getFirst().equals(firstName) &&
                            patient.get(0).getProfile().getName().getLast().equals(lastName)).collect(Collectors.toList()).get(0).get(0);
            try {
                propertyWorker.setProperty("patient_hrs_id", p.getProfile().getHrsid());
            } catch (IOException ignored) {
            }
            return p.getProfile().getHrsid();
        }
    }

    static void setTimeZone(String patientId, String timezone) {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENT));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, String> body = new HashMap<>();
        body.put("time_zone", timezone);
        reqSpec.body(body);

        restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
    }

    static Profile getPatientById(String id) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
        Map<String, String> timePeriod = RandomData.getTimePeriod(20, formatter);
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        ResponseSpecification resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createPathParam(reqSpec, "id", id);
        restUtilities.createQueryParam(reqSpec, "start", timePeriod.get("minusDays"));
        restUtilities.createQueryParam(reqSpec, "end", timePeriod.get("current"));

        Response response = restUtilities.getResponse(reqSpec, "get");
        ArrayList profile = response.jsonPath().get("profile");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(profile.get(0), Profile.class);
    }

    static List<PatientNote> getGetNotesByPatientId(String patientId) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.NOTES));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT + EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Response response = restUtilities.getResponse(restUtilities.createPathParam(reqSpec, "id", patientId), "get");
        return Arrays.asList(response.as(PatientNote[].class));
    }

    static Integer tsToSec8601(String timestamp) {
        if (timestamp == null) return null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            Date dt = sdf.parse(timestamp);
            long epoch = dt.getTime();
            return (int) (epoch / 1000);
        } catch (ParseException e) {
            return null;
        }
    }

    static String getPatientId() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createQueryParam(reqSpec, "status[]", "Activated");
        restUtilities.createQueryParam(reqSpec, "status[]", "Pre-Activated");
        Response response = restUtilities.getResponse(reqSpec, "get");
        Patients patients = response.as(Patients.class);


        String p = patients.getPatientlist().stream().filter(
                patient -> (patient.get(0).getProfile().getName().getFirst().contains("test") ||
                        patient.get(0).getProfile().getName().getLast().contains("test")) &&
                        patient.get(0).getProfile().getClinician() != null &&
                        patient.get(0).getProfile().getClinician().getName().equals(Auth.FIRST_NAME + " " + Auth.LAST_NAME) &&
                        patient.get(0).getDay() > 5).collect(Collectors.toList()).get(0).get(0).getProfile().getHrsid();


        return p;
    }

    static InventoryResponse getDeviceId() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.INVENTORY));

        resPec = restUtilities.getResponseSpecification();


        restUtilities.setContentType(ContentType.JSON);

        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response devices = restUtilities.getResponse(reqSpec, "get");
        List<InventoryResponse> inventoryList = Arrays.asList(devices.as(InventoryResponse[].class));
        return inventoryList.stream().filter(i ->
                !i.getName().equals("HRSTAB10498") &&
                        !i.getId().equals("HRSTAB10498")).collect(Collectors.toList()).get(0);


    }

    static void setTestSettingToPatientInEnvAdmin(String patientId) {
        RestUtilities restUtilities = new RestUtilities();
        Response responseAuth = Connection.adminAuthGeneral();
        Map<String, String> cookies = responseAuth.getCookies();

        restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.API_ADMIN_V2);

        Map<String, Object> data = new HashMap<>();
        data.put("request", "addpatientsetting");
        data.put("env", "QATestingZone");
        data.put("hrsid", patientId);
        data.put("setting", EnvFlags.STATUS_TEST_PATIENT.get());
        data.put("value", true);


        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        reqSpec.header("authorization", AuthScheme.basicScheme("admin", "LaCroix").generateAuthToken());
        reqSpec.cookies(cookies);
        restUtilities.setEndPoint(EndPoints.DATA);
        reqSpec.formParams(data);

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200)
                /*.registerParser("text/html", Parser.JSON)*/);

        Response response = restUtilities.getResponse(reqSpec, "post");
        System.out.println(response.statusCode());
    }

    static Education getEducationByPatientId(String patientId) {
        RestUtilities restUtilities = new RestUtilities();
        Response responseAuth = Connection.adminAuthGeneral();

        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.API_V2);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.EDUCATION));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "get");
        return response.as(Education.class);

    }

    static void setPcVoiceLicenseMax(Integer max) {
        RestUtilities restUtilities = new RestUtilities();
        Response responseAuth = Connection.adminAuthGeneral();
        Map<String, String> cookies = responseAuth.getCookies();

        restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
//            reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.API_ADMIN_V2);

        Map<String, Object> data = new HashMap<>();
        data.put("request", "addenvironmentsetting");
        data.put("environment", "QATestingZone");
        data.put("setting", EnvFlags.SYSTEM_PATIENTCONNECTVOICE_LICENSE_MAX.get());
        data.put("value", max);

        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        reqSpec.header("authorization", AuthScheme.basicScheme("admin", "LaCroix").generateAuthToken());
        reqSpec.cookies(cookies);
        restUtilities.setEndPoint(EndPoints.DATA);
        reqSpec.formParams(data);

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200)
                /*.registerParser("text/html", Parser.JSON)*/);

        Response response = restUtilities.getResponse(reqSpec, "post");

    }

    static void removeEnvSettings(String name) {
        getCurrentSettingsInEnv();
        List<Setting> idFlag = settings.getSettings().stream().filter(setting -> setting.getFlag().equals(name)).collect(Collectors.toList());

        for (Setting setting : idFlag) {
            RestUtilities restUtilities = new RestUtilities();
            Response responseAuth = Connection.adminAuthGeneral();
            Map<String, String> cookies = responseAuth.getCookies();

            restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
            restUtilities.setContentType(ContentType.JSON);
            reqSpec = restUtilities.getRequestSpecification();
//            reqSpec.log().all();
            resPec = restUtilities.getResponseSpecification();
            reqSpec.basePath(Path.API_ADMIN_V2);

            Map<String, Object> data = new HashMap<>();
            data.put("request", "removeenvironmentsetting");
            data.put("environment", "QATestingZone");
            data.put("id", setting.getId());

            restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
            reqSpec.header("authorization", AuthScheme.basicScheme("admin", "LaCroix").generateAuthToken());
            reqSpec.cookies(cookies);
            restUtilities.setEndPoint(EndPoints.DATA);
            reqSpec.formParams(data);

            restUtilities.getResponseSpecification(
                    restUtilities.createResponseBuilder()
                            .expectStatusCode(200)
                    /*.registerParser("text/html", Parser.JSON)*/);

            Response response = restUtilities.getResponse(reqSpec, "post");
        }
    }

    static Boolean getCurrentSettingsInEnv() {
        ObjectMapper mapper = new ObjectMapper();
        RestUtilities restUtilities = new RestUtilities();
        Response responseAuth = Connection.adminAuthGeneral();
        Map<String, String> cookies = responseAuth.getCookies();

        restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.API_ADMIN_V2);

        Map<String, Object> data = new HashMap<>();
        data.put("request", "getenvironmentsettings");
        data.put("environment", "QATestingZone");

        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        reqSpec.header("authorization", AuthScheme.basicScheme("admin", "LaCroix").generateAuthToken());
        reqSpec.cookies(cookies);
        restUtilities.setEndPoint(EndPoints.DATA);
        reqSpec.formParams(data);

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200)
                /*.registerParser("text/html", Parser.JSON)*/);

        Response response = restUtilities.getResponse(reqSpec, "post");
        settings = null;
        try {
            settings = mapper.readValue(response.getBody().asString(), EnvSettings.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            settings.getSettings().stream().filter(setting ->
                    setting.getFlag().equals(EnvFlags.SYSTEM_PATIENTCONNECTVOICE.get())).findFirst().get();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    static UsersByTypeInEnv getEnabledClinicians() {
        RestUtilities restUtilities = new RestUtilities();
        PropertyWorker propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");
//        reqSpec.log().all();

        ResponseSpecification resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        restUtilities.createQueryParam(reqSpec, "filter[type]", "clinician");
        restUtilities.createQueryParam(reqSpec, "filter[environment]", "QATestingZone");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");

        return response.as(UsersByTypeInEnv.class);
    }

}

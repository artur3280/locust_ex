package com.hrs.cc.api.tests.clinician;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.account_data.clinician.Clinician;
import com.hrs.cc.api.models.account_data.clinician.filters.Column;
import com.hrs.cc.api.models.requests.filtering_patient_list.FilterRequest;
import com.hrs.cc.api.models.requests.filtering_patient_list.filtering.*;
import core.PropertyWorker;
import core.rest.RestUtilities;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestClinicianResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private static Clinician clinicianData;
    private static Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private PropertyWorker propertyWorker;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.CLINICIAN));
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
    }

    @BeforeMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
        reqSpec.body("");
    }

    @Test
    public void testClinicianSettingsResponse() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
        clinicianData = response.as(Clinician.class);

        /*ObjectMapper mapper = new ObjectMapper();
        Column risk = clinicianData.getFilterState().getColumns().stream().filter(c -> c.getName().equals("risk")).findFirst().get();
        Filter_Datum myObjects = mapper.convertValue(risk.getFilters().get(0), Filter_Datum.class);*/

        assertNotNull(clinicianData.getFirstName(), "First name field is null");
        assertNotNull(clinicianData.getLastName(), "Last name field is null");
        assertNotNull(clinicianData.getEmail(), "Email field is null");
        assertNotNull(clinicianData.getTelephone(), "Telephone field is null");
        assertNotNull(clinicianData.getRiskEmail(), "RiskEmailfield is null");
        assertNotNull(clinicianData.getRiskText(), "RiskText field is null");
        assertNotNull(clinicianData.getTimeZone(), "Timezone field is null");
        assertNotNull(clinicianData.getSessionTimeout(), "SessionTimeout field is null");
        assertNotNull(clinicianData.getFilterLastName(), "FilterLastName field is null");
        assertNotNull(clinicianData.getFilterFirstName(), "FilterFirstName field is null");
        assertNotNull(clinicianData.getFilterPatientId(), "FilterPatientId field is null");
        assertNotNull(clinicianData.getFilterTablet(), "FilterTablet field is null");
        assertNotNull(clinicianData.getFilterEnrolled(), "FilterEnrolled field is null");
        assertNotNull(clinicianData.getFilterStatus(), "FilterStatus field is null");
        assertNotNull(clinicianData.getFilterRisk(), "FilterRisk field is null");
        assertNotNull(clinicianData.getFilterReviewed(), "FilterReviewed field is null");
        assertNotNull(clinicianData.getFilterSubgroup(), "FilterSubgroup field is null");
        assertNotNull(clinicianData.getFilterClinician(), "FilterClinician field is null");
        assertNotNull(clinicianData.getFilterConditions(), "FilterConditions field is null");
        assertNotNull(clinicianData.getFilterMedications(), "FilterMedications field is null");
        assertNotNull(clinicianData.getFilterActivity(), "FilterActivity field is null");
        assertNotNull(clinicianData.getFilterBloodPressure(), "FilterBloodPressure field is null");
        assertNotNull(clinicianData.getFilterPulseOx(), "FilterPulseOx field is null");
        assertNotNull(clinicianData.getFilterSurvey(), "FilterSurvey field is null");
        assertNotNull(clinicianData.getFilterTemperature(), "FilterTemperature field is null");
        assertNotNull(clinicianData.getFilterWeight(), "FilterWeight field is null");
        assertNotNull(clinicianData.getFilterGlucose(), "FilterGlucose field is null");
        assertNotNull(clinicianData.getFilterImaging(), "FilterImaging field is null");
        assertNotNull(clinicianData.getTelephone(), "Telephone field is null");

        if (clinicianData.getFilterState() != null) {
            assertNotNull(clinicianData.getFilterState().getColumns(), "Filter state field is null");

            for (Column column : clinicianData.getFilterState().getColumns()) {
                assertNotNull(column.getName(), "Name field is null");
                assertNotNull(column.getVisible(), "Visible field is null");
            }

            assertNotNull(clinicianData.getFilterState().getGrouping(), "Grouping field  is null");
            assertNotNull(clinicianData.getFilterState().getPagination(), "Pagination field  is null");
            assertNotNull(clinicianData.getFilterState().getScrollFocus(), "ScrollFocus field  is null");
            assertNotNull(clinicianData.getFilterState().getSelection(), "Selection field  is null");
            assertNotNull(clinicianData.getFilterState().getTreeView(), "TreeView field  is null");

        } else {
            throw new NullPointerException("Field filterState is null");
        }

        assertEquals(clinicianData.getFirstName(), Auth.FIRST_NAME, "FirstName does not match expected");
        assertEquals(clinicianData.getLastName(), Auth.LAST_NAME, "LastName does not match expected");
        assertEquals(clinicianData.getMiddleName(), Auth.MIDDLE_NAME, "MiddleName does not match expected");
        assertEquals(clinicianData.getEmail(), Auth.EMAIL, "Email does not match expected");
        assertEquals(clinicianData.getTimeZone(), Auth.TIME_ZONE, "TimeZone does not match expected");
        assertFalse(clinicianData.getRiskEmail(), "RiskEmail field  is true");
        assertFalse(clinicianData.getRiskText(), "RiskText field  is true");
    }

    @Test
    public void testShowOnlyHighRiskPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(4);
        status.setSearch(statusList);


        FilterRisk risk = new FilterRisk();
        List<Integer> riskList = new ArrayList<>();
        riskList.add(1);
        risk.setSearch(riskList);

        FilterClinician clinician = new FilterClinician();
        List<String> cliniciansList = new ArrayList<>();
        cliniciansList.add("Angelo Popper");


        request.setFilterRisk(risk);
        request.setFilterStatus(status);
        request.setFilterClinician(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyMediumRiskPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(4);
        status.setSearch(statusList);


        FilterRisk risk = new FilterRisk();
        List<Integer> riskList = new ArrayList<>();
        riskList.add(2);
        risk.setSearch(riskList);

        FilterClinician clinician = new FilterClinician();
        List<String> cliniciansList = new ArrayList<>();
        cliniciansList.add("Angelo Popper");


        request.setFilterRisk(risk);
        request.setFilterStatus(status);
        request.setFilterClinician(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");

    }

    @Test
    public void testShowOnlyLowRiskPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(4);
        status.setSearch(statusList);


        FilterRisk risk = new FilterRisk();
        List<Integer> riskList = new ArrayList<>();
        riskList.add(3);
        risk.setSearch(riskList);

        FilterClinician clinician = new FilterClinician();
        List<String> cliniciansList = new ArrayList<>();
        cliniciansList.add("Angelo Popper");


        request.setFilterRisk(risk);
        request.setFilterStatus(status);
        request.setFilterClinician(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyAllRisksPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(4);
        status.setSearch(statusList);


        FilterRisk risk = new FilterRisk();
        List<Integer> riskList = new ArrayList<>();
        riskList.add(1);
        riskList.add(2);
        riskList.add(3);
        risk.setSearch(riskList);

        FilterClinician clinician = new FilterClinician();
        List<String> cliniciansList = new ArrayList<>();
        cliniciansList.add("Angelo Popper");


        request.setFilterRisk(risk);
        request.setFilterStatus(status);
        request.setFilterClinician(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnly0_15DaysEnrolledPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(4);
        status.setSearch(statusList);

        FilterEnrolled enrolled = new FilterEnrolled();
        List<String> enrolledList = new ArrayList<>();
        enrolledList.add("^([0-9]|1[0-5])$");
        enrolled.setSearch(enrolledList);

        FilterClinician clinician = new FilterClinician();
        List<String> cliniciansList = new ArrayList<>();
        cliniciansList.add("Angelo Popper");

        request.setFilterEnrolled(enrolled);
        request.setFilterStatus(status);
        request.setFilterClinician(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnly16_30DaysEnrolledPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(4);
        status.setSearch(statusList);

        FilterEnrolled enrolled = new FilterEnrolled();
        List<String> enrolledList = new ArrayList<>();
        enrolledList.add("^(1[6-9]|2[0-9]|30)$");
        enrolled.setSearch(enrolledList);

        FilterClinician clinician = new FilterClinician();
        List<String> cliniciansList = new ArrayList<>();
        cliniciansList.add("Angelo Popper");

        request.setFilterEnrolled(enrolled);
        request.setFilterStatus(status);
        request.setFilterClinician(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnly31_50DaysEnrolledPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(4);
        status.setSearch(statusList);

        FilterEnrolled enrolled = new FilterEnrolled();
        List<String> enrolledList = new ArrayList<>();
        enrolledList.add("^(3[1-9]|4[0-9]|50)$");
        enrolled.setSearch(enrolledList);

        FilterClinician clinician = new FilterClinician();
        List<String> cliniciansList = new ArrayList<>();
        cliniciansList.add("Angelo Popper");

        request.setFilterEnrolled(enrolled);
        request.setFilterStatus(status);
        request.setFilterClinician(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyMore50DaysEnrolledPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(4);
        status.setSearch(statusList);

        FilterEnrolled enrolled = new FilterEnrolled();
        List<String> enrolledList = new ArrayList<>();
        enrolledList.add("^(5[1-9]|[6-9][0-9]|[1-9][0-9][0-9])$");
        enrolled.setSearch(enrolledList);

        FilterClinician clinician = new FilterClinician();
        List<String> cliniciansList = new ArrayList<>();
        cliniciansList.add("Angelo Popper");

        request.setFilterEnrolled(enrolled);
        request.setFilterStatus(status);
        request.setFilterClinician(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyAllDaysEnrolledPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(4);
        status.setSearch(statusList);

        FilterEnrolled enrolled = new FilterEnrolled();
        List<String> enrolledList = new ArrayList<>();
        enrolledList.add("^([0-9]|1[0-5])$");
        enrolledList.add("^(1[6-9]|2[0-9]|30)$");
        enrolledList.add("^(3[1-9]|4[0-9]|50)$");
        enrolledList.add("^(5[1-9]|[6-9][0-9]|[1-9][0-9][0-9])$");
        enrolled.setSearch(enrolledList);

        FilterClinician clinician = new FilterClinician();
        List<String> cliniciansList = new ArrayList<>();
        cliniciansList.add("Angelo Popper");

        request.setFilterEnrolled(enrolled);
        request.setFilterStatus(status);
        request.setFilterClinician(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyActivatedStatusPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        status.setSearch(statusList);

        request.setFilterStatus(status);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyPausedStatusPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(2);
        status.setSearch(statusList);

        request.setFilterStatus(status);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyDeactivatedStatusPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(3);
        status.setSearch(statusList);

        request.setFilterStatus(status);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyPreActivatedStatusPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(4);
        status.setSearch(statusList);

        request.setFilterStatus(status);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyDischargedStatusPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(5);
        status.setSearch(statusList);

        request.setFilterStatus(status);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyAllStatusesPatients() {
        FilterRequest request = new FilterRequest();

        FilterStatus status = new FilterStatus();
        List<Integer> statusList = new ArrayList<>();
        statusList.add(1);
        statusList.add(2);
        statusList.add(3);
        statusList.add(4);
        statusList.add(5);
        status.setSearch(statusList);

        FilterEnrolled enrolled = new FilterEnrolled();
        List<String> enrolledList = new ArrayList<>();
        enrolledList.add("^([0-9]|1[0-5])$");
        enrolledList.add("^(1[6-9]|2[0-9]|30)$");
        enrolledList.add("^(3[1-9]|4[0-9]|50)$");
        enrolledList.add("^(5[1-9]|[6-9][0-9]|[1-9][0-9][0-9])$");
        enrolled.setSearch(enrolledList);

        FilterClinician clinician = new FilterClinician();
        List<String> cliniciansList = new ArrayList<>();
        cliniciansList.add("Angelo Popper");

        request.setFilterEnrolled(enrolled);
        request.setFilterStatus(status);
        request.setFilterClinician(clinician);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyReviewedPatients() {
        FilterRequest request = new FilterRequest();

        FilterReviewed reviewed = new FilterReviewed();
        List<Integer> reviewedList = new ArrayList<>();
        reviewedList.add(1);
        reviewed.setSearch(reviewedList);

        request.setFilterReviewed(reviewed);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyNonReviewedStatusPatients() {
        FilterRequest request = new FilterRequest();
        FilterReviewed reviewed = new FilterReviewed();
        List<Integer> reviewedList = new ArrayList<>();
        reviewedList.add(2);
        reviewed.setSearch(reviewedList);

        request.setFilterReviewed(reviewed);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnlyReviewedAndNonReviewedPatients() {
        FilterRequest request = new FilterRequest();
        FilterReviewed reviewed = new FilterReviewed();
        List<Integer> reviewedList = new ArrayList<>();
        reviewedList.add(1);
        reviewedList.add(2);
        reviewed.setSearch(reviewedList);

        request.setFilterReviewed(reviewed);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test
    public void testShowOnSubgroupsPatients() {
        FilterRequest request = new FilterRequest();
        FilterSubgroup subgroups = new FilterSubgroup();
        List<String> subgroupList = new ArrayList<>();
        subgroupList.add("west coast");
        subgroupList.add("west coast");
        subgroups.setSearch(subgroupList);

        request.setFilterSubgroup(subgroups);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");

    }

    @Test
    public void testShowNoSubgroupsPatients() {
        FilterRequest request = new FilterRequest();
        FilterSubgroup subgroups = new FilterSubgroup();
        List<String> subgroupList = new ArrayList<>();
        subgroupList.add("--");
        subgroups.setSearch(subgroupList);

        request.setFilterSubgroup(subgroups);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");

    }

    @Test
    public void testShowClinicianPatients() {
        FilterRequest request = new FilterRequest();
        FilterClinician clinicians = new FilterClinician();
        List<String> clinicianList = new ArrayList<>();
        clinicianList.add("Olga Gnezdyonova");
        clinicians.setSearch(clinicianList);

        request.setFilterClinician(clinicians);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(request);
        Response response = restUtilities.getResponse(reqSpec, "post");

        assertEquals(response.getStatusCode(), 200);
        reqSpec.body("");
    }

    @Test(priority = 1)
    public void testResetClinicianPasswordOnSettingsWithEmptyFields() {
        restUtilities.getResponseSpecification(restUtilities.createResponseBuilder()
                .expectStatusCode(400)
                .expectContentType(ContentType.JSON));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PASSWORD);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        restUtilities.getResponse(reqSpec, "post");
    }

    @Test(priority = 1)
    public void testResetClinicianPasswordOnSettingsWithIncorectOldPass() {
        restUtilities.getResponseSpecification(restUtilities.createResponseBuilder()
                .expectStatusCode(400)
                .expectContentType(ContentType.JSON));

        Map<String, String> data = new HashMap<>();
        data.put("confirm_new_password", "Test@1234567890");
        data.put("new_password", "Test@1234567890");
        data.put("old_password", "123456");

        reqSpec.body(data);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PASSWORD);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("message"), "bad old password", "The error has been modified.");
    }

}

package com.hrs.cc.api.tests.environment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Modules;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.environment.Environment;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class TestFilesCollection extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private ObjectMapper mapper = new ObjectMapper();
    private static Environment environment;
    private static Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private PropertyWorker propertyWorker;
    private static String fileId;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
    }


    @BeforeMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
        reqSpec.body("");
    }

    @Test
    public void testAddFile() {
        Map<String, Object> body = new HashMap<>();
        Map<String, String> data = new HashMap<>();
        data.put("requestedName", "Test file ".concat(RandomData.getRandomString(4)));
        data.put("content", Modules.IMAGE_BASE64);
        body.put("data", data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CONTENT);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.body(body);
        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();

        assertNotNull(path.get("data.id"));
//        assertNotNull(path.get("data.url"));
        assertNotNull(path.get("data.createdAt"));

        fileId = path.get("data.id").toString();
    }

    @Test(dependsOnMethods = {"testAddFile"})
    public void testGetFileById() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CONTENT.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", fileId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtilities.getResponse(reqSpec, "get");
        JsonPath path = response.jsonPath();

        assertNotNull(path.get("data.id"));
        assertNotNull(path.get("data.url"));
        assertNotNull(path.get("data.createdAt"));
    }

    @Test(dependsOnMethods = {"testAddFile", "testGetFileById"})
    public void testRemoveFileById() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(204)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CONTENT.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", fileId);
        restUtilities.createQueryParam(reqSpec, "permanent", "true");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtilities.getResponse(reqSpec, "delete");

    }
}

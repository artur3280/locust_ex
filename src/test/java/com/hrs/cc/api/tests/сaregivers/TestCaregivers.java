package com.hrs.cc.api.tests.сaregivers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.caregiver.caregiver_obj.Caregiver;
import com.hrs.cc.api.models.caregiver.caregiver_obj.Data;
import com.hrs.cc.api.models.caregiver.caregiver_obj.Identity;
import com.hrs.cc.api.models.caregiver.chatroom.ChatRoomsCareGiver;
import com.hrs.cc.api.models.caregiver.connections.CaregiverConnectionsResponse;
import com.hrs.cc.api.models.caregiver.message.MessageCareGiver;
import com.hrs.cc.api.models.caregiver.message.MessagesCareGiver;
import com.hrs.cc.api.models.caregiver.patient_metrics.CareGiverPatientMetrics;
import core.PropertyWorker;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TestCaregivers extends Assert {
    private ObjectMapper mapper = new ObjectMapper();
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private PropertyWorker propertyWorker;
    private Map<String, String> userData;
    //    private String pacientHrsId;
    private static Caregiver dataCaregiver;
    //    private static String careGiverId = "NPAiiY9gnWZJyG9B";
    private static String careGiverId;
    private static final String CARE_GIVER_LOGIN = "test_caregiver_54WLN";
    private static final String CARE_GIVER_PASS = "P7MBSDKSBF";
    private static String careGiverToken;
    private static String patientId;
    private static String patientPin;
    private static Integer newChatRoomId;
    private JsonPath chatroom;


    @BeforeTest
    public void setUp() {
        Logger.debugging(false);
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {

        try {
            propertyWorker.setProperty("patient_hrs_id", RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG));
        } catch (IOException e) {
            e.printStackTrace();
        }

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath("");
    }

    @AfterClass
    public void removeObj() {
        RestAssistant.deactivateConnection(propertyWorker.getProperty("patient_hrs_id"));
        mapper = null;
        reqSpec = null;
        resPec = null;
        restUtilities = null;
        userData = null;
        propertyWorker = null;
        System.gc();

    }

    @AfterMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        restUtilities.removeQueryParam(reqSpec, "filter[patient]");
        restUtilities.removeQueryParam(reqSpec, "start");
        restUtilities.removeQueryParam(reqSpec, "end");
        reqSpec.body("");
    }

    @Test
    public void testCreateCareGiverMissingUsername() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(400)
        );

        Data data = new Data();
        data.setFirstName("test first name " + restUtilities.getRandomString(5));
        data.setLastName("test last name " + restUtilities.getRandomString(5));
        Identity identity = new Identity();
        identity.setEmail("test_".concat(restUtilities.getRandomString(6).concat("@gmail.com")));
        identity.setPassword(restUtilities.getRandomString(10));
        data.setIdentity(identity);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CAREGIVERS);

        reqSpec.body(new Caregiver(data));
        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertNotNull(path.get("message"), "massege field is null");
        assertTrue(path.get("message").toString().contains("The given data was invalid"), "massege error is not eql");
//        assertTrue(path.get("errors").toString().contains("The Username field is required."));
    }

    @Test(dependsOnMethods = {"testCreateCareGiverMissingUsername"})
    public void testCreateCareGiverEmptyPassword() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(400)
        );

        Data data = new Data();
        data.setFirstName("test first name " + restUtilities.getRandomString(5));
        data.setLastName("test last name " + restUtilities.getRandomString(5));
        Identity identity = new Identity();
        identity.setUsername("test_caregiver_" + restUtilities.getRandomString(5));
        identity.setEmail("test_".concat(restUtilities.getRandomString(6).concat("@gmail.com")));
        data.setIdentity(identity);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CAREGIVERS);

        reqSpec.body(new Caregiver(data));
        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertNotNull(path.get("message"), "massege field is null");
        assertTrue(path.get("message").toString().contains("The given data was invalid"), "massege error is not eql");
//        assertTrue(path.get("errors").toString().contains("The Password field is required."));
    }

    @Test(dependsOnMethods = {"testCreateCareGiverMissingUsername", "testCreateCareGiverEmptyPassword"})
    public void testCreateCareGiverMissingEmail() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(400)
        );

        Data data = new Data();
        data.setFirstName("test first name " + restUtilities.getRandomString(5));
        data.setLastName("test last name " + restUtilities.getRandomString(5));
        Identity identity = new Identity();
        identity.setUsername("test_caregiver_" + restUtilities.getRandomString(5));
        identity.setEmail("");
        identity.setPassword(restUtilities.getRandomString(10));
        data.setIdentity(identity);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CAREGIVERS);

        reqSpec.body(new Caregiver(data));
        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertNotNull(path.get("message"), "massege field is null");
        assertTrue(path.get("message").toString().contains("The given data was invalid"), "massege error is not eql");
//        assertTrue(path.get("errors").toString().contains("The Email field is required."));
    }

    @Test
    public void testCreateCareGiver() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );

        dataCaregiver = new Caregiver();

        Data data = new Data();
        data.setFirstName("test first name " + restUtilities.getRandomString(5));
        data.setLastName("test last name " + restUtilities.getRandomString(5));
        Identity identity = new Identity();
        identity.setUsername("test_caregiver_" + restUtilities.getRandomString(5));
        identity.setEmail("test_".concat(restUtilities.getRandomString(6).concat("@gmail.com")));
        identity.setPassword(restUtilities.getRandomString(10));
        data.setIdentity(identity);
        dataCaregiver.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CAREGIVERS);

        reqSpec.body(dataCaregiver);
        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertNotNull(path.get("data.firstName"), "data.firstName field is null");
        assertNotNull(path.get("data.lastName"), "data.lastNam field is null");
        assertEquals(path.get("data.firstName"), dataCaregiver.getData().getFirstName(), "data.firstName field not eql " + dataCaregiver.getData().getFirstName());
        assertEquals(path.get("data.lastName"), dataCaregiver.getData().getLastName(), "data.lastName field not eql " + dataCaregiver.getData().getLastName());
        careGiverId = path.get("data.id");
    }

    @Test(dependsOnMethods = {"testCreateCareGiver"})
    public void testCreateCareGiverUseAgainData() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(422)
        );
        reqSpec.body(dataCaregiver);
        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertNotNull(path.get("errors"), "errors field is null");
        assertTrue(path.get("errors").toString().contains("The data.username has already been taken."), "errors field not eql 'The data.username has already been taken.'");
        assertTrue(path.get("errors").toString().contains("The data.email has already been taken."), "errors field not eql 'The data.email has already been taken.'");
    }

    @Test(priority = 1)
    public void testLoginCareGiver() {
        RestUtilities restUtil = new RestUtilities();

        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        resPec = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint("");

        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        data.put("type", "credentials");
//        data.put("username", CARE_GIVER_LOGIN);
//        data.put("password", CARE_GIVER_PASS);
        data.put("username", dataCaregiver.getData().getIdentity().getUsername());
        data.put("password", dataCaregiver.getData().getIdentity().getPassword());
        body.put("data", data);

        reqSpecification.body(body);
        Response response = restUtil.getResponse(reqSpecification, "post");
        JsonPath path = response.jsonPath();
        assertNotNull(path.get("data.token"), "data.token is null");

        careGiverToken = path.get("data.token");
    }

    @Test(dependsOnMethods = {"testLoginCareGiver", "testCreateNewPatientLinkPinV1"}, priority = 1)
    public void testCreateNewPatientLinkPin() {
        String tokenPatient = RestAssistant.getTokenPatient(propertyWorker.getProperty("patient_hrs_id"));

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200));
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_LINK_PINS);

        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        data.put("patientId", propertyWorker.getProperty("patient_hrs_id"));
        data.put("environment", "QATestingZone");
        body.put("data", data);

        reqSpecification.body(body);
        reqSpecification.header("Authorization", "Bearer ".concat(tokenPatient));


        Response response = restUtilities.getResponse(reqSpecification, "post");
        JsonPath jsonPath = response.jsonPath();
        assertNotNull(jsonPath.get("data.code"), "data.code is null");
        patientPin = jsonPath.get("data.code");
    }

    @Test(dependsOnMethods = {"testLoginCareGiver"}, priority = 1)
    public void testCreateNewPatientLinkPinV1() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        ResponseSpecification resPec = restUtilities.getResponseSpecification();
        reqSpecification.basePath(Path.API_V1);

        String tokenPatient = RestAssistant.getTokenPatient(propertyWorker.getProperty("patient_hrs_id"));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_LINK_PINS);

        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        data.put("patientId", propertyWorker.getProperty("patient_hrs_id"));
        data.put("environment", "QATestingZone");
        body.put("data", data);

        reqSpecification.body(body);
        reqSpecification.header("Authorization", "Bearer ".concat(tokenPatient));


        Response response = restUtilities.getResponse(reqSpecification, "post");
        JsonPath jsonPath = response.jsonPath();
        assertNotNull(jsonPath.get("data.attributes.code"), "data.attributes.code is null");
        assertEquals(jsonPath.get("data.resourceType"), "patient-link-pin", "resource type not eql to 'patient-link-pin'");
    }


    @Test(dependsOnMethods = {"testLoginCareGiver", "testCreateNewPatientLinkPin"}, priority = 1)
    public void testGetGenerationPinLink() {

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(204));
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_LINKS);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));
        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        data.put("pin", patientPin);
        body.put("data", data);
        reqSpecification.body(body);
        Response response = restUtilities.getResponse(reqSpecification, "post");
        assertEquals(response.getStatusCode(), 204);

    }

    @Test(dependsOnMethods = {"testLoginCareGiver", "testCreateNewPatientLinkPin", "testGetGenerationPinLink"}, priority = 1)
    public void testGetCareGiverDataResponse() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200));
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_LINKS);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));
        restUtilities.createQueryParam(reqSpecification, "filter[caregiver]", careGiverId);

        Response response = restUtilities.getResponse(reqSpecification, "get");
        CaregiverConnectionsResponse connections = response.as(CaregiverConnectionsResponse.class);
        patientId = connections.getData().get(0).getPatient();
        connections.getData().forEach(c -> {
            assertNotNull(c.getId(), "Id field is null");
            assertNotNull(c.getPatient(), "Patient field is null");
            assertNotNull(c.getCaregiver(), "Caregiver field is null");
            assertEquals(c.getCaregiver(), careGiverId, "caregiver field not eql " + careGiverId);
            assertNotNull(c.getEnvironment(), "Environment field is null");
            assertNotNull(c.getCreated(), "Created field is null");
        });

    }

    @Test(dependsOnMethods = {"testLoginCareGiver",
            "testCreateNewPatientLinkPin",
            "testGetGenerationPinLink",
            "testGetCareGiverDataResponse"}, priority = 1)
    public void testGetCareGiverPatientMetrics() {
        RestAssistant.setAllMetric(patientId);

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200));
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint("/apiv2" + EndPoints.CGC_PATIENT.concat(EndPoints.ID));
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));
        restUtilities.createPathParam(reqSpecification, "id", propertyWorker.getProperty("patient_hrs_id"));

        Response response = restUtilities.getResponse(reqSpecification, "get");
        CareGiverPatientMetrics metrics = response.as(CareGiverPatientMetrics.class);
        assertEquals(java.util.Optional.ofNullable(metrics.getMetrics().getGlucose().getPercentComplete()), java.util.Optional.ofNullable(100));
        assertEquals(java.util.Optional.ofNullable(metrics.getMetrics().getActivity().getPercentComplete()), java.util.Optional.ofNullable(100));
        assertEquals(java.util.Optional.ofNullable(metrics.getMetrics().getTemperature().getPercentComplete()), java.util.Optional.ofNullable(100));
        assertEquals(java.util.Optional.ofNullable(metrics.getMetrics().getWeight().getPercentComplete()), java.util.Optional.ofNullable(100));
        assertEquals(java.util.Optional.ofNullable(metrics.getMetrics().getBloodpressure().getPercentComplete()), java.util.Optional.ofNullable(100));
        assertEquals(java.util.Optional.ofNullable(metrics.getMetrics().getPulseox().getPercentComplete()), java.util.Optional.ofNullable(100));

        assertNotNull(metrics.getMetrics().getStethoscope().getPercentComplete());
        assertNotNull(metrics.getMetrics().getStethoscope().getRisk());

        try {
            assertNotNull(metrics.getMetrics().getPatientConnectVoice().getPercentComplete());
        } catch (NullPointerException e) {
            System.out.println("Does not found percent complete field, please check!");
        }

        try {
            assertNotNull(metrics.getMetrics().getPatientConnectVoice().getRisk());
        } catch (NullPointerException e) {
            System.out.println("Does not found patient connect voice risk, please check!");
        }

    }


    @Test(dependsOnMethods = {"testLoginCareGiver"}, priority = 2)
    public void testCreateNewChatRoomCaregiver() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200));
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_ROOMS);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        ArrayList<String> p = new ArrayList<>();
        p.add(propertyWorker.getProperty("patient_hrs_id"));
        data.put("participants", p);
        body.put("data", data);
        reqSpecification.body(body);

        Response response = restUtilities.getResponse(reqSpecification, "post");
        chatroom = response.jsonPath();
        JsonPath path = response.jsonPath();
        assertNotNull(path.get("data.id"), "data.id is null");
        assertTrue(path.get("data.participants").toString().contains(p.get(0)), "data.participants field not eql");
        newChatRoomId = path.get("data.id");
    }

    @Test(dependsOnMethods = {"testLoginCareGiver", "testCreateNewChatRoomCaregiver"}, priority = 2)
    public void testCreateNewChatRoomCaregiverV1() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        ResponseSpecification resPec = restUtilities.getResponseSpecification();
        reqSpecification.basePath(Path.API_V1);


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_ROOMS);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        ArrayList<String> p = new ArrayList<>();
        p.add(propertyWorker.getProperty("patient_hrs_id"));
        data.put("participants", p);
        body.put("data", data);
        reqSpecification.body(body);

        Response response = restUtilities.getResponse(reqSpecification, "post");
        JsonPath pathV1 = response.jsonPath();

        assertEquals(pathV1.get("data.id").toString(), chatroom.get("data.id").toString(), "id field not eql " + chatroom.get("data.id"));
        assertEquals(pathV1.get("data.resourceType").toString(), chatroom.get("data.resourceType").toString(), "resourceType field not eql " + chatroom.get("data.resourceType"));
        assertEquals(pathV1.get("data.attributes.participants").toString(), chatroom.get("data.participants").toString(), "attributes.participants field not eql " + chatroom.get("data.participants"));
    }


    @Test(dependsOnMethods = {"testLoginCareGiver", "testCreateNewChatRoomCaregiver"}, priority = 2)
    public void testGetAllChatRoomsCaregiver() {

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200));
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_ROOMS);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        Response response = restUtilities.getResponse(reqSpecification, "get");
        ChatRoomsCareGiver rooms = response.as(ChatRoomsCareGiver.class);
        rooms.getData().forEach(c -> {
            assertEquals(c.getResourceType(), "chatrooms", "ResourceType not eql chat room");
            assertNotNull(c.getId(), "chat id is null");
            assertEquals(c.getId(), newChatRoomId, "not eql chat room");
            assertTrue(c.getParticipants().contains(propertyWorker.getProperty("patient_hrs_id")), "does not have patient id");
            assertNotNull(c.getCreatedAt(), "CreatedAt is null");

        });

    }

    @Test(dependsOnMethods = {"testLoginCareGiver", "testCreateNewChatRoomCaregiver"}, priority = 2)
    public void testGetAllChatRoomsCaregiverV1() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        ResponseSpecification resPec = restUtilities.getResponseSpecification();
        reqSpecification.basePath(Path.API_V1);


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_ROOMS);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        Response response = restUtilities.getResponse(reqSpecification, "get");
        ChatRoomsCareGiver rooms = response.as(ChatRoomsCareGiver.class);
        rooms.getData().forEach(c -> {
            assertEquals(c.getResourceType(), "chatrooms", "ResourceType not eql chat room");
            assertNotNull(c.getId(), "chat id is null");
            assertEquals(c.getId(), newChatRoomId, "not eql chat room");
            assertTrue(c.getAttributes().getParticipants().contains(propertyWorker.getProperty("patient_hrs_id")), "does not have patient id");
            assertNotNull(c.getAttributes().getCreatedAt(), "CreatedAt is null");

        });

    }

    @Test(dependsOnMethods = {"testLoginCareGiver", "testCreateNewChatRoomCaregiver", "testGetAllChatRoomsCaregiver"}, priority = 2)
    public void testPostNewMessageInChatRoomCaregiver() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200));
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_MESSAGES);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        data.put("chatroomId", newChatRoomId);
        data.put("targetHrsid", propertyWorker.getProperty("patient_hrs_id"));
        data.put("message", "Test message!" + restUtilities.getRandomString(20));
        data.put("hrsid", careGiverId);
        body.put("data", data);
        reqSpecification.body(body);

        Response response = restUtilities.getResponse(reqSpecification, "post");

        MessageCareGiver message = response.as(MessageCareGiver.class);

        assertNotNull(message.getData().getId(), "Chat id is null");
        assertEquals(message.getData().getResourceType(), "chats", "resourceType is not eql 'chats'");
        assertEquals(message.getData().getChatroomId(), newChatRoomId, "chat room id not eql");
        assertEquals(message.getData().getHrsid(), careGiverId, "hrsId not eql");
        assertEquals(message.getData().getMessage(), data.get("message"), "message not eql");
    }


    @Test(dependsOnMethods = {"testLoginCareGiver", "testCreateNewChatRoomCaregiver", "testGetAllChatRoomsCaregiver"}, priority = 2)
    public void testPostNewMessageInChatRoomCaregiverV1() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        ResponseSpecification resPec = restUtilities.getResponseSpecification();
        reqSpecification.basePath(Path.API_V1);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_MESSAGES);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        data.put("chatroomId", newChatRoomId);
        data.put("targetHrsid", propertyWorker.getProperty("patient_hrs_id"));
        data.put("message", "Test message!" + restUtilities.getRandomString(20));
        data.put("hrsid", careGiverId);
        body.put("data", data);
        reqSpecification.body(body);

        Response response = restUtilities.getResponse(reqSpecification, "post");

        MessageCareGiver message = response.as(MessageCareGiver.class);

        assertNotNull(message.getData().getId(), "Chat id is null");
        assertEquals(message.getData().getResourceType(), "chats", "resourceType is not eql 'chats'");
        assertEquals(message.getData().getAttributes().getChatroomId(), newChatRoomId, "chat room id not eql");
        assertEquals(message.getData().getAttributes().getHrsid(), careGiverId, "hrsId not eql");
        assertEquals(message.getData().getAttributes().getMessage(), data.get("message"), "message not eql");
    }

    @Test(dependsOnMethods = {"testLoginCareGiver", "testCreateNewChatRoomCaregiver", "testGetAllChatRoomsCaregiver", "testPostNewMessageInChatRoomCaregiver"}, priority = 2)
    public void testGetPollingInChatRoomCaregiver() {

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200));
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_MESSAGES);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        restUtilities.createQueryParam(reqSpecification, "filter[chatroomId]", newChatRoomId.toString());

        Response response = restUtilities.getResponse(reqSpecification, "get");

        MessagesCareGiver messages = response.as(MessagesCareGiver.class);
        messages.getMessages().forEach(message -> {
            assertEquals(message.getChatroomId(), newChatRoomId, "ChatroomId field not eql" + newChatRoomId);
            assertEquals(message.getHrsid(), careGiverId, "Hrsid field not eql" + careGiverId);
            assertNotNull(message.getMessage(), "Message field is null");
        });
    }

    @Test(dependsOnMethods = {"testLoginCareGiver",
            "testCreateNewChatRoomCaregiver",
            "testGetAllChatRoomsCaregiver",
            "testPostNewMessageInChatRoomCaregiver",
            "testGetPollingInChatRoomCaregiver"}, priority = 1)
    public void testPollingWhenNotInChatRoomCaregiver() {

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200));
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_MESSAGES);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        restUtilities.createQueryParam(reqSpecification, "filter[hrsid]", careGiverId);
        restUtilities.createQueryParam(reqSpecification, "filter[targetHrsid]", propertyWorker.getProperty("patient_hrs_id"));

        Response response = restUtilities.getResponse(reqSpecification, "get");

        MessagesCareGiver messages = response.as(MessagesCareGiver.class);
        messages.getMessages().forEach(message -> {
            assertNotNull(message.getChatroomId(), "ChatroomId field is null");
            assertEquals(message.getHrsid(), careGiverId, "Hrsid not eql " + careGiverId);
            assertNotNull(message.getMessage(), "Message field is null");
        });
    }

    @Test(dependsOnMethods = {"testLoginCareGiver",
            "testCreateNewChatRoomCaregiver",
            "testGetAllChatRoomsCaregiver",
            "testPostNewMessageInChatRoomCaregiver",
            "testGetPollingInChatRoomCaregiver"}, priority = 2)
    public void testGetChatMessagesByRoomIdAfterDateBeforeDate() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200));
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_MESSAGES);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        restUtilities.createQueryParam(reqSpecification, "filter[chatroomId]", newChatRoomId.toString());
        restUtilities.createQueryParam(reqSpecification, "filter[afterDate]", restUtilities.getTimePeriodTS(10).get("minusDays").concat("+02:00"));
        restUtilities.createQueryParam(reqSpecification, "filter[beforeDate]", restUtilities.getTimePeriodTS(1).get("current").concat("+02:00"));

        Response response = restUtilities.getResponse(reqSpecification, "get");

        MessagesCareGiver messages = response.as(MessagesCareGiver.class);
        messages.getMessages().forEach(message -> {
            assertNotNull(message.getId(), "Message id is null");
            assertEquals(message.getResourceType(), "chats", "Resource type is not eql chats");
            assertEquals(message.getChatroomId(), newChatRoomId, "Chatroom id is not eql " + newChatRoomId);
            assertEquals(message.getHrsid(), careGiverId, "Hrs id not eql " + careGiverId);
            assertNotNull(message.getMessage(), "Message is null or empty");
            assertNotNull(message.getCreatedAt(), "CreatedAt is null or empty");
        });
    }

    @Test(dependsOnMethods = {"testLoginCareGiver",
            "testCreateNewChatRoomCaregiver",
            "testGetAllChatRoomsCaregiver",
            "testPostNewMessageInChatRoomCaregiver",
            "testGetPollingInChatRoomCaregiver"}, priority = 2)
    public void testGetChatMessagesByRoomIdAfterDateBeforeDateV1() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        ResponseSpecification resPec = restUtilities.getResponseSpecification();
        reqSpecification.basePath(Path.API_V1);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_MESSAGES);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        restUtilities.createQueryParam(reqSpecification, "filter[chatroomId]", newChatRoomId.toString());
        restUtilities.createQueryParam(reqSpecification, "filter[afterDate]", restUtilities.getTimePeriodTS(10).get("minusDays").concat("+02:00"));
        restUtilities.createQueryParam(reqSpecification, "filter[beforeDate]", restUtilities.getTimePeriodTS(1).get("current").concat("+02:00"));

        Response response = restUtilities.getResponse(reqSpecification, "get");

        MessagesCareGiver messages = response.as(MessagesCareGiver.class);
        messages.getMessages().forEach(message -> {
            assertNotNull(message.getId(), "Message id is null");
            assertEquals(message.getResourceType(), "chats", "Resource type is not eql chats");
            assertEquals(message.getAttributes().getChatroomId(), newChatRoomId, "Chatroom id is not eql " + newChatRoomId);
            assertEquals(message.getAttributes().getHrsid(), careGiverId, "Hrs id not eql " + careGiverId);
            assertNotNull(message.getAttributes().getMessage(), "Message is null or empty");
            assertNotNull(message.getAttributes().getCreatedAt(), "CreatedAt is null or empty");
        });

    }


    @Test(dependsOnMethods = {"testLoginCareGiver",
            "testCreateNewChatRoomCaregiver",
            "testGetAllChatRoomsCaregiver",
            "testPostNewMessageInChatRoomCaregiver",
            "testGetPollingInChatRoomCaregiver"}, priority = 2)
    public void testGetChatMessagesByHrsIdTargetHrsId() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200));
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_MESSAGES);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        restUtilities.createQueryParam(reqSpecification, "filter[hrsid]", careGiverId);
        restUtilities.createQueryParam(reqSpecification, "filter[targetHrsid]", propertyWorker.getProperty("patient_hrs_id"));

        Response response = restUtilities.getResponse(reqSpecification, "get");

        MessagesCareGiver messages = response.as(MessagesCareGiver.class);
        messages.getMessages().forEach(message -> {
            assertNotNull(message.getId(), "Message id is null");
            assertEquals(message.getResourceType(), "chats", "Resource type is not eql chats");
            assertEquals(message.getChatroomId(), newChatRoomId, "Chatroom id is not eql " + newChatRoomId);
            assertEquals(message.getHrsid(), careGiverId, "Hrs id not eql " + careGiverId);
            assertNotNull(message.getMessage(), "Message is null or empty");
            assertNotNull(message.getCreatedAt(), "CreatedAt is null or empty");
        });
    }

    @Test(dependsOnMethods = {"testLoginCareGiver",
            "testCreateNewChatRoomCaregiver",
            "testGetAllChatRoomsCaregiver",
            "testPostNewMessageInChatRoomCaregiver",
            "testGetPollingInChatRoomCaregiver"}, priority = 2)
    public void testGetChatMessagesByHrsIdTargetHrsIdV1() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        ResponseSpecification resPec = restUtilities.getResponseSpecification();
        reqSpecification.basePath(Path.API_V1);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CHAT_MESSAGES);
        reqSpecification.header("Authorization", "Bearer ".concat(careGiverToken));

        restUtilities.createQueryParam(reqSpecification, "filter[hrsid]", careGiverId);
        restUtilities.createQueryParam(reqSpecification, "filter[targetHrsid]", propertyWorker.getProperty("patient_hrs_id"));

        Response response = restUtilities.getResponse(reqSpecification, "get");

        MessagesCareGiver messages = response.as(MessagesCareGiver.class);
        messages.getMessages().forEach(message -> {
            assertNotNull(message.getId(), "Message id is null");
            assertEquals(message.getResourceType(), "chats", "Resource type is not eql chats");
            assertEquals(message.getAttributes().getChatroomId(), newChatRoomId, "Chatroom id is not eql " + newChatRoomId);
            assertEquals(message.getAttributes().getHrsid(), careGiverId, "Hrs id not eql " + careGiverId);
            assertNotNull(message.getAttributes().getMessage(), "Message is null or empty");
            assertNotNull(message.getAttributes().getCreatedAt(), "CreatedAt is null or empty");
        });
    }

}

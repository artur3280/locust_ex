package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.integratioins_app.answered_questions.EnvAnsweredQuestions;
import com.hrs.cc.api.models.integratioins_app.ivr.question.IvrQuestion;
import com.hrs.cc.api.models.integratioins_app.ivr.questions.Choice;
import com.hrs.cc.api.models.integratioins_app.ivr.questions.Datum;
import com.hrs.cc.api.models.integratioins_app.ivr.questions.IvrQuestionsList;
import com.hrs.cc.api.models.patients.syrvey.PatientSurveys;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import groovy.util.ObservableList;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestGatewayIvrSurvey extends Assert {
    private ObjectMapper mapper = new ObjectMapper();
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private PropertyWorker propertyWorker;
    private static String patientId;
    private static IvrQuestionsList questionsList;
    private static IvrQuestion question;
    private static IvrQuestionsList questionList;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();

        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath("");
        patientId = RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG);
    }

    @AfterClass
    public void removeObj() {
        mapper = null;
        reqSpec = null;
        resPec = null;
        restUtilities = null;
        queryParam = null;
        userData = null;
        propertyWorker = null;
        System.gc();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
    }

    @Test
    public void testGetEnvIrvQuestions() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_QUESTIONS);
        restUtilities.createQueryParam(reqSpec, "filter[env]", "QATestingZone");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
        questionsList = response.as(IvrQuestionsList.class);

        for (Datum question : questionsList.getData()) {
            assertNotNull(question.getId(), "Question id is null");
            assertNotNull(question.getEnvironment(), "Question Environment is null");
            assertNotNull(question.getQuestion(), "Question Question is null");
            assertNotNull(question.getCategory(), "Question Category is null");
            assertNotNull(question.getCreatedAt(), "Question CreatedAt is null");
            assertNotNull(question.getUpdatedAt(), "Question UpdatedAt is null");
            assertEquals(question.getResourceType(), "ivr-questions", "Type is not ivr-questions");
            question.getChoices().forEach(c -> {
                assertNotNull(c.getId(), "Choices id field is null");
                assertNotNull(c.getChoice(), "Choices choice field is null");
                assertNotNull(c.getText(), "Choices text field is null");
                assertNotNull(c.getCreatedAt(), "Choices CreatedAt field is null");
                assertNotNull(c.getUpdatedAt(), "Choices UpdatedAt field is null");
            });
        }

        restUtilities.removeQueryParam(reqSpec, "filter[env]");
    }

    @Test(dependsOnMethods = {"testGetEnvIrvQuestions"})
    public void testGetIvrQuestionResponseById() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_QUESTIONS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", questionsList.getData().get(0).getId().toString());
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Response response = restUtilities.getResponse(reqSpec, "get");
        question = response.as(IvrQuestion.class);

        assertEquals(question.getData().getId(), questionsList.getData().get(0).getId(), "Question ids is not eqls");
        assertNotNull(question.getData().getId(), "Question id is null");
        assertNotNull(question.getData().getEnvironment(), "Question Environment is null");
        assertNotNull(question.getData().getQuestion(), "Question Question is null");
        assertNotNull(question.getData().getCategory(), "Question Category is null");
        assertNotNull(question.getData().getCreatedAt(), "Question CreatedAt is null");
        assertNotNull(question.getData().getUpdatedAt(), "Question UpdatedAt is null");
        assertEquals(question.getData().getResourceType(), "ivr-questions", "Type is not ivr-questions");
        question.getData().getChoices().forEach(c -> {
            assertNotNull(c.getId(), "Choices id field is null");
            assertNotNull(c.getChoice(), "Choices choice field is null");
            assertNotNull(c.getText(), "Choices text field is null");
            assertNotNull(c.getCreatedAt(), "Choices CreatedAt field is null");
            assertNotNull(c.getUpdatedAt(), "Choices UpdatedAt field is null");
        });
    }

    @Test(dependsOnMethods = {"testGetEnvIrvQuestions", "testGetIvrQuestionResponseById"})
    public void testPostAnswerToQuestion() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(204)
        );

//        questionList = RestAssistant.getEnabledIrvQuestions();

        HashMap<Object, Object> bodyAnswer = new HashMap<>();
        ArrayList<Object> dataList = new ArrayList<Object>();

        Map<String, Object> answerOne = new HashMap<>();
        answerOne.put("hrsid", patientId);
        answerOne.put("ivrQuestionId", questionsList.getData().get(0).getId());
        answerOne.put("ivrQuestionText", questionsList.getData().get(0).getQuestion());
        answerOne.put("ivrQuestionChoiceId", questionsList.getData().get(0).getChoices().get(0).getId());
        answerOne.put("ivrQuestionChoiceText", questionsList.getData().get(0).getChoices().get(0).getText());

        Map<String, Object> answerTwo = new HashMap<>();
        answerTwo.put("hrsid", patientId);
        answerTwo.put("ivrQuestionId", questionsList.getData().get(1).getId());
        answerTwo.put("ivrQuestionText", questionsList.getData().get(1).getQuestion());
        answerTwo.put("ivrQuestionChoiceId", questionsList.getData().get(1).getChoices().get(0).getId());
        answerTwo.put("ivrQuestionChoiceText", questionsList.getData().get(1).getChoices().get(0).getText());

        dataList.add(answerOne);
        dataList.add(answerTwo);
        bodyAnswer.put("data", dataList);

        reqSpec.body(bodyAnswer);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_ANSWERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");
    }

    @Test(dependsOnMethods = {"testGetEnvIrvQuestions", "testGetIvrQuestionResponseById", "testPostAnswerToQuestion"})
    public void testGetEnvIrvAnswers() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_ANSWERS);
        restUtilities.createQueryParam(reqSpec, "filter[hrsid]", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
        EnvAnsweredQuestions answeredQuestions = response.as(EnvAnsweredQuestions.class);
        answeredQuestions.getData().forEach(a -> {
            assertNotNull(a.getId(), "Id field is null");
            assertNotNull(a.getHrsid(), "Hrsid field is null");
            assertNotNull(a.getIvrQuestionId(), "IvrQuestionId field is null");
            assertNotNull(a.getIvrQuestionChoiceId(), "IvrQuestionChoiceId field is null");
            assertNotNull(a.getQuestionText(), "QuestionText field is null");
            assertNotNull(a.getChoiceText(), "ChoiceText field is null");
            assertNotNull(a.getCreatedAt(), "CreatedAt field is null");
            assertNotNull(a.getUpdatedAt(), "UpdatedAt field is null");
        });
    }

    @Test(dependsOnMethods = {"testGetEnvIrvQuestions", "testGetIvrQuestionResponseById", "testPostAnswerToQuestion", "testGetEnvIrvAnswers"})
    public void testUpdateDataInTheQuestionById() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_QUESTIONS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", questionsList.getData().get(0).getId().toString());
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        List<Choice> listChoices = new ArrayList<>();
        Choice choice_one = question.getData().getChoices().get(0);
        choice_one.setText("edited ".concat(RandomData.getRandomString(5)));
        listChoices.add(choice_one);
        data.put("question", question.getData().getQuestion());
        data.put("category", question.getData().getCategory());
        data.put("choices", listChoices);
        body.put("data", data);
        reqSpec.body(body);

        Response response = restUtilities.getResponse(reqSpec, "patch");
        IvrQuestion question = response.as(IvrQuestion.class);

        assertNotNull(question.getData().getId(), "Id field is null");
        assertNotNull(question.getData().getEnvironment(), "Environment field is null");
        assertNotNull(question.getData().getQuestion(), "Question field is null");
        assertNotNull(question.getData().getCategory(), "Category field is null");
        assertNotNull(question.getData().getCreatedAt(), "CreatedAt field is null");
        assertNotNull(question.getData().getUpdatedAt(), "UpdatedAt field is null");
        assertEquals(question.getData().getId(), questionsList.getData().get(0).getId(), "Question ids is not equals");
        Choice choice_edited = question.getData().getChoices().get(0);
        assertEquals(choice_edited.getId(), choice_one.getId(), "Edited Id not equals");
        assertEquals(choice_edited.getChoice(), choice_one.getChoice(), "Edited Choice not equals");
        assertEquals(choice_edited.getText(), choice_one.getText(), "Edited Text not equals");
    }

    @Test(dependsOnMethods = {"testGetEnvIrvQuestions", "testGetIvrQuestionResponseById"})
    public void testGetIvrQuestionResponseByIncorrectId() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(404)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_QUESTIONS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", questionsList.getData().get(0).getId().toString().concat("23784683"));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Response response = restUtilities.getResponse(reqSpec, "get");
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
    }

    @Test(dependsOnMethods = {"testGetEnvIrvQuestions", "testGetIvrQuestionResponseById", "testUpdateDataInTheQuestionById"}, enabled = false)
    public void testUpdateChoiceInTheQuestionById() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(403)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_QUESTION_CHOICES.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", questionsList.getData().get(0).getChoices().get(0).getId().toString());
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        Choice choice_one = question.getData().getChoices().get(0);
        data.put("choice", choice_one.getId());
        data.put("text", "edited one choice ".concat(RandomData.getRandomString(4)));

        body.put("data", data);

        reqSpec.body(body);

        Response response = restUtilities.getResponse(reqSpec, "put");
    }

    @Test(dependsOnMethods = {"testGetEnvIrvQuestions", "testGetIvrQuestionResponseById", "testUpdateDataInTheQuestionById"}, enabled = false)
    public void testRemoveQuestionById() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(204)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_QUESTIONS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", questionsList.getData().get(0).getId().toString());
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "delete");


        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
    }

    @Test(dependsOnMethods = {"testGetEnvIrvQuestions", "testGetIvrQuestionResponseById", "testPostAnswerToQuestion", "testGetEnvIrvAnswers"})
    public void testPostAnswerToQuestionWithIncorrectData() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(422)
        );

        questionList = RestAssistant.getEnabledIrvQuestions();

        HashMap<Object, Object> bodyAnswer = new HashMap<>();
        ArrayList<Object> dataList = new ArrayList<Object>();

        Map<String, Object> answerOne = new HashMap<>();
        answerOne.put("hrsid", patientId);
        answerOne.put("ivrQuestionId", questionList.getData().get(0).getId());
        answerOne.put("ivrQuestionText", null);
        answerOne.put("ivrQuestionChoiceId", null);
        answerOne.put("ivrQuestionChoiceText", questionList.getData().get(0).getChoices().get(0).getText());

        Map<String, Object> answerTwo = new HashMap<>();
        answerTwo.put("hrsid", patientId);
        answerTwo.put("ivrQuestionId", questionList.getData().get(1).getId());
        answerTwo.put("ivrQuestionText", questionList.getData().get(1).getQuestion());
        answerTwo.put("ivrQuestionChoiceId", questionList.getData().get(1).getChoices().get(0).getId());
        answerTwo.put("ivrQuestionChoiceText", questionList.getData().get(1).getChoices().get(0).getText());

        dataList.add(answerOne);
        dataList.add(answerTwo);
        bodyAnswer.put("data", dataList);

        reqSpec.body(bodyAnswer);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_ANSWERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");

    }

    @Test(dependsOnMethods = {"testGetEnvIrvQuestions", "testGetIvrQuestionResponseById", "testPostAnswerToQuestion", "testGetEnvIrvAnswers", "testPostAnswerToQuestionWithIncorrectData"})
    public void testPostAnswerToQuestionWithIncorrectUserId() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(422)
        );

        questionList = RestAssistant.getEnabledIrvQuestions();

        HashMap<Object, Object> bodyAnswer = new HashMap<>();
        ArrayList<Object> dataList = new ArrayList<Object>();

        Map<String, Object> answerOne = new HashMap<>();
        answerOne.put("hrsid", RandomData.getRandomString(20));
        answerOne.put("ivrQuestionId", questionList.getData().get(0).getId());
        answerOne.put("ivrQuestionText", questionList.getData().get(0).getQuestion());
        answerOne.put("ivrQuestionChoiceId", questionList.getData().get(0).getChoices().get(0).getId());
        answerOne.put("ivrQuestionChoiceText", questionList.getData().get(0).getChoices().get(0).getText());

        Map<String, Object> answerTwo = new HashMap<>();
        answerTwo.put("hrsid", RandomData.getRandomString(20));
        answerTwo.put("ivrQuestionId", questionList.getData().get(1).getId());
        answerTwo.put("ivrQuestionText", questionList.getData().get(1).getQuestion());
        answerTwo.put("ivrQuestionChoiceId", questionList.getData().get(1).getChoices().get(0).getId());
        answerTwo.put("ivrQuestionChoiceText", questionList.getData().get(1).getChoices().get(0).getText());

        dataList.add(answerOne);
        dataList.add(answerTwo);
        bodyAnswer.put("data", dataList);

        reqSpec.body(bodyAnswer);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_ANSWERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");

    }

    @Test
    public void testGetPatientSurveys() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.API_V2 + EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.SYRVEY));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
//        PatientSurveys surveys = response.as(PatientSurveys.class);
        PatientSurveys surveys = response.as(new TypeRef<PatientSurveys>() {
        });

        surveys.getData().getSurveydata().forEach(surveydatum -> {
            assertNotNull(surveydatum.getSurveystatus(), "Surveystatus field is null");
            assertNotNull(surveydatum.getSchedule().getType(), "Schedule().getType field is null");
            assertNotNull(surveydatum.getMedalert(), "Medalert field is null");
            assertNotNull(surveydatum.getHighalert(), "Highalert field is null");
            assertNotNull(surveydatum.getCategory(), "Category field is null");
            assertNotNull(surveydatum.getQues(), "Ques field is null");
            assertNotNull(surveydatum.getQuesid(), "Quesid field is null");
            assertNotNull(surveydatum.getAnswertype(), "Answertype field is null");
            assertNotNull(surveydatum.getToday(), "Today field is null");
            assertNotNull(surveydatum.getAnswered(), "Answered field is null");
            try {
                surveydatum.getAnswerscale().forEach(answerscale -> {
                    assertNotNull(answerscale.getText(), "Text field is null");
                    assertNotNull(answerscale.getVal(), "Val field is null");
                });
            } catch (NullPointerException e) {
                System.out.println("Question: " + surveydatum.getQuesid() + " doesn't have the answer scale field.");
            }
        });

    }
}
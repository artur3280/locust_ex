package com.hrs.cc.api.tests.reports;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.patients.patient_list.Profile;
import com.hrs.cc.api.models.reports.patients.PatientsOfReport;
import com.hrs.cc.api.models.reports.statuses.Datum;
import com.hrs.cc.api.models.reports.statuses.ReportsStatusesResponse;
import com.hrs.cc.api.models.requests.add_new_report.ReportRequest;
import com.hrs.cc.api.models.requests.new_report_by_patient.ReportByPatientRequest;
import core.PropertyWorker;
import core.rest.RestUtilities;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

public class TestGenerateReportResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;

    private static String patientId;
    private String createdReportId;
    private Profile patient;
    private static Map<String, String> timePeriod;
    private PropertyWorker propertyWorker;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
//        reqSpec.log().all();
        reqSpec.basePath(Path.API_V2.concat(Path.REPORTS));
        resPec = restUtilities.getResponseSpecification();

        patient = RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG);
        patientId = Objects.requireNonNull(patient).getHrsid();
        timePeriod = restUtilities.getTimePeriod(7);

    }

    @AfterMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        restUtilities.removeQueryParam(reqSpec, "status[]");
        reqSpec.body("");
    }

    @Test(enabled = false)
    public void testGenerateReportWithoutPassword() {
        RestAssistant.testSetClinicianInsecureReportsSetting(true);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        List<ReportRequest> types = new ArrayList<>();
        types.add(new ReportRequest("population", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("adherence", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("biometrics", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("clinicians", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("communication", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("engagement", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("enrollment", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("notes", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("patientinteraction", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("readmissions", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("videostatus", timePeriod.get("minusDays"), timePeriod.get("current")));
        reqSpec.body(types);
        Response response = restUtilities.getResponse(reqSpec, "post");

        JsonPath path = response.jsonPath();
        assertNotNull(path.get("data.id"));
        assertNull(path.get("data.password"));
        assertNotNull(path.get("data.type"));
    }

    @Test(dependsOnMethods = {"testGenerateReportWithoutPassword"}, alwaysRun = true, enabled = false)
    public void testPostGenerateReport() {
        RestAssistant.testSetClinicianInsecureReportsSetting(false);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        List<ReportRequest> types = new ArrayList<>();
        types.add(new ReportRequest("population", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("adherence", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("biometrics", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("clinicians", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("communication", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("engagement", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("enrollment", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("notes", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("patientinteraction", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("readmissions", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("videostatus", timePeriod.get("minusDays"), timePeriod.get("current")));
        reqSpec.body(types);
        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        String createdReport = path.get("data.id");
        assertNotNull(path.get("data.id"));
        assertNotNull(path.get("data.password"));
        assertNotNull(path.get("data.type"));


        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.REPORT_STATUSES))
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token"))));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();

        Response checker = restUtilities.getResponse(newReqSpec, "get");
        ReportsStatusesResponse statusesResponse = checker.as(ReportsStatusesResponse.class);
        Datum report = statusesResponse.getData().stream().
                filter(r -> r.getId().equals(createdReport)).collect(Collectors.toList()).get(0);
        assertEquals(report.getPassword(), path.get("data.password"));
        assertNotNull(report.getStatus());
        assertNotNull(report.getReportType());
        assertNotNull(report.getId());
        assertNotNull(report.getStart());
        assertNotNull(report.getStop());
        assertNotNull(report.getDownloaded());
        assertNotNull(report.getReportType());
    }

    @Test(enabled = false)
    public void testPostGenerateReportByPatientId() {
        RestAssistant.testSetClinicianInsecureReportsSetting(false);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        List<ReportByPatientRequest> types = new ArrayList<>();

        types.add(new ReportByPatientRequest("notes", patientId, timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportByPatientRequest("physician", patientId, timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportByPatientRequest("trends", patientId, timePeriod.get("minusDays"), timePeriod.get("current")));

        reqSpec.body(types);
        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        String createdReport = path.get("data.id");
        assertNotNull(path.get("data.id"));
        assertNotNull(path.get("data.password"));
        assertNotNull(path.get("data.type"));

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.REPORT_STATUSES))
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token"))));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();

        Response checker = restUtilities.getResponse(newReqSpec, "get");
        ReportsStatusesResponse statusesResponse = checker.as(ReportsStatusesResponse.class);
        Datum report = statusesResponse.getData().stream().
                filter(r -> r.getId().equals(createdReport)).collect(Collectors.toList()).get(0);
        assertEquals(report.getPassword(), path.get("data.password"));
        assertNotNull(report.getStatus());
        assertNotNull(report.getReportType());
        assertNotNull(report.getId());
        createdReportId = report.getId();
        assertNotNull(report.getStart());
        assertNotNull(report.getStop());
        assertNotNull(report.getDownloaded());
        assertNotNull(report.getReportType());
    }

    @Test(dependsOnMethods = {"testPostGenerateReportByPatientId"}, enabled = false)
    public void testGetPatientByReportId() {
        RestAssistant.testSetClinicianInsecureReportsSetting(false);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PATIENTS));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", createdReportId), Method.GET.name());

        PatientsOfReport patients = response.as(PatientsOfReport.class);
        patients.getData().forEach(datum -> {
            assertEquals(datum.getPatient(), patientId);
            assertEquals(datum.getPid(), patient.getPid());

            assertEquals(datum.getName().getFirst(), patient.getName().getFirst());
            assertEquals(datum.getName().getLast(), patient.getName().getLast());
            assertEquals(datum.getName().getMiddle(), patient.getName().getMiddle());

            assertEquals(datum.getDob(), patient.getDob());
            assertEquals(datum.getGender(), patient.getGender());
            assertEquals(datum.getPhone(), patient.getPhone());
            assertEquals(datum.getStatus(), "active");
        });
    }
}

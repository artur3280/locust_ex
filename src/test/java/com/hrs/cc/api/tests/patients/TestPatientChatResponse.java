package com.hrs.cc.api.tests.patients;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.patients.chat.ClinicianChatMessages;
import com.hrs.cc.api.models.patients.chat.Message;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TestPatientChatResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private static String chatId;
    private static String patientId;
    private PropertyWorker propertyWorker;
    private HashMap<Object, Object> data;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.CHAT));
        resPec = restUtilities.getResponseSpecification();
        patientId = Objects.requireNonNull(RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG));
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        reqSpec.body("");
    }

    @Test
    public void testPostNewMessageToChat() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        data = new HashMap<>();
        data.put("msg", "Test message ".concat(RandomData.getRandomString(200)));
        reqSpec.body(data);

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath jsonPath = response.jsonPath();
        chatId = jsonPath.get("chatid");
        if(chatId.isEmpty()) throw new NullPointerException("Chat id is null");

        assertNotNull(jsonPath.get("chatid"), "chatid field is null");
        assertNotNull(jsonPath.get("time"), "time field is null");
    }

    @Test(dependsOnMethods = {"testPostNewMessageToChat"})
    public void testGetAllMessagesOnChat() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtilities.getResponse(reqSpec, "get");
        ClinicianChatMessages messages = response.as(ClinicianChatMessages.class);
        assertNotNull(messages.getTitle(), "title is null");
        assertEquals(messages.getChatid(),chatId, "data not eql");

        messages.getChatdata().forEach((key, value )->{
            assertNotNull(value.getName(), "Name field is null");
            assertNotNull(value.getType(), "Type field is null");
            assertNotNull(value.getTime(), "Time field is null");
            assertNotNull(value.getTs(), "Ts field is null");
        });

        Map.Entry<String, Message> message = messages.getChatdata().entrySet().stream().filter(m -> m.getValue().getText().equals(data.get("msg"))).findFirst().get();
        assertEquals(message.getValue().getName(),"You", "data not eql");
        assertEquals(message.getValue().getType(),"clinician", "data not eql");
        assertEquals(message.getValue().getText(),data.get("msg"), "data not eql");
    }


}
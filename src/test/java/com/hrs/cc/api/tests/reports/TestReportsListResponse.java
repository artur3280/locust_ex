package com.hrs.cc.api.tests.reports;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.patients.Patients;
import com.hrs.cc.api.models.reports.statuses.ReportsStatusesResponse;
import core.PropertyWorker;
import core.rest.RestUtilities;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestReportsListResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private ResponseSpecBuilder builder = new ResponseSpecBuilder();
    private List<String> reportIds = new ArrayList<>();
    private PropertyWorker propertyWorker;


    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.REPORT_STATUSES));
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        restUtilities.removeQueryParam(reqSpec, "status[]");
        reqSpec.body("");
    }

    @Test
    public void testGetReportStatusesResponse() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec,"get");
        ReportsStatusesResponse statusesResponse = response.as(ReportsStatusesResponse.class);
        statusesResponse.getData().forEach(status->{
            try {
                if (status.getStatus().equals("finished")) {
                    reportIds.add(status.getId());
                }
            } catch (NullPointerException e) {
                System.out.println("This report is not finished");
            }
            assertNotNull(status.getReportType());
            assertNotNull(status.getStart().getDate());
            assertNotNull(status.getStart().getTimezoneType());
            assertNotNull(status.getStart().getTimezone());
            assertNotNull(status.getStop().getDate());
            assertNotNull(status.getStop().getTimezoneType());
            assertNotNull(status.getStop().getTimezone());
            assertNotNull(status.getDownloaded());
            assertNotNull(status.getType());
        });
    }


    @Test(dependsOnMethods = {"testGetReportStatusesResponse"}, priority = 2)
    public void testGetDownloadReportResponse() {

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.REPORTS)));

        builder.expectContentType("application/zip");
        resPec = builder.build();

        restUtilities.setContentType("application/zip");
        restUtilities.setEndPoint(EndPoints.ID);

        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        try {
            restUtilities.createPathParam(newReqSpec, "id", reportIds.get(0));
            Response response = restUtilities.getResponse(newReqSpec, resPec, "get");
            assertEquals(response.statusCode(), 200);
        }catch (IndexOutOfBoundsException e){
            System.out.println("Your moderator does not have any available reports, please check!");
        }
    }

}

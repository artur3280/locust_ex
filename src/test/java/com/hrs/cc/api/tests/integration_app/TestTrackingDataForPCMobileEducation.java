package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.environment.presets.EnvPresets;
import com.hrs.cc.api.models.patients.education.Education;
import com.hrs.cc.api.models.patients.education.Patientcontent;
import com.hrs.cc.api.models.requests.turn_education_content.SetPatientContent;
import core.PropertyWorker;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestTrackingDataForPCMobileEducation extends Assert {
    private ObjectMapper mapper = new ObjectMapper();
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private PropertyWorker propertyWorker;
    private String deviceId;
    private String patientId;
    private Education education;


    @BeforeTest
    public void setUp() {
        Logger.debugging(false);
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        patientId = RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG);

        education = RestAssistant.educationsContentForPatient(0, true, patientId);
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V1);
        resPec = restUtilities.getResponseSpecification();
    }

    @AfterClass
    public void removeObj() {
        mapper = null;
        reqSpec = null;
        resPec = null;
        restUtilities = null;
        propertyWorker = null;
        System.gc();
        RestAssistant.educationsContentForPatient(0, true, patientId);
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
    }

    @Test
    public void testGetPresetsCCMobile() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ENVIROMENTS.concat(EndPoints.ENV_NAME).concat(EndPoints.PRESETS));
        restUtilities.createPathParam(reqSpec, "name", "QATestingZone");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
        EnvPresets presets = response.as(new TypeRef<EnvPresets>() {
        });

        presets.getData().forEach(preset -> {
            assertNotNull(preset.getRelationships());
            boolean checkRelation = preset.getRelationships().getModules().getData().stream().allMatch(
                    module ->
                            !module.getId().toString().equals("") &&
                                    module.getId() != null &&
                                    module.getResourceType().equals("modules"));
            assertTrue(checkRelation, "Some data is fail: id is empty, \n id is null, \n resource type is not 'modules'");
            assertNotNull(preset.getAttributes().getName(), "name field is null");
            assertEquals(preset.getAttributes().getEnv(), "QATestingZone", "field not eql");
            assertNotNull(preset.getId(), "id field is null");
            assertEquals(preset.getResourceType(), "presets", "field not eql");
        });
    }

    @Test
    public void testPostVideoStatusMetrics() {
        Patientcontent video = education.getData().getPatientAllContent().stream().filter(e -> e.getType().equals("video")).findAny().get();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.VIDEO_STATUS_TRACKING);

        Map<Object, Object> body = new HashMap<>();
        Map<Object, Object> data = new HashMap<>();
        data.put("id", video.getId());

        Map<Object, Object> attributes = new HashMap<>();
        attributes.put("hrsid", patientId);
        attributes.put("time", "1");
        attributes.put("totaltime", video.getDuration());
        attributes.put("ftime", restUtilities.getTimePeriodTS(1).get("current").replace("T", " "));
        attributes.put("source", 1);

        data.put("attributes", attributes);
        body.put("data", data);

        reqSpec.body(body);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();

        assertEquals(path.get("data.id"), video.getId(), "Video id is not eql");
        assertEquals(path.get("data.resourceType"), "videoStatus", "ResourceType id is not eql");
        assertNotNull(path.get("data.attributes.ftime"), "Ftime is null");
        assertNotNull(path.get("data.attributes.totalTime"), "totalTime is null");
        assertNotNull(path.get("data.attributes.time"), "time is null");
        assertNotNull(path.get("data.attributes.source"), "source is null");
        assertNotNull(path.get("data.attributes.category"), "category is null");
    }

    @Test
    public void testPostVideoContentViewedStatus() {
        Patientcontent video = education.getData().getPatientAllContent().stream().filter(e -> e.getType().equals("video")).findAny().get();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.CONTENT_VIEWED_TRACKING);

        Map<Object, Object> body = new HashMap<>();
        Map<Object, Object> data = new HashMap<>();
        data.put("id", video.getId());

        Map<Object, Object> attributes = new HashMap<>();
        attributes.put("hrsid", patientId);
        attributes.put("type", "custom");
        attributes.put("category", "education");
        attributes.put("data", "{\"Test\":\"JSON\"}");
        attributes.put("time", "1");
        attributes.put("ftime", restUtilities.getTimePeriodTS(1).get("current").replace("T", " "));
        attributes.put("source", 1);

        data.put("attributes", attributes);
        body.put("data", data);

        reqSpec.body(body);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();

        assertEquals(path.get("data.id"), video.getId(), "Video id is not eql");
        assertEquals(path.get("data.resourceType"), "contentViewed", "ResourceType id is not eql");
        assertNotNull(path.get("data.attributes.ftime"), "Ftime is null");
        assertNotNull(path.get("data.attributes.data"), "attributes.data is null");
        assertNotNull(path.get("data.attributes.time"), "time is null");
        assertNotNull(path.get("data.attributes.source"), "source is null");
        assertNotNull(path.get("data.attributes.category"), "category is null");
    }

    @Test
    public void testPostQuizStatus() {
        Patientcontent quiz = education.getData().getPatientAllContent().stream().filter(e -> e.getType().equals("quiz")).findAny().get();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.QUIZ_TRACKING);

        Map<Object, Object> body = new HashMap<>();
        Map<Object, Object> data = new HashMap<>();
        data.put("id", patientId);


        Map<Object, Object> attributes = new HashMap<>();
        attributes.put("quiz", quiz.getTitle());
        attributes.put("ftime", restUtilities.getTimePeriodTS(1).get("current").replace("T", " "));
        attributes.put("source", 1);

        data.put("attributes", attributes);
        body.put("data", data);

        reqSpec.body(body);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();

        assertEquals(path.get("data.id"), patientId, "Patient id is not eql");
        assertEquals(path.get("data.resourceType"), "quiz", "ResourceType id is not eql quiz");
        assertEquals(path.get("data.attributes.quiz"), quiz.getTitle(), "quiz.getTitle is not eql");
        assertNotNull(path.get("data.attributes.time"), "time is null");
        assertNotNull(path.get("data.attributes.source"), "source is null");
    }

    @Test
    public void testPostQuizAnswersStatus() {
        Patientcontent quiz = education.getData().getPatientAllContent().stream().filter(e -> e.getType().equals("quiz")).findAny().get();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.QUIZ_ANSWER_TRACKING);

        Map<Object, Object> body = new HashMap<>();
        Map<Object, Object> data = new HashMap<>();
        data.put("id", quiz.getId());

        Map<Object, Object> attributes = new HashMap<>();
        attributes.put("hrsid", patientId);
        attributes.put("questionnumber", "1");
        attributes.put("question", quiz.getTitle());
        attributes.put("answer", "{\"answerinfo\":{\"correct\": [\"1\", \"2\", \"3\"], \"selected\": [\"3\"]}}");
        attributes.put("ftime", restUtilities.getTimePeriodTS(1).get("current").replace("T", " "));
        attributes.put("source", 1);

        data.put("attributes", attributes);
        body.put("data", data);

        reqSpec.body(body);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");

        JsonPath path = response.jsonPath();

        assertEquals(path.get("data.id"), quiz.getId(), "QUIZ id is not eql");
        assertEquals(path.get("data.resourceType"), "quizanswer", "ResourceType id is not eql");
        assertNotNull(path.get("data.attributes.ftime"), "Ftime is null");
        assertNotNull(path.get("data.attributes.questionnumber"), "attributes.questionnumber is null");
        assertNotNull(path.get("data.attributes.question"), "question is null");
        assertNotNull(path.get("data.attributes.source"), "source is null");
        assertNotNull(path.get("data.attributes.answer"), "answer is null");
    }
}
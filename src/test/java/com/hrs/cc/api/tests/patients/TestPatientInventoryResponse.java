package com.hrs.cc.api.tests.patients;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.inventory.InventoryResponse;
import com.hrs.cc.api.models.inventory.device_history.DeviceHistory;
import com.hrs.cc.api.models.patients.Patients;
import core.PropertyWorker;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

public class TestPatientInventoryResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private String deviceId;
    private PropertyWorker propertyWorker;
    private String patientId;


    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));
        resPec = restUtilities.getResponseSpecification();
        deviceId = RestAssistant.getDeviceId().getDevid();
        patientId = Objects.requireNonNull(RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG));
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
        reqSpec.body("");
    }

    @Test
    public void testSetDeviceToPatient() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.DEVICE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        queryParam.put("deviceId", deviceId);
        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
        JsonPath jsonPath = response.jsonPath();
        assertEquals(jsonPath.get("status"), "ok");

        queryParam.clear();
        reqSpec.body("");
        restUtilities.removePathParam(reqSpec, "deviceId");
        restUtilities.removePathParam(reqSpec, "id");
    }


    @Test(dependsOnMethods = {"testSetDeviceToPatient"})
    public void testGetDeviceHistoryToPatient() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.DEVICES));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "get");

        DeviceHistory history = response.as(DeviceHistory.class);
        assertNotNull(history.getData(), "history data is null");
        assertEquals(history.getStatus(), "ok");
        history.getData().forEach(data -> {
            assertNotNull(data.getAssignmentDate(), "AssignmentDate field is null");
            assertNotNull(data.getDevice(), "Device field is null");
            assertNotNull(data.getDevice().getName(), "Device.Name field is null");
            if (data.getDevice().getId().equals(deviceId)) {
                assertNotNull(data.getAssignmentDate(), "AssignmentDate field is null");
                assertNotNull(data.getDevice(), "Device field is null");
                assertNotNull(data.getDevice().getName(), "Device.Name field is null");
                assertEquals(data.getDevice().getId(), deviceId, "device id not eql");
            }
        });
        restUtilities.removePathParam(reqSpec, "id");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testSetDeviceToPatient", "testGetDeviceHistoryToPatient"}, priority = 5)
    public void testRemoteDeviceToPatient() {
        Map<String, String> params = new HashMap<>();
        params.put("deviceId", deviceId);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.DEVICE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "delete");
        assertEquals(response.getStatusCode(), 200);

        reqSpec.body("");
        restUtilities.removePathParam(reqSpec, "deviceId");
        restUtilities.removePathParam(reqSpec, "id");
    }
}
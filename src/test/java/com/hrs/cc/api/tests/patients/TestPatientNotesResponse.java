package com.hrs.cc.api.tests.patients;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.patients.notes.Notes;
import com.hrs.cc.api.models.patients.notes.history.HistoryNote;
import com.hrs.cc.api.models.requests.set_patient_note.*;
import core.PropertyWorker;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class TestPatientNotesResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private String newPatientId;
    private List<String> createdNotesId;
    private List<Object> changedNoteModels;
    private PropertyWorker propertyWorker;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.NOTES));
        resPec = restUtilities.getResponseSpecification();
        newPatientId = Objects.requireNonNull(RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG));
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        reqSpec.body("");
    }

    @Test
    public void testSetNewNotes() {
        createdNotesId = new ArrayList<>();

        for (int i = 0; i < 10; i++) {

            Note note = new Note();

            Readmission readmission = new Readmission();
            readmission.setDateObject(LocalDate.now().toString().replace("-", "/"));
            readmission.setLocation(restUtilities.getRandomString().concat(" test location"));
            readmission.setLos(restUtilities.getRandomInt(0, 25));

            Edvisit edvisit = new Edvisit();
            edvisit.setDateObject(LocalDate.now().toString().replace("-", "/"));
            edvisit.setLocation(restUtilities.getRandomString().concat(" test location"));
            edvisit.setLos(restUtilities.getRandomInt(0, 25));

            Observation observation = new Observation();
            observation.setDateObject(LocalDate.now().toString().replace("-", "/"));
            observation.setLocation(restUtilities.getRandomString().concat(" test location"));
            observation.setLos(restUtilities.getRandomInt(0, 25));

            note.setReadmission(readmission);
            note.setEdvisit(edvisit);
            note.setObservation(observation);

            note.setNote(restUtilities.getRandomString().concat(" test note"));

            Communication communication = new Communication();
            communication.setType("video");
            List<String> result = new ArrayList<>();
            result.add("Call MD");
            result.add("Education Given");
            result.add("Email to Visiting Physician/Nurse Practioner");
            result.add("Email to Case Managing RN");
            List<String> followup = new ArrayList<>();
            followup.add("Email Clinical Director-Email To Transitional Grand Aides Supervisor");
            followup.add("Email to Case Managing RN");
            followup.add("Email to Visiting Physician/Nurse Practioner");
            followup.add("PRN visit-MD Order Received");
            List<String> itproblems = new ArrayList<>();
            itproblems.add("Equipment Failure");
            itproblems.add("Transmission Issue");
            itproblems.add("Bluetooth didn't Connect");
            communication.setResult(result);
            communication.setFollowup(followup);
            communication.setItproblems(itproblems);

            note.setCommunication(communication);
            note.setEnrollment(new Enrollment());

            reqSpec.body(note);
            restUtilities.setContentType(ContentType.JSON);
            restUtilities.setEndPoint(EndPoints.PATIENT.concat(EndPoints.ID));
            reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

            Response response = restUtilities.getResponse(
                    restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
            JsonPath jsonPath = response.jsonPath();

            assertEquals(jsonPath.get("createdNote.clinician.name.first"), Auth.FIRST_NAME, "createdNote.clinician.name.first field not eql");
            assertEquals(jsonPath.get("createdNote.clinician.name.last"), Auth.LAST_NAME, "createdNote.clinician.name.last");
            assertEquals(jsonPath.get("createdNote.created.timezone"), "UTC", "createdNote.created.timezone");

            assertNotNull(jsonPath.get("noteId"), "noteId is null");
            createdNotesId.add(jsonPath.get("noteId"));

            restUtilities.removePathParam(reqSpec, "id");
            restUtilities.removeHeaders(reqSpec, "Authorization");
            reqSpec.body("");
        }
    }

    @Test(dependsOnMethods = {"testSetNewNotes"})
    public void testGetLast10PatientNotes() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createQueryParam(reqSpec, "limit", "10");
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "get");

        List<Notes> notesList = Arrays.asList(response.as(Notes[].class));
        assertEquals(notesList.size(), 10);
        notesList.forEach(notes -> {
            assertNotNull(notes.getId(), "Id field is null");
            assertNotNull(notes.getPatient(), "Patient field is null");
            assertNotNull(notes.getClinician(), "Clinician field is null");
            assertNotNull(notes.getClinician().getName(), "Clinician.Name field is null");
            assertNotNull(notes.getClinician().getName().getClinician(), "Clinician.Name.Clinician field is null");
            assertNotNull(notes.getClinician().getName().getFirst(), "Clinician.Name.First field is null");
            assertNotNull(notes.getClinician().getName().getLast(), "Clinician.Name.Last field is null");
            assertNotNull(notes.getClinician().getName().getMiddle(), "Clinician.Name.Middle field is null");
            assertNotNull(notes.getNote(), "Note field is null");
            assertNotNull(notes.getCreated(), "Created field is null");
            assertNotNull(notes.getCreated().getDate(), "Created.Date field is null");
            assertNotNull(notes.getCreated().getTimezone(), "Created.Timezone field is null");
            assertEquals(notes.getCreated().getTimezone(), "UTC", "timezone not UTC");
            assertNotNull(notes.getCreated().getTimezoneType(), "Created.TimezoneType field is null");
            assertNotNull(notes.getStatus(), "Status field is null");
            assertNotNull(notes.getTime(), "Time field is null");

            try {
                assertNotNull(notes.getReadmission(), "Readmission field is null");
                assertNotNull(notes.getReadmission().getLos(), "Readmission.Los field is null");
            } catch (AssertionError e) {
                System.out.println("Some fields does not exist in Readmission");
            }

            try {
                assertNotNull(notes.getObservation(), "Observation field is null");
                assertNotNull(notes.getObservation().getDate(), "Observation.Date field is null");
                assertNotNull(notes.getObservation().getLocation(), "Observation.Location field is null");
                assertNotNull(notes.getObservation().getLos(), "Observation.Los field is null");
            } catch (AssertionError e) {
                System.out.println("Some fields does not exist in Observation");
            }

            try {
                assertNotNull(notes.getEdvisit(), "Edvisit field is null");
                assertNotNull(notes.getEdvisit().getDate(), "Edvisit.Date field is null");
                assertNotNull(notes.getEdvisit().getLocation(), "Edvisit.getLocation field is null");
                assertNotNull(notes.getEdvisit().getLos(), "Edvisit.Los field is null");
            } catch (AssertionError e) {
                System.out.println("Some fields does not exist in Edvisit");
            }

            try {
                assertNotNull(notes.getCommunication(), "Communication field is null");
                assertNotNull(notes.getCommunication().getFollowup(), "Communication.Followup field is null");
                assertNotNull(notes.getCommunication().getItproblems(), "Communication.Itproblems field is null");
                assertNotNull(notes.getCommunication().getResult(), "Communication.Result field is null");
                assertNotNull(notes.getCommunication().getType(), "Communication.Type field is null");
            } catch (AssertionError e) {
                System.out.println("Some fields does not exist in Communication");
            }

        });

        restUtilities.removePathParam(reqSpec, "id");
        restUtilities.removeQueryParam(reqSpec, "limit");
        reqSpec.body("");
    }

    @Test(dependsOnMethods = {"testSetNewNotes"})
    public void testEditCreatedNote() {
        changedNoteModels = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            Note note = new Note();

            Readmission readmission = new Readmission();
            readmission.setDateObject(LocalDate.now().toString().replace("-", "/"));
            readmission.setLocation(restUtilities.getRandomString().concat(" Changed test location"));
            readmission.setLos(restUtilities.getRandomInt(0, 25));

            Edvisit edvisit = new Edvisit();
            edvisit.setDateObject(LocalDate.now().toString().replace("-", "/"));
            edvisit.setLocation(restUtilities.getRandomString().concat(" Changed test location"));
            edvisit.setLos(restUtilities.getRandomInt(0, 25));

            Observation observation = new Observation();
            observation.setDateObject(LocalDate.now().toString().replace("-", "/"));
            observation.setLocation(restUtilities.getRandomString().concat("Changed test location"));
            observation.setLos(restUtilities.getRandomInt(0, 25));

            note.setReadmission(readmission);
            note.setEdvisit(edvisit);
            note.setObservation(observation);

            note.setNote(restUtilities.getRandomString().concat(" Changed test note"));

            Communication communication = new Communication();
            communication.setType("Phone Call");
            List<String> result = new ArrayList<>();
            result.add("Call MD");
            result.add("Education Given");
            List<String> followup = new ArrayList<>();
            followup.add("Email Clinical Director-Email To Transitional Grand Aides Supervisor");
            List<String> itproblems = new ArrayList<>();
            itproblems.add("Equipment Failure");
            communication.setResult(result);
            communication.setFollowup(followup);
            communication.setItproblems(itproblems);
            note.setCommunication(communication);
            note.setEnrollment(new Enrollment());
            reqSpec.body(note);
            changedNoteModels.add(note);

            restUtilities.setContentType(ContentType.JSON);
            restUtilities.setEndPoint(EndPoints.NOTE_ID.concat(EndPoints.PATIENT).concat(EndPoints.ID));
            restUtilities.createPathParam(reqSpec, "noteId", createdNotesId.get(i));
            reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

            Response response = restUtilities.getResponse(
                    restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");

            JsonPath jsonPath = response.jsonPath();

            assertEquals(jsonPath.get("createdNote.clinician.name.first"), Auth.FIRST_NAME, "createdNote.clinician.name.first field not eql" + Auth.FIRST_NAME);
            assertEquals(jsonPath.get("createdNote.clinician.name.last"), Auth.LAST_NAME, "createdNote.clinician.name.last field not eql" + Auth.LAST_NAME);
            assertEquals(jsonPath.get("createdNote.created.timezone"), "UTC", "createdNote.created.timezone field not eql" + "UTC");

            assertEquals(jsonPath.get("createdNote.id").toString(), createdNotesId.get(i), "createdNote.id field not eql " + createdNotesId.get(i));

            assertEquals(response.getStatusCode(), 200);

            restUtilities.removeHeaders(reqSpec, "Authorization");
            restUtilities.removePathParam(reqSpec, "id");
            restUtilities.removePathParam(reqSpec, "noteId");
            reqSpec.body("");
        }
    }

    @Test(dependsOnMethods = {"testSetNewNotes", "testEditCreatedNote"})
    public void testGetHistoryChangedNote() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.NOTE_ID.concat(EndPoints.PATIENT).concat(EndPoints.ID).concat(EndPoints.HISTORY));
        restUtilities.createPathParam(reqSpec, "noteId", createdNotesId.get(0));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "get");
        List<HistoryNote> historyNotes = Arrays.asList(response.as(HistoryNote[].class));

        historyNotes.forEach(history -> {
            assertNotNull(history.getClinician(), "Clinician field s null");
            assertNotNull(history.getClinician().getClinician(), "Clinician.Clinician field s null");
            assertNotNull(history.getClinician().getFirst(), "Clinician.First field s null");
            assertNotNull(history.getClinician().getLast(), "Clinician.Last field s null");
            assertNotNull(history.getClinician().getMiddle(), "Clinician.Middle field s null");
            assertNotNull(history.getReadmission(), "Readmission field s null");
            assertNotNull(history.getReadmission().getId(), "Readmission.Id field s null");
            assertNotNull(history.getReadmission().getPatient(), "Readmission.Patient field s null");
            assertNotNull(history.getReadmission().getNoteHistoryId(), "Readmission.NoteHistoryId field s null");
            assertNotNull(history.getReadmission().getLocation(), "Readmission.Location field s null");
            assertNotNull(history.getReadmission().getLastUpdated(), "Readmission.LastUpdated field s null");
            assertNotNull(history.getEdvisit(), "Edvisit field s null");
            assertNotNull(history.getEdvisit().getId(), "Edvisit.Id field s null");
            assertNotNull(history.getEdvisit().getPatient(), "Edvisit.Patient field s null");
            assertNotNull(history.getEdvisit().getNoteHistoryId(), "Edvisit.NoteHistoryId field s null");
            assertNotNull(history.getEdvisit().getLocation(), "Edvisit.Location field s null");
            assertNotNull(history.getEdvisit().getLastUpdated(), "Edvisit.LastUpdated field s null");
            assertNotNull(history.getObservation(), "Observation field s null");
            assertNotNull(history.getObservation().getId(), "Observation.Id field s null");
            assertNotNull(history.getObservation().getPatient(), "Observation.Patient field s null");
            assertNotNull(history.getObservation().getNoteHistoryId(), "Observation.NoteHistoryId field s null");
            assertNotNull(history.getObservation().getLocation(), "Observation.Location field s null");
            assertNotNull(history.getObservation().getLastUpdated(), "Observation.LastUpdated field s null");

            assertNotNull(history.getTimespan(), "Timespan field s null");
            assertNotNull(history.getTimespan().getId(), "Timespan.Id field s null");
            assertNotNull(history.getTimespan().getNoteHistoryId(), "Timespan.NoteHistoryId field s null");
            assertNotNull(history.getTimespan().getTimeSpent(), "Timespan.TimeSpent field s null");
            assertNotNull(history.getId(), "Id field s null");
            assertNotNull(history.getNoteId(), "NoteId field s null");
            assertNotNull(history.getNote(), "Note field s null");
            assertNotNull(history.getPatient(), "Patient field s null");
            assertNotNull(history.getInsertTime(), "InsertTime field s null");
            assertNotNull(history.getInsertTime().getDate(), "InsertTime.Date field s null");
            assertNotNull(history.getInsertTime().getTimezone(), "InsertTime.Timezone field s null");
            assertEquals(history.getInsertTime().getTimezone(), "UTC", "timezone not eql UTC");
            assertNotNull(history.getInsertTime().getTimezoneType(), "InsertTime.TimezoneType field s null");
        });
        restUtilities.removePathParam(reqSpec, "id");
        restUtilities.removePathParam(reqSpec, "noteId");
        reqSpec.body("");
    }

    @Test(dependsOnMethods = {"testSetNewNotes", "testEditCreatedNote"}, priority = 5)
    public void testRemoveAllNotes() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.NOTE_ID.concat(EndPoints.PATIENT).concat(EndPoints.ID));

        restUtilities.createPathParam(reqSpec, "noteId", createdNotesId.get(0));
        restUtilities.createQueryParam(reqSpec, "reason", "Remove reason ".concat(restUtilities.getRandomString()));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");

        JsonPath path = response.jsonPath();
        assertEquals(path.get("id"), createdNotesId.get(0));
        assertEquals(path.get("status"), "deleted");

        /*Check all notes*/
        restUtilities.removeQueryParam(reqSpec, "reason");
        restUtilities.removePathParam(reqSpec, "noteId");
        reqSpec.body("");

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")))
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.NOTES)));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT.concat(EndPoints.ID));
        restUtilities.createQueryParam(newReqSpec, "limit", "10");
        Response checker = restUtilities.getResponse(
                restUtilities.createPathParam(newReqSpec, "id", newPatientId), "get");

        List<Notes> notesList = Arrays.asList(checker.as(Notes[].class));
        assertEquals(notesList.size(), 10);
        assertTrue(
                notesList
                        .stream()
                        .anyMatch(
                                note -> !String.valueOf(note.getId()).equals(createdNotesId.get(0))), "Some note id eql " + createdNotesId.get(0));

    }

    @Test(dependsOnMethods = {"testSetNewNotes",
            "testEditCreatedNote",
            "testRemoveAllNotes"}, priority = 5)
    public void testRestoreFirstNotes() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.NOTE_ID.concat(EndPoints.PATIENT).concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "noteId", createdNotesId.get(0));
        restUtilities.createQueryParam(reqSpec, "reason", "Remove reason ".concat(restUtilities.getRandomString()));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "patch");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("id"), createdNotesId.get(0), "id not eql" + createdNotesId.get(0));
        assertEquals(path.get("status"), "edited", "status not eql " + "edited");

        restUtilities.removeQueryParam(reqSpec, "reason");
        restUtilities.removePathParam(reqSpec, "noteId");
        restUtilities.removePathParam(reqSpec, "id");
        restUtilities.setContentType(ContentType.JSON);
        reqSpec.body("");
    }
}
package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.*;
import com.hrs.cc.api.models.integratioins_app.connected_devices.DataDevices;
import com.hrs.cc.api.models.integratioins_app.connected_devices.Devices;
import com.hrs.cc.api.models.integratioins_app.files_education_content.FilesEducationContent;
import com.hrs.cc.api.models.integratioins_app.patient_caregiver.PatientsCaregiver;
import com.hrs.cc.api.models.integratioins_app.patient_educations_files.PatientEducationFiles;
import com.hrs.cc.api.models.integratioins_app.patient_status.PatientState;
import com.hrs.cc.api.models.integratioins_app.patient_tasks.DatumTask;
import com.hrs.cc.api.models.integratioins_app.patient_tasks.PatientTasksApp;
import com.hrs.cc.api.models.integratioins_app.patient_tasks_v1.Choice;
import com.hrs.cc.api.models.integratioins_app.patient_tasks_v1.PatientTasksV1;
import com.hrs.cc.api.models.integratioins_app.quizzes.PatientQuizzes;
import com.hrs.cc.api.models.integratioins_app.stethoscope_data.StethoscopeHistoryData;
import com.hrs.cc.api.models.patients.metrics_data.*;
import com.hrs.cc.api.models.patients.metrics_data.uploaded_image_response.UploadedImageResponse;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.activity_manual.ActivityManual;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.activity_manual.DataActivity;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.activity_manual.FacetActivity;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.blood_pressure_manual.BloodPressureManual;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.blood_pressure_manual.DataBP;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.blood_pressure_manual.FacetBP;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.glucose_manual.DataGlucose;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.glucose_manual.FacetGlucose;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.glucose_manual.GlucoseManual;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.image_manual.DataImage;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.image_manual.FacetImage;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.image_manual.ImageManual;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_all_fields.DataMedicationAllFields;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_all_fields.FacetMedicationAllFields;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_all_fields.MedicationAllFields;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_manual.DataMedication;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_manual.FacetMedication;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_manual.MedicationManual;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.pulseox_manual.DataPulseOx;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.pulseox_manual.FacetPulseOx;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.pulseox_manual.PulseOxManual;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.survey_manual.DataSurvey;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.survey_manual.FacetSurvey;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.survey_manual.SurveyManual;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.temperature_manual.DataTemperature;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.temperature_manual.FacetTemperature;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.temperature_manual.TemperatureManual;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.weight_manual.DataWeight;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.weight_manual.Facet;
import com.hrs.cc.api.models.requests.add_metrics_integreation_app.weight_manual.WeightManual;
import com.hrs.cc.api.models.requests.generation_magic_codes.Data;
import com.hrs.cc.api.models.requests.generation_magic_codes.MagiCodeIdentityPacient;
import com.hrs.cc.api.models.requests.request_magic_code_to_app.Datum;
import com.hrs.cc.api.models.requests.request_magic_code_to_app.MagiCodePacientDevice;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.glassfish.grizzly.http.util.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class TestIntegrationWithAppClient extends Assert {
    private ObjectMapper mapper = new ObjectMapper().enable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private static String pacientHrsId;
    private PropertyWorker propertyWorker;
    private DatumTask glucoseData;
    private DatumTask activityData;
    private DatumTask woundimagingData;
    private DatumTask temperatureData;
    private DatumTask weightData;
    private List<DatumTask> medicationData;
    private DatumTask bloodpressureData;
    private DatumTask pulseoxData;
    private List<DatumTask> surveyData;
    private Devices devices;
    private static final String APPLE_CODE = "9B8C7D";
    private static String appleDeviceCode;
    private DatumTask stepsData;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
        Logger.debugging(false);

    }

    @BeforeClass
    public void setUpNext() {
        pacientHrsId = RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG);
        RestAssistant.setModulesToPatientById(pacientHrsId);
        RestAssistant.setRisksToPatientById(pacientHrsId);

        try {
            propertyWorker.setProperty("patient_hrs_id", pacientHrsId);
        } catch (IOException e) {
            e.printStackTrace();
        }

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath("");
    }

    @AfterClass
    public void removeObj() {
        mapper = null;
        reqSpec = null;
        resPec = null;
        restUtilities = null;
        queryParam = null;
        userData = null;
        pacientHrsId = null;
        propertyWorker = null;
        glucoseData = null;
        activityData = null;
        woundimagingData = null;
        temperatureData = null;
        weightData = null;
        medicationData = null;
        bloodpressureData = null;
        pulseoxData = null;
        surveyData = null;
        devices = null;
        System.gc();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        restUtilities.removeQueryParam(reqSpec, "filter[patient]");
        restUtilities.removeQueryParam(reqSpec, "filter[hrsid]");
        restUtilities.removeQueryParam(reqSpec, "sideload");
        restUtilities.removeQueryParam(reqSpec, "filter[gid]");
        restUtilities.removeQueryParam(reqSpec, "filter[identity]");
        restUtilities.removeQueryParam(reqSpec, "start");
        restUtilities.removeQueryParam(reqSpec, "end");

        queryParam.clear();
        reqSpec.body("");
    }

    @Test
    public void testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded() {
        RestAssistant.setByodLicenseMax(1);
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(403));

        MagiCodeIdentityPacient code = new MagiCodeIdentityPacient();
        Data data = new Data();
        data.setIdentity(pacientHrsId);
        code.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.MAGIC_CODES);

        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.body(code);

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath jsonPath = response.jsonPath();
        assertEquals(jsonPath.get("message").toString(), "Limit For Concurrent Login Codes Exceeded", "An error messages are not eql");

        RestAssistant.removeEnvSettings(EnvFlags.SYSTEM_BYOD_LICENSE_MAX.get());
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200));
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded"})
    public void testGenerationMagiCodeForPatientApp() {
        RestAssistant.removeEnvSettings(EnvFlags.SYSTEM_BYOD_LICENSE_MAX.get());

        MagiCodeIdentityPacient code = new MagiCodeIdentityPacient();
        Data data = new Data();
        data.setIdentity(pacientHrsId);
        code.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.MAGIC_CODES);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.body(code);

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath jsonPath = response.jsonPath();
        assertNotNull(jsonPath.get("data").toString(), "data field is null");
        assertNotNull(jsonPath.get("data.code").toString(), "data.code field is null");
        assertFalse(jsonPath.get("data.code").toString().isEmpty(), "data.code is empty");

        try {
            propertyWorker.setProperty("patient_magic_code", RandomData.getSensitiveString(jsonPath.get("data.code").toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp"})
    public void testConnectionMagiCodeForPatientDevice() {
        MagiCodePacientDevice code = new MagiCodePacientDevice();
        Datum data = new Datum();
        data.setType("code");
        data.setCode(propertyWorker.getProperty("patient_magic_code"));
        code.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TOKENS_INTG);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.body(code);

        Response response = restUtilities.getResponse(reqSpec, "post");

        JsonPath jsonPath = response.jsonPath();
        assertNotNull(jsonPath.get("data").toString(), "data field is null");
        assertNotNull(jsonPath.get("data.token").toString(), "data.token field is null");
        assertFalse(jsonPath.get("data.token").toString().isEmpty(), "data.token field is empty");
        assertNull(jsonPath.get("data.refresh"), "data.refresh field is null");

        try {
            propertyWorker.setProperty("patient_device_token", jsonPath.get("data.token"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice"})
    public void testGetPatientTasksAppResponse() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TASKS_INTG);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
        reqSpec.urlEncodingEnabled(false);
        Response response = restUtilities.getResponse(
                restUtilities.createQueryParam(
                        reqSpec,
                        "filter[patient]",
                        propertyWorker.getProperty("patient_hrs_id")), "get");
        PatientTasksApp tasksApp = response.as(PatientTasksApp.class);
        assertNotNull(tasksApp.getData());

        glucoseData = tasksApp.getData().stream().filter(datum -> datum.getType().equals("glucose")).collect(Collectors.toList()).get(0);
        assertFalse(glucoseData.getId().isEmpty());
        assertEquals(glucoseData.getType(), "glucose");
        try {
            assertNotNull(glucoseData.getSchedule().getReminder(), "glucose reminder is null ");
            assertFalse(glucoseData.getGlucose().toString().isEmpty(), "glucose is empty");
            assertNotNull(glucoseData.getData(), "glucose.data is null");
            glucoseData.getData().forEach(gData -> {
                assertFalse(gData.getId().isEmpty(), "Id is empty");
                assertFalse(gData.getType().isEmpty(), "Type is empty");
                assertFalse(gData.getGlucose().toString().isEmpty(), "Glucose is empty");
                assertFalse(gData.getTs().toString().isEmpty(), "Ts is empty");
                assertFalse(gData.getStatus().isEmpty(), "Status is empty");
            });
            assertFalse(glucoseData.getTs().toString().isEmpty());
        } catch (NullPointerException e) {
            System.out.println("Glucose data is empty");
        }

        activityData = tasksApp.getData().stream().filter(datum -> datum.getType().equals("activity")).collect(Collectors.toList()).get(0);
        assertFalse(activityData.getId().isEmpty(), "activity data id is empty");
        assertEquals(activityData.getType(), "activity", "type not eql to activity");
        try {
            assertFalse(activityData.getDuration().toString().isEmpty(), "activity duration is empty");
            assertFalse(activityData.getAction().getGoal().toString().isEmpty(), "activity action goal is empty");
            assertNotNull(activityData.getData());
            activityData.getData().forEach(aData -> {
                assertFalse(aData.getId().isEmpty(), "Id field is null");
                assertFalse(aData.getType().isEmpty(), "Type field is null");
                assertFalse(aData.getDuration().toString().isEmpty(), "Duration field is null");
                assertFalse(aData.getTs().toString().isEmpty(), "Ts field is null");
                assertFalse(aData.getStatus().isEmpty(), "Status field is null");
            });
            assertFalse(activityData.getTs().toString().isEmpty());
        } catch (NullPointerException e) {
            System.out.println("Activity data is empty");
        }

        woundimagingData = tasksApp.getData().stream().filter(datum -> datum.getType().equals("woundimaging")).collect(Collectors.toList()).get(0);
        assertFalse(woundimagingData.getId().isEmpty());
        assertEquals(woundimagingData.getType(), "woundimaging", "current type is not 'woundimaging'");

        stepsData = tasksApp.getData().stream().filter(datum -> datum.getType().equals("steps")).collect(Collectors.toList()).get(0);
        assertFalse(stepsData.getId().isEmpty(), "steps data is empty");
        assertEquals(stepsData.getType(), "steps", "current type is not 'steps");

        temperatureData = tasksApp.getData().stream().filter(datum -> datum.getType().equals("temperature")).collect(Collectors.toList()).get(0);
        assertFalse(temperatureData.getId().isEmpty(), "temperature id is empty");
        assertEquals(temperatureData.getType(), "temperature", "current type is not temperature");
        try {
            temperatureData.getData().forEach(tData -> {
                assertFalse(tData.getId().isEmpty(), "Id field is empty");
                assertFalse(tData.getType().isEmpty(), "Type field is empty");
                assertNotNull(tData.getUnit(), "Unit field is null");
                assertFalse(tData.getTemperature().toString().isEmpty(), "Temperature field is empty");
                assertFalse(tData.getTs().toString().isEmpty(), "Ts field is empty");
                assertFalse(tData.getStatus().isEmpty(), "Status field is empty");
            });
            assertFalse(temperatureData.getTemperature().toString().isEmpty(), "temperature is empty");
            assertFalse(temperatureData.getTs().toString().isEmpty(), "ts field is empty");
        } catch (NullPointerException e) {
            System.out.println("Temperature data is empty");
        }

        weightData = tasksApp.getData().stream().filter(datum -> datum.getType().equals("weight")).collect(Collectors.toList()).get(0);
        assertFalse(weightData.getId().isEmpty(), "weight id is empty");
        assertEquals(weightData.getType(), "weight", "current type is not weight");
        assertNotNull(weightData.getSchedule().getReminder(), "weight reminder is null");
        try {
            assertFalse(weightData.getWindow().toString().isEmpty());
            assertFalse(weightData.getTs().toString().isEmpty());
            assertNotNull(weightData.getData());
            weightData.getData().forEach(wData -> {
                assertFalse(wData.getId().isEmpty(), "Id is Empty");
                assertFalse(wData.getType().isEmpty(), "Type is Empty");
                assertNotNull(wData.getWeight(), "Weight is null");
                assertFalse(wData.getTs().toString().isEmpty(), "Ts is Empty");
                assertFalse(wData.getStatus().isEmpty(), "Status is Empty");
            });
            assertFalse(weightData.getSchedule().getAt().isEmpty());
        } catch (NullPointerException e) {
            System.out.println("Weight data is empty");
        }

        medicationData = tasksApp.getData().stream().filter(datum -> datum.getType().equals("medication")).collect(Collectors.toList());
        medicationData.forEach(medData -> {
            assertNotNull(medData.getSchedule().getReminder(), "medication reminder is null ");

            assertFalse(medData.getId().isEmpty(), "medication id is empty");
            assertEquals(medData.getType(), "medication", "current type is not eql to 'medication'");

            assertFalse(medData.getAction().getName().isEmpty(), "Name field is Empty");
            assertFalse(medData.getAction().getMedicationId().isEmpty(), "MedicationId field is Empty");
            assertFalse(medData.getAction().getDosage().isEmpty(), "Dosage field is Empty");
            assertFalse(medData.getAction().getStrength().toString().isEmpty(), "Strength().toString field is Empty");
            assertFalse(medData.getAction().getUnits().isEmpty(), "Units field is Empty");
            assertFalse(medData.getAction().getCount().toString().isEmpty(), "Count field is Empty");
            assertFalse(medData.getAction().getRoute().isEmpty(), "Route field is Empty");

            assertNotNull(medData.getSchedule().getEvery(), "every field is null");
            assertFalse(medData.getSchedule().getEvery().getMeasure().isEmpty(), "Measure field is empty");
            assertFalse(medData.getSchedule().getAt().isEmpty(), "Schedule at field  is empty");

            assertFalse(medData.getInstruction().isEmpty(), "med instruction is empty");
        });

        DatumTask everyxdays = medicationData.stream().filter(mD -> mD.getAction().getName().equals("LIQUAEMIN SODIUM")).collect(Collectors.toList()).get(0);
        assertEquals(everyxdays.getSchedule().getEvery().getMeasure(), "nthDay", "not eqls to nthDay ");
        assertNotNull(everyxdays.getSchedule().getStart(), "Start field is null");
        assertNotNull(everyxdays.getSchedule().getEnd(), "End field is null");
        assertNotNull(everyxdays.getSchedule().getEvery(), "Every field is null");

        DatumTask dayOfWeek = medicationData.stream().filter(mD -> mD.getAction().getName().equals("ALCOHOL 10% AND DEXTROSE 5%")).collect(Collectors.toList()).get(0);
        assertEquals(dayOfWeek.getSchedule().getEvery().getMeasure(), "dayOfWeek", "Measure field  is not eql");
        assertEquals(dayOfWeek.getSchedule().getEvery().getUnits().toString(), "[Sunday, Monday, Tuesday, Wednesday, Thursday]", "Units array is not eql");
        assertNotNull(dayOfWeek.getSchedule().getEnd(), "end schedule is not eql");
        assertNotNull(dayOfWeek.getSchedule().getEvery(), "Every schedule is not eql");

        bloodpressureData = tasksApp.getData().stream().filter(datum -> datum.getType().equals("bloodpressure")).collect(Collectors.toList()).get(0);
        assertFalse(bloodpressureData.getId().isEmpty(), "bloodpressure is empty");
        assertEquals(bloodpressureData.getType(), "bloodpressure", "current type is not eqls");
        try {
            assertFalse(bloodpressureData.getSystolic().toString().isEmpty(), "bloodpressure systolic is empty");
            assertFalse(bloodpressureData.getDiastolic().toString().isEmpty(), "bloodpressure diastolic is empty");
            assertNotNull(bloodpressureData.getData(), "bloodpressure data wrapper is null");
            bloodpressureData.getData().forEach(bData -> {
                assertFalse(bData.getId().isEmpty(), "Id field is Empty");
                assertFalse(bData.getType().isEmpty(), "Type field is Empty");
                assertFalse(bData.getTs().toString().isEmpty(), "Ts field is Empty");
                assertFalse(bData.getStatus().isEmpty(), "Status field is Empty");
                assertFalse(bData.getSystolic().toString().isEmpty(), "Systolic field is Empty");
                assertFalse(bData.getDiastolic().toString().isEmpty(), "Diastolic field is Empty");
                assertFalse(bData.getHeartrate().toString().isEmpty(), "Heartrate field is Empty");
            });
            assertFalse(bloodpressureData.getTs().toString().isEmpty(), "bloodpressure ts is empty");
            assertFalse(bloodpressureData.getHeartrate().toString().isEmpty(), "bloodpressure heartrate is empty");
        } catch (NullPointerException e) {
            System.out.println("Bloodpressure data is empty");
        }

        pulseoxData = tasksApp.getData().stream().filter(datum -> datum.getType().equals("pulseox")).collect(Collectors.toList()).get(0);
        assertFalse(pulseoxData.getId().isEmpty(), "pulse ox id is empty");
        assertEquals(pulseoxData.getType(), "pulseox", "current type is not eqls");
        try {
            pulseoxData.getData().forEach(pData -> {
                assertFalse(pData.getId().isEmpty(), "Id field is Empty");
                assertFalse(pData.getType().isEmpty(), "Type field is Empty");
                assertFalse(pData.getTs().toString().isEmpty(), "Ts field is Empty");
                assertFalse(pData.getStatus().isEmpty(), "Status field is Empty");
                assertFalse(pData.getSpo2().toString().isEmpty(), "Spo2 field is Empty");
                assertFalse(pData.getHeartrate().toString().isEmpty(), "Heartrate field is Empty");
            });
            assertFalse(pulseoxData.getSpo2().toString().isEmpty(), "Spo2 is Empty");
            assertFalse(pulseoxData.getTs().toString().isEmpty(), "Ts is Empty");
            assertFalse(pulseoxData.getHeartrate().toString().isEmpty(), "Heartrate is Empty");
        } catch (NullPointerException e) {
            System.out.println("PulseOx data is empty");
        }

        surveyData = tasksApp.getData().stream().filter(datum -> datum.getType().equals("survey")).collect(Collectors.toList());
        surveyData.forEach(servData -> {
            assertNotNull(servData.getSchedule().getReminder(), "weight reminder is null ");

            assertFalse(servData.getId().isEmpty(), "survey id is empty");
            assertEquals(servData.getType(), "survey", "current type is not eqls");
            try {
                assertFalse(servData.getAction().getSurveyId().isEmpty(), "survey id is empty");
                assertFalse(servData.getAction().getQuestion().isEmpty(), "survey question is empty");
                assertNotNull(servData.getAction().getChoices(), "survey action choices is empty");

                servData.getAction().getChoices().forEach(choice -> {
                    assertFalse(choice.getId().toString().isEmpty(), "Id field is Empty");
                    assertFalse(choice.getVal().toString().isEmpty(), "Val field is Empty");
                    assertFalse(choice.getType().isEmpty(), "Type field is Empty");
                    assertFalse(choice.getText().isEmpty(), "Text field is Empty");
                });

                assertNotNull(servData.getSchedule().getEvery(), "survey schedule every field is null");
                assertFalse(servData.getSchedule().getEvery().getMeasure().isEmpty(), "Measure field is empty");
                assertFalse(servData.getSchedule().getAt().isEmpty(), "at field is empty");

                assertFalse(servData.getAnswersHaveNoOrder().toString().isEmpty(), "AnswersHaveNoOrder field is Empty");
                assertFalse(servData.getAreAnswersInReverseOrder().toString().isEmpty(), "AreAnswersInReverseOrder field is Empty");
            } catch (NullPointerException e) {
                System.out.println("Survey data is empty");
            }

        });

        restUtilities.removeHeaders(reqSpec, "Authorization");
    }

    @Test(dependsOnMethods = {
            "testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testGetPatientTasksAppResponseV1() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V1);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
        ResponseSpecification resPec = restUtilities.getResponseSpecification();


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TASKS_INTG);
        reqSpec.queryParam("filter[patient]", propertyWorker.getProperty("patient_hrs_id"));
        Response response = restUtilities.getResponse(reqSpec, "get");

        PatientTasksV1 tasks = response.as(PatientTasksV1.class);

        tasks.getData().forEach(task -> {
            assertFalse(task.getId().isEmpty(), "Task id field is empty");
            assertFalse(task.getResourceType().isEmpty(), "Task ResourceType field is empty");
            assertEquals(task.getResourceType(), "tasks", "ResourceType field is not eql to tasks");
        });

        com.hrs.cc.api.models.integratioins_app.patient_tasks_v1.Datum glucose = tasks.getData().stream().filter(t -> t.getAttributes().getType().equals("glucose")).findFirst().get();
        assertTrue(glucose.getId().contains("glucose"), "Id field not contain glucose");
        assertEquals(glucose.getResourceType(), "tasks", "ResourceType not eql to task");
        assertNotNull(glucose.getAttributes().getGlucose(), "Glucose field is null");
        glucose.getAttributes().getData().forEach(data -> {
            assertNotNull(data.getGlucose(), "Glucose field is null");
            assertNotNull(data.getId(), "Id field is null");
            assertEquals(data.getType(), "Manual", "Type field not eql manual");
            assertNotNull(data.getTs(), "Ts field is null");
            assertNotNull(data.getStatus(), "Status field is null");
        });

        com.hrs.cc.api.models.integratioins_app.patient_tasks_v1.Datum activity = tasks.getData().stream().filter(t -> t.getAttributes().getType().equals("activity")).findFirst().get();
        assertTrue(activity.getId().contains("activity"), "Id field not contain activity");
        assertEquals(activity.getResourceType(), "tasks", "ResourceType not eql to task");
        assertNotNull(activity.getAttributes().getDuration(), "Duration field is null");
        activity.getAttributes().getData().forEach(data -> {
            assertNotNull(data.getDuration(), "Duration field is null");
            assertNotNull(data.getId(), "Id field is null");
            assertNotNull(data.getType(), "Type field is null");
            assertNotNull(data.getTs(), "Ts field is null");
            assertNotNull(data.getStatus(), "Status field is null");
        });

        com.hrs.cc.api.models.integratioins_app.patient_tasks_v1.Datum woundimaging = tasks.getData().stream().filter(t -> t.getAttributes().getType().equals("woundimaging")).findFirst().get();
        assertTrue(woundimaging.getId().contains("woundimaging"), "Id field not contain woundimaging");
        assertEquals(woundimaging.getResourceType(), "tasks", "ResourceType not eql to task");

        com.hrs.cc.api.models.integratioins_app.patient_tasks_v1.Datum temperature = tasks.getData().stream().filter(t -> t.getAttributes().getType().equals("temperature")).findFirst().get();
        assertTrue(temperature.getId().contains("temperature"), "Id field not contain temperature");
        assertEquals(temperature.getResourceType(), "tasks", "ResourceType not eql to task");
//        assertNotNull(temperature.getAttributes().getUnit(), "Unit field is null");
        temperature.getAttributes().getData().forEach(data -> {
            assertNotNull(data.getUnit(), "Unit field is null");
            assertNotNull(data.getTemperature(), "Temperature() field is null");
            assertNotNull(data.getId(), "Id field is null");
            assertNotNull(data.getType(), "Type field is null");
            assertNotNull(data.getTs(), "Ts field is null");
            assertNotNull(data.getStatus(), "Status field is null");
        });

        com.hrs.cc.api.models.integratioins_app.patient_tasks_v1.Datum weight = tasks.getData().stream().filter(t -> t.getAttributes().getType().equals("weight")).findFirst().get();
        assertTrue(weight.getId().contains("weight"), "Id field not contain weight");
        assertEquals(weight.getResourceType(), "tasks", "ResourceType not eql to task");
        weight.getAttributes().getData().forEach(data -> {
            assertNotNull(data.getWeight(), "Weight field is null");
            assertNotNull(data.getId(), "Id field is null");
            assertNotNull(data.getType(), "Type field is null");
            assertNotNull(data.getTs(), "Ts field is null");
            assertNotNull(data.getStatus(), "Status field is null");
        });

        List<com.hrs.cc.api.models.integratioins_app.patient_tasks_v1.Datum> medication = tasks.getData().stream().filter(t -> t.getAttributes().getType().equals("medication")).collect(Collectors.toList());
        medication.forEach(m -> {
            assertTrue(m.getId().contains("medication"), "Id field not contain medication");
            assertEquals(m.getResourceType(), "tasks", "ResourceType not eql to task");

            assertNotNull(m.getAttributes().getAction().getName(), "Name field is null");
            assertNotNull(m.getAttributes().getAction().getMedicationId(), "MedicationId field is null");
            assertNotNull(m.getAttributes().getAction().getDosage(), "Dosage field is null");
            assertNotNull(m.getAttributes().getAction().getStrength(), "Strength field is null");
            assertNotNull(m.getAttributes().getAction().getUnits(), "Units field is null");
            assertNotNull(m.getAttributes().getAction().getCount(), "Count field is null");
            assertNotNull(m.getAttributes().getAction().getRoute(), "Route field is null");
            assertNotNull(m.getAttributes().getInstruction(), "Instruction field is null");
        });

        com.hrs.cc.api.models.integratioins_app.patient_tasks_v1.Datum bloodpressure = tasks.getData().stream().filter(t -> t.getAttributes().getType().equals("bloodpressure")).findFirst().get();
        assertTrue(bloodpressure.getId().contains("bloodpressure"), "Id field not contain bloodpressure");
        assertEquals(bloodpressure.getResourceType(), "tasks", "ResourceType not eql to task");
        bloodpressure.getAttributes().getData().forEach(data -> {
            assertNotNull(data.getSystolic(), "Systolic field is null");
            assertNotNull(data.getDiastolic(), "Diastolic field is null");
            assertNotNull(data.getHeartrate(), "Heartrate field is null");
            assertNotNull(data.getId(), "Id field is null");
            assertNotNull(data.getType(), "Type field is null");
            assertNotNull(data.getTs(), "Ts field is null");
            assertNotNull(data.getStatus(), "Status field is null");
        });

        com.hrs.cc.api.models.integratioins_app.patient_tasks_v1.Datum pulseox = tasks.getData().stream().filter(t -> t.getAttributes().getType().equals("pulseox")).findFirst().get();
        assertTrue(pulseox.getId().contains("pulseox"), "Id field not contain pulseox");
        assertEquals(pulseox.getResourceType(), "tasks", "ResourceType not eql to task");
        pulseox.getAttributes().getData().forEach(data -> {
            assertNotNull(data.getSpo2(), "Spo2 field is null");
            assertNotNull(data.getHeartrate(), "Heartrate field is null");
            assertNotNull(data.getId(), "Id field is null");
            assertNotNull(data.getType(), "Type field is null");
            assertNotNull(data.getTs(), "Ts field is null");
            assertNotNull(data.getStatus(), "Status field is null");
        });

        List<com.hrs.cc.api.models.integratioins_app.patient_tasks_v1.Datum> survey = tasks.getData().stream().filter(t -> t.getAttributes().getType().equals("survey")).collect(Collectors.toList());
        survey.forEach(m -> {
            assertTrue(m.getId().contains("survey"), "Id field not contain survey");
            assertEquals(m.getResourceType(), "tasks", "ResourceType not eql to task");

            assertNotNull(m.getAttributes().getAction().getSurveyId(), "SurveyId field is null");
            assertNotNull(m.getAttributes().getAction().getQuestion(), "Question field is null");
            assertNotNull(m.getAttributes().getAction().getOnetime(), "Onetime field is null");
            assertNotNull(m.getAttributes().getAction().getDischarge(), "Discharge field is null");

            for (Choice choice : m.getAttributes().getAction().getChoices()) {
                assertNotNull(choice.getId(), "Id field is null");
                assertNotNull(choice.getVal(), "Val field is null");
                assertNotNull(choice.getType(), "Type field is null");
            }
        });
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice"})
    public void testGetFilesEducationContent() {
        /*turn on education files*/
        RestAssistant.educationsContentForPatient(5, true, propertyWorker.getProperty("patient_hrs_id"));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.FILES);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
        restUtilities.createQueryParam(reqSpec, "filter[hrsid]", propertyWorker.getProperty("patient_hrs_id"));

        Response response = restUtilities.getResponse(reqSpec, Method.GET.name());

        FilesEducationContent educationContent = response.as(FilesEducationContent.class);

        educationContent.getData().stream()
                .filter(f -> f.getType().equals("video"))
                .forEach(fileObject -> {
                    assertNotNull(fileObject.getId(), "Id field is null");
                    assertNotNull(fileObject.getName(), "Name field is null");
                    assertNotNull(fileObject.getConditions(), "Conditions field is null");
                    assertNotNull(fileObject.getDescription(), "Description field is null");
                    assertNotNull(fileObject.getType(), "Type field is null");
                    assertNotNull(fileObject.getDuration(), "Duration field is null");
                });

        educationContent.getData().stream()
                .filter(f -> f.getType().equals("custom"))
                .forEach(fileObject -> {
                    assertNotNull(fileObject.getId(), "Id field is null");
                    assertNotNull(fileObject.getName(), "Name field is null");
                    assertNotNull(fileObject.getConditions(), "Conditions field is null");
                    assertNotNull(fileObject.getType(), "Type field is null");
                    assertNull(fileObject.getDuration(), "Duration field is not null");
                });

        /*turn off education files*/
        RestAssistant.educationsContentForPatient(5, false, propertyWorker.getProperty("patient_hrs_id"));
    }

    @Test(dependsOnMethods = {
            "testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice"})
    public void testGetCaregiversForPatient() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_LINKS);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
        restUtilities.createQueryParam(reqSpec, "filter[patient]", propertyWorker.getProperty("patient_hrs_id"));
        restUtilities.createQueryParam(reqSpec, "sideload[]", "caregiver");

        Response response = restUtilities.getResponse(reqSpec, Method.GET.name());

        PatientsCaregiver patientsCaregiver = response.as(PatientsCaregiver.class);
        patientsCaregiver.getData().forEach(datum -> {
            assertNotNull(datum.getId(), "caregiver-patient-link id is null");
            assertEquals(datum.getResourceType(), "caregiver-patient-link", "type is not caregiver-patient-link");
            assertEquals(datum.getPatient(), propertyWorker.getProperty("patient_hrs_id"), "Patient id is not eql");
            assertNotNull(datum.getCaregiver(), "Caregiver id is null");
            assertNotNull(datum.getEnvironment(), "Environment is null");
            assertNotNull(datum.getCreated(), "created date is null");

            assertNotNull(patientsCaregiver.getSideload().getCaregiver().get(datum.getCaregiver()).getFirstName(), "caregiver first name is null");
            assertNotNull(patientsCaregiver.getSideload().getCaregiver().get(datum.getCaregiver()).getLastName(), "caregiver last name is null");
            assertEquals(patientsCaregiver.getSideload().getCaregiver().get(datum.getCaregiver()).getId(), datum.getCaregiver(), "caregiver id is different");
        });

    }

    @Test(dependsOnMethods = {
            "testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetCaregiversForPatient"})
    public void testGetCaregiversForPatientV1() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(Path.API_V1.concat(EndPoints.PATIENT_LINKS));
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
        restUtilities.createQueryParam(reqSpec, "filter[patient]", propertyWorker.getProperty("patient_hrs_id"));
        restUtilities.createQueryParam(reqSpec, "sideload[]", "caregiver");

        Response response = restUtilities.getResponse(reqSpec, Method.GET.name());
        PatientsCaregiver patientsCaregiver = response.as(PatientsCaregiver.class);
        patientsCaregiver.getData().forEach(datum -> {
            assertNotNull(datum.getId(), "caregiver-patient-link id is null");
            assertEquals(datum.getResourceType(), "caregiver-patient-link", "type is not caregiver-patient-link");
            assertEquals(datum.getAttributes().getPatient(), propertyWorker.getProperty("patient_hrs_id"), "Patient id is not eql");
            assertNotNull(datum.getAttributes().getCaregiver(), "Caregiver id is null");
            assertNotNull(datum.getAttributes().getEnvironment(), "Environment is null");
            assertNotNull(datum.getAttributes().getCreated(), "created date is null");

            assertNotNull(patientsCaregiver.getSideload().getCaregiver().get(datum.getAttributes().getCaregiver()).getFirstName(), "caregiver first name is null");
            assertNotNull(patientsCaregiver.getSideload().getCaregiver().get(datum.getAttributes().getCaregiver()).getLastName(), "caregiver last name is null");
            assertEquals(patientsCaregiver.getSideload().getCaregiver().get(datum.getAttributes().getCaregiver()).getId(), datum.getAttributes().getCaregiver(), "caregiver id is different");
        });

    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice"})
    public void testGetQuizzesV1EducationContent() {
        RestAssistant.educationsContentForPatient(0, true, propertyWorker.getProperty("patient_hrs_id"));
        RestUtilities newRestUtil = new RestUtilities();
        newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
        newReqSpec.basePath(Path.API_V1);
        newRestUtil.setContentType(ContentType.JSON);
        newRestUtil.setEndPoint(EndPoints.QUIZZES);
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
        newRestUtil.getResponseSpecification();

        newReqSpec.queryParam("filter[hrsid]", pacientHrsId);

        Response response = newRestUtil.getResponse(newReqSpec, "get");
        PatientQuizzes quizzes = response.as(PatientQuizzes.class);
        quizzes.getData().forEach(quiz -> {
            assertNotNull(quiz.getId(), "Id field is null");
            assertNotNull(quiz.getResourceType(), "ResourceType field is null");
            assertNotNull(quiz.getAttributes(), "Attributes field is null");
            assertNotNull(quiz.getAttributes().getName(), "Attributes.Name field is null");
            assertNotNull(quiz.getAttributes().getConditions(), "Attributes.Conditions field is null");

            if (!quiz.getAttributes().getQuestions().isEmpty()) {
                quiz.getAttributes().getQuestions().forEach(question -> {
                    assertNotNull(question.getText(), "Text field is null");
                    assertNotNull(question.getExplanation(), "Explanation field is null");
                    assertNotNull(question.getAnswers(), "Answers field is null");

                    question.getAnswers().forEach(answer -> {
                        assertNotNull(answer.getCorrect(), "Correct field is null");
                        assertNotNull(answer.getText(), "Text field is null");
                    });
                });
            }
        });

        RestAssistant.educationsContentForPatient(0, false, propertyWorker.getProperty("patient_hrs_id"));
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice"})
    public void testGetFilesV1EducationContent() {
        RestAssistant.educationsContentForPatient(0, true, propertyWorker.getProperty("patient_hrs_id"));

        RestUtilities newRestUtil = new RestUtilities();
        newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
        newReqSpec.basePath(Path.API_V1);
        newRestUtil.setContentType(ContentType.JSON);
        newRestUtil.setEndPoint(EndPoints.FILES);
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
        newRestUtil.getResponseSpecification();

        newReqSpec.queryParam("filter[hrsid]", pacientHrsId);

        Response response = newRestUtil.getResponse(newReqSpec, "get");
        PatientEducationFiles files = response.as(PatientEducationFiles.class);
        files.getData().forEach(file -> {
            assertNotNull(file.getId(), "Id field is null");
            assertNotNull(file.getResourceType(), "ResourceType field is null");
            assertNotNull(file.getAttributes(), "Attributes field is null");

            assertNotNull(file.getAttributes().getName(), "Attributes.Name field is null");
            assertNotNull(file.getAttributes().getConditions(), "Attributes.Conditions field is null");
            assertNotNull(file.getAttributes().getType(), "Attributes.Type field is null");
        });

        RestAssistant.educationsContentForPatient(0, false, propertyWorker.getProperty("patient_hrs_id"));
    }

    @Test(dependsOnMethods = {
            "testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse",
            "testSendActivityManualOnPatientApp"})
    public void testUserInformationById() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        restUtilities.createPathParam(reqSpec, "id", pacientHrsId);

        Response response = restUtilities.getResponse(reqSpec, "get");

        PatientState patientState = response.as(PatientState.class);
        assertNotNull(patientState.getData().getId(), "Id field is null");
        assertNotNull(patientState.getData().getType(), "Type field is null");
        assertNotNull(patientState.getData().getEnvironment(), "Environment field is null");
//        assertNotNull(patientState.getData().getProfile().getDischarged(), "Profile.Discharged field is null);
        assertNotNull(patientState.getData().getProfile().getStatus(), "Profile.Status field is null");

        assertEquals(patientState.getData().getId(), pacientHrsId, "Id field not eqls");
        assertEquals(patientState.getData().getType(), "patient", "Type field not eql");
        assertEquals(patientState.getData().getProfile().getStatus(), "activated", "patient not activated");
    }

    @Test(dependsOnMethods = {
            "testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse",
            "testSendActivityManualOnPatientApp",
            "testUserInformationById"})
    public void testV1UserInformationById() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(Path.API_V1.concat(EndPoints.USERS).concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        restUtilities.createPathParam(reqSpec, "id", pacientHrsId);

        Response response = restUtilities.getResponse(reqSpec, "get");
        JsonPath path = response.jsonPath();
        assertNotNull(path.get("data.id"), "data.id field is null");
        assertNotNull(path.get("data.resourceType"), "data.resourceType field is null");
        assertNotNull(path.get("data.attributes"), "data.attributes field is null");
        assertNotNull(path.get("data.attributes.type"), "data.attributes.type field is null");
        assertEquals(path.get("data.attributes.type"), "patient", "patient type not eqls");
        assertNotNull(path.get("data.attributes.environment"), "data.attributes.environment field is null");
        assertNotNull(path.get("data.attributes.profile"), "data.attributes.profile field is null");

    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testUserInformationById"})
    public void testGetUserStatusAfterPaused() {
        RestAssistant.pausePatient(pacientHrsId);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        restUtilities.createPathParam(reqSpec, "id", pacientHrsId);

        Response response = restUtilities.getResponse(reqSpec, "get");
        PatientState patientState = response.as(PatientState.class);
        assertNotNull(patientState.getData().getId(), "Id field is null");
        assertNotNull(patientState.getData().getType(), "Type field is null");
        assertNotNull(patientState.getData().getEnvironment(), "Environment field is null");
        assertNotNull(patientState.getData().getProfile().getStatus(), "Profile.Status field is null");

        assertEquals(patientState.getData().getId(), pacientHrsId, "patient ids not eqls");
        assertEquals(patientState.getData().getType(), "patient", "patient type not eqlto 'patient' type");
        assertEquals(patientState.getData().getProfile().getStatus(), "paused", "patient not paused");
    }

    @Test(dependsOnMethods = {
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testUserInformationById",
            "testGetUserStatusAfterPaused"})
    public void testGetUserStatusAfterActivated() {
        RestAssistant.activatedPatient(pacientHrsId);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        restUtilities.createPathParam(reqSpec, "id", pacientHrsId);

        Response response = restUtilities.getResponse(reqSpec, "get");
        PatientState patientState = response.as(PatientState.class);
        assertNotNull(patientState.getData().getId(), "Id field is null");
        assertNotNull(patientState.getData().getType(), "Type field is null");
        assertNotNull(patientState.getData().getEnvironment(), "Environment field is null");
        assertNotNull(patientState.getData().getProfile().getStatus(), "Profile.Status field is null");

        assertEquals(patientState.getData().getId(), pacientHrsId, "patient ids not eqls");
        assertEquals(patientState.getData().getType(), "patient", "patient type not eqlto 'patient' type");

        assertEquals(patientState.getData().getProfile().getStatus(), "activated", "patient not activated");
    }

    @Test(dependsOnMethods = {
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testUserInformationById",
            "testGetUserStatusAfterPaused",
            "testGetUserStatusAfterActivated"})
    public void testGetUserStatusAfterPreDeactivated() {
        RestAssistant.dischargePatient(pacientHrsId);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        restUtilities.createPathParam(reqSpec, "id", pacientHrsId);
        Response response = restUtilities.getResponse(reqSpec, "get");
        PatientState patientState = response.as(PatientState.class);
        assertNotNull(patientState.getData().getId(), "Id field is null");
        assertNotNull(patientState.getData().getType(), "Type field is null");
        assertNotNull(patientState.getData().getEnvironment(), "Environment field is null");
        assertNotNull(patientState.getData().getProfile().getStatus(), "Profile.Status field is null");

        assertEquals(patientState.getData().getId(), pacientHrsId, "patient ids not eqls");
        assertEquals(patientState.getData().getType(), "patient", "patient type not eqlto 'patient' type");

        assertEquals(patientState.getData().getProfile().getStatus(), "predeactivated", "patient not predeactivated");
    }

    @Test(dependsOnMethods = {
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testUserInformationById",
            "testGetUserStatusAfterPaused",
            "testGetUserStatusAfterActivated",
            "testGetUserStatusAfterPreDeactivated"})
    public void testGetUserStatusAfterRemoveDischarged() {
        RestAssistant.removeDischargePatient(pacientHrsId);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        restUtilities.createPathParam(reqSpec, "id", pacientHrsId);
        Response response = restUtilities.getResponse(reqSpec, "get");
        PatientState patientState = response.as(PatientState.class);
        assertNotNull(patientState.getData().getId(), "Id field is null");
        assertNotNull(patientState.getData().getType(), "Type field is null");
        assertNotNull(patientState.getData().getEnvironment(), "Environment field is null");
        assertNotNull(patientState.getData().getProfile().getStatus(), "Profile.Status field is null");

        assertEquals(patientState.getData().getId(), pacientHrsId, "patient ids not eqls");
        assertEquals(patientState.getData().getType(), "patient", "patient type not eqlto 'patient' type");

        assertEquals(patientState.getData().getProfile().getStatus(), "activated", "patient not activated");
    }

    @Test(dependsOnMethods = {
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendWeightManualOnPatientApp() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.METRICS_INTG);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        DataWeight dataWeight = new DataWeight();
        dataWeight.setOwner(propertyWorker.getProperty("patient_hrs_id"));
        dataWeight.setEntered("manual|device");
        dataWeight.setMetric(weightData.getType());

        dataWeight.setRemindAt(restUtilities.getTimePeriodTS(1).get("current").concat("-04:00"));
        dataWeight.setTakenAt(restUtilities.getTimePeriodTS(1).get("current").concat("-04:00"));
        Integer weightGen = restUtilities.getRandomInt(110, 450);
        dataWeight.setFacet(new Facet(weightGen));

        reqSpec.body(new WeightManual(dataWeight));
        Response response = restUtilities.getResponse(reqSpec, "post");

        Weight weights = RestAssistant.getPatientMetricsData(propertyWorker.getProperty("patient_hrs_id")).getWeight();
        Datum____ currentWeight = weights.getData().stream().filter(w -> w.getWeight().equals(dataWeight.getFacet().getWeight())).findFirst().get();
        assertEquals(currentWeight.getWeight(), dataWeight.getFacet().getWeight(), "weights not eqls");
        assertEquals(currentWeight.getStatus(), "new", "status not 'new'");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"}, priority = 1)
    public void testSendStandardWeightManualOnPatientApp() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.METRICS_INTG);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        DataWeight dataWeight = new DataWeight();
        dataWeight.setOwner(propertyWorker.getProperty("patient_hrs_id"));
        dataWeight.setEntered("manual|device");
        dataWeight.setMetric(weightData.getType());

        dataWeight.setRemindAt(restUtilities.getTimePeriodTS(1).get("current").concat("-04:00"));
        dataWeight.setTakenAt(restUtilities.getTimePeriodTS(1).get("current").concat("-04:00"));
        dataWeight.setFacet(new Facet(restUtilities.getRandomInt(300, 699)));
        dataWeight.setDevice("standard-weight");

        reqSpec.body(new WeightManual(dataWeight));

        Response response = restUtilities.getResponse(reqSpec, "post");
        Weight weights = RestAssistant.getPatientMetricsData(propertyWorker.getProperty("patient_hrs_id")).getWeight();
        Datum____ currentWeight = weights.getData().stream().filter(w -> w.getWeight().equals(dataWeight.getFacet().getWeight())).findFirst().get();
        assertEquals(currentWeight.getWeight(), dataWeight.getFacet().getWeight(), "weights not eqls");
        assertEquals(currentWeight.getStatus(), "new", "status not 'new'");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendStandardBloodPressureManualOnPatientApp() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.METRICS_INTG);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        BloodPressureManual bloodPressureManual = new BloodPressureManual();
        DataBP dataBP = new DataBP();
        dataBP.setOwner(propertyWorker.getProperty("patient_hrs_id"));
        dataBP.setEntered("manual|device");
        dataBP.setMetric(bloodpressureData.getType());
        dataBP.setDevice("standard-bloodpressure");
        dataBP.setRemindAt(restUtilities.getTimePeriodTS(1).get("current").concat("+02:00"));
        dataBP.setTakenAt(restUtilities.getTimePeriodTS(1).get("current").concat("+02:00"));

        Integer systolic = restUtilities.getRandomInt(100, 299);
        Integer diasolic = restUtilities.getRandomInt(100, 199);
        Integer heartrate = restUtilities.getRandomInt(20, 80);

        dataBP.setFacetBP(new FacetBP(systolic, diasolic, heartrate));
        bloodPressureManual.setData(dataBP);
        reqSpec.body(bloodPressureManual);

        Response response = restUtilities.getResponse(reqSpec, "post");
        Bloodpressure bloodpressure = mapper.convertValue(
                RestAssistant.getPatientMetricsData(
                        propertyWorker.getProperty("patient_hrs_id")).getBloodpressure(), Bloodpressure.class);
        Datum_____ currentBloodMetric = bloodpressure.getData().stream().filter(
                b -> b.getDiastolic().equals(bloodPressureManual.getData().getFacet().getDiastolic()) &&
                        b.getSystolic().equals(bloodPressureManual.getData().getFacet().getSystolic())).findFirst().get();

        assertEquals(currentBloodMetric.getSystolic(), bloodPressureManual.getData().getFacet().getSystolic(), "current data does not match previously sent");
        assertEquals(currentBloodMetric.getDiastolic(), bloodPressureManual.getData().getFacet().getDiastolic(), "current data does not match previously sent");
        assertEquals(currentBloodMetric.getStatus(), "new", "current data does not match previously sent");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendAllMedicationsManualOnPatientApp() {
        ArrayList<MedicationManual> medData = new ArrayList<>();
        medicationData.forEach(medication -> {
            RestUtilities newRestUtil = new RestUtilities();
            newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
            RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
            newReqSpec.basePath("");
            newRestUtil.setContentType(ContentType.JSON);
            newRestUtil.setEndPoint(EndPoints.METRICS_INTG);
            newRestUtil.removeHeaders(newReqSpec, "Authorization");
            newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
            newRestUtil.getResponseSpecification();

            MedicationManual medicationManual = new MedicationManual();
            DataMedication dataMedication = new DataMedication();
            dataMedication.setOwner(propertyWorker.getProperty("patient_hrs_id"));
            dataMedication.setEntered("manual");
            dataMedication.setMetric(medication.getType());
            dataMedication.setRemindAt(newRestUtil.getTimePeriodTS(1).get("current").concat("-04:00"));
            dataMedication.setTakenAt(newRestUtil.getTimePeriodTS(1).get("current").concat("-04:00"));
            dataMedication.setFacet(new FacetMedication(medication.getAction().getName(), medication.getAction().getDosage()));
            medicationManual.setData(dataMedication);


            newReqSpec.body(medicationManual);
            Response response = newRestUtil.getResponse(newReqSpec, "post");
            newRestUtil.removeHeaders(newReqSpec, "Authorization");
            medData.add(medicationManual);
        });

        Medication medClinician = RestAssistant.getPatientMetricsData(propertyWorker.getProperty("patient_hrs_id")).getMedication();
        medData.forEach(medication -> {
            boolean state = medClinician.getMedications().stream().anyMatch(m ->
                    m.getMed().equals(medication.getData().getFacet().getMedication()) &&
                            m.getDose().equals(medication.getData().getFacet().getDosage()));
            assertTrue(state, "current data does not match previously sent medication data");

        });

    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse",
            "testSendAllMedicationsManualOnPatientApp"})
    public void testSendAllMedicationsUsingAllFieldsOnPatientApp() {
        ArrayList<MedicationAllFields> medData = new ArrayList<>();
        medicationData.forEach(medication -> {
            RestUtilities newRestUtil = new RestUtilities();
            newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
            RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
            newReqSpec.basePath("");
            newRestUtil.setContentType(ContentType.JSON);
            newRestUtil.setEndPoint(EndPoints.METRICS_INTG);
            newRestUtil.removeHeaders(newReqSpec, "Authorization");
            newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
            newRestUtil.getResponseSpecification();

            MedicationAllFields medicationAllFields = new MedicationAllFields();
            DataMedicationAllFields dataMedication = new DataMedicationAllFields();
            FacetMedicationAllFields facetMedicationAllFields = new FacetMedicationAllFields();
            facetMedicationAllFields.setMedication(medication.getAction().getName());
            facetMedicationAllFields.setStrength(medication.getAction().getStrength());
            facetMedicationAllFields.setUnits(medication.getAction().getUnits());
            facetMedicationAllFields.setCount(medication.getAction().getCount());
            facetMedicationAllFields.setRoute(medication.getAction().getRoute());

            dataMedication.setOwner(propertyWorker.getProperty("patient_hrs_id"));
            dataMedication.setEntered("manual");
            dataMedication.setMetric(medication.getType());
            dataMedication.setRemindAt(newRestUtil.getTimePeriodTS(1).get("current").concat("+02:00"));
            dataMedication.setTakenAt(newRestUtil.getTimePeriodTS(1).get("current").concat("+02:00"));
            dataMedication.setFacet(facetMedicationAllFields);
            medicationAllFields.setData(dataMedication);


            newReqSpec.body(medicationAllFields);
            Response response = newRestUtil.getResponse(newReqSpec, "post");
            newRestUtil.removeHeaders(newReqSpec, "Authorization");
            medData.add(medicationAllFields);
        });

        Medication medClinician = RestAssistant.getPatientMetricsData(propertyWorker.getProperty("patient_hrs_id")).getMedication();
        medData.forEach(medication -> {
            boolean state = medClinician.getMedications().stream().anyMatch(m ->
                    m.getMed().equals(medication.getData().getFacet().getMedication()) &&
                            m.getDose().equals(medication.getData().getFacet().getStrength() + " " +
                                    "" + medication.getData().getFacet().getUnits() + " x" + medication.getData().getFacet().getCount() + " " +
                                    "" + medication.getData().getFacet().getRoute()));
            assertTrue(state, "current data does not match previously sent medication data");
        });
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendGlucoseManualOnPatientApp() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.METRICS_INTG);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        GlucoseManual glucoseManual = new GlucoseManual();
        DataGlucose dataGlucose = new DataGlucose();
        dataGlucose.setOwner(propertyWorker.getProperty("patient_hrs_id"));
        dataGlucose.setEntered("manual");
        dataGlucose.setMetric(glucoseData.getType());
        dataGlucose.setRemindAt(null);
        dataGlucose.setTakenAt(restUtilities.getTimePeriodTS(1).get("current").concat("+02:00"));
        Integer glucoseGen = restUtilities.getRandomInt(80, 300);
        dataGlucose.setFacet(new FacetGlucose(glucoseGen));
        glucoseManual.setData(dataGlucose);
        reqSpec.body(glucoseManual);

        Response response = restUtilities.getResponse(reqSpec, "post");

        Glucose glucoseClin = mapper.convertValue(RestAssistant.getPatientMetricsData(propertyWorker.getProperty("patient_hrs_id")).getGlucose(), Glucose.class);

        assertTrue(glucoseClin.getData().stream().anyMatch(g ->
                g.getGlucose().equals(glucoseManual.getData().getFacet().getBloodsugar())), "current data does not match previously sent glucose data[blood sugar]");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendActivityManualOnPatientApp() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.METRICS_INTG);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        ActivityManual activityManual = new ActivityManual();
        DataActivity dataActivity = new DataActivity();
        dataActivity.setOwner(propertyWorker.getProperty("patient_hrs_id"));
        dataActivity.setEntered("manual");
        dataActivity.setMetric(activityData.getType());
        dataActivity.setRemindAt(null);
        dataActivity.setTakenAt(restUtilities.getTimePeriodTS(1).get("current").concat("+02:00"));
        dataActivity.setDevice(null);
        dataActivity.setFacet(new FacetActivity(restUtilities.getRandomInt(150, 500).toString(), "minutes"));
        activityManual.setData(dataActivity);
        reqSpec.body(activityManual);
        restUtilities.getResponse(reqSpec, "post");

        Activity activityClin = RestAssistant.getPatientMetricsData(propertyWorker.getProperty("patient_hrs_id")).getActivity();
        Integer durationSum = activityClin.getData().stream()
                .filter(i -> !i.getStatus().equals("removed"))
                .map(Datum_::getDuration)
                .reduce(0, Integer::sum);

        assertEquals(activityClin.getDuration(), durationSum, "current data does not match previously sent activity data");
        Datum_ currentActivityMetric = activityClin.getData().stream().filter(
                a -> a.getDuration().equals(Integer.valueOf(activityManual.getData().getFacet().getValue()))).findFirst().get();

        assertEquals(currentActivityMetric.getStatus(), "new", "activity status not eql");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendTemperatureManualOnPatientApp() {
        String temperature = restUtilities.getRandomInt(97, 108).toString();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.METRICS_INTG);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        TemperatureManual temperatureManual = new TemperatureManual();
        DataTemperature dataTemperature = new DataTemperature();
        dataTemperature.setOwner(propertyWorker.getProperty("patient_hrs_id"));
        dataTemperature.setEntered("manual");
        dataTemperature.setMetric(temperatureData.getType());
        dataTemperature.setRemindAt(null);
        dataTemperature.setTakenAt(restUtilities.getTimePeriodTS(1).get("current").concat("+02:00"));
        dataTemperature.setFacet(new FacetTemperature(temperature, "f"));
        temperatureManual.setData(dataTemperature);
        reqSpec.body(temperatureManual);
        restUtilities.getResponse(reqSpec, "post");

        Temperature temperatureClin = mapper.convertValue(
                RestAssistant.getPatientMetricsData(
                        propertyWorker.getProperty("patient_hrs_id")).getTemperature(), Temperature.class);

        assertTrue(temperatureClin.getData().stream().anyMatch(
                t -> t.getTemperature().toString().equals(temperatureManual.getData().getFacet().getTemperature())), "current data does not match previously sent temperature");
//        assertTrue(temperatureClin.getData().stream().allMatch(t -> t.getUnit().equals("F") && t.getStatus().equals("new")));

        Datum___ currentTemperature = temperatureClin.getData().stream().filter(
                t -> t.getTemperature().toString().equals(temperatureManual.getData().getFacet().getTemperature())).findFirst().get();

        assertEquals(currentTemperature.getTemperature().toString(), temperatureManual.getData().getFacet().getTemperature(), "current data does not match previously sent temperature");
        assertEquals(currentTemperature.getStatus(), "new", "status is not eql");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendPulseOxManualOnPatientApp() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.METRICS_INTG);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        PulseOxManual pulseOxManual = new PulseOxManual();
        DataPulseOx dataPulseOx = new DataPulseOx();
        dataPulseOx.setOwner(propertyWorker.getProperty("patient_hrs_id"));
        dataPulseOx.setEntered("manual");
        dataPulseOx.setMetric(pulseoxData.getType());
        dataPulseOx.setTakenAt(restUtilities.getTimePeriodTS(1).get("current").concat("+02:00"));
        dataPulseOx.setFacet(new FacetPulseOx(restUtilities.getRandomInt(30, 99).toString(), restUtilities.getRandomInt(80, 199).toString()));
        pulseOxManual.setData(dataPulseOx);
        reqSpec.body(pulseOxManual);

        Response response = restUtilities.getResponse(reqSpec, "post");

        Pulseox pulseOxClin = mapper.convertValue(RestAssistant.getPatientMetricsData(propertyWorker.getProperty("patient_hrs_id")).getPulseox(), Pulseox.class);
        Datum______ currentPulseOx = pulseOxClin.getData().stream().filter(
                p -> p.getSpo2().equals(Integer.valueOf(pulseOxManual.getData().getFacet().getSpo2())) &&
                        p.getHeartrate().equals(Integer.valueOf(pulseOxManual.getData().getFacet().getHr()))).findFirst().get();

        assertEquals(currentPulseOx.getSpo2().toString(), pulseOxManual.getData().getFacet().getSpo2(), "current data does not match previously sent spo2 pulsox");
        assertEquals(currentPulseOx.getHeartrate().toString(), pulseOxManual.getData().getFacet().getHr(), "current data does not match previously sent hr pulsox");
        assertEquals(currentPulseOx.getStatus(), "new", "current status not eql");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendSurveyManualOnPatientApp() {
        surveyData.forEach(survey -> {
            RestUtilities newRestUtil = new RestUtilities();
            newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
            RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
            newReqSpec.basePath("");
            newRestUtil.setContentType(ContentType.JSON);
            newRestUtil.setEndPoint(EndPoints.METRICS_INTG);
            newRestUtil.removeHeaders(newReqSpec, "Authorization");
            newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
            newRestUtil.getResponseSpecification();

            SurveyManual surveyManual = new SurveyManual();
            DataSurvey dataSurvey = new DataSurvey();
            dataSurvey.setOwner(propertyWorker.getProperty("patient_hrs_id"));
            dataSurvey.setEntered("manual");
            dataSurvey.setMetric(survey.getType());
            dataSurvey.setRemindAt(newRestUtil.getTimePeriodTS(1).get("current").concat("+02:00"));
            dataSurvey.setTakenAt(newRestUtil.getTimePeriodTS(1).get("current").concat("+02:00"));
            dataSurvey.setFacet(new FacetSurvey(survey.getAction().getSurveyId(), survey.getAction().getChoices().get(0).getId().toString()));
            surveyManual.setData(dataSurvey);
            newReqSpec.body(surveyManual);

            Response response = newRestUtil.getResponse(newReqSpec, "post");
            newRestUtil.removeHeaders(newReqSpec, "Authorization");
        });

        try {
            Survey surveys = mapper.convertValue(
                    RestAssistant.getPatientMetricsData(propertyWorker.getProperty("patient_hrs_id")).getSurvey(),
                    Survey.class);
            surveyData.forEach(s -> {
                boolean state = surveys.getQuestions().stream().anyMatch(
                        qT ->
                                qT.getQuestion().equals(s.getAction().getQuestion()) &&
                                        qT.getId().equals(s.getAction().getSurveyId()));
                assertTrue(state, "current data does not match previously sent survey[question, id]");
            });

            com.hrs.cc.api.models.patients.survey.Survey surveysCheck = RestAssistant.questionsDetails(propertyWorker.getProperty("patient_hrs_id"));

            surveyData.forEach(s -> {
                boolean state = surveysCheck.getQuestions().stream().anyMatch(qT ->
                        qT.getQuestion().equals(s.getAction().getQuestion()) &&
                                qT.getId().equals(s.getAction().getSurveyId()));

                assertTrue(state, "The survey detail array is empty or or incorrect data exists");
            });


        } catch (IllegalArgumentException e) {
            System.out.println("Survey is not array");
        }

        RestAssistant.getOneTimeQuestions().forEach(q -> {
            assertTrue(q.getAction().getAnswered());
            assertTrue(q.getAction().getOnetime());
        });
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendImageManualOnPatientApp() {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        String time = formatter.format(date);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        calendar.getTime();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.METRICS_INTG);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        ImageManual imageManual = new ImageManual();
        DataImage dataImage = new DataImage();
        dataImage.setOwner(propertyWorker.getProperty("patient_hrs_id"));
        dataImage.setEntered("manual");
        dataImage.setMetric(woundimagingData.getType());
        dataImage.setRemindAt(restUtilities.getTimePeriodTS(2).get("plusDays").concat("+02:00"));
        dataImage.setTakenAt(restUtilities.getTimePeriodTS(2).get("plusDays").concat("+02:00"));
//        dataImage.setFacet(new FacetImage(ImageCreator.getChangedImageBASE64(calendar.getTime(), Modules.IMAGE_BASE64)));
        dataImage.setFacet(new FacetImage(Modules.IMAGE_BASE64));
        imageManual.setData(dataImage);
        reqSpec.body(imageManual);
//        reqSpec.log().all();
        Response response = restUtilities.getResponse(reqSpec, "post");
        UploadedImageResponse imageResponse = response.as(UploadedImageResponse.class);
        assertNotNull(imageResponse.getOwner(), "Owner field is null");
        assertNotNull(imageResponse.getEntered(), "Entered field is null");
        assertEquals(imageResponse.getMetric(), "woundimagingdata", "not eql metric type");
        assertNotNull(imageResponse.getRemindAt(), "RemindAt field is null");
        assertNotNull(imageResponse.getTakenAt(), "TakenAt field is null");
        assertNotNull(imageResponse.getFacet().getImage(), "Facet.Image field is null");
        assertNotNull(imageResponse.getFacet().getId(), "Facet.Id field is null");
        assertNotNull(imageResponse.getFacet().getData(), "Facet.Data field is null");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendStepsData() {
        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        Map<String, String> facet = new HashMap<>();
        data.put("entered", "fitbit");
        data.put("metric", "steps");
        data.put("takenAt", restUtilities.getTimePeriodTS(4).get("current").concat("+02:00"));
        data.put("remindAt", null);
        facet.put("count", RandomData.getRandomInt(200, 1000).toString());
        facet.put("startDate", restUtilities.getTimePeriodTS(4).get("current").concat("+02:00"));
        data.put("facet", facet);
        body.put("data", data);
        reqSpec.body(body);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.METRICS_INTG);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
        restUtilities.getResponse(reqSpec, "post");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendStethoscopeData() {
        Map<String, Object> body = new HashMap<>();
        Map<String, String> data = new HashMap<>();
        data.put("gid", propertyWorker.getProperty("patient_hrs_id"));
        data.put("soundFile", Modules.STETH_BEEP);
        data.put("imageFile", Modules.IMAGE_BASE64);
        data.put("takenAt", restUtilities.getTimePeriodTS(4).get("current").concat("+02:00"));
        body.put("data", data);
        reqSpec.body(body);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.STETHOSCOPE);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertTrue(path.get("data.takenAt").toString().contains(restUtilities.getTimePeriod(1).get("current")), "date not eql");
        assertNotNull(path.get("data.takenAt"), "takenAt field is null");
        assertNotNull(path.get("data.soundUrl"), "soundUrl field is null");
        assertNotNull(path.get("data.imageUrl"), "imageUrl field is null");
        assertNotNull(path.get("data.id"), "id field is null");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendStethoscopeDataWithPastTime() {
        Map<String, Object> body = new HashMap<>();
        Map<String, String> data = new HashMap<>();
        data.put("gid", propertyWorker.getProperty("patient_hrs_id"));
        data.put("soundFile", Modules.STETH_BEEP);
        data.put("imageFile", Modules.IMAGE_BASE64);
        data.put("takenAt", restUtilities.getTimePeriodTS(4).get("minusDays").concat("+02:00"));
        body.put("data", data);
        reqSpec.body(body);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.STETHOSCOPE);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertTrue(path.get("data.takenAt").toString().contains(restUtilities.getTimePeriod(4).get("minusDays")), "date not eqls");
        assertNotNull(path.get("data.takenAt"), "field takenAt is null");
        assertNotNull(path.get("data.soundUrl"), "field soundUrl is null");
        assertNotNull(path.get("data.imageUrl"), "field imageUrl is null");
        assertNotNull(path.get("data.id"), "field id is null");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse"})
    public void testSendStethoscopeDataWithFutureTime() {
        Map<String, Object> body = new HashMap<>();
        Map<String, String> data = new HashMap<>();
        data.put("gid", propertyWorker.getProperty("patient_hrs_id"));
        data.put("soundFile", Modules.STETH_BEEP);
        data.put("imageFile", Modules.IMAGE_BASE64);
        data.put("takenAt", restUtilities.getTimePeriodTS(4).get("plusDays").concat("+02:00"));
        body.put("data", data);
        reqSpec.body(body);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.STETHOSCOPE);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertTrue(path.get("data.takenAt").toString().contains(restUtilities.getTimePeriod(4).get("plusDays")), "date not eql");
        assertNotNull(path.get("data.takenAt"), "takenAt field is null");
        assertNotNull(path.get("data.soundUrl"), "soundUrl field is null");
        assertNotNull(path.get("data.imageUrl"), "imageUrl field is null");
        assertNotNull(path.get("data.id"), "id field is null");
    }

    @Test(dependsOnMethods = {"testGenerationMagiCodeForPatientAppWhenMaximumLimitIsExceeded",
            "testGenerationMagiCodeForPatientApp",
            "testConnectionMagiCodeForPatientDevice",
            "testGetPatientTasksAppResponse",
            "testSendStethoscopeData"})
    public void testDownloadAndPlayStethoscopeDataByPatientId() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.STETHOSCOPE);
        restUtilities.createQueryParam(reqSpec, "filter[gid]", propertyWorker.getProperty("patient_hrs_id"));
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
        StethoscopeHistoryData data = response.as(StethoscopeHistoryData.class);
        boolean state = data.getData().parallelStream().allMatch(d ->
                d.getImageUrl() != null &&
                        d.getSoundUrl() != null &&
                        d.getTakenAt() != null);
        assertTrue(state, "current data in the field is  null [imageUrl, soundUrl, takenAt]");
        assertTrue(data.getData().parallelStream().anyMatch(
                datum -> datum.getTakenAt().contains(restUtilities.getTimePeriod(1).get("current"))), "does not contain current date");
    }

    @Test(priority = 2)
    public void testResponseAllDevicesOnPatientApp() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TASKS_METAS);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createQueryParam(
                        reqSpec, "filter[identity]", propertyWorker.getProperty("patient_hrs_id")), "get");
        devices = response.as(Devices.class);
        assertNotNull(devices.getData());
        devices.getData().forEach(device -> {
            assertFalse(device.getId().isEmpty(), "Id field is Empty");
            assertFalse(device.getIdentity().isEmpty(), "Identity field is Empty");
            assertFalse(device.getAgent().isEmpty(), "Agent field is Empty");
            assertFalse(device.getSource().isEmpty(), "Source field is Empty");
            assertFalse(device.getCreated().isEmpty(), "Created field is Empty");
            assertFalse(device.getExpires().isEmpty(), "Expires field is Empty");
            try {
                assertFalse(device.getLastUsed().isEmpty());
            } catch (NullPointerException e) {
                System.out.println("Device obj:" + device.getId() + " does not have lastused field.");
            }
        });
    }

    @Test(dependsOnMethods = {"testResponseAllDevicesOnPatientApp"}, priority = 2)
    public void testTurnOFFDevicesOnPatientApp() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(204)
        );

        String devId = devices.getData().stream().findFirst().get().getId();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TOKENS_INTG.concat(EndPoints.DEVICE_ID));
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
        restUtilities.getResponse(restUtilities.createPathParam(reqSpec, "deviceId", devId), "delete");

        RestUtilities newRestUtil = new RestUtilities();
        newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
        newReqSpec.basePath("");
        newRestUtil.setContentType(ContentType.JSON);
        newRestUtil.setEndPoint(EndPoints.TASKS_INTG);
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
        newReqSpec.urlEncodingEnabled(false);
        newRestUtil.getResponseSpecification();

        newRestUtil.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(401)
        );

        Response response = newRestUtil.getResponse(restUtilities.createQueryParam(
                newReqSpec, "filter[patient]", propertyWorker.getProperty("patient_hrs_id")), "get");

        assertEquals(response.statusCode(), 401);
    }

    @Test(dependsOnMethods = {"testResponseAllDevicesOnPatientApp",
            "testTurnOFFDevicesOnPatientApp"})
    public void testConnectionAppleCodeForPatientDevice() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
        restUtilities.removeQueryParam(reqSpec, "filter[identity]");
        MagiCodePacientDevice code = new MagiCodePacientDevice();
        Datum data = new Datum();
        data.setType("code");
        data.setCode(APPLE_CODE);
        code.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TOKENS_INTG);
        reqSpec.body(code);
        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath jsonPath = response.jsonPath();
        assertNotNull(jsonPath.get("data").toString());
        assertNotNull(jsonPath.get("data.token").toString());
        assertFalse(jsonPath.get("data.token").toString().isEmpty());
        assertNull(jsonPath.get("data.refresh"));

        try {
            propertyWorker.setProperty("patient_device_token", jsonPath.get("data.token"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        RestAssistant.getTurnOffoDevice(pacientHrsId);

        RestAssistant.getGenerationMCByPatientId(pacientHrsId);

        Response r = RestAssistant.getConnectionMCToDevice(APPLE_CODE);
        JsonPath jsonR = response.jsonPath();

        try {
            propertyWorker.setProperty("patient_device_token", jsonR.get("data.token"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertEquals(r.statusCode(), HttpStatus.OK_200.getStatusCode());
    }

    @Test(dependsOnMethods = {"testResponseAllDevicesOnPatientApp",
            "testTurnOFFDevicesOnPatientApp",
            "testConnectionAppleCodeForPatientDevice"}, priority = 2)
    public void testTurnOFFAppleDevicesOnPatientApp() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(204)
        );

        List<DataDevices> listDevices = RestAssistant.getAllDevices(pacientHrsId).getData();
        String devId = listDevices.get(listDevices.size() - 1).getId();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TOKENS_INTG.concat(EndPoints.DEVICE_ID));
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.getResponse(restUtilities.createPathParam(reqSpec, "deviceId", devId), "delete");
    }

    @Test(priority = 3)
    public void testGetPatientMetricsByDate() {
        RestUtilities restUtilities = new RestUtilities();
        reqSpec = Connection.UserAuth();
        userData = Connection.getUserData();

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        resPec = restUtilities.getResponseSpecification();

        Map<String, String> date = restUtilities.getTimePeriod(2);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        restUtilities.createQueryParam(reqSpec, "start", date.get("current").replace("-", ""));
        restUtilities.createQueryParam(reqSpec, "end", date.get("current").replace("-", ""));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", propertyWorker.getProperty("patient_hrs_id")), "get");

        List<PatientMetricsData> patientMetricsData = Arrays.asList(response.as(new TypeRef<PatientMetricsData[]>() {
        }));


        patientMetricsData.forEach(patientMetricData -> {
            assertFalse(patientMetricData.getDay().toString().isEmpty(), "patient metric data dayis empty");
            assertNotNull(patientMetricData.getMetrics(), "metrics is null");
            try {
                assertFalse(patientMetricData.getMetrics().getActivity().getGoal().toString().isEmpty(), "activity goal is empty");
                patientMetricData.getMetrics().getActivity().getData().forEach(a -> {
                    assertFalse(a.getId().isEmpty(), "Id field is Empty");
                    assertFalse(a.getType().isEmpty(), "Type field is Empty");
                    assertFalse(a.getStatus().isEmpty(), "Status field is Empty");
                });

            } catch (NullPointerException e) {
                System.out.println("Activity is empty");
            }
            assertNotNull(patientMetricData.getMetrics().getBloodpressure(), "Bloodpressure field is null");
            assertNotNull(patientMetricData.getMetrics().getGlucose(), "Glucose field is null");

            try {
                patientMetricData.getMetrics().getMedication().getMedications().forEach(medication_ -> {
                    assertFalse(medication_.getDose().isEmpty(), "Dose field is Empty");
                    try {
                        assertFalse(medication_.getEssential().toString().isEmpty(), "Essential field is Empty");
                    } catch (NullPointerException e) {
                        System.out.println("Medication " + medication_.getId() + " does not have Essential field");
                    }
                    try {
                        assertFalse(medication_.getExpiration().isEmpty(), "Expiration field is Empty");
                    } catch (NullPointerException e) {
                        System.out.println("Medication " + medication_.getId() + " does not have Expiration field");
                    }
                    assertFalse(medication_.getInstruction().isEmpty(), "Instruction field is Empty");
                    assertFalse(medication_.getMed().isEmpty(), "Med field is Empty");
                    assertFalse(medication_.getTime().isEmpty(), "Time field is Empty");
                    assertFalse(medication_.getId().isEmpty(), "Id field is Empty");
                    assertFalse(medication_.getSchedule().getType().isEmpty(), "Schedule.Type field is Empty");
                });
            } catch (NullPointerException e) {
                System.out.println("Metric is empty!");
            }
            assertNotNull(patientMetricData.getMetrics().getPulseox(), "pulse ox is null");

            try {
                Survey surveys = mapper.convertValue(
                        patientMetricData.getMetrics().getSurvey(),
                        Survey.class);

                assertNotNull(surveys, "surveys is null");
                assertFalse(surveys.getTime().toString().isEmpty(), "Time field is Empty");
                assertFalse(surveys.getWindow().toString().isEmpty(), "Window field is Empty");
                surveys.getQuestions().forEach(question -> {
                    assertFalse(question.getId().isEmpty(), "Id field is Empty");
                    assertFalse(question.getQuestion().isEmpty(), "Question field is Empty");
                    try {
                        assertFalse(question.getToday().toString().isEmpty(), "Today field is Empty");
                    } catch (NullPointerException e) {
                        System.out.println("Response does not have Today field");
                    }
                    assertFalse(question.getSchedule().getType().isEmpty(), "Schedule field is Empty");
                });
            } catch (IllegalArgumentException e) {
                System.out.println("Survey is not array");
            }

            assertNotNull(patientMetricData.getMetrics().getTemperature());
            try {
                patientMetricData.getMetrics().getWeight().getData().forEach(w -> {
                    assertFalse(w.getId().isEmpty(), "Id field is Empty");
//                    assertFalse(w.getRisk().isEmpty(), "Risk field is Empty");
                    assertFalse(w.getStatus().isEmpty(), "Status field is Empty");
                    assertFalse(w.getType().isEmpty(), "Type field is Empty");
                });
            } catch (NullPointerException e) {
                System.out.println("Weight is empty");
            }

            assertNotNull(patientMetricData.getProfile());
            assertFalse(patientMetricData.getProfile().getAudioreminders().toString().isEmpty());
            try {
                assertFalse(patientMetricData.getProfile().getClinician().getHrsid().isEmpty(), "Hrsid field is Empty");
                assertFalse(patientMetricData.getProfile().getClinician().getName().isEmpty(), "Name field is Empty");
            } catch (NullPointerException e) {
                System.out.println("Patient " + patientMetricData.getProfile().getHrsid() + " / " + patientMetricData.getProfile().getPid() + " does not have clinician!");
            }
            assertNotNull(patientMetricData.getProfile().getConditions(), "conditions field is empty");

            try {
                patientMetricData.getProfile().getDevicehistory().forEach(devicehistory -> {
                    assertFalse(devicehistory.getDevid().isEmpty(), "Devid field is Empty");
                    assertFalse(devicehistory.getName().isEmpty(), "Name field is Empty");
                });
            } catch (NullPointerException e) {
                System.out.println("Patient does not have device history");
            }

            assertFalse(patientMetricData.getProfile().getDob().isEmpty(), "Dob field is Empty");
            assertFalse(patientMetricData.getProfile().getEdvisit().toString().isEmpty(), "Edvisit().toString field is Empty");
            assertFalse(patientMetricData.getProfile().getEnvphone().isEmpty(), "Envphone field is Empty");
            assertFalse(patientMetricData.getProfile().getExtension().isEmpty(), "Extension field is Empty");
            assertFalse(patientMetricData.getProfile().getGender().isEmpty(), "Gender field is Empty");
            assertFalse(patientMetricData.getProfile().getHrsid().isEmpty(), "Hrsid field is Empty");
            assertFalse(patientMetricData.getProfile().getLanguage().isEmpty(), "Language field is Empty");
            try {
                assertFalse(patientMetricData.getProfile().getLasthospitalization().toString().isEmpty(), "Lasthospitalization field is Empty");
            } catch (NullPointerException e) {
                System.out.println("Patient".concat(propertyWorker.getProperty("patient_hrs_id")).concat("not hospitalization"));
            }
            assertFalse(patientMetricData.getProfile().getLastmoduleupdate().toString().isEmpty(), "Lastmoduleupdate field is Empty");
            assertFalse(patientMetricData.getProfile().getName().getFirst().isEmpty(), "Name.First field is Empty");
            assertFalse(patientMetricData.getProfile().getName().getLast().isEmpty(), "Name.Last field is Empty");
            assertFalse(patientMetricData.getProfile().getPid().isEmpty(), "Pid field is Empty");
            assertFalse(patientMetricData.getProfile().getReadmission().toString().isEmpty(), "Readmission field is Empty");
            assertFalse(patientMetricData.getProfile().getStatus().isEmpty(), "Status field is Empty");

            assertNotNull(patientMetricData.getRisk(), "risk field is null");

            assertNotNull(patientMetricData.getTracking(), "tracking field is null");
            try {
                assertFalse(patientMetricData.getTracking().getLastactive().toString().isEmpty());
            } catch (NullPointerException e) {
                System.out.println("Patient ".concat(propertyWorker.getProperty("patient_hrs_id")).concat("does not have last active data."));
            }
            assertFalse(patientMetricData.getTracking().getData().getMedlist().toString().isEmpty(), "Medlist field is Empty");
            assertFalse(patientMetricData.getTracking().getData().getQuiz().toString().isEmpty(), "Quiz field is Empty");
            assertFalse(patientMetricData.getTracking().getData().getReminders().toString().isEmpty(), "Reminders field is Empty");
            assertFalse(patientMetricData.getTracking().getData().getTracking().toString().isEmpty(), "Tracking field is Empty");
            assertFalse(patientMetricData.getTracking().getData().getVideo().toString().isEmpty(), "Video field is Empty");
        });
    }

}
package com.hrs.cc.api.tests.сaregivers;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.integratioins_app.connected_devices.Devices;
import com.hrs.cc.api.models.patients.Patients;
import com.hrs.cc.api.models.patients.patient_list.PatientList;
import com.hrs.cc.api.models.patients.patient_list.Profile;
import com.hrs.cc.api.models.requests.add_new_patient.CustomAtributs;
import com.hrs.cc.api.models.requests.add_new_patient.Name;
import com.hrs.cc.api.models.requests.add_new_patient.NewPatient;
import com.hrs.cc.api.models.requests.generation_magic_codes.Data;
import com.hrs.cc.api.models.requests.generation_magic_codes.MagiCodeIdentityPacient;
import com.hrs.cc.api.models.requests.request_magic_code_to_app.Datum;
import com.hrs.cc.api.models.requests.request_magic_code_to_app.MagiCodePacientDevice;
import com.hrs.cc.api.models.requests.set_patient_metricks.activity.SetActivity;
import com.hrs.cc.api.models.requests.set_patient_metricks.blood_pressure.SetBloodPressure;
import com.hrs.cc.api.models.requests.set_patient_metricks.glucose.SetGlucose;
import com.hrs.cc.api.models.requests.set_patient_metricks.pulse_ox.SetPulseOx;
import com.hrs.cc.api.models.requests.set_patient_metricks.temperature.SetTemperature;
import com.hrs.cc.api.models.requests.set_patient_metricks.weight.SetWeight;
import core.PropertyWorker;

import core.rest.RestUtilities;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RestAssistant {
    private static RequestSpecification reqSpec;
    private static ResponseSpecification resPec;
    private static RestUtilities restUtilities;
    private static Map<String, String> userData;
    private static String patientToken;
    private static PropertyWorker propertyWorker;

    static String getPatientByName(String firstName, String lastName) {
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        try {
            if (propertyWorker.getProperty("patient_hrs_id") == null) throw new NullPointerException("id is null");
            return propertyWorker.getProperty("patient_hrs_id");
        } catch (NullPointerException e) {
            restUtilities = new RestUtilities();

            restUtilities.setBaseUri(Path.BASE_URI);
            reqSpec = restUtilities.getRequestSpecification();
            reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

            resPec = restUtilities.getResponseSpecification();

            restUtilities.setContentType(ContentType.JSON);
            restUtilities.setEndPoint();
            reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
            restUtilities.createQueryParams(reqSpec, "status[]",
                    "Activated",
                    "Pre-Activated",
                    "Paused",
                    "Deactivated",
                    "Discharged");
            Response response = restUtilities.getResponse(reqSpec, "get");
            Patients patients = response.as(Patients.class);


            PatientList p = patients.getPatientlist().stream().filter(
                    patient -> patient.get(0).getProfile().getName().getFirst().equals(firstName) &&
                            patient.get(0).getProfile().getName().getLast().equals(lastName)).collect(Collectors.toList()).get(0).get(0);
            try {
                propertyWorker.setProperty("patient_hrs_id", p.getProfile().getHrsid());
            } catch (IOException ignored) {
            }
            return p.getProfile().getHrsid();
        }
    }

    static void setAllMetric(String patientId) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENT));
        resPec = restUtilities.getResponseSpecification();


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.body(new SetWeight("test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(50, 100)));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.body(new SetBloodPressure(
                "test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(0, 100),
                restUtilities.getRandomInt(0, 100),
                restUtilities.getRandomInt(0, 1000)));
        restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.body(new SetPulseOx(
                "test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(0, 100),
                restUtilities.getRandomInt(0, 100)));
        restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.body(new SetTemperature(
                "test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(0, 1000)));
        restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");

        SetGlucose glucose = new SetGlucose();
        glucose.setReason("test reason ".concat(restUtilities.getRandomString()));
        glucose.setFtime(restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        Integer data = restUtilities.getRandomInt(0, 1000);
        glucose.setReading(data);
        glucose.setGlucose(data);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.body(glucose);
        restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");

        restUtilities.removeHeaders(reqSpec);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.body(new SetActivity(
                "test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(0, 1000)));
        restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
    }


    static String getRandomEmail() {
        return "test_".concat(restUtilities.getRandomString(6).concat("@gmail.com"));
    }


    static String getTokenPatient(String patientId) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
//        reqSpec.log().all();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath("");


        MagiCodeIdentityPacient code = new MagiCodeIdentityPacient();
        Data data = new Data();
        data.setIdentity(patientId);
        code.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.MAGIC_CODES);
        reqSpec.body(code);

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath jsonPath = response.jsonPath();


        return getConnectingMC(jsonPath.get("data.code").toString());
    }

    static String getConnectingMC(String mCode) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
//        reqSpec.log().all();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath("");


        MagiCodePacientDevice code = new MagiCodePacientDevice();
        Datum data = new Datum();
        data.setType("code");
        data.setCode(mCode);
        code.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TOKENS_INTG);
        reqSpec.body(code);

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath jsonPath = response.jsonPath();
        patientToken = jsonPath.get("data.token").toString();
        return jsonPath.get("data.token").toString();
    }


    static void deactivateConnection(String patientId) {
        Devices devices = getConnectedDevices(patientId);

        RestUtilities restUtilities = new RestUtilities();

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        ResponseSpecification resPec = restUtilities.getResponseSpecification();

        String devId = devices.getData().get(devices.getData().size() - 1).getId();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TOKENS_INTG.concat(EndPoints.DEVICE_ID));

        reqSpec.header("Authorization", "Bearer ".concat(patientToken));

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(204)
        );

        restUtilities.getResponse(restUtilities.createPathParam(reqSpec, "deviceId", devId), "delete");
    }

    static Devices getConnectedDevices(String patientId) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TASKS_METAS);
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(patientToken));

        Response response = restUtilities.getResponse(
                restUtilities.createQueryParam(
                        reqSpec, "filter[identity]", patientId), "get");

        return response.as(Devices.class);
    }

    static String getAddNewPatient() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENT));
        resPec = restUtilities.getResponseSpecification();

        NewPatient patientData = new NewPatient();

        List<CustomAtributs> customAtributs = new ArrayList<>();

        CustomAtributs first = new CustomAtributs();
        first.setId(1);
        first.setName("Test custom attributes");
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated("2017-08-31T11:39:00+0000");
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomAtributs two = new CustomAtributs();
        two.setId(2);
        two.setName("Nickname");
        two.setType("text");
        two.setRequired(false);
        two.setPatientsetup(true);
        two.set_class("col-xs-6");
        two.setEmrTracked(false);
        two.setLastUpdated("2017-09-25T14:22:07+0000");
        two.set$$hashKey("object:2416");
        two.setValue("testnick");

        CustomAtributs three = new CustomAtributs();
        three.setId(4);
        three.setName("Custom Text Field");
        three.setType("text");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-12");
        three.setEmrTracked(false);
        three.setLastUpdated("2017-10-16T14:44:33+0000");
        three.set$$hashKey("object:2417");
        three.setValue("test custom field");

        customAtributs.add(first);
        customAtributs.add(two);
        customAtributs.add(three);
        patientData.setCustomAtributsList(customAtributs);

        Name name = new Name("testPatientForChat".concat(restUtilities.getRandomString())
                , "test".concat(restUtilities.getRandomString()),
                "test".concat(restUtilities.getRandomString()));
        patientData.setName(name);

        patientData.setDob(restUtilities.getTimePeriod(1).get("current"));
        patientData.setGender("F");
        patientData.setPhone("3423890430284902");
        patientData.setPid(restUtilities.getRandomString());
        reqSpec.body(patientData);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");

        JsonPath jsonPath = response.jsonPath();
        return jsonPath.get("hrsid");
    }
}

package com.hrs.cc.api.tests.reports;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.account_data.clinician.Clinician;
import com.hrs.cc.api.models.patients.Patients;
import com.hrs.cc.api.models.patients.metrics_data.Metrics;
import com.hrs.cc.api.models.patients.metrics_data.PatientMetricsData;
import com.hrs.cc.api.models.patients.patient_list.PatientList;
import com.hrs.cc.api.models.patients.patient_list.Profile;
import core.PropertyWorker;
import core.rest.RestUtilities;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class RestAssistant {
    private static RequestSpecification reqSpec;
    private static ResponseSpecification resPec;
    private static RestUtilities restUtilities;
    private static PropertyWorker propertyWorker;

    static Profile getPatientByName(String firstName, String lastName) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createQueryParam(reqSpec, "status[]", "Activated");
        restUtilities.createQueryParam(reqSpec, "status[]", "Pre-Activated");
        restUtilities.createQueryParam(reqSpec, "status[]", "Paused");
        restUtilities.createQueryParam(reqSpec, "status[]", "Deactivated");
        restUtilities.createQueryParam(reqSpec, "status[]", "Discharged");
        Response response = restUtilities.getResponse(reqSpec, "get");
        Patients patients = response.as(Patients.class);


        PatientList p = patients.getPatientlist().stream().filter(
                patient -> patient.get(0).getProfile().getName().getFirst().equals(firstName) &&
                        patient.get(0).getProfile().getName().getLast().equals(lastName)).collect(Collectors.toList()).get(0).get(0);
        if (p != null) {
            return p.getProfile();
        }
        return null;
    }

    static void testSetClinicianInsecureReportsSetting(Boolean status) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.CLINICIAN));

        resPec = restUtilities.getResponseSpecification();


        Clinician clinician = new Clinician();
        clinician.setClinicianInsecureReports(status);


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(clinician);
//        reqSpec.log().all();
        Response response = restUtilities.getResponse(reqSpec, "post");
//        response.getBody().prettyPrint();
    }

}

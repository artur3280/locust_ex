package com.hrs.cc.api.tests.patients;

import com.hrs.cc.api.connection.ClinicianConnection;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.inventory.InventoryResponse;
import com.hrs.cc.api.models.inventory.device_history.DeviceHistory;
import com.hrs.cc.api.models.patients.education.Education;
import com.hrs.cc.api.models.patients.education.Patientcontent;
import com.hrs.cc.api.models.requests.turn_education_content.SetPatientContent;
import core.PropertyWorker;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

public class TestPatientEducationResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private PropertyWorker propertyWorker;
    private String patientId;
    private static Education education;
    private static List<SetPatientContent> newEducation;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        ClinicianConnection.clinicianAut();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2);
        resPec = restUtilities.getResponseSpecification();
        patientId = Objects.requireNonNull(RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG));
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        reqSpec.body("");
    }


    @Test
    public void testGetEducationsResponse() {
        reqSpec.basePath(Path.API_V2);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.EDUCATION);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtilities.getResponse(reqSpec, "get");

        education = response.as(Education.class);
        assertEquals(education.getStatus(), "ok", "data not eql");

        assertNotNull(education.getAuth().getToken(), "token field is null");

        education.getData().getPatientAllContent().forEach(patientcontent -> {
            assertNotNull(patientcontent.getId(), "Id field is null");
            assertNotNull(patientcontent.getType(), "Type field is null");
            assertNotNull(patientcontent.getCategory(), "Category field is null");
            assertNotNull(patientcontent.getTitle(), "Title field is null");
        });
    }

    @Test(dependsOnMethods = {"testGetEducationsResponse"})
    public void testTurnOnAllEducationsContentForPatient() {

        newEducation = education.getData().getPatientAllContent().stream().map(c -> new SetPatientContent(
                c.getId(),
                c.getType(),
                c.getCategory(),
                c.getTitle(),
                true)).collect(Collectors.toList());

        Map<String, Object> body = new HashMap<>();
        body.put("content", newEducation);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.EDUCATION));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.body(body);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");

        Education education = response.as(Education.class);

        assertEquals(education.getStatus(), "ok", "data not eql");
        assertNotNull(education.getAuth().getToken(), "token is null");

    }

    @Test(dependsOnMethods = {"testGetEducationsResponse", "testTurnOnAllEducationsContentForPatient"})
    public void testGetEducationByPatientId() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.EDUCATION));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "get");
        Education education = response.as(Education.class);
        assertEquals(education.getStatus(), "ok", "data not eql");

        assertNotNull(education.getAuth().getToken(), "token is null");

        education.getData().getPatientcontent().forEach(patientcontent -> {
            assertNotNull(patientcontent.getId(), "Id field is null");
            assertNotNull(patientcontent.getType(), "Type field is null");
            assertNotNull(patientcontent.getCategory(), "Category field is null");
            assertNotNull(patientcontent.getTitle(), "Title field is null");
            assertNotNull(patientcontent.getStatus(), "Status field is null");
        });

    }

    @Test(dependsOnMethods = {"testGetEducationsResponse", "testTurnOnAllEducationsContentForPatient", "testGetEducationByPatientId"})
    public void testTurnOffAllEducationsContentForPatientButTwo() {

        List<SetPatientContent> twoEdications = newEducation.stream().limit(2).collect(Collectors.toList());
        Map<String, Object> body = new HashMap<>();
        body.put("content", twoEdications);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.EDUCATION));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.body(body);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");

        Education education = response.as(Education.class);

        assertEquals(education.getStatus(), "ok", "data not eql");
        assertNotNull(education.getAuth().getToken(), "token is null");

        Education current = RestAssistant.getEducationByPatientId(patientId);
        if (current == null) throw new NullPointerException("Current education obj is null");
        assertEquals(current.getData().getPatientcontent().size(), 2, "data not eql");

        assertTrue(current.getData().getPatientcontent().stream().anyMatch(c -> c.getTitle().equals(twoEdications.get(0).getTitle())));
        assertTrue(current.getData().getPatientcontent().stream().anyMatch(c -> c.getTitle().equals(twoEdications.get(1).getTitle())));
    }

    @Test(dependsOnMethods = {"testGetEducationsResponse", "testTurnOnAllEducationsContentForPatient", "testGetEducationByPatientId", "testTurnOffAllEducationsContentForPatientButTwo"})
    public void testTurnOffAllEducationsContentForPatient() {

        List<SetPatientContent> twoEdications = newEducation.stream().limit(0).collect(Collectors.toList());
        Map<String, Object> body = new HashMap<>();
        body.put("content", twoEdications);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.EDUCATION));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.body(body);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
        Education education = response.as(Education.class);

        assertEquals(education.getStatus(), "ok", "data not eql");
        assertNotNull(education.getAuth().getToken(), "token is null");

        Education current = RestAssistant.getEducationByPatientId(patientId);
        if (current == null) throw new NullPointerException("Current education obj is null");

        assertNull(current.getData(), "Current education data is null");
    }

}
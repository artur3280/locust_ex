package com.hrs.cc.api.tests.inventory;

import com.hrs.cc.api.connection.ClinicianConnection;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.inventory.InventoryResponse;
import com.hrs.cc.api.models.inventory.history.TabletHistory;
import com.hrs.cc.api.models.inventory.tablet_status.TabletStatus;
import core.PropertyWorker;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

public class TestInventoryResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private static Map<String, String> queryParam = new HashMap<>();
    private String deviceId;
    private String deviceName;
    private String deviceEnvironment;
    private String deviceDevid;
    private List<InventoryResponse> inventoryList = new ArrayList<>();
    private static Map<Object, List<InventoryResponse>> groupingInventory;
    private PropertyWorker propertyWorker;

    @BeforeTest
    public void setUp() {
        ClinicianConnection.clinicianAut();
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.INVENTORY));
        resPec = restUtilities.getResponseSpecification();
    }


    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
        reqSpec.body("");
    }

    @Test
    public void testGetListOfAllTabletsInInventory() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");

        inventoryList = Arrays.asList(response.as(InventoryResponse[].class));
        deviceId = inventoryList.get(inventoryList.size() - 2).getId();
        deviceName = inventoryList.get(inventoryList.size() - 2).getName();
        deviceEnvironment = inventoryList.get(inventoryList.size() - 2).getEnvironment();
        deviceDevid = inventoryList.get(inventoryList.size() - 2).getDevid();
        inventoryList.forEach(inventory -> {
            assertNotNull(inventory.getAutoId(), "AutoId field is null");
            assertNotNull(inventory.getId(), "Id field is null");
            assertNotNull(inventory.getName(), "Name field is null");
            assertNotNull(inventory.getEnvironment(), "Environment field is null");
            assertNotNull(inventory.getLastUpdated(), "LastUpdated field is null");
            assertNotNull(inventory.getLastUpdated().getDate(), "LastUpdated.Date field is null");
            assertNotNull(inventory.getLastUpdated().getTimezone(), "LastUpdated.Timezone field is null");
            assertNotNull(inventory.getLastUpdated().getTimezoneType(), "LastUpdated.TimezoneType field is null");
            assertNotNull(inventory.getDevid(), "Devid field is null");
            assertNotNull(inventory.getAssignment(), "Assignment field is null");
            assertNotNull(inventory.getAssignment().getStatus(), "Assignment.Status field is null");
            assertNotNull(inventory.getAssignment().getDays(), "Assignment.Days field is null");
            assertNotNull(inventory.getCreatedAt().getDate(), "CreatedAt.Date field is null");
            assertNotNull(inventory.getCreatedAt().getTimezone(), "CreatedAt.Timezone field is null");
            assertNotNull(inventory.getCreatedAt().getTimezone_type(), "CreatedAt.Timezone_type field is null");
        });

        inventoryList = inventoryList.stream().filter(
                i -> i.getAssignment().getStatus().equals("active") &&
                        Integer.parseInt(i.getAssignment().getDays()) > 35).collect(Collectors.toList());
        if (inventoryList.isEmpty())
            throw new NullPointerException("Inventory list is empty. Does not found any active devices.");

    }

    @Test(dependsOnMethods = {"testGetListOfAllTabletsInInventory"})
    public void testUpdateTabletSubgroup() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.SETTINGS));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        queryParam.put("subgroup", "test subgroup");
        reqSpec.body(queryParam);

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", deviceId), "post");

        JsonPath jsonPath = response.jsonPath();
        assertEquals(jsonPath.get("message"), "Updates ok" , "Message not eql");

        restUtilities.removePathParams(reqSpec);

        reqSpec.body("");

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.INVENTORY))
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token"))));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();

        Response tablets = restUtilities.getResponse(newReqSpec, "get");
        List<InventoryResponse> inventoryList = Arrays.asList(tablets.as(InventoryResponse[].class));

        assertEquals(deviceDevid, inventoryList.get(inventoryList.size() - 2).getDevid(), "deviceDevid not eql");
        assertEquals(deviceId, inventoryList.get(inventoryList.size() - 2).getId(), "deviceId not eql");
        assertEquals(deviceName, inventoryList.get(inventoryList.size() - 2).getName(), "deviceName not eql");
        assertEquals(deviceEnvironment, inventoryList.get(inventoryList.size() - 2).getEnvironment(), "deviceEnvironment not eql");
        assertEquals(queryParam.get("subgroup"), inventoryList.get(inventoryList.size() - 2).getSubgroup(), "subgroup not eql");
        queryParam.clear();
    }

    @Test(dependsOnMethods = {"testGetListOfAllTabletsInInventory", "testUpdateTabletSubgroup"}, priority = 1)
    public void testDeleteTabletSubgroup() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.SETTINGS).concat(EndPoints.SUBGROUP));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", deviceId), "delete");
        JsonPath jsonPath = response.jsonPath();
        assertEquals(jsonPath.get("message"), "Subgroup removed", "Message not eql or device is not deleted");
        assertEquals(response.getStatusCode(), 200);

        restUtilities.removePathParams(reqSpec);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();

        Response tablets = restUtilities.getResponse(reqSpec, "get");
        List<InventoryResponse> inventoryList = Arrays.asList(tablets.as(InventoryResponse[].class));

        assertEquals(deviceDevid, inventoryList.get(inventoryList.size() - 2).getDevid(), "deviceDevid is not eql");
        assertEquals(deviceId, inventoryList.get(inventoryList.size() - 2).getId(), "deviceId is not eql");
        assertEquals(deviceName, inventoryList.get(inventoryList.size() - 2).getName(), "deviceName is not eql");
        assertEquals(deviceEnvironment, inventoryList.get(inventoryList.size() - 2).getEnvironment(), "deviceEnvironment is not eql");
        assertNull(inventoryList.get(inventoryList.size() - 1).getSubgroup(), "subgroup is not eql");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testGetListOfAllTabletsInInventory"})
    public void testGetTabletHistory() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.HISTORY.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", deviceId), "get");
        List<TabletHistory> historyList = Arrays.asList(response.as(TabletHistory[].class));
        historyList.forEach(history -> {
            assertNotNull(history.getName(), "Device name is null");
            assertNotNull(history.getHrsid(), "Patient id is null");
            assertNotNull(history.getTime(), "History time is null");
        });
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testGetListOfAllTabletsInInventory"})
    public void testGetTabletStatus() {

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.STATUS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Response defTablet = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", inventoryList.get(0).getId()), "get");
        TabletStatus defTabletStatus = defTablet.as(TabletStatus.class);

        assertNotNull(defTabletStatus.getId(), "Device id is null");
        assertEquals(defTabletStatus.getId(), inventoryList.get(0).getId(), "Device id not eql");
        assertNotNull(defTabletStatus.getConnected(), "Device status is null");
        assertNotNull(defTabletStatus.getName(), "Device name is null");
        assertEquals(defTabletStatus.getName(), inventoryList.get(0).getName(), "device name is not eql");
        try {
            assertNotNull(defTabletStatus.getPatient());
        } catch (AssertionError e) {
            System.out.println("Tablet does not have patient");
        }
        try {
            assertNotNull(defTabletStatus.getBattery());
            assertNotNull(defTabletStatus.getBattery().getLevel());
            assertNotNull(defTabletStatus.getBattery().getStatus());
        } catch (AssertionError e) {
            System.out.println("Tablet does not have battery info");
        }

        try {
            assertNotNull(defTabletStatus.getNetwork().getStrength());
            assertNotNull(defTabletStatus.getNetwork().getType());
        } catch (AssertionError | NullPointerException e) {
            System.out.println("Tablet does not have connections");
        }
        try {
            assertNotNull(defTabletStatus.getGps());
            assertNotNull(defTabletStatus.getGps().getLatitude());
            assertNotNull(defTabletStatus.getGps().getLongitude());
            assertNotNull(defTabletStatus.getGps().getTime());
        } catch (AssertionError | NullPointerException e) {
            System.out.println("Tablet does not have gps information");
        }
        try {
            assertNotNull(defTabletStatus.getLastUpdated());
            assertNotNull(defTabletStatus.getLastUpdated().getDate());
            assertNotNull(defTabletStatus.getLastUpdated().getTimezone());
            assertNotNull(defTabletStatus.getLastUpdated().getTimezoneType());
        } catch (AssertionError | NullPointerException e) {
            System.out.println("Tablet does not have last updated information");
        }

        restUtilities.removeHeaders(reqSpec, "Authorization");
    }
}

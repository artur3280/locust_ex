package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.account_data.Account;
import com.hrs.cc.api.models.integratioins_app.clinician_auth_token.ClinicianAuthToken;
import com.hrs.cc.api.models.integratioins_app.user_by_type_in_env.Datum;
import com.hrs.cc.api.models.integratioins_app.user_by_type_in_env.UsersByTypeInEnv;
import com.hrs.cc.api.models.requests.add_new_clinicain.Data;
import com.hrs.cc.api.models.requests.add_new_clinicain.NewClinician;
import com.hrs.cc.api.models.requests.add_new_clinicain.Profile;
import com.hrs.cc.api.models.requests.edit_clinician.ClinicianEdit;
import com.hrs.cc.api.models.requests.edit_clinician.ClinicianIfo;
import com.hrs.cc.api.models.requests.edit_clinician.CliniciansEdit;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestGatewayUsersRequests extends Assert {
    private ObjectMapper mapper = new ObjectMapper();
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private PropertyWorker propertyWorker;
    private NewClinician clinician;
    private static String readOnlyToken;
    private static String clinicianId;
    private static String clinUserName;
    private static String clinPass;
    private static String clinicianToken;
    private static String readOnlyId;
    private static UsersByTypeInEnv patientState;
    private static String clinicianA;
    private static String clinicianB;
    private static String clinicianC;
    private static List<Datum> usersForChanging;
    private static String clinicianTokenWithoutSubgroup;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath("");
    }

    @AfterClass
    public void removeObj() {
        mapper = null;
        reqSpec = null;
        resPec = null;
        restUtilities = null;
        queryParam = null;
        userData = null;
        propertyWorker = null;
        System.gc();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
    }


    @Test
    public void testCreateNewClinician() {
        /*Creating clinician*/
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        profile.setRights("Clinician");
        ArrayList subgrp = new ArrayList();
        subgrp.add("west coast");
        profile.setSubgroups(subgrp);

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);


        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.statusCode(), 200);

        /*Auth created clinician*/
        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        assertNotNull(authToken.getData().getToken(), "Token field is not null");
        assertNotNull(authToken.getData().getRefresh(), "Refresh token field is not null");
        clinicianToken = authToken.getData().getToken();

        /*Get info about clinician*/
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(authToken.getData().getToken()));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);

        clinicianId = account.getUser();
        if (clinicianId == null) throw new NullPointerException("Clinician id is null");
        clinUserName = clinician.getData().getUserName();
        if (clinUserName == null) throw new NullPointerException("Clinician name is null");
        clinPass = clinician.getData().getPassword();
        if (clinPass == null) throw new NullPointerException("Clinician pass is null");
    }

    @Test(dependsOnMethods = {"testCreateNewClinician"})
    public void testGetListOfCliniciansGeneralSubgroup() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(clinicianToken));
        restUtilities.createQueryParam(reqSpec, "filter[type]", "clinician");
        restUtilities.createQueryParam(reqSpec, "filter[environment]", "QATestingZone");

        Response response = restUtilities.getResponse(reqSpec, "get");
        UsersByTypeInEnv clinicians = response.as(UsersByTypeInEnv.class);
        boolean check = clinicians.getData().stream().allMatch(c -> c.getProfile().getSubgroup().equals("west coast"));
        assertTrue(check, "Some clinicians not eql west coast");

        restUtilities.removeQueryParam(reqSpec, "filter[type]");
        restUtilities.removeQueryParam(reqSpec, "filter[environment]");

    }

    @Test(dependsOnMethods = {"testCreateNewClinician",
            "testGetListOfCliniciansGeneralSubgroup"})
    public void testCreateExistClinician() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(422));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(clinician);

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("message"), "The given data was invalid.", "Message is not eql");
        assertEquals(path.get("errors['data.user.uniqueLogin']"), "Invalid data.user, a login with that name already exist.", "Message is not eql");
    }

    @Test(dependsOnMethods = {
            "testCreateNewClinician",
            "testGetListOfCliniciansGeneralSubgroup",
            "testCreateExistClinician"
    })
    public void testClinicianCanNotRemoveAnotherClinician() {
        String clinicianId = RestAssistant.getEnabledClinicians().getData().get(0).getId();
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(403));

        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(clinicianToken));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);

        Response response = restUtilities.getResponse(reqSpec, "delete");
        assertEquals(response.statusCode(), 403);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("message"), "This action is unauthorized.", "Message is not eql");

    }

    @Test(dependsOnMethods = {
            "testCreateNewClinician",
            "testGetListOfCliniciansGeneralSubgroup",
            "testCreateExistClinician",
            "testClinicianCanNotRemoveAnotherClinician"
    })
    public void testModeratorDeletingClinician() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(204));

        reqSpec = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);

        Response response = restUtilities.getResponse(reqSpec, "delete");
        assertEquals(response.statusCode(), 204);

        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(422));

        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        JsonPath path = re.jsonPath();
        assertEquals(path.get("message"), "user account disabled", "Message is not eql");

    }

    @Test
    public void testCreateNewReadOnlyClinician() {
        clinician = null;
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        profile.setRights("ReadOnly");
        ArrayList subgrp = new ArrayList();
        subgrp.add("west coast");
        profile.setSubgroups(subgrp);

        data.setUserName("Test reaad only clinician " + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);

        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.statusCode(), 200);

        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        assertNotNull(authToken.getData().getToken(), "token is null");
        assertNotNull(authToken.getData().getRefresh(), "refresh token is null");
        readOnlyToken = authToken.getData().getToken();

        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(authToken.getData().getToken()));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);

        readOnlyId = account.getUser();
        if (readOnlyId == null) throw new NullPointerException("Read only id is null");
    }

    @Test(dependsOnMethods = {
            "testCreateNewReadOnlyClinician"})
    public void testReadOnlyCanNotRemoveClinician() {
        String clinicianId = RestAssistant.getEnabledClinicians().getData().get(0).getId();
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(403));

        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(readOnlyToken));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);

        Response response = restUtilities.getResponse(reqSpec, "delete");
        assertEquals(response.statusCode(), 403);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("message"), "This action is unauthorized.", "Message is not eql");
    }

    @Test(dependsOnMethods = {
            "testCreateNewReadOnlyClinician",
            "testReadOnlyCanNotRemoveClinician"})
    public void testCreateExistReadOnlyClinician() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(422));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(clinician);


        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("message"), "The given data was invalid.", "Message is not eql");
        assertEquals(path.get("errors['data.user.uniqueLogin']"), "Invalid data.user, a login with that name already exist.", "Message is not eql");
    }

    @Test(dependsOnMethods = {
            "testCreateNewReadOnlyClinician",
            "testReadOnlyCanNotRemoveClinician",
            "testCreateExistReadOnlyClinician"
    })
    public void testModeratorDeletingReadOnly() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(204));

        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.createPathParam(reqSpec, "id", readOnlyId);

        Response response = restUtilities.getResponse(reqSpec, "delete");
        assertEquals(response.statusCode(), 204);

        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(422));

        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        JsonPath path = re.jsonPath();
        assertEquals(path.get("message"), "user account disabled", "Message is not eql");

    }

    @Test(priority = 3)
    public void testGetClinicianByEnvironment() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.createQueryParam(reqSpec, "filter[type]", "clinician");
        restUtilities.createQueryParam(reqSpec, "filter[environment]", "QATestingZone");

        Response response = restUtilities.getResponse(reqSpec, "get");
        patientState = response.as(UsersByTypeInEnv.class);
        patientState.getData().forEach(datum -> {
            assertNotNull(datum.getId(), "Id field is null");
            assertNotNull(datum.getType(), "Type field is null");
            assertNotNull(datum.getEnvironment(), "Environment field is null");
            assertNotNull(datum.getProfile().getRights(), "Profile.Rights field is null");
            assertNotNull(datum.getProfile().getSubgroup(), "Profile.Subgroup field is null");
        });

        restUtilities.removeQueryParam(reqSpec, "filter[type]");
        restUtilities.removeQueryParam(reqSpec, "filter[environment]");
    }

    @Test
    public void testModeratorDeletingHimself() {
        String moderatorId = RestAssistant.getAdminId();
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(403));

        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.createPathParam(reqSpec, "id", moderatorId);

        Response response = restUtilities.getResponse(reqSpec, "delete");
        assertEquals(response.statusCode(), 403);

    }

    @Test(priority = 3)
    public void testEditSingleClinician() {
        clinicianA = RestAssistant.addClinician();
        restUtilities.getResponseSpecification(
                restUtilities.getResponseBuilder()
                        .expectContentType(ContentType.JSON)
                        .expectStatusCode(204));


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianA);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        ClinicianEdit clinicianEdit = new ClinicianEdit();
        com.hrs.cc.api.models.requests.edit_clinician.Datum data = new com.hrs.cc.api.models.requests.edit_clinician.Datum();
        data.setFirstName("Edited FN".concat(RandomData.getRandomString(5)));
        data.setMiddleName("Edited MN".concat(RandomData.getRandomString(5)));
        data.setLastName("Edited MN".concat(RandomData.getRandomString(5)));
        data.setPassword("2398472893".concat(RandomData.getRandomString(5)));
        data.setEmail("test@test".concat(RandomData.getRandomString(5).concat(".gmail.com")));
        data.setPhone("123123".concat(RandomData.getRandomInt(1000).toString()));
        com.hrs.cc.api.models.requests.edit_clinician.Profile profile = new com.hrs.cc.api.models.requests.edit_clinician.Profile();
        profile.setRights("Moderator");
        ArrayList<String> subGroup = new ArrayList<>();
        subGroup.add("west coast");
        profile.setSubgroups(subGroup);
        data.setProfile(profile);
        clinicianEdit.setData(data);

        reqSpec.body(clinicianEdit);
        restUtilities.getResponse(reqSpec, "patch");

        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + RestAssistant.clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinicianEdit.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        assertNotNull(authToken.getData().getToken(), "Token field is null");
        assertNotNull(authToken.getData().getRefresh(), "Refresh field is null");
        clinicianToken = authToken.getData().getToken();
        if(clinicianToken == null)throw new NullPointerException("Clinician token is null");
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(authToken.getData().getToken()));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);

        assertEquals(clinicianEdit.getData().getFirstName(), account.getFirstname(), "Firstname not eql");
        assertEquals(clinicianEdit.getData().getLastName(), account.getLastname(), "Lastname not eql");
        assertEquals(clinicianEdit.getData().getProfile().getSubgroups().get(0), account.getSubgroup(), "Subgroup not eql");
    }

    @Test(dependsOnMethods = {"testEditSingleClinician"}, priority = 3)
    public void testEditListClinicians() {
        clinicianB = RestAssistant.addClinician();
        clinicianC = RestAssistant.addClinician();
        restUtilities.getResponseSpecification(
                restUtilities.getResponseBuilder()
                        .expectContentType(ContentType.JSON)
                        .expectStatusCode(204));


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);

        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        CliniciansEdit cliniciansEdit = new CliniciansEdit();
        ClinicianIfo dataA = new ClinicianIfo();
        dataA.setFirstName("Edited FN".concat(RandomData.getRandomString(5)));
        dataA.setMiddleName("Edited MN".concat(RandomData.getRandomString(5)));
        dataA.setLastName("Edited MN".concat(RandomData.getRandomString(5)));
        dataA.setPassword("2398472893".concat(RandomData.getRandomString(5)));
        dataA.setEmail("test@test".concat(RandomData.getRandomString(5).concat(".gmail.com")));
        dataA.setPhone("123123".concat(RandomData.getRandomInt(1000).toString()));
        dataA.setHrsid(clinicianB);
        com.hrs.cc.api.models.requests.edit_clinician.Profile profile = new com.hrs.cc.api.models.requests.edit_clinician.Profile();
        profile.setRights("Moderator");
        ArrayList<String> subGroupA = new ArrayList<>();
        subGroupA.add("west coast");
        profile.setSubgroups(subGroupA);
        dataA.setProfile(profile);


        ClinicianIfo dataB = new ClinicianIfo();
        dataB.setFirstName("Edited FN".concat(RandomData.getRandomString(5)));
        dataB.setMiddleName("Edited MN".concat(RandomData.getRandomString(5)));
        dataB.setLastName("Edited MN".concat(RandomData.getRandomString(5)));
        dataB.setPassword("2398472893".concat(RandomData.getRandomString(5)));
        dataB.setEmail("test@test".concat(RandomData.getRandomString(5).concat(".gmail.com")));
        dataB.setPhone("123123".concat(RandomData.getRandomInt(1000).toString()));
        dataB.setHrsid(clinicianC);
        com.hrs.cc.api.models.requests.edit_clinician.Profile profileB = new com.hrs.cc.api.models.requests.edit_clinician.Profile();
        profileB.setRights("Moderator");
        ArrayList<String> subGroupB = new ArrayList<>();
        subGroupB.add("west coast");
        profileB.setSubgroups(subGroupB);
        dataB.setProfile(profileB);

        ArrayList<ClinicianIfo> list = new ArrayList<>();
        list.add(dataA);
        list.add(dataB);
        cliniciansEdit.setData(list);

        reqSpec.body(cliniciansEdit);
        restUtilities.getResponse(reqSpec, "patch");
    }

    @Test(dependsOnMethods = {"testEditSingleClinician", "testCreateNewClinicianWithoutSubgroup", "testEditSingleClinicianWithoutSubgroup", "testEditListClinicians"}, priority = 3)
    public void testRemoveListClinicians() {

        restUtilities.getResponseSpecification(
                restUtilities.getResponseBuilder()
                        .expectContentType(ContentType.JSON)
                        .expectStatusCode(204));


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);

        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Map<String, Object> list = new HashMap<>();
        ArrayList<String> ids = new ArrayList<>();
        ids.add(clinicianA);
        ids.add(clinicianB);
        ids.add(clinicianC);
        list.put("data", ids);

        reqSpec.body(list);
        restUtilities.getResponse(reqSpec, "delete");

    }

    @Test(priority = 4)
    public void testCreateNewClinicianWithoutSubgroup() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        clinician = new NewClinician();
        Data data = new Data();
        Profile profile = new Profile();

        profile.setRights("Clinician");

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);


        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.statusCode(), 200);

        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        assertNotNull(authToken.getData().getToken(), "token is null");
        assertNotNull(authToken.getData().getRefresh(), "refresh token is null");
        clinicianTokenWithoutSubgroup = authToken.getData().getToken();
        if(clinicianTokenWithoutSubgroup == null)throw new NullPointerException("clinicianTokenWithoutSubgroup is null");
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(authToken.getData().getToken()));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);
        assertNull(account.getSubgroup(), "subgroup is not null");
        clinicianId = account.getUser();

    }

    @Test(dependsOnMethods = {"testCreateNewClinicianWithoutSubgroup"})
    public void testGetListOfCliniciansDifferentSubgroupWhenClinicianWithoutSubgroup() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(clinicianTokenWithoutSubgroup));
        restUtilities.createQueryParam(reqSpec, "filter[type]", "clinician");
        restUtilities.createQueryParam(reqSpec, "filter[environment]", "QATestingZone");

        Response response = restUtilities.getResponse(reqSpec, "get");
        UsersByTypeInEnv clinicians = response.as(UsersByTypeInEnv.class);
        boolean check = clinicians.getData().stream().anyMatch(
                c -> c.getProfile().getSubgroup().equals("west coast") || c.getProfile().getSubgroup().equals(""));
        assertTrue(check, "clinicians is not difference subgroup");

        restUtilities.removeQueryParam(reqSpec, "filter[type]");
        restUtilities.removeQueryParam(reqSpec, "filter[environment]");

    }

    @Test(dependsOnMethods = {"testCreateNewClinicianWithoutSubgroup"}, priority = 4)
    public void testEditSingleClinicianWithoutSubgroup() {
        clinicianA = RestAssistant.addClinician();
        restUtilities.getResponseSpecification(
                restUtilities.getResponseBuilder()
                        .expectContentType(ContentType.JSON)
                        .expectStatusCode(204));


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", clinicianA);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        ClinicianEdit clinicianEdit = new ClinicianEdit();
        com.hrs.cc.api.models.requests.edit_clinician.Datum data = new com.hrs.cc.api.models.requests.edit_clinician.Datum();
        data.setFirstName("Edited FN".concat(RandomData.getRandomString(5)));
        data.setMiddleName("Edited MN".concat(RandomData.getRandomString(5)));
        data.setLastName("Edited MN".concat(RandomData.getRandomString(5)));
        data.setPassword("2398472893".concat(RandomData.getRandomString(5)));
        data.setEmail("test@test".concat(RandomData.getRandomString(5).concat(".gmail.com")));
        data.setPhone("123123".concat(RandomData.getRandomInt(1000).toString()));
        com.hrs.cc.api.models.requests.edit_clinician.Profile profile = new com.hrs.cc.api.models.requests.edit_clinician.Profile();
        profile.setRights("Moderator");

        data.setProfile(profile);
        clinicianEdit.setData(data);

        reqSpec.body(clinicianEdit);
        restUtilities.getResponse(reqSpec, "patch");

        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + RestAssistant.clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinicianEdit.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        assertNotNull(authToken.getData().getToken(), "token is null");
        assertNotNull(authToken.getData().getRefresh(), "refresh token is null ");
        clinicianToken = authToken.getData().getToken();
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(authToken.getData().getToken()));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);
        assertNotNull(account.getSubgroup(), "subgroup is null");
    }

    @Test(dependsOnMethods = {"testGetClinicianByEnvironment"}, priority = 5)
    public void testMultipleChangingReadOnlyUsersToClinicianType() {
        usersForChanging = patientState.getData().stream()
                .filter(
                        u -> u.getLastName().contains("Test last") &&
                                u.getFirstName().contains("Test first") &&
                                u.getMiddleName().contains("Test middle") &&
                                u.getProfile().getRights().equals("ReadOnly")).limit(2).collect(Collectors.toList());

        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));

        Map<Object, Object> body = new HashMap<>();
        List<Object> dataList = new ArrayList<>();

        Map<String, Object> dataUserOne = new HashMap<>();
        Map<String, String> profile = new HashMap<>();
        profile.put("rights", "ReadOnly");
        dataUserOne.put("hrsid", usersForChanging.get(0).getId());
        dataUserOne.put("profile", profile);
        dataList.add(dataUserOne);

        Map<String, Object> dataUserTwo = new HashMap<>();
        dataUserTwo.put("hrsid", usersForChanging.get(1).getId());
        dataUserTwo.put("profile", profile);
        dataList.add(dataUserTwo);

        body.put("data", dataList);
        reqSpec.body(body);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.getResponse(reqSpec, "patch");

        boolean checkUsers = RestAssistant.getEnabledClinicians()
                .getData()
                .stream()
                .filter(u ->
                        u.getId().equals(usersForChanging.get(0).getId()) &&
                                u.getId().equals(usersForChanging.get(1).getId()))
                .allMatch(u -> u.getProfile().getRights().equals("ReadOnly"));
        assertTrue(checkUsers, "Readonly clinician not changed");


    }

    @Test(dependsOnMethods = {"testGetClinicianByEnvironment", "testMultipleChangingReadOnlyUsersToClinicianType"}, priority = 5)
    public void testMultipleChangingClinicianUsersToReadOnlyType() {
        restUtilities.getResponseSpecification(restUtilities.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));

        Map<Object, Object> body = new HashMap<>();
        List<Object> dataList = new ArrayList<>();

        Map<String, Object> dataUserOne = new HashMap<>();
        Map<String, String> profile = new HashMap<>();
        profile.put("rights", "Clinician");
        dataUserOne.put("hrsid", usersForChanging.get(0).getId());
        dataUserOne.put("profile", profile);
        dataList.add(dataUserOne);

        Map<String, Object> dataUserTwo = new HashMap<>();
        dataUserTwo.put("hrsid", usersForChanging.get(1).getId());
        dataUserTwo.put("profile", profile);
        dataList.add(dataUserTwo);

        body.put("data", dataList);
        reqSpec.body(body);

        restUtilities.setContentType(ContentType.JSON);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.setEndPoint(EndPoints.USERS);
        restUtilities.getResponse(reqSpec, "patch");

        boolean checkUsers = RestAssistant.getEnabledClinicians()
                .getData()
                .stream()
                .filter(u ->
                        u.getId().equals(usersForChanging.get(0).getId()) &&
                                u.getId().equals(usersForChanging.get(1).getId()))
                .allMatch(u -> u.getProfile().getRights().equals("clinician"));
        assertTrue(checkUsers, "Type of clinicians are not changed");
    }

    @Test(dependsOnMethods = {"testGetClinicianByEnvironment"}, priority = 5)
    public void testRemoveInactiveClinicians() {
        List<Datum> usersForRemoving = patientState.getData().stream()
                .filter(
                        u -> u.getLastName().contains("Test last") &&
                                u.getFirstName().contains("Test first") &&
                                u.getMiddleName().contains("Test middle") &&
                                u.getStatus().equals("inactive")).limit(2).collect(Collectors.toList());

        restUtilities.getResponseSpecification(
                restUtilities.getResponseBuilder()
                        .expectContentType(ContentType.JSON)
                        .expectStatusCode(204));


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);

        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        usersForRemoving.forEach(u -> {
            System.out.println(u.getFirstName() + "/" + u.getLastName());
        });

        Map<String, Object> list = new HashMap<>();
        ArrayList<String> ids = new ArrayList<>();
        ids.add(usersForRemoving.get(0).getId());
        ids.add(usersForRemoving.get(1).getId());
        list.put("data", ids);

        reqSpec.body(list);
        restUtilities.getResponse(reqSpec, "delete");

    }

}
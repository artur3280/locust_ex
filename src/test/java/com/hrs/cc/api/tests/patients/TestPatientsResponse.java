package com.hrs.cc.api.tests.patients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Modules;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.patients.Patients;
import com.hrs.cc.api.models.patients.patient_list.*;
import com.hrs.cc.api.models.patients.patient_metric.PatientMetric;
import com.hrs.cc.api.models.patients.patient_metric.response_after_add.MetricksResponse;
import com.hrs.cc.api.models.patients.patient_notes.PatientNote;
import com.hrs.cc.api.models.patients.profile.PatientProfile;
import com.hrs.cc.api.models.requests.set_patient_metricks.Schedule;
import com.hrs.cc.api.models.requests.set_patient_metricks.*;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class TestPatientsResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private ObjectMapper mapper = new ObjectMapper();

    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;

    private String patientWithOverWievData;
    private PropertyWorker propertyWorker;
    private static List<List<PatientList>> patientsList;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
        Logger.debugging(false);
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
        reqSpec.body("");
    }


    @Test
    public void testPatientsResponse() {
        restUtilities.createQueryParam(reqSpec, "status[]", "Activated");
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
        Patients patients = response.as(Patients.class);

        patientsList = patients.getPatientlist().stream().filter(
                patient -> !patient.get(0).getProfile().getHrsid().
                        equals(propertyWorker.getProperty("patient_hrs_id")) &&
                        patient.get(0).getProfile().getClinician() != null &&
                        patient.get(0).getProfile().getName().getLast().contains("test") &&
                        patient.get(0).getProfile().getClinician().getName().
                                equals(Auth.FIRST_NAME + " " + Auth.LAST_NAME) &&
                        patient.get(0).getDay() >= 1 &&
                        patient.get(0).getProfile().getStatus().equals("activated")).collect(Collectors.toList());

        patients.getPatientlist().forEach(patientLists -> {
            assertNotNull(patientLists.get(0).getProfile());
            assertNotNull(patientLists.get(0).getAdditionalProperties());
            assertNotNull(patientLists.get(0).getDay());
            assertNotNull(patientLists.get(0).getRisk());
            assertNotNull(patientLists.get(0).getTracking());
            assertNotNull(patientLists.get(0).getTs());
            try {
                assertNotNull(patientLists.get(0).getProfile().getClinician());
            } catch (AssertionError e) {
                System.out.println("Patient ".concat(patientLists.get(0).getProfile().getHrsid())
                        .concat(" does not attached to clinicians!! Please check!"));
            }
            try {
                assertNotNull(patientLists.get(0).getProfile().getExtension());
            } catch (AssertionError e) {
                System.out.println("Patient ".concat(patientLists.get(0).getProfile().getHrsid())
                        .concat(" does not have Extension!! Please check!"));
            }
            assertNotNull(patientLists.get(0).getProfile().getGender());
            assertNotNull(patientLists.get(0).getProfile().getReadmission());
            assertNotNull(patientLists.get(0).getProfile().getEdvisit());
            assertNotNull(patientLists.get(0).getProfile().getTestpatient());
            assertNotNull(patientLists.get(0).getProfile().getPid());
            assertNotNull(patientLists.get(0).getProfile().getHrsid());
            assertNotNull(patientLists.get(0).getProfile().getStartdate());
            assertNotNull(patientLists.get(0).getProfile().getLastmoduleupdate());
            assertNotNull(patientLists.get(0).getProfile().getVolume());
            assertNotNull(patientLists.get(0).getProfile().getName());
            assertNotNull(patientLists.get(0).getProfile().getConditions());
            assertNotNull(patientLists.get(0).getProfile().getConditions());
            assertNotNull(patientLists.get(0).getProfile().getAudioreminders());
            assertNotNull(patientLists.get(0).getProfile().getStatus());
            assertNotNull(patientLists.get(0).getProfile().getReviewed());
            try {
                assertNotNull(patientLists.get(0).getProfile().getPcv());
                assertNotNull(patientLists.get(0).getProfile().getPcv().getPhone());
                assertNotNull(patientLists.get(0).getProfile().getPcv().getActive());
            } catch (AssertionError e) {
                System.out.println("Patient does not have PCV field");
            }

            Risk detailPatient = null;
            try {
                detailPatient = mapper.convertValue(
                        patientLists.get(0).getRisk().get(0), Risk.class);
                detailPatient.getDetails().forEach(detail -> {
                    assertNotNull(detail);
                    assertNotNull(detail.getMetricIds());
                    assertNotNull(detail.getText());
                    assertNotNull(detail.getMetrics());
                    assertNotNull(detail.getType());
                });
            } catch (NullPointerException | IndexOutOfBoundsException e) {
                System.out.println("Patient does not have detail field!");
            }

            Metrics metricsPatient = null;
            try {
                metricsPatient = mapper.convertValue(patientLists.get(0).getMetrics(), Metrics.class);
                metricsPatient.getSurvey().getQuestions().forEach(question -> {
                    assertNotNull(question.getSchedule());
                    assertNotNull(question.getSchedule().getType());
                    assertNotNull(question.getQuestion());
                    assertNotNull(question.getId());
                    assertNotNull(question.getTs());
                });

                patientWithOverWievData = patientLists.get(0).getProfile().getHrsid();
            } catch (NullPointerException | IllegalArgumentException e) {
                System.out.println("The patient does not have metrics");
            }

            try {
                Medication medicationsPatient = mapper.convertValue(metricsPatient.getMedications(), Medication.class);
                medicationsPatient.getMedications().forEach(medicationItem -> {
                    assertNotNull(medicationItem.getSchedule());
                    assertNotNull(medicationItem.getSchedule().getType());
                    assertNotNull(medicationItem.getDose());
                    assertNotNull(medicationItem.getTime());
                    assertNotNull(medicationItem.getMed());
                });
            } catch (NullPointerException | IllegalArgumentException e) {
                System.out.println("The medication of patient is empty!");
            }

            assertNotNull(patientLists.get(0).getTracking());
            assertNotNull(patientLists.get(0).getTracking().getData());
            assertNotNull(patientLists.get(0).getTracking().getData().getMedlist());
            assertNotNull(patientLists.get(0).getTracking().getData().getTracking());
            assertNotNull(patientLists.get(0).getTracking().getData().getVideo());
            assertNotNull(patientLists.get(0).getTracking().getData().getQuiz());
            assertNotNull(patientLists.get(0).getTracking().getData().getReminders());
            assertNotNull(patientLists.get(0).getDay());
            assertNotNull(patientLists.get(0).getTs());
        });

        restUtilities.removeQueryParam(reqSpec, "status[]");
        restUtilities.setContentType(ContentType.JSON);
    }

    @Test(dependsOnMethods = {"testPatientsResponse"})
    public void testPatientResponse() {
        RestAssistant.setTimeZone(patientsList.get(0).get(0).getProfile().getHrsid(), "American/Denver");

        Map<String, String> time = restUtilities.getTimePeriod(10);
        restUtilities.configDecode(reqSpec, true);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid());
        queryParam.put("start", time.get("current").replace("-", ""));
        queryParam.put("end", time.get("plusDays").replace("-", ""));

        Response response = restUtilities.getResponse(restUtilities.createQueryParam(reqSpec, queryParam), "get");

        ArrayList profile = response.jsonPath().get("profile");
        ArrayList metrics = response.jsonPath().get("metrics");
        ArrayList tracking = response.jsonPath().get("tracking");

        Profile patientProfile = mapper.convertValue(profile.get(0), Profile.class);

        assertEquals(patientProfile.getTimezone(), "American/Denver");
        assertEquals(patientsList.get(0).get(0).getProfile().getClinician().getHrsid(),
                patientProfile.getClinician().getHrsid());
        assertEquals(patientsList.get(0).get(0).getProfile().getSubgroup(),
                patientProfile.getSubgroup());
        assertEquals(patientsList.get(0).get(0).getProfile().getSubgroup(),
                patientProfile.getSubgroup());
        assertEquals(patientsList.get(0).get(0).getProfile().getName(),
                patientProfile.getName());

        Metrics patientMetrics = mapper.convertValue(metrics.get(1), Metrics.class);
        assertNotNull(patientMetrics.getSurvey());
        assertNotNull(patientMetrics.getSurvey().getQuestions());

        patientMetrics.getSurvey().getQuestions().forEach(q -> {
            assertNotNull(q.getSchedule());
            assertNotNull(q.getSchedule().getType());
            assertNotNull(q.getQuestion());
            try {
                assertNotNull(q.getToday());
            } catch (AssertionError e) {
                System.out.println("the today field not found in the survey " + q.getQuestion() + "/ " + q.getId());
            }
            assertNotNull(q.getId());
            assertNotNull(q.getTs());
            try {
                assertTrue(q.getDischarge());
            } catch (NullPointerException e) {
                System.out.println("the discharge field not found in the survey " + q.getQuestion() + "/ " + q.getId());
            }
        });

        Tracking trackingPatient = mapper.convertValue(tracking.get(1), Tracking.class);
        assertNotNull(trackingPatient.getData().getMedlist());
        assertNotNull(trackingPatient.getData().getQuiz().toString());
        assertNotNull(trackingPatient.getData().getReminders().toString());
        assertNotNull(trackingPatient.getData().getTracking().toString());
        assertNotNull(trackingPatient.getData().getVideo().toString());

    }

    @Test(dependsOnMethods = {"testPatientsResponse"})
    public void testPatientMetricResponse() {
        queryParam.put("id", patientsList.get(0).get(0).getProfile().getHrsid());
        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT)));
//        newReqSpec.log().all();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        restUtilities.createQueryParam(newReqSpec, "type", "weight");
        restUtilities.createQueryParam(newReqSpec, "criteria", "first");
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(newReqSpec, queryParam), "get");
        PatientMetric patientMetric = response.as(PatientMetric.class);
        try {
            assertNotNull(patientMetric.getId());
        } catch (AssertionError e) {
            System.out.println("Object response does not have metric id. Please check!");
        }
        try {
            assertNotNull(patientMetric.getPatient());
        } catch (AssertionError e) {
            System.out.println("Does not have patient!");
        }
        try {
            assertNotNull(patientMetric.getClinician());
        } catch (AssertionError e) {
            System.out.println("Does not have clinician!");
        }
        try {
            assertNotNull(patientMetric.getType());
        } catch (AssertionError e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getWeight());
        } catch (AssertionError e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getReminder());
        } catch (AssertionError e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getFinished());
        } catch (AssertionError e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getTime());
        } catch (AssertionError e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getTime().getDate());
        } catch (Exception e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getTime().getTimezoneType());
        } catch (NullPointerException e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getTime().getTimezone());
        } catch (NullPointerException e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getStatus());
        } catch (AssertionError e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getLastUpdated());
        } catch (AssertionError e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getLastUpdated().getDate());
        } catch (NullPointerException e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getLastUpdated().getTimezone());
        } catch (NullPointerException e) {
            System.out.println("fail");
        }
        try {
            assertNotNull(patientMetric.getLastUpdated().getTimezoneType());
        } catch (NullPointerException e) {
            System.out.println("fail");
        }
    }

    @Test(dependsOnMethods = {
            "testPatientsResponse",
            "testPausePatientWithOnlyReadmittedField",
            "testPausePatientWithFieldsAbsent",
            "testPausePatientWithOnlyEdVisitField",
            "testPausePatientWithAllFields",
            "testPausePatient",
            "testDeactivatePatient",
            "testReactivatePatient",
            "testDischargePatient",
            "testSetMetricsToPatient",
            "testGetPatientTodayOverviewData",
            "testGetPatient14DaysOverviewData"})
    public void testGetPatient7DaysOverviewData() {
        Map<String, String> date = restUtilities.getTimePeriod(7);
        String start = date.get("current").replace("-", "");
        String end = date.get("plusDays").replace("-", "");

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        queryParam.put("start", start);
        queryParam.put("end", end);
        restUtilities.createQueryParam(reqSpec, queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(
                        reqSpec, "id",
                        patientWithOverWievData),
                "get");
        List<PatientList> profileHistory = Arrays.asList(response.as(PatientList[].class));

        profileHistory.forEach(history -> {
            assertNotNull(history.getProfile());
            try {
                assertNotNull(history.getProfile().getClinician());
                assertNotNull(history.getProfile().getClinician().getName());
                assertNotNull(history.getProfile().getClinician().getHrsid());
            } catch (AssertionError e) {
                System.out.println("Patient does not have clinician history data!!");
            }

            try {
                assertNotNull(history.getProfile().getDevicehistory());
                history.getProfile().getDevicehistory().forEach(devicehistory -> {
                    assertNotNull(devicehistory.getDevid());
                    assertNotNull(devicehistory.getName());
                    assertNotNull(devicehistory.getTs());
                });
            } catch (AssertionError e) {
                System.out.println("Patient does not have device history data!!");
            }

            assertNotNull(history.getProfile().getExtension());
            assertNotNull(history.getProfile().getReadmission());
            assertNotNull(history.getProfile().getEdvisit());
            assertNotNull(history.getProfile().getTestpatient());
            assertNotNull(history.getProfile().getPid());
            assertNotNull(history.getProfile().getLanguage());
            assertNotNull(history.getProfile().getLastmoduleupdate());
            assertNotNull(history.getProfile().getVolume());
//            assertNotNull(history.getProfile().getPhone());
            assertNotNull(history.getProfile().getName());
            assertNotNull(history.getProfile().getEnvphone());
            assertNotNull(history.getProfile().getConditions());
            assertNotNull(history.getProfile().getAudioreminders());
            assertNotNull(history.getProfile().getStatus());

            assertNotNull(history.getMetrics());
            assertNotNull(history.getTracking());
            assertNotNull(history.getTracking().getData());
            assertNotNull(history.getTracking().getData().getMedlist());
            assertNotNull(history.getTracking().getData().getQuiz());
            assertNotNull(history.getTracking().getData().getReminders());
            assertNotNull(history.getTracking().getData().getTracking());
            assertNotNull(history.getTracking().getData().getVideo());
        });
        queryParam.clear();
        restUtilities.removeQueryParam(reqSpec, "start");
        restUtilities.removeQueryParam(reqSpec, "end");
    }

    @Test(dependsOnMethods = {"testPatientsResponse",
            "testPausePatient",
            "testDeactivatePatient",
            "testReactivatePatient",
            "testDischargePatient",
            "testSetMetricsToPatient",
            "testGetPatientTodayOverviewData"})
    public void testGetPatient14DaysOverviewData() {
        Map<String, String> date = restUtilities.getTimePeriod(14);
        String start = date.get("current").replace("-", "");
        String end = date.get("plusDays").replace("-", "");

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        queryParam.put("start", start);
        queryParam.put("end", end);
        restUtilities.createQueryParam(reqSpec, queryParam);
        Response response = restUtilities.getResponse(restUtilities.createPathParam(reqSpec, "id", patientWithOverWievData), "get");
        List<PatientList> profileHistory = Arrays.asList(response.as(PatientList[].class));

        profileHistory.forEach(history -> {
            assertNotNull(history.getProfile());
            try {
                assertNotNull(history.getProfile().getClinician());
                assertNotNull(history.getProfile().getClinician().getName());
                assertNotNull(history.getProfile().getClinician().getHrsid());
            } catch (AssertionError e) {
                System.out.println("Patient does not have clinician history data!!");
            }

            try {
                assertNotNull(history.getProfile().getDevicehistory());
                history.getProfile().getDevicehistory().forEach(devicehistory -> {
                    assertNotNull(devicehistory.getDevid());
                    assertNotNull(devicehistory.getName());
                    assertNotNull(devicehistory.getTs());
                });
            } catch (AssertionError e) {
                System.out.println("Patient does not have device history data!!");
            }

            assertNotNull(history.getProfile().getExtension());
            assertNotNull(history.getProfile().getReadmission());
            assertNotNull(history.getProfile().getEdvisit());
            assertNotNull(history.getProfile().getTestpatient());
            assertNotNull(history.getProfile().getPid());
            assertNotNull(history.getProfile().getLanguage());
            assertNotNull(history.getProfile().getLastmoduleupdate());
            assertNotNull(history.getProfile().getVolume());
//            assertNotNull(history.getProfile().getPhone());
            assertNotNull(history.getProfile().getName());
            assertNotNull(history.getProfile().getEnvphone());
            assertNotNull(history.getProfile().getConditions());
            assertNotNull(history.getProfile().getAudioreminders());
            assertNotNull(history.getProfile().getStatus());

            assertNotNull(history.getMetrics());
            assertNotNull(history.getTracking());
            assertNotNull(history.getTracking().getData());
            assertNotNull(history.getTracking().getData().getMedlist());
            assertNotNull(history.getTracking().getData().getQuiz());
            assertNotNull(history.getTracking().getData().getReminders());
            assertNotNull(history.getTracking().getData().getTracking());
            assertNotNull(history.getTracking().getData().getVideo());
        });
        queryParam.clear();
        restUtilities.removeQueryParam(reqSpec, "start");
        restUtilities.removeQueryParam(reqSpec, "end");
    }

    @Test(dependsOnMethods = {"testPatientsResponse"})
    public void testPausePatient() {
        queryParam.put("patient", patientsList.get(0).get(0).getProfile().getHrsid());
        queryParam.put("note", "reason for pausing ".concat(restUtilities.getRandomString()));
        queryParam.put("edvisit", "true");
        queryParam.put("readmitted", "true");
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PAUSE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));
        assertNotNull(path.get("noteid"));

        PatientNote note = RestAssistant.getGetNotesByPatientId(patientsList.get(0).get(0).getProfile().getHrsid())
                .stream()
                .filter(n -> n.getId().equals(Integer.valueOf(path.get("noteid"))))
                .collect(Collectors.toList()).get(0);

        assertEquals(note.getPatient(), patientsList.get(0).get(0).getProfile().getHrsid());
        assertNotNull(note.getClinician());
        assertNotNull(note.getNote());
        assertEquals(note.getStatus(), "active");
        assertNotNull(note.getReadmission());
        assertNull(note.getReadmission().getDate());
        assertNull(note.getReadmission().getLocation());
        assertNotNull(note.getEdvisit());
        assertNull(note.getEdvisit().getDate());
        assertNull(note.getEdvisit().getLocation());

        queryParam.clear();
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse",
            "testPausePatient"})
    public void testPausePatientWithAllFields() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yy");
        String currentDate = RandomData.getTimePeriod(1, formatter).get("current");

        Map<String, Object> bodyQuery = new HashMap<>();
        bodyQuery.put("patient", patientsList.get(0).get(0).getProfile().getHrsid());
        bodyQuery.put("note", "patient stuck in time ".concat(restUtilities.getRandomString()));

        Map<String, Object> edvisit = new HashMap<>();
        edvisit.put("dateObject", RandomData.getTimePeriod(2, formatter).get("current"));
        edvisit.put("location", "education location");
        edvisit.put("los", 1);
        edvisit.put("date", RestAssistant.tsToSec8601(currentDate.concat("T00:00:00.000-0000")));
        bodyQuery.put("edvisit", edvisit);

        Map<String, Object> readmitted = new HashMap<>();
        readmitted.put("dateObject", RandomData.getTimePeriod(2, formatter).get("current"));
        readmitted.put("location", "readmission location");
        readmitted.put("los", 1);
        readmitted.put("date", RestAssistant.tsToSec8601(currentDate.concat("T00:00:00.000-0000")));
        bodyQuery.put("readmitted", readmitted);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PAUSE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(bodyQuery);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));
        assertNotNull(path.get("noteid"));

        PatientNote note = RestAssistant.getGetNotesByPatientId(patientsList.get(0).get(0).getProfile().getHrsid())
                .stream()
                .filter(n -> n.getId().equals(Integer.valueOf(path.get("noteid"))))
                .collect(Collectors.toList()).get(0);

        assertEquals(note.getPatient(), patientsList.get(0).get(0).getProfile().getHrsid());

        assertNotNull(note.getClinician());
        assertNotNull(note.getNote());
        assertEquals(note.getStatus(), "active");
        assertNotNull(note.getReadmission());
        assertEquals(note.getReadmission().getLocation(), ((Map) bodyQuery.get("readmitted")).get("location"));
        assertEquals(note.getEdvisit().getLocation(), ((Map) bodyQuery.get("edvisit")).get("location"));

        bodyQuery.clear();
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse",
            "testPausePatient",
            "testPausePatientWithAllFields"})
    public void testPausePatientWithOnlyEdVisitField() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yy");
        String currentDate = RandomData.getTimePeriod(1, formatter).get("current");

        Map<String, Object> bodyQuery = new HashMap<>();

        bodyQuery.put("patient", patientsList.get(0).get(0).getProfile().getHrsid());
        bodyQuery.put("note", "patient stuck in time ".concat(restUtilities.getRandomString()));

        Map<String, Object> edvisit = new HashMap<>();
        edvisit.put("dateObject", RandomData.getTimePeriod(2, formatter).get("current"));
        edvisit.put("location", "education location");
        edvisit.put("los", 1);
        edvisit.put("date", RestAssistant.tsToSec8601(currentDate.concat("T00:00:00.000-0000")));
        bodyQuery.put("edvisit", edvisit);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PAUSE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(bodyQuery);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));
        assertNotNull(path.get("noteid"));

        PatientNote note = RestAssistant.getGetNotesByPatientId(patientsList.get(0).get(0).getProfile().getHrsid())
                .stream()
                .filter(n -> n.getId().equals(Integer.valueOf(path.get("noteid"))))
                .collect(Collectors.toList()).get(0);

        assertEquals(note.getPatient(), patientsList.get(0).get(0).getProfile().getHrsid());

        assertNotNull(note.getClinician());
        assertNotNull(note.getNote());
        assertEquals(note.getStatus(), "active");
        assertEquals(note.getEdvisit().getLocation(), ((Map) bodyQuery.get("edvisit")).get("location"));
        assertNull(note.getReadmission());

        bodyQuery.clear();
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse",
            "testPausePatient",
            "testPausePatientWithAllFields",
            "testPausePatientWithOnlyEdVisitField"})
    public void testPausePatientWithOnlyReadmittedField() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yy");
        String currentDate = RandomData.getTimePeriod(1, formatter).get("current");

        Map<String, Object> bodyQuery = new HashMap<>();
        bodyQuery.put("patient", patientsList.get(0).get(0).getProfile().getHrsid());
        bodyQuery.put("note", "patient stuck in time ".concat(restUtilities.getRandomString()));

        Map<String, Object> readmitted = new HashMap<>();
        readmitted.put("dateObject", RandomData.getTimePeriod(2, formatter).get("current"));
        readmitted.put("location", "readmission location");
        readmitted.put("los", 1);
        readmitted.put("date", RestAssistant.tsToSec8601(currentDate.concat("T00:00:00.000-0000")));
        bodyQuery.put("readmitted", readmitted);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PAUSE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(bodyQuery);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));
        assertNotNull(path.get("noteid"));

        PatientNote note = RestAssistant.getGetNotesByPatientId(patientsList.get(0).get(0).getProfile().getHrsid())
                .stream()
                .filter(n -> n.getId().equals(Integer.valueOf(path.get("noteid"))))
                .collect(Collectors.toList()).get(0);

        assertEquals(note.getPatient(), patientsList.get(0).get(0).getProfile().getHrsid());
        assertNotNull(note.getClinician());
        assertNotNull(note.getNote());
        assertEquals(note.getStatus(), "active");
        assertEquals(note.getReadmission().getLocation(), ((Map) bodyQuery.get("readmitted")).get("location"));
        assertNull(note.getEdvisit());

        bodyQuery.clear();
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse",
            "testPausePatient",
            "testPausePatientWithAllFields",
            "testPausePatientWithOnlyEdVisitField",
            "testPausePatientWithOnlyReadmittedField"})
    public void testPausePatientWithFieldsAbsent() {
        Map<String, Object> bodyQuery = new HashMap<>();
        bodyQuery.put("patient", patientsList.get(0).get(0).getProfile().getHrsid());
        bodyQuery.put("note", "patient stuck in time ".concat(restUtilities.getRandomString()));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PAUSE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(bodyQuery);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));
        assertNotNull(path.get("noteid"));

        PatientNote note = RestAssistant.getGetNotesByPatientId(patientsList.get(0).get(0).getProfile().getHrsid())
                .stream()
                .filter(n -> n.getId().equals(Integer.valueOf(path.get("noteid"))))
                .collect(Collectors.toList()).get(0);

        assertEquals(note.getPatient(), patientsList.get(0).get(0).getProfile().getHrsid());
        assertNotNull(note.getClinician());
        assertNotNull(note.getNote());
        assertEquals(note.getStatus(), "active");
        assertNull(note.getReadmission());
        assertNull(note.getEdvisit());

        bodyQuery.clear();
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse",
            "testPausePatient",
            "testPausePatientWithAllFields",
            "testPausePatientWithOnlyEdVisitField",
            "testPausePatientWithOnlyReadmittedField",
            "testPausePatientWithFieldsAbsent"})
    public void testDeactivatePatient() {
        queryParam.put("patient", patientsList.get(0).get(0).getProfile().getHrsid());
        queryParam.put("note", "note ".concat(restUtilities.getRandomString()));
        queryParam.put("edvisit", "true");
        queryParam.put("readmitted", "true");
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.DEACTIVATE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));
        assertNotNull(path.get("noteid"));

        queryParam.clear();
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse",
            "testPausePatient",
            "testPausePatientWithAllFields",
            "testPausePatientWithOnlyEdVisitField",
            "testPausePatientWithOnlyReadmittedField",
            "testPausePatientWithFieldsAbsent",
            "testDeactivatePatient"})
    public void testReactivatePatient() {
        queryParam.put("patient", patientsList.get(0).get(0).getProfile().getHrsid());
        queryParam.put("note", "note ".concat(restUtilities.getRandomString()));
        queryParam.put("edvisit", "true");
        queryParam.put("readmitted", "true");
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.REACTIVATE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "post");

        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "activated");
        assertNotNull(path.get("auth"));
        assertNotNull(path.get("noteid"));

        queryParam.clear();
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse",
            "testPausePatient",
            "testPausePatientWithAllFields",
            "testPausePatientWithOnlyEdVisitField",
            "testPausePatientWithOnlyReadmittedField",
            "testPausePatientWithFieldsAbsent",
            "testDeactivatePatient",
            "testReactivatePatient"})
    public void testDischargePatient() {
        queryParam.put("patient", patientsList.get(0).get(0).getProfile().getHrsid());
        queryParam.put("date", restUtilities.getTimePeriod(10).get("plusDays"));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.DISCHARGE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "post");

        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));

        queryParam.clear();
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse",
            "testPausePatient",
            "testPausePatientWithAllFields",
            "testPausePatientWithOnlyEdVisitField",
            "testPausePatientWithOnlyReadmittedField",
            "testPausePatientWithFieldsAbsent",
            "testDeactivatePatient",
            "testReactivatePatient",
            "testDischargePatient"}, priority = 5)
    public void testRemoveDischargePatient() {
        queryParam.put("patient", patientsList.get(0).get(0).getProfile().getHrsid());
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.DISCHARGE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "delete");

        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("auth"));

        queryParam.clear();
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse"})
    public void testSetMetricsToPatient() {
        NewMetricks newMetrick = new NewMetricks();
        ModuleInfo info = new ModuleInfo();

        List<String> modulesList = new ArrayList<>();
        modulesList.add(Modules.HELTHY);
        modulesList.add(Modules.CHF);
        modulesList.add(Modules.COPD);
        modulesList.add(Modules.DIABETES);
        modulesList.add(Modules.ACTIVITY);
        modulesList.add(Modules.WEIGHT);
        modulesList.add(Modules.TEMPERATURE);
        modulesList.add(Modules.BLOOD_PRESSURE);
        modulesList.add(Modules.MEDICATION);
        modulesList.add(Modules.SURVEY);
        modulesList.add(Modules.PULSE_OX);
        modulesList.add(Modules.GLUCOSE);
        modulesList.add(Modules.WOUND_IMAGING);
        modulesList.add(Modules.PATIENT_CONNECT_VOICE);
        modulesList.add(Modules.STETHOSCOPE);
        modulesList.add(Modules.STEPS);
        info.setModules(modulesList);
        info.setActivityreminder(new Activityreminder());
        info.setSurveyreminder(new Surveyreminder());
        info.setWeightreminder(new Weightreminder());

        List<Medicationreminder> medicationreminderList = new ArrayList<>();
        Medicationreminder medicationreminder = new Medicationreminder();
        medicationreminder.setMedication("test medication ".concat(restUtilities.getRandomString()));
        medicationreminder.setDose("1 mg x1 Oral");
        medicationreminder.setSchedule(new Schedule("true",
                "everyday",
                restUtilities.getTimePeriod(5).get("current"),
                "Test instruction: ".concat(restUtilities.getRandomString())));
        medicationreminderList.add(medicationreminder);
        info.setMedicationreminders(medicationreminderList);

        newMetrick.setModuleInfo(info);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.MODULE_INFO));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.body(newMetrick);

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "post");
        MetricksResponse metricksResponse = response.as(MetricksResponse.class);

        assertEquals(metricksResponse.getStatus(), "ok");
        assertNotNull(metricksResponse.getAuth());
        assertNotNull(metricksResponse.getAuth().getToken());

        assertNotNull(metricksResponse.getInstalldata());
        assertNotNull(metricksResponse.getInstalldata().getUpdatedisplay());
        assertNotNull(metricksResponse.getInstalldata().getModules());
        assertEquals(metricksResponse.getInstalldata().getModules(), newMetrick.getModuleInfo().getModules());

        assertNotNull(metricksResponse.getInstalldata().getActivityreminder());
        assertEquals(metricksResponse.getInstalldata().getActivityreminder().getTime()
                , newMetrick.getModuleInfo().getActivityreminder().getTime().toString());
        assertEquals(metricksResponse.getInstalldata().getActivityreminder().getWindow()
                , newMetrick.getModuleInfo().getActivityreminder().getWindow());

        assertNotNull(metricksResponse.getInstalldata().getSurveyreminder());
        assertEquals(metricksResponse.getInstalldata().getSurveyreminder().getTime()
                , newMetrick.getModuleInfo().getSurveyreminder().getTime().toString());
        assertEquals(metricksResponse.getInstalldata().getSurveyreminder().getWindow()
                , newMetrick.getModuleInfo().getSurveyreminder().getWindow());

        assertNotNull(metricksResponse.getInstalldata().getTemperaturereminders());
        assertNotNull(metricksResponse.getInstalldata().getTemperaturereminders().getStatus());
        assertEquals(metricksResponse.getInstalldata().getTemperaturereminders().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getWeightreminder());
        assertEquals(metricksResponse.getInstalldata().getWeightreminder().getTime()
                , newMetrick.getModuleInfo().getWeightreminder().getTime().toString());
        assertEquals(metricksResponse.getInstalldata().getWeightreminder().getWindow()
                , newMetrick.getModuleInfo().getWeightreminder().getWindow());

        assertNotNull(metricksResponse.getInstalldata().getWoundimagingreminders());
        assertNotNull(metricksResponse.getInstalldata().getWoundimagingreminders().getStatus());
        assertEquals(metricksResponse.getInstalldata().getWoundimagingreminders().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getMedicationreminders());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getMedication()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getMedication());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getDose()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getDose());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getType()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getType());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getExpiration()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getExpiration());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getEssential()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getEssential());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getInstruction()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getSchedule().getInstruction());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getTime()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getTime());
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getWindow()
                , newMetrick.getModuleInfo().getMedicationreminders().get(0).getWindow());

        assertNotNull(metricksResponse.getInstalldata().getGlucosereminder());
        assertNotNull(metricksResponse.getInstalldata().getGlucosereminder().getStatus());
        assertEquals(metricksResponse.getInstalldata().getGlucosereminder().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getPulseoxreminders());
        assertNotNull(metricksResponse.getInstalldata().getPulseoxreminders().getStatus());
        assertEquals(metricksResponse.getInstalldata().getPulseoxreminders().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getBloodpressurereminders());
        assertNotNull(metricksResponse.getInstalldata().getBloodpressurereminders().getStatus());
        assertEquals(metricksResponse.getInstalldata().getBloodpressurereminders().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getActivationhistory());
        assertNotNull(metricksResponse.getInstalldata().getActivationhistory().get(0).getStatus());
        assertEquals(metricksResponse.getInstalldata().getActivationhistory().get(0).getStatus(), "preactivate");

        queryParam.clear();
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse",
            "testPausePatient",
            "testPausePatientWithAllFields",
            "testPausePatientWithOnlyEdVisitField",
            "testPausePatientWithOnlyReadmittedField",
            "testPausePatientWithFieldsAbsent",
            "testDeactivatePatient",
            "testReactivatePatient",
            "testDischargePatient",
            "testSetMetricsToPatient"})
    public void testGetPatientTodayOverviewData() {
        Map<String, String> date = restUtilities.getTimePeriod(2);
        String start = date.get("current").replace("-", "");
        String end = date.get("plusDays").replace("-", "");

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        queryParam.put("start", start);
        queryParam.put("end", end);
        restUtilities.createQueryParam(reqSpec, queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "get");
        List<PatientList> profileHistory = Arrays.asList(response.as(PatientList[].class));

        profileHistory.forEach(history -> {
            assertNotNull(history.getProfile());
            try {
                assertNotNull(history.getProfile().getClinician());
                assertNotNull(history.getProfile().getClinician().getName());
                assertNotNull(history.getProfile().getClinician().getHrsid());
            } catch (AssertionError e) {
                System.out.println("Does not clinician!! Please check!!");
            }

            try {
                assertNotNull(history.getProfile().getDevicehistory());
                history.getProfile().getDevicehistory().forEach(devicehistory -> {
                    assertNotNull(devicehistory.getDevid());
                    assertNotNull(devicehistory.getName());
                    assertNotNull(devicehistory.getTs());
                });
            } catch (AssertionError e) {
                System.out.println("Does not device history!! Please check!!");
            }

            assertNotNull(history.getProfile().getExtension());
            assertNotNull(history.getProfile().getGender());
            assertNotNull(history.getProfile().getReadmission());
            assertNotNull(history.getProfile().getEdvisit());
            assertNotNull(history.getProfile().getTestpatient());
            assertNotNull(history.getProfile().getPid());
            assertNotNull(history.getProfile().getLanguage());
            assertEquals(history.getProfile().getHrsid(), patientsList.get(0).get(0).getProfile().getHrsid());
            assertNotNull(history.getProfile().getLastmoduleupdate());
            assertNotNull(history.getProfile().getVolume());
            try {
                assertNotNull(history.getProfile().getPhone());
            } catch (AssertionError e) {
                System.out.println("Profile does not have phone!! Please check!");
            }
            try {
                assertNotNull(history.getProfile().getDob());
            } catch (AssertionError e) {
                System.out.println("Profile does not have DOB!! Please check!");
            }
            assertNotNull(history.getProfile().getName());
            assertNotNull(history.getProfile().getName().getFirst());
            assertNotNull(history.getProfile().getName().getLast());
            assertNotNull(history.getProfile().getConditions());

            try {
                assertNotNull(history.getProfile().getDevice());
                assertNotNull(history.getProfile().getDevice().getName());
                assertNotNull(history.getProfile().getDevice().getDevid());
            } catch (AssertionError e) {
                System.out.println("Not devices!!");
            }

            assertNotNull(history.getProfile().getAudioreminders());
            assertNotNull(history.getProfile().getStatus());

            assertNotNull(history.getMetrics());
            assertNotNull(history.getTracking());
            assertNotNull(history.getTracking().getData());
            assertNotNull(history.getTracking().getData().getMedlist());
            assertNotNull(history.getTracking().getData().getQuiz());
            assertNotNull(history.getTracking().getData().getReminders());
            assertNotNull(history.getTracking().getData().getTracking());
            assertNotNull(history.getTracking().getData().getVideo());
        });
        queryParam.clear();
        restUtilities.removeQueryParam(reqSpec, "start");
        restUtilities.removeQueryParam(reqSpec, "end");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse",
            "testPausePatient",
            "testPausePatientWithAllFields",
            "testPausePatientWithOnlyEdVisitField",
            "testPausePatientWithOnlyReadmittedField",
            "testPausePatientWithFieldsAbsent",
            "testDeactivatePatient",
            "testReactivatePatient",
            "testDischargePatient",
            "testSetMetricsToPatient"}, priority = 5)
    public void testGetPatientHistoryNotes() {

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.NOTES))
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")))
        );

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT + EndPoints.ID);
        restUtilities.createQueryParam(newReqSpec, "limit", "10");
        Response response = restUtilities.getResponse(restUtilities.createPathParam(newReqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "get");
        List<PatientNote> notes = Arrays.asList(response.as(PatientNote[].class));

        notes.forEach(patientNote -> {
            assertNotNull(patientNote.getId());
            assertNotNull(patientNote.getPatient());
            assertNotNull(patientNote.getClinician().getName().getFirst());
            assertNotNull(patientNote.getClinician().getName().getLast());
            assertNotNull(patientNote.getNote());
            assertNotNull(patientNote.getStatus());
        });
    }

    @Test(dependsOnMethods = {"testPatientsResponse"})
    public void testGetPatientSurveyDetailsForToday() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.SYRVEY_DETAILS).concat(EndPoints.DATE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createPathParam(reqSpec, "date", restUtilities.getTimePeriod(2).get("current"));
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "get");
        com.hrs.cc.api.models.patients.survey.Survey survey = response.as(com.hrs.cc.api.models.patients.survey.Survey.class);
        assertNotNull(survey.getGroups());
        survey.getGroups().forEach(group -> {
            assertNotNull(group.getName());
            assertNotNull(group.getQuestionids());
            assertNotNull(group.getScoretype());
            assertNotNull(group.getScoredisplay());
            group.getScoredisplay().forEach(scoreDisplay -> {
                assertNotNull(scoreDisplay.getMax());
                assertNotNull(scoreDisplay.getMin());
            });
        });
        assertNotNull(survey.getQuestions());
        survey.getQuestions().forEach(question -> {
            assertNotNull(question.getSchedule());
            assertNotNull(question.getSchedule().getType());
            assertNotNull(question.getMedalert());
            assertNotNull(question.getHighalert());
            assertNotNull(question.getCategory());
            assertNotNull(question.getQuestion());
            assertNotNull(question.getId());
            assertNotNull(question.getAnswertype());
        });
        queryParam.clear();
        restUtilities.removePathParam(reqSpec, "date");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testPatientsResponse"})
    public void testPatientInformation() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientsList.get(0).get(0).getProfile().getHrsid()), "get");
        restUtilities.removePathParam(reqSpec, "id");

        PatientProfile profile = Arrays.asList(response.as(PatientProfile[].class)).get(0);
        try {
            assertNotNull(profile.getProfile().getClinician(), "Clinician field is null");
            assertNotNull(profile.getProfile().getClinician().getName(), "Clinician.Name field is null");
            assertNotNull(profile.getProfile().getClinician().getHrsid(), "Clinician.Hrsid field is null");
        } catch (AssertionError e) {
            System.out.println("Does not have clinician!!");
        }
        assertNotNull(profile.getProfile().getExtension(), "Profile.Extension field is null");
        assertNotNull(profile.getProfile().getGender(), "Profile.Gender field is null");
        assertNotNull(profile.getProfile().getReadmission(), "Profile.Readmission field is null");
        assertNotNull(profile.getProfile().getEdvisit(), "Profile.Edvisit field is null");
        assertNotNull(profile.getProfile().getTestpatient(), "Profile.Testpatient field is null");
        assertNotNull(profile.getProfile().getPid(), "Profile.Pid field is null");
        assertEquals(profile.getProfile().getLanguage(), "english");
        assertEquals(profile.getProfile().getHrsid(), patientsList.get(0).get(0).getProfile().getHrsid());
        assertNotNull(profile.getProfile().getStartdate(), "Profile.Startdate field is null");
        assertNotNull(profile.getProfile().getLastmoduleupdate(), "Profile.Lastmoduleupdate field is null");
        assertNotNull(profile.getProfile().getVolume(), "Profile.Volume field is null");
        try {
            assertNotNull(profile.getProfile().getPhone(), "Profile.Phone field is null");
        } catch (AssertionError e) {
            System.out.println("does not have phone!! Please check!");
        }

        assertNotNull(profile.getProfile().getName().getLast(), "Profile.Name().getLast field is null");
        assertNotNull(profile.getProfile().getName().getFirst(), "Profile.Name().getFirst field is null");
        assertNotNull(profile.getProfile().getEnvphone(), "Profile.Envphone field is null");
        assertNotNull(profile.getProfile().getConditions(), "Profile.Conditions field is null");
        assertNotNull(profile.getProfile().getAudioreminders(), "Profile.Audioreminders field is null");
        assertNotNull(profile.getProfile().getStatus(), "Profile.Status field is null");
        assertNotNull(profile.getRisk(), "Risk field is null");
        assertNotNull(profile.getMetrics(), "Metrics field is null");

        assertNotNull(profile.getTracking().getData().getReminders(), "Tracking.Data.Reminders field is null");
        assertNotNull(profile.getTracking().getData().getQuiz(), "Tracking.Data.Quiz field is null");
        assertNotNull(profile.getTracking().getData().getVideo(), "Tracking.Data.Video field is null");
        assertNotNull(profile.getTracking().getData().getTracking(), "Tracking.Data.Tracking field is null");
        assertNotNull(profile.getTracking().getData().getMedlist(), "Tracking.Data.Medlist field is null");
    }
}

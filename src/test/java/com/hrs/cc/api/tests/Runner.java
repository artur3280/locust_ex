package com.hrs.cc.api.tests;

import com.hrs.cc.api.connection.ClinicianConnection;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.int_patient.MakeNewIntegrationPatient;
import core.PropertyWorker;
import core.rest.loggers.Logger;
import org.testng.ITestContext;
import org.testng.ITestNGListener;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Runner {

    public static void main(String[] args) throws IOException {
        Path.setBaseUrl(args);

        ClinicianConnection.clinicianAut();
        MakeNewIntegrationPatient.start();

        TestNG testNG = new TestNG(true);
        Logger logger = new Logger();
        testNG.addListener((ITestNGListener) logger);
        testNG.setDefaultTestName("HRS restAssured test");


        boolean hasFailures = run(Configurator.getSuits(), testNG);
        Configurator.sendDataToTR(true, testNG, logger, false);
        Configurator.sendDataToSlack(true, testNG, logger);
        new PropertyWorker(Connection.AUTH_PROP).removeAllProperty();
        System.exit(hasFailures ? 1 : 0);
    }

    private static boolean run(List<XmlSuite> suite, TestNG testNG) {
        ITestNGListener results = new TestListenerAdapter() {
            @Override
            public void onStart(ITestContext testContext) {
                super.onStart(testContext); }
        };
        testNG.addListener(results);
        boolean hasFailures;
        try {

            testNG.setXmlSuites(suite);
            testNG.run();
            hasFailures = ((TestListenerAdapter)
                    results).getFailedTests().size() > 0 || ((TestListenerAdapter) results).getSkippedTests().size() > 0;
//            if (Runner.class.getProtectionDomain().getCodeSource().getLocation().getFile().endsWith(".jar")) {
//                new Configurations().removeAllProperty();
//            }
        } catch (Throwable e) {
            /* something goes wrong... */
            e.printStackTrace();
            hasFailures = true;
        }
        return hasFailures;
    }
}

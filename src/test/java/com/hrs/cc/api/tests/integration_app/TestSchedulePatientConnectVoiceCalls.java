package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.integratioins_app.ivr.questions.IvrQuestionsList;
import com.hrs.cc.api.models.integratioins_app.ivr.schedule.IvrSchedule;
import com.hrs.cc.api.models.patients.patient_list.PatientList;
import com.hrs.cc.api.models.requests.set_pc_voice_settings.*;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestSchedulePatientConnectVoiceCalls extends Assert {
    private ObjectMapper mapper = new ObjectMapper();
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();

    private PropertyWorker propertyWorker;

    private static String patientId;


    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() throws IOException {
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath("");
        patientId = RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG);
        propertyWorker.setProperty("patient_hrs_id", patientId);
        RestAssistant.setModulesToPatientById(propertyWorker.getProperty("patient_hrs_id"));
    }

    @AfterClass
    public void removeObj() {
        mapper = null;
        reqSpec = null;
        resPec = null;
        restUtilities = null;
        queryParam = null;
        propertyWorker = null;
        System.gc();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
    }

    @Test
    public void testInactiveAllEntries() {
        IvrQuestionsList questions = RestAssistant.getEnabledIrvQuestions();

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        PcVoiceSettings settings = new PcVoiceSettings();
        Data data = new Data();
        data.setId(patientId);
        data.setHrsid(patientId);
        data.setStatus("active");
        data.setCallAt(RandomData.getTime(1L).get("minus"));
        Retry retry = new Retry();
        retry.setMeasure("hour");
        retry.setUnits("1");
        data.setRetry(retry);
        data.setRetryCount(3);
        ArrayList<Entry> entryList = new ArrayList<>();

        Entry one = new Entry();
        one.setIvrQuestionId(questions.getData().get(0).getId());
        one.setStatus("inactive");
        Schedule schedule = new Schedule();
        schedule.setEnd(RandomData.getTimePeriod(8, dateFormat).get("plusDays"));
        schedule.setStart(RandomData.getTimePeriod(1, dateFormat).get("current"));
        Every every = new Every();
        every.setMeasure("daily");
        every.setWeekends(false);
        List<String> units = new ArrayList<>();
        units.add("1");
        every.setUnits(units);
        schedule.setEvery(every);
        one.setSchedule(schedule);
        entryList.add(one);

        Entry two = new Entry();
        two.setIvrQuestionId(questions.getData().get(1).getId());
        two.setStatus("inactive");
        Schedule scheduleTwo = new Schedule();
        scheduleTwo.setEnd(RandomData.getTimePeriod(8, dateFormat).get("plusDays"));
        scheduleTwo.setStart(RandomData.getTimePeriod(1, dateFormat).get("current"));
        Every everyTwo = new Every();
        everyTwo.setMeasure("daily");
        everyTwo.setWeekends(false);
        List<String> unitsTwo = new ArrayList<>();
        unitsTwo.add("1");
        everyTwo.setUnits(unitsTwo);
        scheduleTwo.setEvery(everyTwo);
        two.setSchedule(scheduleTwo);
        entryList.add(two);

        data.setEntries(entryList);
        settings.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(settings);


        Response response = restUtilities.getResponse(reqSpec, "patch");
        PcVoiceSettings newSettings = response.as(PcVoiceSettings.class);
        assertEquals(newSettings.getData().getId(), settings.getData().getId());

        assertTrue(newSettings.getData().getEntries().stream().allMatch(entry -> entry.getStatus().equals("inactive")), "Some entries does not have 'inactive' status");

    }

    @Test(dependsOnMethods = {"testInactiveAllEntries"})
    public void testInactiveOneEntry() {
        IvrQuestionsList questions = RestAssistant.getEnabledIrvQuestions();

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        PcVoiceSettings settings = new PcVoiceSettings();
        Data data = new Data();
        data.setId(patientId);
        data.setHrsid(patientId);
        data.setStatus("active");
        data.setCallAt(RandomData.getTime(1L).get("minus"));
        Retry retry = new Retry();
        retry.setMeasure("hour");
        retry.setUnits("1");
        data.setRetry(retry);
        data.setRetryCount(3);
        ArrayList<Entry> entryList = new ArrayList<>();

        Entry one = new Entry();
        one.setIvrQuestionId(questions.getData().get(0).getId());
        one.setStatus("active");
        Schedule schedule = new Schedule();
        schedule.setEnd(RandomData.getTimePeriod(8, dateFormat).get("plusDays"));
        schedule.setStart(RandomData.getTimePeriod(1, dateFormat).get("current"));
        Every every = new Every();
        every.setMeasure("daily");
        every.setWeekends(false);
        List<String> units = new ArrayList<>();
        units.add("1");
        every.setUnits(units);
        schedule.setEvery(every);
        one.setSchedule(schedule);
        entryList.add(one);

        Entry two = new Entry();
        two.setIvrQuestionId(questions.getData().get(1).getId());
        two.setStatus("inactive");
        Schedule scheduleTwo = new Schedule();
        scheduleTwo.setEnd(RandomData.getTimePeriod(8, dateFormat).get("plusDays"));
        scheduleTwo.setStart(RandomData.getTimePeriod(1, dateFormat).get("current"));
        Every everyTwo = new Every();
        everyTwo.setMeasure("daily");
        everyTwo.setWeekends(false);
        List<String> unitsTwo = new ArrayList<>();
        unitsTwo.add("1");
        everyTwo.setUnits(unitsTwo);
        scheduleTwo.setEvery(everyTwo);
        two.setSchedule(scheduleTwo);
        entryList.add(two);

        data.setEntries(entryList);
        settings.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(settings);

        Response response = restUtilities.getResponse(reqSpec, "patch");
        PcVoiceSettings newSettings = response.as(PcVoiceSettings.class);
        assertEquals(newSettings.getData().getId(), settings.getData().getId());

        Entry oneEntry = newSettings.getData().getEntries().stream().filter(e->e.getIvrQuestionId().equals(questions.getData().get(0).getId())).findFirst().get();
        Entry twoEntry = newSettings.getData().getEntries().stream().filter(e->e.getIvrQuestionId().equals(questions.getData().get(1).getId())).findFirst().get();
        if(oneEntry == null)throw new NullPointerException("One entry is null");
        if(twoEntry == null)throw new NullPointerException("Two entry is null");

        assertEquals(oneEntry.getStatus(),"active", "field not eql");
        assertEquals(twoEntry.getStatus(),"inactive", "field not eql");
    }

    @Test(dependsOnMethods = {"testInactiveAllEntries", "testInactiveOneEntry"})
    public void testInactiveVoice() {
        IvrQuestionsList questions = RestAssistant.getEnabledIrvQuestions();

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        PcVoiceSettings settings = new PcVoiceSettings();
        Data data = new Data();
        data.setId(patientId);
        data.setHrsid(patientId);
        data.setStatus("inactive");
        data.setCallAt(RandomData.getTime(1L).get("minus"));
        Retry retry = new Retry();
        retry.setMeasure("hour");
        retry.setUnits("1");
        data.setRetry(retry);
        data.setRetryCount(3);
        ArrayList<Entry> entryList = new ArrayList<>();

        Entry one = new Entry();
        one.setIvrQuestionId(questions.getData().get(0).getId());
        one.setStatus("active");
        Schedule schedule = new Schedule();
        schedule.setEnd(RandomData.getTimePeriod(8, dateFormat).get("plusDays"));
        schedule.setStart(RandomData.getTimePeriod(1, dateFormat).get("current"));
        Every every = new Every();
        every.setMeasure("daily");
        every.setWeekends(false);
        List<String> units = new ArrayList<>();
        units.add("1");
        every.setUnits(units);
        schedule.setEvery(every);
        one.setSchedule(schedule);
        entryList.add(one);

        Entry two = new Entry();
        two.setIvrQuestionId(questions.getData().get(1).getId());
        two.setStatus("active");
        Schedule scheduleTwo = new Schedule();
        scheduleTwo.setEnd(RandomData.getTimePeriod(8, dateFormat).get("plusDays"));
        scheduleTwo.setStart(RandomData.getTimePeriod(1, dateFormat).get("current"));
        Every everyTwo = new Every();
        everyTwo.setMeasure("daily");
        everyTwo.setWeekends(false);
        List<String> unitsTwo = new ArrayList<>();
        unitsTwo.add("1");
        everyTwo.setUnits(unitsTwo);
        scheduleTwo.setEvery(everyTwo);
        two.setSchedule(scheduleTwo);
        entryList.add(two);

        data.setEntries(entryList);
        settings.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(settings);

        Response response = restUtilities.getResponse(reqSpec, "patch");

        PcVoiceSettings newSettings = response.as(PcVoiceSettings.class);
        assertEquals(newSettings.getData().getStatus(),"inactive", "field not eql");
        assertNull(newSettings.getData().getEntries(), "entries field is null");
    }

    @Test(dependsOnMethods = {"testInactiveAllEntries", "testInactiveOneEntry", "testInactiveVoice"})
    public void testActiveVoiceAndAllEntries() {
        IvrQuestionsList questions = RestAssistant.getEnabledIrvQuestions();

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        PcVoiceSettings settings = new PcVoiceSettings();
        Data data = new Data();
        data.setId(patientId);
        data.setHrsid(patientId);
        data.setStatus("active");
        data.setCallAt(RandomData.getTime(1L).get("minus"));
        Retry retry = new Retry();
        retry.setMeasure("hour");
        retry.setUnits("1");
        data.setRetry(retry);
        data.setRetryCount(3);
        ArrayList<Entry> entryList = new ArrayList<>();

        Entry one = new Entry();
        one.setIvrQuestionId(questions.getData().get(0).getId());
        one.setStatus("active");
        Schedule schedule = new Schedule();
        schedule.setEnd(RandomData.getTimePeriod(8, dateFormat).get("plusDays"));
        schedule.setStart(RandomData.getTimePeriod(1, dateFormat).get("current"));
        Every every = new Every();
        every.setMeasure("daily");
        every.setWeekends(false);
        List<String> units = new ArrayList<>();
        units.add("1");
        every.setUnits(units);
        schedule.setEvery(every);
        one.setSchedule(schedule);
        entryList.add(one);

        Entry two = new Entry();
        two.setIvrQuestionId(questions.getData().get(1).getId());
        two.setStatus("active");
        Schedule scheduleTwo = new Schedule();
        scheduleTwo.setEnd(RandomData.getTimePeriod(8, dateFormat).get("plusDays"));
        scheduleTwo.setStart(RandomData.getTimePeriod(1, dateFormat).get("current"));
        Every everyTwo = new Every();
        everyTwo.setMeasure("daily");
        everyTwo.setWeekends(false);
        List<String> unitsTwo = new ArrayList<>();
        unitsTwo.add("1");
        everyTwo.setUnits(unitsTwo);
        scheduleTwo.setEvery(everyTwo);
        two.setSchedule(scheduleTwo);
        entryList.add(two);

        data.setEntries(entryList);
        settings.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(settings);

        Response response = restUtilities.getResponse(reqSpec, "patch");
        PcVoiceSettings newSettings = response.as(PcVoiceSettings.class);
        assertEquals(newSettings.getData().getId(), settings.getData().getId(), "field not eql");

        assertEquals(newSettings.getData().getStatus(),"active", "field not eql");
        assertTrue(newSettings.getData().getEntries().stream().allMatch(entry -> entry.getStatus().equals("active")), "Some entries is not active");
    }

    @Test(dependsOnMethods = {
            "testInactiveAllEntries",
            "testInactiveOneEntry",
            "testInactiveVoice",
            "testActiveVoiceAndAllEntries"})
    public void testEditScheduleFrequencyByNthDayAndActivePeriodPCVoiceEntry() {
        IvrQuestionsList questions = RestAssistant.getEnabledIrvQuestions();

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        PcVoiceSettings settings = new PcVoiceSettings();
        Data data = new Data();
        data.setId(patientId);
        data.setHrsid(patientId);
        data.setStatus("active");
        data.setCallAt(RandomData.getTime(1L).get("minus"));
        Retry retry = new Retry();
        retry.setMeasure("hour");
        retry.setUnits("1");
        data.setRetry(retry);
        data.setRetryCount(3);
        ArrayList<Entry> entryList = new ArrayList<>();

        Entry one = new Entry();
        one.setIvrQuestionId(questions.getData().get(0).getId());
        one.setStatus("active");
        Schedule schedule = new Schedule();
        schedule.setEnd(RandomData.getTimePeriod(10, dateFormat).get("plusDays"));
        schedule.setStart(RandomData.getTimePeriod(1, dateFormat).get("current"));
        Every every = new Every();
        every.setMeasure("nthDay");
        every.setWeekends(true);
        List<String> units = new ArrayList<>();
        units.add(RandomData.getRandomInt(2,5).toString());
        every.setUnits(units);
        schedule.setEvery(every);
        one.setSchedule(schedule);
        entryList.add(one);

        Entry two = new Entry();
        two.setIvrQuestionId(questions.getData().get(1).getId());
        two.setStatus("active");
        Schedule scheduleTwo = new Schedule();
        scheduleTwo.setEnd(RandomData.getTimePeriod(7, dateFormat).get("plusDays"));
        scheduleTwo.setStart(RandomData.getTimePeriod(1, dateFormat).get("current"));
        Every everyTwo = new Every();
        everyTwo.setMeasure("nthDay");
        everyTwo.setWeekends(true);
        List<String> unitsTwo = new ArrayList<>();
        unitsTwo.add(RandomData.getRandomInt(2,5).toString());
        everyTwo.setUnits(unitsTwo);
        scheduleTwo.setEvery(everyTwo);
        two.setSchedule(scheduleTwo);
        entryList.add(two);

        data.setEntries(entryList);
        settings.setData(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(settings);

        Response response = restUtilities.getResponse(reqSpec, "patch");
        PcVoiceSettings newSettings = response.as(PcVoiceSettings.class);

        Entry oneEntry = newSettings.getData().getEntries().stream().filter(e->e.getIvrQuestionId().equals(questions.getData().get(0).getId())).findFirst().get();
        Entry twoEntry = newSettings.getData().getEntries().stream().filter(e->e.getIvrQuestionId().equals(questions.getData().get(1).getId())).findFirst().get();
        if(oneEntry == null)throw new NullPointerException("One entry is null");
        if(twoEntry == null)throw new NullPointerException("Two entry is null");

        assertTrue(oneEntry.getSchedule().getEnd().toString().contains(settings.getData().getEntries().get(0).getSchedule().getEnd().toString()), "field not eql");
        assertTrue(oneEntry.getSchedule().getEvery().getWeekends(), "every not Weekends");
        assertEquals(oneEntry.getSchedule().getEvery().getMeasure(),
                settings.getData().getEntries().get(0).getSchedule().getEvery().getMeasure(), "field not eql");
        assertEquals(oneEntry.getSchedule().getEvery().getUnits(),
                settings.getData().getEntries().get(0).getSchedule().getEvery().getUnits(), "field not eql");

        assertTrue(twoEntry.getSchedule().getEnd().toString().contains(settings.getData().getEntries().get(1).getSchedule().getEnd().toString()));
        assertTrue(twoEntry.getSchedule().getEvery().getWeekends(), "every not Weekends");
        assertEquals(twoEntry.getSchedule().getEvery().getMeasure(),
                settings.getData().getEntries().get(1).getSchedule().getEvery().getMeasure(), "field not eql");
        assertEquals(twoEntry.getSchedule().getEvery().getUnits(),
                settings.getData().getEntries().get(1).getSchedule().getEvery().getUnits(), "field not eql");

    }

    @Test(dependsOnMethods = {
            "testInactiveAllEntries",
            "testInactiveOneEntry",
            "testInactiveVoice",
            "testActiveVoiceAndAllEntries"}, priority = 2)
    public void testConsentFormFile() {
        ResponseSpecification responseSpecification = restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType("application/pdf")
                        .expectStatusCode(200)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(Path.API_V2
                .concat(EndPoints.PATIENT)
                .concat(EndPoints.ID)
                .concat(EndPoints.CONSENT)
                .concat(EndPoints.DOWNLOAD));

        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        Response response = restUtilities.getResponse(reqSpec, "get");
    }
}
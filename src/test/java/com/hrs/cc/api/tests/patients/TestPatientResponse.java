package com.hrs.cc.api.tests.patients;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.*;
import com.hrs.cc.api.models.integratioins_app.modules_cc_mobile.ModulesCCMobile;
import com.hrs.cc.api.models.integratioins_app.user_by_type_in_env.Datum;
import com.hrs.cc.api.models.patients.module_info.PatientModulesInfo;
import com.hrs.cc.api.models.patients.patient_metric.metric_weight.WeightMetric;
import com.hrs.cc.api.models.patients.patient_metric.response_after_add.MetricksResponse;
import com.hrs.cc.api.models.patients.patient_status.PatientStatus;
import com.hrs.cc.api.models.patients.profile.Profile;
import com.hrs.cc.api.models.requests.add_new_patient.CustomAtributs;
import com.hrs.cc.api.models.requests.add_new_patient.Name;
import com.hrs.cc.api.models.requests.add_new_patient.NewPatient;
import com.hrs.cc.api.models.requests.set_patient_metricks.*;
import com.hrs.cc.api.models.requests.set_patient_metricks.activity.SetActivity;
import com.hrs.cc.api.models.requests.set_patient_metricks.blood_pressure.SetBloodPressure;
import com.hrs.cc.api.models.requests.set_patient_metricks.glucose.SetGlucose;
import com.hrs.cc.api.models.requests.set_patient_metricks.pulse_ox.SetPulseOx;
import com.hrs.cc.api.models.requests.set_patient_metricks.temperature.SetTemperature;
import com.hrs.cc.api.models.requests.set_patient_metricks.weight.SetWeight;
import com.hrs.cc.api.models.requests.set_quick_note.QuickNote;
import com.hrs.cc.api.models.requests.settings_patient.CustomNewAtributs;
import com.hrs.cc.api.models.requests.settings_patient.SettingPatient;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class TestPatientResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private static NewPatient patientDataResponse;
    private String newPatientId;
    private static Map<String, Integer> createdMetrics = new HashMap<>();
    private PropertyWorker propertyWorker;
    private NewMetricks newMetric;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENT));
        resPec = restUtilities.getResponseSpecification();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        restUtilities.removeQueryParam(reqSpec, "status[]");
        reqSpec.body("");
    }

    @Test
    public void testAddNewPatient() {
        patientDataResponse = new NewPatient();

        List<CustomAtributs> customAtributs = new ArrayList<>();

        CustomAtributs first = new CustomAtributs();
        first.setId(1);
        first.setName("Test custom attributes");
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated("2017-08-31T11:39:00+0000");
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomAtributs two = new CustomAtributs();
        two.setId(2);
        two.setName("Nickname");
        two.setType("text");
        two.setRequired(false);
        two.setPatientsetup(true);
        two.set_class("col-xs-6");
        two.setEmrTracked(false);
        two.setLastUpdated("2017-09-25T14:22:07+0000");
        two.set$$hashKey("object:2416");
        two.setValue("testnick");

        CustomAtributs three = new CustomAtributs();
        three.setId(4);
        three.setName("Custom Text Field");
        three.setType("text");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-12");
        three.setEmrTracked(false);
        three.setLastUpdated("2017-10-16T14:44:33+0000");
        three.set$$hashKey("object:2417");
        three.setValue("test custom field");

        customAtributs.add(first);
        customAtributs.add(two);
        customAtributs.add(three);
        patientDataResponse.setCustomAtributsList(customAtributs);

        Name name = new Name("test".concat(restUtilities.getRandomString())
                , "test".concat(restUtilities.getRandomString()),
                "test".concat(restUtilities.getRandomString()));
        patientDataResponse.setName(name);

        patientDataResponse.setDob(restUtilities.getTimePeriod(1).get("current"));
        patientDataResponse.setGender("F");
        patientDataResponse.setPhone("3423890430284902");
        patientDataResponse.setPid(restUtilities.getRandomString());
        patientDataResponse.setSubgroup("test subgroup");
        reqSpec.body(patientDataResponse);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();

        Response response = restUtilities.getResponse(reqSpec, "post");

        JsonPath jsonPath = response.jsonPath();
        newPatientId = jsonPath.get("hrsid");
        assertNotNull(jsonPath.get("hrsid"), "hrsid is null");
        assertEquals(jsonPath.get("status"), "ok");

        Profile createdPatient = RestAssistant.getPatientById(newPatientId);

        assertEquals(createdPatient.getName().getFirst(), name.getFirst(), "getFirst field not eql");
        assertEquals(createdPatient.getName().getLast(), name.getLast(), "getLast field not eql");
        assertEquals(createdPatient.getName().getMiddle(), name.getMiddle(), "getMiddle field not eql");
        assertNull(createdPatient.getSubgroup(), "Subgroub field not null");
        assertEquals(createdPatient.getDob(), patientDataResponse.getDob(), "Dob field is not eql");
        assertEquals(createdPatient.getGender(), patientDataResponse.getGender(), "Gender field not eql");
        assertEquals(createdPatient.getPid(), patientDataResponse.getPid(), "Pid field is not eql");

    }

    @Test(dependsOnMethods = {"testAddNewPatient"}, priority = 4)
    public void testSetStatusTestPatientIntoAdminPanel() {
        RestAssistant.setTestSettingToPatientInEnvAdmin(newPatientId);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "get");
        Profile profile = response.as(Profile.class);
        assertTrue(profile.getTestpatient(), "is not test patient");
    }

    @Test(dependsOnMethods = {"testAddNewPatient"})
    public void testSetTimeZoneToPatient() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, String> body = new HashMap<>();
        body.put("time_zone", "American/Denver");
        body.put("subgroup", "test subgroup");
        reqSpec.body(body);

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");

        Profile createdPatient = RestAssistant.getPatientById(newPatientId);

        assertEquals(createdPatient.getName().getFirst(), patientDataResponse.getName().getFirst(), "getFirst field not eql");
        assertEquals(createdPatient.getName().getLast(), patientDataResponse.getName().getLast(), "getLast field not eql");
        assertEquals(createdPatient.getName().getMiddle(), patientDataResponse.getName().getMiddle(), "getMiddle field not eql");
        assertEquals(createdPatient.getSubgroup(), body.get("subgroup"), "Subgroub field not eql");
        assertEquals(createdPatient.getTimezone(), body.get("time_zone"), "Timezone not eql");
        assertEquals(createdPatient.getDob(), patientDataResponse.getDob(), "Dob field is not eql");
        assertEquals(createdPatient.getGender(), patientDataResponse.getGender(), "Gender field not eql");
        assertEquals(createdPatient.getPid(), patientDataResponse.getPid(), "Pid field is not eql");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetClinicianToPatientWhenClinicianIdIncorrect"})
    public void testSetClinicianToPatient() {
        Datum clinicianData = RestAssistant.getEnabledClinicians().getData().stream().filter(c -> c.getFirstName().contains("Edited")).findAny().get();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, Object> body = new HashMap<>();
        Map<String, String> clinician = new HashMap<>();
        clinician.put("hrsid", clinicianData.getId());
        body.put("clinician", clinician);
        reqSpec.body(body);

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("changedClinician").toString(), "true");
        assertEquals(path.get("status"), "ok");

        Profile patient = RestAssistant.getPatientById(newPatientId);
        if (patient == null) throw new NullPointerException("Patient is null");
        assertEquals(patient.getClinician().getHrsid(), clinicianData.getId(), "Clinician id not eql");
    }

    @Test(dependsOnMethods = {"testAddNewPatient"})
    public void testSetClinicianToPatientWhenClinicianIdIncorrect() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, Object> body = new HashMap<>();
        Map<String, String> clinician = new HashMap<>();
        clinician.put("hrsid", RandomData.getRandomString(10));
        body.put("clinician", clinician);
        reqSpec.body(body);

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("changedClinician").toString(), "true");
        assertEquals(path.get("status"), "ok");

        Profile patient = RestAssistant.getPatientById(newPatientId);
        if (patient == null) throw new NullPointerException("Patient is null");

        assertTrue(patient.getClinician().getName().contains(Auth.FIRST_NAME), "Clinician has been change");
        assertTrue(patient.getClinician().getName().contains(Auth.LAST_NAME), "Clinician has been change");
    }

    @Test(priority = 6)
    public void testAddAlreadyExistsPatient() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENT));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(500)
        );

        patientDataResponse = new NewPatient();

        List<CustomAtributs> customAtributs = new ArrayList<>();

        CustomAtributs first = new CustomAtributs();
        first.setId(1);
        first.setName("Test custom attributes");
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated("2017-08-31T11:39:00+0000");
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomAtributs two = new CustomAtributs();
        two.setId(2);
        two.setName("Nickname");
        two.setType("text");
        two.setRequired(false);
        two.setPatientsetup(true);
        two.set_class("col-xs-6");
        two.setEmrTracked(false);
        two.setLastUpdated("2017-09-25T14:22:07+0000");
        two.set$$hashKey("object:2416");
        two.setValue("1");

        CustomAtributs three = new CustomAtributs();
        three.setId(4);
        three.setName("Custom Text Field");
        three.setType("text");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-12");
        three.setEmrTracked(false);
        three.setLastUpdated("2017-10-16T14:44:33+0000");
        three.set$$hashKey("object:2417");
        three.setValue("1");

        customAtributs.add(first);
        customAtributs.add(two);
        customAtributs.add(three);
        patientDataResponse.setCustomAtributsList(customAtributs);

        Name name = new Name();
        name.setLast("Integration");
        name.setFirst("UserApp");
        name.setMiddle("");
        patientDataResponse.setName(name);

        patientDataResponse.setDob("01/01/1990");
        patientDataResponse.setGender("F");
        patientDataResponse.setPhone("091019990000111");
        patientDataResponse.setPid("testAppIntegration");
        reqSpec.body(patientDataResponse);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("message"), "Patient ID already exists");
    }


    @Test(dependsOnMethods = "testAddNewPatient", priority = 5)
    public void testCheckingPcVoiceLicenseError() {
        RestAssistant.setPcVoiceLicenseMax(3);

        newMetric = new NewMetricks();
        ModuleInfo info = new ModuleInfo();

        List<String> modulesList = new ArrayList<>();
        modulesList.add(Modules.MEDICATION);
        modulesList.add(Modules.PATIENT_CONNECT_VOICE);

        info.setModules(modulesList);
        info.setActivityreminder(new Activityreminder());
        info.setSurveyreminder(new Surveyreminder());
        info.setWeightreminder(new Weightreminder());

        List<Medicationreminder> medicationreminderList = new ArrayList<>();
        Medicationreminder medicationreminder = new Medicationreminder();
        medicationreminder.setMedication("test medication ".concat(restUtilities.getRandomString()));
        medicationreminder.setDose("1 mg x1 Oral");
        medicationreminder.setSchedule(new Schedule("true",
                "everyday",
                restUtilities.getTimePeriod(25).get("plusDays"),
                "Test instruction: ".concat(restUtilities.getRandomString())));
        medicationreminderList.add(medicationreminder);

        Medicationreminder everyXdaysReminder = new Medicationreminder();
        everyXdaysReminder.setMedication("LIQUAEMIN SODIUM");
        everyXdaysReminder.setDose("12 mg x1 Oral");
        Schedule scheduleEveryXdaysReminder = new Schedule();
        scheduleEveryXdaysReminder.setEssential("true");
        scheduleEveryXdaysReminder.setType("everyxdays");
        scheduleEveryXdaysReminder.setX(restUtilities.getRandomInt(1, 7).toString());
        scheduleEveryXdaysReminder.setStartday(String.valueOf(restUtilities.getCurrentTimeStamp()));
        scheduleEveryXdaysReminder.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleEveryXdaysReminder.setExpiration(restUtilities.getTimePeriod(25).get("plusDays"));
        everyXdaysReminder.setSchedule(scheduleEveryXdaysReminder);
        everyXdaysReminder.setTime("540");
        everyXdaysReminder.setTimes("9:00AM");
        everyXdaysReminder.setWindow("60");
        medicationreminderList.add(everyXdaysReminder);

        Medicationreminder customDaysRemider = new Medicationreminder();
        customDaysRemider.setMedication("ALCOHOL 10% AND DEXTROSE 5%");
        customDaysRemider.setDose("200 mg x2 Oral");
        Schedule scheduleCustomDaysRemider = new Schedule();
        scheduleCustomDaysRemider.setEssential("true");
        scheduleCustomDaysRemider.setType("custom");
        scheduleCustomDaysRemider.setSchedule(Modules.customMedicationsDays);
        scheduleCustomDaysRemider.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleCustomDaysRemider.setExpiration(restUtilities.getTimePeriod(25).get("plusDays"));
        customDaysRemider.setSchedule(scheduleCustomDaysRemider);
        customDaysRemider.setTime("540");
        customDaysRemider.setTimes("9:00AM");
        customDaysRemider.setWindow("60");
        medicationreminderList.add(customDaysRemider);

        info.setMedicationreminders(medicationreminderList);
        newMetric.setModuleInfo(info);

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENTS))
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token"))));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.MODULE_INFO));
        newReqSpec.body(newMetric);

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");

        JsonPath path = response.jsonPath();

        try {
            assertNotNull(path.get("pcVoiceLicenseError"), "pcVoiceLicenseError is null");
            assertTrue(path.get("pcVoiceLicenseError"), "pcVoiceLicenseError is not true");
        }finally {
            RestAssistant.removeEnvSettings(EnvFlags.SYSTEM_PATIENTCONNECTVOICE_LICENSE_MAX.get());
        }


    }

    @Test(dependsOnMethods = {"testAddNewPatient"})
    public void testSetMetricsToPatient() {
        newMetric = new NewMetricks();
        ModuleInfo info = new ModuleInfo();

        List<String> modulesList = new ArrayList<>();
        modulesList.add(Modules.HELTHY);
        modulesList.add(Modules.CHF);
        modulesList.add(Modules.COPD);
        modulesList.add(Modules.DIABETES);
        modulesList.add(Modules.ACTIVITY);
        modulesList.add(Modules.WEIGHT);
        modulesList.add(Modules.TEMPERATURE);
        modulesList.add(Modules.BLOOD_PRESSURE);
        modulesList.add(Modules.MEDICATION);
        modulesList.add(Modules.SURVEY);
        modulesList.add(Modules.PULSE_OX);
        modulesList.add(Modules.GLUCOSE);
        modulesList.add(Modules.WOUND_IMAGING);
        modulesList.add(Modules.WOUND_IMAGING);
        modulesList.add(Modules.STETHOSCOPE);
        modulesList.add(Modules.PATIENT_CONNECT_VOICE);
        modulesList.add(Modules.STEPS);
        info.setModules(modulesList);
        info.setActivityreminder(new Activityreminder());
        info.setSurveyreminder(new Surveyreminder());
        info.setWeightreminder(new Weightreminder());

        List<Medicationreminder> medicationreminderList = new ArrayList<>();
        Medicationreminder medicationreminder = new Medicationreminder();
        medicationreminder.setMedication("test medication ".concat(restUtilities.getRandomString()));
        medicationreminder.setDose("1 mg x1 Oral");
        medicationreminder.setSchedule(new Schedule("true",
                "everyday",
                restUtilities.getTimePeriod(25).get("plusDays"),
                "Test instruction: ".concat(restUtilities.getRandomString())));
        medicationreminderList.add(medicationreminder);

        Medicationreminder everyXdaysReminder = new Medicationreminder();
        everyXdaysReminder.setMedication("LIQUAEMIN SODIUM");
        everyXdaysReminder.setDose("12 mg x1 Oral");
        Schedule scheduleEveryXdaysReminder = new Schedule();
        scheduleEveryXdaysReminder.setEssential("true");
        scheduleEveryXdaysReminder.setType("everyxdays");
        scheduleEveryXdaysReminder.setX(restUtilities.getRandomInt(1, 7).toString());
        scheduleEveryXdaysReminder.setStartday(String.valueOf(restUtilities.getCurrentTimeStamp()));
        scheduleEveryXdaysReminder.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleEveryXdaysReminder.setExpiration(restUtilities.getTimePeriod(25).get("plusDays"));
        everyXdaysReminder.setSchedule(scheduleEveryXdaysReminder);
        everyXdaysReminder.setTime("540");
        everyXdaysReminder.setTimes("9:00AM");
        everyXdaysReminder.setWindow("60");
        medicationreminderList.add(everyXdaysReminder);

        Medicationreminder customDaysRemider = new Medicationreminder();
        customDaysRemider.setMedication("ALCOHOL 10% AND DEXTROSE 5%");
        customDaysRemider.setDose("200 mg x2 Oral");
        Schedule scheduleCustomDaysRemider = new Schedule();
        scheduleCustomDaysRemider.setEssential("true");
        scheduleCustomDaysRemider.setType("custom");
        scheduleCustomDaysRemider.setSchedule(Modules.customMedicationsDays);
        scheduleCustomDaysRemider.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleCustomDaysRemider.setExpiration(restUtilities.getTimePeriod(25).get("plusDays"));
        customDaysRemider.setSchedule(scheduleCustomDaysRemider);
        customDaysRemider.setTime("540");
        customDaysRemider.setTimes("9:00AM");
        customDaysRemider.setWindow("60");
        medicationreminderList.add(customDaysRemider);

        info.setMedicationreminders(medicationreminderList);
        newMetric.setModuleInfo(info);

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENTS))
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token"))));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.MODULE_INFO));
        newReqSpec.body(newMetric);

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");

        MetricksResponse metricksResponse = response.as(MetricksResponse.class);

        assertEquals(metricksResponse.getStatus(), "ok", "status not ok");
        assertNotNull(metricksResponse.getAuth(), "Auth field is null");
        assertNotNull(metricksResponse.getAuth().getToken(), "Auth().getToken field is null");

        assertNotNull(metricksResponse.getInstalldata(), "Installdata field is null");
        assertNotNull(metricksResponse.getInstalldata().getUpdatedisplay(), "Installdata.Updatedisplay field is null");
        assertNotNull(metricksResponse.getInstalldata().getModules(), "Installdata.Modules field is null");
        assertEquals(metricksResponse.getInstalldata().getModules(), newMetric.getModuleInfo().getModules(), "modules not eqls");

        assertNotNull(metricksResponse.getInstalldata().getActivityreminder(), "Installdata.Activityreminder field is null");
        assertEquals(metricksResponse.getInstalldata().getActivityreminder().getTime()
                , newMetric.getModuleInfo().getActivityreminder().getTime().toString(), "activity reminder time not eql");
        assertEquals(metricksResponse.getInstalldata().getActivityreminder().getWindow()
                , newMetric.getModuleInfo().getActivityreminder().getWindow(), "activity reminder window not eql");

        assertNotNull(metricksResponse.getInstalldata().getSurveyreminder(), "Installdata.Surveyreminder field is null");
        assertEquals(metricksResponse.getInstalldata().getSurveyreminder().getTime()
                , newMetric.getModuleInfo().getSurveyreminder().getTime().toString(), "metricks reminder time not eql");
        assertEquals(metricksResponse.getInstalldata().getSurveyreminder().getWindow()
                , newMetric.getModuleInfo().getSurveyreminder().getWindow(), "survey reminder window not eql");

        assertNotNull(metricksResponse.getInstalldata().getTemperaturereminders(), "Installdata.Temperaturereminders field is null");
        assertNotNull(metricksResponse.getInstalldata().getTemperaturereminders().getStatus(), "Installdata.Temperaturereminders.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getTemperaturereminders().getStatus(), "active", "Temperaturereminders not eql");

        assertNotNull(metricksResponse.getInstalldata().getWeightreminder(), "Installdata.Weightreminder field is null");
        assertEquals(metricksResponse.getInstalldata().getWeightreminder().getTime()
                , newMetric.getModuleInfo().getWeightreminder().getTime().toString(), "Weightreminder.Time not eql");
        assertEquals(metricksResponse.getInstalldata().getWeightreminder().getWindow()
                , newMetric.getModuleInfo().getWeightreminder().getWindow(), "Weightreminder.Window not eql");

        assertNotNull(metricksResponse.getInstalldata().getWoundimagingreminders(), "Installdata.Woundimagingreminders field is null");
        assertNotNull(metricksResponse.getInstalldata().getWoundimagingreminders().getStatus(), "Installdata.Woundimagingreminders.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getWoundimagingreminders().getStatus(), "active", "Woundimagingreminders.Status not active");

        assertNotNull(metricksResponse.getInstalldata().getMedicationreminders(), "Installdata.Medicationreminders field is null");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getMedication()
                , newMetric.getModuleInfo().getMedicationreminders().get(0).getMedication(), "Installdata.Medicationreminders.Medication");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getDose()
                , newMetric.getModuleInfo().getMedicationreminders().get(0).getDose(), "Installdata.Medicationreminders.Dose");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getType()
                , newMetric.getModuleInfo().getMedicationreminders().get(0).getSchedule().getType(), "Installdata.Medicationreminders.Schedule.Type");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getExpiration()
                , newMetric.getModuleInfo().getMedicationreminders().get(0).getSchedule().getExpiration(), "Installdata.Medicationreminders.Schedule.Expiration");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getEssential()
                , newMetric.getModuleInfo().getMedicationreminders().get(0).getSchedule().getEssential(), "Installdata.Medicationreminders.Schedule.Essential");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getSchedule().getInstruction()
                , newMetric.getModuleInfo().getMedicationreminders().get(0).getSchedule().getInstruction(), "Installdata.Medicationreminders.Schedule.Instruction");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getTime()
                , newMetric.getModuleInfo().getMedicationreminders().get(0).getTime(), "Installdata.Medicationreminders.Time");
        assertEquals(metricksResponse.getInstalldata().getMedicationreminders().get(0).getWindow()
                , newMetric.getModuleInfo().getMedicationreminders().get(0).getWindow(), "Installdata.Medicationreminders.Window");

        assertNotNull(metricksResponse.getInstalldata().getGlucosereminder(), "Installdata.Glucosereminder field is null");
        assertNotNull(metricksResponse.getInstalldata().getGlucosereminder().getStatus(), "Installdata.Glucosereminder.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getGlucosereminder().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getPulseoxreminders(), "Installdata.Pulseoxreminders field is null");
        assertNotNull(metricksResponse.getInstalldata().getPulseoxreminders().getStatus(), "Installdata.Pulseoxreminders.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getPulseoxreminders().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getBloodpressurereminders(), "Installdata.Bloodpressurereminders field is null");
        assertNotNull(metricksResponse.getInstalldata().getBloodpressurereminders().getStatus(), "Installdata.Bloodpressurereminders.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getBloodpressurereminders().getStatus(), "active");

        assertNotNull(metricksResponse.getInstalldata().getActivationhistory(), "Installdata.Activationhistory field is null");
        assertNotNull(metricksResponse.getInstalldata().getActivationhistory().get(0).getStatus(), "Installdata.Activationhistory.Status field is null");
        assertEquals(metricksResponse.getInstalldata().getActivationhistory().get(0).getStatus(), "preactivate");

        List<com.hrs.cc.api.models.patients.patient_metric.response_after_add.response.Medicationreminder> everyXdaysMedications =
                metricksResponse.getInstalldata().getMedicationreminders().stream().filter(
                        m -> m.getSchedule().getType().equals("everyxdays")).collect(Collectors.toList());

        List<com.hrs.cc.api.models.patients.patient_metric.response_after_add.response.Medicationreminder> custoMedications =
                metricksResponse.getInstalldata().getMedicationreminders().stream().filter(
                        m -> m.getSchedule().getType().equals("custom")).collect(Collectors.toList());


        custoMedications.forEach(customMed -> {
            assertNotNull(customMed.getMedication(), "Medication field is null");
            assertNotNull(customMed.getDose(), "Dose field is null");
            assertNotNull(customMed.getTime(), "Time field is null");
            assertNotNull(customMed.getWindow(), "Window field is null");
            assertNotNull(customMed.getSchedule(), "Schedule field is null");
            assertNotNull(customMed.getSchedule().getType(), "Schedule.Type field is null");
            assertNotNull(customMed.getSchedule().getSchedule(), "Schedule.Schedule field is null");
            assertNotNull(customMed.getSchedule().getExpiration(), "Schedule.Expiration field is null");
            assertNotNull(customMed.getSchedule().getEssential(), "Schedule.Essential field is null");
            assertNotNull(customMed.getSchedule().getInstruction(), "Schedule.Instruction field is null");

            assertEquals(customMed.getMedication(), customDaysRemider.getMedication(), "Medication field not eql " + customDaysRemider.getMedication());
            assertEquals(customMed.getDose(), customDaysRemider.getDose(), "Dose field not eql " + customDaysRemider.getDose());
            assertEquals(customMed.getTime(), customDaysRemider.getTime(), "Time field not eql " + customDaysRemider.getTime());
            assertEquals(customMed.getWindow(), customDaysRemider.getWindow(), "Window field not eql " + customDaysRemider.getWindow());
            assertEquals(customMed.getSchedule().getType(), customDaysRemider.getSchedule().getType(), "Schedule.Type field not eql " + customDaysRemider.getSchedule().getType());
            assertEquals(customMed.getSchedule().getSchedule(), customDaysRemider.getSchedule().getSchedule(), "Schedule.Schedule field not eql " + customDaysRemider.getSchedule().getSchedule());
            assertEquals(customMed.getSchedule().getEssential(), customDaysRemider.getSchedule().getEssential(), "Schedule.Essential field not eql " + customDaysRemider.getSchedule().getEssential());
            assertEquals(customMed.getSchedule().getExpiration(), customDaysRemider.getSchedule().getExpiration(), "Schedule.Expiration field not eql " + customDaysRemider.getSchedule().getExpiration());
            assertEquals(customMed.getSchedule().getInstruction(), customDaysRemider.getSchedule().getInstruction(), "Schedule.Instruction field not eql " + customDaysRemider.getSchedule().getInstruction());
        });

        everyXdaysMedications.forEach(everyMed -> {
            assertNotNull(everyMed.getMedication(), "Medication field is null");
            assertNotNull(everyMed.getDose(), "Dose field is null");
            assertNotNull(everyMed.getTime(), "Time field is null");
            assertNotNull(everyMed.getWindow(), "Window field is null");
            assertNotNull(everyMed.getSchedule(), "Schedule field is null");
            assertNotNull(everyMed.getSchedule().getType(), "Schedule().getType field is null");
            assertNotNull(everyMed.getSchedule().getX(), "Schedule.X field is null");
            assertNotNull(everyMed.getSchedule().getStartday(), "Schedule.Startday field is null");
            assertNotNull(everyMed.getSchedule().getEssential(), "Schedule.Essential field is null");
            assertNotNull(everyMed.getSchedule().getExpiration(), "Schedule.Expiration field is null");
            assertNotNull(everyMed.getSchedule().getInstruction(), "Schedule.Instruction field is null");

            assertEquals(everyMed.getMedication(), everyXdaysReminder.getMedication(), "Medication field not eql " + everyXdaysReminder.getMedication());
            assertEquals(everyMed.getDose(), everyXdaysReminder.getDose(), "Dose field not eql " + everyXdaysReminder.getDose());
            assertEquals(everyMed.getTime(), everyXdaysReminder.getTime(), "Time field not eql " + everyXdaysReminder.getTime());
            assertEquals(everyMed.getWindow(), everyXdaysReminder.getWindow(), "Window field not eql " + everyXdaysReminder.getWindow());
            assertEquals(everyMed.getSchedule().getType(), everyXdaysReminder.getSchedule().getType(), "Schedule.Type( field not eql " + everyXdaysReminder.getSchedule().getType());
            assertEquals(everyMed.getSchedule().getX(), everyXdaysReminder.getSchedule().getX(), "Schedule.X field not eql " + everyXdaysReminder.getSchedule().getX());
            assertEquals(everyMed.getSchedule().getStartday(), everyXdaysReminder.getSchedule().getStartday(), "Schedule.Startday field not eql " + everyXdaysReminder.getSchedule().getStartday());
            assertEquals(everyMed.getSchedule().getEssential(), everyXdaysReminder.getSchedule().getEssential(), "Schedule.Essential field not eql " + everyXdaysReminder.getSchedule().getEssential());
            assertEquals(everyMed.getSchedule().getExpiration(), everyXdaysReminder.getSchedule().getExpiration(), "Schedule.Expiration field not eql " + everyXdaysReminder.getSchedule().getExpiration());
            assertEquals(everyMed.getSchedule().getInstruction(), everyXdaysReminder.getSchedule().getInstruction(), "Schedule.Instruction field not eql " + everyXdaysReminder.getSchedule().getInstruction());
        });

    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"})
    public void testGetPatientModules() {
        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENTS))
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token"))));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.MODULE_INFO));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(newReqSpec, "id", newPatientId), "get");

        PatientModulesInfo modulesInfo = response.as(PatientModulesInfo.class);
        boolean checkModules = modulesInfo.getData().getPatientmoduleinfo().getModules().stream().anyMatch(m ->
                m.equals(Modules.HELTHY) ||
                        m.equals(Modules.CHF) ||
                        m.equals(Modules.COPD) ||
                        m.equals(Modules.DIABETES) ||
                        m.equals(Modules.ACTIVITY) ||
                        m.equals(Modules.WEIGHT) ||
                        m.equals(Modules.TEMPERATURE) ||
                        m.equals(Modules.BLOOD_PRESSURE) ||
                        m.equals(Modules.MEDICATION) ||
                        m.equals(Modules.SURVEY) ||
                        m.equals(Modules.PULSE_OX) ||
                        m.equals(Modules.GLUCOSE) ||
                        m.equals(Modules.WOUND_IMAGING) ||
                        m.equals(Modules.STETHOSCOPE) ||
                        m.equals(Modules.PATIENT_CONNECT_VOICE) ||
                        m.equals(Modules.STEPS));
        assertTrue(checkModules, "Some modules are turn off");
        assertEquals(modulesInfo.getData().getPatientmoduleinfo().getActivityreminders().getTime(), newMetric.getModuleInfo().getActivityreminder().getTime(), "Not eql activity reminder time");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testGetPatientActivationStatus() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.ACTIVATIONS));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "get");
        PatientStatus status = Arrays.asList(response.as(PatientStatus[].class)).get(0);
        assertEquals(status.getStatus(), "preactivate", "Status not eql 'preactivate'");
        assertNotNull(status.getLastUpdated().getDate(), "status.getLastUpdated.Date field is null");
        assertNotNull(status.getLastUpdated().getTimezone(), "status.getLastUpdated.Timezone field is null");
        assertNotNull(status.getLastUpdated().getTimezoneType(), "status.getLastUpdated.TimezoneType field is null");
        assertEquals(response.getStatusCode(), 200);
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetQuickNote() {
        QuickNote quickNote = new QuickNote();
        quickNote.setQuicknote(new com.hrs.cc.api.models.requests.set_quick_note.Note("test quick note "
                .concat(restUtilities.getRandomString())));
        reqSpec.body(quickNote);
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        assertEquals(response.getStatusCode(), 200);
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("quicknote"), "quicknote field is null");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetWeightMetric() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.body(new SetWeight("test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(110, 450)));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("metric"), "metric field is null");
        createdMetrics.put("weight", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetWeightMetric"}, priority = 2)
    public void testEditWeightMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "weight");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("rtime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("metric", createdMetrics.get("weight").toString());
        params.put("weight", restUtilities.getRandomInt(50, 150).toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"
            , "testSetWeightMetric", "testEditWeightMetric"}, priority = 5)
    public void testDeleteWeightMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "activity");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("weight").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetActivityMetric() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(new SetActivity("test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(150, 500)));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("metric"), "metric field is null");
        createdMetrics.put("activity", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetActivityMetric"}, priority = 2)
    public void testEditActivityMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "activity");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("rtime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("metric", createdMetrics.get("activity").toString());
        params.put("duration", restUtilities.getRandomInt(0, 1000).toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetActivityMetric", "testEditActivityMetric"}, priority = 5)
    public void testDeleteActivityMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "activity");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("activity").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetBloodPressureMetric() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(new SetBloodPressure(
                "test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(100, 299),
                restUtilities.getRandomInt(100, 199),
                restUtilities.getRandomInt(20, 80)));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("metric"), "metric field is null");
        createdMetrics.put("blood", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetBloodPressureMetric"}, priority = 2)
    public void testEditBloodPressureMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "bloodpressure");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("metric", createdMetrics.get("blood").toString());
        params.put("systolic", restUtilities.getRandomInt(100, 223).toString());
        params.put("diastolic", restUtilities.getRandomInt(100, 190).toString());
        params.put("heartrate", restUtilities.getRandomInt(20, 60).toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetBloodPressureMetric", "testEditBloodPressureMetric"}, priority = 5)
    public void testDeleteBloodPressureMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "bloodpressure");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("blood").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetGlucoseMetric() {
        SetGlucose glucose = new SetGlucose();
        glucose.setReason("test reason ".concat(restUtilities.getRandomString()));
        glucose.setFtime(restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        Integer data = restUtilities.getRandomInt(80, 300);
        glucose.setReading(data);
        glucose.setGlucose(data);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(glucose);

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("metric"), "metric field is null");
        createdMetrics.put("glucose", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetGlucoseMetric"}, priority = 2)
    public void testEditGlucoseMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "glucose");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("metric", createdMetrics.get("glucose").toString());
        params.put("reading", restUtilities.getRandomInt(80, 300).toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetGlucoseMetric", "testEditGlucoseMetric"}, priority = 5)
    public void testDeleteGlucoseMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "glucose");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("glucose").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetPulseOXMetric() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(new SetPulseOx(
                "test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(30, 99),
                restUtilities.getRandomInt(80, 199)));
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("metric"), "metric field is null");
        createdMetrics.put("pulseox", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetPulseOXMetric"}, priority = 2)
    public void testEditPulseOXMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "pulseox");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("rtime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("metric", createdMetrics.get("pulseox").toString());
        params.put("spo2", restUtilities.getRandomInt(80, 99).toString());
        params.put("heartrate", restUtilities.getRandomInt(80, 199).toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetPulseOXMetric", "testEditPulseOXMetric"}, priority = 5)
    public void testDeletePulseOXMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "pulseox");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("pulseox").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetTemperatureMetric() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(new SetTemperature(
                "test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(97, 108)));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertNotNull(path.get("metric"), "metric field is null");
        createdMetrics.put("temperature", path.get("metric"));
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetTemperatureMetric"}, priority = 2)
    public void testEditTemperatureMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "temperature");
        params.put("reason", "new test reason ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("temperature").toString());
        params.put("ftime", restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"));
        params.put("temperature", restUtilities.getRandomInt(97, 108).toString());
        params.put("temperatureunit", "F");
        params.put("unit", "F");

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "put");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetTemperatureMetric", "testEditTemperatureMetric"}, priority = 5)
    public void testDeleteTemperatureMetric() {
        Map<String, String> params = new HashMap<>();
        params.put("type", "temperature");
        params.put("reason", "deleted ".concat(restUtilities.getRandomString()));
        params.put("metric", createdMetrics.get("temperature").toString());

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(params);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "delete");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 3)
    public void testGetPatientBaselineWeight() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(new SetWeight("test reason ".concat(restUtilities.getRandomString()),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getTimePeriod(2).get("current").concat(" 14:25:00"),
                restUtilities.getRandomInt(50, 100)));

        Response newWeight = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "post");
        JsonPath path = newWeight.jsonPath();
        assertEquals(path.get("status"), "ok");


        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT))
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")))
        );

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENT_METRIC);

        restUtilities.createQueryParam(newReqSpec, "type", "weight");
        restUtilities.createQueryParam(newReqSpec, "criteria", "first");

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(newReqSpec, "id", newPatientId), "get");

        WeightMetric weightMetric = response.as(WeightMetric.class);
        assertNotNull(weightMetric.getId(), "Id field is null");
        assertEquals(weightMetric.getPatient(), newPatientId, "patientid not eql " + newPatientId);
        assertNotNull(weightMetric.getClinician(), "Clinician field is null");
        assertNotNull(weightMetric.getType(), "Type field is null");
        assertNotNull(weightMetric.getWeight(), "Weight field is null");
        assertNotNull(weightMetric.getReminder(), "Reminder field is null");
        assertNotNull(weightMetric.getFinished(), "Finished field is null");
        assertNotNull(weightMetric.getTime(), "Time field is null");
        assertNotNull(weightMetric.getTime().getDate(), "Time().getDate field is null");
        assertNotNull(weightMetric.getTime().getTimezone(), "Time().getTimezone field is null");
        assertNotNull(weightMetric.getTime().getTimezoneType(), "Time().getTimezoneType field is null");
        assertNotNull(weightMetric.getStatus(), "Status field is null");
        assertNotNull(weightMetric.getReason(), "Reason field is null");
        assertNotNull(weightMetric.getLastUpdated(), "LastUpdated field is null");
        assertNotNull(weightMetric.getLastUpdated().getDate(), "LastUpdated.Date field is null");
        assertNotNull(weightMetric.getLastUpdated().getTimezone(), "LastUpdated.Timezone field is null");
        assertNotNull(weightMetric.getLastUpdated().getTimezoneType(), "LastUpdated.TimezoneType field is null");
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {
            "testAddNewPatient",
            "testSetTimeZoneToPatient",
            "testSetMetricsToPatient",
            "testSetQuickNote",
            "testSetActivityMetric",
            "testSetBloodPressureMetric",
            "testSetGlucoseMetric",
            "testSetPulseOXMetric",
            "testSetTemperatureMetric"
    }, priority = 1)
    public void testGetPatientProfile() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", newPatientId), "get");
        Profile profile = response.as(Profile.class);
        assertEquals(profile.getTimezone(), "American/Denver", "Timezone field not eql " + "American/Denver");
        assertNotNull(profile.getClinician(), "Clinician field is null");
        assertNotNull(profile.getClinician().getName(), "Clinician.Name field is null");
        assertNotNull(profile.getClinician().getHrsid(), "Clinician.Hrsid field is null");
        assertNotNull(profile.getExtension(), "Extension field is null");
//        assertEquals(profile.getGender(), patientData.getGender());
        assertNotNull(profile.getReadmission(), "Readmission field is null");
        assertNotNull(profile.getEdvisit(), "Edvisit field is null");
        assertNotNull(profile.getTestpatient(), "Testpatient field is null");
        assertEquals(profile.getSubgroup(), "test subgroup", "Subgroupfield not eql " + "test subgroup");
        assertEquals(profile.getPid(), patientDataResponse.getPid(), "Pid field not eql " + patientDataResponse.getPid());
        assertEquals(profile.getLanguage(), "english", "Language field not eql " + "english");
        assertEquals(profile.getHrsid(), newPatientId, "Hrsidfield not eql " + newPatientId);
        assertNotNull(profile.getStartdate(), "Startdate field is null");
        assertNotNull(profile.getLastmoduleupdate(), "Lastmoduleupdate field is null");
        assertNotNull(profile.getVolume(), "Volume field is null");
        assertEquals(profile.getPhone(), patientDataResponse.getPhone(), "Phone field not eql " + patientDataResponse.getPhone());
        assertEquals(profile.getDob(), patientDataResponse.getDob(), "Dob field not eql " + patientDataResponse.getDob());
        assertEquals(profile.getName().getMiddle(), patientDataResponse.getName().getMiddle(), "Name.Middle field not eql " + patientDataResponse.getName().getMiddle());
        assertEquals(profile.getName().getLast(), patientDataResponse.getName().getLast(), "Name.Last field not eql " + patientDataResponse.getName().getLast());
        assertEquals(profile.getName().getFirst(), patientDataResponse.getName().getFirst(), "Name.First field not eql " + patientDataResponse.getName().getFirst());
        assertNotNull(profile.getEnvphone(), "Envphone field is null");
        assertNotNull(profile.getConditions(), "Conditions field is null");
        assertNotNull(profile.getAudioreminders(), "Audioreminders field is null");
        assertEquals(profile.getStatus(), "activated", "Status field not eql " + "activated");

        assertNotNull(profile.getPcv().getActive(), "Pcv.Active field is null");
        assertNotNull(profile.getPcv().getPhone(), "Pcv.Phone field is null");

        profile.getPatientInfoCustomAttributes().forEach(attribute -> {
            assertNotNull(attribute.getId(), "Id field is null");
            assertNotNull(attribute.getName(), "Name field is null");
            assertNotNull(attribute.getType(), "Type field is null");
            assertNotNull(attribute.getRequired(), "Required field is null");
            assertNotNull(attribute.getPatientsetup(), "Patientsetup field is null");
            assertNotNull(attribute.get_class(), "_class field is null");
            assertNotNull(attribute.getEmrTracked(), "EmrTracked field is null");
            assertNotNull(attribute.getLastUpdated(), "LastUpdated field is null");
        });
        restUtilities.removePathParam(reqSpec, "id");
    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient"}, priority = 1)
    public void testSetGenderUnknownToPatientSettings() {

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT)));


        SettingPatient settingPatient = new SettingPatient();
        List<CustomNewAtributs> custom = new ArrayList<>();

        CustomNewAtributs first = new CustomNewAtributs();
        first.setId(restUtilities.getRandomInt(1, 100));
        first.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomNewAtributs two = new CustomNewAtributs();
        two.setId(restUtilities.getRandomInt(1, 100));
        two.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        two.setType("boolean");
        two.setRequired(true);
        two.setPatientsetup(true);
        two.set_class("col-xs-3");
        two.setEmrTracked(false);
        two.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        two.set$$hashKey("object:2415");
        two.setValue("true");

        CustomNewAtributs three = new CustomNewAtributs();
        three.setId(restUtilities.getRandomInt(1, 100));
        three.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        three.setType("boolean");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-3");
        three.setEmrTracked(false);
        three.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        three.set$$hashKey("object:2415");
        three.setValue("true");

        custom.add(first);
        custom.add(two);
        custom.add(three);
        settingPatient.setPATIENTINFO_CUSTOMATTRIBUTES(custom);
        settingPatient.setSubgroup("test subgroup");

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        settingPatient.setLasthospitalization(RandomData.getTimePeriod(5, dateFormat).get("minusDays"));
        settingPatient.setGender(GenderTypes.UNKNOWN);
        settingPatient.setAlternatefirstname(restUtilities.getRandomString());
        settingPatient.setAlternatelastname(restUtilities.getRandomString());
        settingPatient.setAlternatetelephone(restUtilities.getRandomString());
        newReqSpec.body(settingPatient);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertEquals(response.statusCode(), 200);

        Profile profile = RestAssistant.getPatientById(newPatientId);
        assertEquals(profile.getGender(), GenderTypes.UNKNOWN);

    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient", "testSetGenderUnknownToPatientSettings"}, priority = 1)
    public void testSetGenderOtherToPatientSettings() {
        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT)));


        SettingPatient settingPatient = new SettingPatient();
        List<CustomNewAtributs> custom = new ArrayList<>();

        CustomNewAtributs first = new CustomNewAtributs();
        first.setId(restUtilities.getRandomInt(1, 100));
        first.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomNewAtributs two = new CustomNewAtributs();
        two.setId(restUtilities.getRandomInt(1, 100));
        two.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        two.setType("boolean");
        two.setRequired(true);
        two.setPatientsetup(true);
        two.set_class("col-xs-3");
        two.setEmrTracked(false);
        two.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        two.set$$hashKey("object:2415");
        two.setValue("true");

        CustomNewAtributs three = new CustomNewAtributs();
        three.setId(restUtilities.getRandomInt(1, 100));
        three.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        three.setType("boolean");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-3");
        three.setEmrTracked(false);
        three.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        three.set$$hashKey("object:2415");
        three.setValue("true");

        custom.add(first);
        custom.add(two);
        custom.add(three);
        settingPatient.setPATIENTINFO_CUSTOMATTRIBUTES(custom);
        settingPatient.setSubgroup("test subgroup");

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        settingPatient.setLasthospitalization(RandomData.getTimePeriod(5, dateFormat).get("minusDays"));
        settingPatient.setGender(GenderTypes.OTHER);
        settingPatient.setAlternatefirstname(restUtilities.getRandomString());
        settingPatient.setAlternatelastname(restUtilities.getRandomString());
        settingPatient.setAlternatetelephone(restUtilities.getRandomString());
        newReqSpec.body(settingPatient);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertEquals(response.statusCode(), 200);

        Profile profile = RestAssistant.getPatientById(newPatientId);
        assertEquals(profile.getGender(), GenderTypes.OTHER);

    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient",
            "testSetGenderUnknownToPatientSettings", "testSetGenderOtherToPatientSettings"}, priority = 1)
    public void testSetGenderMaleToPatientSettings() {
        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT)));


        SettingPatient settingPatient = new SettingPatient();
        List<CustomNewAtributs> custom = new ArrayList<>();

        CustomNewAtributs first = new CustomNewAtributs();
        first.setId(restUtilities.getRandomInt(1, 100));
        first.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomNewAtributs two = new CustomNewAtributs();
        two.setId(restUtilities.getRandomInt(1, 100));
        two.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        two.setType("boolean");
        two.setRequired(true);
        two.setPatientsetup(true);
        two.set_class("col-xs-3");
        two.setEmrTracked(false);
        two.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        two.set$$hashKey("object:2415");
        two.setValue("true");

        CustomNewAtributs three = new CustomNewAtributs();
        three.setId(restUtilities.getRandomInt(1, 100));
        three.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        three.setType("boolean");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-3");
        three.setEmrTracked(false);
        three.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        three.set$$hashKey("object:2415");
        three.setValue("true");

        custom.add(first);
        custom.add(two);
        custom.add(three);
        settingPatient.setPATIENTINFO_CUSTOMATTRIBUTES(custom);
        settingPatient.setSubgroup("test subgroup");

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        settingPatient.setLasthospitalization(RandomData.getTimePeriod(5, dateFormat).get("minusDays"));
        settingPatient.setGender(GenderTypes.MALE);
        settingPatient.setAlternatefirstname(restUtilities.getRandomString());
        settingPatient.setAlternatelastname(restUtilities.getRandomString());
        settingPatient.setAlternatetelephone(restUtilities.getRandomString());
        newReqSpec.body(settingPatient);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertEquals(response.statusCode(), 200);

        Profile profile = RestAssistant.getPatientById(newPatientId);
        assertEquals(profile.getGender(), GenderTypes.MALE);

    }

    @Test(dependsOnMethods = {"testAddNewPatient",
            "testSetMetricsToPatient",
            "testSetGenderUnknownToPatientSettings",
            "testSetGenderOtherToPatientSettings",
            "testSetGenderMaleToPatientSettings"}, priority = 1)
    public void testSetEmptyGenderToPatientSettings() {
        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT)));

        SettingPatient settingPatient = new SettingPatient();
        List<CustomNewAtributs> custom = new ArrayList<>();

        CustomNewAtributs first = new CustomNewAtributs();
        first.setId(restUtilities.getRandomInt(1, 100));
        first.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomNewAtributs two = new CustomNewAtributs();
        two.setId(restUtilities.getRandomInt(1, 100));
        two.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        two.setType("boolean");
        two.setRequired(true);
        two.setPatientsetup(true);
        two.set_class("col-xs-3");
        two.setEmrTracked(false);
        two.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        two.set$$hashKey("object:2415");
        two.setValue("true");

        CustomNewAtributs three = new CustomNewAtributs();
        three.setId(restUtilities.getRandomInt(1, 100));
        three.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        three.setType("boolean");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-3");
        three.setEmrTracked(false);
        three.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        three.set$$hashKey("object:2415");
        three.setValue("true");

        custom.add(first);
        custom.add(two);
        custom.add(three);
        settingPatient.setPATIENTINFO_CUSTOMATTRIBUTES(custom);
        settingPatient.setSubgroup("test subgroup");

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        settingPatient.setLasthospitalization(RandomData.getTimePeriod(5, dateFormat).get("minusDays"));
        settingPatient.setGender(GenderTypes.EMPTY);
        settingPatient.setAlternatefirstname(restUtilities.getRandomString());
        settingPatient.setAlternatelastname(restUtilities.getRandomString());
        settingPatient.setAlternatetelephone(restUtilities.getRandomString());
        newReqSpec.body(settingPatient);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertEquals(response.statusCode(), 200);

        Profile profile = RestAssistant.getPatientById(newPatientId);
        assertEquals(profile.getGender(), GenderTypes.EMPTY);

    }

    @Test(dependsOnMethods = {"testAddNewPatient", "testSetMetricsToPatient",
            "testSetGenderUnknownToPatientSettings", "testSetGenderOtherToPatientSettings",
            "testSetGenderMaleToPatientSettings", "testSetEmptyGenderToPatientSettings"}, priority = 1)
    public void testSetPatientSettings() {
        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2.concat(Path.PATIENT)));


        SettingPatient settingPatient = new SettingPatient();
        List<CustomNewAtributs> custom = new ArrayList<>();

        CustomNewAtributs first = new CustomNewAtributs();
        first.setId(restUtilities.getRandomInt(1, 100));
        first.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomNewAtributs two = new CustomNewAtributs();
        two.setId(restUtilities.getRandomInt(1, 100));
        two.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        two.setType("boolean");
        two.setRequired(true);
        two.setPatientsetup(true);
        two.set_class("col-xs-3");
        two.setEmrTracked(false);
        two.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        two.set$$hashKey("object:2415");
        two.setValue("true");

        CustomNewAtributs three = new CustomNewAtributs();
        three.setId(restUtilities.getRandomInt(1, 100));
        three.setName("Test new custom attributes ".concat(restUtilities.getRandomString()));
        three.setType("boolean");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-3");
        three.setEmrTracked(false);
        three.setLastUpdated(restUtilities.getTimePeriod(2).get("current").concat("T11:39:00+0000"));
        three.set$$hashKey("object:2415");
        three.setValue("true");

        custom.add(first);
        custom.add(two);
        custom.add(three);
        settingPatient.setPATIENTINFO_CUSTOMATTRIBUTES(custom);
        settingPatient.setSubgroup("test subgroup");

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        settingPatient.setLasthospitalization(RandomData.getTimePeriod(5, dateFormat).get("minusDays"));
        settingPatient.setGender(GenderTypes.FEMALE);
        settingPatient.setAlternatefirstname(restUtilities.getRandomString());
        settingPatient.setAlternatelastname(restUtilities.getRandomString());
        settingPatient.setAlternatetelephone(restUtilities.getRandomString());
        newReqSpec.body(settingPatient);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(restUtilities.createPathParam(newReqSpec, "id", newPatientId), "post");
        JsonPath path = response.jsonPath();
        assertEquals(path.get("status"), "ok");
        assertEquals(response.statusCode(), 200);

        Profile profile = RestAssistant.getPatientById(newPatientId);
        assertEquals(profile.getGender(), GenderTypes.FEMALE);
    }

    @Test(priority = 6)
    public void testGetModulesCCMobile() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        ResponseSpecification resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.API_V1);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.MODULES);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
        ModulesCCMobile modules = response.as(ModulesCCMobile.class);
        boolean assertionField = modules.getData().stream().anyMatch(m ->
                m.getAttributes().getName().equals(EnvModules.ACTIVITY.get()) ||
                        m.getAttributes().getName().equals(EnvModules.BLOOD_PRESSURE.get()) ||
                        m.getAttributes().getName().equals(EnvModules.GLUCOSE.get()) ||
                        m.getAttributes().getName().equals(EnvModules.MEDICATION.get()) ||
                        m.getAttributes().getName().equals(EnvModules.PULSE_OX.get()) ||
                        m.getAttributes().getName().equals(EnvModules.SURVEY.get()) ||
                        m.getAttributes().getName().equals(EnvModules.TEMPERATURE.get()) ||
                        m.getAttributes().getName().equals(EnvModules.WEIGHT.get()) ||
                        m.getAttributes().getName().equals(EnvModules.WOUND_IMAGING.get()) ||
                        m.getAttributes().getName().equals(EnvModules.PATIENTCONNECT_VOICE.get()) ||
                        m.getAttributes().getName().equals(EnvModules.STETHOSCOPE.get()) &&
                                m.getAttributes().getShortname().equals(Modules.ACTIVITY) ||
                        m.getAttributes().getShortname().equals(Modules.BLOOD_PRESSURE) ||
                        m.getAttributes().getShortname().equals(Modules.GLUCOSE) ||
                        m.getAttributes().getShortname().equals(Modules.MEDICATION) ||
                        m.getAttributes().getShortname().equals(Modules.PULSE_OX) ||
                        m.getAttributes().getShortname().equals(Modules.SURVEY) ||
                        m.getAttributes().getShortname().equals(Modules.TEMPERATURE) ||
                        m.getAttributes().getShortname().equals(Modules.WEIGHT) ||
                        m.getAttributes().getShortname().equals(Modules.WOUND_IMAGING) ||
                        m.getAttributes().getShortname().equals(Modules.STETHOSCOPE) ||
                        m.getAttributes().getShortname().equals(Modules.STEPS));
        assertTrue(assertionField, "some modules ccmodules are turn off");
    }

}
package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.admin.edit_survey.SurveysTypes;
import com.hrs.cc.api.models.admin.edit_survey.Value;
import com.hrs.cc.api.models.admin.surveys.AdminSurveys;
import com.hrs.cc.api.models.admin.surveys_types.AdminSurveysTypes;
import com.hrs.cc.api.models.admin.surveys_types.Type;
import core.rest.RestUtilities;
import core.rest.auth.schemes.AuthScheme;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Headers;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TestAdminPanelRequests extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private static Map<String, String> cookies = new HashMap<>();
    private static Headers headers;
    private ObjectMapper mapper = new ObjectMapper();
    private AdminSurveys adminSurveys;
    private AdminSurveysTypes adminSurveysTypes;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        Response response = Connection.adminAuthGeneral();
        cookies = response.getCookies();
        headers = response.getHeaders();
        restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.API_ADMIN_V2);

    }

    @AfterMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
    }

    @Test
    public void testAllSurveysInAdminPanel() {
        Map<String, String> data = new HashMap<>();
        data.put("request", "getsurveylist");
        data.put("environment", "QATestingZone");

        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        reqSpec.header("authorization", AuthScheme.basicScheme("admin","LaCroix").generateAuthToken());
        reqSpec.cookies(cookies);
        restUtilities.setEndPoint(EndPoints.DATA);
        reqSpec.formParams(data);

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200)
                        .registerParser("text/html", Parser.JSON));

        Response response = restUtilities.getResponse(reqSpec, "post");

        try {
            adminSurveys = mapper.readValue(response.getBody().asString(), AdminSurveys.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        adminSurveys.getSurveylist().forEach(s->{
            assertNotNull(s.getId(), "Survey id is null");
            assertNotNull(s.getCategory(), "Survey category is null");
            assertNotNull(s.getQuestion(), "Survey question is null");
            assertNotNull(s.getAnswertype(), "Survey answer type is null");
            assertNotNull(s.getSchedule(), "Survey schedule is null");
        });
    }

    @Test
    public void testGetGlobalSurveyQuestionTypesInAdminPanel() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_ADMIN_V2);

        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        reqSpec.header("authorization", AuthScheme.basicScheme("admin","LaCroix").generateAuthToken());
        reqSpec.cookies(cookies);
        restUtilities.setEndPoint(EndPoints.DATA);

        Map<String, Object> data = new HashMap<>();
        data.put("request", "getglobalsurveyquestiontypes");
        reqSpec.formParams(data);

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200)
                        .registerParser("text/html", Parser.JSON));

        Response response = restUtilities.getResponse(reqSpec, "post");
        ObjectMapper mapper = new ObjectMapper();

        try {
            adminSurveysTypes = mapper.readValue(response.getBody().asString(), AdminSurveysTypes.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        adminSurveysTypes.getTypes().forEach(t->{
            assertNotNull(t.getId(), "Survey id field is null");
            assertNotNull(t.getName(), "Survey name field is null");
            assertNotNull(t.getBest(), "Survey best field is null");
            assertNotNull(t.getWorst(), "Survey worst field is null");
            assertNotNull(t.getValues(), "Survey values field is null");
        });

    }

    @Test(dependsOnMethods = {"testGetGlobalSurveyQuestionTypesInAdminPanel"})
    public void testEditGlobalSurveyQuestionTypeInAdminPanel() {
        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_ADMIN_V2);

        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        reqSpec.header("authorization", AuthScheme.basicScheme("admin","LaCroix").generateAuthToken());
        reqSpec.cookies(cookies);
        restUtilities.setEndPoint(EndPoints.DATA);


        SurveysTypes type = new SurveysTypes();
        type.setId(adminSurveysTypes.getTypes().get(0).getId());
        type.setBest(adminSurveysTypes.getTypes().get(0).getBest());
        type.setWorst(adminSurveysTypes.getTypes().get(0).getWorst());
        type.setType(adminSurveysTypes.getTypes().get(0).getName());
        ArrayList<String> list = new ArrayList<>();
        list.add("yes_test");
        list.add("no_test");
        Value value = new Value();
        value.setLang("eng");
        value.setText(list);

        ArrayList<Value> values = new ArrayList<>();
        values.add(value);
        type.setValues(values);

        Map<String, Object> data = new HashMap<>();
        data.put("request", "editglobalsurveyquestiontype");
        data.put("data", type);
        reqSpec.formParams(data);

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200)
                .registerParser("text/html", Parser.JSON));

        Response response = restUtilities.getResponse(reqSpec, "post");
        assertEquals(response.getStatusCode(), 200);

        RestUtilities restUtil = new RestUtilities();
        restUtil.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
        RequestSpecification req = restUtil.getRequestSpecification();
        req.basePath(Path.API_ADMIN_V2);

        restUtil.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        req.header("authorization", AuthScheme.basicScheme("admin","LaCroix").generateAuthToken());
        req.cookies(cookies);
        restUtil.setEndPoint(EndPoints.DATA);

        Map<String, Object> data1 = new HashMap<>();
        data1.put("request", "getglobalsurveyquestiontypes");
        req.formParams(data1);

        restUtil.getResponseSpecification(
                restUtil.createResponseBuilder()
                        .expectStatusCode(200)
                .registerParser("text/html", Parser.JSON));

        Response check = restUtil.getResponse(req, "post");
        try {
            adminSurveysTypes = mapper.readValue(check.getBody().asString(), AdminSurveysTypes.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Type changedType = adminSurveysTypes.getTypes().stream().filter(t -> t.getId().equals(type.getId())).findFirst().get();
        assertEquals(changedType.getValues().toString(), type.getValues().toString(), "Values are not eqls");
    }

}

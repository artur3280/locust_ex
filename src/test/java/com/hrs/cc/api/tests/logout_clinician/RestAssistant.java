package com.hrs.cc.api.tests.logout_clinician;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.account_data.Account;
import com.hrs.cc.api.models.environment.settings.EnvSettings;
import com.hrs.cc.api.models.requests.add_new_clinicain.NewClinician;
import com.hrs.cc.api.models.requests.set_patient_metricks.*;
import core.PropertyWorker;
import core.rest.RestUtilities;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.util.*;

public class RestAssistant {
    private static RequestSpecification reqSpec;
    private static ResponseSpecification resPec;
    private static RestUtilities restUtilities;
    private static final String TOKEN_PATH = "/token.json";
    private static PropertyWorker propertyWorker = new PropertyWorker(Connection.AUTH_PROP);



    static Account moderatorData() {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2);

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtilities.getResponse(reqSpec, "get");
        return response.as(Account.class);
    }

}

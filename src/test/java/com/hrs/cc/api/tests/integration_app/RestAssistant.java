package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.EnvFlags;
import com.hrs.cc.api.constans.Modules;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.account_data.Account;
import com.hrs.cc.api.models.account_data.clinician.Clinician;
import com.hrs.cc.api.models.environment.settings.EnvSettings;
import com.hrs.cc.api.models.environment.settings.Setting;
import com.hrs.cc.api.models.integratioins_app.clinician_auth_token.ClinicianAuthToken;
import com.hrs.cc.api.models.integratioins_app.connected_devices.DataDevices;
import com.hrs.cc.api.models.integratioins_app.connected_devices.Devices;
import com.hrs.cc.api.models.integratioins_app.ivr.questions.IvrQuestionsList;
import com.hrs.cc.api.models.integratioins_app.patient_tasks.DatumTask;
import com.hrs.cc.api.models.integratioins_app.patient_tasks.PatientTasksApp;
import com.hrs.cc.api.models.integratioins_app.user_by_type_in_env.UsersByTypeInEnv;
import com.hrs.cc.api.models.inventory.InventoryResponse;
import com.hrs.cc.api.models.patients.Patients;
import com.hrs.cc.api.models.patients.education.Education;
import com.hrs.cc.api.models.patients.metrics_data.Metrics;
import com.hrs.cc.api.models.patients.metrics_data.PatientMetricsData;
import com.hrs.cc.api.models.patients.patient_list.PatientList;
import com.hrs.cc.api.models.patients.survey.Survey;
import com.hrs.cc.api.models.requests.add_new_clinicain.NewClinician;
import com.hrs.cc.api.models.requests.add_new_clinicain.Profile;
import com.hrs.cc.api.models.requests.add_new_patient.CustomAtributs;
import com.hrs.cc.api.models.requests.add_new_patient.Name;
import com.hrs.cc.api.models.requests.add_new_patient.NewPatient;
import com.hrs.cc.api.models.requests.generation_magic_codes.Data;
import com.hrs.cc.api.models.requests.generation_magic_codes.MagiCodeIdentityPacient;
import com.hrs.cc.api.models.requests.request_magic_code_to_app.Datum;
import com.hrs.cc.api.models.requests.request_magic_code_to_app.MagiCodePacientDevice;
import com.hrs.cc.api.models.requests.set_patient_metricks.*;
import com.hrs.cc.api.models.requests.turn_education_content.SetPatientContent;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import core.rest.auth.schemes.AuthScheme;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class RestAssistant {
    private static RequestSpecification reqSpec;
    private static ResponseSpecification resPec;
    private static RestUtilities restUtilities;
    private static Map<String, String> userData;
    public static String newPatientId;
    private static final String TOKEN_PATH = "/token.json";
    public static NewMetricks newMetrick;
    private static PropertyWorker propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
    public static NewClinician clinician;
    private static EnvSettings settings;

    static String getPatientByName(String firstName, String lastName) {

        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        try {
            if (propertyWorker.getProperty("patient_hrs_id") == null) throw new NullPointerException("id is null");
            return propertyWorker.getProperty("patient_hrs_id");
        } catch (NullPointerException e) {
            RestUtilities restUtilities = new RestUtilities();
            restUtilities.setBaseUri(Path.BASE_URI);
            RequestSpecification reqSpec = restUtilities.getRequestSpecification();
            reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));
            ResponseSpecification resPec = restUtilities.getResponseSpecification();

            restUtilities.setContentType(ContentType.JSON);
            restUtilities.setEndPoint();
            reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

            restUtilities.createQueryParams(reqSpec, "status[]",
                    "Activated",
                    "Pre-Activated",
                    "Paused",
                    "Deactivated",
                    "Discharged");

            Response response = restUtilities.getResponse(reqSpec, "get");
            Patients patients = response.as(Patients.class);


            PatientList p = patients.getPatientlist().stream().filter(
                    patient -> patient.get(0).getProfile().getName().getFirst().equals(firstName) &&
                            patient.get(0).getProfile().getName().getLast().equals(lastName)).collect(Collectors.toList()).get(0).get(0);

            if(p.getProfile().getHrsid().isEmpty())throw new NullPointerException("Patient does not found");
            return p.getProfile().getHrsid();
        }

    }

    static String getPatientList() {
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities = new RestUtilities();

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.createQueryParams(reqSpec, "status[]",
                "Activated"/*,
                "Pre-Activated",
                "Paused",
                "Deactivated",
                "Discharged"*/);
        Response response = restUtilities.getResponse(reqSpec, "get");
        Patients patients = response.as(Patients.class);


        return patients.getPatientlist().stream().findFirst().get().get(0).getProfile().getHrsid();
    }

    public static String addNewPatient() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENT));
        resPec = restUtilities.getResponseSpecification();

        NewPatient patientData = new NewPatient();

        List<CustomAtributs> customAttributes = new ArrayList<>();

        CustomAtributs first = new CustomAtributs();
        first.setId(1);
        first.setName("Test custom attributes");
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated("2017-08-31T11:39:00+0000");
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomAtributs two = new CustomAtributs();
        two.setId(2);
        two.setName("Nickname");
        two.setType("text");
        two.setRequired(false);
        two.setPatientsetup(true);
        two.set_class("col-xs-6");
        two.setEmrTracked(false);
        two.setLastUpdated("2017-09-25T14:22:07+0000");
        two.set$$hashKey("object:2416");
        two.setValue("testnick");

        CustomAtributs three = new CustomAtributs();
        three.setId(4);
        three.setName("Custom Text Field");
        three.setType("text");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-12");
        three.setEmrTracked(false);
        three.setLastUpdated("2017-10-16T14:44:33+0000");
        three.set$$hashKey("object:2417");
        three.setValue("test custom field");

        customAttributes.add(first);
        customAttributes.add(two);
        customAttributes.add(three);
        patientData.setCustomAtributsList(customAttributes);

        Name name = new Name(
                "Test first name ".concat(RandomData.getRandomString(5))
                , "Test middle name ".concat(RandomData.getRandomString(5))
                , "Test last name ".concat(RandomData.getRandomString(5)));

        patientData.setName(name);

        patientData.setDob("01/01/1990");
        patientData.setGender("F");
        patientData.setPhone("9082237696");
        patientData.setPid("testPatient".concat(RandomData.getRandomString(5)));
        reqSpec.body(patientData);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");

        JsonPath jsonPath = response.jsonPath();
        return jsonPath.get("hrsid");
    }

    static void setModulesToPatientById(String patientId) {
        PropertyWorker propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        String date = RandomData.getTimePeriod(25, dateFormat).get("plusDays");

        RestUtilities restUtilities = new RestUtilities();

        restUtilities.setBaseUri(Path.BASE_URI);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        ResponseSpecification resPec = restUtilities.getResponseSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.MODULE_INFO));

        newMetrick = new NewMetricks();
        ModuleInfo info = new ModuleInfo();

        List<String> modulesList = new ArrayList<>();
//        modulesList.add(Modules.HELTHY);
//        modulesList.add(Modules.CHF);
//        modulesList.add(Modules.COPD);
//        modulesList.add(Modules.DIABETES);
        modulesList.add(Modules.ACTIVITY);
        modulesList.add(Modules.BLOOD_PRESSURE);
        modulesList.add(Modules.GLUCOSE);
        modulesList.add(Modules.MEDICATION);
        modulesList.add(Modules.PULSE_OX);
        modulesList.add(Modules.SURVEY);
        modulesList.add(Modules.TEMPERATURE);
        modulesList.add(Modules.WEIGHT);
        modulesList.add(Modules.WOUND_IMAGING);
        modulesList.add(Modules.PATIENT_CONNECT_VOICE);
        modulesList.add(Modules.STETHOSCOPE);
        modulesList.add(Modules.STEPS);
        info.setModules(modulesList);
        info.setActivityreminder(new Activityreminder("20", "-"));
        info.setSurveyreminder(new Surveyreminder("60", "600"));
        info.setWeightreminder(new Weightreminder("60", "540"));

        List<Medicationreminder> medicationreminderList = new ArrayList<>();

        Medicationreminder SULFAPYRIDINE = new Medicationreminder();
        SULFAPYRIDINE.setMedication("SULFAPYRIDINE");
        SULFAPYRIDINE.setDose("120 mg x1 Oral");
        SULFAPYRIDINE.setSchedule(new Schedule("undefined",
                "everyday",
                date,
                "test special instruction: ".concat(restUtilities.getRandomString())));
        SULFAPYRIDINE.setTimes("PRN");
        SULFAPYRIDINE.setTime("PRN");
        SULFAPYRIDINE.setWindow("30");
        medicationreminderList.add(SULFAPYRIDINE);

        Medicationreminder PERCORTEN = new Medicationreminder();
        PERCORTEN.setMedication("PERCORTEN");
        PERCORTEN.setDose("122 ml x1 Oral");
        PERCORTEN.setSchedule(new Schedule("undefined",
                "everyday",
                date,
                "test special instruction: ".concat(restUtilities.getRandomString())));
        PERCORTEN.setTimes("PRN");
        PERCORTEN.setTime("PRN");
        PERCORTEN.setWindow("30");
        medicationreminderList.add(PERCORTEN);

        Medicationreminder METANDREN = new Medicationreminder();
        METANDREN.setMedication("METANDREN");
        METANDREN.setDose("1 tblsp x2 Oral");
        METANDREN.setSchedule(new Schedule("undefined",
                "everyday",
                date,
                "test special instruction: ".concat(restUtilities.getRandomString())));
        METANDREN.setTimes("PRN");
        METANDREN.setTime("PRN");
        METANDREN.setWindow("30");
        medicationreminderList.add(METANDREN);

        Medicationreminder STILBETIN = new Medicationreminder();
        STILBETIN.setMedication("STILBETIN");
        STILBETIN.setDose("1 mg x1 Oral");
        STILBETIN.setSchedule(new Schedule("false", "everyday", "test special instruction: ".concat(restUtilities.getRandomString())));
        STILBETIN.setTimes("PRN");
        STILBETIN.setTime("PRN");
        STILBETIN.setWindow("30");
        medicationreminderList.add(STILBETIN);

        Medicationreminder everyXdaysReminder = new Medicationreminder();
        everyXdaysReminder.setMedication("LIQUAEMIN SODIUM");
        everyXdaysReminder.setDose("12 mg x1 Oral");
        Schedule scheduleEveryXdaysReminder = new Schedule();
        scheduleEveryXdaysReminder.setEssential("true");
        scheduleEveryXdaysReminder.setType("everyxdays");
        scheduleEveryXdaysReminder.setX(restUtilities.getRandomInt(1, 7).toString());
        scheduleEveryXdaysReminder.setStartday(String.valueOf(restUtilities.getCurrentTimeStamp()));
        scheduleEveryXdaysReminder.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleEveryXdaysReminder.setExpiration(date);
        everyXdaysReminder.setSchedule(scheduleEveryXdaysReminder);
        everyXdaysReminder.setTime("540");
        everyXdaysReminder.setTimes("9:00AM");
        everyXdaysReminder.setWindow("60");
        medicationreminderList.add(everyXdaysReminder);

        Medicationreminder customDaysRemider = new Medicationreminder();
        customDaysRemider.setMedication("ALCOHOL 10% AND DEXTROSE 5%");
        customDaysRemider.setDose("200 mg x2 Oral");
        Schedule scheduleCustomDaysRemider = new Schedule();
        scheduleCustomDaysRemider.setEssential("true");
        scheduleCustomDaysRemider.setType("custom");
        scheduleCustomDaysRemider.setSchedule(Modules.customMedicationsDays);
        scheduleCustomDaysRemider.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleCustomDaysRemider.setExpiration(date);
        customDaysRemider.setSchedule(scheduleCustomDaysRemider);
        customDaysRemider.setTime("540");
        customDaysRemider.setTimes("9:00AM");
        customDaysRemider.setWindow("60");
        medicationreminderList.add(customDaysRemider);

        info.setMedicationreminders(medicationreminderList);
        newMetrick.setModuleInfo(info);

        reqSpec.body(newMetrick);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
        if (response.statusCode() == 200) {
            System.out.println("Set metrics to patient: " + patientId);
        } else {
            System.out.println("Something wrong! Please check!");
        }
    }

    static void setModulesToPatientByIdWithoutPCV(String patientId) {
        PropertyWorker propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        String date = RandomData.getTimePeriod(25, dateFormat).get("plusDays");

        RestUtilities restUtilities = new RestUtilities();

        restUtilities.setBaseUri(Path.BASE_URI);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        ResponseSpecification resPec = restUtilities.getResponseSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.MODULE_INFO));

        newMetrick = new NewMetricks();
        ModuleInfo info = new ModuleInfo();

        List<String> modulesList = new ArrayList<>();
        modulesList.add(Modules.HELTHY);
        modulesList.add(Modules.CHF);
        modulesList.add(Modules.COPD);
        modulesList.add(Modules.DIABETES);
        modulesList.add(Modules.ACTIVITY);
        modulesList.add(Modules.WEIGHT);
        modulesList.add(Modules.TEMPERATURE);
        modulesList.add(Modules.BLOOD_PRESSURE);
        modulesList.add(Modules.MEDICATION);
        modulesList.add(Modules.SURVEY);
        modulesList.add(Modules.PULSE_OX);
        modulesList.add(Modules.GLUCOSE);
        modulesList.add(Modules.WOUND_IMAGING);
        modulesList.add(Modules.STEPS);
        modulesList.add(Modules.STETHOSCOPE);
        info.setModules(modulesList);
        info.setActivityreminder(new Activityreminder("20", "-"));
        info.setSurveyreminder(new Surveyreminder("60", "600"));
        info.setWeightreminder(new Weightreminder("60", "540"));

        List<Medicationreminder> medicationreminderList = new ArrayList<>();

        Medicationreminder SULFAPYRIDINE = new Medicationreminder();
        SULFAPYRIDINE.setMedication("SULFAPYRIDINE");
        SULFAPYRIDINE.setDose("120 mg x1 Oral");
        SULFAPYRIDINE.setSchedule(new Schedule("undefined",
                "everyday",
                date,
                "test special instruction: ".concat(restUtilities.getRandomString())));
        SULFAPYRIDINE.setTimes("PRN");
        SULFAPYRIDINE.setTime("PRN");
        SULFAPYRIDINE.setWindow("30");
        medicationreminderList.add(SULFAPYRIDINE);

        Medicationreminder PERCORTEN = new Medicationreminder();
        PERCORTEN.setMedication("PERCORTEN");
        PERCORTEN.setDose("122 ml x1 Oral");
        PERCORTEN.setSchedule(new Schedule("undefined",
                "everyday",
                date,
                "test special instruction: ".concat(restUtilities.getRandomString())));
        PERCORTEN.setTimes("PRN");
        PERCORTEN.setTime("PRN");
        PERCORTEN.setWindow("30");
        medicationreminderList.add(PERCORTEN);

        Medicationreminder METANDREN = new Medicationreminder();
        METANDREN.setMedication("METANDREN");
        METANDREN.setDose("1 tblsp x2 Oral");
        METANDREN.setSchedule(new Schedule("undefined",
                "everyday",
                date,
                "test special instruction: ".concat(restUtilities.getRandomString())));
        METANDREN.setTimes("PRN");
        METANDREN.setTime("PRN");
        METANDREN.setWindow("30");
        medicationreminderList.add(METANDREN);

        Medicationreminder STILBETIN = new Medicationreminder();
        STILBETIN.setMedication("STILBETIN");
        STILBETIN.setDose("1 mg x1 Oral");
        STILBETIN.setSchedule(new Schedule("false", "everyday", "test special instruction: ".concat(restUtilities.getRandomString())));
        STILBETIN.setTimes("PRN");
        STILBETIN.setTime("PRN");
        STILBETIN.setWindow("30");
        medicationreminderList.add(STILBETIN);

        Medicationreminder everyXdaysReminder = new Medicationreminder();
        everyXdaysReminder.setMedication("LIQUAEMIN SODIUM");
        everyXdaysReminder.setDose("12 mg x1 Oral");
        Schedule scheduleEveryXdaysReminder = new Schedule();
        scheduleEveryXdaysReminder.setEssential("true");
        scheduleEveryXdaysReminder.setType("everyxdays");
        scheduleEveryXdaysReminder.setX(restUtilities.getRandomInt(1, 7).toString());
        scheduleEveryXdaysReminder.setStartday(String.valueOf(restUtilities.getCurrentTimeStamp()));
        scheduleEveryXdaysReminder.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleEveryXdaysReminder.setExpiration(date);
        everyXdaysReminder.setSchedule(scheduleEveryXdaysReminder);
        everyXdaysReminder.setTime("540");
        everyXdaysReminder.setTimes("9:00AM");
        everyXdaysReminder.setWindow("60");
        medicationreminderList.add(everyXdaysReminder);

        Medicationreminder customDaysRemider = new Medicationreminder();
        customDaysRemider.setMedication("ALCOHOL 10% AND DEXTROSE 5%");
        customDaysRemider.setDose("200 mg x2 Oral");
        Schedule scheduleCustomDaysRemider = new Schedule();
        scheduleCustomDaysRemider.setEssential("true");
        scheduleCustomDaysRemider.setType("custom");
        scheduleCustomDaysRemider.setSchedule(Modules.customMedicationsDays);
        scheduleCustomDaysRemider.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleCustomDaysRemider.setExpiration(date);
        customDaysRemider.setSchedule(scheduleCustomDaysRemider);
        customDaysRemider.setTime("540");
        customDaysRemider.setTimes("9:00AM");
        customDaysRemider.setWindow("60");
        medicationreminderList.add(customDaysRemider);

        info.setMedicationreminders(medicationreminderList);
        newMetrick.setModuleInfo(info);

        reqSpec.body(newMetrick);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
        if (response.statusCode() == 200) {
            System.out.println("Set metrics to patient: " + patientId);
        } else {
            System.out.println("Something wrong! Please check!");
        }
    }

    static void setRisksToPatientById(String patientId) {
        RestUtilities restUtilities = new RestUtilities();

        restUtilities.setBaseUri(Path.BASE_URI);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));
        ResponseSpecification resPec = restUtilities.getResponseSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.RISK));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        String bodyReq = "{\n" +
                "    \"settings\": {\n" +
                "        \"riskdata\": [\n" +
                "            {\n" +
                "                \"type\": \"activity_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastolicgreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 100\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_heartrategreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 150\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"glucose_bloodsugargreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 120\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"medication_missedessential\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"pulseox_heartrategreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": 80\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"pulseox_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"survey_checkanswers\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"survey_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"temperature_temperaturegreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 100\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"survey_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_decreasexlbsfromstart\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 120\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_decreasexlbsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 10\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_decreasexlbsfromstart\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 90\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastoliclessthan\",\n" +
                "                \"alerttype\": \"highrisk\",\n" +
                "                \"value\": \"40\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_systolicgreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": \"200\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"pulseox_spo2lessthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": \"90\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastolicgreaterthan\",\n" +
                "                \"alerttype\": \"highrisk\",\n" +
                "                \"value\": \"160\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastolicgreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": \"140\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}";

        reqSpec.body(bodyReq);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
        if (response.statusCode() == 200) {
            System.out.println("Set risks to patient: " + patientId);
        } else {
            System.out.println("Something wrong! Please check!");
        }
    }

    static Metrics getPatientMetricsData(String patientId) {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        restUtilities.setBaseUri(Path.BASE_URI);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        ResponseSpecification resPec = restUtilities.getResponseSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, String> date = restUtilities.getTimePeriod(10);
        restUtilities.createQueryParam(reqSpec, "start", date.get("current").replace("-", ""));
        restUtilities.createQueryParam(reqSpec, "end", date.get("current").replace("-", ""));

        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "get");


        return Arrays.asList(response.as(PatientMetricsData[].class)).get(0).getMetrics();
    }

    static String getGenerationMCByPatientId(String patientId) {
        MagiCodeIdentityPacient code = new MagiCodeIdentityPacient();
        Data data = new Data();
        data.setIdentity(patientId);
        code.setData(data);

        restUtilities = new RestUtilities();

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");
        reqSpec.header("Authorization", "Bearer "
                .concat(new PropertyWorker(Connection.AUTH_PROP).getProperty("clinician_intg_token")));
        resPec = restUtilities.getResponseSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.MAGIC_CODES);
        reqSpec.body(code);

        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath jsonPath = response.jsonPath();
        return jsonPath.get("data.code").toString();
    }

    static Response getConnectionMCToDevice(String deviceId) {
        MagiCodePacientDevice code = new MagiCodePacientDevice();
        Datum data = new Datum();
        data.setType("code");
        data.setCode(deviceId);
        code.setData(data);

        restUtilities = new RestUtilities();

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");
        reqSpec.header("Authorization", "Bearer "
                .concat(new PropertyWorker(Connection.AUTH_PROP).getProperty("clinician_intg_token")));
        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TOKENS_INTG);
        reqSpec.body(code);

        return restUtilities.getResponse(reqSpec, "post");

    }

    static void getTurnOffoDevice(String pacientHrsId) {
        List<DataDevices> listDevices = RestAssistant.getAllDevices(pacientHrsId).getData();
        String devId = listDevices.get(listDevices.size() - 1).getId();

        RestUtilities restUtil = new RestUtilities();

        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtil.getRequestSpecification();
        reqSpec.basePath("");

        resPec = restUtil.getResponseSpecification(restUtil.createResponseBuilder().
                expectContentType(ContentType.JSON)
                .expectStatusCode(204));

        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint(EndPoints.TOKENS_INTG.concat(EndPoints.DEVICE_ID));
        reqSpec.header("Authorization", "Bearer "
                .concat(new PropertyWorker(Connection.AUTH_PROP).getProperty("clinician_intg_token")));
        restUtil.getResponse(restUtil.createPathParam(reqSpec, "deviceId", devId), "delete");
    }

    static Devices getAllDevices(String patientId) {
        MagiCodeIdentityPacient code = new MagiCodeIdentityPacient();
        Data data = new Data();
        data.setIdentity(patientId);
        code.setData(data);

        restUtilities = new RestUtilities();

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");
        resPec = restUtilities.getResponseSpecification(restUtilities.createResponseBuilder().expectStatusCode(200));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TASKS_METAS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(
                restUtilities.createQueryParam(
                        reqSpec, "filter[identity]", patientId), "get");
        return response.as(Devices.class);

    }

    static com.hrs.cc.api.models.patients.profile.Profile getPatientById(String id) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
        Map<String, String> timePeriod = RandomData.getTimePeriod(20, formatter);
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);


        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.createPathParam(reqSpec, "id", id);
        restUtilities.createQueryParam(reqSpec, "start", timePeriod.get("minusDays"));
        restUtilities.createQueryParam(reqSpec, "end", timePeriod.get("current"));

        Response response = restUtilities.getResponse(reqSpec, "get");
        ArrayList profile = response.jsonPath().get("profile");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(profile.get(0), com.hrs.cc.api.models.patients.profile.Profile.class);
    }

    static void testSetClinicianInsecureReportsSetting(Boolean status) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.CLINICIAN));

        resPec = restUtilities.getResponseSpecification();


        Clinician clinician = new Clinician();
        clinician.setClinicianInsecureReports(status);


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.SETTINGS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(clinician);
//        reqSpec.log().all();
        Response response = restUtilities.getResponse(reqSpec, "post");
//        response.getBody().prettyPrint();
    }

    static List<DatumTask> getOneTimeQuestions() {
        PropertyWorker propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestUtilities newRestUtil = new RestUtilities();
        newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
        newReqSpec.basePath("");
        newRestUtil.setContentType(ContentType.JSON);
        newRestUtil.setEndPoint(EndPoints.TASKS_INTG);
        newRestUtil.removeHeaders(newReqSpec, "Authorization");
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("patient_device_token")));
        newRestUtil.getResponseSpecification();

        Response response = newRestUtil.getResponse(
                newRestUtil.createQueryParam(
                        newReqSpec,
                        "filter[patient]",
                        propertyWorker.getProperty("patient_hrs_id")), "get");
        PatientTasksApp tasksApp = response.as(PatientTasksApp.class);
        return tasksApp.getData().stream().filter(
                datum -> datum.getType().equals("survey") &&
                        datum.getAction().getOnetime().equals(true)).collect(Collectors.toList());
    }

    static void getUploadedImage(String patientId, String imageId) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.WI));
        restUtilities.removeHeaders(reqSpec, "Authorization");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        resPec = restUtilities.getResponseSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PREVIEW.concat(EndPoints.ID).concat(EndPoints.IMAGE_ID));

        restUtilities.createPathParam(reqSpec, "id", patientId);
        restUtilities.createPathParam(reqSpec, "imageId", imageId);

//https://apiv2.hrsalpha.com/v2/wi/preview/mNUySZH6bVuLjGGnP9Sj/139
//https://apiv2.hrsalpha.com/v2/wi/preview/mNUySZH6bVuLjGGnP9Sj/138
        Response response = restUtilities.getResponse(reqSpec, "get");
    }

    static void pausePatient(String patientId) {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2);

        resPec = restUtilities.getResponseSpecification();

        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("patient", patientId);
        queryParam.put("note", "reason for pausing ".concat(restUtilities.getRandomString()));
        queryParam.put("edvisit", "false");
        queryParam.put("readmitted", "false");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.PAUSE));
        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");

        JsonPath path = response.jsonPath();
        System.out.println("Patient has been paused:".concat(path.get("status")));

    }

    static void dischargePatient(String patientId) {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2);

        resPec = restUtilities.getResponseSpecification();

        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("patient", patientId);
        queryParam.put("note", "reason for pausing ".concat(restUtilities.getRandomString()));
        queryParam.put("edvisit", "false");
        queryParam.put("readmitted", "false");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.DISCHARGE));
        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");

        JsonPath path = response.jsonPath();
        System.out.println("Patient has been discharge:".concat(path.get("status")));

    }

    static String currentClinicianId() {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2);

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtilities.getResponse(reqSpec, "get");
        return response.as(Account.class).getUser();

    }

    static void removeDischargePatient(String patientId) {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2);

        resPec = restUtilities.getResponseSpecification();

        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("patient", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.DISCHARGE));
        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "delete");

        JsonPath path = response.jsonPath();
        System.out.println("remove discharge:".concat(path.get("status")));

    }

    static void getDeactivationPatient(String patientId) {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2);

        resPec = restUtilities.getResponseSpecification();

        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("patient", patientId);
        queryParam.put("edvisit", "false");
        queryParam.put("readmitted", "false");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.DEACTIVATE));
        reqSpec.body(queryParam);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");

        JsonPath path = response.jsonPath();
        System.out.println("remove discharge:".concat(path.get("status")));

    }

    static void activatedPatient(String id) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));
//        reqSpec.log().all();

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.REACTIVATE));
        restUtilities.createPathParam(reqSpec, "id", id);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");
        System.out.println("Activated user " + response.getStatusLine());
    }

    static String getAdminId() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2);
//        reqSpec.log().all();

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setEndPoint();
        restUtilities.setContentType(ContentType.JSON);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");

        return response.as(Account.class).getUser();
    }

    static String addClinician() {

        RestUtilities rest = new RestUtilities();
        ResponseSpecification resPec = rest.getResponseSpecification();
        rest.getResponseSpecification(rest.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        rest.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = rest.getRequestSpecification();
        reqSpec.basePath("");
//        reqSpec.log().all();


        rest.setContentType(ContentType.JSON);
        rest.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        clinician = new NewClinician();
        com.hrs.cc.api.models.requests.add_new_clinicain.Data data = new com.hrs.cc.api.models.requests.add_new_clinicain.Data();
        Profile profile = new Profile();

        profile.setRights("ReadOnly");
        ArrayList subgrp = new ArrayList();
        subgrp.add("west coast");
        profile.setSubgroups(subgrp);

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);

        Response response = rest.getResponse(reqSpec, "post");

        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        String clinicianToken = authToken.getData().getToken();
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(authToken.getData().getToken()));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);
        return account.getUser();
    }

    static String addClinician(String subgroup) {

        RestUtilities rest = new RestUtilities();
        ResponseSpecification resPec = rest.getResponseSpecification();
        rest.getResponseSpecification(rest.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        rest.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = rest.getRequestSpecification();
        reqSpec.basePath("");
//        reqSpec.log().all();


        rest.setContentType(ContentType.JSON);
        rest.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        clinician = new NewClinician();
        com.hrs.cc.api.models.requests.add_new_clinicain.Data data = new com.hrs.cc.api.models.requests.add_new_clinicain.Data();
        Profile profile = new Profile();

        profile.setRights("Clinician");
        ArrayList subgrp = new ArrayList();
        subgrp.add(subgroup);
        profile.setSubgroups(subgrp);

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);

        Response response = rest.getResponse(reqSpec, "post");

        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        String clinicianToken = authToken.getData().getToken();
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(authToken.getData().getToken()));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);
        return account.getUser();
    }

    static String addReadOnly(String subgroup) {

        RestUtilities rest = new RestUtilities();
        ResponseSpecification resPec = rest.getResponseSpecification();
        rest.getResponseSpecification(rest.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        rest.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = rest.getRequestSpecification();
        reqSpec.basePath("");
//        reqSpec.log().all();


        rest.setContentType(ContentType.JSON);
        rest.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        clinician = new NewClinician();
        com.hrs.cc.api.models.requests.add_new_clinicain.Data data = new com.hrs.cc.api.models.requests.add_new_clinicain.Data();
        Profile profile = new Profile();

        profile.setRights("ReadOnly");
        ArrayList subgrp = new ArrayList();
        subgrp.add(subgroup);
        profile.setSubgroups(subgrp);

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);

        Response response = rest.getResponse(reqSpec, "post");

        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        String clinicianToken = authToken.getData().getToken();
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(authToken.getData().getToken()));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);
        return account.getUser();
    }

    static String addClinician(String subgroup, String moderatorToken) {

        RestUtilities rest = new RestUtilities();
        ResponseSpecification resPec = rest.getResponseSpecification();
        rest.getResponseSpecification(rest.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        rest.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = rest.getRequestSpecification();
        reqSpec.basePath("");
//        reqSpec.log().all();


        rest.setContentType(ContentType.JSON);
        rest.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(moderatorToken));
        clinician = new NewClinician();
        com.hrs.cc.api.models.requests.add_new_clinicain.Data data = new com.hrs.cc.api.models.requests.add_new_clinicain.Data();
        Profile profile = new Profile();

        profile.setRights("Clinician");
        ArrayList subgrp = new ArrayList();
        subgrp.add(subgroup);
        profile.setSubgroups(subgrp);

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);

        Response response = rest.getResponse(reqSpec, "post");

        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        String clinicianToken = authToken.getData().getToken();
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(authToken.getData().getToken()));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);
        return account.getUser();
    }

    static String addClinician(ArrayList<String> subgroup) {

        RestUtilities rest = new RestUtilities();
        ResponseSpecification resPec = rest.getResponseSpecification();
        rest.getResponseSpecification(rest.getResponseBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200));
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        rest.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = rest.getRequestSpecification();
        reqSpec.basePath("");

        rest.setContentType(ContentType.JSON);
        rest.setEndPoint(EndPoints.USERS);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        clinician = new NewClinician();
        com.hrs.cc.api.models.requests.add_new_clinicain.Data data = new com.hrs.cc.api.models.requests.add_new_clinicain.Data();
        Profile profile = new Profile();

        profile.setRights("Clinician");
        profile.setSubgroups(subgroup);

        data.setUserName("Test clinician" + RandomData.getRandomString(10));
        data.setPassword(RandomData.getRandomPassword());
        data.setProfile(profile);
        data.setFirstName("Test first name " + RandomData.getRandomString(10));
        data.setMiddleName("Test middle name" + RandomData.getRandomString(10));
        data.setLastName("Test last name" + RandomData.getRandomString(10));
        data.setEmail(RandomData.getRandomEmail());
        data.setPhone(RandomData.getRandomInt(13).toString());
        data.setEnvironment("QATestingZone");
        data.setType("clinician");
        clinician.setData(data);

        reqSpec.body(clinician);

        Response response = rest.getResponse(reqSpec, "post");

        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + clinician.getData().getUserName() + "\"" + ",\n" +
                "        \"password\": " + "\"" + clinician.getData().getPassword() + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        String clinicianToken = authToken.getData().getToken();
        RestUtilities accRestUtil = new RestUtilities();

        accRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification rqSpec = accRestUtil.getRequestSpecification();

        rqSpec.basePath(Path.API_V2);
        ResponseSpecification resSpecificationAcc = accRestUtil.getResponseSpecification();
        accRestUtil.setContentType(ContentType.JSON);
        accRestUtil.setEndPoint();
        rqSpec.header("Authorization", "Bearer ".concat(authToken.getData().getToken()));
        Response accauntData = accRestUtil.getResponse(rqSpec, "get");
        Account account = accauntData.as(Account.class);
        return account.getUser();
    }

    static void activateIRVSchedule(String patientId) {

        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);


        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, Object> body = new HashMap<>();
        Map<String, String> data = new HashMap<>();
        data.put("status", "active");
        body.put("data", data);
        reqSpec.body(body);
        restUtilities.getResponse(reqSpec, "patch");
    }

    static void turnOnPatientVoiceInEnv() {
        if (!getCurrentSettingsInEnv()) {
            RestUtilities restUtilities = new RestUtilities();
            Response responseAuth = Connection.adminAuthGeneral();
            Map<String, String> cookies = responseAuth.getCookies();

            restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
            restUtilities.setContentType(ContentType.JSON);
            reqSpec = restUtilities.getRequestSpecification();
//            reqSpec.log().all();
            resPec = restUtilities.getResponseSpecification();
            reqSpec.basePath(Path.API_ADMIN_V2);

            Map<String, Object> data = new HashMap<>();
            data.put("request", "addenvironmentsetting");
            data.put("environment", "QATestingZone");
            data.put("setting", EnvFlags.SYSTEM_PATIENTCONNECTVOICE.get());

            Map<String, String> setting = new HashMap<>();
            setting.put("phone", "19382009930");
            setting.put("pcVoiceEnv", "Test BOT");
            setting.put("pcVoiceAllPatients", "true");
            data.put("value", setting);

            restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
            reqSpec.header("authorization", AuthScheme.basicScheme("admin", "LaCroix").generateAuthToken());
            reqSpec.cookies(cookies);
            restUtilities.setEndPoint(EndPoints.DATA);
            reqSpec.formParams(data);

            restUtilities.getResponseSpecification(
                    restUtilities.createResponseBuilder()
                            .expectStatusCode(200)
                    /*.registerParser("text/html", Parser.JSON)*/);

            Response response = restUtilities.getResponse(reqSpec, "post");
            System.out.println(response.statusCode());
        }
    }

    static Boolean getCurrentSettingsInEnv() {
        ObjectMapper mapper = new ObjectMapper();
        RestUtilities restUtilities = new RestUtilities();
        Response responseAuth = Connection.adminAuthGeneral();
        Map<String, String> cookies = responseAuth.getCookies();

        restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.API_ADMIN_V2);

        Map<String, Object> data = new HashMap<>();
        data.put("request", "getenvironmentsettings");
        data.put("environment", "QATestingZone");

        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        reqSpec.header("authorization", AuthScheme.basicScheme("admin", "LaCroix").generateAuthToken());
        reqSpec.cookies(cookies);
        restUtilities.setEndPoint(EndPoints.DATA);
        reqSpec.formParams(data);

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200)
                /*.registerParser("text/html", Parser.JSON)*/);

        Response response = restUtilities.getResponse(reqSpec, "post");
        settings = null;
        try {
            settings = mapper.readValue(response.getBody().asString(), EnvSettings.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            settings.getSettings().stream().filter(setting ->
                    setting.getFlag().equals(EnvFlags.SYSTEM_PATIENTCONNECTVOICE.get())).findFirst().get();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    static IvrQuestionsList getEnabledIrvQuestions() {
        RestUtilities restUtilities = new RestUtilities();
        PropertyWorker propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");
//        reqSpec.log().all();

        ResponseSpecification resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_QUESTIONS);
        restUtilities.createQueryParam(reqSpec, "filter[env]", "QATestingZone");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
        return response.as(IvrQuestionsList.class);
    }

    static UsersByTypeInEnv getEnabledClinicians() {
        RestUtilities restUtilities = new RestUtilities();
        PropertyWorker propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");
//        reqSpec.log().all();

        ResponseSpecification resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS);
        restUtilities.createQueryParam(reqSpec, "filter[type]", "clinician");
        restUtilities.createQueryParam(reqSpec, "filter[environment]", "QATestingZone");
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");

        return response.as(UsersByTypeInEnv.class);
    }


    static void setSubgroupToModerator(List<String> groups) {
        String id = getAdminId();
        RestUtilities restUtil = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtil.setBaseUri(Path.BASE_URI);

        RequestSpecification reqSpec = restUtil.getRequestSpecification();
        reqSpec.basePath("");

        restUtil.getResponseSpecification(
                restUtil.createResponseBuilder()
                        .expectStatusCode(204));

        Map<String, Object> body = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        Map<String, List<String>> profile = new HashMap<>();
        profile.put("subgroups", groups);
        data.put("profile", profile);
        body.put("data", data);

        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        restUtil.createPathParam(reqSpec, "id", id);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        reqSpec.body(body);
//        reqSpec.log().all();
        Response response = restUtil.getResponse(reqSpec, "patch");
//        response.getBody().prettyPrint();
    }

    static void removeClinician(String clinicianId) {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);

        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");

        ResponseSpecification resPec = restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(204));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);

        Response response = restUtilities.getResponse(reqSpec, "delete");

    }

    static void removeClinician(String clinicianId, String moderatorToken) {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);

        RequestSpecification reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");

        ResponseSpecification resPec = restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(204));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.USERS.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(moderatorToken));
        restUtilities.createPathParam(reqSpec, "id", clinicianId);

        Response response = restUtilities.getResponse(reqSpec, "delete");

    }

    static String authModeratorWest(String login, String pass) {
        RestUtilities restUtil = new RestUtilities();
        String dataU = "{\n" +
                "    \"data\": {\n" +
                "        \"username\": " + "\"" + login + "\"" + ",\n" +
                "        \"password\": " + "\"" + pass + "\"" + ",\n" +
                "        \"type\":" + "\"" + "credentials" + "\"" + "\n" +
                "    }\n" +
                "}";
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.TOKENS_INTEGRATION);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint();
        reqSpecification.body(dataU);
        Response re = restUtil.getResponse(reqSpecification, "post");
        ClinicianAuthToken authToken = re.as(ClinicianAuthToken.class);

        return authToken.getData().getToken();
    }

    static void setByodLicenseMax(Integer max) {
        RestUtilities restUtilities = new RestUtilities();
        Response responseAuth = Connection.adminAuthGeneral();
        Map<String, String> cookies = responseAuth.getCookies();

        restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
//            reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.API_ADMIN_V2);

        Map<String, Object> data = new HashMap<>();
        data.put("request", "addenvironmentsetting");
        data.put("environment", "QATestingZone");
        data.put("setting", EnvFlags.SYSTEM_BYOD_LICENSE_MAX.get());
        data.put("value", max);

        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        reqSpec.header("authorization", AuthScheme.basicScheme("admin", "LaCroix").generateAuthToken());
        reqSpec.cookies(cookies);
        restUtilities.setEndPoint(EndPoints.DATA);
        reqSpec.formParams(data);

        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200)
                /*.registerParser("text/html", Parser.JSON)*/);

        Response response = restUtilities.getResponse(reqSpec, "post");

    }

    static void removeEnvSettings(String name) {
        getCurrentSettingsInEnv();
        List<Setting> idFlag = settings.getSettings().stream().filter(setting -> setting.getFlag().equals(name)).collect(Collectors.toList());

        for (Setting setting : idFlag) {
            RestUtilities restUtilities = new RestUtilities();
            Response responseAuth = Connection.adminAuthGeneral();
            Map<String, String> cookies = responseAuth.getCookies();

            restUtilities.setBaseUri(Path.BASE_ADMIN_URI_APP_INTEGRATION);
            restUtilities.setContentType(ContentType.JSON);
            reqSpec = restUtilities.getRequestSpecification();
//            reqSpec.log().all();
            resPec = restUtilities.getResponseSpecification();
            reqSpec.basePath(Path.API_ADMIN_V2);

            Map<String, Object> data = new HashMap<>();
            data.put("request", "removeenvironmentsetting");
            data.put("environment", "QATestingZone");
            data.put("id", setting.getId());

            restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
            reqSpec.header("authorization", AuthScheme.basicScheme("admin", "LaCroix").generateAuthToken());
            reqSpec.cookies(cookies);
            restUtilities.setEndPoint(EndPoints.DATA);
            reqSpec.formParams(data);

            restUtilities.getResponseSpecification(
                    restUtilities.createResponseBuilder()
                            .expectStatusCode(200)
                    /*.registerParser("text/html", Parser.JSON)*/);

            Response response = restUtilities.getResponse(reqSpec, "post");
        }
    }

    static Education educationsContentForPatient(long limit, boolean status, String patientId) {
        /*get list of the education files*/
        PropertyWorker propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestUtilities restUtil = new RestUtilities();
        restUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification reqSpecification = restUtil.getRequestSpecification();
        reqSpecification.basePath(Path.API_V2);
        ResponseSpecification resSpecification = restUtil.getResponseSpecification();
        restUtil.setContentType(ContentType.JSON);

        restUtil.setEndPoint(EndPoints.EDUCATION);
        reqSpecification.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtil.getResponse(reqSpecification, "get");
        Education educations = response.as(Education.class);

        limit = educations.getData().getPatientAllContent().size();

        /*turn on some educations to patient*/
        List<SetPatientContent> toEnableEducation = null;
        if (status) {
            toEnableEducation = educations.getData().getPatientAllContent().stream().limit(limit).map(c -> new SetPatientContent(
                    c.getId(),
                    c.getType(),
                    c.getCategory(),
                    c.getTitle(),
                    true
            )).collect(Collectors.toList());
        } else {
            toEnableEducation = new ArrayList<>();
        }

        Map<String, Object> body = new HashMap<>();
        body.put("content", toEnableEducation);


        restUtil.setContentType(ContentType.JSON);
        restUtil.setEndPoint(EndPoints.PATIENTS.concat(EndPoints.ID).concat(EndPoints.EDUCATION));
        reqSpecification.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpecification.body(body);
        restUtil.removeHeaders(reqSpecification, "Authorization");
        reqSpecification.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response turnOn = restUtil.getResponse(
                restUtil.createPathParam(reqSpecification, "id", patientId), "post");

        return educations;
    }

    static void setTimeZone(String patientId, String timezone) {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENT));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.PROFILE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, String> body = new HashMap<>();
        body.put("time_zone", timezone);
        reqSpec.body(body);

        restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
    }

    static Survey questionsDetails(String patientId) {
        RestUtilities restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.SYRVEY_DETAILS).concat(EndPoints.DATE));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createPathParam(reqSpec, "date", restUtilities.getTimePeriod(2).get("current"));
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "get");

        return response.as(Survey.class);
    }

    static List<InventoryResponse> getDeviceId() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.INVENTORY));

        resPec = restUtilities.getResponseSpecification();


        restUtilities.setContentType(ContentType.JSON);

        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response devices = restUtilities.getResponse(reqSpec, "get");
        List<InventoryResponse> inventoryList = Arrays.asList(devices.as(InventoryResponse[].class));
        return inventoryList.stream().filter(i ->
                !i.getName().equals("HRSTAB10498") &&
                        !i.getId().equals("HRSTAB10498")).collect(Collectors.toList());


    }

}

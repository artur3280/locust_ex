package com.hrs.cc.api.tests;

import com.hrs.cc.api.constans.Path;
import core.rest.configs.Configurations;
import core.rest.loggers.Logger;
import core.rest.loggers.SlackAPI;
import core.rest.loggers.testrail.CaseStatus;
import core.rest.loggers.testrail.TestRailApi;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Configurator {

    static void sendDataToTR(Boolean turnOn, TestNG testNG, Logger logger, Boolean close) throws IOException {
        if (turnOn) {
            logger.testRailApi = new TestRailApi(Configurations.getConfigParam("appName"),
                    Configurations.getConfigParam("suiteName"));

            logger.testRailApi.addNewTestCases(logger.getTestContexts().get(0).getSuite());
            logger.testRailApi.createRun("Regression automation Api tests for " + Path.BASE_URI +": ", "Api", Path.BASE_URI);

            logger.testRailApi.runCases(logger.getPassedTests(), CaseStatus.PASSED,"PASSED", Logger.getRecSpecMap());
            logger.testRailApi.runCases(logger.getFailedTests(), CaseStatus.FAILED,"FAILED", Logger.getRecSpecMap());
            logger.testRailApi.runCases(logger.getSkippedTests(), CaseStatus.BLOCKED,"SKIPPED", Logger.getRecSpecMap());
            if (close) {
                logger.testRailApi.closeRun();
            }
        }

    }

    static void sendDataToSlack(Boolean turnOn, TestNG testNG, Logger logger) throws IOException {
        if (turnOn) {
            SlackAPI slackAPI = new SlackAPI(testNG,logger);
            slackAPI.send();
        }
    }

    static List<XmlSuite> getSuits() throws IOException {
        List<XmlSuite> suiteList = (List <XmlSuite>)(new Parser(getFile()).parse());
        suiteList.forEach((key)->{
            key.setVerbose(1);
            key.setParallel(XmlSuite.ParallelMode.CLASSES);
            key.setThreadCount(11);
        });
        return suiteList;
    }

    private static InputStream getFile() {
        return Runner.class.getClassLoader().getResourceAsStream("tests_configs/hrs_regression_suit.xml");
    }

}

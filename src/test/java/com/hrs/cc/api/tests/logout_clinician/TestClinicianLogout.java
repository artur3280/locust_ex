package com.hrs.cc.api.tests.logout_clinician;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import core.PropertyWorker;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Map;

public class TestClinicianLogout extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private static Map<String, String> userData;
    private PropertyWorker propertyWorker;
    private static String tokenId;


    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath("");
        Logger.debugging(false);
        resPec = restUtilities.getResponseSpecification();
        tokenId = RestAssistant.moderatorData().getToken();
    }

    @Test
    public void testClinicianLogout(){
        /*Remove token*/
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.TOKENS_INTG.concat(EndPoints.ID));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        reqSpec.pathParam("id", tokenId);
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(204)
        );

        Response response = restUtilities.getResponse(reqSpec, "delete");
        assertEquals(response.statusCode(),204);


        /*Checking token*/
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(401)
        );

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .setBaseUri(Path.BASE_URI)
                        .setBasePath(Path.API_V2)
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token"))));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        Response res = restUtilities.getResponse(newReqSpec, "get");
        assertEquals(res.statusCode(),401);

    }



}

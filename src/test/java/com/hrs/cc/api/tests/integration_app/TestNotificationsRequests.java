package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.EnvFlags;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.account_data.Account;
import com.hrs.cc.api.models.integratioins_app.clinician_auth_token.ClinicianAuthToken;
import com.hrs.cc.api.models.integratioins_app.notifications.ClinicianNotifications;
import com.hrs.cc.api.models.integratioins_app.notifications.Notification;
import com.hrs.cc.api.models.requests.add_new_clinicain.Data;
import com.hrs.cc.api.models.requests.add_new_clinicain.NewClinician;
import com.hrs.cc.api.models.requests.add_new_clinicain.Profile;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TestNotificationsRequests extends Assert {
    private ObjectMapper mapper = new ObjectMapper();
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private PropertyWorker propertyWorker;
    private String patientHrsId;
    private static String patientDeviceToken;
    private Integer videoCallId;
    private String notificationId;
    private String clinicianId;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        patientHrsId = RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG);
        clinicianId = RestAssistant.currentClinicianId();
        String device_code = RestAssistant.getGenerationMCByPatientId(patientHrsId);
        JsonPath path = RestAssistant.getConnectionMCToDevice(device_code).jsonPath();
        patientDeviceToken = path.get("data.token");

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.NOTIFICATIONS);
    }

    @AfterClass
    public void removeDevice() {
        RestAssistant.getTurnOffoDevice(patientHrsId);
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
    }

    @Test
    public void testMakeVideoCall() {
        RestUtilities newRestUtil = new RestUtilities();
        newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
        newReqSpec.basePath(Path.VIDEO_CALL);
        newRestUtil.setContentType(ContentType.JSON);
        newRestUtil.setEndPoint("");
        newReqSpec.header("Authorization", "Bearer ".concat(patientDeviceToken));
        newRestUtil.getResponseSpecification();

        Map<String, Object> body = new HashMap<>();
        Map<Object, Object> data = new HashMap<>();

        ArrayList<Map<String, String>> list = new ArrayList<>();
        Map<String, String> initiator = new HashMap<>();
        initiator.put("hrsid", patientHrsId);
        initiator.put("role", "initiator");
        list.add(initiator);

        Map<String, String> participant = new HashMap<>();
        participant.put("hrsid", clinicianId);
        participant.put("role", "participant");
        list.add(participant);

        data.put("participants", list);
        body.put("data", data);

        newReqSpec.body(body);


        Response response = newRestUtil.getResponse(newReqSpec, "post");

        JsonPath path = response.jsonPath();

        videoCallId = path.get("data.id");
        assertNotNull(path.get("data.id"), "data.id field is null");
        assertEquals(path.get("data.status"), "waiting", " status field is not eql");
        assertNotNull(path.get("data.createdAt"), "data.createdAt field is null");
        assertNotNull(path.get("data.updatedAt"), "data.updatedAt field is null");
    }

    @Test(dependsOnMethods = {"testMakeVideoCall"})
    public void testGetTokenVideoCall() {
        RestUtilities newRestUtil = new RestUtilities();
        newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
        newReqSpec.basePath(Path.VIDEO_TOKENS_CALL);
        newRestUtil.setContentType(ContentType.JSON);
        newRestUtil.setEndPoint("");
        newReqSpec.header("Authorization", "Bearer ".concat(patientDeviceToken));
        newRestUtil.getResponseSpecification();

        Map<String, Object> body = new HashMap<>();
        Map<Object, Object> data = new HashMap<>();
        data.put("callId", videoCallId);
        Map<String, String> participant = new HashMap<>();
        participant.put("hrsid", patientHrsId);
        data.put("participant", participant);
        newReqSpec.body(body);

        body.put("data", data);
        newReqSpec.body(body);

        Response response = newRestUtil.getResponse(newReqSpec, "post");

        JsonPath path = response.jsonPath();

        assertNotNull(path.get("data.id"), "data.id field is null");
        assertEquals(path.get("data.callId"), videoCallId, "field not eql");
        assertNotNull(path.get("data.value"), "data.value field is null");
        assertNotNull(path.get("data.sessionId"), "data.sessionId field is null");
        assertNotNull(path.get("data.projectKey"), "data.projectKey field is null");

    }

    @Test(dependsOnMethods = {"testMakeVideoCall"})
    public void testGetTokenVideoCallsById() {
        RestUtilities newRestUtil = new RestUtilities();
        newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
        newReqSpec.basePath(Path.VIDEO_CALL);
        newRestUtil.setContentType(ContentType.JSON);
        newRestUtil.setEndPoint(EndPoints.ID);
        newReqSpec.pathParam("id", videoCallId);
        newReqSpec.header("Authorization", "Bearer ".concat(patientDeviceToken));
        newRestUtil.getResponseSpecification();

        Response response = newRestUtil.getResponse(newReqSpec, "get");

        JsonPath path = response.jsonPath();

        assertNotNull(path.get("data.id"), "data.id field is null");
        assertEquals(path.get("data.id"), videoCallId, "field not eql");
        assertNotNull(path.get("data.status"), "data.status field is null");
        assertNotNull(path.get("data.createdAt"), "data.createdAt field is null");
        assertNotNull(path.get("data.updatedAt"), "data.updatedAt field is null");

    }

    @Test
    public void testMakeVoiceCall() {
        RestUtilities newRestUtil = new RestUtilities();
        newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
        newReqSpec.basePath(Path.API_V2);
        newRestUtil.setContentType(ContentType.JSON);
        newRestUtil.setEndPoint(EndPoints.VOICE.concat(EndPoints.START));
        newReqSpec.header("Authorization", "Bearer ".concat(patientDeviceToken));
        newRestUtil.getResponseSpecification();

        newReqSpec.queryParam("type", "clinician");

        Response response = newRestUtil.getResponse(newReqSpec, "get");

        JsonPath path = response.jsonPath();

        assertNotNull(path.get("callid"), "callid field is null");
        assertEquals(path.get("calltype"), "conference", "field is no eql");
        assertNotNull(path.get("num"), "num field is null");
        assertNotNull(path.get("tid"), "tid field is null");
        assertNotNull(path.get("token"), "token field is null");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"testMakeVideoCall", "testGetTokenVideoCall", "testMakeVoiceCall"})
    public void testGetNotifications() {
        RestUtilities newRestUtil = new RestUtilities();
        newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
        newReqSpec.basePath(Path.API_V2);
        newRestUtil.setContentType(ContentType.JSON);
        newRestUtil.setEndPoint(EndPoints.NOTIFICATIONS);
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        newRestUtil.getResponseSpecification();

        newReqSpec.queryParam("filter[to]", patientHrsId);
        Response response = newRestUtil.getResponse(newReqSpec, "get");

        ClinicianNotifications notifications = response.as(ClinicianNotifications.class);

        notifications.getNotifications().forEach(notification -> {
            assertNotNull(notification.getPatient(), "Patient field is null");
            assertNotNull(notification.getPatient().getFirstName(), "Patient.FirstName field is null");
            assertNotNull(notification.getPatient().getLastName(), "Patient.LastName field is null");
            assertNotNull(notification.getPatient().getEnvironment(), "Patient.Environment field is null");
            assertNotNull(notification.getPatient().getMiddleName(), "Patient.MiddleName field is null");
            assertNotNull(notification.getPatient().getHrsId(), "Patient.HrsId field is null");
            assertNotNull(notification.getPatient().getId(), "Patient.Id field is null");

            assertNotNull(notification.getEpoch(), "Epoch field is null");
            assertNotNull(notification.getId(), "Id field is null");
            assertNotNull(notification.getType(), "Type field is null");
            assertNotNull(notification.getStatus(), "Status field is null");
        });

        notificationId = notifications.getNotifications().stream().findFirst().get().getId();
        if(notificationId.isEmpty()) throw new NullPointerException("notification id is null");
    }

    @Test(dependsOnMethods = {"testMakeVideoCall", "testGetTokenVideoCall", "testMakeVoiceCall", "testGetNotifications"})
    public void testGetNotificationById() {
        RestUtilities newRestUtil = new RestUtilities();
        newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
        newReqSpec.basePath("");
        newRestUtil.setContentType(ContentType.JSON);
        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));

        newRestUtil.setEndPoint(EndPoints.NOTIFICATIONS.concat(EndPoints.ID));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        newRestUtil.getResponseSpecification();

        newReqSpec.pathParam("id", notificationId);
        Map<String, Object> data = new HashMap<>();
        data.put("acknowleded", "1");

        newReqSpec.formParams(data);

        Response response = newRestUtil.getResponse(newReqSpec, "patch");

    }

    @Test(dependsOnMethods = {"testMakeVideoCall", "testGetTokenVideoCall", "testMakeVoiceCall", "testGetNotifications"})
    public void testAnsweredNotifications() {
        RestUtilities newRestUtil = new RestUtilities();
        newRestUtil.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        RequestSpecification newReqSpec = newRestUtil.getRequestSpecification();
        newReqSpec.basePath(Path.API_V1);
        newRestUtil.setContentType(ContentType.JSON);
        newRestUtil.setEndPoint(EndPoints.NOTIFICATIONS.concat(EndPoints.ID));
        newReqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        newRestUtil.getResponseSpecification();

        newReqSpec.pathParam("id", notificationId);
        Response response = newRestUtil.getResponse(newReqSpec, "patch");

        ClinicianNotifications notifications = response.as(ClinicianNotifications.class);


    }
}
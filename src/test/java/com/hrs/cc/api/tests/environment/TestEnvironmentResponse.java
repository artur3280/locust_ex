package com.hrs.cc.api.tests.environment;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.EnvFlags;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.environment.Environment;
import com.hrs.cc.api.models.environment.data.Condition;
import com.hrs.cc.api.models.environment.data.EnvironmentSetting;
import com.hrs.cc.api.models.environment.data.Metric;
import com.hrs.cc.api.models.environment.data.PatientInfoCustomAttributes;
import com.hrs.cc.api.models.environment.data.environment_settings_type.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.models.patients.education.Education;
import core.PropertyWorker;
import core.rest.RestUtilities;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

public class TestEnvironmentResponse extends Assert {
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private ObjectMapper mapper = new ObjectMapper();
    private static Environment environment;
    private static Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private PropertyWorker propertyWorker;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.ENVIRONMENT));
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
    }


    @BeforeMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
        reqSpec.body("");
    }

    @Test
    public void testEnvironmentResponse() {
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtilities.getResponse(reqSpec, "get");
        environment = response.as(Environment.class);

        assertNotNull(environment.getSettings(), "getSettings field is null");
        assertNotNull(environment.getSettings().getSubgroups(), "getSettings().getSubgroups field is null");
        assertFalse(environment.getSettings().getSubgroups().isEmpty(), "Subgroups field is empty");
        assertNotNull(environment.getEnvironmentSettings(), "getEnvironmentSettings field is null");
        assertFalse(environment.getEnvironmentSettings().get(0).getFlag().isEmpty(), "EnvironmentSettings field is empty");
        assertNotNull(environment.getEnvironmentSettings().get(0).getValue(), "getEnvironmentSettings().get(0).getValue field is null");
        assertNotNull(environment.getMetrics(), "getMetrics field is null");
        assertNotNull(environment.getConditions(), "getConditions field is null");
        assertNotNull(environment.getPatientInfoCustomAttributes(), "getPatientInfoCustomAttributes field is null");
    }

    @Test(dependsOnMethods = {"testEnvironmentResponse"})
    public void testEnvironmentSettings() {

        EnvironmentSetting activitySetting = environment.getEnvironmentSettings()
                .stream()
                .filter(setting -> setting.getFlag().equals(EnvFlags.METRIC_MULTIPARTACTIVITY.get()))
                .findFirst()
                .get();

        Activities activityTypes = mapper.convertValue(activitySetting.getValue(), Activities.class);
        assertTrue(activityTypes.getActivity().stream()
                .allMatch(activity -> activity.getType() != null));

        assertTrue(activityTypes.getActivity().stream()
                .anyMatch(activity -> activity.getType().equals("exercise") || activity.getType().equals("getexercise")));


        try {
            EnvironmentSetting temperatureSetting = environment.getEnvironmentSettings()
                    .stream()
                    .filter(setting -> setting.getFlag().equals(EnvFlags.METRIC_TEMPERATURE.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) temperatureSetting.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.METRIC_TEMPERATURE.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting METRIC_TEMPERATURE = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.METRIC_TEMPERATURE.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) METRIC_TEMPERATURE.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.METRIC_TEMPERATURE.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting METRIC_WOUNDIMAGING = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.METRIC_WOUNDIMAGING.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) METRIC_WOUNDIMAGING.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.METRIC_WOUNDIMAGING.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting METRIC_TIMESPENTMONITORING = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.METRIC_TIMESPENTMONITORING.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) METRIC_TIMESPENTMONITORING.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.METRIC_TIMESPENTMONITORING.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting RESOURCE_CONSENTFORM = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.RESOURCE_CONSENTFORM.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) RESOURCE_CONSENTFORM.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.RESOURCE_CONSENTFORM.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting EDUCATION_DIETINFO = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.EDUCATION_DIETINFO.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) EDUCATION_DIETINFO.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.EDUCATION_DIETINFO.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_ADVANCEDNOTE = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_ADVANCEDNOTE.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_ADVANCEDNOTE.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_ADVANCEDNOTE.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_ADVANCEDSETTINGS = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_ADVANCEDSETTINGS.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_ADVANCEDSETTINGS.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_ADVANCEDSETTINGS.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_SECUREWEBMESSAGE = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_SECUREWEBMESSAGE.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_SECUREWEBMESSAGE.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_SECUREWEBMESSAGE.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_ACTIVECACHING = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_ACTIVECACHING.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_ACTIVECACHING.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_ACTIVECACHING.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_REPORTS = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_REPORTS.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_REPORTS.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_REPORTS.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_TELEHEALTHREPORTS = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_TELEHEALTHREPORTS.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_TELEHEALTHREPORTS.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_TELEHEALTHREPORTS.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_MESSAGING = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_MESSAGING.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_MESSAGING.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_MESSAGING.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_ALWAYSONBLUETOOTH = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_ALWAYSONBLUETOOTH.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_ALWAYSONBLUETOOTH.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_ALWAYSONBLUETOOTH.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_QUICKNOTE = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_QUICKNOTE.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_QUICKNOTE.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_QUICKNOTE.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_ADJUSTABLERISK = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_ADJUSTABLERISK.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_ADJUSTABLERISK.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_ADJUSTABLERISK.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_ADJUSTABLERISKALERTS = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_ADJUSTABLERISKALERTS.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_ADJUSTABLERISKALERTS.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_ADJUSTABLERISKALERTS.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting PATIENTINFO_CUSTOMATTRIBUTES = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.PATIENTINFO_CUSTOMATTRIBUTES.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) PATIENTINFO_CUSTOMATTRIBUTES.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.PATIENTINFO_CUSTOMATTRIBUTES.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting EMR_SENDIMAGE = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.EMR_SENDIMAGE.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) EMR_SENDIMAGE.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.EMR_SENDIMAGE.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_BYOD = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_BYOD.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_BYOD.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_BYOD.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_MULTIPLEREADINGS = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_MULTIPLEREADINGS.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_MULTIPLEREADINGS.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_MULTIPLEREADINGS.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_AUTOASSIGNCLINICIAN = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_AUTOASSIGNCLINICIAN.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_AUTOASSIGNCLINICIAN.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_AUTOASSIGNCLINICIAN.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_INSECUREREPORTS = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_INSECUREREPORTS.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_INSECUREREPORTS.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_INSECUREREPORTS.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_MANAGEMENT = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_MANAGEMENT.get()))
                    .findFirst()
                    .get();
            assertTrue((Boolean) SYSTEM_MANAGEMENT.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_MANAGEMENT.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting EDUCATION_CUSTOM = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.EDUCATION_CUSTOM.get()))
                    .findFirst()
                    .get();
            assertTrue(EDUCATION_CUSTOM.toString().contains("Custom Education"));
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.EDUCATION_CUSTOM.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_SUBGROUPS = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_SUBGROUPS.get()))
                    .findFirst()
                    .get();
            assertTrue(SYSTEM_SUBGROUPS.toString().contains("east coast"));
            assertTrue(SYSTEM_SUBGROUPS.toString().contains("test subgroup"));
            assertTrue(SYSTEM_SUBGROUPS.toString().contains("west coast"));
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_SUBGROUPS.get()).concat(" is turn off."));
        }

        try {
            List<EnvironmentSetting<Object>> PRESET_CUSTOM = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.PRESET_CUSTOM.get()))
                    .collect(Collectors.toList());

            assertTrue(PRESET_CUSTOM.stream().anyMatch(p ->
                    p.toString().contains("activity") ||
                            p.toString().contains("weight") ||
                            p.toString().contains("temperature")));
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.PRESET_CUSTOM.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_RISKDATA = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_RISKDATA.get())).findFirst().get();
            Riskdatum riskdatum = mapper.convertValue(SYSTEM_RISKDATA.getValue(), Riskdatum.class);
            assertFalse(riskdatum.getRiskdata().isEmpty());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_RISKDATA.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_DATABASEVERSION = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_DATABASEVERSION.get())).findFirst().get();
            assertNotNull(SYSTEM_DATABASEVERSION.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_DATABASEVERSION.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_TWILIOINFO = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_TWILIOINFO.get())).findFirst().get();
            assertNotNull(SYSTEM_TWILIOINFO.getValue());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_TWILIOINFO.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_ENVIRONMENT_TZ = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_ENVIRONMENT_TZ.get())).findFirst().get();
            assertTrue(SYSTEM_ENVIRONMENT_TZ.getValue().toString().contains("America/New_York (EST)"));
            assertTrue(SYSTEM_ENVIRONMENT_TZ.getValue().toString().contains("America/New_York"));
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_ENVIRONMENT_TZ.get()).concat(" is turn off."));
        }

        try {
            EnvironmentSetting SYSTEM_ENVIRONMENTLOGO = environment.getEnvironmentSettings()
                    .stream()
                    .filter(data -> data.getFlag().equals(EnvFlags.SYSTEM_ENVIRONMENTLOGO.get())).findFirst().get();
            assertFalse(SYSTEM_ENVIRONMENTLOGO.toString().isEmpty());
        } catch (NoSuchElementException e) {
            System.out.println("The environment setting ".concat(EnvFlags.SYSTEM_ENVIRONMENTLOGO.get()).concat(" is turn off."));
        }


    }

    @Test(dependsOnMethods = {"testEnvironmentResponse"})
    public void testEnvironmentMetrics() {
        List<Metric> environmentSetting = environment.getMetrics();

        assertTrue(environmentSetting.toString().contains("duration"), "duration settings is not contains");
        assertTrue(environmentSetting.toString().contains("systolic"), "systolic settings is not contains");
        assertTrue(environmentSetting.toString().contains("heartrate"), "heartrate settings is not contains");
        assertTrue(environmentSetting.toString().contains("diastolic"), "diastolic settings is not contains");
        assertTrue(environmentSetting.toString().contains("glucose"), "glucose settings is not contains");
        assertTrue(environmentSetting.toString().contains("dose"), "dose settings is not contains");
        assertTrue(environmentSetting.toString().contains("med"), "med settings is not contains");
        assertTrue(environmentSetting.toString().contains("spo2"), "spo2 settings is not contains");
        assertTrue(environmentSetting.toString().contains("question"), "question settings is not contains");
        assertTrue(environmentSetting.toString().contains("answer"), "answer settings is not contains");
        assertTrue(environmentSetting.toString().contains("temperature"), "temperature settings is not contains");
        assertTrue(environmentSetting.toString().contains("weight"), "weight settings is not contains");
        assertTrue(environmentSetting.toString().contains("images"), "images settings is not contains");

        assertEquals(environmentSetting.get(0).getData().getDuration().getUnits(), "mins",  "Value isnot equals to mins");
        assertEquals(environmentSetting.get(0).getData().getDuration().getType(), "long",  "Value isnot equals to long");
        assertEquals(environmentSetting.get(0).getName(), "Activity",  "Value isnot equals to Activity");
        assertEquals(environmentSetting.get(0).getShortname(), "activity",  "Value isnot equals to activity");

        assertEquals(environmentSetting.get(1).getData().getSystolic().getUnits(), "mg/dL",  "Value isnot equals to mg/dL");
        assertEquals(environmentSetting.get(1).getData().getSystolic().getType(), "int",  "Value isnot equals to int");
        assertEquals(environmentSetting.get(1).getData().getHeartrate().getUnits(), "bpm",  "Value isnot equals to bpm");
        assertEquals(environmentSetting.get(1).getData().getHeartrate().getType(), "int",  "Value isnot equals to int");
        assertEquals(environmentSetting.get(1).getData().getDiastolic().getUnits(), "mg/dL",  "Value isnot equals to mg/dL");
        assertEquals(environmentSetting.get(1).getData().getDiastolic().getType(), "int",  "Value isnot equals to int");
        assertEquals(environmentSetting.get(1).getName(), "Blood Pressure",  "Value isnot equals to Blood Pressure");
        assertEquals(environmentSetting.get(1).getShortname(), "bloodpressure",  "Value isnot equals to bloodpressure");

        assertEquals(environmentSetting.get(2).getData().getGlucose().getUnits(), "mg/dL",  "Value isnot equals to mg/dL");
        assertEquals(environmentSetting.get(2).getData().getGlucose().getType(), "int",  "Value isnot equals to int");
        assertEquals(environmentSetting.get(2).getName(), "Glucose",  "Value isnot equals to Glucose");
        assertEquals(environmentSetting.get(2).getShortname(), "glucose",  "Value isnot equals to glucose");

        assertEquals(environmentSetting.get(3).getData().getDose().getType(), "string",  "Value isnot equals to string");
        assertEquals(environmentSetting.get(3).getData().getMed().getType(), "string",  "Value isnot equals to string");
        assertEquals(environmentSetting.get(3).getName(), "Medication",  "Value isnot equals to Medication");
        assertEquals(environmentSetting.get(3).getShortname(), "medication",  "Value isnot equals to medication");

        assertEquals(environmentSetting.get(4).getData().getSpo2().getUnits(), "%",  "Value isnot equals to %");
        assertEquals(environmentSetting.get(4).getData().getSpo2().getType(), "int",  "Value isnot equals to int");
        assertEquals(environmentSetting.get(4).getData().getHeartrate().getUnits(), "bpm",  "Value isnot equals to bpm");
        assertEquals(environmentSetting.get(4).getData().getHeartrate().getType(), "int",  "Value isnot equals to int");
        assertEquals(environmentSetting.get(4).getName(), "Pulse Ox",  "Value isnot equals to Pulse Ox");
        assertEquals(environmentSetting.get(4).getShortname(), "pulseox",  "Value isnot equals to pulseox");

        assertEquals(environmentSetting.get(5).getData().getQuestion().getType(), "string",  "Value isnot equals to string");
        assertEquals(environmentSetting.get(5).getData().getAnswer().getType(), "string",  "Value isnot equals to string");
        assertEquals(environmentSetting.get(5).getName(), "Survey",  "Value isnot equals to Survey");
        assertEquals(environmentSetting.get(5).getShortname(), "survey",  "Value isnot equals to survey");

        assertEquals(environmentSetting.get(6).getData().getTemperature().getUnits(), "F",  "Value isnot equals to F");
        assertEquals(environmentSetting.get(6).getData().getTemperature().getType(), "float",  "Value isnot equals to float");
        assertEquals(environmentSetting.get(6).getName(), "Temperature",  "Value isnot equals to Temperature");
        assertEquals(environmentSetting.get(6).getShortname(), "temperature",  "Value isnot equals to temperature");

        assertEquals(environmentSetting.get(7).getData().getWeight().getUnits(), "lbs",  "Value isnot equals to lbs");
        assertEquals(environmentSetting.get(7).getData().getWeight().getType(), "float",  "Value isnot equals to float");
        assertEquals(environmentSetting.get(7).getName(), "Weight",  "Value isnot equals to Weight");
        assertEquals(environmentSetting.get(7).getShortname(), "weight",  "Value isnot equals to weight");

        assertEquals(environmentSetting.get(8).getData().getImages().getUnits(), "imgs",  "Value isnot equals to imgs");
        assertEquals(environmentSetting.get(8).getData().getImages().getType(), "int",  "Value isnot equals to int");
        assertEquals(environmentSetting.get(8).getName(), "Wound Imaging",  "Value isnot equals to Wound Imaging");
        assertEquals(environmentSetting.get(8).getShortname(), "woundimaging",  "Value isnot equals to woundimaging");
    }

    @Test(dependsOnMethods = {"testEnvironmentResponse"})
    public void testEnvironmentConditions() {
        List<Condition> condition = environment.getConditions();
        assertFalse(condition.isEmpty());

        condition.forEach(cond -> {
            assertNotNull(cond.getName());
            assertNotNull(cond.getShortname());
            assertNotNull(cond.getModules());
            assertFalse(cond.getModules().isEmpty());

        });

        assertTrue(condition.stream().anyMatch(c ->
                c.getName().equals("Healthy") &&
                        c.getShortname().equals("healthy") &&
                        !c.getModules().isEmpty()
        ), "Does not have healthy conditional");

        assertTrue(condition.stream().anyMatch(c ->
                c.getName().equals("MainThree") &&
                        c.getShortname().equals("mainthree") &&
                        !c.getModules().isEmpty()),  "Does not have MainThree conditional");

        assertTrue(condition.stream().anyMatch(c ->
                c.getName().equals("Hypertension") &&
                        c.getShortname().equals("hypertension") &&
                        !c.getModules().isEmpty()), "Does not have Hypertension conditional");

        assertTrue(condition.stream().anyMatch(c ->
                c.getName().equals("COPD") &&
                        c.getShortname().equals("copd") &&
                        !c.getModules().isEmpty()), "Does not have COPD conditional");

        assertTrue(condition.stream().anyMatch(c ->
                c.getName().equals("Diabetes") &&
                        c.getShortname().equals("diabetes") &&
                        !c.getModules().isEmpty()), "Does not have Diabetes conditional");
    }

    @Test(dependsOnMethods = {"testEnvironmentResponse"})
    public void testEnvironmentPatientCustomAttributes() {
        List<PatientInfoCustomAttributes> patientCustomAttributes = environment.getPatientInfoCustomAttributes();

        assertNotNull(patientCustomAttributes);
        PatientInfoCustomAttributes attribute = patientCustomAttributes.get(0);
        assertFalse(attribute.getId().toString().isEmpty(), "getId() field is empty");
        assertFalse(attribute.getName().isEmpty(), "getName field is empty");
        assertFalse(attribute.getType().isEmpty(), "getType field is empty");
        assertFalse(attribute.getRequired().toString().isEmpty(), "getRequired().toString field is empty");
        assertFalse(attribute.getPatientsetup().toString().isEmpty(), "getPatientsetup().toString field is empty");
        assertFalse(attribute.get_class().isEmpty(), "get_class field is empty");
        assertFalse(attribute.getEmrTracked().toString().isEmpty(), "getEmrTracked().toString field is empty");
        assertFalse(attribute.getLastUpdated().isEmpty(), "getLastUpdated field is empty");
    }
}

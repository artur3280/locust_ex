package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.patients.patient_list.PatientList;
import com.hrs.cc.api.models.reports.statuses.Datum;
import com.hrs.cc.api.models.reports.statuses.ReportsStatusesResponse;
import com.hrs.cc.api.models.requests.add_new_report.ReportRequest;
import core.PropertyWorker;
import core.rest.RestUtilities;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestGatewayReportsRequests extends Assert {
    private ObjectMapper mapper = new ObjectMapper();
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private PropertyWorker propertyWorker;
    private static List<List<PatientList>> patientsList;
    private static Map<String, String> timePeriod;
    private static String patientId;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath(Path.API_V2);
        timePeriod = restUtilities.getTimePeriod(7);
        patientId = RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG);
    }

    @AfterClass
    public void removeObj() {
        mapper = null;
        reqSpec = null;
        resPec = null;
        restUtilities = null;
        queryParam = null;
        userData = null;
        propertyWorker = null;
        System.gc();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        restUtilities.removeQueryParam(reqSpec, "filter[patient]");
        restUtilities.removeQueryParam(reqSpec, "start");
        restUtilities.removeQueryParam(reqSpec, "end");
        queryParam.clear();
    }

    @Test(enabled = false)
    public void testGenerateGateWayReportWithoutPassword() {
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.REPORTS);

        RestAssistant.testSetClinicianInsecureReportsSetting(true);
        List<ReportRequest> types = new ArrayList<>();
        types.add(new ReportRequest("population", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("adherence", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("biometrics", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("clinicians", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("communication", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("engagement", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("enrollment", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("notes", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("patientinteraction", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("readmissions", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("videostatus", timePeriod.get("minusDays"), timePeriod.get("current")));
        reqSpec.body(types);
        Response response = restUtilities.getResponse(reqSpec, "post");

        JsonPath path = response.jsonPath();
        assertNotNull(path.get("data.id"), "data.id field is null");
        assertNull(path.get("data.password"), "data.password field is not null");
        assertNotNull(path.get("data.type"), "data.type field is null");
    }


    @Test(dependsOnMethods = {"testGenerateGateWayReportWithoutPassword"}, enabled = false)
    public void testPostGenerateGateWayReport() {
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.REPORTS);

        RestAssistant.testSetClinicianInsecureReportsSetting(false);
        List<ReportRequest> types = new ArrayList<>();
        types.add(new ReportRequest("population", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("adherence", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("biometrics", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("clinicians", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("communication", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("engagement", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("enrollment", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("notes", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("patientinteraction", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("readmissions", timePeriod.get("minusDays"), timePeriod.get("current")));
        types.add(new ReportRequest("videostatus", timePeriod.get("minusDays"), timePeriod.get("current")));
        reqSpec.body(types);
        Response response = restUtilities.getResponse(reqSpec, "post");

        JsonPath path = response.jsonPath();
        assertNotNull(path.get("data.id"), "data.id field is null");
        assertNotNull(path.get("data.password"), "data.password field is null");
        assertNotNull(path.get("data.type"), "data.type field is null");
        String createdReport = path.get("data.id");


        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")))
                        .setBaseUri(Path.BASE_URI_APP_INTEGRATION)
                        .setBasePath(Path.API_V2));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.REPORT_STATUSES);
        Response checker = restUtilities.getResponse(newReqSpec, "get");
        ReportsStatusesResponse statusesResponse = checker.as(ReportsStatusesResponse.class);
        Datum report = statusesResponse.getData().stream().
                filter(r -> r.getId().equals(createdReport)).collect(Collectors.toList()).get(0);
        assertEquals(report.getPassword(), path.get("data.password"), "Passwords not eqls");
        assertNotNull(report.getStatus(), "Status field is null");
        assertNotNull(report.getReportType(), "ReportType field is null");
        assertNotNull(report.getId(), "Id field is null");
        assertNotNull(report.getStart(), "Start field is null");
        assertNotNull(report.getStop(), "Stop field is null");
        assertNotNull(report.getDownloaded(), "Downloaded field is null");
    }

    @Test(dependsOnMethods = {"testGenerateGateWayReportWithoutPassword"}, enabled = false)
    public void testPostGeneratePopulationGateWayReport() {
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.REPORTS);

        RestAssistant.testSetClinicianInsecureReportsSetting(false);
        List<ReportRequest> types = new ArrayList<>();
        types.add(new ReportRequest("population", timePeriod.get("minusDays"), timePeriod.get("current")));
        reqSpec.body(types);
        Response response = restUtilities.getResponse(reqSpec, "post");
        JsonPath path = response.jsonPath();
        assertNotNull(path.get("data.id"), "data.id field is null");
        assertNotNull(path.get("data.password"), "data.password field is null");
        assertNotNull(path.get("data.type"), "data.type field is null");
        String createdReport = path.get("data.id");

        RequestSpecification newReqSpec = restUtilities.getRequestSpecification(
                restUtilities.createRequestBuilder()
                        .addHeader("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")))
                        .setBaseUri(Path.BASE_URI_APP_INTEGRATION)
                        .setBasePath(Path.API_V2));

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.REPORT_STATUSES);
        Response checker = restUtilities.getResponse(newReqSpec, "get");
        ReportsStatusesResponse statusesResponse = checker.as(ReportsStatusesResponse.class);
        Datum report = statusesResponse.getData().stream().
                filter(r -> r.getId().equals(createdReport)).collect(Collectors.toList()).get(0);
        assertEquals(report.getPassword(), path.get("data.password"), "Passwords not eqls");
        assertEquals(report.getReportType(), "population", "Type is not population");

        assertNotNull(report.getStatus(), "Status field is null");
        assertNotNull(report.getReportType(), "ReportType field is null");
        assertNotNull(report.getId(), "Id field is null");
        assertNotNull(report.getStart(), "Start field is null");
        assertNotNull(report.getStop(), "Stop field is null");
        assertNotNull(report.getDownloaded(), "Downloaded field is null");

    }
}
package com.hrs.cc.api.tests.integration_app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.integratioins_app.ivr.questions.Datum;
import com.hrs.cc.api.models.integratioins_app.ivr.questions.IvrQuestionsList;
import com.hrs.cc.api.models.integratioins_app.ivr.schedule.IvrSchedule;
import com.hrs.cc.api.models.patients.patient_list.PatientList;
import com.hrs.cc.api.models.patients.syrvey.PatientSurveys;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestGatewayIvrSchedule extends Assert {
    private ObjectMapper mapper = new ObjectMapper();
    private RequestSpecification reqSpec;
    private ResponseSpecification resPec;
    private RestUtilities restUtilities;
    private Map<String, String> queryParam = new HashMap<>();
    private static Map<String, String> userData;
    private PropertyWorker propertyWorker;
    private static List<List<PatientList>> patientsList;
    private static Map<String, String> timePeriod;
    private static String patientId;
    private static HashMap<Object, Object> body;

    @BeforeTest
    public void setUp() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
        RestAssured.reset();
    }

    @BeforeClass
    public void setUpNext() {
        Logger.debugging(false);
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();
        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath("");
        patientId = RestAssistant.getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG);
        RestAssistant.setModulesToPatientById(patientId);
    }

    @AfterClass
    public void removeObj() {
        mapper = null;
        reqSpec = null;
        resPec = null;
        restUtilities = null;
        queryParam = null;
        userData = null;
        propertyWorker = null;
        System.gc();
    }

    @AfterMethod
    public void configure() {
        restUtilities.removeQueryParam(reqSpec, queryParam);
        restUtilities.removeHeaders(reqSpec);
        restUtilities.removePathParams(reqSpec);
        restUtilities.resetQueryPath(reqSpec);
        queryParam.clear();
    }


    @Test
    public void testGetActiveIrvScheduleIfTurnONModule() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
        IvrSchedule ivrSchedule = response.as(IvrSchedule.class);

        assertEquals(ivrSchedule.getData().getId(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getHrsid(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getStatus(), "active", "Status not active");
    }

    @Test(dependsOnMethods = {"testGetActiveIrvScheduleIfTurnONModule"})
    public void testGetInactiveIrvScheduleIfTurnOffModule() {
        RestAssistant.setModulesToPatientByIdWithoutPCV(patientId);
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");

        IvrSchedule ivrSchedule = response.as(IvrSchedule.class);

        assertEquals(ivrSchedule.getData().getId(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getHrsid(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getStatus(), "inactive", "Status not inactive");
    }

    @Test(dependsOnMethods = {"testGetActiveIrvScheduleIfTurnONModule", "testGetInactiveIrvScheduleIfTurnOffModule"})
    public void testSetActiveStatusToIrvSchedule() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, Object> body = new HashMap<>();
        Map<String, String> data = new HashMap<>();
        data.put("status", "active");
        body.put("data", data);
        reqSpec.body(body);


        Response response = restUtilities.getResponse(reqSpec, "patch");
        IvrSchedule ivrSchedule = response.as(IvrSchedule.class);

        assertEquals(ivrSchedule.getData().getId(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getHrsid(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getStatus(), "active", "Status not active");
        assertNotNull(ivrSchedule.getData().getCallAt(), "CallAt field is null");
        assertNotNull(ivrSchedule.getData().getRetry().getMeasure(), "Retry.Measure field is null");
        assertNotNull(ivrSchedule.getData().getRetry().getUnits(), "Retry.Units field is null");
        assertNotNull(ivrSchedule.getData().getRetryCount(), "RetryCount field is null");
        assertNotNull(ivrSchedule.getData().getEntries(), "Entries field is null");
    }

    @Test(dependsOnMethods = {"testGetActiveIrvScheduleIfTurnONModule", "testGetInactiveIrvScheduleIfTurnOffModule", "testSetActiveStatusToIrvSchedule"})
    public void testSetInActiveStatusToIrvSchedule() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, Object> body = new HashMap<>();
        Map<String, String> data = new HashMap<>();
        data.put("status", "inactive");
        body.put("data", data);
        reqSpec.body(body);


        Response response = restUtilities.getResponse(reqSpec, "patch");
        IvrSchedule ivrSchedule = response.as(IvrSchedule.class);

        assertEquals(ivrSchedule.getData().getId(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getHrsid(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getStatus(), "inactive", "Status not inactive");
    }

    @Test(dependsOnMethods = {"testGetActiveIrvScheduleIfTurnONModule", "testGetInactiveIrvScheduleIfTurnOffModule", "testSetActiveStatusToIrvSchedule", "testSetInActiveStatusToIrvSchedule"})
    public void testGetInactiveIrvSchedule() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
        IvrSchedule ivrSchedule = response.as(IvrSchedule.class);

        assertEquals(ivrSchedule.getData().getId(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getHrsid(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getStatus(), "inactive", "Status is not inactive");
    }

    @Test(dependsOnMethods = {"testGetActiveIrvScheduleIfTurnONModule", "testGetInactiveIrvScheduleIfTurnOffModule", "testSetActiveStatusToIrvSchedule", "testSetInActiveStatusToIrvSchedule"})
    public void testSetIncorrectStatusToIrvSchedule() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Map<String, Object> body = new HashMap<>();
        Map<String, String> data = new HashMap<>();
        data.put("status", RandomData.getRandomString(5));
        body.put("data", data);
        reqSpec.body(body);


        Response response = restUtilities.getResponse(reqSpec, "patch");
        IvrSchedule ivrSchedule = response.as(IvrSchedule.class);

        assertEquals(ivrSchedule.getData().getId(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getHrsid(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getStatus(), "inactive", "Status is not inactive");
    }

    @Test
    public void testGetEncIrvScheduleWithIncorrectToken() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(401)
        );

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(RandomData.getRandomString()));

        Response response = restUtilities.getResponse(reqSpec, "get");
        JsonPath path = response.jsonPath();
        assertTrue(path.get("errors").toString().contains("No credentials provided"), "Error message is not 'No credentials provided'");
    }

    @Test
    public void testGetEncIrvScheduleWithIncorrectId() {
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(404)
        );

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", RandomData.getRandomString(5));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));
        Response response = restUtilities.getResponse(reqSpec, "get");

        JsonPath path = response.jsonPath();
        assertEquals(path.get("message"),"Unknown IVR Schedule ID", "Error message is not 'Unknown IVR Schedule ID'");
    }

    @Test(priority = 1)
    public void testGetStatusIvrAfterPausedPatient() {
        RestAssistant.activateIRVSchedule(patientId);
        RestAssistant.pausePatient(patientId);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");

        IvrSchedule ivrSchedule = response.as(IvrSchedule.class);

        assertEquals(ivrSchedule.getData().getId(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getHrsid(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getStatus(), "inactive", "Status is not inactive");
        RestAssistant.activatedPatient(patientId);
    }

    @Test(dependsOnMethods = {"testGetStatusIvrAfterPausedPatient"}, priority = 1)
    public void testGetStatusIvrAfterDeactivatePatient() {
        RestAssistant.activateIRVSchedule(patientId);
        RestAssistant.getDeactivationPatient(patientId);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", patientId);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");

        IvrSchedule ivrSchedule = response.as(IvrSchedule.class);

        assertEquals(ivrSchedule.getData().getId(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getHrsid(), patientId, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getStatus(), "inactive", "Status is not inactive");
        RestAssistant.activatedPatient(patientId);
    }

    @Test
    public void testGetScheduleAfterCreatedPatient() {
        Logger.debugging(true);
        String id = RestAssistant.addNewPatient();
//        RestAssistant.setModulesToPatientById(id);
//        RestAssistant.setRisksToPatientById(id);


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(200)
        );
        restUtilities.setEndPoint(EndPoints.IVR_SCHEDULE.concat(EndPoints.ID));
        restUtilities.createPathParam(reqSpec, "id", id);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "get");
        IvrSchedule ivrSchedule = response.as(IvrSchedule.class);

        assertEquals(ivrSchedule.getData().getId(), id, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getHrsid(), id, "Patient ids not eql");
        assertEquals(ivrSchedule.getData().getStatus(), "active", "Status is inactive");
    }

    @Test(enabled = false)
    public void testCallToPatient() {
        RestAssistant.turnOnPatientVoiceInEnv();

        RestUtilities restUtilities = new RestUtilities();
        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        restUtilities.setContentType(ContentType.JSON);
        reqSpec = restUtilities.getRequestSpecification();

        resPec = restUtilities.getResponseSpecification();
        reqSpec.basePath("");


        restUtilities.setContentType(ContentType.JSON);
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder().
                        expectContentType(ContentType.JSON)
                        .expectStatusCode(500)
        );
        restUtilities.setEndPoint(EndPoints.IVR);
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));


        reqSpec.body("{\n" +
                "    \"data\": {\n" +
                "    \"hrsid\": \"mNUySZH6bVuLjGGnP9Sj\",\n" +
                "    \"firstQuestion\": 1,\n" +
                "    \"questions\": {\n" +
                "      \"question_1\": {\n" +
                "            \"body\": \"How are you feeling?\",\n" +
                "            \"type\": \"numeric_choice\",\n" +
                "        \"id\": 14,\n" +
                "        \"workflow\": {\n" +
                "        \t\"choice_1\": 4,\n" +
                "        \t\"choice_3\": 4,\n" +
                "        \t\"choice_5\": 4\n" +
                "        },\n" +
                "        \"choices\": [\n" +
                "          {\n" +
                "              \"id\": 1,\n" +
                "            \"body\": \"Great\",\n" +
                "            \"choice\": 1\n" +
                "          },\n" +
                "          {\n" +
                "              \"id\": 1,\n" +
                "            \"body\": \"Ok\",\n" +
                "            \"choice\": 5\n" +
                "          },\n" +
                "          {\n" +
                "              \"id\": 1,\n" +
                "            \"body\": \"Bad\",\n" +
                "            \"choice\": 3\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      \"question_4\": {\n" +
                "            \"body\": \"Did you take your medicine today?\",\n" +
                "            \"type\": \"numeric_choice\",\n" +
                "        \"id\": 15,\n" +
                "        \"workflow\": {},\n" +
                "         \"choices\": [\n" +
                "          {\n" +
                "              \"id\": 6,\n" +
                "            \"body\": \"Yes\",\n" +
                "            \"choice\": 1\n" +
                "          },\n" +
                "          {\n" +
                "              \"id\": 7,\n" +
                "            \"body\": \"No\",\n" +
                "            \"choice\": 2\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}");

        Response response = restUtilities.getResponse(reqSpec, "post");

    }
}
package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "schedule",
        "dose",
        "instruction",
        "time",
        "id",
        "med",
        "ts",
        "status",
        "expiration",
        "essential",
        "window"
})
public class Medication_ {

    @JsonProperty("schedule")
    private Schedule_ schedule;
    @JsonProperty("dose")
    private String dose;
    @JsonProperty("instruction")
    private String instruction;
    @JsonProperty("time")
    private String time;
    @JsonProperty("id")
    private String id;
    @JsonProperty("med")
    private String med;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("status")
    private String status;
    @JsonProperty("expiration")
    private String expiration = null;
    @JsonProperty("essential")
    private Boolean essential = null;
    @JsonProperty("window")
    private String window;

    public Medication_() {
    }

    public Medication_(Schedule_ schedule, String dose, String instruction, String time, String id, String med, Long ts, String status, String expiration, Boolean essential, String window) {
        this.schedule = schedule;
        this.dose = dose;
        this.instruction = instruction;
        this.time = time;
        this.id = id;
        this.med = med;
        this.ts = ts;
        this.status = status;
        this.expiration = expiration;
        this.essential = essential;
        this.window = window;
    }

    public Schedule_ getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule_ schedule) {
        this.schedule = schedule;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMed() {
        return med;
    }

    public void setMed(String med) {
        this.med = med;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public Boolean getEssential() {
        return essential;
    }

    public void setEssential(Boolean essential) {
        this.essential = essential;
    }

    public String getWindow() {
        return window;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    @Override
    public String toString() {
        return "Medication_{" +
                "schedule=" + schedule +
                ", dose='" + dose + '\'' +
                ", instruction='" + instruction + '\'' +
                ", time='" + time + '\'' +
                ", id='" + id + '\'' +
                ", med='" + med + '\'' +
                ", ts=" + ts +
                ", status='" + status + '\'' +
                ", expiration='" + expiration + '\'' +
                ", essential=" + essential +
                ", window='" + window + '\'' +
                '}';
    }
}

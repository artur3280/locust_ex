package com.hrs.cc.api.models.integratioins_app.clinician_auth_token;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "meta"
})
public class ClinicianAuthToken {

    @JsonProperty("data")
    private Data data;
    @JsonProperty("meta")
    private Meta metas = null;

    public ClinicianAuthToken() {
    }

    public ClinicianAuthToken(Data data, Meta metas) {
        this.data = data;
        this.metas = metas;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Meta getMetas() {
        return metas;
    }

    public void setMetas(Meta metas) {
        this.metas = metas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClinicianAuthToken that = (ClinicianAuthToken) o;
        return Objects.equals(data, that.data) &&
                Objects.equals(metas, that.metas);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, metas);
    }

    @Override
    public String toString() {
        return "ClinicianAuthToken{" +
                "data=" + data +
                ", metas=" + metas +
                '}';
    }
}

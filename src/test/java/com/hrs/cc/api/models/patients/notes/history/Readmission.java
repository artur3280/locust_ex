package com.hrs.cc.api.models.patients.notes.history;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "patient",
        "noteHistoryId",
        "date",
        "location",
        "lengthOfStay",
        "lastUpdated"
})
public class Readmission {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("patient")
    private String patient;
    @JsonProperty("noteHistoryId")
    private String noteHistoryId;
    @JsonProperty("date")
    private Long date;
    @JsonProperty("location")
    private String location;
    @JsonProperty("lengthOfStay")
    private String lengthOfStay;
    @JsonProperty("lastUpdated")
    private String lastUpdated;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public Readmission() {
    }

    public Readmission(Long id, String patient, String noteHistoryId, Long date, String location, String lengthOfStay, String lastUpdated) {
        this.id = id;
        this.patient = patient;
        this.noteHistoryId = noteHistoryId;
        this.date = date;
        this.location = location;
        this.lengthOfStay = lengthOfStay;
        this.lastUpdated = lastUpdated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getNoteHistoryId() {
        return noteHistoryId;
    }

    public void setNoteHistoryId(String noteHistoryId) {
        this.noteHistoryId = noteHistoryId;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLengthOfStay() {
        return lengthOfStay;
    }

    public void setLengthOfStay(String lengthOfStay) {
        this.lengthOfStay = lengthOfStay;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Readmission that = (Readmission) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(patient, that.patient) &&
                Objects.equals(noteHistoryId, that.noteHistoryId) &&
                Objects.equals(date, that.date) &&
                Objects.equals(location, that.location) &&
                Objects.equals(lengthOfStay, that.lengthOfStay) &&
                Objects.equals(lastUpdated, that.lastUpdated) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, patient, noteHistoryId, date, location, lengthOfStay, lastUpdated, additionalProperties);
    }

    @Override
    public String toString() {
        return "Readmission{" +
                "id=" + id +
                ", patient='" + patient + '\'' +
                ", noteHistoryId='" + noteHistoryId + '\'' +
                ", date=" + date +
                ", location='" + location + '\'' +
                ", lengthOfStay='" + lengthOfStay + '\'' +
                ", lastUpdated='" + lastUpdated + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

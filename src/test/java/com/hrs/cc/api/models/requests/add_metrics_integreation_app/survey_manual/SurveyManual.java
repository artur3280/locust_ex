package com.hrs.cc.api.models.requests.add_metrics_integreation_app.survey_manual;


public class SurveyManual {
    private DataSurvey data;

    /**
     * No args constructor for use in serialization
     */
    public SurveyManual() {
    }

    /**
     * @param data
     */
    public SurveyManual(DataSurvey data) {
        super();
        this.data = data;
    }

    public DataSurvey getData() {
        return data;
    }

    public void setData(DataSurvey data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "DataSurvey{" +
                "data=" + data +
                '}';
    }
}

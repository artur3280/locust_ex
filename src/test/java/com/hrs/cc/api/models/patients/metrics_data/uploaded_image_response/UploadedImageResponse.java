package com.hrs.cc.api.models.patients.metrics_data.uploaded_image_response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "owner",
        "entered",
        "metric",
        "remindAt",
        "takenAt",
        "device",
        "facet"
})
public class UploadedImageResponse {
    @JsonProperty("owner")
    private String owner;
    @JsonProperty("entered")
    private String entered;
    @JsonProperty("metric")
    private String metric;
    @JsonProperty("remindAt")
    private String remindAt;
    @JsonProperty("takenAt")
    private String takenAt;
    @JsonProperty("device")
    private Object device;
    @JsonProperty("facet")
    private Facet facet;

    public UploadedImageResponse() {
    }

    public UploadedImageResponse(String owner, String entered, String metric, String remindAt, String takenAt, Object device, Facet facet) {
        this.owner = owner;
        this.entered = entered;
        this.metric = metric;
        this.remindAt = remindAt;
        this.takenAt = takenAt;
        this.device = device;
        this.facet = facet;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getEntered() {
        return entered;
    }

    public void setEntered(String entered) {
        this.entered = entered;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getRemindAt() {
        return remindAt;
    }

    public void setRemindAt(String remindAt) {
        this.remindAt = remindAt;
    }

    public String getTakenAt() {
        return takenAt;
    }

    public void setTakenAt(String takenAt) {
        this.takenAt = takenAt;
    }

    public Object getDevice() {
        return device;
    }

    public void setDevice(Object device) {
        this.device = device;
    }

    public Facet getFacet() {
        return facet;
    }

    public void setFacet(Facet facet) {
        this.facet = facet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UploadedImageResponse that = (UploadedImageResponse) o;
        return Objects.equals(owner, that.owner) &&
                Objects.equals(entered, that.entered) &&
                Objects.equals(metric, that.metric) &&
                Objects.equals(remindAt, that.remindAt) &&
                Objects.equals(takenAt, that.takenAt) &&
                Objects.equals(device, that.device) &&
                Objects.equals(facet, that.facet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, entered, metric, remindAt, takenAt, device, facet);
    }

    @Override
    public String toString() {
        return "UploadedImageResponse{" +
                "owner='" + owner + '\'' +
                ", entered='" + entered + '\'' +
                ", metric='" + metric + '\'' +
                ", remindAt='" + remindAt + '\'' +
                ", takenAt='" + takenAt + '\'' +
                ", device=" + device +
                ", facet=" + facet +
                '}';
    }
}

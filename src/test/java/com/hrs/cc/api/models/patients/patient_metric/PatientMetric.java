package com.hrs.cc.api.models.patients.patient_metric;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "patient",
        "clinician",
        "type",
        "weight",
        "reminder",
        "finished",
        "time",
        "status",
        "reason",
        "lastUpdated"
})
public class PatientMetric {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("patient")
    private String patient;
    @JsonProperty("clinician")
    private String clinician;
    @JsonProperty("type")
    private String type;
    @JsonProperty("weight")
    private String weight;
    @JsonProperty("reminder")
    private String reminder;
    @JsonProperty("finished")
    private String finished;
    @JsonProperty("time")
    private Time time;
    @JsonProperty("status")
    private String status;
    @JsonProperty("reason")
    private Object reason;
    @JsonProperty("lastUpdated")
    private LastUpdated lastUpdated;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public PatientMetric() {
    }

    public PatientMetric(Integer id, String patient, String clinician, String type, String weight, String reminder, String finished, Time time, String status, Object reason, LastUpdated lastUpdated) {
        this.id = id;
        this.patient = patient;
        this.clinician = clinician;
        this.type = type;
        this.weight = weight;
        this.reminder = reminder;
        this.finished = finished;
        this.time = time;
        this.status = status;
        this.reason = reason;
        this.lastUpdated = lastUpdated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getClinician() {
        return clinician;
    }

    public void setClinician(String clinician) {
        this.clinician = clinician;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getReminder() {
        return reminder;
    }

    public void setReminder(String reminder) {
        this.reminder = reminder;
    }

    public String getFinished() {
        return finished;
    }

    public void setFinished(String finished) {
        this.finished = finished;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getReason() {
        return reason;
    }

    public void setReason(Object reason) {
        this.reason = reason;
    }

    public LastUpdated getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LastUpdated lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientMetric that = (PatientMetric) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(patient, that.patient) &&
                Objects.equals(clinician, that.clinician) &&
                Objects.equals(type, that.type) &&
                Objects.equals(weight, that.weight) &&
                Objects.equals(reminder, that.reminder) &&
                Objects.equals(finished, that.finished) &&
                Objects.equals(time, that.time) &&
                Objects.equals(status, that.status) &&
                Objects.equals(reason, that.reason) &&
                Objects.equals(lastUpdated, that.lastUpdated) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, patient, clinician, type, weight, reminder, finished, time, status, reason, lastUpdated, additionalProperties);
    }

    @Override
    public String toString() {
        return "PatientMetric{" +
                "id=" + id +
                ", patient='" + patient + '\'' +
                ", clinician='" + clinician + '\'' +
                ", type='" + type + '\'' +
                ", weight='" + weight + '\'' +
                ", reminder='" + reminder + '\'' +
                ", finished='" + finished + '\'' +
                ", time=" + time +
                ", status='" + status + '\'' +
                ", reason=" + reason +
                ", lastUpdated=" + lastUpdated +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

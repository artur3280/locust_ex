package com.hrs.cc.api.models.patients.caregivers_list;


public class Sideload {

    private Caregiver caregiver;

    public Sideload() {
    }

    public Sideload(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    @Override
    public String toString() {
        return "Sideload{" +
                "caregiver=" + caregiver +
                '}';
    }
}

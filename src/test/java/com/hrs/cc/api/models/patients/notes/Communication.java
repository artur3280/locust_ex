package com.hrs.cc.api.models.patients.notes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "result",
        "followup",
        "itproblems"
})
public class Communication {

    @JsonProperty("type")
    private String type;
    @JsonProperty("result")
    private List<String> result = null;
    @JsonProperty("followup")
    private List<String> followup = null;
    @JsonProperty("itproblems")
    private List<String> itproblems = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Communication() {
    }

    public Communication(String type, List<String> result, List<String> followup, List<String> itproblems) {
        this.type = type;
        this.result = result;
        this.followup = followup;
        this.itproblems = itproblems;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getResult() {
        return result;
    }

    public void setResult(List<String> result) {
        this.result = result;
    }

    public List<String> getFollowup() {
        return followup;
    }

    public void setFollowup(List<String> followup) {
        this.followup = followup;
    }

    public List<String> getItproblems() {
        return itproblems;
    }

    public void setItproblems(List<String> itproblems) {
        this.itproblems = itproblems;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Communication that = (Communication) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(result, that.result) &&
                Objects.equals(followup, that.followup) &&
                Objects.equals(itproblems, that.itproblems) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, result, followup, itproblems, additionalProperties);
    }

    @Override
    public String toString() {
        return "Communication{" +
                "type='" + type + '\'' +
                ", result=" + result +
                ", followup=" + followup +
                ", itproblems=" + itproblems +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

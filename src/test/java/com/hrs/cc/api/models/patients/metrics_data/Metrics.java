package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "glucose",
        "activity",
        "woundimaging",
        "temperature",
        "weight",
        "survey",
        "medication",
        "bloodpressure",
        "pulseox",
        "patientconnectvoice",
        "stethoscope",
        "steps"
})
public class Metrics <T>{

    @JsonProperty("glucose")
    private T glucose;
    @JsonProperty("activity")
    private Activity activity;
    @JsonProperty("woundimaging")
    private T woundimaging;
    @JsonProperty("temperature")
    private T temperature;
    @JsonProperty("weight")
    private Weight weight;
    @JsonProperty("survey")
    private T survey;
    @JsonProperty("medication")
    private Medication medication;
    @JsonProperty("bloodpressure")
    private T bloodpressure;
    @JsonProperty("pulseox")
    private T pulseox;
    @JsonProperty("patientconnectvoice")
    private ArrayList<Object> patientconnectvoice = null;
    @JsonProperty("stethoscope")
    private Stethoscope stethoscope = null;
    @JsonProperty("steps")
    private ArrayList<Objects> steps = null;

    public Metrics() {
    }

    public Metrics(T glucose, Activity activity, T woundimaging, T temperature, Weight weight, T survey, Medication medication, T bloodpressure, T pulseox, ArrayList<Object> patientconnectvoice, Stethoscope stethoscope, ArrayList<Objects> steps) {
        this.glucose = glucose;
        this.activity = activity;
        this.woundimaging = woundimaging;
        this.temperature = temperature;
        this.weight = weight;
        this.survey = survey;
        this.medication = medication;
        this.bloodpressure = bloodpressure;
        this.pulseox = pulseox;
        this.patientconnectvoice = patientconnectvoice;
        this.stethoscope = stethoscope;
        this.steps = steps;
    }

    public T getGlucose() {
        return glucose;
    }

    public void setGlucose(T glucose) {
        this.glucose = glucose;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public T getWoundimaging() {
        return woundimaging;
    }

    public void setWoundimaging(T woundimaging) {
        this.woundimaging = woundimaging;
    }

    public T getTemperature() {
        return temperature;
    }

    public void setTemperature(T temperature) {
        this.temperature = temperature;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public T getSurvey() {
        return survey;
    }

    public void setSurvey(T survey) {
        this.survey = survey;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public T getBloodpressure() {
        return bloodpressure;
    }

    public void setBloodpressure(T bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    public T getPulseox() {
        return pulseox;
    }

    public void setPulseox(T pulseox) {
        this.pulseox = pulseox;
    }

    public ArrayList<Object> getPatientconnectvoice() {
        return patientconnectvoice;
    }

    public void setPatientconnectvoice(ArrayList<Object> patientconnectvoice) {
        this.patientconnectvoice = patientconnectvoice;
    }

    public Stethoscope getStethoscope() {
        return stethoscope;
    }

    public void setStethoscope(Stethoscope stethoscope) {
        this.stethoscope = stethoscope;
    }

    public ArrayList<Objects> getSteps() {
        return steps;
    }

    public void setSteps(ArrayList<Objects> steps) {
        this.steps = steps;
    }

    @Override
    public String toString() {
        return "Metrics{" +
                "glucose=" + glucose +
                ", activity=" + activity +
                ", woundimaging=" + woundimaging +
                ", temperature=" + temperature +
                ", weight=" + weight +
                ", survey=" + survey +
                ", medication=" + medication +
                ", bloodpressure=" + bloodpressure +
                ", pulseox=" + pulseox +
                ", patientconnectvoice=" + patientconnectvoice +
                ", stethoscope=" + stethoscope +
                ", steps=" + steps +
                '}';
    }
}

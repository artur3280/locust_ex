package com.hrs.cc.api.models.environment.presets;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "relationships",
        "attributes",
        "id",
        "resourceType"
})
public class Datum {

    @JsonProperty("relationships")
    private Relationships relationships;
    @JsonProperty("attributes")
    private Attributes attributes;
    @JsonProperty("id")
    private String id;
    @JsonProperty("resourceType")
    private String resourceType;

    public Datum() {
    }

    public Datum(Relationships relationships, Attributes attributes, String id, String resourceType) {
        this.relationships = relationships;
        this.attributes = attributes;
        this.id = id;
        this.resourceType = resourceType;
    }

    public Relationships getRelationships() {
        return relationships;
    }

    public void setRelationships(Relationships relationships) {
        this.relationships = relationships;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "relationships=" + relationships +
                ", attributes=" + attributes +
                ", id='" + id + '\'' +
                ", resourceType='" + resourceType + '\'' +
                '}';
    }
}

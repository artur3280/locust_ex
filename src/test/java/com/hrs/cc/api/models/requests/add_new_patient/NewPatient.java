package com.hrs.cc.api.models.requests.add_new_patient;

import java.util.List;
import java.util.Objects;

public class NewPatient {
    private List<CustomAtributs> customAtributsList = null;
    private Name name;
    private String dob;
    private String gender;
    private String phone;
    private String pid;
    private String subgroup;

    public NewPatient() {
    }

    public NewPatient(List<CustomAtributs> customAtributsList, Name name, String dob, String gender, String phone, String pid, String subgroup) {
        this.customAtributsList = customAtributsList;
        this.name = name;
        this.dob = dob;
        this.gender = gender;
        this.phone = phone;
        this.pid = pid;
        this.subgroup = subgroup;
    }

    public List<CustomAtributs> getCustomAtributsList() {
        return customAtributsList;
    }

    public void setCustomAtributsList(List<CustomAtributs> customAtributsList) {
        this.customAtributsList = customAtributsList;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Override
    public String toString() {
        return "NewPatient{" +
                "PATIENTINFOCUSTOMATTRIBUTE=" + customAtributsList +
                ", name=" + name +
                ", dob='" + dob + '\'' +
                ", gender='" + gender + '\'' +
                ", phone='" + phone + '\'' +
                ", pid='" + pid + '\'' +
                ", subgroup='" + subgroup + '\'' +
                '}';
    }

    public String getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(String subgroup) {
        this.subgroup = subgroup;
    }
}

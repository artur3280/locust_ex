package com.hrs.cc.api.models.environment.data.environment_settings_type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "riskdata",
        "phone",
        "appid",
        "name"
})
public class Riskdatum {
    @JsonProperty("riskdata")
    private List<Riskdata> riskdata = null;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("appid")
    private String appid;
    @JsonProperty("name")
    private String name;
    @JsonProperty("shortname")
    private String shortname;
    @JsonProperty("modules")
    private List<String> modules;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Riskdatum() {
    }

    public Riskdatum(List<Riskdata> riskdata) {
        this.riskdata = riskdata;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public List<Riskdata> getRiskdata() {
        return riskdata;
    }

    public void setRiskdata(List<Riskdata> riskdata) {
        this.riskdata = riskdata;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Riskdatum riskdatum = (Riskdatum) o;
        return Objects.equals(riskdata, riskdatum.riskdata) &&
                Objects.equals(additionalProperties, riskdatum.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(riskdata, additionalProperties);
    }

    @Override
    public String toString() {
        return "Riskdatum{" +
                "riskdata=" + riskdata +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

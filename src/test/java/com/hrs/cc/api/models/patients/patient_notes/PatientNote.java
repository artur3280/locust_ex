package com.hrs.cc.api.models.patients.patient_notes;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "patient",
        "clinician",
        "note",
        "created",
        "updated",
        "status",
        "time",
        "timespent",
        "readmission",
        "edvisit",
        "communication",
        "enrollment",
        "observation"
})
public class PatientNote {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("patient")
    private String patient;
    @JsonProperty("clinician")
    private Clinician clinician;
    @JsonProperty("note")
    private String note;
    @JsonProperty("created")
    private Created created;
    @JsonProperty("updated")
    private Object updated;
    @JsonProperty("status")
    private String status;
    @JsonProperty("time")
    private String time;
    @JsonProperty("timespent")
    private Object timespent;
    @JsonProperty("readmission")
    private Readmission readmission;
    @JsonProperty("edvisit")
    private Edvisit edvisit;
    @JsonProperty("communication")
    private Object communication;
    @JsonProperty("enrollment")
    private Object enrollment;
    @JsonProperty("observation")
    private Object observation;

    public PatientNote() {
    }

    public PatientNote(Integer id, String patient, Clinician clinician, String note, Created created, Object updated, String status, String time, Object timespent, Readmission readmission, Edvisit edvisit) {
        this.id = id;
        this.patient = patient;
        this.clinician = clinician;
        this.note = note;
        this.created = created;
        this.updated = updated;
        this.status = status;
        this.time = time;
        this.timespent = timespent;
        this.readmission = readmission;
        this.edvisit = edvisit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public Clinician getClinician() {
        return clinician;
    }

    public void setClinician(Clinician clinician) {
        this.clinician = clinician;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Created getCreated() {
        return created;
    }

    public void setCreated(Created created) {
        this.created = created;
    }

    public Object getUpdated() {
        return updated;
    }

    public void setUpdated(Object updated) {
        this.updated = updated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Object getTimespent() {
        return timespent;
    }

    public void setTimespent(Object timespent) {
        this.timespent = timespent;
    }

    public Readmission getReadmission() {
        return readmission;
    }

    public void setReadmission(Readmission readmission) {
        this.readmission = readmission;
    }

    public Edvisit getEdvisit() {
        return edvisit;
    }

    public void setEdvisit(Edvisit edvisit) {
        this.edvisit = edvisit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientNote that = (PatientNote) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(patient, that.patient) &&
                Objects.equals(clinician, that.clinician) &&
                Objects.equals(note, that.note) &&
                Objects.equals(created, that.created) &&
                Objects.equals(updated, that.updated) &&
                Objects.equals(status, that.status) &&
                Objects.equals(time, that.time) &&
                Objects.equals(timespent, that.timespent) &&
                Objects.equals(readmission, that.readmission) &&
                Objects.equals(edvisit, that.edvisit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, patient, clinician, note, created, updated, status, time, timespent, readmission, edvisit);
    }

    @Override
    public String toString() {
        return "PatientNote{" +
                "id=" + id +
                ", patient='" + patient + '\'' +
                ", clinician=" + clinician +
                ", note='" + note + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                ", status='" + status + '\'' +
                ", time='" + time + '\'' +
                ", timespent=" + timespent +
                ", readmission=" + readmission +
                ", edvisit=" + edvisit +
                '}';
    }
}

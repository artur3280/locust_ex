package com.hrs.cc.api.models.requests.set_patient_metricks;

import java.util.Objects;

public class NewMetricks {
    private ModuleInfo moduleInfo;

    public NewMetricks(ModuleInfo moduleInfo) {
        this.moduleInfo = moduleInfo;
    }

    public NewMetricks() {
    }

    public ModuleInfo getModuleInfo() {
        return moduleInfo;
    }

    public void setModuleInfo(ModuleInfo moduleInfo) {
        this.moduleInfo = moduleInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewMetricks that = (NewMetricks) o;
        return Objects.equals(moduleInfo, that.moduleInfo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(moduleInfo);
    }

    @Override
    public String toString() {
        return "NewMetricks{" +
                "moduleInfo=" + moduleInfo +
                '}';
    }
}

package com.hrs.cc.api.models.patients.dashboard_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "changeid",
        "patienthrsid",
        "newclinicianhrsid",
        "previousclinicianhrsid"
})
public class ClinicianChange {
    @JsonProperty("changeid")
    private String changeid;
    @JsonProperty("patienthrsid")
    private String patienthrsid;
    @JsonProperty("newclinicianhrsid")
    private String newclinicianhrsid;
    @JsonProperty("previousclinicianhrsid")
    private String previousclinicianhrsid;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public ClinicianChange() {
    }

    public ClinicianChange(String changeid, String patienthrsid, String newclinicianhrsid, String previousclinicianhrsid) {
        this.changeid = changeid;
        this.patienthrsid = patienthrsid;
        this.newclinicianhrsid = newclinicianhrsid;
        this.previousclinicianhrsid = previousclinicianhrsid;
    }

    public String getChangeid() {
        return changeid;
    }

    public void setChangeid(String changeid) {
        this.changeid = changeid;
    }

    public String getPatienthrsid() {
        return patienthrsid;
    }

    public void setPatienthrsid(String patienthrsid) {
        this.patienthrsid = patienthrsid;
    }

    public String getNewclinicianhrsid() {
        return newclinicianhrsid;
    }

    public void setNewclinicianhrsid(String newclinicianhrsid) {
        this.newclinicianhrsid = newclinicianhrsid;
    }

    public String getPreviousclinicianhrsid() {
        return previousclinicianhrsid;
    }

    public void setPreviousclinicianhrsid(String previousclinicianhrsid) {
        this.previousclinicianhrsid = previousclinicianhrsid;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClinicianChange that = (ClinicianChange) o;
        return Objects.equals(changeid, that.changeid) &&
                Objects.equals(patienthrsid, that.patienthrsid) &&
                Objects.equals(newclinicianhrsid, that.newclinicianhrsid) &&
                Objects.equals(previousclinicianhrsid, that.previousclinicianhrsid) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(changeid, patienthrsid, newclinicianhrsid, previousclinicianhrsid, additionalProperties);
    }

    @Override
    public String toString() {
        return "ClinicianChange{" +
                "changeid='" + changeid + '\'' +
                ", patienthrsid='" + patienthrsid + '\'' +
                ", newclinicianhrsid='" + newclinicianhrsid + '\'' +
                ", previousclinicianhrsid='" + previousclinicianhrsid + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

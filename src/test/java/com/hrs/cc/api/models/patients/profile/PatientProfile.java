package com.hrs.cc.api.models.patients.profile;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "profile",
        "risk",
        "metrics",
        "tracking",
        "day",
        "ts",
        "lastActivatedDate"
})
public class PatientProfile<T> {

    @JsonProperty("profile")
    private Profile profile;
    @JsonProperty("risk")
    private List<Object> risk = null;
    @JsonProperty("metrics")
    private T metrics = null;
    @JsonProperty("tracking")
    private Tracking tracking;
    @JsonProperty("day")
    private Integer day;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("lastActivatedDate")
    private Long lastActivatedDate;
    @JsonProperty("consentFormAvailable")
    private Boolean consentFormAvailable;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public PatientProfile() {
    }

    public PatientProfile(Profile profile, List<Object> risk, T metrics, Tracking tracking, Integer day, Long ts, Boolean consentFormAvailable, Long lastActivatedDate) {
        this.profile = profile;
        this.risk = risk;
        this.metrics = metrics;
        this.tracking = tracking;
        this.day = day;
        this.ts = ts;
        this.consentFormAvailable = consentFormAvailable;
        this.lastActivatedDate = lastActivatedDate;
    }

    public Boolean getConsentFormAvailable() {
        return consentFormAvailable;
    }

    public void setConsentFormAvailable(Boolean consentFormAvailable) {
        this.consentFormAvailable = consentFormAvailable;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<Object> getRisk() {
        return risk;
    }

    public void setRisk(List<Object> risk) {
        this.risk = risk;
    }

    public T getMetrics() {
        return metrics;
    }

    public void setMetrics(T metrics) {
        this.metrics = metrics;
    }

    public Tracking getTracking() {
        return tracking;
    }

    public void setTracking(Tracking tracking) {
        this.tracking = tracking;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public Long getLastActivatedDate() {
        return lastActivatedDate;
    }

    public void setLastActivatedDate(Long lastActivatedDate) {
        this.lastActivatedDate = lastActivatedDate;
    }

    @Override
    public String toString() {
        return "PatientProfile{" +
                "profile=" + profile +
                ", risk=" + risk +
                ", metrics=" + metrics +
                ", tracking=" + tracking +
                ", day=" + day +
                ", ts=" + ts +
                ", lastActivatedDate=" + lastActivatedDate +
                ", consentFormAvailable=" + consentFormAvailable +
                '}';
    }
}

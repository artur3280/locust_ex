package com.hrs.cc.api.models.inventory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "timezone_type",
        "timezone"
})
public class CreatedAt {

    @JsonProperty("date")
    private String date;
    @JsonProperty("timezone_type")
    private Integer timezone_type;
    @JsonProperty("timezone")
    private String timezone;

    public CreatedAt() {
    }

    public CreatedAt(String date, Integer timezone_type, String timezone) {
        this.date = date;
        this.timezone_type = timezone_type;
        this.timezone = timezone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTimezone_type() {
        return timezone_type;
    }

    public void setTimezone_type(Integer timezone_type) {
        this.timezone_type = timezone_type;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @Override
    public String toString() {
        return "CreatedAt{" +
                "date='" + date + '\'' +
                ", timezone_type=" + timezone_type +
                ", timezone='" + timezone + '\'' +
                '}';
    }
}

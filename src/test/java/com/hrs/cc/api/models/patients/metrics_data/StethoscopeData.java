package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "takenAt",
        "imageUrl",
        "soundUrl"
})
public class StethoscopeData {

    @JsonProperty("id")
    private String id;
    @JsonProperty("takenAt")
    private String takenAt;
    @JsonProperty("imageUrl")
    private String imageUrl;
    @JsonProperty("soundUrl")
    private String soundUrl;

    public StethoscopeData() {
    }

    public StethoscopeData(String id, String takenAt, String imageUrl, String soundUrl) {
        this.id = id;
        this.takenAt = takenAt;
        this.imageUrl = imageUrl;
        this.soundUrl = soundUrl;
    }

    public String getTakenAt() {
        return takenAt;
    }

    public void setTakenAt(String takenAt) {
        this.takenAt = takenAt;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSoundUrl() {
        return soundUrl;
    }

    public void setSoundUrl(String soundUrl) {
        this.soundUrl = soundUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "StethoscopeData{" +
                "id='" + id + '\'' +
                ", takenAt='" + takenAt + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", soundUrl='" + soundUrl + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.environment.data.environment_settings_type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "alerttype",
        "type",
        "events",
        "phone",
        "value"
})
public class Riskdata {
    @JsonProperty("alerttype")
    private String alerttype;
    @JsonProperty("type")
    private String type;
    @JsonProperty("events")
    private List<String> events = null;
    @JsonProperty("value")
    private String value;
    @JsonProperty("phone")
    private String phone;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Riskdata() {
    }

    public Riskdata(String alerttype, String type, List<String> events, String value, String phone) {
        this.alerttype = alerttype;
        this.type = type;
        this.events = events;
        this.value = value;
        this.phone = phone;
    }

    public String getAlerttype() {
        return alerttype;
    }

    public void setAlerttype(String alerttype) {
        this.alerttype = alerttype;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getEvents() {
        return events;
    }

    public void setEvents(List<String> events) {
        this.events = events;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Riskdata riskdata = (Riskdata) o;
        return Objects.equals(alerttype, riskdata.alerttype) &&
                Objects.equals(type, riskdata.type) &&
                Objects.equals(events, riskdata.events) &&
                Objects.equals(value, riskdata.value) &&
                Objects.equals(phone, riskdata.phone) &&
                Objects.equals(additionalProperties, riskdata.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(alerttype, type, events, value, phone, additionalProperties);
    }

    @Override
    public String toString() {
        return "Riskdata{" +
                "alerttype='" + alerttype + '\'' +
                ", type='" + type + '\'' +
                ", events=" + events +
                ", value='" + value + '\'' +
                ", phone='" + phone + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

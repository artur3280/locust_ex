package com.hrs.cc.api.models.patients.notes.history;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "clinician",
        "readmission",
        "edvisit",
        "observation",
        "advanced",
        "timespan",
        "id",
        "noteId",
        "patient",
        "note",
        "insertTime",
        "editTime"
})
public class HistoryNote {

    @JsonProperty("clinician")
    private Clinician clinician;
    @JsonProperty("readmission")
    private Readmission readmission;
    @JsonProperty("edvisit")
    private Edvisit edvisit;
    @JsonProperty("observation")
    private Observation observation;
    @JsonProperty("advanced")
    private Object advanced;
    @JsonProperty("timespan")
    private Timespan timespan;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("noteId")
    private String noteId;
    @JsonProperty("patient")
    private String patient;
    @JsonProperty("note")
    private String note;
    @JsonProperty("insertTime")
    private InsertTime insertTime;
    @JsonProperty("editTime")
    private EditTime editTime;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public HistoryNote() {
    }

    public HistoryNote(Clinician clinician, Readmission readmission, Edvisit edvisit, Observation observation, Object advanced, Timespan timespan, Integer id, String noteId, String patient, String note, InsertTime insertTime, EditTime editTime) {
        this.clinician = clinician;
        this.readmission = readmission;
        this.edvisit = edvisit;
        this.observation = observation;
        this.advanced = advanced;
        this.timespan = timespan;
        this.id = id;
        this.noteId = noteId;
        this.patient = patient;
        this.note = note;
        this.insertTime = insertTime;
        this.editTime = editTime;
    }

    public Clinician getClinician() {
        return clinician;
    }

    public void setClinician(Clinician clinician) {
        this.clinician = clinician;
    }

    public Readmission getReadmission() {
        return readmission;
    }

    public void setReadmission(Readmission readmission) {
        this.readmission = readmission;
    }

    public Edvisit getEdvisit() {
        return edvisit;
    }

    public void setEdvisit(Edvisit edvisit) {
        this.edvisit = edvisit;
    }

    public Observation getObservation() {
        return observation;
    }

    public void setObservation(Observation observation) {
        this.observation = observation;
    }

    public Object getAdvanced() {
        return advanced;
    }

    public void setAdvanced(Object advanced) {
        this.advanced = advanced;
    }

    public Timespan getTimespan() {
        return timespan;
    }

    public void setTimespan(Timespan timespan) {
        this.timespan = timespan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public InsertTime getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(InsertTime insertTime) {
        this.insertTime = insertTime;
    }

    public EditTime getEditTime() {
        return editTime;
    }

    public void setEditTime(EditTime editTime) {
        this.editTime = editTime;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoryNote that = (HistoryNote) o;
        return Objects.equals(clinician, that.clinician) &&
                Objects.equals(readmission, that.readmission) &&
                Objects.equals(edvisit, that.edvisit) &&
                Objects.equals(observation, that.observation) &&
                Objects.equals(advanced, that.advanced) &&
                Objects.equals(timespan, that.timespan) &&
                Objects.equals(id, that.id) &&
                Objects.equals(noteId, that.noteId) &&
                Objects.equals(patient, that.patient) &&
                Objects.equals(note, that.note) &&
                Objects.equals(insertTime, that.insertTime) &&
                Objects.equals(editTime, that.editTime) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(clinician, readmission, edvisit, observation, advanced, timespan, id, noteId, patient, note, insertTime, editTime, additionalProperties);
    }

    @Override
    public String toString() {
        return "HistoryNote{" +
                "clinician=" + clinician +
                ", readmission=" + readmission +
                ", edvisit=" + edvisit +
                ", observation=" + observation +
                ", advanced=" + advanced +
                ", timespan=" + timespan +
                ", id=" + id +
                ", noteId='" + noteId + '\'' +
                ", patient='" + patient + '\'' +
                ", note='" + note + '\'' +
                ", insertTime=" + insertTime +
                ", editTime=" + editTime +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

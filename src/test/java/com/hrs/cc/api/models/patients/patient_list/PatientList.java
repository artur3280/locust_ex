package com.hrs.cc.api.models.patients.patient_list;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "discharged",
        "profile",
        "risk",
        "metrics",
        "tracking",
        "day",
        "ts",
        "consentFormAvailable",
        "lastActivatedDate"
})
public class PatientList<T> {
    @JsonProperty("discharged")
    private String discharged;
    @JsonProperty("profile")
    private Profile profile;
    @JsonProperty("risk")
    private List<Risk> risk = null;
    @JsonProperty("metrics")
    private T metrics;
    @JsonProperty("tracking")
    private Tracking tracking;
    @JsonProperty("day")
    private Integer day;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("lastActivatedDate")
    private Long lastActivatedDate;
    @JsonProperty("consentFormAvailable")
    private Boolean consentFormAvailable;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private ArrayList<T> ts1;
    private ObjectMapper mapper = new ObjectMapper();

    public PatientList() {
    }

    public PatientList(String discharged, Profile profile, List<Risk> risk, T metrics, Tracking tracking, Integer day, Long ts, Boolean consentFormAvailable, Long lastActivatedDate) {
        this.discharged = discharged;
        this.profile = profile;
        this.risk = risk;
        this.metrics = metrics;
        this.tracking = tracking;
        this.day = day;
        this.ts = ts;
        this.consentFormAvailable = consentFormAvailable;
        this.lastActivatedDate = lastActivatedDate;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<Risk> getRisk() {
        return risk;
    }

    public void setRisk(List<Risk> risk) {
        this.risk = risk;
    }

    public T getMetrics() {

        return metrics;
    }

    public Metrics getMetricsAsObject() {
        Metrics metricsOb = null;
        if (!(getMetrics() instanceof ArrayList)) {
            metricsOb = mapper.convertValue(getMetrics(), Metrics.class);
        }
        return metricsOb;
    }


    public void setMetrics(T metrics) {
        this.metrics = metrics;
    }

    public Tracking getTracking() {
        return tracking;
    }

    public void setTracking(Tracking tracking) {
        this.tracking = tracking;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public Boolean getConsentFormAvailable() {
        return consentFormAvailable;
    }

    public void setConsentFormAvailable(Boolean consentFormAvailable) {
        this.consentFormAvailable = consentFormAvailable;
    }

    public String getDischarged() {
        return discharged;
    }

    public void setDischarged(String discharged) {
        this.discharged = discharged;
    }

    public Long getLastActivatedDate() {
        return lastActivatedDate;
    }

    public void setLastActivatedDate(Long lastActivatedDate) {
        this.lastActivatedDate = lastActivatedDate;
    }

    @Override
    public String toString() {
        return "PatientList{" +
                "discharged='" + discharged + '\'' +
                ", profile=" + profile +
                ", risk=" + risk +
                ", metrics=" + metrics +
                ", tracking=" + tracking +
                ", day=" + day +
                ", ts=" + ts +
                ", lastActivatedDate=" + lastActivatedDate +
                ", consentFormAvailable=" + consentFormAvailable +
                ", additionalProperties=" + additionalProperties +
                ", ts1=" + ts1 +
                ", mapper=" + mapper +
                '}';
    }
}

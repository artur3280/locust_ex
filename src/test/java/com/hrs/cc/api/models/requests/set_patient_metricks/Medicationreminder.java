package com.hrs.cc.api.models.requests.set_patient_metricks;

import java.util.Objects;

public class Medicationreminder {
    private String medication;
    private String dose;
    private Schedule schedule;
    private String times = "PRN";
    private String time = "PRN";
    private String window = "30";

    public Medicationreminder() {
    }

    public Medicationreminder(String medication, String dose, Schedule schedule, String times, String time, String window) {
        this.medication = medication;
        this.dose = dose;
        this.schedule = schedule;
        this.times = times;
        this.time = time;
        this.window = window;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWindow() {
        return window;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Medicationreminder that = (Medicationreminder) o;
        return Objects.equals(medication, that.medication) &&
                Objects.equals(dose, that.dose) &&
                Objects.equals(schedule, that.schedule) &&
                Objects.equals(times, that.times) &&
                Objects.equals(time, that.time) &&
                Objects.equals(window, that.window);
    }

    @Override
    public int hashCode() {

        return Objects.hash(medication, dose, schedule, times, time, window);
    }

    @Override
    public String toString() {
        return "Medicationreminder{" +
                "medication='" + medication + '\'' +
                ", dose='" + dose + '\'' +
                ", schedule=" + schedule +
                ", times='" + times + '\'' +
                ", time='" + time + '\'' +
                ", window='" + window + '\'' +
                '}';
    }
}

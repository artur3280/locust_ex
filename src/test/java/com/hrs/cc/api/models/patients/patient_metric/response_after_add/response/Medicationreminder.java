package com.hrs.cc.api.models.patients.patient_metric.response_after_add.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "medication",
        "dose",
        "schedule",
        "time",
        "window"
})
public class Medicationreminder {

    @JsonProperty("medication")
    private String medication;
    @JsonProperty("dose")
    private String dose;
    @JsonProperty("schedule")
    private Schedule schedule;
    @JsonProperty("time")
    private String time;
    @JsonProperty("window")
    private String window;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Medicationreminder() {
    }

    public Medicationreminder(String medication, String dose, Schedule schedule, String time, String window) {
        this.medication = medication;
        this.dose = dose;
        this.schedule = schedule;
        this.time = time;
        this.window = window;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWindow() {
        return window;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Medicationreminder that = (Medicationreminder) o;
        return Objects.equals(medication, that.medication) &&
                Objects.equals(dose, that.dose) &&
                Objects.equals(schedule, that.schedule) &&
                Objects.equals(time, that.time) &&
                Objects.equals(window, that.window) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(medication, dose, schedule, time, window, additionalProperties);
    }

    @Override
    public String toString() {
        return "Medicationreminder{" +
                "medication='" + medication + '\'' +
                ", dose='" + dose + '\'' +
                ", schedule=" + schedule +
                ", time='" + time + '\'' +
                ", window='" + window + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

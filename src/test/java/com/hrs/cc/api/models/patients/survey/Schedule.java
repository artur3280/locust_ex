package com.hrs.cc.api.models.patients.survey;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "x",
        "startday",
        "schedule"
})
public class Schedule {

    @JsonProperty("type")
    private String type;
    @JsonProperty("x")
    private String x;
    @JsonProperty("startday")
    private String startday;
    @JsonProperty("schedule")
    private String schedule;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Schedule() {
    }

    public Schedule(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return Objects.equals(type, schedule.type) &&
                Objects.equals(additionalProperties, schedule.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, additionalProperties);
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "type='" + type + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

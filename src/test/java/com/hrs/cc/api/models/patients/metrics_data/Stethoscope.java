package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class Stethoscope {

    @JsonProperty("data")
    private List<StethoscopeData> data;

    public Stethoscope() {
    }

    public Stethoscope(List<StethoscopeData> data) {
        this.data = data;
    }

    public List<StethoscopeData> getData() {
        return data;
    }

    public void setData(List<StethoscopeData> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Stethoscope{" +
                "data=" + data +
                '}';
    }
}
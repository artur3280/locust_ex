package com.hrs.cc.api.models.reports.patients;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "patient",
        "pid",
        "name",
        "dob",
        "gender",
        "phone",
        "status",
        "device"
})
public class Datum {

    @JsonProperty("patient")
    private String patient;
    @JsonProperty("pid")
    private String pid;
    @JsonProperty("name")
    private Name name;
    @JsonProperty("dob")
    private String dob;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("status")
    private String status;
    @JsonProperty("device")
    private String device;

    public Datum(String patient, String pid, Name name, String dob, String gender, String phone, String status, String device) {
        this.patient = patient;
        this.pid = pid;
        this.name = name;
        this.dob = dob;
        this.gender = gender;
        this.phone = phone;
        this.status = status;
        this.device = device;
    }

    public Datum() {
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Datum datum = (Datum) o;
        return Objects.equals(patient, datum.patient) &&
                Objects.equals(pid, datum.pid) &&
                Objects.equals(name, datum.name) &&
                Objects.equals(dob, datum.dob) &&
                Objects.equals(gender, datum.gender) &&
                Objects.equals(phone, datum.phone) &&
                Objects.equals(status, datum.status) &&
                Objects.equals(device, datum.device);
    }

    @Override
    public int hashCode() {
        return Objects.hash(patient, pid, name, dob, gender, phone, status, device);
    }

    @Override
    public String toString() {
        return "DatumTask{" +
                "patient='" + patient + '\'' +
                ", pid='" + pid + '\'' +
                ", name=" + name +
                ", dob='" + dob + '\'' +
                ", gender='" + gender + '\'' +
                ", phone='" + phone + '\'' +
                ", status='" + status + '\'' +
                ", device='" + device + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "systolic",
        "diastolic",
        "heartrate",
        "deleted",
        "id",
        "type",
        "ts",
        "status",
        "risk"
})
public class Datum_____ {

    @JsonProperty("systolic")
    private Integer systolic;
    @JsonProperty("diastolic")
    private Integer diastolic;
    @JsonProperty("heartrate")
    private Integer heartrate;
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("status")
    private String status;
    @JsonProperty("risk")
    private String risk;


    public Datum_____() {
    }

    public Datum_____(Integer systolic, Integer diastolic, Integer heartrate, String id, String type, Boolean deleted, Long ts, String status, String risk) {
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.heartrate = heartrate;
        this.id = id;
        this.type = type;
        this.deleted = deleted;
        this.ts = ts;
        this.status = status;
        this.risk = risk;
    }

    public Integer getSystolic() {
        return systolic;
    }

    public void setSystolic(Integer systolic) {
        this.systolic = systolic;
    }

    public Integer getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(Integer diastolic) {
        this.diastolic = diastolic;
    }

    public Integer getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(Integer heartrate) {
        this.heartrate = heartrate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Datum_____{" +
                "systolic=" + systolic +
                ", diastolic=" + diastolic +
                ", heartrate=" + heartrate +
                ", id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", deleted=" + deleted +
                ", ts=" + ts +
                ", status='" + status + '\'' +
                ", risk='" + risk + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.integratioins_app.quizzes;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "answers",
        "explanation",
        "text"
})
public class Question {

    @JsonProperty("answers")
    private List<Answer> answers = null;
    @JsonProperty("explanation")
    private String explanation;
    @JsonProperty("text")
    private String text;

    public Question() {
    }

    public Question(List<Answer> answers, String explanation, String text) {
        this.answers = answers;
        this.explanation = explanation;
        this.text = text;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Question{" +
                "answers=" + answers +
                ", explanation='" + explanation + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}

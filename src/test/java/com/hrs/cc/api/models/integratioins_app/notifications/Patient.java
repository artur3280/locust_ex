package com.hrs.cc.api.models.integratioins_app.notifications;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "firstName",
        "lastName",
        "environment",
        "middleName",
        "hrsId",
        "id"
})
public class Patient {

    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("environment")
    private String environment;
    @JsonProperty("middleName")
    private String middleName;
    @JsonProperty("hrsId")
    private String hrsId;
    @JsonProperty("id")
    private String id;

    public Patient() {
    }

    public Patient(String firstName, String lastName, String environment, String middleName, String hrsId, String id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.environment = environment;
        this.middleName = middleName;
        this.hrsId = hrsId;
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getHrsId() {
        return hrsId;
    }

    public void setHrsId(String hrsId) {
        this.hrsId = hrsId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", environment='" + environment + '\'' +
                ", middleName='" + middleName + '\'' +
                ", hrsId='" + hrsId + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}

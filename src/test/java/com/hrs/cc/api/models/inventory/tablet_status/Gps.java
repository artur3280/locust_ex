package com.hrs.cc.api.models.inventory.tablet_status;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "latitude",
        "time",
        "longitude"
})
public class Gps {

    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("time")
    private Long time;
    @JsonProperty("longitude")
    private Double longitude;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Gps() {
    }

    public Gps(Double latitude, Long time, Double longitude) {
        this.latitude = latitude;
        this.time = time;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gps gps = (Gps) o;
        return Objects.equals(latitude, gps.latitude) &&
                Objects.equals(time, gps.time) &&
                Objects.equals(longitude, gps.longitude) &&
                Objects.equals(additionalProperties, gps.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(latitude, time, longitude, additionalProperties);
    }

    @Override
    public String toString() {
        return "Gps{" +
                "latitude=" + latitude +
                ", time=" + time +
                ", longitude=" + longitude +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.patients.syrvey;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "surveydata"
})
public class Data {

    @JsonProperty("surveydata")
    private List<Surveydatum> surveydata = null;

    public Data() {
    }

    public Data(List<Surveydatum> surveydata) {
        this.surveydata = surveydata;
    }

    public List<Surveydatum> getSurveydata() {
        return surveydata;
    }

    public void setSurveydata(List<Surveydatum> surveydata) {
        this.surveydata = surveydata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return Objects.equals(surveydata, data.surveydata);
    }

    @Override
    public int hashCode() {
        return Objects.hash(surveydata);
    }

    @Override
    public String toString() {
        return "Data{" +
                "surveydata=" + surveydata +
                '}';
    }
}

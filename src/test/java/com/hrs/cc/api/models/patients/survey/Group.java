package com.hrs.cc.api.models.patients.survey;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "questionids",
        "scoretype",
        "scoredisplay"
})
public class Group {

    @JsonProperty("name")
    private String name;
    @JsonProperty("questionids")
    private List<Integer> questionids = null;
    @JsonProperty("scoretype")
    private String scoretype;
    @JsonProperty("scoredisplay")
    private List<ScoreDisplay> scoredisplay = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Group() {
    }

    public Group(String name, List<Integer> questionids, String scoretype, List<ScoreDisplay> scoredisplay) {
        this.name = name;
        this.questionids = questionids;
        this.scoretype = scoretype;
        this.scoredisplay = scoredisplay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getQuestionids() {
        return questionids;
    }

    public void setQuestionids(List<Integer> questionids) {
        this.questionids = questionids;
    }

    public String getScoretype() {
        return scoretype;
    }

    public void setScoretype(String scoretype) {
        this.scoretype = scoretype;
    }

    public List<ScoreDisplay> getScoredisplay() {
        return scoredisplay;
    }

    public void setScoredisplay(List<ScoreDisplay> scoredisplay) {
        this.scoredisplay = scoredisplay;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(name, group.name) &&
                Objects.equals(questionids, group.questionids) &&
                Objects.equals(scoretype, group.scoretype) &&
                Objects.equals(scoredisplay, group.scoredisplay) &&
                Objects.equals(additionalProperties, group.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, questionids, scoretype, scoredisplay, additionalProperties);
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", questionids=" + questionids +
                ", scoretype='" + scoretype + '\'' +
                ", scoredisplay=" + scoredisplay +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

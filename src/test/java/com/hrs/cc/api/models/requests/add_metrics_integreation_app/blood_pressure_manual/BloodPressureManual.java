package com.hrs.cc.api.models.requests.add_metrics_integreation_app.blood_pressure_manual;

public class BloodPressureManual {
    private DataBP data;

    /**
     * No args constructor for use in serialization
     *
     */
    public BloodPressureManual() {
    }

    /**
     *
     * @param data
     */
    public BloodPressureManual(DataBP data) {
        super();
        this.data = data;
    }

    public DataBP getData() {
        return data;
    }

    public void setData(DataBP data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BloodPressureManual{" +
                "data=" + data +
                '}';
    }
}

package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "lastactive"
})
public class Tracking {

    @JsonProperty("data")
    private Data data;
    @JsonProperty("lastactive")
    private Long lastactive;

    public Tracking() {
    }

    public Tracking(Data data) {
        this.data = data;
    }

    public Tracking(Data data, Long lastactive) {
        this.data = data;
        this.lastactive = lastactive;
    }

    public Data getData() {
        return data;
    }

    public Long getLastactive() {
        return lastactive;
    }

    public void setLastactive(Long lastactive) {
        this.lastactive = lastactive;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tracking tracking = (Tracking) o;
        return Objects.equals(data, tracking.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "Tracking{" +
                "data=" + data +
                '}';
    }
}

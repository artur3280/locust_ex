package com.hrs.cc.api.models.requests.add_metrics_integreation_app.pulseox_manual;


import com.hrs.cc.api.models.requests.add_metrics_integreation_app.survey_manual.DataSurvey;

public class PulseOxManual {
    private DataPulseOx data;

    /**
     * No args constructor for use in serialization
     */
    public PulseOxManual() {
    }

    /**
     * @param data
     */
    public PulseOxManual(DataPulseOx data) {
        super();
        this.data = data;
    }

    public DataPulseOx getData() {
        return data;
    }

    public void setData(DataPulseOx data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PulseOxManual{" +
                "data=" + data +
                '}';
    }
}

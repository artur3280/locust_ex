package com.hrs.cc.api.models.requests.set_patient_note;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Note {
    private Readmission readmission;
    private Edvisit edvisit;
    private Observation observation;
    private String note;
    private Communication communication;
    private Enrollment enrollment;
    private Integer timespentHours = 1;
    private Integer timespentMinutes = 23;
    private Integer timespentSeconds = 40;
    private Boolean isReadmission = true;
    private Boolean isEDVisit = true;
    private Boolean isObservation = true;
    private Long timespent = 3627L;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public Note(Readmission readmission, Edvisit edvisit, Observation observation, String note, Communication communication, Enrollment enrollment, Integer timespentHours, Integer timespentMinutes, Integer timespentSeconds, Boolean isReadmission, Boolean isEDVisit, Boolean isObservation, Long timespent) {
        this.readmission = readmission;
        this.edvisit = edvisit;
        this.observation = observation;
        this.note = note;
        this.communication = communication;
        this.enrollment = enrollment;
        this.timespentHours = timespentHours;
        this.timespentMinutes = timespentMinutes;
        this.timespentSeconds = timespentSeconds;
        this.isReadmission = isReadmission;
        this.isEDVisit = isEDVisit;
        this.isObservation = isObservation;
        this.timespent = timespent;
    }

    public Note() {
    }

    public Readmission getReadmission() {
        return readmission;
    }

    public void setReadmission(Boolean readmission) {
        isReadmission = readmission;
    }

    public Boolean getVisit() {
        return isEDVisit;
    }

    public void setVisit(Boolean visit) {
        isEDVisit = visit;
    }

    public void setReadmission(Readmission readmission) {
        this.readmission = readmission;
    }

    public Edvisit getEdvisit() {
        return edvisit;
    }

    public void setEdvisit(Edvisit edvisit) {
        this.edvisit = edvisit;
    }

    public Observation getObservation() {
        return observation;
    }

    public void setObservation(Boolean observation) {
        isObservation = observation;
    }

    public Long getTimespent() {
        return timespent;
    }

    public void setTimespent(Long timespent) {
        this.timespent = timespent;
    }

    public void setObservation(Observation observation) {
        this.observation = observation;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Communication getCommunication() {
        return communication;
    }

    public void setCommunication(Communication communication) {
        this.communication = communication;
    }

    public Enrollment getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(Enrollment enrollment) {
        this.enrollment = enrollment;
    }

    public Integer getTimespentHours() {
        return timespentHours;
    }

    public void setTimespentHours(Integer timespentHours) {
        this.timespentHours = timespentHours;
    }

    public Integer getTimespentMinutes() {
        return timespentMinutes;
    }

    public void setTimespentMinutes(Integer timespentMinutes) {
        this.timespentMinutes = timespentMinutes;
    }

    public Integer getTimespentSeconds() {
        return timespentSeconds;
    }

    public void setTimespentSeconds(Integer timespentSeconds) {
        this.timespentSeconds = timespentSeconds;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Note note1 = (Note) o;
        return Objects.equals(readmission, note1.readmission) &&
                Objects.equals(edvisit, note1.edvisit) &&
                Objects.equals(observation, note1.observation) &&
                Objects.equals(note, note1.note) &&
                Objects.equals(communication, note1.communication) &&
                Objects.equals(enrollment, note1.enrollment) &&
                Objects.equals(timespentHours, note1.timespentHours) &&
                Objects.equals(timespentMinutes, note1.timespentMinutes) &&
                Objects.equals(timespentSeconds, note1.timespentSeconds) &&
                Objects.equals(isReadmission, note1.isReadmission) &&
                Objects.equals(isEDVisit, note1.isEDVisit) &&
                Objects.equals(isObservation, note1.isObservation) &&
                Objects.equals(timespent, note1.timespent) &&
                Objects.equals(additionalProperties, note1.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(readmission, edvisit, observation, note, communication, enrollment, timespentHours, timespentMinutes, timespentSeconds, isReadmission, isEDVisit, isObservation, timespent, additionalProperties);
    }

    @Override
    public String toString() {
        return "Note{" +
                "readmission=" + readmission +
                ", edvisit=" + edvisit +
                ", observation=" + observation +
                ", note='" + note + '\'' +
                ", communication=" + communication +
                ", enrollment=" + enrollment +
                ", timespentHours=" + timespentHours +
                ", timespentMinutes=" + timespentMinutes +
                ", timespentSeconds=" + timespentSeconds +
                ", isReadmission=" + isReadmission +
                ", isEDVisit=" + isEDVisit +
                ", isObservation=" + isObservation +
                ", timespent=" + timespent +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.patients.patient_status;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "lastUpdated"
})
public class PatientStatus {

    @JsonProperty("status")
    private String status;
    @JsonProperty("lastUpdated")
    private LastUpdated lastUpdated;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public PatientStatus() {
    }

    public PatientStatus(String status, LastUpdated lastUpdated) {
        this.status = status;
        this.lastUpdated = lastUpdated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LastUpdated getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LastUpdated lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientStatus that = (PatientStatus) o;
        return Objects.equals(status, that.status) &&
                Objects.equals(lastUpdated, that.lastUpdated) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(status, lastUpdated, additionalProperties);
    }

    @Override
    public String toString() {
        return "PatientStatus{" +
                "status='" + status + '\'' +
                ", lastUpdated=" + lastUpdated +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

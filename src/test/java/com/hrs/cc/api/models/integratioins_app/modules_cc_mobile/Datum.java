package com.hrs.cc.api.models.integratioins_app.modules_cc_mobile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "attributes",
        "id",
        "resourceType"
})
public class Datum {

    @JsonProperty("attributes")
    private Attributes attributes;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("resourceType")
    private String resourceType;

    public Datum() {
    }

    public Datum(Attributes attributes, Integer id, String resourceType) {
        this.attributes = attributes;
        this.id = id;
        this.resourceType = resourceType;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "attributes=" + attributes +
                ", id=" + id +
                ", resourceType='" + resourceType + '\'' +
                '}';
    }
}

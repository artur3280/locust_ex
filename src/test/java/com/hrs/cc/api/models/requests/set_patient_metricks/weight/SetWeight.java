package com.hrs.cc.api.models.requests.set_patient_metricks.weight;

import java.util.Objects;

public class SetWeight {

    private String type = "weight";
    private String reason;
    private String ftime;
    private String rtime;
    private Integer weight;

    public SetWeight() {
    }

    public SetWeight(String reason, String ftime, String rtime, Integer weight) {
        this.reason = reason;
        this.ftime = ftime;
        this.rtime = rtime;
        this.weight = weight;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFtime() {
        return ftime;
    }

    public void setFtime(String ftime) {
        this.ftime = ftime;
    }

    public String getRtime() {
        return rtime;
    }

    public void setRtime(String rtime) {
        this.rtime = rtime;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SetWeight setWeight = (SetWeight) o;
        return Objects.equals(type, setWeight.type) &&
                Objects.equals(reason, setWeight.reason) &&
                Objects.equals(ftime, setWeight.ftime) &&
                Objects.equals(rtime, setWeight.rtime) &&
                Objects.equals(weight, setWeight.weight);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, reason, ftime, rtime, weight);
    }

    @Override
    public String
    toString() {
        return "SetWeight{" +
                "type='" + type + '\'' +
                ", reason='" + reason + '\'' +
                ", ftime='" + ftime + '\'' +
                ", rtime='" + rtime + '\'' +
                ", weight=" + weight +
                '}';
    }
}

package com.hrs.cc.api.models.admin.surveys_types;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "best",
        "worst",
        "values"
})
public class Type {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("best")
    private String best;
    @JsonProperty("worst")
    private String worst;
    @JsonProperty("values")
    private List<Value> values = null;

    public Type() {
    }

    public Type(String id, String name, String best, String worst, List<Value> values) {
        this.id = id;
        this.name = name;
        this.best = best;
        this.worst = worst;
        this.values = values;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBest() {
        return best;
    }

    public void setBest(String best) {
        this.best = best;
    }

    public String getWorst() {
        return worst;
    }

    public void setWorst(String worst) {
        this.worst = worst;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "Type{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", best='" + best + '\'' +
                ", worst='" + worst + '\'' +
                ", values=" + values +
                '}';
    }
}

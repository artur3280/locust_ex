package com.hrs.cc.api.models.integratioins_app.user_by_type_in_env;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "meta"
})
public class UsersByTypeInEnv {

    @JsonProperty("data")
    private List<Datum> data = null;
    @JsonProperty("meta")
    private Meta meta;

    public UsersByTypeInEnv() {
    }

    public UsersByTypeInEnv(List<Datum> data, Meta meta) {
        this.data = data;
        this.meta = meta;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public String toString() {
        return "UsersByTypeInEnv{" +
                "data=" + data +
                ", meta=" + meta +
                '}';
    }
}

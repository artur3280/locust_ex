package com.hrs.cc.api.models.patients.dashboard_list.riskdetails;

import com.hrs.cc.api.models.patients.dashboard_list.riskdetails.details.Detail;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "details",
        "modules"
})
public class RiskDetails {

    @JsonProperty("details")
    private List<Detail> details = null;
    @JsonProperty("modules")
    private List<String> modules = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public RiskDetails() {
    }

    public RiskDetails(List<Detail> details, List<String> modules) {
        this.details = details;
        this.modules = modules;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RiskDetails that = (RiskDetails) o;
        return Objects.equals(details, that.details) &&
                Objects.equals(modules, that.modules) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(details, modules, additionalProperties);
    }

    @Override
    public String toString() {
        return "RiskDetails{" +
                "details=" + details +
                ", modules=" + modules +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

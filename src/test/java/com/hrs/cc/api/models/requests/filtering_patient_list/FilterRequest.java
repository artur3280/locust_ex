package com.hrs.cc.api.models.requests.filtering_patient_list;

import com.hrs.cc.api.models.requests.filtering_patient_list.filtering.*;

import java.util.Objects;

public class FilterRequest {
    private FilterFirstName filterFirstName = new FilterFirstName();
    private FilterLastName filterLastName = new FilterLastName();
    private FilterPatientId filterPatientId = new FilterPatientId();
    private FilterTablet filterTablet = new FilterTablet();
    private FilterEnrolled filterEnrolled = new FilterEnrolled();
    private FilterStatus filterStatus = new FilterStatus();
    private FilterRisk filterRisk = new FilterRisk();
    private FilterReviewed filterReviewed = new FilterReviewed();
    private FilterSubgroup filterSubgroup = new FilterSubgroup();
    private FilterClinician filterClinician = new FilterClinician();
    private FilterConditions filterConditions = new FilterConditions();
    private FilterMedications filterMedications = new FilterMedications();
    private FilterActivity filterActivity = new FilterActivity();
    private FilterBloodPressure filterBloodPressure = new FilterBloodPressure();
    private FilterPulseOx filterPulseOx = new FilterPulseOx();
    private FilterSurvey filterSurvey = new FilterSurvey();
    private FilterTemperature filterTemperature = new FilterTemperature();
    private FilterWeight filterWeight = new FilterWeight();
    private FilterGlucose filterGlucose = new FilterGlucose();
    private FilterImaging filterImaging = new FilterImaging();

    public FilterRequest() {
    }

    public FilterRequest(FilterFirstName filterFirstName, FilterLastName filterLastName, FilterPatientId filterPatientId, FilterTablet filterTablet, FilterEnrolled filterEnrolled, FilterStatus filterStatus, FilterRisk filterRisk, FilterReviewed filterReviewed, FilterSubgroup filterSubgroup, FilterClinician filterClinician, FilterConditions filterConditions, FilterMedications filterMedications, FilterActivity filterActivity, FilterBloodPressure filterBloodPressure, FilterPulseOx filterPulseOx, FilterSurvey filterSurvey, FilterTemperature filterTemperature, FilterWeight filterWeight, FilterGlucose filterGlucose, FilterImaging filterImaging) {
        this.filterFirstName = filterFirstName;
        this.filterLastName = filterLastName;
        this.filterPatientId = filterPatientId;
        this.filterTablet = filterTablet;
        this.filterEnrolled = filterEnrolled;
        this.filterStatus = filterStatus;
        this.filterRisk = filterRisk;
        this.filterReviewed = filterReviewed;
        this.filterSubgroup = filterSubgroup;
        this.filterClinician = filterClinician;
        this.filterConditions = filterConditions;
        this.filterMedications = filterMedications;
        this.filterActivity = filterActivity;
        this.filterBloodPressure = filterBloodPressure;
        this.filterPulseOx = filterPulseOx;
        this.filterSurvey = filterSurvey;
        this.filterTemperature = filterTemperature;
        this.filterWeight = filterWeight;
        this.filterGlucose = filterGlucose;
        this.filterImaging = filterImaging;
    }

    public FilterFirstName getFilterFirstName() {
        return filterFirstName;
    }

    public void setFilterFirstName(FilterFirstName filterFirstName) {
        this.filterFirstName = filterFirstName;
    }

    public FilterLastName getFilterLastName() {
        return filterLastName;
    }

    public void setFilterLastName(FilterLastName filterLastName) {
        this.filterLastName = filterLastName;
    }

    public FilterPatientId getFilterPatientId() {
        return filterPatientId;
    }

    public void setFilterPatientId(FilterPatientId filterPatientId) {
        this.filterPatientId = filterPatientId;
    }

    public FilterTablet getFilterTablet() {
        return filterTablet;
    }

    public void setFilterTablet(FilterTablet filterTablet) {
        this.filterTablet = filterTablet;
    }

    public FilterEnrolled getFilterEnrolled() {
        return filterEnrolled;
    }

    public void setFilterEnrolled(FilterEnrolled filterEnrolled) {
        this.filterEnrolled = filterEnrolled;
    }

    public FilterStatus getFilterStatus() {
        return filterStatus;
    }

    public void setFilterStatus(FilterStatus filterStatus) {
        this.filterStatus = filterStatus;
    }

    public FilterRisk getFilterRisk() {
        return filterRisk;
    }

    public void setFilterRisk(FilterRisk filterRisk) {
        this.filterRisk = filterRisk;
    }

    public FilterReviewed getFilterReviewed() {
        return filterReviewed;
    }

    public void setFilterReviewed(FilterReviewed filterReviewed) {
        this.filterReviewed = filterReviewed;
    }

    public FilterSubgroup getFilterSubgroup() {
        return filterSubgroup;
    }

    public void setFilterSubgroup(FilterSubgroup filterSubgroup) {
        this.filterSubgroup = filterSubgroup;
    }

    public FilterClinician getFilterClinician() {
        return filterClinician;
    }

    public void setFilterClinician(FilterClinician filterClinician) {
        this.filterClinician = filterClinician;
    }

    public FilterConditions getFilterConditions() {
        return filterConditions;
    }

    public void setFilterConditions(FilterConditions filterConditions) {
        this.filterConditions = filterConditions;
    }

    public FilterMedications getFilterMedications() {
        return filterMedications;
    }

    public void setFilterMedications(FilterMedications filterMedications) {
        this.filterMedications = filterMedications;
    }

    public FilterActivity getFilterActivity() {
        return filterActivity;
    }

    public void setFilterActivity(FilterActivity filterActivity) {
        this.filterActivity = filterActivity;
    }

    public FilterBloodPressure getFilterBloodPressure() {
        return filterBloodPressure;
    }

    public void setFilterBloodPressure(FilterBloodPressure filterBloodPressure) {
        this.filterBloodPressure = filterBloodPressure;
    }

    public FilterPulseOx getFilterPulseOx() {
        return filterPulseOx;
    }

    public void setFilterPulseOx(FilterPulseOx filterPulseOx) {
        this.filterPulseOx = filterPulseOx;
    }

    public FilterSurvey getFilterSurvey() {
        return filterSurvey;
    }

    public void setFilterSurvey(FilterSurvey filterSurvey) {
        this.filterSurvey = filterSurvey;
    }

    public FilterTemperature getFilterTemperature() {
        return filterTemperature;
    }

    public void setFilterTemperature(FilterTemperature filterTemperature) {
        this.filterTemperature = filterTemperature;
    }

    public FilterWeight getFilterWeight() {
        return filterWeight;
    }

    public void setFilterWeight(FilterWeight filterWeight) {
        this.filterWeight = filterWeight;
    }

    public FilterGlucose getFilterGlucose() {
        return filterGlucose;
    }

    public void setFilterGlucose(FilterGlucose filterGlucose) {
        this.filterGlucose = filterGlucose;
    }

    public FilterImaging getFilterImaging() {
        return filterImaging;
    }

    public void setFilterImaging(FilterImaging filterImaging) {
        this.filterImaging = filterImaging;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilterRequest that = (FilterRequest) o;
        return Objects.equals(filterFirstName, that.filterFirstName) &&
                Objects.equals(filterLastName, that.filterLastName) &&
                Objects.equals(filterPatientId, that.filterPatientId) &&
                Objects.equals(filterTablet, that.filterTablet) &&
                Objects.equals(filterEnrolled, that.filterEnrolled) &&
                Objects.equals(filterStatus, that.filterStatus) &&
                Objects.equals(filterRisk, that.filterRisk) &&
                Objects.equals(filterReviewed, that.filterReviewed) &&
                Objects.equals(filterSubgroup, that.filterSubgroup) &&
                Objects.equals(filterClinician, that.filterClinician) &&
                Objects.equals(filterConditions, that.filterConditions) &&
                Objects.equals(filterMedications, that.filterMedications) &&
                Objects.equals(filterActivity, that.filterActivity) &&
                Objects.equals(filterBloodPressure, that.filterBloodPressure) &&
                Objects.equals(filterPulseOx, that.filterPulseOx) &&
                Objects.equals(filterSurvey, that.filterSurvey) &&
                Objects.equals(filterTemperature, that.filterTemperature) &&
                Objects.equals(filterWeight, that.filterWeight) &&
                Objects.equals(filterGlucose, that.filterGlucose) &&
                Objects.equals(filterImaging, that.filterImaging);
    }

    @Override
    public int hashCode() {

        return Objects.hash(filterFirstName, filterLastName, filterPatientId, filterTablet, filterEnrolled, filterStatus, filterRisk, filterReviewed, filterSubgroup, filterClinician, filterConditions, filterMedications, filterActivity, filterBloodPressure, filterPulseOx, filterSurvey, filterTemperature, filterWeight, filterGlucose, filterImaging);
    }

    @Override
    public String toString() {
        return "FilterRequest{" +
                "filterFirstName=" + filterFirstName +
                ", filterLastName=" + filterLastName +
                ", filterPatientId=" + filterPatientId +
                ", filterTablet=" + filterTablet +
                ", filterEnrolled=" + filterEnrolled +
                ", filterStatus=" + filterStatus +
                ", filterRisk=" + filterRisk +
                ", filterReviewed=" + filterReviewed +
                ", filterSubgroup=" + filterSubgroup +
                ", filterClinician=" + filterClinician +
                ", filterConditions=" + filterConditions +
                ", filterMedications=" + filterMedications +
                ", filterActivity=" + filterActivity +
                ", filterBloodPressure=" + filterBloodPressure +
                ", filterPulseOx=" + filterPulseOx +
                ", filterSurvey=" + filterSurvey +
                ", filterTemperature=" + filterTemperature +
                ", filterWeight=" + filterWeight +
                ", filterGlucose=" + filterGlucose +
                ", filterImaging=" + filterImaging +
                '}';
    }
}

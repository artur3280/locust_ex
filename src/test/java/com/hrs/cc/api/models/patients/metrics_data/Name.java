package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "middle",
        "last",
        "first"
})
public class Name {

    @JsonProperty("middle")
    private String middle;
    @JsonProperty("last")
    private String last;
    @JsonProperty("first")
    private String first;

    public Name() {
    }

    public Name(String middle, String last, String first) {
        this.middle = middle;
        this.last = last;
        this.first = first;
    }

    public String getMiddle() {
        return middle;
    }

    public void setMiddle(String middle) {
        this.middle = middle;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Name name = (Name) o;
        return Objects.equals(middle, name.middle) &&
                Objects.equals(last, name.last) &&
                Objects.equals(first, name.first);
    }

    @Override
    public int hashCode() {
        return Objects.hash(middle, last, first);
    }

    @Override
    public String toString() {
        return "Name{" +
                "middle='" + middle + '\'' +
                ", last='" + last + '\'' +
                ", first='" + first + '\'' +
                '}';
    }
}

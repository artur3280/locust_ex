package com.hrs.cc.api.models.account_data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.hrs.cc.api.models.account_data.Application;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "authentication",
        "application"
})
public class Firebase {
    @JsonProperty("authentication")
    private String authentication;
    @JsonProperty("application")
    private Application application;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Firebase() {
    }

    public Firebase(String authentication, Application application) {
        this.authentication = authentication;
        this.application = application;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Firebase firebase = (Firebase) o;
        return Objects.equals(authentication, firebase.authentication) &&
                Objects.equals(application, firebase.application) &&
                Objects.equals(additionalProperties, firebase.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(authentication, application, additionalProperties);
    }


}

package com.hrs.cc.api.models.integratioins_app.clinician_auth_token;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "firstLogin"
})
public class Meta {
    @JsonProperty("firstLogin")
    private Boolean firstLogin;

    public Meta() {
    }

    public Meta(Boolean firstLogin) {
        this.firstLogin = firstLogin;
    }

    public Boolean getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(Boolean firstLogin) {
        this.firstLogin = firstLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meta meta = (Meta) o;
        return Objects.equals(firstLogin, meta.firstLogin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstLogin);
    }

    @Override
    public String toString() {
        return "Meta{" +
                "firstLogin=" + firstLogin +
                '}';
    }
}

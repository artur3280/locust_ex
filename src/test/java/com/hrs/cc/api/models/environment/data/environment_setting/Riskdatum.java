package com.hrs.cc.api.models.environment.data.environment_setting;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "alerttype",
        "type",
        "events",
        "value"
})
public class Riskdatum {
    @JsonProperty("alerttype")
    private String alerttype;
    @JsonProperty("type")
    private String type;
    @JsonProperty("events")
    private List<String> events = null;
    @JsonProperty("value")
    private String value;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Riskdatum() {
    }

    public Riskdatum(String alerttype, String type, List<String> events, String value) {
        this.alerttype = alerttype;
        this.type = type;
        this.events = events;
        this.value = value;
    }

    public String getAlerttype() {
        return alerttype;
    }

    public void setAlerttype(String alerttype) {
        this.alerttype = alerttype;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getEvents() {
        return events;
    }

    public void setEvents(List<String> events) {
        this.events = events;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Riskdatum riskdatum = (Riskdatum) o;
        return Objects.equals(alerttype, riskdatum.alerttype) &&
                Objects.equals(type, riskdatum.type) &&
                Objects.equals(events, riskdatum.events) &&
                Objects.equals(value, riskdatum.value) &&
                Objects.equals(additionalProperties, riskdatum.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(alerttype, type, events, value, additionalProperties);
    }

    @Override
    public String toString() {
        return "Riskdata{" +
                "alerttype='" + alerttype + '\'' +
                ", type='" + type + '\'' +
                ", events=" + events +
                ", value='" + value + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

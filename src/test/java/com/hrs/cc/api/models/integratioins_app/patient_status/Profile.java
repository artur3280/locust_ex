package com.hrs.cc.api.models.integratioins_app.patient_status;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "discharged",
        "status",
        "deactivated_at"
})
public class Profile {

    @JsonProperty("discharged")
    private Boolean discharged;
    @JsonProperty("status")
    private String status;
    @JsonProperty("deactivated_at")
    private String deactivated_at;

    public Profile() {
    }

    public Profile(Boolean discharged, String status, String deactivated_at) {
        this.discharged = discharged;
        this.status = status;
        this.deactivated_at = deactivated_at;
    }

    public Boolean getDischarged() {
        return discharged;
    }

    public void setDischarged(Boolean discharged) {
        this.discharged = discharged;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeactivated_at() {
        return deactivated_at;
    }

    public void setDeactivated_at(String deactivated_at) {
        this.deactivated_at = deactivated_at;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "discharged=" + discharged +
                ", status='" + status + '\'' +
                ", deactivated_at='" + deactivated_at + '\'' +
                '}';
    }
}

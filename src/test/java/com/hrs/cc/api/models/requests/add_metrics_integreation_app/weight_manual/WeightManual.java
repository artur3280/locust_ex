package com.hrs.cc.api.models.requests.add_metrics_integreation_app.weight_manual;

public class WeightManual {
    private DataWeight data;

    /**
     * No args constructor for use in serialization
     *
     */
    public WeightManual() {
    }

    /**
     *
     * @param data
     */
    public WeightManual(DataWeight data) {
        super();
        this.data = data;
    }

    public DataWeight getData() {
        return data;
    }

    public void setData(DataWeight data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PatientMetricsData{" +
                "data=" + data +
                '}';
    }
}

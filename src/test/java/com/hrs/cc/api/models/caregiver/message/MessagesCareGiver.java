package com.hrs.cc.api.models.caregiver.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class MessagesCareGiver {

    @JsonProperty("data")
    private ArrayList<Data> messages;

    public MessagesCareGiver() {
    }

    public MessagesCareGiver( ArrayList<Data> messages) {
        this.messages = messages;
    }


    public ArrayList<Data> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Data> messages) {
        this.messages = messages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessagesCareGiver that = (MessagesCareGiver) o;
        return Objects.equals(messages, that.messages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messages);
    }

    @Override
    public String toString() {
        return "MessagesCareGiver{" +
                "messages=" + messages +
                '}';
    }
}

package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "ts",
        "status"
})
public class Datum__ {

    @JsonProperty("id")
    private String id;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("status")
    private String status;

    public Datum__() {
    }

    public Datum__(String id, Long ts, String status) {
        this.id = id;
        this.ts = ts;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Datum__ datum__ = (Datum__) o;
        return Objects.equals(id, datum__.id) &&
                Objects.equals(ts, datum__.ts) &&
                Objects.equals(status, datum__.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ts, status);
    }

    @Override
    public String toString() {
        return "Datum__{" +
                "id='" + id + '\'' +
                ", ts=" + ts +
                ", status='" + status + '\'' +
                '}';
    }
}

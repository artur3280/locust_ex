package com.hrs.cc.api.models.inventory.tablet_status;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "timezone_type",
        "timezone"
})
public class LastUpdated {

    @JsonProperty("date")
    private String date;
    @JsonProperty("timezone_type")
    private Long timezoneType;
    @JsonProperty("timezone")
    private String timezone;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public LastUpdated() {
    }

    public LastUpdated(String date, Long timezoneType, String timezone) {
        this.date = date;
        this.timezoneType = timezoneType;
        this.timezone = timezone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getTimezoneType() {
        return timezoneType;
    }

    public void setTimezoneType(Long timezoneType) {
        this.timezoneType = timezoneType;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LastUpdated that = (LastUpdated) o;
        return Objects.equals(date, that.date) &&
                Objects.equals(timezoneType, that.timezoneType) &&
                Objects.equals(timezone, that.timezone) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(date, timezoneType, timezone, additionalProperties);
    }

    @Override
    public String toString() {
        return "LastUpdated{" +
                "date='" + date + '\'' +
                ", timezoneType=" + timezoneType +
                ", timezone='" + timezone + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

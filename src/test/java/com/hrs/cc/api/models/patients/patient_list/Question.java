package com.hrs.cc.api.models.patients.patient_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "schedule",
        "question",
        "answer",
        "today",
        "id",
        "once",
        "ts",
        "discharge"
})
public class Question {

    @JsonProperty("schedule")
    private Schedule schedule;
    @JsonProperty("question")
    private String question;
    @JsonProperty("answer")
    private String answer;
    @JsonProperty("today")
    private Boolean today;
    @JsonProperty("once")
    private Boolean once;
    @JsonProperty("discharge")
    private Boolean discharge;
    @JsonProperty("id")
    private String id;
    @JsonProperty("ts")
    private Integer ts;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Question() {
    }

    public Question(Schedule schedule, String question, String answer, Boolean today, Boolean once, Boolean discharge, String id, Integer ts) {
        this.schedule = schedule;
        this.question = question;
        this.answer = answer;
        this.today = today;
        this.once = once;
        this.discharge = discharge;
        this.id = id;
        this.ts = ts;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Boolean getToday() {
        return today;
    }

    public void setToday(Boolean today) {
        this.today = today;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getTs() {
        return ts;
    }

    public void setTs(Integer ts) {
        this.ts = ts;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public Boolean getDischarge() {
        return discharge;
    }

    public void setDischarge(Boolean discharge) {
        this.discharge = discharge;
    }

    public Boolean getOnce() {
        return once;
    }

    public void setOnce(Boolean once) {
        this.once = once;
    }
}

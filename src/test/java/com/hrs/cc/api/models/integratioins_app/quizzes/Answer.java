package com.hrs.cc.api.models.integratioins_app.quizzes;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "correct",
        "text"
})
public class Answer {

    @JsonProperty("correct")
    private Boolean correct;
    @JsonProperty("text")
    private String text;

    public Answer() {
    }

    public Answer(Boolean correct, String text) {
        this.correct = correct;
        this.text = text;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "correct=" + correct +
                ", text='" + text + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.account_data.clinician;


import com.fasterxml.jackson.annotation.*;
import com.hrs.cc.api.models.account_data.clinician.filters.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "first_name",
        "last_name",
        "middle_name",
        "email",
        "telephone",
        "risk_email",
        "risk_text",
        "time_zone",
        "session_timeout",
        "clinician_insecure_reports",
        "filter_first_name",
        "filter_last_name",
        "filter_patient_id",
        "filter_tablet",
        "filter_enrolled",
        "filter_status",
        "filter_risk",
        "filter_reviewed",
        "filter_subgroup",
        "filter_clinician",
        "filter_conditions",
        "filter_medications",
        "filter_activity",
        "filter_blood_pressure",
        "filter_pulse_ox",
        "filter_survey",
        "filter_temperature",
        "filter_weight",
        "filter_glucose",
        "filter_imaging",
        "filter_state"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Clinician {
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("middle_name")
    private String middle_name;
    @JsonProperty("email")
    private String email;
    @JsonProperty("telephone")
    private String telephone;
    @JsonProperty("risk_email")
    private Boolean riskEmail;
    @JsonProperty("risk_text")
    private Boolean riskText;
    @JsonProperty("time_zone")
    private String timeZone;
    @JsonProperty("session_timeout")
    private String sessionTimeout;
    @JsonProperty("clinician_insecure_reports")
    private Boolean clinicianInsecureReports;
    @JsonProperty("filter_first_name")
    private FilterFirstName filterFirstName;
    @JsonProperty("filter_last_name")
    private FilterLastName filterLastName;
    @JsonProperty("filter_patient_id")
    private FilterPatientId filterPatientId;
    @JsonProperty("filter_tablet")
    private FilterTablet filterTablet;
    @JsonProperty("filter_enrolled")
    private FilterEnrolled filterEnrolled;
    @JsonProperty("filter_status")
    private FilterStatus filterStatus;
    @JsonProperty("filter_risk")
    private FilterRisk filterRisk;
    @JsonProperty("filter_reviewed")
    private FilterReviewed filterReviewed;
    @JsonProperty("filter_subgroup")
    private FilterSubgroup filterSubgroup;
    @JsonProperty("filter_clinician")
    private FilterClinician filterClinician;
    @JsonProperty("filter_conditions")
    private FilterConditions filterConditions;
    @JsonProperty("filter_medications")
    private FilterMedications filterMedications;
    @JsonProperty("filter_activity")
    private FilterActivity filterActivity;
    @JsonProperty("filter_blood_pressure")
    private FilterBloodPressure filterBloodPressure;
    @JsonProperty("filter_pulse_ox")
    private FilterPulseOx filterPulseOx;
    @JsonProperty("filter_survey")
    private FilterSurvey filterSurvey;
    @JsonProperty("filter_temperature")
    private FilterTemperature filterTemperature;
    @JsonProperty("filter_weight")
    private FilterWeight filterWeight;
    @JsonProperty("filter_glucose")
    private FilterGlucose filterGlucose;
    @JsonProperty("filter_imaging")
    private FilterImaging filterImaging;
    @JsonProperty("filter_state")
    private FilterState filterState;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Clinician() {
    }

    public Clinician(String firstName, String lastName, String middle_name, String email, String telephone, Boolean riskEmail, Boolean riskText, String timeZone, String sessionTimeout, Boolean clinicianInsecureReports, FilterFirstName filterFirstName, FilterLastName filterLastName, FilterPatientId filterPatientId, FilterTablet filterTablet, FilterEnrolled filterEnrolled, FilterStatus filterStatus, FilterRisk filterRisk, FilterReviewed filterReviewed, FilterSubgroup filterSubgroup, FilterClinician filterClinician, FilterConditions filterConditions, FilterMedications filterMedications, FilterActivity filterActivity, FilterBloodPressure filterBloodPressure, FilterPulseOx filterPulseOx, FilterSurvey filterSurvey, FilterTemperature filterTemperature, FilterWeight filterWeight, FilterGlucose filterGlucose, FilterImaging filterImaging, FilterState filterState) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middle_name = middle_name;
        this.email = email;
        this.telephone = telephone;
        this.riskEmail = riskEmail;
        this.riskText = riskText;
        this.timeZone = timeZone;
        this.sessionTimeout = sessionTimeout;
        this.clinicianInsecureReports = clinicianInsecureReports;
        this.filterFirstName = filterFirstName;
        this.filterLastName = filterLastName;
        this.filterPatientId = filterPatientId;
        this.filterTablet = filterTablet;
        this.filterEnrolled = filterEnrolled;
        this.filterStatus = filterStatus;
        this.filterRisk = filterRisk;
        this.filterReviewed = filterReviewed;
        this.filterSubgroup = filterSubgroup;
        this.filterClinician = filterClinician;
        this.filterConditions = filterConditions;
        this.filterMedications = filterMedications;
        this.filterActivity = filterActivity;
        this.filterBloodPressure = filterBloodPressure;
        this.filterPulseOx = filterPulseOx;
        this.filterSurvey = filterSurvey;
        this.filterTemperature = filterTemperature;
        this.filterWeight = filterWeight;
        this.filterGlucose = filterGlucose;
        this.filterImaging = filterImaging;
        this.filterState = filterState;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Boolean getRiskEmail() {
        return riskEmail;
    }

    public void setRiskEmail(Boolean riskEmail) {
        this.riskEmail = riskEmail;
    }

    public Boolean getRiskText() {
        return riskText;
    }

    public void setRiskText(Boolean riskText) {
        this.riskText = riskText;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(String sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public FilterFirstName getFilterFirstName() {
        return filterFirstName;
    }

    public void setFilterFirstName(FilterFirstName filterFirstName) {
        this.filterFirstName = filterFirstName;
    }

    public FilterLastName getFilterLastName() {
        return filterLastName;
    }

    public void setFilterLastName(FilterLastName filterLastName) {
        this.filterLastName = filterLastName;
    }

    public FilterPatientId getFilterPatientId() {
        return filterPatientId;
    }

    public void setFilterPatientId(FilterPatientId filterPatientId) {
        this.filterPatientId = filterPatientId;
    }

    public FilterTablet getFilterTablet() {
        return filterTablet;
    }

    public void setFilterTablet(FilterTablet filterTablet) {
        this.filterTablet = filterTablet;
    }

    public FilterEnrolled getFilterEnrolled() {
        return filterEnrolled;
    }

    public void setFilterEnrolled(FilterEnrolled filterEnrolled) {
        this.filterEnrolled = filterEnrolled;
    }

    public FilterStatus getFilterStatus() {
        return filterStatus;
    }

    public void setFilterStatus(FilterStatus filterStatus) {
        this.filterStatus = filterStatus;
    }

    public FilterRisk getFilterRisk() {
        return filterRisk;
    }

    public void setFilterRisk(FilterRisk filterRisk) {
        this.filterRisk = filterRisk;
    }

    public FilterReviewed getFilterReviewed() {
        return filterReviewed;
    }

    public void setFilterReviewed(FilterReviewed filterReviewed) {
        this.filterReviewed = filterReviewed;
    }

    public FilterSubgroup getFilterSubgroup() {
        return filterSubgroup;
    }

    public void setFilterSubgroup(FilterSubgroup filterSubgroup) {
        this.filterSubgroup = filterSubgroup;
    }

    public FilterClinician getFilterClinician() {
        return filterClinician;
    }

    public void setFilterClinician(FilterClinician filterClinician) {
        this.filterClinician = filterClinician;
    }

    public FilterConditions getFilterConditions() {
        return filterConditions;
    }

    public void setFilterConditions(FilterConditions filterConditions) {
        this.filterConditions = filterConditions;
    }

    public FilterMedications getFilterMedications() {
        return filterMedications;
    }

    public void setFilterMedications(FilterMedications filterMedications) {
        this.filterMedications = filterMedications;
    }

    public FilterActivity getFilterActivity() {
        return filterActivity;
    }

    public void setFilterActivity(FilterActivity filterActivity) {
        this.filterActivity = filterActivity;
    }

    public FilterBloodPressure getFilterBloodPressure() {
        return filterBloodPressure;
    }

    public void setFilterBloodPressure(FilterBloodPressure filterBloodPressure) {
        this.filterBloodPressure = filterBloodPressure;
    }

    public FilterPulseOx getFilterPulseOx() {
        return filterPulseOx;
    }

    public void setFilterPulseOx(FilterPulseOx filterPulseOx) {
        this.filterPulseOx = filterPulseOx;
    }

    public FilterSurvey getFilterSurvey() {
        return filterSurvey;
    }

    public void setFilterSurvey(FilterSurvey filterSurvey) {
        this.filterSurvey = filterSurvey;
    }

    public FilterTemperature getFilterTemperature() {
        return filterTemperature;
    }

    public void setFilterTemperature(FilterTemperature filterTemperature) {
        this.filterTemperature = filterTemperature;
    }

    public FilterWeight getFilterWeight() {
        return filterWeight;
    }

    public void setFilterWeight(FilterWeight filterWeight) {
        this.filterWeight = filterWeight;
    }

    public FilterGlucose getFilterGlucose() {
        return filterGlucose;
    }

    public void setFilterGlucose(FilterGlucose filterGlucose) {
        this.filterGlucose = filterGlucose;
    }

    public FilterImaging getFilterImaging() {
        return filterImaging;
    }

    public void setFilterImaging(FilterImaging filterImaging) {
        this.filterImaging = filterImaging;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public Boolean getClinicianInsecureReports() {
        return clinicianInsecureReports;
    }

    public void setClinicianInsecureReports(Boolean clinicianInsecureReports) {
        this.clinicianInsecureReports = clinicianInsecureReports;
    }

    public FilterState getFilterState() {
        return filterState;
    }

    public void setFilterState(FilterState filterState) {
        this.filterState = filterState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clinician clinician = (Clinician) o;
        return Objects.equals(firstName, clinician.firstName) &&
                Objects.equals(lastName, clinician.lastName) &&
                Objects.equals(middle_name, clinician.middle_name) &&
                Objects.equals(email, clinician.email) &&
                Objects.equals(telephone, clinician.telephone) &&
                Objects.equals(riskEmail, clinician.riskEmail) &&
                Objects.equals(riskText, clinician.riskText) &&
                Objects.equals(timeZone, clinician.timeZone) &&
                Objects.equals(sessionTimeout, clinician.sessionTimeout) &&
                Objects.equals(clinicianInsecureReports, clinician.clinicianInsecureReports) &&
                Objects.equals(filterFirstName, clinician.filterFirstName) &&
                Objects.equals(filterLastName, clinician.filterLastName) &&
                Objects.equals(filterPatientId, clinician.filterPatientId) &&
                Objects.equals(filterTablet, clinician.filterTablet) &&
                Objects.equals(filterEnrolled, clinician.filterEnrolled) &&
                Objects.equals(filterStatus, clinician.filterStatus) &&
                Objects.equals(filterRisk, clinician.filterRisk) &&
                Objects.equals(filterReviewed, clinician.filterReviewed) &&
                Objects.equals(filterSubgroup, clinician.filterSubgroup) &&
                Objects.equals(filterClinician, clinician.filterClinician) &&
                Objects.equals(filterConditions, clinician.filterConditions) &&
                Objects.equals(filterMedications, clinician.filterMedications) &&
                Objects.equals(filterActivity, clinician.filterActivity) &&
                Objects.equals(filterBloodPressure, clinician.filterBloodPressure) &&
                Objects.equals(filterPulseOx, clinician.filterPulseOx) &&
                Objects.equals(filterSurvey, clinician.filterSurvey) &&
                Objects.equals(filterTemperature, clinician.filterTemperature) &&
                Objects.equals(filterWeight, clinician.filterWeight) &&
                Objects.equals(filterGlucose, clinician.filterGlucose) &&
                Objects.equals(filterImaging, clinician.filterImaging) &&
                Objects.equals(filterState, clinician.filterState) &&
                Objects.equals(additionalProperties, clinician.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, middle_name, email, telephone, riskEmail, riskText, timeZone, sessionTimeout, clinicianInsecureReports, filterFirstName, filterLastName, filterPatientId, filterTablet, filterEnrolled, filterStatus, filterRisk, filterReviewed, filterSubgroup, filterClinician, filterConditions, filterMedications, filterActivity, filterBloodPressure, filterPulseOx, filterSurvey, filterTemperature, filterWeight, filterGlucose, filterImaging, filterState, additionalProperties);
    }

    @Override
    public String toString() {
        return "Clinician{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middle_name='" + middle_name + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", riskEmail=" + riskEmail +
                ", riskText=" + riskText +
                ", timeZone='" + timeZone + '\'' +
                ", sessionTimeout='" + sessionTimeout + '\'' +
                ", clinicianInsecureReports=" + clinicianInsecureReports +
                ", filterFirstName=" + filterFirstName +
                ", filterLastName=" + filterLastName +
                ", filterPatientId=" + filterPatientId +
                ", filterTablet=" + filterTablet +
                ", filterEnrolled=" + filterEnrolled +
                ", filterStatus=" + filterStatus +
                ", filterRisk=" + filterRisk +
                ", filterReviewed=" + filterReviewed +
                ", filterSubgroup=" + filterSubgroup +
                ", filterClinician=" + filterClinician +
                ", filterConditions=" + filterConditions +
                ", filterMedications=" + filterMedications +
                ", filterActivity=" + filterActivity +
                ", filterBloodPressure=" + filterBloodPressure +
                ", filterPulseOx=" + filterPulseOx +
                ", filterSurvey=" + filterSurvey +
                ", filterTemperature=" + filterTemperature +
                ", filterWeight=" + filterWeight +
                ", filterGlucose=" + filterGlucose +
                ", filterImaging=" + filterImaging +
                ", filterState=" + filterState +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.patients.notes;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "location",
        "los"
})
public class Readmission {

    @JsonProperty("date")
    private Long date;
    @JsonProperty("location")
    private String location;
    @JsonProperty("los")
    private Integer los;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Readmission() {
    }

    public Readmission(Long date, String location, Integer los) {
        this.date = date;
        this.location = location;
        this.los = los;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getLos() {
        return los;
    }

    public void setLos(Integer los) {
        this.los = los;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Readmission that = (Readmission) o;
        return Objects.equals(date, that.date) &&
                Objects.equals(location, that.location) &&
                Objects.equals(los, that.los) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(date, location, los, additionalProperties);
    }

    @Override
    public String toString() {
        return "Readmission{" +
                "date=" + date +
                ", location='" + location + '\'' +
                ", los=" + los +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.inventory.device_history;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "assignmentDate",
        "device"
})
public class Data {

    @JsonProperty("assignmentDate")
    private String assignmentDate;
    @JsonProperty("device")
    private Device device;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Data() {
    }

    public Data(String assignmentDate, Device device) {
        this.assignmentDate = assignmentDate;
        this.device = device;
    }

    public String getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(String assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return Objects.equals(assignmentDate, data.assignmentDate) &&
                Objects.equals(device, data.device) &&
                Objects.equals(additionalProperties, data.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(assignmentDate, device, additionalProperties);
    }

    @Override
    public String toString() {
        return "Data{" +
                "assignmentDate='" + assignmentDate + '\'' +
                ", device=" + device +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

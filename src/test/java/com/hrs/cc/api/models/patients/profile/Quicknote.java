package com.hrs.cc.api.models.patients.profile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "clinician",
        "note",
        "time"
})
public class Quicknote {

    @JsonProperty("clinician")
    private String clinician;
    @JsonProperty("note")
    private String note;
    @JsonProperty("time")
    private Long time;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Quicknote() {
    }

    public Quicknote(String clinician, String note, Long time) {
        this.clinician = clinician;
        this.note = note;
        this.time = time;
    }

    public String getClinician() {
        return clinician;
    }

    public void setClinician(String clinician) {
        this.clinician = clinician;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Quicknote quicknote = (Quicknote) o;
        return Objects.equals(clinician, quicknote.clinician) &&
                Objects.equals(note, quicknote.note) &&
                Objects.equals(time, quicknote.time) &&
                Objects.equals(additionalProperties, quicknote.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(clinician, note, time, additionalProperties);
    }

    @Override
    public String toString() {
        return "Quicknote{" +
                "clinician='" + clinician + '\'' +
                ", note='" + note + '\'' +
                ", time=" + time +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

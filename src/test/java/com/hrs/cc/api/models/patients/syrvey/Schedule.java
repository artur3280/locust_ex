package com.hrs.cc.api.models.patients.syrvey;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "schedule",
        "x",
        "startday",
        "exp"
})
public class Schedule {

    @JsonProperty("type")
    private String type;
    @JsonProperty("schedule")
    private String schedule;
    @JsonProperty("x")
    private String x;
    @JsonProperty("startday")
    private String startday;
    @JsonProperty("exp")
    private String exp;

    public Schedule() {
    }

    public Schedule(String type, String schedule, String x, String startday, String exp) {
        this.type = type;
        this.schedule = schedule;
        this.x = x;
        this.startday = startday;
        this.exp = exp;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getStartday() {
        return startday;
    }

    public void setStartday(String startday) {
        this.startday = startday;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "type='" + type + '\'' +
                ", schedule='" + schedule + '\'' +
                ", x='" + x + '\'' +
                ", startday='" + startday + '\'' +
                ", exp='" + exp + '\'' +
                '}';
    }
}

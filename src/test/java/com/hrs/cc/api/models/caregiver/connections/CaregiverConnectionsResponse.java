package com.hrs.cc.api.models.caregiver.connections;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class CaregiverConnectionsResponse {

    @JsonProperty("data")
    private List<Datum> data = null;

    public CaregiverConnectionsResponse() {
    }

    public CaregiverConnectionsResponse(List<Datum> data) {
        this.data = data;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "CaregiverConnectionsResponse{" +
                "data=" + data +
                '}';
    }
}

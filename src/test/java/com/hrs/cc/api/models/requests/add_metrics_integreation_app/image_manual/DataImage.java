package com.hrs.cc.api.models.requests.add_metrics_integreation_app.image_manual;

public class DataImage {
    private String owner;
    private String entered;
    private String metric;
    private String remindAt;
    private String takenAt;
    private String device = null;
    private FacetImage facet;

    public DataImage() {
    }

    public DataImage(String owner, String entered, String metric, String remindAt, String takenAt, String device, FacetImage facet) {
        this.owner = owner;
        this.entered = entered;
        this.metric = metric;
        this.remindAt = remindAt;
        this.takenAt = takenAt;
        this.device = device;
        this.facet = facet;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getEntered() {
        return entered;
    }

    public void setEntered(String entered) {
        this.entered = entered;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getRemindAt() {
        return remindAt;
    }

    public void setRemindAt(String remindAt) {
        this.remindAt = remindAt;
    }

    public String getTakenAt() {
        return takenAt;
    }

    public void setTakenAt(String takenAt) {
        this.takenAt = takenAt;
    }

    public FacetImage getFacet() {
        return facet;
    }

    public void setFacet(FacetImage facet) {
        this.facet = facet;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    @Override
    public String toString() {
        return "DataImage{" +
                "owner='" + owner + '\'' +
                ", entered='" + entered + '\'' +
                ", metric='" + metric + '\'' +
                ", remindAt='" + remindAt + '\'' +
                ", takenAt='" + takenAt + '\'' +
                ", device='" + device + '\'' +
                ", facet=" + facet +
                '}';
    }
}

package com.hrs.cc.api.models.patients.metrics_data.uploaded_image_response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "image",
        "id",
        "chunk",
        "data"
})
public class Facet {

    @JsonProperty("image")
    private String image;
    @JsonProperty("id")
    private String id;
    @JsonProperty("chunk")
    private Integer chunk;
    @JsonProperty("data")
    private String data;

    public Facet() {
    }

    public Facet(String image, String id, Integer chunk, String data) {
        this.image = image;
        this.id = id;
        this.chunk = chunk;
        this.data = data;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getChunk() {
        return chunk;
    }

    public void setChunk(Integer chunk) {
        this.chunk = chunk;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Facet facet = (Facet) o;
        return Objects.equals(image, facet.image) &&
                Objects.equals(id, facet.id) &&
                Objects.equals(chunk, facet.chunk) &&
                Objects.equals(data, facet.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(image, id, chunk, data);
    }

    @Override
    public String toString() {
        return "Facet{" +
                "image='" + image + '\'' +
                ", id='" + id + '\'' +
                ", chunk=" + chunk +
                ", data='" + data + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.patients.patient_notes;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "location",
        "los"
})
public class Readmission {

    @JsonProperty("date")
    private Object date;
    @JsonProperty("location")
    private Object location;
    @JsonProperty("los")
    private Integer los;

    public Readmission() {
    }

    public Readmission(Object date, Object location, Integer los) {
        this.date = date;
        this.location = location;
        this.los = los;
    }

    public Object getDate() {
        return date;
    }

    public void setDate(Object date) {
        this.date = date;
    }

    public Object getLocation() {
        return location;
    }

    public void setLocation(Object location) {
        this.location = location;
    }

    public Integer getLos() {
        return los;
    }

    public void setLos(Integer los) {
        this.los = los;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Readmission that = (Readmission) o;
        return Objects.equals(date, that.date) &&
                Objects.equals(location, that.location) &&
                Objects.equals(los, that.los);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, location, los);
    }

    @Override
    public String toString() {
        return "Readmission{" +
                "date=" + date +
                ", location=" + location +
                ", los=" + los +
                '}';
    }
}

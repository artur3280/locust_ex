package com.hrs.cc.api.models.requests.set_patient_metricks.temperature;

import java.util.Objects;

public class SetTemperature {
    private String type = "temperature";
    private String reason;
    private String ftime;
    private Integer temperature;
    private String unit = "F";
    private String temperatureunit = "F";

    public SetTemperature() {
    }

    public SetTemperature(String reason, String ftime, Integer temperature) {
        this.reason = reason;
        this.ftime = ftime;
        this.temperature = temperature;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFtime() {
        return ftime;
    }

    public void setFtime(String ftime) {
        this.ftime = ftime;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getTemperatureunit() {
        return temperatureunit;
    }

    public void setTemperatureunit(String temperatureunit) {
        this.temperatureunit = temperatureunit;
    }

    @Override
    public String toString() {
        return "SetTemperature{" +
                "type='" + type + '\'' +
                ", reason='" + reason + '\'' +
                ", ftime='" + ftime + '\'' +
                ", temperature=" + temperature +
                ", unit='" + unit + '\'' +
                ", temperatureunit='" + temperatureunit + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.requests.filtering_patient_list.filtering;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "visible",
        "sort",
        "search"
})
public class Filter<T> {
    @JsonProperty("visible")
    private Boolean visible = true;
    @JsonProperty("sort")
    private String sort = "";
    @JsonProperty("search")
    private T search = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Filter() {
    }

    public Filter(Boolean visible, String sort, T search) {
        this.visible = visible;
        this.sort = sort;
        this.search = search;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public T getSearch() {
        return search;
    }

    public void setSearch(T search) {
        this.search = search;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Filter filter = (Filter) o;
        return Objects.equals(visible, filter.visible) &&
                Objects.equals(sort, filter.sort) &&
                Objects.equals(search, filter.search) &&
                Objects.equals(additionalProperties, filter.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(visible, sort, search, additionalProperties);
    }

    @Override
    public String toString() {
        return "Filter{" +
                "visible=" + visible +
                ", sort='" + sort + '\'' +
                ", search=" + search +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

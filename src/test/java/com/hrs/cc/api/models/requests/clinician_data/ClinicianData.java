package com.hrs.cc.api.models.requests.clinician_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "filter_state"
})
public class ClinicianData {

    @JsonProperty("filter_state")
    private FilterState filterState;

    public ClinicianData() {
    }

    public ClinicianData(FilterState filterState) {
        this.filterState = filterState;
    }

    public FilterState getFilterState() {
        return filterState;
    }

    public void setFilterState(FilterState filterState) {
        this.filterState = filterState;
    }

    @Override
    public String toString() {
        return "ClinicianData{" +
                "filterState=" + filterState +
                '}';
    }
}

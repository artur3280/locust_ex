package com.hrs.cc.api.models.requests.turn_education_content;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "id",
        "type",
        "category",
        "title",
        "status",
        "duration",
        "description"
})
public class SetPatientContent {

    private String id;

    private String type;

    private String category;

    private String title;

    private Boolean enabled;

    public SetPatientContent() {
    }

    public SetPatientContent(String id, String type, String category, String title, Boolean enabled) {
        this.id = id;
        this.type = type;
        this.category = category;
        this.title = title;
        this.enabled = enabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}

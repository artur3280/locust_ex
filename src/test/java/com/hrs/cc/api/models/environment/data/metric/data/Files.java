package com.hrs.cc.api.models.environment.data.metric.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "units",
        "type"
})
public class Files {
    @JsonProperty("units")
    private String units;
    @JsonProperty("type")
    private String type;

    public Files() {
    }

    public Files(String units, String type) {
        this.units = units;
        this.type = type;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Files files = (Files) o;
        return Objects.equals(units, files.units) &&
                Objects.equals(type, files.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(units, type);
    }

    @Override
    public String toString() {
        return "Files{" +
                "units='" + units + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}

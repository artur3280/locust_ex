package com.hrs.cc.api.models.requests.set_patient_metricks;

import java.util.Objects;

public class Activityreminder {
    private String window = "-";
    private String time = "20";

    public Activityreminder() {
    }

    public Activityreminder(String time, String window) {
        this.time = time;
        this.window = window;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWindow() {
        return window;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Activityreminder that = (Activityreminder) o;
        return Objects.equals(window, that.window) &&
                Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {

        return Objects.hash(window, time);
    }

    @Override
    public String toString() {
        return "Activityreminder{" +
                "window='" + window + '\'' +
                ", time=" + time +
                '}';
    }
}

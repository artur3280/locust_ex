package com.hrs.cc.api.models.environment.data.environment_setting;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "riskdata",
        "phone",
        "appid"
})
public class Value {

    @JsonProperty("riskdata")
    private List<Riskdatum> riskdata = null;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("appid")
    private String appid;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Value() {
    }

    public Value(List<Riskdatum> riskdata, String phone, String appid) {
        this.riskdata = riskdata;
        this.phone = phone;
        this.appid = appid;
    }

    public List<Riskdatum> getRiskdata() {
        return riskdata;
    }

    public void setRiskdata(List<Riskdatum> riskdata) {
        this.riskdata = riskdata;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Value value = (Value) o;
        return Objects.equals(riskdata, value.riskdata) &&
                Objects.equals(phone, value.phone) &&
                Objects.equals(appid, value.appid) &&
                Objects.equals(additionalProperties, value.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(riskdata, phone, appid, additionalProperties);
    }

    @Override
    public String toString() {
        return "Value{" +
                "riskdata=" + riskdata +
                ", phone='" + phone + '\'' +
                ", appid='" + appid + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

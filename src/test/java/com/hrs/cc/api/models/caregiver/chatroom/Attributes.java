package com.hrs.cc.api.models.caregiver.chatroom;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "participants",
        "createdAt",
})
public class Attributes {
    @JsonProperty("participants")
    private List<String> participants = null;
    @JsonProperty("createdAt")
    private String createdAt;

    public Attributes() {
    }

    public Attributes(List<String> participants, String createdAt) {
        this.participants = participants;
        this.createdAt = createdAt;
    }

    public List<String> getParticipants() {
        return participants;
    }

    public void setParticipants(List<String> participants) {
        this.participants = participants;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "participants=" + participants +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }
}

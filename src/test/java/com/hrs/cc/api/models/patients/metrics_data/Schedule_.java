package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "startday",
        "x",
        "schedule"
})
public class Schedule_ {

    @JsonProperty("type")
    private String type;
    @JsonProperty("startday")
    private String startday;
    @JsonProperty("x")
    private String x;
    @JsonProperty("schedule")
    private String schedule;

    public Schedule_() {
    }

    public Schedule_(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule_ schedule_ = (Schedule_) o;
        return Objects.equals(type, schedule_.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }

    @Override
    public String toString() {
        return "Schedule_{" +
                "type='" + type + '\'' +
                '}';
    }
}

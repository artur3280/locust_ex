package com.hrs.cc.api.models.account_data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "apiKey",
        "authDomain",
        "databaseURL",
        "projectId",
        "storageBucket",
        "messagingSenderId",
        "messaging_server_key"
})
public class Application {
    @JsonProperty("apiKey")
    private String apiKey;
    @JsonProperty("authDomain")
    private String authDomain;
    @JsonProperty("databaseURL")
    private String databaseURL;
    @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("storageBucket")
    private String storageBucket;
    @JsonProperty("messagingSenderId")
    private String messagingSenderId;
    @JsonProperty("messaging_server_key")
    private String messaging_server_key;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Application() {
    }

    public Application(String apiKey, String authDomain, String databaseURL, String projectId, String storageBucket, String messagingSenderId) {
        this.apiKey = apiKey;
        this.authDomain = authDomain;
        this.databaseURL = databaseURL;
        this.projectId = projectId;
        this.storageBucket = storageBucket;
        this.messagingSenderId = messagingSenderId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getAuthDomain() {
        return authDomain;
    }

    public void setAuthDomain(String authDomain) {
        this.authDomain = authDomain;
    }

    public String getDatabaseURL() {
        return databaseURL;
    }

    public void setDatabaseURL(String databaseURL) {
        this.databaseURL = databaseURL;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getStorageBucket() {
        return storageBucket;
    }

    public void setStorageBucket(String storageBucket) {
        this.storageBucket = storageBucket;
    }

    public String getMessagingSenderId() {
        return messagingSenderId;
    }

    public void setMessagingSenderId(String messagingSenderId) {
        this.messagingSenderId = messagingSenderId;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Application that = (Application) o;
        return Objects.equals(apiKey, that.apiKey) &&
                Objects.equals(authDomain, that.authDomain) &&
                Objects.equals(databaseURL, that.databaseURL) &&
                Objects.equals(projectId, that.projectId) &&
                Objects.equals(storageBucket, that.storageBucket) &&
                Objects.equals(messagingSenderId, that.messagingSenderId) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(apiKey, authDomain, databaseURL, projectId, storageBucket, messagingSenderId, additionalProperties);
    }

    @Override
    public String toString() {
        return "Application{" +
                "apiKey='" + apiKey + '\'' +
                ", authDomain='" + authDomain + '\'' +
                ", databaseURL='" + databaseURL + '\'' +
                ", projectId='" + projectId + '\'' +
                ", storageBucket='" + storageBucket + '\'' +
                ", messagingSenderId='" + messagingSenderId + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}


package com.hrs.cc.api.models.integratioins_app.patient_tasks_v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "val",
        "type",
        "selectOnly",
        "text",
        "risk"
})
public class Choice {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("val")
    private Integer val;
    @JsonProperty("type")
    private String type;
    @JsonProperty("selectOnly")
    private Boolean selectOnly;
    @JsonProperty("text")
    private String text;
    @JsonProperty("risk")
    private String risk;

    public Choice() {
    }

    public Choice(Integer id, Integer val, String type, Boolean selectOnly, String text, String risk) {
        this.id = id;
        this.val = val;
        this.type = type;
        this.selectOnly = selectOnly;
        this.text = text;
        this.risk = risk;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVal() {
        return val;
    }

    public void setVal(Integer val) {
        this.val = val;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getSelectOnly() {
        return selectOnly;
    }

    public void setSelectOnly(Boolean selectOnly) {
        this.selectOnly = selectOnly;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    @Override
    public String toString() {
        return "Choice{" +
                "id=" + id +
                ", val=" + val +
                ", type='" + type + '\'' +
                ", selectOnly=" + selectOnly +
                ", text='" + text + '\'' +
                ", risk='" + risk + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.requests.add_metrics_integreation_app.temperature_manual;


public class TemperatureManual {
    private DataTemperature data;

    /**
     * No args constructor for use in serialization
     *
     */
    public TemperatureManual() {
    }

    /**
     *
     * @param data
     */
    public TemperatureManual(DataTemperature data) {
        super();
        this.data = data;
    }

    public DataTemperature getData() {
        return data;
    }

    public void setData(DataTemperature data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "TemperatureManual{" +
                "data=" + data +
                '}';
    }
}

package com.hrs.cc.api.models.integratioins_app.quizzes;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "conditions",
        "description",
        "questions"
})
public class Attributes {

    @JsonProperty("name")
    private String name;
    @JsonProperty("conditions")
    private List<String> conditions = null;
    @JsonProperty("description")
    private Object description;
    @JsonProperty("questions")
    private List<Question> questions = null;

    public Attributes() {
    }

    public Attributes(String name, List<String> conditions, Object description, List<Question> questions) {
        this.name = name;
        this.conditions = conditions;
        this.description = description;
        this.questions = questions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "name='" + name + '\'' +
                ", conditions=" + conditions +
                ", description=" + description +
                ", questions=" + questions +
                '}';
    }
}

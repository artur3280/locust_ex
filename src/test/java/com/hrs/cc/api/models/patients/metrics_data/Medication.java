package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "medications"
})
public class Medication {

    @JsonProperty("medications")
    private List<Medication_> medications = null;

    public Medication() {
    }

    public Medication(List<Medication_> medications) {
        this.medications = medications;
    }

    public List<Medication_> getMedications() {
        return medications;
    }

    public void setMedications(List<Medication_> medications) {
        this.medications = medications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Medication that = (Medication) o;
        return Objects.equals(medications, that.medications);
    }

    @Override
    public int hashCode() {
        return Objects.hash(medications);
    }

    @Override
    public String toString() {
        return "Medication{" +
                "medications=" + medications +
                '}';
    }
}

package com.hrs.cc.api.models.environment.presets;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class Modules {

    @JsonProperty("data")
    private List<Datum_> data = null;

    public Modules() {
    }

    public Modules(List<Datum_> data) {
        this.data = data;
    }

    public List<Datum_> getData() {
        return data;
    }

    public void setData(List<Datum_> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Modules{" +
                "data=" + data +
                '}';
    }
}

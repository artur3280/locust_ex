package com.hrs.cc.api.models.patients.profile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "id",
        "time",
        "group"
})
public class Reviewed {

    @JsonProperty("name")
    private String name;
    @JsonProperty("id")
    private String id;
    @JsonProperty("time")
    private String time;
    @JsonProperty("group")
    private Boolean group;

    public Reviewed() {
    }

    public Reviewed(String name, String id, String time, Boolean group) {
        this.name = name;
        this.id = id;
        this.time = time;
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Boolean getGroup() {
        return group;
    }

    public void setGroup(Boolean group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "Reviewed{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", time='" + time + '\'' +
                ", group=" + group +
                '}';
    }
}

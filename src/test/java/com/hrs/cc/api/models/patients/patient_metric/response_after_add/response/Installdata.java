package com.hrs.cc.api.models.patients.patient_metric.response_after_add.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "updatedisplay",
        "modules",
        "activityreminder",
        "surveyreminder",
        "temperaturereminders",
        "weightreminder",
        "woundimagingreminders",
        "medicationreminders",
        "glucosereminder",
        "pulseoxreminders",
        "bloodpressurereminders",
        "activationhistory"
})
public class Installdata {

    @JsonProperty("updatedisplay")
    private List<String> updatedisplay = null;
    @JsonProperty("modules")
    private List<String> modules = null;
    @JsonProperty("activityreminder")
    private Activityreminder activityreminder;
    @JsonProperty("surveyreminder")
    private Surveyreminder surveyreminder;
    @JsonProperty("temperaturereminders")
    private Temperaturereminders temperaturereminders;
    @JsonProperty("weightreminder")
    private Weightreminder weightreminder;
    @JsonProperty("woundimagingreminders")
    private Woundimagingreminders woundimagingreminders;
    @JsonProperty("medicationreminders")
    private List<Medicationreminder> medicationreminders = null;
    @JsonProperty("glucosereminder")
    private Glucosereminder glucosereminder;
    @JsonProperty("pulseoxreminders")
    private Pulseoxreminders pulseoxreminders;
    @JsonProperty("bloodpressurereminders")
    private Bloodpressurereminders bloodpressurereminders;
    @JsonProperty("activationhistory")
    private List<Activationhistory> activationhistory = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Installdata() {
    }

    public Installdata(List<String> updatedisplay, List<String> modules, Activityreminder activityreminder, Surveyreminder surveyreminder, Temperaturereminders temperaturereminders, Weightreminder weightreminder, Woundimagingreminders woundimagingreminders, List<Medicationreminder> medicationreminders, Glucosereminder glucosereminder, Pulseoxreminders pulseoxreminders, Bloodpressurereminders bloodpressurereminders, List<Activationhistory> activationhistory) {
        this.updatedisplay = updatedisplay;
        this.modules = modules;
        this.activityreminder = activityreminder;
        this.surveyreminder = surveyreminder;
        this.temperaturereminders = temperaturereminders;
        this.weightreminder = weightreminder;
        this.woundimagingreminders = woundimagingreminders;
        this.medicationreminders = medicationreminders;
        this.glucosereminder = glucosereminder;
        this.pulseoxreminders = pulseoxreminders;
        this.bloodpressurereminders = bloodpressurereminders;
        this.activationhistory = activationhistory;
    }

    public List<String> getUpdatedisplay() {
        return updatedisplay;
    }

    public void setUpdatedisplay(List<String> updatedisplay) {
        this.updatedisplay = updatedisplay;
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    public Activityreminder getActivityreminder() {
        return activityreminder;
    }

    public void setActivityreminder(Activityreminder activityreminder) {
        this.activityreminder = activityreminder;
    }

    public Surveyreminder getSurveyreminder() {
        return surveyreminder;
    }

    public void setSurveyreminder(Surveyreminder surveyreminder) {
        this.surveyreminder = surveyreminder;
    }

    public Temperaturereminders getTemperaturereminders() {
        return temperaturereminders;
    }

    public void setTemperaturereminders(Temperaturereminders temperaturereminders) {
        this.temperaturereminders = temperaturereminders;
    }

    public Weightreminder getWeightreminder() {
        return weightreminder;
    }

    public void setWeightreminder(Weightreminder weightreminder) {
        this.weightreminder = weightreminder;
    }

    public Woundimagingreminders getWoundimagingreminders() {
        return woundimagingreminders;
    }

    public void setWoundimagingreminders(Woundimagingreminders woundimagingreminders) {
        this.woundimagingreminders = woundimagingreminders;
    }

    public List<Medicationreminder> getMedicationreminders() {
        return medicationreminders;
    }

    public void setMedicationreminders(List<Medicationreminder> medicationreminders) {
        this.medicationreminders = medicationreminders;
    }

    public Glucosereminder getGlucosereminder() {
        return glucosereminder;
    }

    public void setGlucosereminder(Glucosereminder glucosereminder) {
        this.glucosereminder = glucosereminder;
    }

    public Pulseoxreminders getPulseoxreminders() {
        return pulseoxreminders;
    }

    public void setPulseoxreminders(Pulseoxreminders pulseoxreminders) {
        this.pulseoxreminders = pulseoxreminders;
    }

    public Bloodpressurereminders getBloodpressurereminders() {
        return bloodpressurereminders;
    }

    public void setBloodpressurereminders(Bloodpressurereminders bloodpressurereminders) {
        this.bloodpressurereminders = bloodpressurereminders;
    }

    public List<Activationhistory> getActivationhistory() {
        return activationhistory;
    }

    public void setActivationhistory(List<Activationhistory> activationhistory) {
        this.activationhistory = activationhistory;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Installdata that = (Installdata) o;
        return Objects.equals(updatedisplay, that.updatedisplay) &&
                Objects.equals(modules, that.modules) &&
                Objects.equals(activityreminder, that.activityreminder) &&
                Objects.equals(surveyreminder, that.surveyreminder) &&
                Objects.equals(temperaturereminders, that.temperaturereminders) &&
                Objects.equals(weightreminder, that.weightreminder) &&
                Objects.equals(woundimagingreminders, that.woundimagingreminders) &&
                Objects.equals(medicationreminders, that.medicationreminders) &&
                Objects.equals(glucosereminder, that.glucosereminder) &&
                Objects.equals(pulseoxreminders, that.pulseoxreminders) &&
                Objects.equals(bloodpressurereminders, that.bloodpressurereminders) &&
                Objects.equals(activationhistory, that.activationhistory) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(updatedisplay, modules, activityreminder, surveyreminder, temperaturereminders, weightreminder, woundimagingreminders, medicationreminders, glucosereminder, pulseoxreminders, bloodpressurereminders, activationhistory, additionalProperties);
    }

    @Override
    public String toString() {
        return "Installdata{" +
                "updatedisplay=" + updatedisplay +
                ", modules=" + modules +
                ", activityreminder=" + activityreminder +
                ", surveyreminder=" + surveyreminder +
                ", temperaturereminders=" + temperaturereminders +
                ", weightreminder=" + weightreminder +
                ", woundimagingreminders=" + woundimagingreminders +
                ", medicationreminders=" + medicationreminders +
                ", glucosereminder=" + glucosereminder +
                ", pulseoxreminders=" + pulseoxreminders +
                ", bloodpressurereminders=" + bloodpressurereminders +
                ", activationhistory=" + activationhistory +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.requests.add_metrics_integreation_app.glucose_manual;

public class FacetGlucose {
    private Integer bloodsugar;

    /**
     * No args constructor for use in serialization
     *
     */
    public FacetGlucose() {
    }

    /**
     *
     * @param bloodsugar
     */
    public FacetGlucose(Integer bloodsugar) {
        super();
        this.bloodsugar = bloodsugar;
    }

    public Integer getBloodsugar() {
        return bloodsugar;
    }

    public void setBloodsugar(Integer weight) {
        this.bloodsugar = weight;
    }

    @Override
    public String toString() {
        return "FacetGlucose{" +
                "bloodsugar=" + bloodsugar +
                '}';
    }
}

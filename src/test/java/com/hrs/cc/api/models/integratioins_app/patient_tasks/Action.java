package com.hrs.cc.api.models.integratioins_app.patient_tasks;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "goal",
        "name",
        "medicationId",
        "dosage",
        "strength",
        "units",
        "count",
        "route",
        "surveyId",
        "question",
        "choices",
        "onetime",
        "discharge"
})
public class Action {

    @JsonProperty("goal")
    private Integer goal;
    @JsonProperty("name")
    private String name;
    @JsonProperty("medicationId")
    private String medicationId;
    @JsonProperty("dosage")
    private String dosage;
    @JsonProperty("strength")
    private Integer strength;
    @JsonProperty("units")
    private String units;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("route")
    private String route;
    @JsonProperty("surveyId")
    private String surveyId;
    @JsonProperty("question")
    private String question;
    @JsonProperty("choices")
    private List<Choice> choices = null;
    @JsonProperty("onetime")
    private Boolean onetime;
    @JsonProperty("discharge")
    private Boolean discharge;
    @JsonProperty("answered")
    private Boolean answered;

    public Action() {
    }

    public Action(Integer goal, String name, String medicationId, String dosage, Integer strength, String units, Integer count, String route, String surveyId, String question, List<Choice> choices, Boolean onetime, Boolean discharge, Boolean answered) {
        this.goal = goal;
        this.name = name;
        this.medicationId = medicationId;
        this.dosage = dosage;
        this.strength = strength;
        this.units = units;
        this.count = count;
        this.route = route;
        this.surveyId = surveyId;
        this.question = question;
        this.choices = choices;
        this.onetime = onetime;
        this.discharge = discharge;
        this.answered = answered;
    }

    public Integer getGoal() {
        return goal;
    }

    public void setGoal(Integer goal) {
        this.goal = goal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(String medicationId) {
        this.medicationId = medicationId;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public Boolean getOnetime() {
        return onetime;
    }

    public void setOnetime(Boolean onetime) {
        this.onetime = onetime;
    }

    public Boolean getDischarge() {
        return discharge;
    }

    public void setDischarge(Boolean discharge) {
        this.discharge = discharge;
    }

    public Boolean getAnswered() {
        return answered;
    }

    public void setAnswered(Boolean answered) {
        this.answered = answered;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Action action = (Action) o;
        return Objects.equals(goal, action.goal) &&
                Objects.equals(name, action.name) &&
                Objects.equals(medicationId, action.medicationId) &&
                Objects.equals(dosage, action.dosage) &&
                Objects.equals(strength, action.strength) &&
                Objects.equals(units, action.units) &&
                Objects.equals(count, action.count) &&
                Objects.equals(route, action.route) &&
                Objects.equals(surveyId, action.surveyId) &&
                Objects.equals(question, action.question) &&
                Objects.equals(choices, action.choices) &&
                Objects.equals(onetime, action.onetime) &&
                Objects.equals(discharge, action.discharge) &&
                Objects.equals(answered, action.answered);
    }

    @Override
    public int hashCode() {
        return Objects.hash(goal, name, medicationId, dosage, strength, units, count, route, surveyId, question, choices, onetime, discharge, answered);
    }

    @Override
    public String toString() {
        return "Action{" +
                "goal=" + goal +
                ", name='" + name + '\'' +
                ", medicationId='" + medicationId + '\'' +
                ", dosage='" + dosage + '\'' +
                ", strength=" + strength +
                ", units='" + units + '\'' +
                ", count=" + count +
                ", route='" + route + '\'' +
                ", surveyId='" + surveyId + '\'' +
                ", question='" + question + '\'' +
                ", choices=" + choices +
                ", onetime=" + onetime +
                ", discharge=" + discharge +
                ", answered=" + answered +
                '}';
    }
}
package com.hrs.cc.api.models.environment.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "subgroups",
        "phone"
})
public class Settings {
    @JsonProperty("subgroups")
    private List<String> subgroups = null;
    @JsonProperty("phone")
    private String phone;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Settings() {
    }

    public Settings(List<String> subgroups, String phone) {
        this.subgroups = subgroups;
        this.phone = phone;
    }

    public List<String> getSubgroups() {
        return subgroups;
    }

    public void setSubgroups(List<String> subgroups) {
        this.subgroups = subgroups;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Settings settings = (Settings) o;
        return Objects.equals(subgroups, settings.subgroups) &&
                Objects.equals(phone, settings.phone) &&
                Objects.equals(additionalProperties, settings.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(subgroups, phone, additionalProperties);
    }

    @Override
    public String toString() {
        return "Settings{" +
                "subgroups=" + subgroups +
                ", phone='" + phone + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

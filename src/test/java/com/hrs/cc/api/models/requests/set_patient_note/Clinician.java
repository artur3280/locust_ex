package com.hrs.cc.api.models.requests.set_patient_note;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name"
})
public class Clinician {

    @JsonProperty("name")
    private Name name;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Clinician() {
    }

    public Clinician(Name name) {
        this.name = name;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clinician clinician = (Clinician) o;
        return Objects.equals(name, clinician.name) &&
                Objects.equals(additionalProperties, clinician.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, additionalProperties);
    }

    @Override
    public String toString() {
        return "Clinician{" +
                "name=" + name +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

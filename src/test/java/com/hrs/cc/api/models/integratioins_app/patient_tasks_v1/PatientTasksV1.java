package com.hrs.cc.api.models.integratioins_app.patient_tasks_v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class PatientTasksV1 {

    @JsonProperty("data")
    private List<Datum> data = null;

    public PatientTasksV1() {
    }

    public PatientTasksV1(List<Datum> data) {
        this.data = data;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PatientTasksV1{" +
                "data=" + data +
                '}';
    }
}

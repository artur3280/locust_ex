package com.hrs.cc.api.models.admin.surveys;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "language",
        "question"
})
public class Translation {

    @JsonProperty("language")
    private String language;
    @JsonProperty("question")
    private String question;

    public Translation() {
    }

    public Translation(String language, String question) {
        this.language = language;
        this.question = question;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "Translation{" +
                "language='" + language + '\'' +
                ", question='" + question + '\'' +
                '}';
    }
}

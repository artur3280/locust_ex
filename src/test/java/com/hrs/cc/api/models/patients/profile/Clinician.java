package com.hrs.cc.api.models.patients.profile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "hrsid"
})
public class Clinician {

    @JsonProperty("name")
    private String name;
    @JsonProperty("hrsid")
    private String hrsid;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Clinician() {
    }

    public Clinician(String name, String hrsid) {
        this.name = name;
        this.hrsid = hrsid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clinician clinician = (Clinician) o;
        return Objects.equals(name, clinician.name) &&
                Objects.equals(hrsid, clinician.hrsid) &&
                Objects.equals(additionalProperties, clinician.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, hrsid, additionalProperties);
    }

    @Override
    public String toString() {
        return "Clinician{" +
                "name='" + name + '\'' +
                ", hrsid='" + hrsid + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

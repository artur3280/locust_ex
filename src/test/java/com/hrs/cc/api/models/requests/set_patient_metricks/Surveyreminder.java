package com.hrs.cc.api.models.requests.set_patient_metricks;

import java.util.Objects;

public class Surveyreminder {
    private String window = "60";
    private String time = "600";

    public Surveyreminder() {
    }

    public Surveyreminder(String window, String time) {
        this.window = window;
        this.time = time;
    }

    public String getWindow() {
        return window;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Surveyreminder that = (Surveyreminder) o;
        return Objects.equals(window, that.window) &&
                Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {

        return Objects.hash(window, time);
    }

    @Override
    public String toString() {
        return "Surveyreminder{" +
                "window='" + window + '\'' +
                ", time=" + time +
                '}';
    }
}

package com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_all_fields;

public class FacetMedicationAllFields {
    private String medication;
    private Integer strength;
    private String units;
    private Integer count;
    private String route;

    /**
     * No args constructor for use in serialization
     *
     */
    public FacetMedicationAllFields() {
    }

    public FacetMedicationAllFields(String medication, Integer strength, String units, Integer count, String route) {
        this.medication = medication;
        this.strength = strength;
        this.units = units;
        this.count = count;
        this.route = route;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    @Override
    public String toString() {
        return "FacetMedication{" +
                "medication='" + medication + '\'' +
                ", strength=" + strength +
                ", units='" + units + '\'' +
                ", count=" + count +
                ", route='" + route + '\'' +
                '}';
    }
}

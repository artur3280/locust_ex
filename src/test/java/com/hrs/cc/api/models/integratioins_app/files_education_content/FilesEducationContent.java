package com.hrs.cc.api.models.integratioins_app.files_education_content;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class FilesEducationContent {

    @JsonProperty("data")
    private List<FileObject> data = null;

    public FilesEducationContent() {
    }

    public FilesEducationContent(List<FileObject> data) {
        this.data = data;
    }

    public List<FileObject> getData() {
        return data;
    }

    public void setData(List<FileObject> data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilesEducationContent that = (FilesEducationContent) o;
        return Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "FilesEducationContent{" +
                "data=" + data +
                '}';
    }
}

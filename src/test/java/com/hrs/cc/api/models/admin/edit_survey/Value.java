package com.hrs.cc.api.models.admin.edit_survey;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "lang",
        "text"
})
public class Value {

    @JsonProperty("lang")
    private String lang;
    @JsonProperty("text")
    private List<String> text = null;

    public Value() {
    }

    public Value(String lang, List<String> text) {
        this.lang = lang;
        this.text = text;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Value{" +
                "lang='" + lang + '\'' +
                ", text=" + text +
                '}';
    }
}

package com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_all_fields;

public class DataMedicationAllFields {
    private String owner;
    private String entered;
    private String metric;
    private String device = null;
    private String remindAt;
    private String takenAt;
    private FacetMedicationAllFields facet;

    public DataMedicationAllFields() {
    }

    public DataMedicationAllFields(String owner, String entered, String metric, String device, String remindAt, String takenAt, FacetMedicationAllFields facet) {
        this.owner = owner;
        this.entered = entered;
        this.metric = metric;
        this.device = device;
        this.remindAt = remindAt;
        this.takenAt = takenAt;
        this.facet = facet;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getEntered() {
        return entered;
    }

    public void setEntered(String entered) {
        this.entered = entered;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getRemindAt() {
        return remindAt;
    }

    public void setRemindAt(String remindAt) {
        this.remindAt = remindAt;
    }

    public String getTakenAt() {
        return takenAt;
    }

    public void setTakenAt(String takenAt) {
        this.takenAt = takenAt;
    }

    public FacetMedicationAllFields getFacet() {
        return facet;
    }

    public void setFacet(FacetMedicationAllFields facet) {
        this.facet = facet;
    }

    @Override
    public String toString() {
        return "DataMedication{" +
                "owner='" + owner + '\'' +
                ", entered='" + entered + '\'' +
                ", metric='" + metric + '\'' +
                ", device='" + device + '\'' +
                ", remindAt='" + remindAt + '\'' +
                ", takenAt='" + takenAt + '\'' +
                ", facet=" + facet +
                '}';
    }
}

package com.hrs.cc.api.models.patients.education;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "auth",
        "data"
})
public class Education {

    @JsonProperty("status")
    private String status;
    @JsonProperty("auth")
    private Auth auth;
    @JsonProperty("data")
    private Data data;

    public Education() {
    }

    public Education(String status, Auth auth, Data data) {
        this.status = status;
        this.auth = auth;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Education education = (Education) o;
        return Objects.equals(status, education.status) &&
                Objects.equals(auth, education.auth) &&
                Objects.equals(data, education.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, auth, data);
    }

    @Override
    public String toString() {
        return "Education{" +
                "status='" + status + '\'' +
                ", auth=" + auth +
                ", data=" + data +
                '}';
    }
}

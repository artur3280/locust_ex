package com.hrs.cc.api.models.requests.set_patient_metricks.blood_pressure;

import java.util.Objects;

public class SetBloodPressure {
    private String type = "bloodpressure";
    private String reason;
    private String ftime;
    private Integer systolic;
    private Integer diastolic;
    private Integer heartrate;

    public SetBloodPressure() {
    }

    public SetBloodPressure(String reason, String ftime, Integer systolic, Integer diastolic, Integer heartrate) {
        this.reason = reason;
        this.ftime = ftime;
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.heartrate = heartrate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFtime() {
        return ftime;
    }

    public void setFtime(String ftime) {
        this.ftime = ftime;
    }

    public Integer getSystolic() {
        return systolic;
    }

    public void setSystolic(Integer systolic) {
        this.systolic = systolic;
    }

    public Integer getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(Integer diastolic) {
        this.diastolic = diastolic;
    }

    public Integer getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(Integer heartrate) {
        this.heartrate = heartrate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SetBloodPressure that = (SetBloodPressure) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(reason, that.reason) &&
                Objects.equals(ftime, that.ftime) &&
                Objects.equals(systolic, that.systolic) &&
                Objects.equals(diastolic, that.diastolic) &&
                Objects.equals(heartrate, that.heartrate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, reason, ftime, systolic, diastolic, heartrate);
    }

    @Override
    public String toString() {
        return "SetBloodPressure{" +
                "type='" + type + '\'' +
                ", reason='" + reason + '\'' +
                ", ftime='" + ftime + '\'' +
                ", systolic=" + systolic +
                ", diastolic=" + diastolic +
                ", heartrate=" + heartrate +
                '}';
    }
}

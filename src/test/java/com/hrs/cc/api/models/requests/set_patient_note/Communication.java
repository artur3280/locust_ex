package com.hrs.cc.api.models.requests.set_patient_note;

import java.util.List;
import java.util.Objects;

public class Communication {
    private List<String> result = null;
    private List<String> followup = null;
    private List<String> itproblems = null;
    private String type;

    public Communication() {
    }

    public Communication(List<String> result, List<String> followup, List<String> itproblems, String type) {
        this.result = result;
        this.followup = followup;
        this.itproblems = itproblems;
        this.type = type;
    }

    public List<String> getResult() {
        return result;
    }

    public void setResult(List<String> result) {
        this.result = result;
    }

    public List<String> getFollowup() {
        return followup;
    }

    public void setFollowup(List<String> followup) {
        this.followup = followup;
    }

    public List<String> getItproblems() {
        return itproblems;
    }

    public void setItproblems(List<String> itproblems) {
        this.itproblems = itproblems;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Communication that = (Communication) o;
        return Objects.equals(result, that.result) &&
                Objects.equals(followup, that.followup) &&
                Objects.equals(itproblems, that.itproblems) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {

        return Objects.hash(result, followup, itproblems, type);
    }

    @Override
    public String toString() {
        return "Communication{" +
                "result=" + result +
                ", followup=" + followup +
                ", itproblems=" + itproblems +
                ", type='" + type + '\'' +
                '}';
    }
}

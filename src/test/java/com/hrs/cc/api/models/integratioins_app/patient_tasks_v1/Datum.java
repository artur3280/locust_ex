package com.hrs.cc.api.models.integratioins_app.patient_tasks_v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "resourceType",
        "attributes"
})
public class Datum {

    @JsonProperty("id")
    private String id;
    @JsonProperty("resourceType")
    private String resourceType;
    @JsonProperty("attributes")
    private Attributes attributes;

    public Datum() {
    }

    public Datum(String id, String resourceType, Attributes attributes) {
        this.id = id;
        this.resourceType = resourceType;
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "id='" + id + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", attributes=" + attributes +
                '}';
    }
}

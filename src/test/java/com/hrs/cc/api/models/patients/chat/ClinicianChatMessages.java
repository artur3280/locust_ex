package com.hrs.cc.api.models.patients.chat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "title",
        "chatid",
        "chatdata"
})
public class ClinicianChatMessages {
    @JsonProperty("title")
    private String title;
    @JsonProperty("chatid")
    private String chatid;
    @JsonProperty("chatdata")
    private Map<String, Message > chatdata = new HashMap<>();

    public ClinicianChatMessages(String title, String chatid, Map<String, Message> chatdata) {
        this.title = title;
        this.chatid = chatid;
        this.chatdata = chatdata;
    }

    public ClinicianChatMessages() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChatid() {
        return chatid;
    }

    public void setChatid(String chatid) {
        this.chatid = chatid;
    }

    public Map<String, Message> getChatdata() {
        return chatdata;
    }

    public void setChatdata(Map<String, Message> chatdata) {
        this.chatdata = chatdata;
    }
}

package com.hrs.cc.api.models.patients.dashboard_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "sys",
        "dia",
        "hr",
        "ts"
})
public class Bp {

    @JsonProperty("active")
    private String active;
    @JsonProperty("sys")
    private String sys;
    @JsonProperty("dia")
    private String dia;
    @JsonProperty("hr")
    private String hr;
    @JsonProperty("ts")
    private String ts;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Bp() {
    }

    public Bp(String active, String sys, String dia, String hr, String ts) {
        this.active = active;
        this.sys = sys;
        this.dia = dia;
        this.hr = hr;
        this.ts = ts;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getSys() {
        return sys;
    }

    public void setSys(String sys) {
        this.sys = sys;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHr() {
        return hr;
    }

    public void setHr(String hr) {
        this.hr = hr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bp bp = (Bp) o;
        return Objects.equals(active, bp.active) &&
                Objects.equals(sys, bp.sys) &&
                Objects.equals(dia, bp.dia) &&
                Objects.equals(hr, bp.hr) &&
                Objects.equals(ts, bp.ts) &&
                Objects.equals(additionalProperties, bp.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(active, sys, dia, hr, ts, additionalProperties);
    }

    @Override
    public String toString() {
        return "Bp{" +
                "active='" + active + '\'' +
                ", sys='" + sys + '\'' +
                ", dia='" + dia + '\'' +
                ", hr='" + hr + '\'' +
                ", ts='" + ts + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

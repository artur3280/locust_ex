package com.hrs.cc.api.models.caregiver.patient_metrics;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "hrsid"
})
public class Profile {

    @JsonProperty("hrsid")
    private String hrsid;

    public Profile() {
    }

    public Profile(String hrsid) {
        this.hrsid = hrsid;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profile profile = (Profile) o;
        return Objects.equals(hrsid, profile.hrsid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hrsid);
    }

    @Override
    public String toString() {
        return "Profile{" +
                "hrsid='" + hrsid + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "systolic",
        "diastolic",
        "data",
        "deleted",
        "heartrate",
        "ts"
})
public class Bloodpressure {

    @JsonProperty("systolic")
    private Integer systolic;
    @JsonProperty("diastolic")
    private Integer diastolic;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("data")
    private List<Datum_____> data = null;
    @JsonProperty("heartrate")
    private Integer heartrate;
    @JsonProperty("ts")
    private Long ts;

    public Bloodpressure() {
    }

    public Bloodpressure(Integer systolic, Integer diastolic, Boolean deleted, List<Datum_____> data, Integer heartrate, Long ts) {
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.deleted = deleted;
        this.data = data;
        this.heartrate = heartrate;
        this.ts = ts;
    }

    public Integer getSystolic() {
        return systolic;
    }

    public void setSystolic(Integer systolic) {
        this.systolic = systolic;
    }

    public Integer getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(Integer diastolic) {
        this.diastolic = diastolic;
    }

    public List<Datum_____> getData() {
        return data;
    }

    public void setData(List<Datum_____> data) {
        this.data = data;
    }

    public Integer getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(Integer heartrate) {
        this.heartrate = heartrate;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Bloodpressure{" +
                "systolic=" + systolic +
                ", diastolic=" + diastolic +
                ", deleted=" + deleted +
                ", data=" + data +
                ", heartrate=" + heartrate +
                ", ts=" + ts +
                '}';
    }
}

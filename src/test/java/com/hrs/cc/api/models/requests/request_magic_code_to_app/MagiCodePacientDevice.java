package com.hrs.cc.api.models.requests.request_magic_code_to_app;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class MagiCodePacientDevice {
    @JsonProperty("data")
    private Datum data;

    public MagiCodePacientDevice() {
    }

    public MagiCodePacientDevice(Datum data) {
        this.data = data;
    }

    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MagiCodePacientDevice magiCodePacientDevice = (MagiCodePacientDevice) o;
        return Objects.equals(data, magiCodePacientDevice.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "ClinicianAuthToken{" +
                "data=" + data +
                '}';
    }
}

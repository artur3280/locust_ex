package com.hrs.cc.api.models.patients.dashboard_list;

import com.hrs.cc.api.models.patients.dashboard_list.riskdetails.RiskDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "days",
        "hrsid",
        "pid",
        "status",
        "conditions",
        "subgroup",
        "connectedclinician",
        "fname",
        "mname",
        "lname",
        "reviewedlist",
        "risk",
        "highriskyesterday",
        "highrisktoday",
        "riskdetails",
        "meds",
        "activity",
        "bp",
        "glucose",
        "pulseox",
        "survey",
        "weight",
        "temperature",
        "clinicianchange",
        "woundimaging",
        "steps"
})
public class PatientList {

    @JsonProperty("date")
    private String date;
    @JsonProperty("days")
    private String days;
    @JsonProperty("hrsid")
    private String hrsid;
    @JsonProperty("pid")
    private String pid;
    @JsonProperty("status")
    private String status;
    @JsonProperty("conditions")
    private List<String> conditions = null;
    @JsonProperty("subgroup")
    private String subgroup;
    @JsonProperty("connectedclinician")
    private ConnectedClinician connectedclinician;
    @JsonProperty("fname")
    private String fname;
    @JsonProperty("mname")
    private String mname;
    @JsonProperty("lname")
    private String lname;
    @JsonProperty("reviewedlist")
    private List<Reviewedlist> reviewedlist = null;
    @JsonProperty("risk")
    private String risk;
    @JsonProperty("highriskyesterday")
    private String highriskyesterday;
    @JsonProperty("highrisktoday")
    private String highrisktoday;
    @JsonProperty("riskdetails")
    private RiskDetails riskdetails;
    @JsonProperty("meds")
    private Meds meds;
    @JsonProperty("activity")
    private Activity activity;
    @JsonProperty("bp")
    private Bp bp;
    @JsonProperty("glucose")
    private Glucose glucose;
    @JsonProperty("pulseox")
    private Pulseox pulseox;
    @JsonProperty("survey")
    private Survey survey;
    @JsonProperty("weight")
    private Weight weight;
    @JsonProperty("temperature")
    private Temperature temperature;
    @JsonProperty("woundimaging")
    private WoundImaging woundimaging;
    @JsonProperty("discharge")
    private String discharge;
    @JsonProperty("clinicianchange")
    private ClinicianChange clinicianchange;
    @JsonProperty("steps")
    private Steps steps;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public PatientList() {
    }

    public PatientList(String date, String days, String hrsid, String pid, String status, List<String> conditions, String subgroup, ConnectedClinician connectedclinician, String fname, String mname, String lname, List<Reviewedlist> reviewedlist, String risk, String highriskyesterday, String highrisktoday, RiskDetails riskdetails, Meds meds, Activity activity, Bp bp, Glucose glucose, Pulseox pulseox, Survey survey, Weight weight, Temperature temperature, WoundImaging woundimaging, String discharge, ClinicianChange clinicianchange, Steps steps) {
        this.date = date;
        this.days = days;
        this.hrsid = hrsid;
        this.pid = pid;
        this.status = status;
        this.conditions = conditions;
        this.subgroup = subgroup;
        this.connectedclinician = connectedclinician;
        this.fname = fname;
        this.mname = mname;
        this.lname = lname;
        this.reviewedlist = reviewedlist;
        this.risk = risk;
        this.highriskyesterday = highriskyesterday;
        this.highrisktoday = highrisktoday;
        this.riskdetails = riskdetails;
        this.meds = meds;
        this.activity = activity;
        this.bp = bp;
        this.glucose = glucose;
        this.pulseox = pulseox;
        this.survey = survey;
        this.weight = weight;
        this.temperature = temperature;
        this.woundimaging = woundimaging;
        this.discharge = discharge;
        this.clinicianchange = clinicianchange;
        this.steps = steps;
    }

    public Steps getSteps() {
        return steps;
    }

    public void setSteps(Steps steps) {
        this.steps = steps;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public String getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(String subgroup) {
        this.subgroup = subgroup;
    }

    public ConnectedClinician getConnectedclinician() {
        return connectedclinician;
    }

    public void setConnectedclinician(ConnectedClinician connectedclinician) {
        this.connectedclinician = connectedclinician;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public RiskDetails getRiskdetails() {
        return riskdetails;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public List<Reviewedlist> getReviewedlist() {
        return reviewedlist;
    }

    public void setReviewedlist(List<Reviewedlist> reviewedlist) {
        this.reviewedlist = reviewedlist;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public String getHighriskyesterday() {
        return highriskyesterday;
    }

    public void setHighriskyesterday(String highriskyesterday) {
        this.highriskyesterday = highriskyesterday;
    }

    public String getHighrisktoday() {
        return highrisktoday;
    }

    public void setHighrisktoday(String highrisktoday) {
        this.highrisktoday = highrisktoday;
    }

    public RiskDetails getRiskDetails() {
        return riskdetails;
    }

    public void setRiskdetails(RiskDetails riskdetails) {
        this.riskdetails = riskdetails;
    }

    public Meds getMeds() {
        return meds;
    }

    public void setMeds(Meds meds) {
        this.meds = meds;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Bp getBp() {
        return bp;
    }

    public void setBp(Bp bp) {
        this.bp = bp;
    }

    public Glucose getGlucose() {
        return glucose;
    }

    public void setGlucose(Glucose glucose) {
        this.glucose = glucose;
    }

    public Pulseox getPulseox() {
        return pulseox;
    }

    public void setPulseox(Pulseox pulseox) {
        this.pulseox = pulseox;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public WoundImaging getWoundimaging() {
        return woundimaging;
    }

    public void setWoundimaging(WoundImaging woundimaging) {
        this.woundimaging = woundimaging;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getDischarge() {
        return discharge;
    }

    public void setDischarge(String discharge) {
        this.discharge = discharge;
    }

    public ClinicianChange getClinicianchange() {
        return clinicianchange;
    }

    public void setClinicianchange(ClinicianChange clinicianchange) {
        this.clinicianchange = clinicianchange;
    }

    @Override
    public String toString() {
        return "PatientList{" +
                "date='" + date + '\'' +
                ", days='" + days + '\'' +
                ", hrsid='" + hrsid + '\'' +
                ", pid='" + pid + '\'' +
                ", status='" + status + '\'' +
                ", conditions=" + conditions +
                ", subgroup='" + subgroup + '\'' +
                ", connectedclinician=" + connectedclinician +
                ", fname='" + fname + '\'' +
                ", mname='" + mname + '\'' +
                ", lname='" + lname + '\'' +
                ", reviewedlist=" + reviewedlist +
                ", risk='" + risk + '\'' +
                ", highriskyesterday='" + highriskyesterday + '\'' +
                ", highrisktoday='" + highrisktoday + '\'' +
                ", riskdetails=" + riskdetails +
                ", meds=" + meds +
                ", activity=" + activity +
                ", bp=" + bp +
                ", glucose=" + glucose +
                ", pulseox=" + pulseox +
                ", survey=" + survey +
                ", weight=" + weight +
                ", temperature=" + temperature +
                ", woundimaging=" + woundimaging +
                ", discharge='" + discharge + '\'' +
                ", clinicianchange=" + clinicianchange +
                ", steps=" + steps +
                '}';
    }
}

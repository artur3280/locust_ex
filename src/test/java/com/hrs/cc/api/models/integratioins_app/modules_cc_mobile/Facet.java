package com.hrs.cc.api.models.integratioins_app.modules_cc_mobile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "duration",
        "diastolic",
        "systolic",
        "heartrate",
        "glucose",
        "dose",
        "med",
        "spo2",
        "answer",
        "question",
        "temperature",
        "weight",
        "images",
        "files",
        "count"
})
public class Facet {

    @JsonProperty("duration")
    private Duration duration;
    @JsonProperty("diastolic")
    private Diastolic diastolic;
    @JsonProperty("systolic")
    private Systolic systolic;
    @JsonProperty("heartrate")
    private Heartrate heartrate;
    @JsonProperty("glucose")
    private Glucose glucose;
    @JsonProperty("dose")
    private Dose dose;
    @JsonProperty("med")
    private Med med;
    @JsonProperty("spo2")
    private Spo2 spo2;
    @JsonProperty("answer")
    private Answer answer;
    @JsonProperty("question")
    private Question question;
    @JsonProperty("temperature")
    private Temperature temperature;
    @JsonProperty("weight")
    private Weight weight;
    @JsonProperty("images")
    private Images images;
    @JsonProperty("files")
    private Files files;
    @JsonProperty("count")
    private Count count;

    public Facet() {
    }

    public Facet(Duration duration, Diastolic diastolic, Systolic systolic, Heartrate heartrate, Glucose glucose, Dose dose, Med med, Spo2 spo2, Answer answer, Question question, Temperature temperature, Weight weight, Images images, Files files) {
        this.duration = duration;
        this.diastolic = diastolic;
        this.systolic = systolic;
        this.heartrate = heartrate;
        this.glucose = glucose;
        this.dose = dose;
        this.med = med;
        this.spo2 = spo2;
        this.answer = answer;
        this.question = question;
        this.temperature = temperature;
        this.weight = weight;
        this.images = images;
        this.files = files;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Diastolic getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(Diastolic diastolic) {
        this.diastolic = diastolic;
    }

    public Systolic getSystolic() {
        return systolic;
    }

    public void setSystolic(Systolic systolic) {
        this.systolic = systolic;
    }

    public Heartrate getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(Heartrate heartrate) {
        this.heartrate = heartrate;
    }

    public Glucose getGlucose() {
        return glucose;
    }

    public void setGlucose(Glucose glucose) {
        this.glucose = glucose;
    }

    public Dose getDose() {
        return dose;
    }

    public void setDose(Dose dose) {
        this.dose = dose;
    }

    public Med getMed() {
        return med;
    }

    public void setMed(Med med) {
        this.med = med;
    }

    public Spo2 getSpo2() {
        return spo2;
    }

    public void setSpo2(Spo2 spo2) {
        this.spo2 = spo2;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }
}

package com.hrs.cc.api.models.integratioins_app.patient_tasks;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;
import java.util.List;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "answers",
        "language"
})
public class AnswerTranslations {

    @JsonProperty("answers")
    private List<Object> answers = null;
    @JsonProperty("language")
    private String language;

    public AnswerTranslations() {
    }

    public AnswerTranslations(List<Object> answers, String language) {
        this.answers = answers;
        this.language = language;
    }

    public List<Object> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Object> answers) {
        this.answers = answers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnswerTranslations that = (AnswerTranslations) o;
        return Objects.equals(answers, that.answers) &&
                Objects.equals(language, that.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(answers, language);
    }

    @Override
    public String toString() {
        return "AnswerTranslations{" +
                "answers='" + answers + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}

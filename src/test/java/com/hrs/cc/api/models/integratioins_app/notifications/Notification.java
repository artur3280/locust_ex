package com.hrs.cc.api.models.integratioins_app.notifications;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "patient",
        "epoch",
        "id",
        "type",
        "status"
})
public class Notification {

    @JsonProperty("patient")
    private Patient patient;
    @JsonProperty("epoch")
    private Integer epoch;
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private Integer type;
    @JsonProperty("status")
    private String status;

    public Notification() {
    }

    public Notification(Patient patient, Integer epoch, String id, Integer type, String status) {
        this.patient = patient;
        this.epoch = epoch;
        this.id = id;
        this.type = type;
        this.status = status;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Integer getEpoch() {
        return epoch;
    }

    public void setEpoch(Integer epoch) {
        this.epoch = epoch;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "patient=" + patient +
                ", epoch=" + epoch +
                ", id='" + id + '\'' +
                ", type=" + type +
                ", status='" + status + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.integratioins_app.connected_devices;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "identity",
        "agent",
        "source",
        "created",
        "expires",
        "lastUsed",
        "blacklisted"
})

public class DataDevices {
    @JsonProperty("id")
    private String id;
    @JsonProperty("identity")
    private String identity;
    @JsonProperty("agent")
    private String agent;
    @JsonProperty("source")
    private String source;
    @JsonProperty("created")
    private String created;
    @JsonProperty("expires")
    private String expires;
    @JsonProperty("lastUsed")
    private String lastUsed;
    @JsonProperty("blacklisted")
    private Object blacklisted;

    public DataDevices() {
    }

    public DataDevices(String id, String identity, String agent, String source, String created, String expires, String lastUsed, Object blacklisted) {
        this.id = id;
        this.identity = identity;
        this.agent = agent;
        this.source = source;
        this.created = created;
        this.expires = expires;
        this.lastUsed = lastUsed;
        this.blacklisted = blacklisted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(String lastUsed) {
        this.lastUsed = lastUsed;
    }

    public Object getBlacklisted() {
        return blacklisted;
    }

    public void setBlacklisted(Object blacklisted) {
        this.blacklisted = blacklisted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataDevices that = (DataDevices) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(identity, that.identity) &&
                Objects.equals(agent, that.agent) &&
                Objects.equals(source, that.source) &&
                Objects.equals(created, that.created) &&
                Objects.equals(expires, that.expires) &&
                Objects.equals(lastUsed, that.lastUsed) &&
                Objects.equals(blacklisted, that.blacklisted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, identity, agent, source, created, expires, lastUsed, blacklisted);
    }

    @Override
    public String toString() {
        return "DataDevices{" +
                "id='" + id + '\'' +
                ", identity='" + identity + '\'' +
                ", agent='" + agent + '\'' +
                ", source='" + source + '\'' +
                ", created='" + created + '\'' +
                ", expires='" + expires + '\'' +
                ", lastUsed='" + lastUsed + '\'' +
                ", blacklisted=" + blacklisted +
                '}';
    }
}

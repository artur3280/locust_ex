package com.hrs.cc.api.models.environment.presets;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "modules"
})
public class Relationships {

    @JsonProperty("modules")
    private Modules modules;

    public Relationships() {
    }

    public Relationships(Modules modules) {
        this.modules = modules;
    }

    public Modules getModules() {
        return modules;
    }

    public void setModules(Modules modules) {
        this.modules = modules;
    }

    @Override
    public String toString() {
        return "Relationships{" +
                "modules=" + modules +
                '}';
    }
}

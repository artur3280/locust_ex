package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.hrs.cc.api.models.patients.patient_list.QuickNote;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "clinician",
        "devicehistory",
        "extension",
        "gender",
        "readmission",
        "edvisit",
        "testpatient",
        "lasthospitalization",
        "pid",
        "language",
        "hrsid",
        "startdate",
        "lastmoduleupdate",
        "volume",
        "phone",
        "byodDevice",
        "dob",
        "name",
        "envphone",
        "conditions",
        "audioreminders",
        "quicknote",
        "status",
        "reviewed",
        "device",
        "pcv",
        "alternatetelephone",
        "alternatefirstname",
        "alternatemiddlename",
        "alternatelastname",
        "alternaterelationship"
})
public class Profile {

    @JsonProperty("clinician")
    private Clinician clinician;
    @JsonProperty("devicehistory")
    private List<Devicehistory> devicehistory = null;
    @JsonProperty("extension")
    private String extension;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("readmission")
    private Integer readmission;
    @JsonProperty("edvisit")
    private Integer edvisit;
    @JsonProperty("testpatient")
    private Boolean testpatient;
    @JsonProperty("lasthospitalization")
    private Long lasthospitalization;
    @JsonProperty("pid")
    private String pid;
    @JsonProperty("language")
    private String language;
    @JsonProperty("hrsid")
    private String hrsid;
    @JsonProperty("startdate")
    private Long startdate;
    @JsonProperty("lastmoduleupdate")
    private Long lastmoduleupdate;
    @JsonProperty("volume")
    private Integer volume;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("byodDevice")
    private ByodDevice byodDevice;
    @JsonProperty("dob")
    private String dob;
    @JsonProperty("name")
    private Name name;
    @JsonProperty("envphone")
    private String envphone;
    @JsonProperty("conditions")
    private List<String> conditions = null;
    @JsonProperty("audioreminders")
    private Boolean audioreminders;
    @JsonProperty("status")
    private String status;
    @JsonProperty("quicknote")
    private QuickNote quicknote;
    @JsonProperty("device")
    private Object device;
    @JsonProperty("reviewed")
    private Object reviewed;
    @JsonProperty("pcv")
    private Pcv pcv;
    @JsonProperty("alternatetelephone")
    private String alternatetelephone;
    @JsonProperty("alternatefirstname")
    private String alternatefirstname;
    @JsonProperty("alternatemiddlename")
    private String alternatemiddlename;
    @JsonProperty("alternatelastname")
    private String alternatelastname;
    @JsonProperty("alternaterelationship")
    private String alternaterelationship;

    public Profile() {
    }

    public Profile(Clinician clinician, List<Devicehistory> devicehistory, String extension, String gender, Integer readmission, Integer edvisit, Boolean testpatient, Long lasthospitalization, String pid, String language, String hrsid, Long startdate, Long lastmoduleupdate, Integer volume, String phone, ByodDevice byodDevice, String dob, Name name, String envphone, List<String> conditions, Boolean audioreminders, String status, QuickNote quicknote, Object device, Object reviewed, Pcv pcv, String alternatetelephone, String alternatefirstname, String alternatemiddlename, String alternatelastname, String alternaterelationship) {
        this.clinician = clinician;
        this.devicehistory = devicehistory;
        this.extension = extension;
        this.gender = gender;
        this.readmission = readmission;
        this.edvisit = edvisit;
        this.testpatient = testpatient;
        this.lasthospitalization = lasthospitalization;
        this.pid = pid;
        this.language = language;
        this.hrsid = hrsid;
        this.startdate = startdate;
        this.lastmoduleupdate = lastmoduleupdate;
        this.volume = volume;
        this.phone = phone;
        this.byodDevice = byodDevice;
        this.dob = dob;
        this.name = name;
        this.envphone = envphone;
        this.conditions = conditions;
        this.audioreminders = audioreminders;
        this.status = status;
        this.quicknote = quicknote;
        this.device = device;
        this.reviewed = reviewed;
        this.pcv = pcv;
        this.alternatetelephone = alternatetelephone;
        this.alternatefirstname = alternatefirstname;
        this.alternatemiddlename = alternatemiddlename;
        this.alternatelastname = alternatelastname;
        this.alternaterelationship = alternaterelationship;
    }

    public Clinician getClinician() {
        return clinician;
    }

    public void setClinician(Clinician clinician) {
        this.clinician = clinician;
    }

    public List<Devicehistory> getDevicehistory() {
        return devicehistory;
    }

    public void setDevicehistory(List<Devicehistory> devicehistory) {
        this.devicehistory = devicehistory;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getReadmission() {
        return readmission;
    }

    public void setReadmission(Integer readmission) {
        this.readmission = readmission;
    }

    public Integer getEdvisit() {
        return edvisit;
    }

    public void setEdvisit(Integer edvisit) {
        this.edvisit = edvisit;
    }

    public Boolean getTestpatient() {
        return testpatient;
    }

    public void setTestpatient(Boolean testpatient) {
        this.testpatient = testpatient;
    }

    public Long getLasthospitalization() {
        return lasthospitalization;
    }

    public void setLasthospitalization(Long lasthospitalization) {
        this.lasthospitalization = lasthospitalization;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public Long getStartdate() {
        return startdate;
    }

    public void setStartdate(Long startdate) {
        this.startdate = startdate;
    }

    public Long getLastmoduleupdate() {
        return lastmoduleupdate;
    }

    public void setLastmoduleupdate(Long lastmoduleupdate) {
        this.lastmoduleupdate = lastmoduleupdate;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public ByodDevice getByodDevice() {
        return byodDevice;
    }

    public void setByodDevice(ByodDevice byodDevice) {
        this.byodDevice = byodDevice;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getEnvphone() {
        return envphone;
    }

    public void setEnvphone(String envphone) {
        this.envphone = envphone;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public Boolean getAudioreminders() {
        return audioreminders;
    }

    public void setAudioreminders(Boolean audioreminders) {
        this.audioreminders = audioreminders;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public QuickNote getQuicknote() {
        return quicknote;
    }

    public void setQuicknote(QuickNote quicknote) {
        this.quicknote = quicknote;
    }

    public Object getDevice() {
        return device;
    }

    public void setDevice(Object device) {
        this.device = device;
    }

    public Object getReviewed() {
        return reviewed;
    }

    public void setReviewed(Object reviewed) {
        this.reviewed = reviewed;
    }

    public Pcv getPcv() {
        return pcv;
    }

    public void setPcv(Pcv pcv) {
        this.pcv = pcv;
    }

    public String getAlternatetelephone() {
        return alternatetelephone;
    }

    public void setAlternatetelephone(String alternatetelephone) {
        this.alternatetelephone = alternatetelephone;
    }

    public String getAlternatemiddlename() {
        return alternatemiddlename;
    }

    public void setAlternatemiddlename(String alternatemiddlename) {
        this.alternatemiddlename = alternatemiddlename;
    }

    public String getAlternatefirstname() {
        return alternatefirstname;
    }

    public void setAlternatefirstname(String alternatefirstname) {
        this.alternatefirstname = alternatefirstname;
    }

    public String getAlternatelastname() {
        return alternatelastname;
    }

    public void setAlternatelastname(String alternatelastname) {
        this.alternatelastname = alternatelastname;
    }

    public String getAlternaterelationship() {
        return alternaterelationship;
    }

    public void setAlternaterelationship(String alternaterelationship) {
        this.alternaterelationship = alternaterelationship;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "clinician=" + clinician +
                ", devicehistory=" + devicehistory +
                ", extension='" + extension + '\'' +
                ", gender='" + gender + '\'' +
                ", readmission=" + readmission +
                ", edvisit=" + edvisit +
                ", testpatient=" + testpatient +
                ", lasthospitalization=" + lasthospitalization +
                ", pid='" + pid + '\'' +
                ", language='" + language + '\'' +
                ", hrsid='" + hrsid + '\'' +
                ", startdate=" + startdate +
                ", lastmoduleupdate=" + lastmoduleupdate +
                ", volume=" + volume +
                ", phone='" + phone + '\'' +
                ", byodDevice=" + byodDevice +
                ", dob='" + dob + '\'' +
                ", name=" + name +
                ", envphone='" + envphone + '\'' +
                ", conditions=" + conditions +
                ", audioreminders=" + audioreminders +
                ", status='" + status + '\'' +
                ", quicknote=" + quicknote +
                ", device=" + device +
                ", reviewed=" + reviewed +
                ", pcv=" + pcv +
                ", alternatetelephone='" + alternatetelephone + '\'' +
                ", alternatefirstname='" + alternatefirstname + '\'' +
                ", alternatemiddlename='" + alternatemiddlename + '\'' +
                ", alternatelastname='" + alternatelastname + '\'' +
                ", alternaterelationship='" + alternaterelationship + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.integratioins_app.patient_tasks;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "units",
        "measure"
})
public class Every <T>{

    @JsonProperty("units")
    private List<T> units = null;
    @JsonProperty("measure")
    private String measure;

    public Every() {
    }

    public Every(List<T> units, String measure) {
        this.units = units;
        this.measure = measure;
    }

    public List<T> getUnits() {
        return units;
    }

    public void setUnits(List<T> units) {
        this.units = units;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Every every = (Every) o;
        return Objects.equals(units, every.units) &&
                Objects.equals(measure, every.measure);
    }

    @Override
    public int hashCode() {
        return Objects.hash(units, measure);
    }

    @Override
    public String toString() {
        return "Every{" +
                "units=" + units +
                ", measure='" + measure + '\'' +
                '}';
    }
}

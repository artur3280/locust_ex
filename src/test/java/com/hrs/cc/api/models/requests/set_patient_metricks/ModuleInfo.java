package com.hrs.cc.api.models.requests.set_patient_metricks;

import java.util.List;
import java.util.Objects;

public class ModuleInfo {
    private List<String> modules = null;
    private Activityreminder activityreminder;
    private Surveyreminder surveyreminder;
    private Weightreminder weightreminder;
    private List<Medicationreminder> medicationreminders = null;

    public ModuleInfo() {
    }

    public ModuleInfo(List<String> modules, Activityreminder activityreminder, Surveyreminder surveyreminder, Weightreminder weightreminder, List<Medicationreminder> medicationreminders) {
        this.modules = modules;
        this.activityreminder = activityreminder;
        this.surveyreminder = surveyreminder;
        this.weightreminder = weightreminder;
        this.medicationreminders = medicationreminders;
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    public Activityreminder getActivityreminder() {
        return activityreminder;
    }

    public void setActivityreminder(Activityreminder activityreminder) {
        this.activityreminder = activityreminder;
    }

    public Surveyreminder getSurveyreminder() {
        return surveyreminder;
    }

    public void setSurveyreminder(Surveyreminder surveyreminder) {
        this.surveyreminder = surveyreminder;
    }

    public Weightreminder getWeightreminder() {
        return weightreminder;
    }

    public void setWeightreminder(Weightreminder weightreminder) {
        this.weightreminder = weightreminder;
    }

    public List<Medicationreminder> getMedicationreminders() {
        return medicationreminders;
    }

    public void setMedicationreminders(List<Medicationreminder> medicationreminders) {
        this.medicationreminders = medicationreminders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModuleInfo that = (ModuleInfo) o;
        return Objects.equals(modules, that.modules) &&
                Objects.equals(activityreminder, that.activityreminder) &&
                Objects.equals(surveyreminder, that.surveyreminder) &&
                Objects.equals(weightreminder, that.weightreminder) &&
                Objects.equals(medicationreminders, that.medicationreminders);
    }

    @Override
    public int hashCode() {

        return Objects.hash(modules, activityreminder, surveyreminder, weightreminder, medicationreminders);
    }

    @Override
    public String toString() {
        return "ModuleInfo{" +
                "modules=" + modules +
                ", activityreminder=" + activityreminder +
                ", surveyreminder=" + surveyreminder +
                ", weightreminder=" + weightreminder +
                ", medicationreminders=" + medicationreminders +
                '}';
    }
}

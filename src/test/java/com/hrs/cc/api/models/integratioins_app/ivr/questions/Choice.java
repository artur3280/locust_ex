package com.hrs.cc.api.models.integratioins_app.ivr.questions;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "choice",
        "text",
        "createdAt",
        "updatedAt"
})
public class Choice {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("choice")
    private Integer choice;
    @JsonProperty("text")
    private String text;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("updatedAt")
    private String updatedAt;

    public Choice() {
    }

    public Choice(Integer id, Integer choice, String text, String createdAt, String updatedAt) {
        this.id = id;
        this.choice = choice;
        this.text = text;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChoice() {
        return choice;
    }

    public void setChoice(Integer choice) {
        this.choice = choice;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Choice choice1 = (Choice) o;
        return Objects.equals(id, choice1.id) &&
                Objects.equals(choice, choice1.choice) &&
                Objects.equals(text, choice1.text) &&
                Objects.equals(createdAt, choice1.createdAt) &&
                Objects.equals(updatedAt, choice1.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, choice, text, createdAt, updatedAt);
    }

    @Override
    public String toString() {
        return "Choice{" +
                "id=" + id +
                ", choice=" + choice +
                ", text='" + text + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.requests.add_metrics_integreation_app.activity_manual;

public class FacetActivity {
    private String value;
    private String unit;

    /**
     * No args constructor for use in serialization
     *
     */
    public FacetActivity() {
    }


    /**
     *
     * @param value
     * @param unit
     */

    public FacetActivity(String value, String unit) {
        this.value = value;
        this.unit = unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "FacetActivity{" +
                "value='" + value + '\'' +
                ", unit='" + unit + '\'' +
                '}';
    }
}

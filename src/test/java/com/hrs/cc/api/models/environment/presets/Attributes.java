package com.hrs.cc.api.models.environment.presets;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "env"
})
public class Attributes {

    @JsonProperty("name")
    private String name;
    @JsonProperty("env")
    private String env;

    public Attributes() {
    }

    public Attributes(String name, String env) {
        this.name = name;
        this.env = env;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "name='" + name + '\'' +
                ", env='" + env + '\'' +
                '}';
    }
}

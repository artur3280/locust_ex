package com.hrs.cc.api.models.account_data.clinician.filters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "columns",
        "scrollFocus",
        "selection",
        "grouping",
        "treeView",
        "pagination"
})
public class FilterState {

    @JsonProperty("columns")
    private List<Column> columns;
    @JsonProperty("scrollFocus")
    private List<Object> scrollFocus;
    @JsonProperty("selection")
    private List<Object> selection;
    @JsonProperty("grouping")
    private List<Object> grouping;
    @JsonProperty("treeView")
    private List<Object> treeView;
    @JsonProperty("pagination")
    private List<Object> pagination;

    public FilterState(List<Column> columns, List<Object> scrollFocus, List<Object> selection, List<Object> grouping, List<Object> treeView, List<Object> pagination) {
        this.columns = columns;
        this.scrollFocus = scrollFocus;
        this.selection = selection;
        this.grouping = grouping;
        this.treeView = treeView;
        this.pagination = pagination;
    }

    public FilterState() {
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public List<Object> getScrollFocus() {
        return scrollFocus;
    }

    public void setScrollFocus(List<Object> scrollFocus) {
        this.scrollFocus = scrollFocus;
    }

    public List<Object> getSelection() {
        return selection;
    }

    public void setSelection(List<Object> selection) {
        this.selection = selection;
    }

    public List<Object> getGrouping() {
        return grouping;
    }

    public void setGrouping(List<Object> grouping) {
        this.grouping = grouping;
    }

    public List<Object> getTreeView() {
        return treeView;
    }

    public void setTreeView(List<Object> treeView) {
        this.treeView = treeView;
    }

    public List<Object> getPagination() {
        return pagination;
    }

    public void setPagination(List<Object> pagination) {
        this.pagination = pagination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilterState that = (FilterState) o;
        return Objects.equals(columns, that.columns) &&
                Objects.equals(scrollFocus, that.scrollFocus) &&
                Objects.equals(selection, that.selection) &&
                Objects.equals(grouping, that.grouping) &&
                Objects.equals(treeView, that.treeView) &&
                Objects.equals(pagination, that.pagination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(columns, scrollFocus, selection, grouping, treeView, pagination);
    }

    @Override
    public String toString() {
        return "FilterState{" +
                "columns=" + columns +
                ", scrollFocus=" + scrollFocus +
                ", selection=" + selection +
                ", grouping=" + grouping +
                ", treeView=" + treeView +
                ", pagination=" + pagination +
                '}';
    }
}

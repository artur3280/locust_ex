package com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_manual;

import com.hrs.cc.api.models.requests.add_metrics_integreation_app.weight_manual.DataWeight;

public class MedicationManual {
    private DataMedication data;

    /**
     * No args constructor for use in serialization
     *
     */
    public MedicationManual() {
    }

    /**
     *
     * @param data
     */
    public MedicationManual(DataMedication data) {
        super();
        this.data = data;
    }

    public DataMedication getData() {
        return data;
    }

    public void setData(DataMedication data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "MedicationManual{" +
                "data=" + data +
                '}';
    }
}

package com.hrs.cc.api.models.requests.settings_patient;

import java.util.List;
import java.util.Objects;

public class SettingPatient {
    private List<CustomNewAtributs> PATIENTINFO_CUSTOMATTRIBUTES = null;
    private String subgroup;
    private String testpatient = "yes";
    private String lasthospitalization;
    private String alternatefirstname;
    private String alternatelastname;
    private String alternatetelephone;
    private String gender;

    public SettingPatient() {
    }

    public SettingPatient(List<CustomNewAtributs> PATIENTINFO_CUSTOMATTRIBUTES, String subgroup, String testpatient, String lasthospitalization, String alternatefirstname, String alternatelastname, String alternatetelephone, String gender) {
        this.PATIENTINFO_CUSTOMATTRIBUTES = PATIENTINFO_CUSTOMATTRIBUTES;
        this.subgroup = subgroup;
        this.testpatient = testpatient;
        this.lasthospitalization = lasthospitalization;
        this.alternatefirstname = alternatefirstname;
        this.alternatelastname = alternatelastname;
        this.alternatetelephone = alternatetelephone;
        this.gender = gender;
    }

    public List<CustomNewAtributs> getPATIENTINFO_CUSTOMATTRIBUTES() {
        return PATIENTINFO_CUSTOMATTRIBUTES;
    }

    public void setPATIENTINFO_CUSTOMATTRIBUTES(List<CustomNewAtributs> PATIENTINFO_CUSTOMATTRIBUTES) {
        this.PATIENTINFO_CUSTOMATTRIBUTES = PATIENTINFO_CUSTOMATTRIBUTES;
    }

    public String getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(String subgroup) {
        this.subgroup = subgroup;
    }

    public String getTestpatient() {
        return testpatient;
    }

    public void setTestpatient(String testpatient) {
        this.testpatient = testpatient;
    }

    public String getLasthospitalization() {
        return lasthospitalization;
    }

    public void setLasthospitalization(String lasthospitalization) {
        this.lasthospitalization = lasthospitalization;
    }

    public String getAlternatefirstname() {
        return alternatefirstname;
    }

    public void setAlternatefirstname(String alternatefirstname) {
        this.alternatefirstname = alternatefirstname;
    }

    public String getAlternatelastname() {
        return alternatelastname;
    }

    public void setAlternatelastname(String alternatelastname) {
        this.alternatelastname = alternatelastname;
    }

    public String getAlternatetelephone() {
        return alternatetelephone;
    }

    public void setAlternatetelephone(String alternatetelephone) {
        this.alternatetelephone = alternatetelephone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SettingPatient that = (SettingPatient) o;
        return Objects.equals(PATIENTINFO_CUSTOMATTRIBUTES, that.PATIENTINFO_CUSTOMATTRIBUTES) &&
                Objects.equals(subgroup, that.subgroup) &&
                Objects.equals(testpatient, that.testpatient) &&
                Objects.equals(lasthospitalization, that.lasthospitalization) &&
                Objects.equals(alternatefirstname, that.alternatefirstname) &&
                Objects.equals(alternatelastname, that.alternatelastname) &&
                Objects.equals(alternatetelephone, that.alternatetelephone) &&
                Objects.equals(gender, that.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(PATIENTINFO_CUSTOMATTRIBUTES, subgroup, testpatient, lasthospitalization, alternatefirstname, alternatelastname, alternatetelephone, gender);
    }

    @Override
    public String toString() {
        return "SettingPatient{" +
                "PATIENTINFO_CUSTOMATTRIBUTES=" + PATIENTINFO_CUSTOMATTRIBUTES +
                ", subgroup='" + subgroup + '\'' +
                ", testpatient='" + testpatient + '\'' +
                ", lasthospitalization='" + lasthospitalization + '\'' +
                ", alternatefirstname='" + alternatefirstname + '\'' +
                ", alternatelastname='" + alternatelastname + '\'' +
                ", alternatetelephone='" + alternatetelephone + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.requests.add_metrics_integreation_app.temperature_manual;

public class FacetTemperature {
    private String temperature;
    private String unit;

    /**
     * No args constructor for use in serialization
     *
     */
    public FacetTemperature() {
    }

    /**
     *
     * @param temperature
     * @param unit
     */
    public FacetTemperature(String temperature, String unit) {
        this.temperature = temperature;
        this.unit = unit;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "FacetTemperature{" +
                "temperature='" + temperature + '\'' +
                ", unit='" + unit + '\'' +
                '}';
    }
}

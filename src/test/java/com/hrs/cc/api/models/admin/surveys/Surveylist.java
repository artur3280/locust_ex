package com.hrs.cc.api.models.admin.surveys;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "status",
        "category",
        "question",
        "answertype",
        "schedule",
        "medalert",
        "highalert",
        "subgroups",
        "translations",
        "x"
})
public class Surveylist {

    @JsonProperty("id")
    private String id;
    @JsonProperty("status")
    private String status;
    @JsonProperty("category")
    private String category;
    @JsonProperty("question")
    private String question;
    @JsonProperty("answertype")
    private String answertype;
    @JsonProperty("schedule")
    private Schedule schedule;
    @JsonProperty("medalert")
    private String medalert;
    @JsonProperty("highalert")
    private String highalert;
    @JsonProperty("x")
    private String x;
    @JsonProperty("subgroups")
    private List<String> subgroups = null;
    @JsonProperty("translations")
    private List<Translation> translations = null;

    public Surveylist() {
    }

    public Surveylist(String id, String status, String category, String question, String answertype, Schedule schedule, String medalert, String highalert, List<String> subgroups, List<Translation> translations) {
        this.id = id;
        this.status = status;
        this.category = category;
        this.question = question;
        this.answertype = answertype;
        this.schedule = schedule;
        this.medalert = medalert;
        this.highalert = highalert;
        this.subgroups = subgroups;
        this.translations = translations;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswertype() {
        return answertype;
    }

    public void setAnswertype(String answertype) {
        this.answertype = answertype;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public String getMedalert() {
        return medalert;
    }

    public void setMedalert(String medalert) {
        this.medalert = medalert;
    }

    public String getHighalert() {
        return highalert;
    }

    public void setHighalert(String highalert) {
        this.highalert = highalert;
    }

    public List<String> getSubgroups() {
        return subgroups;
    }

    public void setSubgroups(List<String> subgroups) {
        this.subgroups = subgroups;
    }

    public List<Translation> getTranslations() {
        return translations;
    }

    public void setTranslations(List<Translation> translations) {
        this.translations = translations;
    }

    @Override
    public String toString() {
        return "Surveylist{" +
                "id='" + id + '\'' +
                ", status='" + status + '\'' +
                ", category='" + category + '\'' +
                ", question='" + question + '\'' +
                ", answertype='" + answertype + '\'' +
                ", schedule=" + schedule +
                ", medalert='" + medalert + '\'' +
                ", highalert='" + highalert + '\'' +
                ", subgroups=" + subgroups +
                ", translations=" + translations +
                '}';
    }
}

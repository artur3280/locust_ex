package com.hrs.cc.api.models.patients.module_info;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "diastolicmin",
        "diastolicmax",
        "systolicmin",
        "systolicmax"
})
public class Bloodpressurereminders {

    @JsonProperty("diastolicmin")
    private String diastolicmin;
    @JsonProperty("diastolicmax")
    private String diastolicmax;
    @JsonProperty("systolicmin")
    private String systolicmin;
    @JsonProperty("systolicmax")
    private String systolicmax;

    public Bloodpressurereminders() {
    }

    public Bloodpressurereminders(String diastolicmin, String diastolicmax, String systolicmin, String systolicmax) {
        this.diastolicmin = diastolicmin;
        this.diastolicmax = diastolicmax;
        this.systolicmin = systolicmin;
        this.systolicmax = systolicmax;
    }

    public String getDiastolicmin() {
        return diastolicmin;
    }

    public void setDiastolicmin(String diastolicmin) {
        this.diastolicmin = diastolicmin;
    }

    public String getDiastolicmax() {
        return diastolicmax;
    }

    public void setDiastolicmax(String diastolicmax) {
        this.diastolicmax = diastolicmax;
    }

    public String getSystolicmin() {
        return systolicmin;
    }

    public void setSystolicmin(String systolicmin) {
        this.systolicmin = systolicmin;
    }

    public String getSystolicmax() {
        return systolicmax;
    }

    public void setSystolicmax(String systolicmax) {
        this.systolicmax = systolicmax;
    }

    @Override
    public String toString() {
        return "Bloodpressurereminders{" +
                "diastolicmin='" + diastolicmin + '\'' +
                ", diastolicmax='" + diastolicmax + '\'' +
                ", systolicmin='" + systolicmin + '\'' +
                ", systolicmax='" + systolicmax + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_all_fields;

public class MedicationAllFields {
    private DataMedicationAllFields data;

    /**
     * No args constructor for use in serialization
     *
     */
    public MedicationAllFields() {
    }

    /**
     *
     * @param data
     */
    public MedicationAllFields(DataMedicationAllFields data) {
        super();
        this.data = data;
    }

    public DataMedicationAllFields getData() {
        return data;
    }

    public void setData(DataMedicationAllFields data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "MedicationManual{" +
                "data=" + data +
                '}';
    }
}

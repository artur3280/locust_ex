package com.hrs.cc.api.models.requests.set_patient_note;

import java.util.Objects;

public class Enrollment {
    private String status = "Patient Admitted but Telehealth Unit not Installed";

    public Enrollment() {
    }

    public Enrollment(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Enrollment that = (Enrollment) o;
        return Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {

        return Objects.hash(status);
    }

    @Override
    public String toString() {
        return "Enrollment{" +
                "status='" + status + '\'' +
                '}';
    }
}

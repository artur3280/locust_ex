package com.hrs.cc.api.models.patients.notes.history;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "clinician",
        "first",
        "middle",
        "last"
})
public class Clinician {

    @JsonProperty("clinician")
    private String clinician;
    @JsonProperty("first")
    private String first;
    @JsonProperty("middle")
    private String middle;
    @JsonProperty("last")
    private String last;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Clinician() {
    }

    public Clinician(String clinician, String first, String middle, String last) {
        this.clinician = clinician;
        this.first = first;
        this.middle = middle;
        this.last = last;
    }

    public String getClinician() {
        return clinician;
    }

    public void setClinician(String clinician) {
        this.clinician = clinician;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getMiddle() {
        return middle;
    }

    public void setMiddle(String middle) {
        this.middle = middle;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clinician clinician1 = (Clinician) o;
        return Objects.equals(clinician, clinician1.clinician) &&
                Objects.equals(first, clinician1.first) &&
                Objects.equals(middle, clinician1.middle) &&
                Objects.equals(last, clinician1.last) &&
                Objects.equals(additionalProperties, clinician1.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(clinician, first, middle, last, additionalProperties);
    }

    @Override
    public String toString() {
        return "Clinician{" +
                "clinician='" + clinician + '\'' +
                ", first='" + first + '\'' +
                ", middle='" + middle + '\'' +
                ", last='" + last + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

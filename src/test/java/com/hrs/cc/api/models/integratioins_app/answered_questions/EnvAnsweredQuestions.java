package com.hrs.cc.api.models.integratioins_app.answered_questions;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class EnvAnsweredQuestions {

    @JsonProperty("data")
    private List<Datum> data = null;

    public EnvAnsweredQuestions() {
    }

    public EnvAnsweredQuestions(List<Datum> data) {
        this.data = data;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "EnvAnsweredQuestions{" +
                "data=" + data +
                '}';
    }
}

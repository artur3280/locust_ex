package com.hrs.cc.api.models.integratioins_app.patient_tasks_v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "units",
        "measure"
})
public class Every <T>{

    @JsonProperty("units")
    private T units = null;
    @JsonProperty("measure")
    private String measure;

    public Every(T units, String measure) {
        this.units = units;
        this.measure = measure;
    }

    public Every() {
    }

    public T getUnits() {
        return units;
    }

    public void setUnits(T units) {
        this.units = units;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public String toString() {
        return "Every{" +
                "units=" + units +
                ", measure='" + measure + '\'' +
                '}';
    }
}

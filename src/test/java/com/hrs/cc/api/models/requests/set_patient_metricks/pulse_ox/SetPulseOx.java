package com.hrs.cc.api.models.requests.set_patient_metricks.pulse_ox;

import java.util.Objects;

public class SetPulseOx {

    private String type = "pulseox";
    private String reason;
    private String ftime;
    private String rtime;
    private Integer spo2;
    private Integer heartrate;

    public SetPulseOx() {
    }

    public SetPulseOx(String reason, String ftime, String rtime, Integer spo2, Integer heartrate) {
        this.reason = reason;
        this.ftime = ftime;
        this.rtime = rtime;
        this.spo2 = spo2;
        this.heartrate = heartrate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFtime() {
        return ftime;
    }

    public void setFtime(String ftime) {
        this.ftime = ftime;
    }

    public String getRtime() {
        return rtime;
    }

    public void setRtime(String rtime) {
        this.rtime = rtime;
    }

    public Integer getSpo2() {
        return spo2;
    }

    public void setSpo2(Integer spo2) {
        this.spo2 = spo2;
    }

    public Integer getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(Integer heartrate) {
        this.heartrate = heartrate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SetPulseOx that = (SetPulseOx) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(reason, that.reason) &&
                Objects.equals(ftime, that.ftime) &&
                Objects.equals(rtime, that.rtime) &&
                Objects.equals(spo2, that.spo2) &&
                Objects.equals(heartrate, that.heartrate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, reason, ftime, rtime, spo2, heartrate);
    }

    @Override
    public String toString() {
        return "SetPulseOx{" +
                "type='" + type + '\'' +
                ", reason='" + reason + '\'' +
                ", ftime='" + ftime + '\'' +
                ", rtime='" + rtime + '\'' +
                ", spo2=" + spo2 +
                ", heartrate=" + heartrate +
                '}';
    }
}

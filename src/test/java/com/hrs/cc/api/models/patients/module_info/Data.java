package com.hrs.cc.api.models.patients.module_info;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "patientmoduleinfo"
})
public class Data {

    @JsonProperty("patientmoduleinfo")
    private Patientmoduleinfo patientmoduleinfo;

    public Data() {
    }

    public Data(Patientmoduleinfo patientmoduleinfo) {
        this.patientmoduleinfo = patientmoduleinfo;
    }

    public Patientmoduleinfo getPatientmoduleinfo() {
        return patientmoduleinfo;
    }

    public void setPatientmoduleinfo(Patientmoduleinfo patientmoduleinfo) {
        this.patientmoduleinfo = patientmoduleinfo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "patientmoduleinfo=" + patientmoduleinfo +
                '}';
    }
}

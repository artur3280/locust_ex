package com.hrs.cc.api.models.environment.data.metric.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type"
})
public class Med {
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Med() {
    }

    public Med(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Med med = (Med) o;
        return Objects.equals(type, med.type) &&
                Objects.equals(additionalProperties, med.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, additionalProperties);
    }

    @Override
    public String toString() {
        return "Med{" +
                "type='" + type + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

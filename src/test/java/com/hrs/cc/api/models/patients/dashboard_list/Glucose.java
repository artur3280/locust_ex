package com.hrs.cc.api.models.patients.dashboard_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "reading",
        "ts"
})
public class Glucose {

    @JsonProperty("active")
    private String active;
    @JsonProperty("reading")
    private String reading;
    @JsonProperty("ts")
    private String ts;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Glucose() {
    }

    public Glucose(String active, String reading, String ts) {
        this.active = active;
        this.reading = reading;
        this.ts = ts;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getReading() {
        return reading;
    }

    public void setReading(String reading) {
        this.reading = reading;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Glucose glucose = (Glucose) o;
        return Objects.equals(active, glucose.active) &&
                Objects.equals(reading, glucose.reading) &&
                Objects.equals(ts, glucose.ts) &&
                Objects.equals(additionalProperties, glucose.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(active, reading, ts, additionalProperties);
    }

    @Override
    public String toString() {
        return "Glucose{" +
                "active='" + active + '\'' +
                ", reading='" + reading + '\'' +
                ", ts='" + ts + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

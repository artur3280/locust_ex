package com.hrs.cc.api.models.requests.request_magic_code_to_app;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "code"
})
public class Datum {

    @JsonProperty("type")
    private String type;
    @JsonProperty("code")
    private String code;

    public Datum() {
    }

    public Datum(String type, String code) {
        this.type = type;
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Datum data = (Datum) o;
        return Objects.equals(type, data.type) &&
                Objects.equals(code, data.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, code);
    }

    @Override
    public String toString() {
        return "DatumTask{" +
                "type='" + type + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.patients.dashboard_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "woundimaging",
        "ts"
})
public class WoundImaging {

    @JsonProperty("active")
    private String active;
    @JsonProperty("woundimaging")
    private String woundimaging;
    @JsonProperty("ts")
    private String ts;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public WoundImaging() {
    }

    public WoundImaging(String active, String woundimaging, String ts) {
        this.active = active;
        this.woundimaging = woundimaging;
        this.ts = ts;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getWoundimaging() {
        return woundimaging;
    }

    public void setWoundimaging(String woundimaging) {
        this.woundimaging = woundimaging;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WoundImaging that = (WoundImaging) o;
        return Objects.equals(active, that.active) &&
                Objects.equals(woundimaging, that.woundimaging) &&
                Objects.equals(ts, that.ts) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(active, woundimaging, ts, additionalProperties);
    }

    @Override
    public String toString() {
        return "WoundImaging{" +
                "active='" + active + '\'' +
                ", woundimaging='" + woundimaging + '\'' +
                ", ts='" + ts + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

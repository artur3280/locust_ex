package com.hrs.cc.api.models.patients.notes.history;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "patient",
        "noteHistoryId",
        "date",
        "location",
        "lengthOfStay",
        "lastUpdated"
})
public class Edvisit {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("patient")
    private String patient;
    @JsonProperty("noteHistoryId")
    private String noteHistoryId;
    @JsonProperty("date")
    private Long date;
    @JsonProperty("location")
    private String location;
    @JsonProperty("lengthOfStay")
    private String lengthOfStay;
    @JsonProperty("lastUpdated")
    private String lastUpdated;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Edvisit() {
    }

    public Edvisit(Integer id, String patient, String noteHistoryId, Long date, String location, String lengthOfStay, String lastUpdated) {
        this.id = id;
        this.patient = patient;
        this.noteHistoryId = noteHistoryId;
        this.date = date;
        this.location = location;
        this.lengthOfStay = lengthOfStay;
        this.lastUpdated = lastUpdated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getNoteHistoryId() {
        return noteHistoryId;
    }

    public void setNoteHistoryId(String noteHistoryId) {
        this.noteHistoryId = noteHistoryId;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLengthOfStay() {
        return lengthOfStay;
    }

    public void setLengthOfStay(String lengthOfStay) {
        this.lengthOfStay = lengthOfStay;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edvisit edvisit = (Edvisit) o;
        return Objects.equals(id, edvisit.id) &&
                Objects.equals(patient, edvisit.patient) &&
                Objects.equals(noteHistoryId, edvisit.noteHistoryId) &&
                Objects.equals(date, edvisit.date) &&
                Objects.equals(location, edvisit.location) &&
                Objects.equals(lengthOfStay, edvisit.lengthOfStay) &&
                Objects.equals(lastUpdated, edvisit.lastUpdated) &&
                Objects.equals(additionalProperties, edvisit.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, patient, noteHistoryId, date, location, lengthOfStay, lastUpdated, additionalProperties);
    }

    @Override
    public String toString() {
        return "Edvisit{" +
                "id=" + id +
                ", patient='" + patient + '\'' +
                ", noteHistoryId='" + noteHistoryId + '\'' +
                ", date=" + date +
                ", location='" + location + '\'' +
                ", lengthOfStay='" + lengthOfStay + '\'' +
                ", lastUpdated='" + lastUpdated + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

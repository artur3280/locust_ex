package com.hrs.cc.api.models.patients.profile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "lastactive"
})
public class Tracking {

    @JsonProperty("data")
    private Data data;
    @JsonProperty("lastactive")
    private Long lastactive;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Tracking() {
    }

    public Tracking(Data data, Long lastactive) {
        this.data = data;
        this.lastactive = lastactive;
    }

    public Long getLastactive() {
        return lastactive;
    }

    public void setLastactive(Long lastactive) {
        this.lastactive = lastactive;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tracking tracking = (Tracking) o;
        return Objects.equals(data, tracking.data) &&
                Objects.equals(lastactive, tracking.lastactive) &&
                Objects.equals(additionalProperties, tracking.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, lastactive, additionalProperties);
    }

    @Override
    public String toString() {
        return "Tracking{" +
                "data=" + data +
                ", lastactive=" + lastactive +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

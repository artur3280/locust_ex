package com.hrs.cc.api.models.patients.patient_metric.response_after_add;

import com.hrs.cc.api.models.patients.patient_metric.response_after_add.response.Auth;
import com.hrs.cc.api.models.patients.patient_metric.response_after_add.response.Installdata;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "auth",
        "installdata"
})
public class MetricksResponse {

    @JsonProperty("status")
    private String status;
    @JsonProperty("auth")
    private Auth auth;
    @JsonProperty("installdata")
    private Installdata installdata;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public MetricksResponse() {
    }

    public MetricksResponse(String status, Auth auth, Installdata installdata) {
        this.status = status;
        this.auth = auth;
        this.installdata = installdata;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Installdata getInstalldata() {
        return installdata;
    }

    public void setInstalldata(Installdata installdata) {
        this.installdata = installdata;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetricksResponse that = (MetricksResponse) o;
        return Objects.equals(status, that.status) &&
                Objects.equals(auth, that.auth) &&
                Objects.equals(installdata, that.installdata) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(status, auth, installdata, additionalProperties);
    }

    @Override
    public String toString() {
        return "MetricksResponse{" +
                "status='" + status + '\'' +
                ", auth=" + auth +
                ", installdata=" + installdata +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.inventory.tablet_status;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.hrs.cc.api.models.inventory.tablet_status.Battery;
import com.hrs.cc.api.models.inventory.tablet_status.Network;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "connected",
        "name",
        "patient",
        "battery",
        "network",
        "gps",
        "lastUpdated"
})
public class TabletStatus {

    @JsonProperty("id")
    private String id;
    @JsonProperty("connected")
    private Boolean connected;
    @JsonProperty("name")
    private String name;
    @JsonProperty("patient")
    private String patient;
    @JsonProperty("battery")
    private Battery battery;
    @JsonProperty("network")
    private Network network;
    @JsonProperty("gps")
    private Gps gps;
    @JsonProperty("lastUpdated")
    private LastUpdated lastUpdated;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public TabletStatus() {
    }

    public TabletStatus(String id, Boolean connected, String name, String patient, Battery battery, Network network, Gps gps, LastUpdated lastUpdated) {
        this.id = id;
        this.connected = connected;
        this.name = name;
        this.patient = patient;
        this.battery = battery;
        this.network = network;
        this.gps = gps;
        this.lastUpdated = lastUpdated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getConnected() {
        return connected;
    }

    public void setConnected(Boolean connected) {
        this.connected = connected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public Battery getBattery() {
        return battery;
    }

    public void setBattery(Battery battery) {
        this.battery = battery;
    }

    public Network getNetwork() {
        return network;
    }

    public void setNetwork(Network network) {
        this.network = network;
    }

    public Gps getGps() {
        return gps;
    }

    public void setGps(Gps gps) {
        this.gps = gps;
    }

    public LastUpdated getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LastUpdated lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TabletStatus that = (TabletStatus) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(connected, that.connected) &&
                Objects.equals(name, that.name) &&
                Objects.equals(patient, that.patient) &&
                Objects.equals(battery, that.battery) &&
                Objects.equals(network, that.network) &&
                Objects.equals(gps, that.gps) &&
                Objects.equals(lastUpdated, that.lastUpdated) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, connected, name, patient, battery, network, gps, lastUpdated, additionalProperties);
    }

    @Override
    public String toString() {
        return "TabletStatus{" +
                "id='" + id + '\'' +
                ", connected=" + connected +
                ", name='" + name + '\'' +
                ", patient='" + patient + '\'' +
                ", battery=" + battery +
                ", network=" + network +
                ", gps=" + gps +
                ", lastUpdated=" + lastUpdated +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

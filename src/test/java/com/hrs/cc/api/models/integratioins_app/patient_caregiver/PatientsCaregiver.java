package com.hrs.cc.api.models.integratioins_app.patient_caregiver;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "sideload"
})
public class PatientsCaregiver {

    @JsonProperty("data")
    private List<Datum> data = null;
    @JsonProperty("sideload")
    private Sideload sideload;

    public PatientsCaregiver() {
    }

    public PatientsCaregiver(List<Datum> data, Sideload sideload) {
        this.data = data;
        this.sideload = sideload;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Sideload getSideload() {
        return sideload;
    }

    public void setSideload(Sideload sideload) {
        this.sideload = sideload;
    }

    @Override
    public String toString() {
        return "PatiensCaregiver{" +
                "data=" + data +
                ", sideload=" + sideload +
                '}';
    }
}

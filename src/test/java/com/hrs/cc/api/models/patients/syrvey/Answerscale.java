package com.hrs.cc.api.models.patients.syrvey;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "val",
        "text"
})
public class Answerscale {

    @JsonProperty("val")
    private String val;
    @JsonProperty("text")
    private String text;

    public Answerscale() {
    }

    public Answerscale(String val, String text) {
        this.val = val;
        this.text = text;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answerscale that = (Answerscale) o;
        return Objects.equals(val, that.val) &&
                Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(val, text);
    }

    @Override
    public String toString() {
        return "Answerscale{" +
                "val='" + val + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}

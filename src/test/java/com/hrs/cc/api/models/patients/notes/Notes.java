package com.hrs.cc.api.models.patients.notes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonPropertyOrder({
        "id",
        "patient",
        "clinician",
        "note",
        "created",
        "updated",
        "status",
        "time",
        "timespent",
        "readmission",
        "edvisit",
        "observation",
        "communication",
        "enrollment"
})
public class Notes {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("patient")
    private String patient;
    @JsonProperty("clinician")
    private Clinician clinician;
    @JsonProperty("note")
    private String note;
    @JsonProperty("created")
    private Created created;
    @JsonProperty("updated")
    private Object updated;
    @JsonProperty("status")
    private String status;
    @JsonProperty("time")
    private String time;
    @JsonProperty("timespent")
    private String timespent;
    @JsonProperty("readmission")
    private Readmission readmission;
    @JsonProperty("edvisit")
    private Edvisit edvisit;
    @JsonProperty("observation")
    private Observation observation;
    @JsonProperty("communication")
    private Communication communication;
    @JsonProperty("enrollment")
    private Enrollment enrollment;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Notes() {
    }

    public Notes(Integer id, String patient, Clinician clinician, String note, Created created, Object updated, String status, String time, String timespent, Readmission readmission, Edvisit edvisit, Observation observation, Communication communication, Enrollment enrollment) {
        this.id = id;
        this.patient = patient;
        this.clinician = clinician;
        this.note = note;
        this.created = created;
        this.updated = updated;
        this.status = status;
        this.time = time;
        this.timespent = timespent;
        this.readmission = readmission;
        this.edvisit = edvisit;
        this.observation = observation;
        this.communication = communication;
        this.enrollment = enrollment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public Clinician getClinician() {
        return clinician;
    }

    public void setClinician(Clinician clinician) {
        this.clinician = clinician;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Created getCreated() {
        return created;
    }

    public void setCreated(Created created) {
        this.created = created;
    }

    public Object getUpdated() {
        return updated;
    }

    public void setUpdated(Object updated) {
        this.updated = updated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTimespent() {
        return timespent;
    }

    public void setTimespent(String timespent) {
        this.timespent = timespent;
    }

    public Readmission getReadmission() {
        return readmission;
    }

    public void setReadmission(Readmission readmission) {
        this.readmission = readmission;
    }

    public Edvisit getEdvisit() {
        return edvisit;
    }

    public void setEdvisit(Edvisit edvisit) {
        this.edvisit = edvisit;
    }

    public Observation getObservation() {
        return observation;
    }

    public void setObservation(Observation observation) {
        this.observation = observation;
    }

    public Communication getCommunication() {
        return communication;
    }

    public void setCommunication(Communication communication) {
        this.communication = communication;
    }

    public Enrollment getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(Enrollment enrollment) {
        this.enrollment = enrollment;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notes notes = (Notes) o;
        return Objects.equals(id, notes.id) &&
                Objects.equals(patient, notes.patient) &&
                Objects.equals(clinician, notes.clinician) &&
                Objects.equals(note, notes.note) &&
                Objects.equals(created, notes.created) &&
                Objects.equals(updated, notes.updated) &&
                Objects.equals(status, notes.status) &&
                Objects.equals(time, notes.time) &&
                Objects.equals(timespent, notes.timespent) &&
                Objects.equals(readmission, notes.readmission) &&
                Objects.equals(edvisit, notes.edvisit) &&
                Objects.equals(observation, notes.observation) &&
                Objects.equals(communication, notes.communication) &&
                Objects.equals(enrollment, notes.enrollment) &&
                Objects.equals(additionalProperties, notes.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, patient, clinician, note, created, updated, status, time, timespent, readmission, edvisit, observation, communication, enrollment, additionalProperties);
    }

    @Override
    public String toString() {
        return "Notes{" +
                "id=" + id +
                ", patient='" + patient + '\'' +
                ", clinician=" + clinician +
                ", note='" + note + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                ", status='" + status + '\'' +
                ", time='" + time + '\'' +
                ", timespent='" + timespent + '\'' +
                ", readmission=" + readmission +
                ", edvisit=" + edvisit +
                ", observation=" + observation +
                ", communication=" + communication +
                ", enrollment=" + enrollment +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.patients.dashboard_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "medstaken",
        "medstotal",
        "ts"
})
public class Meds {

    @JsonProperty("active")
    private String active;
    @JsonProperty("medstaken")
    private String medstaken;
    @JsonProperty("medstotal")
    private String medstotal;
    @JsonProperty("ts")
    private String ts;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Meds() {
    }

    public Meds(String active, String medstaken, String medstotal, String ts) {
        this.active = active;
        this.medstaken = medstaken;
        this.medstotal = medstotal;
        this.ts = ts;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getMedstaken() {
        return medstaken;
    }

    public void setMedstaken(String medstaken) {
        this.medstaken = medstaken;
    }

    public String getMedstotal() {
        return medstotal;
    }

    public void setMedstotal(String medstotal) {
        this.medstotal = medstotal;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meds meds = (Meds) o;
        return Objects.equals(active, meds.active) &&
                Objects.equals(medstaken, meds.medstaken) &&
                Objects.equals(medstotal, meds.medstotal) &&
                Objects.equals(ts, meds.ts) &&
                Objects.equals(additionalProperties, meds.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(active, medstaken, medstotal, ts, additionalProperties);
    }

    @Override
    public String toString() {
        return "Meds{" +
                "active='" + active + '\'' +
                ", medstaken='" + medstaken + '\'' +
                ", medstotal='" + medstotal + '\'' +
                ", ts='" + ts + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

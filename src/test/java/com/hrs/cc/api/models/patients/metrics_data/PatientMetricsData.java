package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "profile",
        "risk",
        "metrics",
        "tracking",
        "day",
        "ts",
        "lastActivatedDate",
        "discharged",
        "consentFormAvailable"
})
public class PatientMetricsData {

    @JsonProperty("profile")
    private Profile profile;
    @JsonProperty("risk")
    private List<Risk> risk = null;
    @JsonProperty("metrics")
    private Metrics metrics;
    @JsonProperty("tracking")
    private Tracking tracking;
    @JsonProperty("day")
    private Integer day;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("lastActivatedDate")
    private Long lastActivatedDate;
    @JsonProperty("consentFormAvailable")
    private Boolean consentFormAvailable;
    @JsonProperty("discharged")
    private String discharged;

    public PatientMetricsData() {
    }

    public PatientMetricsData(Profile profile, List<Risk> risk, Metrics metrics, Tracking tracking, Integer day, Long ts, Boolean consentFormAvailable, Long lastActivatedDate) {
        this.profile = profile;
        this.risk = risk;
        this.metrics = metrics;
        this.tracking = tracking;
        this.day = day;
        this.ts = ts;
        this.consentFormAvailable = consentFormAvailable;
        this.lastActivatedDate = lastActivatedDate;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<Risk> getRisk() {
        return risk;
    }

    public void setRisk(List<Risk> risk) {
        this.risk = risk;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public void setMetrics(Metrics metrics) {
        this.metrics = metrics;
    }

    public Tracking getTracking() {
        return tracking;
    }

    public void setTracking(Tracking tracking) {
        this.tracking = tracking;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Boolean getConsentFormAvailable() {
        return consentFormAvailable;
    }

    public void setConsentFormAvailable(Boolean consentFormAvailable) {
        this.consentFormAvailable = consentFormAvailable;
    }

    public Long getLastActivatedDate() {
        return lastActivatedDate;
    }

    public void setLastActivatedDate(Long lastActivatedDate) {
        this.lastActivatedDate = lastActivatedDate;
    }

    public String getDischarged() {
        return discharged;
    }

    public void setDischarged(String discharged) {
        this.discharged = discharged;
    }

    @Override
    public String toString() {
        return "PatientMetricsData{" +
                "profile=" + profile +
                ", risk=" + risk +
                ", metrics=" + metrics +
                ", tracking=" + tracking +
                ", day=" + day +
                ", ts=" + ts +
                ", lastActivatedDate=" + lastActivatedDate +
                ", consentFormAvailable=" + consentFormAvailable +
                ", discharged='" + discharged + '\'' +
                '}';
    }
}

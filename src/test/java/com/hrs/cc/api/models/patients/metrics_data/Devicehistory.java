package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "devid",
        "name",
        "ts"
})
public class Devicehistory {

    @JsonProperty("devid")
    private String devid;
    @JsonProperty("name")
    private String name;
    @JsonProperty("ts")
    private Long ts;

    public Devicehistory() {
    }

    public Devicehistory(String devid, String name, Long ts) {
        this.devid = devid;
        this.name = name;
        this.ts = ts;
    }

    public String getDevid() {
        return devid;
    }

    public void setDevid(String devid) {
        this.devid = devid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Devicehistory that = (Devicehistory) o;
        return Objects.equals(devid, that.devid) &&
                Objects.equals(name, that.name) &&
                Objects.equals(ts, that.ts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(devid, name, ts);
    }

    @Override
    public String toString() {
        return "Devicehistory{" +
                "devid='" + devid + '\'' +
                ", name='" + name + '\'' +
                ", ts=" + ts +
                '}';
    }
}

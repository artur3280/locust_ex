package com.hrs.cc.api.models.patients.module_info;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "time"
})
public class Activityreminders {

    @JsonProperty("time")
    private String time;

    public Activityreminders() {
    }

    public Activityreminders(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Activityreminders{" +
                "time='" + time + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.caregiver.chatroom;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "participants",
        "createdAt",
        "resourceType",
        "attributes"
})
public class Datum {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("participants")
    private List<String> participants = null;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("resourceType")
    private String resourceType;
    @JsonProperty("attributes")
    private Attributes attributes = null;


    public Datum() {
    }

    public Datum(Integer id, List<String> participants, String createdAt, String resourceType, Attributes attributes) {
        this.id = id;
        this.participants = participants;
        this.createdAt = createdAt;
        this.resourceType = resourceType;
        this.attributes = attributes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getParticipants() {
        return participants;
    }

    public void setParticipants(List<String> participants) {
        this.participants = participants;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "id=" + id +
                ", participants=" + participants +
                ", createdAt='" + createdAt + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", attributes=" + attributes +
                '}';
    }
}

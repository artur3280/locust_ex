package com.hrs.cc.api.models.patients.dashboard_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "bpm",
        "spo2",
        "ts"
})
public class Pulseox {

    @JsonProperty("active")
    private String active;
    @JsonProperty("bpm")
    private String bpm;
    @JsonProperty("spo2")
    private String spo2;
    @JsonProperty("ts")
    private String ts;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Pulseox() {
    }

    public Pulseox(String active, String bpm, String spo2, String ts) {
        this.active = active;
        this.bpm = bpm;
        this.spo2 = spo2;
        this.ts = ts;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getBpm() {
        return bpm;
    }

    public void setBpm(String bpm) {
        this.bpm = bpm;
    }

    public String getSpo2() {
        return spo2;
    }

    public void setSpo2(String spo2) {
        this.spo2 = spo2;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pulseox pulseox = (Pulseox) o;
        return Objects.equals(active, pulseox.active) &&
                Objects.equals(bpm, pulseox.bpm) &&
                Objects.equals(spo2, pulseox.spo2) &&
                Objects.equals(ts, pulseox.ts) &&
                Objects.equals(additionalProperties, pulseox.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(active, bpm, spo2, ts, additionalProperties);
    }

    @Override
    public String toString() {
        return "Pulseox{" +
                "active='" + active + '\'' +
                ", bpm='" + bpm + '\'' +
                ", spo2='" + spo2 + '\'' +
                ", ts='" + ts + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

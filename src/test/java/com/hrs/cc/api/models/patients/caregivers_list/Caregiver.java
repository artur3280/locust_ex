package com.hrs.cc.api.models.patients.caregivers_list;

import java.util.Map;

public class Caregiver {
    private Map<String, DataInfo> giver;

    public Caregiver() {
    }

    public Caregiver(Map<String, DataInfo> giver) {
        this.giver = giver;
    }

    public Map<String, DataInfo> getGiver() {
        return giver;
    }

    public void setGiver(Map<String, DataInfo> giver) {
        this.giver = giver;
    }

    @Override
    public String toString() {
        return "Caregiver{" +
                "giver=" + giver +
                '}';
    }
}

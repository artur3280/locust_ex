package com.hrs.cc.api.models.patients.patient_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "clinician",
        "devicehistory",
        "extension",
        "subgroup",
        "gender",
        "readmission",
        "edvisit",
        "testpatient",
        "lasthospitalization",
        "pid",
        "language",
        "quickNote",
        "hrsid",
        "startdate",
        "lastmoduleupdate",
        "alternatetelephone",
        "alternatefirstname",
        "alternatelastname",
        "alternatemiddlename",
        "volume",
        "phone",
        "dob",
        "name",
        "envphone",
        "conditions",
        "device",
        "audioreminders",
        "status",
        "reviewed",
        "byodDevice",
        "alternaterelationship",
        "pcv",
        "timezone"
})
public class Profile {
    @JsonProperty("clinician")
    private Clinician clinician = null;
    @JsonProperty("devicehistory")
    private List<Devicehistory> devicehistory = null;
    @JsonProperty("extension")
    private String extension;
    @JsonProperty("subgroup")
    private String subgroup;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("readmission")
    private Integer readmission;
    @JsonProperty("edvisit")
    private Integer edvisit;
    @JsonProperty("testpatient")
    private Boolean testpatient;
    @JsonProperty("lasthospitalization")
    private Long lasthospitalization;
    @JsonProperty("alternatetelephone")
    private String alternatetelephone;
    @JsonProperty("alternatefirstname")
    private String alternatefirstname;
    @JsonProperty("alternatelastname")
    private String alternatelastname;
    @JsonProperty("alternatemiddlename")
    private String alternatemiddlename;
    @JsonProperty("pid")
    private String pid;
    @JsonProperty("language")
    private String language;
    @JsonProperty("quicknote")
    private QuickNote quickNote;
    @JsonProperty("hrsid")
    private String hrsid;
    @JsonProperty("startdate")
    private Long startdate;
    @JsonProperty("lastmoduleupdate")
    private Long lastmoduleupdate;
    @JsonProperty("volume")
    private Integer volume;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("dob")
    private String dob;
    @JsonProperty("name")
    private Name name;
    @JsonProperty("envphone")
    private String envphone;
    @JsonProperty("conditions")
    private List<String> conditions = null;
    @JsonProperty("alternaterelationship")
    private String alternaterelationship = null;
    @JsonProperty("timezone")
    private String timezone;
    @JsonProperty("device")
    private Device device;
    @JsonProperty("audioreminders")
    private Boolean audioreminders;
    @JsonProperty("status")
    private String status;
    @JsonProperty("byodDevice")
    private ByodDevice byodDevice;
    @JsonProperty("reviewed")
    private List<Object> reviewed = null;
    @JsonProperty("pcv")
    private Pcv pcv;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Profile() {
    }

    public Profile(Clinician clinician, List<Devicehistory> devicehistory, String extension, String subgroup, String gender, Integer readmission, Integer edvisit, Boolean testpatient, Long lasthospitalization, String alternatetelephone, String alternatefirstname, String alternatelastname, String alternatemiddlename, String pid, String language, QuickNote quickNote, String hrsid, Long startdate, Long lastmoduleupdate, Integer volume, String phone, String dob, Name name, String envphone, List<String> conditions, String alternaterelationship, String timezone, Device device, Boolean audioreminders, String status, ByodDevice byodDevice, List<Object> reviewed, Pcv pcv) {
        this.clinician = clinician;
        this.devicehistory = devicehistory;
        this.extension = extension;
        this.subgroup = subgroup;
        this.gender = gender;
        this.readmission = readmission;
        this.edvisit = edvisit;
        this.testpatient = testpatient;
        this.lasthospitalization = lasthospitalization;
        this.alternatetelephone = alternatetelephone;
        this.alternatefirstname = alternatefirstname;
        this.alternatelastname = alternatelastname;
        this.alternatemiddlename = alternatemiddlename;
        this.pid = pid;
        this.language = language;
        this.quickNote = quickNote;
        this.hrsid = hrsid;
        this.startdate = startdate;
        this.lastmoduleupdate = lastmoduleupdate;
        this.volume = volume;
        this.phone = phone;
        this.dob = dob;
        this.name = name;
        this.envphone = envphone;
        this.conditions = conditions;
        this.alternaterelationship = alternaterelationship;
        this.timezone = timezone;
        this.device = device;
        this.audioreminders = audioreminders;
        this.status = status;
        this.byodDevice = byodDevice;
        this.reviewed = reviewed;
        this.pcv = pcv;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Clinician getClinician() {
        return clinician;
    }

    public void setClinician(Clinician clinician) {
        this.clinician = clinician;
    }

    public List<Devicehistory> getDevicehistory() {
        return devicehistory;
    }

    public void setDevicehistory(List<Devicehistory> devicehistory) {
        this.devicehistory = devicehistory;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getAlternatetelephone() {
        return alternatetelephone;
    }

    public void setAlternatetelephone(String alternatetelephone) {
        this.alternatetelephone = alternatetelephone;
    }

    public String getAlternatelastname() {
        return alternatelastname;
    }

    public void setAlternatelastname(String alternatelastname) {
        this.alternatelastname = alternatelastname;
    }

    public String getAlternatefirstname() {
        return alternatefirstname;
    }

    public void setAlternatefirstname(String alternatefirstname) {
        this.alternatefirstname = alternatefirstname;
    }

    public String getAlternatemiddlename() {
        return alternatemiddlename;
    }

    public void setAlternatemiddlename(String alternatemiddlename) {
        this.alternatemiddlename = alternatemiddlename;
    }

    public String getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(String subgroup) {
        this.subgroup = subgroup;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getReadmission() {
        return readmission;
    }

    public void setReadmission(Integer readmission) {
        this.readmission = readmission;
    }

    public Integer getEdvisit() {
        return edvisit;
    }

    public void setEdvisit(Integer edvisit) {
        this.edvisit = edvisit;
    }

    public Boolean getTestpatient() {
        return testpatient;
    }

    public void setTestpatient(Boolean testpatient) {
        this.testpatient = testpatient;
    }

    public Long getLasthospitalization() {
        return lasthospitalization;
    }

    public void setLasthospitalization(Long lasthospitalization) {
        this.lasthospitalization = lasthospitalization;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public QuickNote getQuickNote() {
        return quickNote;
    }

    public void setQuickNote(QuickNote quickNote) {
        this.quickNote = quickNote;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public Long getStartdate() {
        return startdate;
    }

    public void setStartdate(Long startdate) {
        this.startdate = startdate;
    }

    public Long getLastmoduleupdate() {
        return lastmoduleupdate;
    }

    public void setLastmoduleupdate(Long lastmoduleupdate) {
        this.lastmoduleupdate = lastmoduleupdate;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getEnvphone() {
        return envphone;
    }

    public void setEnvphone(String envphone) {
        this.envphone = envphone;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Boolean getAudioreminders() {
        return audioreminders;
    }

    public void setAudioreminders(Boolean audioreminders) {
        this.audioreminders = audioreminders;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Object> getReviewed() {
        return reviewed;
    }

    public void setReviewed(List<Object> reviewed) {
        this.reviewed = reviewed;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getAlternaterelationship() {
        return alternaterelationship;
    }

    public void setAlternaterelationship(String alternaterelationship) {
        this.alternaterelationship = alternaterelationship;
    }

    public ByodDevice getByodDevice() {
        return byodDevice;
    }

    public void setByodDevice(ByodDevice byodDevice) {
        this.byodDevice = byodDevice;
    }

    public Pcv getPcv() {
        return pcv;
    }

    public void setPcv(Pcv pcv) {
        this.pcv = pcv;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "clinician=" + clinician +
                ", devicehistory=" + devicehistory +
                ", extension='" + extension + '\'' +
                ", subgroup='" + subgroup + '\'' +
                ", gender='" + gender + '\'' +
                ", readmission=" + readmission +
                ", edvisit=" + edvisit +
                ", testpatient=" + testpatient +
                ", lasthospitalization=" + lasthospitalization +
                ", alternatetelephone='" + alternatetelephone + '\'' +
                ", alternatefirstname='" + alternatefirstname + '\'' +
                ", alternatelastname='" + alternatelastname + '\'' +
                ", alternatemiddlename='" + alternatemiddlename + '\'' +
                ", pid='" + pid + '\'' +
                ", language='" + language + '\'' +
                ", quickNote=" + quickNote +
                ", hrsid='" + hrsid + '\'' +
                ", startdate=" + startdate +
                ", lastmoduleupdate=" + lastmoduleupdate +
                ", volume=" + volume +
                ", phone='" + phone + '\'' +
                ", dob='" + dob + '\'' +
                ", name=" + name +
                ", envphone='" + envphone + '\'' +
                ", conditions=" + conditions +
                ", alternaterelationship='" + alternaterelationship + '\'' +
                ", timezone='" + timezone + '\'' +
                ", device=" + device +
                ", audioreminders=" + audioreminders +
                ", status='" + status + '\'' +
                ", byodDevice=" + byodDevice +
                ", reviewed=" + reviewed +
                ", pcv=" + pcv +
                '}';
    }
}

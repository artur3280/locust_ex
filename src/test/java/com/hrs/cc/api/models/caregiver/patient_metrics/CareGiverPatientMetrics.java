package com.hrs.cc.api.models.caregiver.patient_metrics;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ts",
        "profile",
        "metrics",
        "ts"
})
public class CareGiverPatientMetrics {

    @JsonProperty("ts")
    private long ts;
    @JsonProperty("profile")
    private Profile profile;
    @JsonProperty("metrics")
    private Metrics metrics;

    public CareGiverPatientMetrics() {
    }

    public CareGiverPatientMetrics(Integer ts, Profile profile, Metrics metrics) {
        this.ts = ts;
        this.profile = profile;
        this.metrics = metrics;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public void setMetrics(Metrics metrics) {
        this.metrics = metrics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CareGiverPatientMetrics that = (CareGiverPatientMetrics) o;
        return Objects.equals(ts, that.ts) &&
                Objects.equals(profile, that.profile) &&
                Objects.equals(metrics, that.metrics);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ts, profile, metrics);
    }

    @Override
    public String toString() {
        return "CareGiverPatientMetrics{" +
                "ts=" + ts +
                ", profile=" + profile +
                ", metrics=" + metrics +
                '}';
    }
}

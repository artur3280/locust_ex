package com.hrs.cc.api.models.requests.edit_clinician;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "rights",
        "subgroups"
})
public class Profile {

    @JsonProperty("rights")
    private String rights;
    @JsonProperty("subgroups")
    private List<String> subgroups = null;

    public Profile() {
    }

    public Profile(String rights, List<String> subgroups) {
        this.rights = rights;
        this.subgroups = subgroups;
    }

    public String getRights() {
        return rights;
    }

    public void setRights(String rights) {
        this.rights = rights;
    }

    public List<String> getSubgroups() {
        return subgroups;
    }

    public void setSubgroups(List<String> subgroups) {
        this.subgroups = subgroups;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "rights='" + rights + '\'' +
                ", subgroups=" + subgroups +
                '}';
    }
}

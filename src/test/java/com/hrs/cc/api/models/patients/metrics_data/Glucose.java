package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "glucose",
        "data",
        "deleted",
        "type",
        "ts"
})
public class Glucose {

    @JsonProperty("glucose")
    private Integer glucose;
    @JsonProperty("data")
    private List<Datum> data = null;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("type")
    private String type;
    @JsonProperty("ts")
    private Long ts;

    public Glucose() {
    }

    public Glucose(Integer glucose, List<Datum> data, Boolean deleted, String type, Long ts) {
        this.glucose = glucose;
        this.data = data;
        this.deleted = deleted;
        this.type = type;
        this.ts = ts;
    }

    public Integer getGlucose() {
        return glucose;
    }

    public void setGlucose(Integer glucose) {
        this.glucose = glucose;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Glucose{" +
                "glucose=" + glucose +
                ", data=" + data +
                ", deleted=" + deleted +
                ", type='" + type + '\'' +
                ", ts=" + ts +
                '}';
    }
}

package com.hrs.cc.api.models.patients.patient_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "last",
        "first",
        "middle"
})
public class Name {

    @JsonProperty("last")
    private String last;
    @JsonProperty("first")
    private String first;
    @JsonProperty("middle")
    private String middle;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Name() {
    }

    public Name(String last, String first, String middle) {
        this.last = last;
        this.first = first;
        this.middle = middle;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getMiddle() {
        return middle;
    }

    public void setMiddle(String middle) {
        this.middle = middle;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Name name = (Name) o;
        return Objects.equals(last, name.last) &&
                Objects.equals(first, name.first) &&
                Objects.equals(middle, name.middle) &&
                Objects.equals(additionalProperties, name.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(last, first, middle, additionalProperties);
    }

    @Override
    public String toString() {
        return "Name{" +
                "last='" + last + '\'' +
                ", first='" + first + '\'' +
                ", middle='" + middle + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.requests.add_metrics_integreation_app.pulseox_manual;

public class FacetPulseOx {
    private String spo2;
    private String hr;

    public FacetPulseOx() {
    }

    public FacetPulseOx(String spo2, String hr) {
        this.spo2 = spo2;
        this.hr = hr;
    }

    public String getSpo2() {
        return spo2;
    }

    public void setSpo2(String spo2) {
        this.spo2 = spo2;
    }

    public String getHr() {
        return hr;
    }

    public void setHr(String hr) {
        this.hr = hr;
    }

    @Override
    public String toString() {
        return "FacetPulseOx{" +
                "spo2='" + spo2 + '\'' +
                ", hr='" + hr + '\'' +
                '}';
    }
}

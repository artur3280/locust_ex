package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "startday",
        "x",
        "schedule"
})
public class Schedule {

    @JsonProperty("type")
    private String type;
    @JsonProperty("startday")
    private Integer startday;
    @JsonProperty("x")
    private Integer x;
    @JsonProperty("schedule")
    private String schedule;

    public Schedule() {
    }

    public Schedule(String type, Integer startday, Integer x, String schedule) {
        this.type = type;
        this.startday = startday;
        this.x = x;
        this.schedule = schedule;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getStartday() {
        return startday;
    }

    public void setStartday(Integer startday) {
        this.startday = startday;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule1 = (Schedule) o;
        return Objects.equals(type, schedule1.type) &&
                Objects.equals(startday, schedule1.startday) &&
                Objects.equals(x, schedule1.x) &&
                Objects.equals(schedule, schedule1.schedule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, startday, x, schedule);
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "type='" + type + '\'' +
                ", startday=" + startday +
                ", x=" + x +
                ", schedule='" + schedule + '\'' +
                '}';
    }
}

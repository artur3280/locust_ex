package com.hrs.cc.api.models.requests.set_pc_voice_settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "weekends",
        "measure",
        "units"
})
public class Every {

    @JsonProperty("weekends")
    private Boolean weekends;
    @JsonProperty("measure")
    private String measure;
    @JsonProperty("units")
    private List<String> units = null;

    public Every() {
    }

    public Every(Boolean weekends, String measure, List<String> units) {
        this.weekends = weekends;
        this.measure = measure;
        this.units = units;
    }

    public Boolean getWeekends() {
        return weekends;
    }

    public void setWeekends(Boolean weekends) {
        this.weekends = weekends;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public List<String> getUnits() {
        return units;
    }

    public void setUnits(List<String> units) {
        this.units = units;
    }
}

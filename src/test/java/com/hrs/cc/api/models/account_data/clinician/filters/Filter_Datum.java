package com.hrs.cc.api.models.account_data.clinician.filters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "term",
        "options"
})
public class Filter_Datum {

    @JsonProperty("term")
    private List term = null;
    @JsonProperty("options")
    private List<Option> options = null;

    public Filter_Datum(List<Object> term, List<Option> options) {
        this.term = term;
        this.options = options;
    }

    public Filter_Datum() {
    }

    public List<Object> getTerm() {
        return term;
    }

    public void setTerm(List<Object> term) {
        this.term = term;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "Filter_Datum{" +
                "term=" + term +
                ", options=" + options +
                '}';
    }
}

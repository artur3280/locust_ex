package com.hrs.cc.api.models.reports.statuses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "progress",
        "status",
        "id",
        "report-type",
        "password",
        "start",
        "stop",
        "downloaded",
        "type"
})
public class Datum {

    @JsonProperty("progress")
    private Integer progress;
    @JsonProperty("status")
    private String status;
    @JsonProperty("id")
    private String id;
    @JsonProperty("report-type")
    private String reportType;
    @JsonProperty("password")
    private String password;
    @JsonProperty("start")
    private Start start;
    @JsonProperty("stop")
    private Stop stop;
    @JsonProperty("downloaded")
    private Boolean downloaded;
    @JsonProperty("type")
    private String type;

    public Datum() {
    }

    public Datum(Integer progress, String status, String id, String reportType, String password, Start start, Stop stop, Boolean downloaded, String type) {
        this.progress = progress;
        this.status = status;
        this.id = id;
        this.reportType = reportType;
        this.password = password;
        this.start = start;
        this.stop = stop;
        this.downloaded = downloaded;
        this.type = type;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Start getStart() {
        return start;
    }

    public void setStart(Start start) {
        this.start = start;
    }

    public Stop getStop() {
        return stop;
    }

    public void setStop(Stop stop) {
        this.stop = stop;
    }

    public Boolean getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Boolean downloaded) {
        this.downloaded = downloaded;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Datum datum = (Datum) o;
        return Objects.equals(progress, datum.progress) &&
                Objects.equals(status, datum.status) &&
                Objects.equals(id, datum.id) &&
                Objects.equals(reportType, datum.reportType) &&
                Objects.equals(password, datum.password) &&
                Objects.equals(start, datum.start) &&
                Objects.equals(stop, datum.stop) &&
                Objects.equals(downloaded, datum.downloaded) &&
                Objects.equals(type, datum.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(progress, status, id, reportType, password, start, stop, downloaded, type);
    }

    @Override
    public String toString() {
        return "DatumTask{" +
                "progress=" + progress +
                ", status='" + status + '\'' +
                ", id='" + id + '\'' +
                ", reportType='" + reportType + '\'' +
                ", password='" + password + '\'' +
                ", start=" + start +
                ", stop=" + stop +
                ", downloaded=" + downloaded +
                ", type='" + type + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.requests.set_pc_voice_settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ivrQuestionId",
        "status",
        "schedule"
})
public class Entry {

    @JsonProperty("ivrQuestionId")
    private Integer ivrQuestionId;
    @JsonProperty("status")
    private String status;
    @JsonProperty("schedule")
    private Schedule schedule;

    public Entry() {
    }

    public Entry(Integer ivrQuestionId, String status, Schedule schedule) {
        this.ivrQuestionId = ivrQuestionId;
        this.status = status;
        this.schedule = schedule;
    }

    public Integer getIvrQuestionId() {
        return ivrQuestionId;
    }

    public void setIvrQuestionId(Integer ivrQuestionId) {
        this.ivrQuestionId = ivrQuestionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }
}

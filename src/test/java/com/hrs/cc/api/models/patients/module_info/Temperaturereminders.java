package com.hrs.cc.api.models.patients.module_info;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active"
})
public class Temperaturereminders {

    @JsonProperty("active")
    private String active;

    public Temperaturereminders() {
    }

    public Temperaturereminders(String active) {
        this.active = active;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Temperaturereminders{" +
                "active='" + active + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.caregiver.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "chatroomId",
        "hrsid",
        "message",
        "createdAt"
})

public class Attributes {

    @JsonProperty("chatroomId")
    private Integer chatroomId;
    @JsonProperty("hrsid")
    private String hrsid;
    @JsonProperty("message")
    private String message;
    @JsonProperty("createdAt")
    private String createdAt;

    public Attributes() {
    }

    public Attributes(Integer chatroomId, String hrsid, String message, String createdAt) {
        this.chatroomId = chatroomId;
        this.hrsid = hrsid;
        this.message = message;
        this.createdAt = createdAt;
    }

    public Integer getChatroomId() {
        return chatroomId;
    }

    public void setChatroomId(Integer chatroomId) {
        this.chatroomId = chatroomId;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "chatroomId=" + chatroomId +
                ", hrsid='" + hrsid + '\'' +
                ", message='" + message + '\'' +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }
}

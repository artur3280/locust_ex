package com.hrs.cc.api.models.environment.settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "type",
        "conditions",
        "start"
})
public class Content {

    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("conditions")
    private List<String> conditions = null;
    @JsonProperty("start")
    private String start;

    public Content() {
    }

    public Content(String id, String type, List<String> conditions, String start) {
        this.id = id;
        this.type = type;
        this.conditions = conditions;
        this.start = start;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    @Override
    public String toString() {
        return "Content{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", conditions=" + conditions +
                ", start='" + start + '\'' +
                '}';
    }
}

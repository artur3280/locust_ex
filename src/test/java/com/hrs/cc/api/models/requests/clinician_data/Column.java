package com.hrs.cc.api.models.requests.clinician_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "visible",
        "width",
        "sort",
        "filters"
})
public class Column {

    @JsonProperty("name")
    private String name;
    @JsonProperty("visible")
    private Boolean visible;
    @JsonProperty("width")
    private String width;
    @JsonProperty("sort")
    private Sort sort;
    @JsonProperty("filters")
    private List<Filter> filters = null;

    public Column() {
    }

    public Column(String name, Boolean visible, String width, Sort sort, List<Filter> filters) {
        this.name = name;
        this.visible = visible;
        this.width = width;
        this.sort = sort;
        this.filters = filters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    @Override
    public String toString() {
        return "Column{" +
                "name='" + name + '\'' +
                ", visible=" + visible +
                ", width='" + width + '\'' +
                ", sort=" + sort +
                ", filters=" + filters +
                '}';
    }
}

package com.hrs.cc.api.models.requests.add_metrics_integreation_app.weight_manual;

public class Facet {
    private Integer weight;

    /**
     * No args constructor for use in serialization
     *
     */
    public Facet() {
    }

    /**
     *
     * @param weight
     */
    public Facet(Integer weight) {
        super();
        this.weight = weight;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Facet{" +
                "weight=" + weight +
                '}';
    }
}

package com.hrs.cc.api.models.integratioins_app.user_by_type_in_env;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "firstName",
        "lastName",
        "environment",
        "phoneNumber",
        "profile",
        "middleName",
        "id",
        "type",
        "email",
        "status"
})
public class Datum {

    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("environment")
    private String environment;
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("profile")
    private Profile profile;
    @JsonProperty("middleName")
    private String middleName;
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("email")
    private String email;
    @JsonProperty("status")
    private String status;

    public Datum() {
    }

    public Datum(String firstName, String lastName, String environment, String phoneNumber, Profile profile, String middleName, String id, String type, String email, String status) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.environment = environment;
        this.phoneNumber = phoneNumber;
        this.profile = profile;
        this.middleName = middleName;
        this.id = id;
        this.type = type;
        this.email = email;
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Datum datum = (Datum) o;
        return Objects.equals(firstName, datum.firstName) &&
                Objects.equals(lastName, datum.lastName) &&
                Objects.equals(environment, datum.environment) &&
                Objects.equals(phoneNumber, datum.phoneNumber) &&
                Objects.equals(profile, datum.profile) &&
                Objects.equals(middleName, datum.middleName) &&
                Objects.equals(id, datum.id) &&
                Objects.equals(type, datum.type) &&
                Objects.equals(email, datum.email) &&
                Objects.equals(status, datum.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, environment, phoneNumber, profile, middleName, id, type, email, status);
    }

    @Override
    public String toString() {
        return "Datum{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", environment='" + environment + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", profile=" + profile +
                ", middleName='" + middleName + '\'' +
                ", id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", email='" + email + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}

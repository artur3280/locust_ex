package com.hrs.cc.api.models.patients.notes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "location",
        "los"
})
public class Edvisit {

    @JsonProperty("date")
    private Long date;
    @JsonProperty("location")
    private String location;
    @JsonProperty("los")
    private Integer los;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Edvisit() {
    }

    public Edvisit(Long date, String location, Integer los) {
        this.date = date;
        this.location = location;
        this.los = los;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getLos() {
        return los;
    }

    public void setLos(Integer los) {
        this.los = los;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edvisit edvisit = (Edvisit) o;
        return Objects.equals(date, edvisit.date) &&
                Objects.equals(location, edvisit.location) &&
                Objects.equals(los, edvisit.los) &&
                Objects.equals(additionalProperties, edvisit.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(date, location, los, additionalProperties);
    }

    @Override
    public String toString() {
        return "Edvisit{" +
                "date=" + date +
                ", location='" + location + '\'' +
                ", los=" + los +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

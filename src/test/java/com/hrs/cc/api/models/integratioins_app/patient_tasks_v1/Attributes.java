package com.hrs.cc.api.models.integratioins_app.patient_tasks_v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "glucose",
        "data",
        "ts",
        "duration",
        "action",
        "unit",
        "temperature",
        "weight",
        "schedule",
        "window",
        "instruction",
        "essential",
        "systolic",
        "diastolic",
        "heartrate",
        "spo2",
        "answersHaveNoOrder",
        "areAnswersInReverseOrder",
        "title"
})
public class Attributes {

    @JsonProperty("type")
    private String type;
    @JsonProperty("glucose")
    private Integer glucose;
    @JsonProperty("data")
    private List<Datum_> data = null;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("duration")
    private Integer duration;
    @JsonProperty("action")
    private Action action;
    @JsonProperty("unit")
    private String unit;
    @JsonProperty("temperature")
    private Integer temperature;
    @JsonProperty("weight")
    private Integer weight;
    @JsonProperty("schedule")
    private Schedule schedule;
    @JsonProperty("window")
    private Integer window;
    @JsonProperty("instruction")
    private String instruction;
    @JsonProperty("essential")
    private Boolean essential;
    @JsonProperty("systolic")
    private Integer systolic;
    @JsonProperty("diastolic")
    private Integer diastolic;
    @JsonProperty("heartrate")
    private Integer heartrate;
    @JsonProperty("spo2")
    private Integer spo2;
    @JsonProperty("answersHaveNoOrder")
    private Boolean answersHaveNoOrder;
    @JsonProperty("areAnswersInReverseOrder")
    private Boolean areAnswersInReverseOrder;
    @JsonProperty("title")
    private String title;

    public Attributes() {
    }

    public Attributes(String type, Integer glucose, List<Datum_> data, Long ts, Integer duration, Action action, String unit, Integer temperature, Integer weight, Schedule schedule, Integer window, String instruction, Boolean essential, Integer systolic, Integer diastolic, Integer heartrate, Integer spo2, Boolean answersHaveNoOrder, Boolean areAnswersInReverseOrder, String title) {
        this.type = type;
        this.glucose = glucose;
        this.data = data;
        this.ts = ts;
        this.duration = duration;
        this.action = action;
        this.unit = unit;
        this.temperature = temperature;
        this.weight = weight;
        this.schedule = schedule;
        this.window = window;
        this.instruction = instruction;
        this.essential = essential;
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.heartrate = heartrate;
        this.spo2 = spo2;
        this.answersHaveNoOrder = answersHaveNoOrder;
        this.areAnswersInReverseOrder = areAnswersInReverseOrder;
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getGlucose() {
        return glucose;
    }

    public void setGlucose(Integer glucose) {
        this.glucose = glucose;
    }

    public List<Datum_> getData() {
        return data;
    }

    public void setData(List<Datum_> data) {
        this.data = data;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Integer getWindow() {
        return window;
    }

    public void setWindow(Integer window) {
        this.window = window;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public Boolean getEssential() {
        return essential;
    }

    public void setEssential(Boolean essential) {
        this.essential = essential;
    }

    public Integer getSystolic() {
        return systolic;
    }

    public void setSystolic(Integer systolic) {
        this.systolic = systolic;
    }

    public Integer getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(Integer diastolic) {
        this.diastolic = diastolic;
    }

    public Integer getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(Integer heartrate) {
        this.heartrate = heartrate;
    }

    public Integer getSpo2() {
        return spo2;
    }

    public void setSpo2(Integer spo2) {
        this.spo2 = spo2;
    }

    public Boolean getAnswersHaveNoOrder() {
        return answersHaveNoOrder;
    }

    public void setAnswersHaveNoOrder(Boolean answersHaveNoOrder) {
        this.answersHaveNoOrder = answersHaveNoOrder;
    }

    public Boolean getAreAnswersInReverseOrder() {
        return areAnswersInReverseOrder;
    }

    public void setAreAnswersInReverseOrder(Boolean areAnswersInReverseOrder) {
        this.areAnswersInReverseOrder = areAnswersInReverseOrder;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "type='" + type + '\'' +
                ", glucose=" + glucose +
                ", data=" + data +
                ", ts=" + ts +
                ", duration=" + duration +
                ", action=" + action +
                ", unit='" + unit + '\'' +
                ", temperature=" + temperature +
                ", weight=" + weight +
                ", schedule=" + schedule +
                ", window=" + window +
                ", instruction='" + instruction + '\'' +
                ", essential=" + essential +
                ", systolic=" + systolic +
                ", diastolic=" + diastolic +
                ", heartrate=" + heartrate +
                ", spo2=" + spo2 +
                ", answersHaveNoOrder=" + answersHaveNoOrder +
                ", areAnswersInReverseOrder=" + areAnswersInReverseOrder +
                ", title='" + title + '\'' +
                '}';
    }
}

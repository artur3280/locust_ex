package com.hrs.cc.api.models.patients.module_info;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "hours",
        "minutes",
        "ampm",
        "window"
})
public class Weightreminders {

    @JsonProperty("hours")
    private String hours;
    @JsonProperty("minutes")
    private String minutes;
    @JsonProperty("ampm")
    private String ampm;
    @JsonProperty("window")
    private String window;

    public Weightreminders() {
    }

    public Weightreminders(String hours, String minutes, String ampm, String window) {
        this.hours = hours;
        this.minutes = minutes;
        this.ampm = ampm;
        this.window = window;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String getAmpm() {
        return ampm;
    }

    public void setAmpm(String ampm) {
        this.ampm = ampm;
    }

    public String getWindow() {
        return window;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    @Override
    public String toString() {
        return "Weightreminders{" +
                "hours='" + hours + '\'' +
                ", minutes='" + minutes + '\'' +
                ", ampm='" + ampm + '\'' +
                ", window='" + window + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_manual;

public class FacetMedication {
    private String medication;
    private String dosage;

    /**
     * No args constructor for use in serialization
     *
     */
    public FacetMedication() {
    }

    /**
     *
     * @param medication
     * @param dosage
     */
    public FacetMedication(String medication, String dosage) {
        this.medication = medication;
        this.dosage = dosage;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    @Override
    public String toString() {
        return "FacetMedication{" +
                "medication='" + medication + '\'' +
                ", dosage='" + dosage + '\'' +
                '}';
    }
}

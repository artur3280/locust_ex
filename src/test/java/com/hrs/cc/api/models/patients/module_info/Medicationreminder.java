package com.hrs.cc.api.models.patients.module_info;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "medication",
        "dose",
        "times",
        "window",
        "schedule"
})
public class Medicationreminder {

    @JsonProperty("medication")
    private String medication;
    @JsonProperty("dose")
    private String dose;
    @JsonProperty("times")
    private String times;
    @JsonProperty("window")
    private String window;
    @JsonProperty("schedule")
    private Schedule schedule;

    public Medicationreminder() {
    }

    public Medicationreminder(String medication, String dose, String times, String window, Schedule schedule) {
        this.medication = medication;
        this.dose = dose;
        this.times = times;
        this.window = window;
        this.schedule = schedule;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getWindow() {
        return window;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return "Medicationreminder{" +
                "medication='" + medication + '\'' +
                ", dose='" + dose + '\'' +
                ", times='" + times + '\'' +
                ", window='" + window + '\'' +
                ", schedule=" + schedule +
                '}';
    }
}

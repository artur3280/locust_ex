package com.hrs.cc.api.models.admin.surveys;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "surveylist"
})
public class AdminSurveys {

    @JsonProperty("surveylist")
    private List<Surveylist> surveylist = null;

    public AdminSurveys() {}

    public AdminSurveys(List<Surveylist> surveylist) {
        this.surveylist = surveylist;
    }

    public List<Surveylist> getSurveylist() {
        return surveylist;
    }

    public void setSurveylist(List<Surveylist> surveylist) {
        this.surveylist = surveylist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdminSurveys that = (AdminSurveys) o;
        return Objects.equals(surveylist, that.surveylist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(surveylist);
    }

    @Override
    public String toString() {
        return "AdminSurveys{" +
                "surveylist=" + surveylist +
                '}';
    }
}

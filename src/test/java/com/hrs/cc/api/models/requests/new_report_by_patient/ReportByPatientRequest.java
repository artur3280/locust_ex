package com.hrs.cc.api.models.requests.new_report_by_patient;

public class ReportByPatientRequest {
    private String report;
    private String patient;
    private String start;
    private String stop;

    public ReportByPatientRequest(String report, String patient, String start, String stop) {
        this.report = report;
        this.patient = patient;
        this.start = start;
        this.stop = stop;
    }

    public ReportByPatientRequest() {
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    @Override
    public String toString() {
        return "ReportRequest{" +
                "report='" + report + '\'' +
                ", patient='" + patient + '\'' +
                ", start='" + start + '\'' +
                ", stop='" + stop + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.patients.patient_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "goal"
})
public class Activity {

    @JsonProperty("goal")
    private Integer goal;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Activity() {
    }

    public Activity(Integer goal) {
        this.goal = goal;
    }

    public Integer getGoal() {
        return goal;
    }

    public void setGoal(Integer goal) {
        this.goal = goal;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Activity activity = (Activity) o;
        return Objects.equals(goal, activity.goal) &&
                Objects.equals(additionalProperties, activity.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(goal, additionalProperties);
    }

    @Override
    public String toString() {
        return "Activity{" +
                "goal=" + goal +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

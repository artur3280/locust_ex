package com.hrs.cc.api.models.requests.add_metrics_integreation_app.image_manual;

public class FacetImage {
    private String image;

    /**
     * No args constructor for use in serialization
     *
     */
    public FacetImage() {
    }

    public FacetImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "FacetImage{" +
                "image='" + image + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.integratioins_app.ivr.question;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "environment",
        "question",
        "category",
        "choices",
        "createdAt",
        "updatedAt"
})
public class Datum {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("environment")
    private String environment;
    @JsonProperty("question")
    private String question;
    @JsonProperty("category")
    private String category;
    @JsonProperty("choices")
    private List<Choice> choices = null;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("updatedAt")
    private String updatedAt;

    public Datum() {
    }

    public Datum(Integer id, String environment, String question, String category, List<Choice> choices, String createdAt, String updatedAt) {
        this.id = id;
        this.environment = environment;
        this.question = question;
        this.category = category;
        this.choices = choices;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Datum datum = (Datum) o;
        return Objects.equals(id, datum.id) &&
                Objects.equals(environment, datum.environment) &&
                Objects.equals(question, datum.question) &&
                Objects.equals(category, datum.category) &&
                Objects.equals(choices, datum.choices) &&
                Objects.equals(createdAt, datum.createdAt) &&
                Objects.equals(updatedAt, datum.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, environment, question, category, choices, createdAt, updatedAt);
    }

    @Override
    public String toString() {
        return "Datum{" +
                "id=" + id +
                ", environment='" + environment + '\'' +
                ", question='" + question + '\'' +
                ", category='" + category + '\'' +
                ", choices=" + choices +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}

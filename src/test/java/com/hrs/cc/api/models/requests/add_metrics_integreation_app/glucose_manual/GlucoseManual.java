package com.hrs.cc.api.models.requests.add_metrics_integreation_app.glucose_manual;

public class GlucoseManual {
    private DataGlucose data;

    /**
     * No args constructor for use in serialization
     *
     */
    public GlucoseManual() {
    }

    /**
     *
     * @param data
     */
    public GlucoseManual(DataGlucose data) {
        super();
        this.data = data;
    }

    public DataGlucose getData() {
        return data;
    }

    public void setData(DataGlucose data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "GlucoseManual{" +
                "data=" + data +
                '}';
    }
}

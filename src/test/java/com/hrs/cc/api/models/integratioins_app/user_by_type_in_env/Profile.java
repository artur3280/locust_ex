package com.hrs.cc.api.models.integratioins_app.user_by_type_in_env;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "subgroup",
        "rights",
        "username"
})
public class Profile {

    @JsonProperty("subgroup")
    private String subgroup;
    @JsonProperty("rights")
    private String rights;
    @JsonProperty("username")
    private String username;

    public Profile() {
    }

    public Profile(String subgroup, String rights, String username) {
        this.subgroup = subgroup;
        this.rights = rights;
        this.username = username;
    }

    public String getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(String subgroup) {
        this.subgroup = subgroup;
    }

    public String getRights() {
        return rights;
    }

    public void setRights(String rights) {
        this.rights = rights;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "subgroup='" + subgroup + '\'' +
                ", rights='" + rights + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}

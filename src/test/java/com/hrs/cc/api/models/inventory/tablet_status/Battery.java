package com.hrs.cc.api.models.inventory.tablet_status;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "level",
        "status"
})
public class Battery {

    @JsonProperty("level")
    private Long level;
    @JsonProperty("status")
    private String status;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Battery() {
    }

    public Battery(Long level, String status) {
        this.level = level;
        this.status = status;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Battery battery = (Battery) o;
        return Objects.equals(level, battery.level) &&
                Objects.equals(status, battery.status) &&
                Objects.equals(additionalProperties, battery.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(level, status, additionalProperties);
    }

    @Override
    public String toString() {
        return "Battery{" +
                "level=" + level +
                ", status='" + status + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

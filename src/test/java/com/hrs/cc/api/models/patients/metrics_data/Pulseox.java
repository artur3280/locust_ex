package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "spo2",
        "heartrate",
        "deleted",
        "type",
        "ts"
})
public class Pulseox {

    @JsonProperty("data")
    private List<Datum______> data = null;
    @JsonProperty("spo2")
    private Integer spo2;
    @JsonProperty("heartrate")
    private Integer heartrate;
    @JsonProperty("type")
    private String type;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("ts")
    private Long ts;

    public Pulseox() {
    }

    public Pulseox(List<Datum______> data, Integer spo2, Integer heartrate, String type, Boolean deleted, Long ts) {
        this.data = data;
        this.spo2 = spo2;
        this.heartrate = heartrate;
        this.type = type;
        this.deleted = deleted;
        this.ts = ts;
    }

    public List<Datum______> getData() {
        return data;
    }

    public void setData(List<Datum______> data) {
        this.data = data;
    }

    public Integer getSpo2() {
        return spo2;
    }

    public void setSpo2(Integer spo2) {
        this.spo2 = spo2;
    }

    public Integer getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(Integer heartrate) {
        this.heartrate = heartrate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Pulseox{" +
                "data=" + data +
                ", spo2=" + spo2 +
                ", heartrate=" + heartrate +
                ", type='" + type + '\'' +
                ", deleted='" + deleted + '\'' +
                ", ts=" + ts +
                '}';
    }
}

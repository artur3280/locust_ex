package com.hrs.cc.api.models.patients.patient_list;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "schedule",
        "dose",
        "time",
        "med",
        "expiration",
        "essential",
        "instruction"
})
public class MedicationItem {
    @JsonProperty("schedule")
    private Schedule schedule;
    @JsonProperty("dose")
    private String dose;
    @JsonProperty("time")
    private String time;
    @JsonProperty("med")
    private String med;
    @JsonProperty("expiration")
    private String expiration;
    @JsonProperty("essential")
    private Boolean essential;
    @JsonProperty("instruction")
    private String instruction;

    public MedicationItem() {
    }

    public MedicationItem(Schedule schedule, String dose, String time, String med, String expiration, Boolean essential, String instruction) {
        this.schedule = schedule;
        this.dose = dose;
        this.time = time;
        this.med = med;
        this.expiration = expiration;
        this.essential = essential;
        this.instruction = instruction;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMed() {
        return med;
    }

    public void setMed(String med) {
        this.med = med;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public Boolean getEssential() {
        return essential;
    }

    public void setEssential(Boolean essential) {
        this.essential = essential;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
}

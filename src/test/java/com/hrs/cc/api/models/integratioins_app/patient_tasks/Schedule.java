package com.hrs.cc.api.models.integratioins_app.patient_tasks;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "at",
        "every",
        "start",
        "window",
        "end",
        "reminder"
})
public class Schedule {

    @JsonProperty("at")
    private String at;
    @JsonProperty("every")
    private Every every;
    @JsonProperty("start")
    private String start;
    @JsonProperty("window")
    private Integer window;
    @JsonProperty("end")
    private String end;
    @JsonProperty("reminder")
    private String reminder;

    public Schedule(String at, Every every, String start, Integer window, String end, String reminder) {
        this.at = at;
        this.every = every;
        this.start = start;
        this.window = window;
        this.end = end;
        this.reminder = reminder;
    }

    public Schedule() {
    }

    public String getAt() {
        return at;
    }

    public void setAt(String at) {
        this.at = at;
    }

    public Every getEvery() {
        return every;
    }

    public void setEvery(Every every) {
        this.every = every;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public Integer getWindow() {
        return window;
    }

    public void setWindow(Integer window) {
        this.window = window;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getReminder() {
        return reminder;
    }

    public void setReminder(String reminder) {
        this.reminder = reminder;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "at='" + at + '\'' +
                ", every=" + every +
                ", start='" + start + '\'' +
                ", window=" + window +
                ", end='" + end + '\'' +
                ", reminder='" + reminder + '\'' +
                '}';
    }
}

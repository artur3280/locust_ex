package com.hrs.cc.api.models.patients.caregivers_list;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


public class Datum {

    private Integer id;
    private String patient;
    private String caregiver;
    private String environment;
    private String created;

    public Datum() {
    }

    public Datum(Integer id, String patient, String caregiver, String environment, String created) {
        this.id = id;
        this.patient = patient;
        this.caregiver = caregiver;
        this.environment = environment;
        this.created = created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(String caregiver) {
        this.caregiver = caregiver;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "id=" + id +
                ", patient='" + patient + '\'' +
                ", caregiver='" + caregiver + '\'' +
                ", environment='" + environment + '\'' +
                ", created='" + created + '\'' +
                '}';
    }
}

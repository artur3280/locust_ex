package com.hrs.cc.api.models.requests.edit_clinician;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class ClinicianEdit {

    @JsonProperty("data")
    private Datum data = null;

    public ClinicianEdit() {
    }

    public ClinicianEdit(Datum data) {
        this.data = data;
    }

    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClinicianEdit{" +
                "data=" + data +
                '}';
    }
}

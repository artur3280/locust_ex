package com.hrs.cc.api.models.integratioins_app.patient_tasks_v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "at",
        "every",
        "end",
        "start",
        "window",
        "reminder"
})
public class Schedule {

    @JsonProperty("at")
    private String at;
    @JsonProperty("every")
    private Every every;
    @JsonProperty("end")
    private String end;
    @JsonProperty("start")
    private String start;
    @JsonProperty("window")
    private Integer window;
    @JsonProperty("reminder")
    private String reminder;

    public Schedule() {
    }

    public Schedule(String at, Every every, String end, String start, Integer window, String reminder) {
        this.at = at;
        this.every = every;
        this.end = end;
        this.start = start;
        this.window = window;
        this.reminder = reminder;
    }

    public String getAt() {
        return at;
    }

    public void setAt(String at) {
        this.at = at;
    }

    public Every getEvery() {
        return every;
    }

    public void setEvery(Every every) {
        this.every = every;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public Integer getWindow() {
        return window;
    }

    public void setWindow(Integer window) {
        this.window = window;
    }

    public String getReminder() {
        return reminder;
    }

    public void setReminder(String reminder) {
        this.reminder = reminder;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "at='" + at + '\'' +
                ", every=" + every +
                ", end='" + end + '\'' +
                ", start='" + start + '\'' +
                ", window=" + window +
                ", reminder='" + reminder + '\'' +
                '}';
    }
}

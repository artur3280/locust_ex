package com.hrs.cc.api.models.environment.data.environment_settings_type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "subgroups"
})
public class Subgroups {
    @JsonProperty("subgroups")
    private List<String> subgroups = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Subgroups() {
    }

    public Subgroups(List<String> subgroups) {
        this.subgroups = subgroups;
    }

    public List<String> getSubgroups() {
        return subgroups;
    }

    public void setSubgroups(List<String> subgroups) {
        this.subgroups = subgroups;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subgroups subgroups1 = (Subgroups) o;
        return Objects.equals(subgroups, subgroups1.subgroups) &&
                Objects.equals(additionalProperties, subgroups1.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(subgroups, additionalProperties);
    }

    @Override
    public String toString() {
        return "Subgroups{" +
                "subgroups=" + subgroups +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.admin.edit_survey;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "type",
        "best",
        "worst",
        "values"
})
public class SurveysTypes {

    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("best")
    private String best;
    @JsonProperty("worst")
    private String worst;
    @JsonProperty("values")
    private List<Value> values = null;

    public SurveysTypes() {
    }

    public SurveysTypes(String id, String type, String best, String worst, List<Value> values) {
        this.id = id;
        this.type = type;
        this.best = best;
        this.worst = worst;
        this.values = values;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBest() {
        return best;
    }

    public void setBest(String best) {
        this.best = best;
    }

    public String getWorst() {
        return worst;
    }

    public void setWorst(String worst) {
        this.worst = worst;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "SurveysTypes{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", best='" + best + '\'' +
                ", worst='" + worst + '\'' +
                ", values=" + values +
                '}';
    }
}

package com.hrs.cc.api.models.inventory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "autoId",
        "name",
        "environment",
        "patient",
        "status",
        "lastUpdated",
        "devid",
        "assignment",
        "subgroup",
        "deletedAt",
        "createdAt"
})
public class InventoryResponse {

    @JsonProperty("autoId")
    private Integer autoId;
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("environment")
    private String environment;
    @JsonProperty("patient")
    private Patient patient;
    @JsonProperty("status")
    private Object status;
    @JsonProperty("lastUpdated")
    private LastUpdated lastUpdated;
    @JsonProperty("devid")
    private String devid;
    @JsonProperty("assignment")
    private Assignment assignment;
    @JsonProperty("subgroup")
    private Object subgroup;
    @JsonProperty("createdAt")
    private CreatedAt createdAt;
    @JsonProperty("deletedAt")
    private Object deletedAt = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public InventoryResponse() {
    }

    public InventoryResponse(Integer autoId, String id, String name, String environment, Patient patient, Object status, LastUpdated lastUpdated, String devid, Assignment assignment, Object subgroup, CreatedAt createdAt, Map<String, Object> additionalProperties) {
        this.autoId = autoId;
        this.id = id;
        this.name = name;
        this.environment = environment;
        this.patient = patient;
        this.status = status;
        this.lastUpdated = lastUpdated;
        this.devid = devid;
        this.assignment = assignment;
        this.subgroup = subgroup;
        this.createdAt = createdAt;
        this.additionalProperties = additionalProperties;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public LastUpdated getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LastUpdated lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getDevid() {
        return devid;
    }

    public void setDevid(String devid) {
        this.devid = devid;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public Object getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(Object subgroup) {
        this.subgroup = subgroup;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public Integer getAutoId() {
        return autoId;
    }

    public void setAutoId(Integer autoId) {
        this.autoId = autoId;
    }

    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "InventoryResponse{" +
                "autoId=" + autoId +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", environment='" + environment + '\'' +
                ", patient=" + patient +
                ", status=" + status +
                ", lastUpdated=" + lastUpdated +
                ", devid='" + devid + '\'' +
                ", assignment=" + assignment +
                ", subgroup=" + subgroup +
                ", createdAt=" + createdAt +
                '}';
    }
}

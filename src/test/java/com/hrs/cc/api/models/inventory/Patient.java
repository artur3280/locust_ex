package com.hrs.cc.api.models.inventory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "patient",
        "pid",
        "name",
        "dob",
        "gender",
        "phone",
        "status",
        "device",
        "hrsid"
})
public class Patient {

    @JsonProperty("patient")
    private String patient;
    @JsonProperty("pid")
    private String pid;
    @JsonProperty("name")
    private Name name;
    @JsonProperty("dob")
    private String dob;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("phone")
    private Object phone;
    @JsonProperty("status")
    private String status;
    @JsonProperty("device")
    private String device;
    @JsonProperty("hrsid")
    private String hrsid;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Patient() {
    }

    public Patient(String patient, String pid, Name name, String dob, String gender, Object phone, String status, String device, String hrsid) {
        this.patient = patient;
        this.pid = pid;
        this.name = name;
        this.dob = dob;
        this.gender = gender;
        this.phone = phone;
        this.status = status;
        this.device = device;
        this.hrsid = hrsid;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient1 = (Patient) o;
        return Objects.equals(patient, patient1.patient) &&
                Objects.equals(pid, patient1.pid) &&
                Objects.equals(name, patient1.name) &&
                Objects.equals(dob, patient1.dob) &&
                Objects.equals(gender, patient1.gender) &&
                Objects.equals(phone, patient1.phone) &&
                Objects.equals(status, patient1.status) &&
                Objects.equals(device, patient1.device) &&
                Objects.equals(hrsid, patient1.hrsid) &&
                Objects.equals(additionalProperties, patient1.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(patient, pid, name, dob, gender, phone, status, device, hrsid, additionalProperties);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "patient='" + patient + '\'' +
                ", pid='" + pid + '\'' +
                ", name=" + name +
                ", dob='" + dob + '\'' +
                ", gender='" + gender + '\'' +
                ", phone=" + phone +
                ", status='" + status + '\'' +
                ", device='" + device + '\'' +
                ", hrsid='" + hrsid + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

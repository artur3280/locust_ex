package com.hrs.cc.api.models.requests.clinician_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "value"
})
public class Option {

    @JsonProperty("id")
    private String id;
    @JsonProperty("value")
    private String value;

    public Option(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public Option() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Option{" +
                "id='" + id + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.patients.module_info;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "expiration",
        "essential",
        "instruction",
        "startday",
        "schedule",
        "x"
})
public class Schedule {

    @JsonProperty("type")
    private String type;
    @JsonProperty("expiration")
    private String expiration;
    @JsonProperty("essential")
    private String essential;
    @JsonProperty("instruction")
    private String instruction;
    @JsonProperty("x")
    private String x;
    @JsonProperty("startday")
    private String startday;
    @JsonProperty("schedule")
    private String schedule;

    public Schedule() {
    }

    public Schedule(String type, String expiration, String essential, String instruction, String x, String startday, String schedule) {
        this.type = type;
        this.expiration = expiration;
        this.essential = essential;
        this.instruction = instruction;
        this.x = x;
        this.startday = startday;
        this.schedule = schedule;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getEssential() {
        return essential;
    }

    public void setEssential(String essential) {
        this.essential = essential;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getStartday() {
        return startday;
    }

    public void setStartday(String startday) {
        this.startday = startday;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "type='" + type + '\'' +
                ", expiration='" + expiration + '\'' +
                ", essential='" + essential + '\'' +
                ", instruction='" + instruction + '\'' +
                ", x='" + x + '\'' +
                ", startday='" + startday + '\'' +
                ", schedule='" + schedule + '\'' +
                '}';
    }
}

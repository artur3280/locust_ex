package com.hrs.cc.api.models.integratioins_app.patient_caregiver;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "caregiver"
})
public class Sideload {

    @JsonProperty("caregiver")
    private Map<String, Caregiver> caregiver;

    public Sideload() {
    }

    public Sideload(Map<String,  Caregiver> caregiver) {
        this.caregiver = caregiver;
    }

    public Map<String,  Caregiver> getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Map<String,Caregiver> caregiver) {
        this.caregiver = caregiver;
    }

    @Override
    public String toString() {
        return "Sideload{" +
                "caregiver=" + caregiver +
                '}';
    }
}

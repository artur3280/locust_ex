package com.hrs.cc.api.models.integratioins_app.notifications;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "notifications",
        "status"
})
public class ClinicianNotifications {

    @JsonProperty("notifications")
    private List<Notification> notifications = null;
    @JsonProperty("status")
    private String status;

    public ClinicianNotifications() {
    }

    public ClinicianNotifications(List<Notification> notifications, String status) {
        this.notifications = notifications;
        this.status = status;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClinicianNotifications{" +
                "notifications=" + notifications +
                ", status='" + status + '\'' +
                '}';
    }
}

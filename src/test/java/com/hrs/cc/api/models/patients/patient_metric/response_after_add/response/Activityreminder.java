package com.hrs.cc.api.models.patients.patient_metric.response_after_add.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "time",
        "window"
})
public class Activityreminder {

    @JsonProperty("time")
    private String time;
    @JsonProperty("window")
    private String window;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Activityreminder() {
    }

    public Activityreminder(String time, String window) {
        this.time = time;
        this.window = window;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWindow() {
        return window;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Activityreminder that = (Activityreminder) o;
        return Objects.equals(time, that.time) &&
                Objects.equals(window, that.window) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(time, window, additionalProperties);
    }

    @Override
    public String toString() {
        return "Activityreminder{" +
                "time='" + time + '\'' +
                ", window='" + window + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.patients.syrvey;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "surveystatus",
        "schedule",
        "medalert",
        "highalert",
        "category",
        "ques",
        "quesid",
        "answertype",
        "today",
        "answered",
        "answerscale"
})
public class Surveydatum {

    @JsonProperty("surveystatus")
    private String surveystatus;
    @JsonProperty("schedule")
    private Schedule schedule;
    @JsonProperty("medalert")
    private String medalert;
    @JsonProperty("highalert")
    private String highalert;
    @JsonProperty("category")
    private String category;
    @JsonProperty("ques")
    private String ques;
    @JsonProperty("quesid")
    private String quesid;
    @JsonProperty("answertype")
    private String answertype;
    @JsonProperty("today")
    private String today;
    @JsonProperty("answered")
    private Long answered;
    @JsonProperty("answerscale")
    private List<Answerscale> answerscale = null;

    public Surveydatum() {
    }

    public Surveydatum(String surveystatus, Schedule schedule, String medalert, String highalert, String category, String ques, String quesid, String answertype, String today, Long answered, List<Answerscale> answerscale) {
        this.surveystatus = surveystatus;
        this.schedule = schedule;
        this.medalert = medalert;
        this.highalert = highalert;
        this.category = category;
        this.ques = ques;
        this.quesid = quesid;
        this.answertype = answertype;
        this.today = today;
        this.answered = answered;
        this.answerscale = answerscale;
    }

    public String getSurveystatus() {
        return surveystatus;
    }

    public void setSurveystatus(String surveystatus) {
        this.surveystatus = surveystatus;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public String getMedalert() {
        return medalert;
    }

    public void setMedalert(String medalert) {
        this.medalert = medalert;
    }

    public String getHighalert() {
        return highalert;
    }

    public void setHighalert(String highalert) {
        this.highalert = highalert;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getQues() {
        return ques;
    }

    public void setQues(String ques) {
        this.ques = ques;
    }

    public String getQuesid() {
        return quesid;
    }

    public void setQuesid(String quesid) {
        this.quesid = quesid;
    }

    public String getAnswertype() {
        return answertype;
    }

    public void setAnswertype(String answertype) {
        this.answertype = answertype;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public Long getAnswered() {
        return answered;
    }

    public void setAnswered(Long answered) {
        this.answered = answered;
    }

    public List<Answerscale> getAnswerscale() {
        return answerscale;
    }

    public void setAnswerscale(List<Answerscale> answerscale) {
        this.answerscale = answerscale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Surveydatum that = (Surveydatum) o;
        return Objects.equals(surveystatus, that.surveystatus) &&
                Objects.equals(schedule, that.schedule) &&
                Objects.equals(medalert, that.medalert) &&
                Objects.equals(highalert, that.highalert) &&
                Objects.equals(category, that.category) &&
                Objects.equals(ques, that.ques) &&
                Objects.equals(quesid, that.quesid) &&
                Objects.equals(answertype, that.answertype) &&
                Objects.equals(today, that.today) &&
                Objects.equals(answered, that.answered) &&
                Objects.equals(answerscale, that.answerscale);
    }

    @Override
    public int hashCode() {
        return Objects.hash(surveystatus, schedule, medalert, highalert, category, ques, quesid, answertype, today, answered, answerscale);
    }

    @Override
    public String toString() {
        return "Surveydatum{" +
                "surveystatus='" + surveystatus + '\'' +
                ", schedule=" + schedule +
                ", medalert='" + medalert + '\'' +
                ", highalert='" + highalert + '\'' +
                ", category='" + category + '\'' +
                ", ques='" + ques + '\'' +
                ", quesid='" + quesid + '\'' +
                ", answertype='" + answertype + '\'' +
                ", today='" + today + '\'' +
                ", answered=" + answered +
                ", answerscale=" + answerscale +
                '}';
    }
}

package com.hrs.cc.api.models.requests.edit_clinician;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class CliniciansEdit {

    @JsonProperty("data")
    private List<ClinicianIfo> data = null;

    public CliniciansEdit() {
    }

    public CliniciansEdit(List<ClinicianIfo> data) {
        this.data = data;
    }

    public List<ClinicianIfo> getData() {
        return data;
    }

    public void setData(List<ClinicianIfo> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClinicianEdit{" +
                "data=" + data +
                '}';
    }
}

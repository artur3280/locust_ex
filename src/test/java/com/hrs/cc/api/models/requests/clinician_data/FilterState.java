package com.hrs.cc.api.models.requests.clinician_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "columns",
        "scrollFocus",
        "selection",
        "grouping",
        "treeView",
        "pagination"
})
public class FilterState {

    @JsonProperty("columns")
    private List<Column> columns = null;
    @JsonProperty("scrollFocus")
    private ScrollFocus scrollFocus;
    @JsonProperty("selection")
    private List<Object> selection = null;
    @JsonProperty("grouping")
    private Grouping grouping;
    @JsonProperty("treeView")
    private TreeView treeView;
    @JsonProperty("pagination")
    private Pagination pagination;

    public FilterState() {
    }

    public FilterState(List<Column> columns, ScrollFocus scrollFocus, List<Object> selection, Grouping grouping, TreeView treeView, Pagination pagination) {
        this.columns = columns;
        this.scrollFocus = scrollFocus;
        this.selection = selection;
        this.grouping = grouping;
        this.treeView = treeView;
        this.pagination = pagination;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public ScrollFocus getScrollFocus() {
        return scrollFocus;
    }

    public void setScrollFocus(ScrollFocus scrollFocus) {
        this.scrollFocus = scrollFocus;
    }

    public List<Object> getSelection() {
        return selection;
    }

    public void setSelection(List<Object> selection) {
        this.selection = selection;
    }

    public Grouping getGrouping() {
        return grouping;
    }

    public void setGrouping(Grouping grouping) {
        this.grouping = grouping;
    }

    public TreeView getTreeView() {
        return treeView;
    }

    public void setTreeView(TreeView treeView) {
        this.treeView = treeView;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    @Override
    public String toString() {
        return "FilterState{" +
                "columns=" + columns +
                ", scrollFocus=" + scrollFocus +
                ", selection=" + selection +
                ", grouping=" + grouping +
                ", treeView=" + treeView +
                ", pagination=" + pagination +
                '}';
    }
}

package com.hrs.cc.api.models.requests.clinician_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "options",
        "term"
})
public class Filter {

    @JsonProperty("options")
    private List<Option> options = null;
    @JsonProperty("term")
    private List<Integer> term = null;

    public Filter(List<Option> options, List<Integer> term) {
        this.options = options;
        this.term = term;
    }

    public Filter() {
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public List<Integer> getTerm() {
        return term;
    }

    public void setTerm(List<Integer> term) {
        this.term = term;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "options=" + options +
                ", term=" + term +
                '}';
    }
}

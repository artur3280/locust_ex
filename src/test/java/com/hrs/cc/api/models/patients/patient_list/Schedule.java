package com.hrs.cc.api.models.patients.patient_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "schedule",
        "startday",
        "x"
})
public class Schedule {
    @JsonProperty("type")
    private String type;
    @JsonProperty("schedule")
    private String schedule;
    @JsonProperty("startday")
    private String startday;
    @JsonProperty("x")
    private String x;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Schedule() {
    }

    public Schedule(String type, String schedule, String startday, String x) {
        this.type = type;
        this.schedule = schedule;
        this.startday = startday;
        this.x = x;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getStartday() {
        return startday;
    }

    public void setStartday(String startday) {
        this.startday = startday;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule1 = (Schedule) o;
        return Objects.equals(type, schedule1.type) &&
                Objects.equals(schedule, schedule1.schedule) &&
                Objects.equals(startday, schedule1.startday) &&
                Objects.equals(x, schedule1.x) &&
                Objects.equals(additionalProperties, schedule1.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, schedule, startday, x, additionalProperties);
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "type='" + type + '\'' +
                ", schedule='" + schedule + '\'' +
                ", startday='" + startday + '\'' +
                ", x='" + x + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.patients.patient_list;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "glucose",
        "activity",
        "woundimaging",
        "temperature",
        "weight",
        "survey",
        "bloodpressure",
        "pulseox",
        "medication",
        "patientconnectvoice",
        "stethoscope",
        "steps"
})
public class Metrics<T> {

    @JsonProperty("glucose")
    private T glucose;
    @JsonProperty("activity")
    private T activity;
    @JsonProperty("woundimaging")
    private T woundimaging;
    @JsonProperty("temperature")
    private T temperature;
    @JsonProperty("weight")
    private T weight;
    @JsonProperty("survey")
    private Survey survey;
    @JsonProperty("medication")
    private T medications;
    @JsonProperty("bloodpressure")
    private T bloodpressure;
    @JsonProperty("pulseox")
    private T pulseox;
    @JsonProperty("patientconnectvoice")
    private T patientconnectvoice;
    @JsonProperty("stethoscope")
    private Stethoscope stethoscope = null;
    @JsonProperty("steps")
    private List<Object> steps = null;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private ObjectMapper mapper = new ObjectMapper();

    public Metrics() {
    }

    public Metrics(T glucose, T activity, T woundimaging, T temperature, T weight, Survey survey, T medications, T bloodpressure, T pulseox, T patientconnectvoice, Stethoscope stethoscope, List<Object> steps) {
        this.glucose = glucose;
        this.activity = activity;
        this.woundimaging = woundimaging;
        this.temperature = temperature;
        this.weight = weight;
        this.survey = survey;
        this.medications = medications;
        this.bloodpressure = bloodpressure;
        this.pulseox = pulseox;
        this.patientconnectvoice = patientconnectvoice;
        this.stethoscope = stethoscope;
        this.steps = steps;
    }

    public T getGlucose() {
        return glucose;
    }

    public void setGlucose(T glucose) {
        this.glucose = glucose;
    }

    public T getActivity() {
        return activity;
    }

    public void setActivity(T activity) {
        this.activity = activity;
    }

    public T getWoundimaging() {
        return woundimaging;
    }

    public void setWoundimaging(T woundimaging) {
        this.woundimaging = woundimaging;
    }

    public T getTemperature() {
        return temperature;
    }

    public void setTemperature(T temperature) {
        this.temperature = temperature;
    }

    public T getWeight() {
        return weight;
    }

    public void setWeight(T weight) {
        this.weight = weight;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public T getMedications() {
        return medications;
    }

    public void setMedications(T medications) {
        this.medications = medications;
    }

    public T getBloodpressure() {
        return bloodpressure;
    }

    public void setBloodpressure(T bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    public T getPulseox() {
        return pulseox;
    }

    public void setPulseox(T pulseox) {
        this.pulseox = pulseox;
    }

    public T getPatientconnectvoice() {
        return patientconnectvoice;
    }

    public void setPatientconnectvoice(T patientconnectvoice) {
        this.patientconnectvoice = patientconnectvoice;
    }

    public Stethoscope getStethoscope() {
        return stethoscope;
    }

    public void setStethoscope(Stethoscope stethoscope) {
        this.stethoscope = stethoscope;
    }

    public List<Object> getSteps() {
        return steps;
    }

    public void setSteps(List<Object> steps) {
        this.steps = steps;
    }

    public Activity getActivityObject() {
        Activity activity = null;
        if (!(getActivity() instanceof ArrayList)) {
            activity = mapper.convertValue(getActivity(), Activity.class);
        }
        return activity;
    }

    @Override
    public String toString() {
        return "Metrics{" +
                "glucose=" + glucose +
                ", activity=" + activity +
                ", woundimaging=" + woundimaging +
                ", temperature=" + temperature +
                ", weight=" + weight +
                ", survey=" + survey +
                ", medications=" + medications +
                ", bloodpressure=" + bloodpressure +
                ", pulseox=" + pulseox +
                ", patientconnectvoice=" + patientconnectvoice +
                ", stethoscope=" + stethoscope +
                ", steps=" + steps +
                '}';
    }
}

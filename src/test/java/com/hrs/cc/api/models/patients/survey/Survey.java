package com.hrs.cc.api.models.patients.survey;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "groups",
        "questions"
})
public class Survey {

    @JsonProperty("groups")
    private List<Group> groups = null;
    @JsonProperty("questions")
    private List<Question> questions = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Survey() {
    }

    public Survey(List<Group> groups, List<Question> questions) {
        this.groups = groups;
        this.questions = questions;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Survey survey = (Survey) o;
        return Objects.equals(groups, survey.groups) &&
                Objects.equals(questions, survey.questions) &&
                Objects.equals(additionalProperties, survey.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(groups, questions, additionalProperties);
    }

    @Override
    public String toString() {
        return "Survey{" +
                "groups=" + groups +
                ", questions=" + questions +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

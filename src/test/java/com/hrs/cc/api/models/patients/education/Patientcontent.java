package com.hrs.cc.api.models.patients.education;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonPropertyOrder({
        "id",
        "type",
        "category",
        "title",
        "status",
        "duration",
        "description"
})
public class Patientcontent {

    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("category")
    private String category;
    @JsonProperty("title")
    private String title;
    @JsonProperty("status")
    private String status;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("description")
    private String description;

    public Patientcontent() {
    }

    public Patientcontent(String id, String type, String category, String title, String status) {
        this.id = id;
        this.type = type;
        this.category = category;
        this.title = title;
        this.status = status;
    }

    public Patientcontent(String id, String type, String category, String title, String status, String duration, String description) {
        this.id = id;
        this.type = type;
        this.category = category;
        this.title = title;
        this.status = status;
        this.duration = duration;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patientcontent that = (Patientcontent) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(type, that.type) &&
                Objects.equals(category, that.category) &&
                Objects.equals(title, that.title) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, category, title, status);
    }

    @Override
    public String toString() {
        return "Patientcontent{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", category='" + category + '\'' +
                ", title='" + title + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.environment.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "type",
        "required",
        "patientsetup",
        "class",
        "emr_tracked",
        "last_updated"
})
public class PatientInfoCustomAttributes {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("type")
    private String type;
    @JsonProperty("required")
    private Boolean required;
    @JsonProperty("patientsetup")
    private Boolean patientsetup;
    @JsonProperty("class")
    private String _class;
    @JsonProperty("emr_tracked")
    private Boolean emrTracked;
    @JsonProperty("last_updated")
    private String lastUpdated;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public PatientInfoCustomAttributes() {
    }

    public PatientInfoCustomAttributes(Integer id, String name, String type, Boolean required, Boolean patientsetup, String _class, Boolean emrTracked, String lastUpdated) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.required = required;
        this.patientsetup = patientsetup;
        this._class = _class;
        this.emrTracked = emrTracked;
        this.lastUpdated = lastUpdated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Boolean getPatientsetup() {
        return patientsetup;
    }

    public void setPatientsetup(Boolean patientsetup) {
        this.patientsetup = patientsetup;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public Boolean getEmrTracked() {
        return emrTracked;
    }

    public void setEmrTracked(Boolean emrTracked) {
        this.emrTracked = emrTracked;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientInfoCustomAttributes that = (PatientInfoCustomAttributes) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(type, that.type) &&
                Objects.equals(required, that.required) &&
                Objects.equals(patientsetup, that.patientsetup) &&
                Objects.equals(_class, that._class) &&
                Objects.equals(emrTracked, that.emrTracked) &&
                Objects.equals(lastUpdated, that.lastUpdated) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, type, required, patientsetup, _class, emrTracked, lastUpdated, additionalProperties);
    }

    @Override
    public String toString() {
        return "PatientInfoCustomAttributes{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", required=" + required +
                ", patientsetup=" + patientsetup +
                ", _class='" + _class + '\'' +
                ", emrTracked=" + emrTracked +
                ", lastUpdated='" + lastUpdated + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

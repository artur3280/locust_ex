package com.hrs.cc.api.models.requests.set_patient_metricks.glucose;

import java.util.Objects;

public class SetGlucose {

    private String type = "glucose";
    private String reason;
    private String ftime;
    private Integer reading;
    private Integer glucose;

    public SetGlucose() {
    }

    public SetGlucose(String type, String reason, String ftime, Integer reading, Integer glucose) {
        this.type = type;
        this.reason = reason;
        this.ftime = ftime;
        this.reading = reading;
        this.glucose = glucose;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFtime() {
        return ftime;
    }

    public void setFtime(String ftime) {
        this.ftime = ftime;
    }

    public Integer getReading() {
        return reading;
    }

    public void setReading(Integer reading) {
        this.reading = reading;
    }

    public Integer getGlucose() {
        return glucose;
    }

    public void setGlucose(Integer glucose) {
        this.glucose = glucose;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SetGlucose that = (SetGlucose) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(reason, that.reason) &&
                Objects.equals(ftime, that.ftime) &&
                Objects.equals(reading, that.reading) &&
                Objects.equals(glucose, that.glucose);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, reason, ftime, reading, glucose);
    }

    @Override
    public String toString() {
        return "SetGlucose{" +
                "type='" + type + '\'' +
                ", reason='" + reason + '\'' +
                ", ftime='" + ftime + '\'' +
                ", reading=" + reading +
                ", glucose=" + glucose +
                '}';
    }
}

package com.hrs.cc.api.models.integratioins_app.patient_educations_files;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "conditions",
        "description",
        "type",
        "url",
        "duration"
})
public class Attributes {

    @JsonProperty("name")
    private String name;
    @JsonProperty("conditions")
    private List<String> conditions = null;
    @JsonProperty("description")
    private Object description;
    @JsonProperty("type")
    private String type;
    @JsonProperty("url")
    private String url;
    @JsonProperty("duration")
    private Object duration;

    public Attributes() {
    }

    public Attributes(String name, List<String> conditions, Object description, String type, String url, Object duration) {
        this.name = name;
        this.conditions = conditions;
        this.description = description;
        this.type = type;
        this.url = url;
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Object getDuration() {
        return duration;
    }

    public void setDuration(Object duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "name='" + name + '\'' +
                ", conditions=" + conditions +
                ", description=" + description +
                ", type='" + type + '\'' +
                ", url='" + url + '\'' +
                ", duration=" + duration +
                '}';
    }
}

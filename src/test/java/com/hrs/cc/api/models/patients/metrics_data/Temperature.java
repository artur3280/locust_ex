package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "unit",
        "data",
        "temperature",
        "deleted",
        "type",
        "ts"
})
public class Temperature {

    @JsonProperty("unit")
    private String unit;
    @JsonProperty("data")
    private List<Datum___> data = null;
    @JsonProperty("temperature")
    private Integer temperature;
    @JsonProperty("type")
    private String type;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("ts")
    private Long ts;

    public Temperature() {
    }

    public Temperature(String unit, List<Datum___> data, Integer temperature, String type, Boolean deleted, Long ts) {
        this.unit = unit;
        this.data = data;
        this.temperature = temperature;
        this.type = type;
        this.deleted = deleted;
        this.ts = ts;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public List<Datum___> getData() {
        return data;
    }

    public void setData(List<Datum___> data) {
        this.data = data;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Temperature{" +
                "unit='" + unit + '\'' +
                ", data=" + data +
                ", temperature=" + temperature +
                ", type='" + type + '\'' +
                ", deleted=" + deleted +
                ", ts=" + ts +
                '}';
    }
}

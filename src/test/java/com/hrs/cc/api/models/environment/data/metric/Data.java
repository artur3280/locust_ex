package com.hrs.cc.api.models.environment.data.metric;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.hrs.cc.api.models.environment.data.metric.data.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "duration",
        "systolic",
        "heartrate",
        "diastolic",
        "glucose",
        "dose",
        "med",
        "spo2",
        "question",
        "answer",
        "temperature",
        "weight",
        "images",
        "files",
        "count"
})
public class Data {
    @JsonProperty("duration")
    private Duration duration;
    @JsonProperty("systolic")
    private Systolic systolic;
    @JsonProperty("heartrate")
    private Heartrate heartrate;
    @JsonProperty("diastolic")
    private Diastolic diastolic;
    @JsonProperty("glucose")
    private Glucose glucose;
    @JsonProperty("dose")
    private Dose dose;
    @JsonProperty("med")
    private Med med;
    @JsonProperty("spo2")
    private Spo2 spo2;
    @JsonProperty("question")
    private Question question;
    @JsonProperty("answer")
    private Answer answer;
    @JsonProperty("temperature")
    private Temperature temperature;
    @JsonProperty("weight")
    private Weight weight;
    @JsonProperty("images")
    private Images images;
    @JsonProperty("files")
    private Files files;
    @JsonProperty("count")
    private Count count;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Data() {
    }

    public Data(Duration duration, Systolic systolic, Heartrate heartrate, Diastolic diastolic, Glucose glucose, Dose dose, Med med, Spo2 spo2, Question question, Answer answer, Temperature temperature, Weight weight, Images images, Files files, Count count) {
        this.duration = duration;
        this.systolic = systolic;
        this.heartrate = heartrate;
        this.diastolic = diastolic;
        this.glucose = glucose;
        this.dose = dose;
        this.med = med;
        this.spo2 = spo2;
        this.question = question;
        this.answer = answer;
        this.temperature = temperature;
        this.weight = weight;
        this.images = images;
        this.files = files;
        this.count = count;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Systolic getSystolic() {
        return systolic;
    }

    public void setSystolic(Systolic systolic) {
        this.systolic = systolic;
    }

    public Heartrate getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(Heartrate heartrate) {
        this.heartrate = heartrate;
    }

    public Diastolic getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(Diastolic diastolic) {
        this.diastolic = diastolic;
    }

    public Glucose getGlucose() {
        return glucose;
    }

    public void setGlucose(Glucose glucose) {
        this.glucose = glucose;
    }

    public Dose getDose() {
        return dose;
    }

    public void setDose(Dose dose) {
        this.dose = dose;
    }

    public Med getMed() {
        return med;
    }

    public void setMed(Med med) {
        this.med = med;
    }

    public Spo2 getSpo2() {
        return spo2;
    }

    public void setSpo2(Spo2 spo2) {
        this.spo2 = spo2;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public Count getCount() {
        return count;
    }

    public void setCount(Count count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Data{" +
                "duration=" + duration +
                ", systolic=" + systolic +
                ", heartrate=" + heartrate +
                ", diastolic=" + diastolic +
                ", glucose=" + glucose +
                ", dose=" + dose +
                ", med=" + med +
                ", spo2=" + spo2 +
                ", question=" + question +
                ", answer=" + answer +
                ", temperature=" + temperature +
                ", weight=" + weight +
                ", images=" + images +
                ", files=" + files +
                ", count=" + count +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

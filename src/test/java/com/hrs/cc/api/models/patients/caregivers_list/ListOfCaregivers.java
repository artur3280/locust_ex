package com.hrs.cc.api.models.patients.caregivers_list;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

public class ListOfCaregivers {

    private List<Datum> data = null;
    private Sideload sideload;

    public ListOfCaregivers() {
    }

    public ListOfCaregivers(List<Datum> data, Sideload sideload) {
        this.data = data;
        this.sideload = sideload;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Sideload getSideload() {
        return sideload;
    }

    public void setSideload(Sideload sideload) {
        this.sideload = sideload;
    }

    @Override
    public String toString() {
        return "ListOfCaregivers{" +
                "data=" + data +
                ", sideload=" + sideload +
                '}';
    }
}

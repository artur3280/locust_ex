package com.hrs.cc.api.models.patients.dashboard_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "survey",
        "ts"
})
public class Survey {

    @JsonProperty("active")
    private String active;
    @JsonProperty("survey")
    private String survey;
    @JsonProperty("ts")
    private String ts;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Survey() {
    }

    public Survey(String active, String survey, String ts) {
        this.active = active;
        this.survey = survey;
        this.ts = ts;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getSurvey() {
        return survey;
    }

    public void setSurvey(String survey) {
        this.survey = survey;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Survey survey1 = (Survey) o;
        return Objects.equals(active, survey1.active) &&
                Objects.equals(survey, survey1.survey) &&
                Objects.equals(ts, survey1.ts) &&
                Objects.equals(additionalProperties, survey1.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(active, survey, ts, additionalProperties);
    }

    @Override
    public String toString() {
        return "Survey{" +
                "active='" + active + '\'' +
                ", survey='" + survey + '\'' +
                ", ts='" + ts + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

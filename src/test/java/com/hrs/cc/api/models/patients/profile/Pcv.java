package com.hrs.cc.api.models.patients.profile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "phone",
        "active"
})
public class Pcv {

    @JsonProperty("phone")
    private String phone;
    @JsonProperty("active")
    private Boolean active;

    public Pcv() {
    }

    public Pcv(String phone, Boolean active) {
        this.phone = phone;
        this.active = active;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pcv pcv = (Pcv) o;
        return Objects.equals(phone, pcv.phone) &&
                Objects.equals(active, pcv.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phone, active);
    }

    @Override
    public String toString() {
        return "Pcv{" +
                "phone='" + phone + '\'' +
                ", active=" + active +
                '}';
    }
}

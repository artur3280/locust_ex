package com.hrs.cc.api.models.integratioins_app.stethoscope_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "takenAt",
        "soundUrl",
        "imageUrl"
})
public class Datum {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("takenAt")
    private String takenAt;
    @JsonProperty("soundUrl")
    private String soundUrl;
    @JsonProperty("imageUrl")
    private String imageUrl;

    public Datum() {
    }

    public Datum(Integer id, String takenAt, String soundUrl, String imageUrl) {
        this.id = id;
        this.takenAt = takenAt;
        this.soundUrl = soundUrl;
        this.imageUrl = imageUrl;
    }

    public String getTakenAt() {
        return takenAt;
    }

    public void setTakenAt(String takenAt) {
        this.takenAt = takenAt;
    }

    public String getSoundUrl() {
        return soundUrl;
    }

    public void setSoundUrl(String soundUrl) {
        this.soundUrl = soundUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "id=" + id +
                ", takenAt='" + takenAt + '\'' +
                ", soundUrl='" + soundUrl + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}

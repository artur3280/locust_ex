package com.hrs.cc.api.models.integratioins_app.files_education_content;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "conditions",
        "description",
        "resourceType",
        "type",
        "url",
        "duration"
})
public class FileObject {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("conditions")
    private List<String> conditions = null;
    @JsonProperty("description")
    private Object description;
    @JsonProperty("type")
    private String type;
    @JsonProperty("url")
    private String url;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("resourceType")
    private String resourceType;

    public FileObject() {
    }

    public FileObject(String id, String name, List<String> conditions, Object description, String type, String url, String duration, String resourceType) {
        this.id = id;
        this.name = name;
        this.conditions = conditions;
        this.description = description;
        this.type = type;
        this.url = url;
        this.duration = duration;
        this.resourceType = resourceType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public String toString() {
        return "FileObject{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", conditions=" + conditions +
                ", description=" + description +
                ", type='" + type + '\'' +
                ", url='" + url + '\'' +
                ", duration='" + duration + '\'' +
                ", resourceType='" + resourceType + '\'' +
                '}';
    }
}

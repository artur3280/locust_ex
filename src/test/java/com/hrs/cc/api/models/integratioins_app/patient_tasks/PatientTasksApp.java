package com.hrs.cc.api.models.integratioins_app.patient_tasks;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class PatientTasksApp {

    @JsonProperty("data")
    private List<DatumTask> data = null;

    public PatientTasksApp() {
    }

    public PatientTasksApp(List<DatumTask> data) {
        this.data = data;
    }

    public List<DatumTask> getData() {
        return data;
    }

    public void setData(List<DatumTask> data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientTasksApp that = (PatientTasksApp) o;
        return Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "PatientTasksApp{" +
                "data=" + data +
                '}';
    }
}

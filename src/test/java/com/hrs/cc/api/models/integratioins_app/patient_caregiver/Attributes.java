package com.hrs.cc.api.models.integratioins_app.patient_caregiver;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "patient",
        "caregiver",
        "environment",
        "created",
})
public class Attributes {
    @JsonProperty("patient")
    private String patient;
    @JsonProperty("caregiver")
    private String caregiver;
    @JsonProperty("environment")
    private String environment;
    @JsonProperty("created")
    private String created;

    public Attributes() {
    }

    public Attributes(String patient, String caregiver, String environment, String created) {
        this.patient = patient;
        this.caregiver = caregiver;
        this.environment = environment;
        this.created = created;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(String caregiver) {
        this.caregiver = caregiver;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "patient='" + patient + '\'' +
                ", caregiver='" + caregiver + '\'' +
                ", environment='" + environment + '\'' +
                ", created='" + created + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.patients.module_info;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "auth",
        "data"
})
public class PatientModulesInfo {

    @JsonProperty("status")
    private String status;
    @JsonProperty("auth")
    private Auth auth;
    @JsonProperty("data")
    private Data data;

    public PatientModulesInfo() {
    }

    public PatientModulesInfo(String status, Auth auth, Data data) {
        this.status = status;
        this.auth = auth;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PatientModulesInfo{" +
                "status='" + status + '\'' +
                ", auth=" + auth +
                ", data=" + data +
                '}';
    }
}

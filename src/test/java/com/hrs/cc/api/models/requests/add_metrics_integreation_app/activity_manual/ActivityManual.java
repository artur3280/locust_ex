package com.hrs.cc.api.models.requests.add_metrics_integreation_app.activity_manual;

import com.hrs.cc.api.models.requests.add_metrics_integreation_app.medication_manual.DataMedication;

public class ActivityManual {
    private DataActivity data;

    /**
     * No args constructor for use in serialization
     *
     */
    public ActivityManual() {
    }

    /**
     *
     * @param data
     */
    public ActivityManual(DataActivity data) {
        super();
        this.data = data;
    }

    public DataActivity getData() {
        return data;
    }

    public void setData(DataActivity data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ActivityManual{" +
                "data=" + data +
                '}';
    }
}

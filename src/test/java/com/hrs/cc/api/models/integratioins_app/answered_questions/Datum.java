package com.hrs.cc.api.models.integratioins_app.answered_questions;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "hrsid",
        "ivrQuestionId",
        "ivrQuestionChoiceId",
        "questionText",
        "choiceText",
        "createdAt",
        "updatedAt"
})
public class Datum {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("hrsid")
    private String hrsid;
    @JsonProperty("ivrQuestionId")
    private Integer ivrQuestionId;
    @JsonProperty("ivrQuestionChoiceId")
    private Integer ivrQuestionChoiceId;
    @JsonProperty("questionText")
    private String questionText;
    @JsonProperty("choiceText")
    private String choiceText;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("updatedAt")
    private String updatedAt;

    public Datum() {
    }

    public Datum(Integer id, String hrsid, Integer ivrQuestionId, Integer ivrQuestionChoiceId, String questionText, String choiceText, String createdAt, String updatedAt) {
        this.id = id;
        this.hrsid = hrsid;
        this.ivrQuestionId = ivrQuestionId;
        this.ivrQuestionChoiceId = ivrQuestionChoiceId;
        this.questionText = questionText;
        this.choiceText = choiceText;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public Integer getIvrQuestionId() {
        return ivrQuestionId;
    }

    public void setIvrQuestionId(Integer ivrQuestionId) {
        this.ivrQuestionId = ivrQuestionId;
    }

    public Integer getIvrQuestionChoiceId() {
        return ivrQuestionChoiceId;
    }

    public void setIvrQuestionChoiceId(Integer ivrQuestionChoiceId) {
        this.ivrQuestionChoiceId = ivrQuestionChoiceId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getChoiceText() {
        return choiceText;
    }

    public void setChoiceText(String choiceText) {
        this.choiceText = choiceText;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "id=" + id +
                ", hrsid='" + hrsid + '\'' +
                ", ivrQuestionId=" + ivrQuestionId +
                ", ivrQuestionChoiceId=" + ivrQuestionChoiceId +
                ", questionText='" + questionText + '\'' +
                ", choiceText='" + choiceText + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.integratioins_app.patient_tasks;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "glucose",
        "id",
        "type",
        "ts",
        "status",
        "duration",
        "unit",
        "temperature",
        "weight",
        "systolic",
        "diastolic",
        "heartrate",
        "risk",
        "deleted",
        "edited",
        "spo2"
})
public class Datum_ {

    @JsonProperty("glucose")
    private Integer glucose;
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("status")
    private String status;
    @JsonProperty("duration")
    private Integer duration;
    @JsonProperty("unit")
    private String unit;
    @JsonProperty("temperature")
    private Integer temperature;
    @JsonProperty("weight")
    private Integer weight;
    @JsonProperty("systolic")
    private Integer systolic;
    @JsonProperty("diastolic")
    private Integer diastolic;
    @JsonProperty("heartrate")
    private Integer heartrate;
    @JsonProperty("spo2")
    private Integer spo2;
    @JsonProperty("risk")
    private String risk;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("edited")
    private Boolean edited;

    public Datum_() {
    }

    public Datum_(Integer glucose, String id, String type, Long ts, String status, Integer duration, String unit, Integer temperature, Integer weight, Integer systolic, Integer diastolic, Integer heartrate, Integer spo2, String risk, Boolean deleted, Boolean edited) {
        this.glucose = glucose;
        this.id = id;
        this.type = type;
        this.ts = ts;
        this.status = status;
        this.duration = duration;
        this.unit = unit;
        this.temperature = temperature;
        this.weight = weight;
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.heartrate = heartrate;
        this.spo2 = spo2;
        this.risk = risk;
        this.deleted = deleted;
        this.edited = edited;
    }

    public Integer getGlucose() {
        return glucose;
    }

    public void setGlucose(Integer glucose) {
        this.glucose = glucose;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getSystolic() {
        return systolic;
    }

    public void setSystolic(Integer systolic) {
        this.systolic = systolic;
    }

    public Integer getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(Integer diastolic) {
        this.diastolic = diastolic;
    }

    public Integer getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(Integer heartrate) {
        this.heartrate = heartrate;
    }

    public Integer getSpo2() {
        return spo2;
    }

    public void setSpo2(Integer spo2) {
        this.spo2 = spo2;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getEdited() {
        return edited;
    }

    public void setEdited(Boolean edited) {
        this.edited = edited;
    }

    @Override
    public String toString() {
        return "Datum_{" +
                "glucose=" + glucose +
                ", id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", ts=" + ts +
                ", status='" + status + '\'' +
                ", duration=" + duration +
                ", unit='" + unit + '\'' +
                ", temperature=" + temperature +
                ", weight=" + weight +
                ", systolic=" + systolic +
                ", diastolic=" + diastolic +
                ", heartrate=" + heartrate +
                ", spo2=" + spo2 +
                ", risk='" + risk + '\'' +
                ", deleted=" + deleted +
                ", edited=" + edited +
                '}';
    }
}

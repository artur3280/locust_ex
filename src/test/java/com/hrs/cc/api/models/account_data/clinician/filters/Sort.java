package com.hrs.cc.api.models.account_data.clinician.filters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "priority",
        "direction"
})
public class Sort {

    @JsonProperty("priority")
    private Integer priority;
    @JsonProperty("direction")
    private String direction;

    public Sort() {
    }

    public Sort(Integer priority, String direction) {
        this.priority = priority;
        this.direction = direction;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Sort{" +
                "priority=" + priority +
                ", direction='" + direction + '\'' +
                '}';
    }
}

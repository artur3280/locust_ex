package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "glucose",
        "risk",
        "id",
        "type",
        "ts",
        "status",
        "deleted"
})
public class Datum {

    @JsonProperty("glucose")
    private Integer glucose;
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("status")
    private String status;
    @JsonProperty("risk")
    private String risk;
    @JsonProperty("deleted")
    private Boolean deleted;

    public Datum() {
    }

    public Datum(Integer glucose, String id, String type, Long ts, String status, String risk, Boolean deleted) {
        this.glucose = glucose;
        this.id = id;
        this.type = type;
        this.ts = ts;
        this.status = status;
        this.risk = risk;
        this.deleted = deleted;
    }

    public Integer getGlucose() {
        return glucose;
    }

    public void setGlucose(Integer glucose) {
        this.glucose = glucose;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "glucose=" + glucose +
                ", id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", ts=" + ts +
                ", status='" + status + '\'' +
                ", risk='" + risk + '\'' +
                ", deleted='" + deleted + '\'' +
                '}';
    }
}

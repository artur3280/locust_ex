package com.hrs.cc.api.models.admin.surveys_types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "types"
})
public class AdminSurveysTypes {

    @JsonProperty("types")
    private List<Type> types = null;

    public AdminSurveysTypes() {
    }

    public AdminSurveysTypes(List<Type> types) {
        this.types = types;
    }

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return "AdminSurveysTypes{" +
                "types=" + types +
                '}';
    }
}

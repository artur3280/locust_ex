package com.hrs.cc.api.models.requests.set_patient_note;

import java.util.Objects;

public class Edvisit {

    private String dateObject;
    private String location;
    private Integer los;
    private Long date = 1524517200000L;


    public Edvisit() {
    }

    public Edvisit(String dateObject, String location, Integer los, Long date) {
        this.dateObject = dateObject;
        this.location = location;
        this.los = los;
        this.date = date;
    }

    public String getDateObject() {
        return dateObject;
    }

    public void setDateObject(String dateObject) {
        this.dateObject = dateObject;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getLos() {
        return los;
    }

    public void setLos(Integer los) {
        this.los = los;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edvisit edvisit = (Edvisit) o;
        return Objects.equals(dateObject, edvisit.dateObject) &&
                Objects.equals(location, edvisit.location) &&
                Objects.equals(los, edvisit.los) &&
                Objects.equals(date, edvisit.date);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dateObject, location, los, date);
    }

    @Override
    public String toString() {
        return "Edvisit{" +
                "dateObject='" + dateObject + '\'' +
                ", location='" + location + '\'' +
                ", los=" + los +
                ", date=" + date +
                '}';
    }
}

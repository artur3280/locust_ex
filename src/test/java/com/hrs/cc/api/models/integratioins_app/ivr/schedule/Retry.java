package com.hrs.cc.api.models.integratioins_app.ivr.schedule;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "measure",
        "units"
})
public class Retry {

    @JsonProperty("measure")
    private String measure;
    @JsonProperty("units")
    private String units;

    public Retry() {
    }

    public Retry(String measure, String units) {
        this.measure = measure;
        this.units = units;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Retry retry = (Retry) o;
        return Objects.equals(measure, retry.measure) &&
                Objects.equals(units, retry.units);
    }

    @Override
    public int hashCode() {
        return Objects.hash(measure, units);
    }

    @Override
    public String toString() {
        return "Retry{" +
                "measure='" + measure + '\'' +
                ", units='" + units + '\'' +
                '}';
    }
}

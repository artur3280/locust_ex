package com.hrs.cc.api.models.requests.add_metrics_integreation_app.pulseox_manual;


public class DataPulseOx {
    private String owner;
    private String entered;
    private String metric;
    private String remindAt = null;
    private String takenAt;

    private FacetPulseOx facet;

    public DataPulseOx() {
    }

    public DataPulseOx(String owner, String entered, String metric, String remindAt, String takenAt, FacetPulseOx facet) {
        this.owner = owner;
        this.entered = entered;
        this.metric = metric;
        this.remindAt = remindAt;
        this.takenAt = takenAt;
        this.facet = facet;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getEntered() {
        return entered;
    }

    public void setEntered(String entered) {
        this.entered = entered;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getRemindAt() {
        return remindAt;
    }

    public void setRemindAt(String remindAt) {
        this.remindAt = remindAt;
    }

    public String getTakenAt() {
        return takenAt;
    }

    public void setTakenAt(String takenAt) {
        this.takenAt = takenAt;
    }

    public FacetPulseOx getFacet() {
        return facet;
    }

    public void setFacet(FacetPulseOx facet) {
        this.facet = facet;
    }

    @Override
    public String toString() {
        return "DataPulseOx{" +
                "owner='" + owner + '\'' +
                ", entered='" + entered + '\'' +
                ", metric='" + metric + '\'' +
                ", remindAt='" + remindAt + '\'' +
                ", takenAt='" + takenAt + '\'' +
                ", facet=" + facet +
                '}';
    }
}

package com.hrs.cc.api.models.patients.patient_list;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "takenAt",
        "imageUrl",
        "soundUrl"
})
public class StethoscopeData {

    @JsonProperty("takenAt")
    private String takenAt;
    @JsonProperty("imageUrl")
    private String imageUrl;
    @JsonProperty("soundUrl")
    private String soundUrl;

    public StethoscopeData() {
    }

    public StethoscopeData(String takenAt, String imageUrl, String soundUrl) {
        this.takenAt = takenAt;
        this.imageUrl = imageUrl;
        this.soundUrl = soundUrl;
    }

    public String getTakenAt() {
        return takenAt;
    }

    public void setTakenAt(String takenAt) {
        this.takenAt = takenAt;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSoundUrl() {
        return soundUrl;
    }

    public void setSoundUrl(String soundUrl) {
        this.soundUrl = soundUrl;
    }

    @Override
    public String toString() {
        return "data{" +
                "takenAt='" + takenAt + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", soundUrl='" + soundUrl + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.requests.add_metrics_integreation_app.image_manual;

public class ImageManual {
    private DataImage data;

    /**
     * No args constructor for use in serialization
     */
    public ImageManual() {
    }

    /**
     * @param data
     */
    public ImageManual(DataImage data) {
        super();
        this.data = data;
    }

    public DataImage getData() {
        return data;
    }

    public void setData(DataImage data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ImageManual{" +
                "data=" + data +
                '}';
    }
}

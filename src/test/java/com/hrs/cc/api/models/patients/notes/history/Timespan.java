package com.hrs.cc.api.models.patients.notes.history;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "noteHistoryId",
        "timeSpent"
})
public class Timespan {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("noteHistoryId")
    private String noteHistoryId;
    @JsonProperty("timeSpent")
    private String timeSpent;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Timespan() {
    }

    public Timespan(Long id, String noteHistoryId, String timeSpent) {
        this.id = id;
        this.noteHistoryId = noteHistoryId;
        this.timeSpent = timeSpent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNoteHistoryId() {
        return noteHistoryId;
    }

    public void setNoteHistoryId(String noteHistoryId) {
        this.noteHistoryId = noteHistoryId;
    }

    public String getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(String timeSpent) {
        this.timeSpent = timeSpent;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Timespan timespan = (Timespan) o;
        return Objects.equals(id, timespan.id) &&
                Objects.equals(noteHistoryId, timespan.noteHistoryId) &&
                Objects.equals(timeSpent, timespan.timeSpent) &&
                Objects.equals(additionalProperties, timespan.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, noteHistoryId, timeSpent, additionalProperties);
    }

    @Override
    public String toString() {
        return "Timespan{" +
                "id=" + id +
                ", noteHistoryId='" + noteHistoryId + '\'' +
                ", timeSpent='" + timeSpent + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.requests.edit_clinician;

public class ClinicianIfo {

    private String firstName;
    private String middleName;
    private String lastName;
    private String password;
    private String email;
    private String phone;
    private Profile profile;
    private String hrsid;

    public ClinicianIfo() {
    }

    public ClinicianIfo(String firstName, String middleName, String lastName, String password, String email, String phone, Profile profile, String hrsid) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.profile = profile;
        this.hrsid = hrsid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", profile=" + profile +
                '}';
    }
}

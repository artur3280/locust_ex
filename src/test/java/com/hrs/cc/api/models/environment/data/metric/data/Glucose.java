package com.hrs.cc.api.models.environment.data.metric.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "units",
        "type"
})
public class Glucose {
    @JsonProperty("units")
    private String units;
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Glucose() {
    }

    public Glucose(String units, String type) {
        this.units = units;
        this.type = type;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Glucose glucose = (Glucose) o;
        return Objects.equals(units, glucose.units) &&
                Objects.equals(type, glucose.type) &&
                Objects.equals(additionalProperties, glucose.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(units, type, additionalProperties);
    }

    @Override
    public String toString() {
        return "Glucose{" +
                "units='" + units + '\'' +
                ", type='" + type + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

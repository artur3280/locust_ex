package com.hrs.cc.api.models.inventory.history;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "hrsid",
        "time"
})
public class TabletHistory {

    @JsonProperty("name")
    private String name;
    @JsonProperty("hrsid")
    private String hrsid;
    @JsonProperty("time")
    private Long time;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public TabletHistory() {
    }

    public TabletHistory(String name, String hrsid, Long time) {
        this.name = name;
        this.hrsid = hrsid;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TabletHistory that = (TabletHistory) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(hrsid, that.hrsid) &&
                Objects.equals(time, that.time) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, hrsid, time, additionalProperties);
    }

    @Override
    public String toString() {
        return "TabletHistory{" +
                "name='" + name + '\'' +
                ", hrsid='" + hrsid + '\'' +
                ", time=" + time +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

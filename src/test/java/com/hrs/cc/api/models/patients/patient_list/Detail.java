package com.hrs.cc.api.models.patients.patient_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "metricIds",
        "text",
        "metrics",
        "type"
})
public class Detail {

    @JsonProperty("metricIds")
    private List<String> metricIds = null;
    @JsonProperty("text")
    private String text;
    @JsonProperty("metrics")
    private List<String> metrics = null;
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Detail() {
    }

    public Detail(List<String> metricIds, String text, List<String> metrics, String type) {
        this.metricIds = metricIds;
        this.text = text;
        this.metrics = metrics;
        this.type = type;
    }

    public List<String> getMetricIds() {
        return metricIds;
    }

    public void setMetricIds(List<String> metricIds) {
        this.metricIds = metricIds;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<String> metrics) {
        this.metrics = metrics;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Detail detail = (Detail) o;
        return Objects.equals(metricIds, detail.metricIds) &&
                Objects.equals(text, detail.text) &&
                Objects.equals(metrics, detail.metrics) &&
                Objects.equals(type, detail.type) &&
                Objects.equals(additionalProperties, detail.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(metricIds, text, metrics, type, additionalProperties);
    }

    @Override
    public String toString() {
        return "Detail{" +
                "metricIds=" + metricIds +
                ", text='" + text + '\'' +
                ", metrics=" + metrics +
                ", type='" + type + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

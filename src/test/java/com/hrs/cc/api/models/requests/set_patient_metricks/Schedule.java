package com.hrs.cc.api.models.requests.set_patient_metricks;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Objects;

public class Schedule {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String essential;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String type;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String expiration;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String instruction;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String x;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String startday;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String schedule;

    public Schedule() {
    }

    public Schedule(String essential, String type, String expiration, String instruction) {
        this.essential = essential;
        this.type = type;
        this.expiration = expiration;
        this.instruction = instruction;
    }

    public Schedule(String essential, String type, String instruction, String x, String startday) {
        this.essential = essential;
        this.type = type;
        this.instruction = instruction;
        this.x = x;
        this.startday = startday;
    }

    public Schedule(String essential, String type, String instruction) {
        this.essential = essential;
        this.type = type;
        this.instruction = instruction;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getEssential() {
        return essential;
    }

    public void setEssential(String essential) {
        this.essential = essential;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getStartday() {
        return startday;
    }

    public void setStartday(String startday) {
        this.startday = startday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return Objects.equals(essential, schedule.essential) &&
                Objects.equals(type, schedule.type) &&
                Objects.equals(expiration, schedule.expiration) &&
                Objects.equals(instruction, schedule.instruction);
    }

    @Override
    public int hashCode() {

        return Objects.hash(essential, type, expiration, instruction);
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "essential='" + essential + '\'' +
                ", type='" + type + '\'' +
                ", expiration='" + expiration + '\'' +
                ", instruction='" + instruction + '\'' +
                '}';
    }
}

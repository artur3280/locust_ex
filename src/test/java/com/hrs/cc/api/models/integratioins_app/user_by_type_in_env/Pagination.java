package com.hrs.cc.api.models.integratioins_app.user_by_type_in_env;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "total"
})
public class Pagination {
    @JsonProperty("total")
    private Integer total;

    public Pagination() {
    }

    public Pagination(Integer total) {
        this.total = total;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Pagination{" +
                "total=" + total +
                '}';
    }
}

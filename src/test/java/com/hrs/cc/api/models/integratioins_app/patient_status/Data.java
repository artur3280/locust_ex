package com.hrs.cc.api.models.integratioins_app.patient_status;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "type",
        "environment",
        "resourceType",
        "profile"
})
public class Data {

    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("resourceType")
    private String resourceType;
    @JsonProperty("environment")
    private String environment;
    @JsonProperty("profile")
    private Profile profile;

    public Data() {
    }

    public Data(String id, String type, String resourceType, String environment, Profile profile) {
        this.id = id;
        this.type = type;
        this.resourceType = resourceType;
        this.environment = environment;
        this.profile = profile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public String toString() {
        return "Data{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", environment='" + environment + '\'' +
                ", profile=" + profile +
                '}';
    }
}

package com.hrs.cc.api.models.environment.settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "settings"
})
public class EnvSettings {

    @JsonProperty("settings")
    private List<Setting> settings = null;

    public EnvSettings() {
    }

    public EnvSettings(List<Setting> settings) {
        this.settings = settings;
    }

    public List<Setting> getSettings() {
        return settings;
    }

    public void setSettings(List<Setting> settings) {
        this.settings = settings;
    }

    @Override
    public String toString() {
        return "EnvSettings{" +
                "settings=" + settings +
                '}';
    }
}

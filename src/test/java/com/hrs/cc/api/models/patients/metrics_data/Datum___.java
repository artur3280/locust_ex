package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "unit",
        "temperature",
        "id",
        "type",
        "ts",
        "status",
        "deleted",
        "edited",
        "risk"
})

public class Datum___ {

    @JsonProperty("unit")
    private String unit;
    @JsonProperty("temperature")
    private Integer temperature;
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("status")
    private String status;
    @JsonProperty("risk")
    private String risk;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("edited")
    private Boolean edited;

    public Datum___() {
    }

    public Datum___(String unit, Integer temperature, String id, String type, Long ts, String status, String risk, Boolean deleted, Boolean edited) {
        this.unit = unit;
        this.temperature = temperature;
        this.id = id;
        this.type = type;
        this.ts = ts;
        this.status = status;
        this.risk = risk;
        this.deleted = deleted;
        this.edited = edited;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getEdited() {
        return edited;
    }

    public void setEdited(Boolean edited) {
        this.edited = edited;
    }

    @Override
    public String toString() {
        return "Datum___{" +
                "unit='" + unit + '\'' +
                ", temperature=" + temperature +
                ", id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", ts=" + ts +
                ", status='" + status + '\'' +
                ", risk='" + risk + '\'' +
                ", deleted=" + deleted +
                ", edited=" + edited +
                '}';
    }
}

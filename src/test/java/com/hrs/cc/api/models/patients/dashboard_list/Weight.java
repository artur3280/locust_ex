package com.hrs.cc.api.models.patients.dashboard_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "weight",
        "ts"
})
public class Weight {

    @JsonProperty("active")
    private String active;
    @JsonProperty("weight")
    private String weight;
    @JsonProperty("ts")
    private String ts;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Weight() {
    }

    public Weight(String active, String weight, String ts) {
        this.active = active;
        this.weight = weight;
        this.ts = ts;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Weight weight1 = (Weight) o;
        return Objects.equals(active, weight1.active) &&
                Objects.equals(weight, weight1.weight) &&
                Objects.equals(ts, weight1.ts) &&
                Objects.equals(additionalProperties, weight1.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(active, weight, ts, additionalProperties);
    }

    @Override
    public String toString() {
        return "Weight{" +
                "active='" + active + '\'' +
                ", weight='" + weight + '\'' +
                ", ts='" + ts + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

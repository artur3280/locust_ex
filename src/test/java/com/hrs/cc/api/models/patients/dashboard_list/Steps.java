package com.hrs.cc.api.models.patients.dashboard_list;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "count",
        "ts"

})
public class Steps {
    @JsonProperty("active")
    private String active;
    @JsonProperty("count")
    private String count;
    @JsonProperty("ts")
    private String ts;

    public Steps() {
    }

    public Steps(String active, String count, String ts) {
        this.active = active;
        this.count = count;
        this.ts = ts;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    @Override
    public String toString() {
        return "Steps{" +
                "active=" + active +
                ", count=" + count +
                ", ts=" + ts +
                '}';
    }
}

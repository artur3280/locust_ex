package com.hrs.cc.api.models.patients.survey;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "schedule",
        "medalert",
        "highalert",
        "category",
        "question",
        "id",
        "spanish",
        "answer",
        "type",
        "answered",
        "points",
        "answertype",
        "riskanswervalues",
        "risklevel"
})
public class Question {

    @JsonProperty("schedule")
    private Schedule schedule;
    @JsonProperty("medalert")
    private String medalert;
    @JsonProperty("highalert")
    private String highalert;
    @JsonProperty("category")
    private String category;
    @JsonProperty("question")
    private String question;
    @JsonProperty("id")
    private String id;
    @JsonProperty("answertype")
    private String answertype;
    @JsonProperty("spanish")
    private String spanish;
    @JsonProperty("type")
    private String type;
    @JsonProperty("answered")
    private String answered;
    @JsonProperty("points")
    private String points;
    @JsonProperty("riskanswervalues")
    private String riskanswervalues;
    @JsonProperty("risklevel")
    private String risklevel;
    @JsonProperty("answer")
    private ArrayList<String> answer;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Question() {
    }

    public Question(Schedule schedule, String medalert, String highalert, String category, String question, String id, String answertype, String spanish, String type, String answered, String points, String riskanswervalues, String risklevel, ArrayList<String> answer, Map<String, Object> additionalProperties) {
        this.schedule = schedule;
        this.medalert = medalert;
        this.highalert = highalert;
        this.category = category;
        this.question = question;
        this.id = id;
        this.answertype = answertype;
        this.spanish = spanish;
        this.type = type;
        this.answered = answered;
        this.points = points;
        this.riskanswervalues = riskanswervalues;
        this.risklevel = risklevel;
        this.answer = answer;
        this.additionalProperties = additionalProperties;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public String getMedalert() {
        return medalert;
    }

    public void setMedalert(String medalert) {
        this.medalert = medalert;
    }

    public String getHighalert() {
        return highalert;
    }

    public void setHighalert(String highalert) {
        this.highalert = highalert;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAnswertype() {
        return answertype;
    }

    public void setAnswertype(String answertype) {
        this.answertype = answertype;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getSpanish() {
        return spanish;
    }

    public void setSpanish(String spanish) {
        this.spanish = spanish;
    }

    public ArrayList<String> getAnswer() {
        return answer;
    }

    public void setAnswer(ArrayList<String> answer) {
        this.answer = answer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAnswered() {
        return answered;
    }

    public void setAnswered(String answered) {
        this.answered = answered;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getRiskanswervalues() {
        return riskanswervalues;
    }

    public void setRiskanswervalues(String riskanswervalues) {
        this.riskanswervalues = riskanswervalues;
    }

    public String getRisklevel() {
        return risklevel;
    }

    public void setRisklevel(String risklevel) {
        this.risklevel = risklevel;
    }

    @Override
    public String toString() {
        return "Question{" +
                "schedule=" + schedule +
                ", medalert='" + medalert + '\'' +
                ", highalert='" + highalert + '\'' +
                ", category='" + category + '\'' +
                ", question='" + question + '\'' +
                ", id='" + id + '\'' +
                ", answertype='" + answertype + '\'' +
                ", spanish='" + spanish + '\'' +
                ", type='" + type + '\'' +
                ", answered='" + answered + '\'' +
                ", points='" + points + '\'' +
                ", riskanswervalues='" + riskanswervalues + '\'' +
                ", risklevel='" + risklevel + '\'' +
                ", answer=" + answer +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

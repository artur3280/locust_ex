package com.hrs.cc.api.models.requests.add_metrics_integreation_app.blood_pressure_manual;

public class FacetBP {
    private Integer systolic;
    private Integer diastolic;
    private Integer heartrate;

    /**
     * No args constructor for use in serialization
     *
     */
    public FacetBP() {
    }

    /**
     *
     * @param systolic
     * @param diastolic
     * @param heartrate
     */
    public FacetBP(Integer systolic, Integer diastolic, Integer heartrate) {
        super();
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.heartrate = heartrate;
    }

    public Integer getSystolic() {
        return systolic;
    }

    public void setSystolic(Integer systolic) {
        this.systolic = systolic;
    }

    public Integer getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(Integer diastolic) {
        this.diastolic = diastolic;
    }

    public Integer getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(Integer heartrate) {
        this.heartrate = heartrate;
    }

    @Override
    public String toString() {
        return "FacetBP{" +
                "systolic=" + systolic +
                ", diastolic=" + diastolic +
                ", heartrate=" + heartrate +
                '}';
    }
}

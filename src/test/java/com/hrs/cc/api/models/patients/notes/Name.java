package com.hrs.cc.api.models.patients.notes;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "clinician",
        "first",
        "middle",
        "last"
})
public class Name {

    @JsonProperty("clinician")
    private String clinician;
    @JsonProperty("first")
    private String first;
    @JsonProperty("middle")
    private String middle;
    @JsonProperty("last")
    private String last;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Name() {
    }

    public Name(String clinician, String first, String middle, String last) {
        this.clinician = clinician;
        this.first = first;
        this.middle = middle;
        this.last = last;
    }

    public String getClinician() {
        return clinician;
    }

    public void setClinician(String clinician) {
        this.clinician = clinician;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getMiddle() {
        return middle;
    }

    public void setMiddle(String middle) {
        this.middle = middle;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Name name = (Name) o;
        return Objects.equals(clinician, name.clinician) &&
                Objects.equals(first, name.first) &&
                Objects.equals(middle, name.middle) &&
                Objects.equals(last, name.last) &&
                Objects.equals(additionalProperties, name.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(clinician, first, middle, last, additionalProperties);
    }

    @Override
    public String toString() {
        return "Name{" +
                "clinician='" + clinician + '\'' +
                ", first='" + first + '\'' +
                ", middle='" + middle + '\'' +
                ", last='" + last + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

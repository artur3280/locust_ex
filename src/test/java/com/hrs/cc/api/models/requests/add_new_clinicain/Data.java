package com.hrs.cc.api.models.requests.add_new_clinicain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "userName",
        "password",
        "profile",
        "firstName",
        "middleName",
        "lastName",
        "email",
        "phone",
        "environment",
        "type"
})
public class Data {

    @JsonProperty("userName")
    private String userName;
    @JsonProperty("password")
    private String password;
    @JsonProperty("profile")
    private Profile profile;
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("middleName")
    private String middleName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("email")
    private String email;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("environment")
    private String environment;
    @JsonProperty("type")
    private String type;

    public Data() {
    }

    public Data(String userName, String password, Profile profile, String firstName, String middleName, String lastName, String email, String phone, String environment, String type) {
        this.userName = userName;
        this.password = password;
        this.profile = profile;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.environment = environment;
        this.type = type;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return Objects.equals(userName, data.userName) &&
                Objects.equals(password, data.password) &&
                Objects.equals(profile, data.profile) &&
                Objects.equals(firstName, data.firstName) &&
                Objects.equals(middleName, data.middleName) &&
                Objects.equals(lastName, data.lastName) &&
                Objects.equals(email, data.email) &&
                Objects.equals(phone, data.phone) &&
                Objects.equals(environment, data.environment) &&
                Objects.equals(type, data.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, password, profile, firstName, middleName, lastName, email, phone, environment, type);
    }

    @Override
    public String toString() {
        return "Data{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", profile=" + profile +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", environment='" + environment + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.patients.dashboard_list.riskdetails.details;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "text",
        "metrics",
        "metricIds"
})
public class Detail {

    @JsonProperty("type")
    private String type;
    @JsonProperty("text")
    private String text;
    @JsonProperty("metrics")
    private List<String> metrics = null;
    @JsonProperty("metricIds")
    private List<String> metricIds = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Detail() {
    }

    public Detail(String type, String text, List<String> metrics, List<String> metricIds) {
        this.type = type;
        this.text = text;
        this.metrics = metrics;
        this.metricIds = metricIds;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<String> metrics) {
        this.metrics = metrics;
    }

    public List<String> getMetricIds() {
        return metricIds;
    }

    public void setMetricIds(List<String> metricIds) {
        this.metricIds = metricIds;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Detail detail = (Detail) o;
        return Objects.equals(type, detail.type) &&
                Objects.equals(text, detail.text) &&
                Objects.equals(metrics, detail.metrics) &&
                Objects.equals(metricIds, detail.metricIds) &&
                Objects.equals(additionalProperties, detail.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, text, metrics, metricIds, additionalProperties);
    }

    @Override
    public String toString() {
        return "Detail{" +
                "type='" + type + '\'' +
                ", text='" + text + '\'' +
                ", metrics=" + metrics +
                ", metricIds=" + metricIds +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

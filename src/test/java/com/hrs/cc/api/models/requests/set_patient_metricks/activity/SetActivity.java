package com.hrs.cc.api.models.requests.set_patient_metricks.activity;

import java.util.Objects;

public class SetActivity {

    private String type = "activity";
    private String reason;
    private String ftime;
    private String rtime;
    private Integer duration;

    public SetActivity() {
    }

    public SetActivity(String reason, String ftime, String rtime, Integer duration) {
        this.reason = reason;
        this.ftime = ftime;
        this.rtime = rtime;
        this.duration = duration;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFtime() {
        return ftime;
    }

    public void setFtime(String ftime) {
        this.ftime = ftime;
    }

    public String getRtime() {
        return rtime;
    }

    public void setRtime(String rtime) {
        this.rtime = rtime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SetActivity setWeight = (SetActivity) o;
        return Objects.equals(type, setWeight.type) &&
                Objects.equals(reason, setWeight.reason) &&
                Objects.equals(ftime, setWeight.ftime) &&
                Objects.equals(rtime, setWeight.rtime) &&
                Objects.equals(duration, setWeight.duration);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, reason, ftime, rtime, duration);
    }

    @Override
    public String
    toString() {
        return "SetWeight{" +
                "type='" + type + '\'' +
                ", reason='" + reason + '\'' +
                ", ftime='" + ftime + '\'' +
                ", rtime='" + rtime + '\'' +
                ", duration=" + duration +
                '}';
    }
}

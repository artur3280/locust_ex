package com.hrs.cc.api.models.requests.add_metrics_integreation_app.survey_manual;

public class FacetSurvey {
    private String question;
    private String answer;

    /**
     * No args constructor for use in serialization
     *
     */
    public FacetSurvey() {
    }

    public FacetSurvey(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "FacetSurvey{" +
                "question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                '}';
    }
}

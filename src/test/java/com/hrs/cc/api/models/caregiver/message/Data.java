package com.hrs.cc.api.models.caregiver.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "chatroomId",
        "hrsid",
        "message",
        "createdAt",
        "resourceType",
        "attributes"
})
public class Data {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("chatroomId")
    private Integer chatroomId;
    @JsonProperty("hrsid")
    private String hrsid;
    @JsonProperty("message")
    private String message;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("resourceType")
    private String resourceType;
    @JsonProperty("attributes")
    private Attributes attributes = null;

    public Data() {
    }

    public Data(Integer id, Integer chatroomId, String hrsid, String message, String createdAt, String resourceType, Attributes attributes) {
        this.id = id;
        this.chatroomId = chatroomId;
        this.hrsid = hrsid;
        this.message = message;
        this.createdAt = createdAt;
        this.resourceType = resourceType;
        this.attributes = attributes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChatroomId() {
        return chatroomId;
    }

    public void setChatroomId(Integer chatroomId) {
        this.chatroomId = chatroomId;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "Data{" +
                "id=" + id +
                ", chatroomId=" + chatroomId +
                ", hrsid='" + hrsid + '\'' +
                ", message='" + message + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", attributes=" + attributes +
                '}';
    }
}

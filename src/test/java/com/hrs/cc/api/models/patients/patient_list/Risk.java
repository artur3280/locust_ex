package com.hrs.cc.api.models.patients.patient_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "details",
        "value",
        "modules",
        "ts"
})
public class Risk {

    @JsonProperty("details")
    private List<Detail> details = null;
    @JsonProperty("value")
    private Integer value;
    @JsonProperty("modules")
    private List<String> modules = null;
    @JsonProperty("ts")
    private Long ts;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Risk() {
    }

    public Risk(List<Detail> details, Integer value, List<String> modules, Long ts) {
        this.details = details;
        this.value = value;
        this.modules = modules;
        this.ts = ts;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Risk risk = (Risk) o;
        return Objects.equals(details, risk.details) &&
                Objects.equals(value, risk.value) &&
                Objects.equals(modules, risk.modules) &&
                Objects.equals(ts, risk.ts) &&
                Objects.equals(additionalProperties, risk.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(details, value, modules, ts, additionalProperties);
    }

    @Override
    public String toString() {
        return "Risk{" +
                "details=" + details +
                ", value=" + value +
                ", modules=" + modules +
                ", ts=" + ts +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

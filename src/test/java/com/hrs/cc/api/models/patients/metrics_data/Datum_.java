package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "duration",
        "id",
        "type",
        "ts",
        "status",
        "deleted",
        "edited"
})
public class Datum_ {

    @JsonProperty("duration")
    private Integer duration;
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("status")
    private String status;
    @JsonProperty("edited")
    private Boolean edited;
    @JsonProperty("deleted")
    private Boolean deleted;

    public Datum_() {
    }

    public Datum_(Integer duration, String id, String type, Long ts, String status, Boolean edited, Boolean deleted) {
        this.duration = duration;
        this.id = id;
        this.type = type;
        this.ts = ts;
        this.status = status;
        this.edited = edited;
        this.deleted = deleted;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getEdited() {
        return edited;
    }

    public void setEdited(Boolean edited) {
        this.edited = edited;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Datum_{" +
                "duration=" + duration +
                ", id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", ts=" + ts +
                ", status='" + status + '\'' +
                ", edited=" + edited +
                ", deleted=" + deleted +
                '}';
    }
}

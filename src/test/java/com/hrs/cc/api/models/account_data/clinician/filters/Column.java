package com.hrs.cc.api.models.account_data.clinician.filters;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "visible",
        "width",
        "sort",
        "filters"
})

public class Column <T> {

    @JsonProperty("name")
    private String name;
    @JsonProperty("visible")
    private Boolean visible;
    @JsonProperty("width")
    private String width;
    @JsonProperty("sort")
    private T sort = null;
    @JsonProperty("filters")
    private List<T> filters = null;

    public Column(String name, Boolean visible, String width, T sort, List<T> filters) {
        this.name = name;
        this.visible = visible;
        this.width = width;
        this.sort = sort;
        this.filters = filters;
    }

    public Column() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public T getSort() {
        return sort;
    }

    public void setSort(T sort) {
        this.sort = sort;
    }

    public List<T> getFilters() {
        return filters;
    }

    public void setFilters(List<T> filters) {
        this.filters = filters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Column column = (Column) o;
        return Objects.equals(name, column.name) &&
                Objects.equals(visible, column.visible) &&
                Objects.equals(width, column.width) &&
                Objects.equals(sort, column.sort) &&
                Objects.equals(filters, column.filters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, visible, width, sort, filters);
    }

    @Override
    public String toString() {
        return "Column{" +
                "name='" + name + '\'' +
                ", visible=" + visible +
                ", width='" + width + '\'' +
                ", sort=" + sort +
                ", filters=" + filters +
                '}';
    }
}

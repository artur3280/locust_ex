package com.hrs.cc.api.models.patients;


import com.hrs.cc.api.models.patients.patient_list.PatientList;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "patientlist"
})
public class Patients {

    @JsonProperty("patientlist")
    private List<List<PatientList>> patientlist = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private ObjectMapper mapper = new ObjectMapper();

    public Patients(List<List<PatientList>> patientlist) {
        this.patientlist = patientlist;
    }

    public Patients() {
    }

    public List<List<PatientList>> getPatientlist() {
        return patientlist;
    }

    public PatientList getPatientlistAsObject() {
        PatientList patientList = null;
        if (!(getPatientlist() instanceof ArrayList)) {
            patientList = mapper.convertValue(getPatientlist(), PatientList.class);
        }
        return patientList;
    }

    public void setPatientlist(List<List<PatientList>> patientlist) {
        this.patientlist = patientlist;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patients patients = (Patients) o;
        return Objects.equals(patientlist, patients.patientlist) &&
                Objects.equals(additionalProperties, patients.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(patientlist, additionalProperties);
    }

    @Override
    public String toString() {
        return "Patients{" +
                "patientlist=" + patientlist +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

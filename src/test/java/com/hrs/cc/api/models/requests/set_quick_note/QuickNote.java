package com.hrs.cc.api.models.requests.set_quick_note;

import java.util.Objects;

public class QuickNote {
    private Note quicknote;

    public QuickNote() {
    }

    public QuickNote(Note quicknote) {
        this.quicknote = quicknote;
    }

    public Note getQuicknote() {
        return quicknote;
    }

    public void setQuicknote(Note quicknote) {
        this.quicknote = quicknote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuickNote quickNote = (QuickNote) o;
        return Objects.equals(quicknote, quickNote.quicknote);
    }

    @Override
    public int hashCode() {

        return Objects.hash(quicknote);
    }

    @Override
    public String toString() {
        return "QuickNote{" +
                "quicknote=" + quicknote +
                '}';
    }
}

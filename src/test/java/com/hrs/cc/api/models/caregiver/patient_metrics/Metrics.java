package com.hrs.cc.api.models.caregiver.patient_metrics;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "glucose",
        "activity",
        "woundimaging",
        "temperature",
        "weight",
        "survey",
        "bloodpressure",
        "pulseox",
        "medication",
        "patientconnectvoice",
        "stethoscope",
        "steps"
})
public class Metrics {

    @JsonProperty("glucose")
    private Glucose glucose;
    @JsonProperty("activity")
    private Activity activity;
    @JsonProperty("woundimaging")
    private Woundimaging woundimaging;
    @JsonProperty("temperature")
    private Temperature temperature;
    @JsonProperty("weight")
    private Weight weight;
    @JsonProperty("survey")
    private Survey survey;
    @JsonProperty("bloodpressure")
    private Bloodpressure bloodpressure;
    @JsonProperty("pulseox")
    private Pulseox pulseox;
    @JsonProperty("medication")
    private Medication medication;
    @JsonProperty("patientconnectvoice")
    private PatientConnectVoice patientConnectVoice;
    @JsonProperty("stethoscope")
    private Stethoscope stethoscope;
    @JsonProperty("steps")
    private Steps steps;

    public Metrics() {
    }

    public Metrics(Glucose glucose, Activity activity, Woundimaging woundimaging, Temperature temperature, Weight weight, Survey survey, Bloodpressure bloodpressure, Pulseox pulseox, Medication medication, PatientConnectVoice patientConnectVoice, Stethoscope stethoscope, Steps steps) {
        this.glucose = glucose;
        this.activity = activity;
        this.woundimaging = woundimaging;
        this.temperature = temperature;
        this.weight = weight;
        this.survey = survey;
        this.bloodpressure = bloodpressure;
        this.pulseox = pulseox;
        this.medication = medication;
        this.patientConnectVoice = patientConnectVoice;
        this.stethoscope = stethoscope;
        this.steps = steps;
    }

    public Glucose getGlucose() {
        return glucose;
    }

    public void setGlucose(Glucose glucose) {
        this.glucose = glucose;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Woundimaging getWoundimaging() {
        return woundimaging;
    }

    public void setWoundimaging(Woundimaging woundimaging) {
        this.woundimaging = woundimaging;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public Bloodpressure getBloodpressure() {
        return bloodpressure;
    }

    public void setBloodpressure(Bloodpressure bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    public Pulseox getPulseox() {
        return pulseox;
    }

    public void setPulseox(Pulseox pulseox) {
        this.pulseox = pulseox;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public PatientConnectVoice getPatientConnectVoice() {
        return patientConnectVoice;
    }

    public void setPatientConnectVoice(PatientConnectVoice patientConnectVoice) {
        this.patientConnectVoice = patientConnectVoice;
    }

    public Stethoscope getStethoscope() {
        return stethoscope;
    }

    public void setStethoscope(Stethoscope stethoscope) {
        this.stethoscope = stethoscope;
    }

    public Steps getSteps() {
        return steps;
    }

    public void setSteps(Steps steps) {
        this.steps = steps;
    }

    @Override
    public String toString() {
        return "Metrics{" +
                "glucose=" + glucose +
                ", activity=" + activity +
                ", woundimaging=" + woundimaging +
                ", temperature=" + temperature +
                ", weight=" + weight +
                ", survey=" + survey +
                ", bloodpressure=" + bloodpressure +
                ", pulseox=" + pulseox +
                ", medication=" + medication +
                ", patientConnectVoice=" + patientConnectVoice +
                ", stethoscope=" + stethoscope +
                ", steps=" + steps +
                '}';
    }
}

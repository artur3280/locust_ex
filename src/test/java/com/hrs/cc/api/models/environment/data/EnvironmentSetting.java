package com.hrs.cc.api.models.environment.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "flag",
        "value",
        "url"
})
public class EnvironmentSetting<T> {
    @JsonProperty("flag")
    private String flag;
    @JsonProperty("url")
    private String url;
    @JsonProperty("value")
    private T value;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public EnvironmentSetting() {
    }

    public EnvironmentSetting(String flag, String url, T value) {
        this.flag = flag;
        this.url = url;
        this.value = value;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnvironmentSetting<?> that = (EnvironmentSetting<?>) o;
        return Objects.equals(flag, that.flag) &&
                Objects.equals(url, that.url) &&
                Objects.equals(value, that.value) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(flag, url, value, additionalProperties);
    }

    @Override
    public String toString() {
        return "EnvironmentSetting{" +
                "flag='" + flag + '\'' +
                ", url='" + url + '\'' +
                ", value=" + value +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

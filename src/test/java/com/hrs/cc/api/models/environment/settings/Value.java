package com.hrs.cc.api.models.environment.settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "appid",
        "phone",
        "riskdata",
        "content",
        "name",
        "modules",
        "value",
        "pcVoiceEnv",
        "pcVoiceAllPatients",
        "title",
        "subgroups",
        "activity"
})
public class Value <T> {

    @JsonProperty("appid")
    private String appid;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("riskdata")
    private List<Riskdatum> riskdata = null;
    @JsonProperty("content")
    private List<Content> content = null;
    @JsonProperty("name")
    private String name;
    @JsonProperty("modules")
    private List<String> modules = null;
    @JsonProperty("value")
    private String value;
    @JsonProperty("pcVoiceEnv")
    private String pcVoiceEnv;
    @JsonProperty("pcVoiceAllPatients")
    private String pcVoiceAllPatients;
    @JsonProperty("activity")
    private Object activity;
    @JsonProperty("title")
    private Object title;
    @JsonProperty("subgroups")
    private Object subgroups;

    public Value() {
    }

    public Value(String appid, String phone, List<Riskdatum> riskdata, List<Content> content, String name, List<String> modules, String value, String pcVoiceEnv, String pcVoiceAllPatients) {
        this.appid = appid;
        this.phone = phone;
        this.riskdata = riskdata;
        this.content = content;
        this.name = name;
        this.modules = modules;
        this.value = value;
        this.pcVoiceEnv = pcVoiceEnv;
        this.pcVoiceAllPatients = pcVoiceAllPatients;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Riskdatum> getRiskdata() {
        return riskdata;
    }

    public void setRiskdata(List<Riskdatum> riskdata) {
        this.riskdata = riskdata;
    }

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPcVoiceEnv() {
        return pcVoiceEnv;
    }

    public void setPcVoiceEnv(String pcVoiceEnv) {
        this.pcVoiceEnv = pcVoiceEnv;
    }

    public String getPcVoiceAllPatients() {
        return pcVoiceAllPatients;
    }

    public void setPcVoiceAllPatients(String pcVoiceAllPatients) {
        this.pcVoiceAllPatients = pcVoiceAllPatients;
    }

    @Override
    public String toString() {
        return "Value{" +
                "appid='" + appid + '\'' +
                ", phone='" + phone + '\'' +
                ", riskdata=" + riskdata +
                ", content=" + content +
                ", name='" + name + '\'' +
                ", modules=" + modules +
                ", value='" + value + '\'' +
                ", pcVoiceEnv='" + pcVoiceEnv + '\'' +
                ", pcVoiceAllPatients='" + pcVoiceAllPatients + '\'' +
                '}';
    }
}

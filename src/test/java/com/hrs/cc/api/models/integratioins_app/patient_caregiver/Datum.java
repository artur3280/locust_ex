package com.hrs.cc.api.models.integratioins_app.patient_caregiver;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "resourceType",
        "patient",
        "caregiver",
        "environment",
        "created",
        "attributes"
})
public class Datum {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("resourceType")
    private String resourceType;
    @JsonProperty("patient")
    private String patient;
    @JsonProperty("caregiver")
    private String caregiver;
    @JsonProperty("environment")
    private String environment;
    @JsonProperty("created")
    private String created;
    @JsonProperty("attributes")
    private Attributes attributes = null;

    public Datum() {
    }

    public Datum(Integer id, String resourceType, String patient, String caregiver, String environment, String created, Attributes attributes) {
        this.id = id;
        this.resourceType = resourceType;
        this.patient = patient;
        this.caregiver = caregiver;
        this.environment = environment;
        this.created = created;
        this.attributes = attributes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(String caregiver) {
        this.caregiver = caregiver;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "id=" + id +
                ", resourceType='" + resourceType + '\'' +
                ", patient='" + patient + '\'' +
                ", caregiver='" + caregiver + '\'' +
                ", environment='" + environment + '\'' +
                ", created='" + created + '\'' +
                ", attributes=" + attributes +
                '}';
    }
}

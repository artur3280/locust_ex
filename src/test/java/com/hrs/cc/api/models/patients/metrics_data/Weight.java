package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "weight",
        "time",
        "window",
        "type",
        "deleted",
        "ts"
})
public class Weight {

    @JsonProperty("data")
    private List<Datum____> data = null;
    @JsonProperty("weight")
    private Integer weight;
    @JsonProperty("time")
    private Integer time;
    @JsonProperty("window")
    private Integer window;
    @JsonProperty("type")
    private String type;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("ts")
    private Long ts;

    public Weight() {
    }

    public Weight(List<Datum____> data, Integer weight, Integer time, Integer window, String type, Boolean deleted, Long ts) {
        this.data = data;
        this.weight = weight;
        this.time = time;
        this.window = window;
        this.type = type;
        this.deleted = deleted;
        this.ts = ts;
    }

    public List<Datum____> getData() {
        return data;
    }

    public void setData(List<Datum____> data) {
        this.data = data;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getWindow() {
        return window;
    }

    public void setWindow(Integer window) {
        this.window = window;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Weight{" +
                "data=" + data +
                ", weight=" + weight +
                ", time=" + time +
                ", window=" + window +
                ", type='" + type + '\'' +
                ", deleted=" + deleted +
                ", ts=" + ts +
                '}';
    }
}

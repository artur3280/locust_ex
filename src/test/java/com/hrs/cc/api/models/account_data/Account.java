package com.hrs.cc.api.models.account_data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "usertype",
        "user",
        "token",
        "env",
        "roles",
        "username",
        "firstname",
        "lastname",
        "exp",
        "time_zone",
        "firebase",
        "subgroup"
})
public class Account {

    @JsonProperty("usertype")
    private String usertype;
    @JsonProperty("user")
    private String user;
    @JsonProperty("token")
    private String token;
    @JsonProperty("env")
    private String env;
    @JsonProperty("roles")
    private List<String> roles = null;
    @JsonProperty("username")
    private String username;
    @JsonProperty("firstname")
    private String firstname;
    @JsonProperty("lastname")
    private String lastname;
    @JsonProperty("exp")
    private Integer exp;
    @JsonProperty("time_zone")
    private String timeZone;
    @JsonProperty("firebase")
    private Firebase firebase;
    @JsonProperty("subgroup")
    private String subgroup;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Account() {
    }

    public Account(String usertype, String user, String token, String env, List<String> roles, String username, String firstname, String lastname, Integer exp, String timeZone, Firebase firebase, String subgroup) {
        this.usertype = usertype;
        this.user = user;
        this.token = token;
        this.env = env;
        this.roles = roles;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.exp = exp;
        this.timeZone = timeZone;
        this.firebase = firebase;
        this.subgroup = subgroup;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Firebase getFirebase() {
        return firebase;
    }

    public void setFirebase(Firebase firebase) {
        this.firebase = firebase;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(String subgroup) {
        this.subgroup = subgroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(usertype, account.usertype) &&
                Objects.equals(user, account.user) &&
                Objects.equals(token, account.token) &&
                Objects.equals(env, account.env) &&
                Objects.equals(roles, account.roles) &&
                Objects.equals(username, account.username) &&
                Objects.equals(firstname, account.firstname) &&
                Objects.equals(lastname, account.lastname) &&
                Objects.equals(exp, account.exp) &&
                Objects.equals(timeZone, account.timeZone) &&
                Objects.equals(firebase, account.firebase) &&
                Objects.equals(subgroup, account.subgroup) &&
                Objects.equals(additionalProperties, account.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(usertype, user, token, env, roles, username, firstname, lastname, exp, timeZone, firebase, subgroup, additionalProperties);
    }

    @Override
    public String toString() {
        return "Account{" +
                "usertype='" + usertype + '\'' +
                ", user='" + user + '\'' +
                ", token='" + token + '\'' +
                ", env='" + env + '\'' +
                ", roles=" + roles +
                ", username='" + username + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", exp=" + exp +
                ", timeZone='" + timeZone + '\'' +
                ", firebase=" + firebase +
                ", subgroup='" + subgroup + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.caregiver.connections;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "patient",
        "caregiver",
        "environment",
        "created",
        "resourceType"
})
public class Datum {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("patient")
    private String patient;
    @JsonProperty("caregiver")
    private String caregiver;
    @JsonProperty("environment")
    private String environment;
    @JsonProperty("created")
    private String created;
    @JsonProperty("resourceType")
    private String resourceType;

    public Datum() {
    }

    public Datum(Integer id, String patient, String caregiver, String environment, String created, String resourceType) {
        this.id = id;
        this.patient = patient;
        this.caregiver = caregiver;
        this.environment = environment;
        this.created = created;
        this.resourceType = resourceType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(String caregiver) {
        this.caregiver = caregiver;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "id=" + id +
                ", patient='" + patient + '\'' +
                ", caregiver='" + caregiver + '\'' +
                ", environment='" + environment + '\'' +
                ", created='" + created + '\'' +
                ", resourceType='" + resourceType + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.environment.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "shortname",
        "modules"
})
public class Condition {
    @JsonProperty("name")
    private String name;
    @JsonProperty("shortname")
    private String shortname;
    @JsonProperty("modules")
    private List<String> modules = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Condition() {
    }

    public Condition(String name, String shortname, List<String> modules) {
        this.name = name;
        this.shortname = shortname;
        this.modules = modules;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Condition condition = (Condition) o;
        return Objects.equals(name, condition.name) &&
                Objects.equals(shortname, condition.shortname) &&
                Objects.equals(modules, condition.modules) &&
                Objects.equals(additionalProperties, condition.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, shortname, modules, additionalProperties);
    }

    @Override
    public String toString() {
        return "Condition{" +
                "name='" + name + '\'' +
                ", shortname='" + shortname + '\'' +
                ", modules=" + modules +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

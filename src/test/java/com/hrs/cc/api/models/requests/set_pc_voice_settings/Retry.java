package com.hrs.cc.api.models.requests.set_pc_voice_settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "measure",
        "units"
})
public class Retry {

    @JsonProperty("measure")
    private String measure;
    @JsonProperty("units")
    private String units;

    public Retry() {
    }

    public Retry(String measure, String units) {
        this.measure = measure;
        this.units = units;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }
}

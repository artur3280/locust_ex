package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "value",
        "modules",
        "ts",
        "details"
})
public class Risk {

    @JsonProperty("value")
    private Integer value;
    @JsonProperty("modules")
    private List<String> modules = null;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("details")
    private List<Detail> details = null;

    public Risk() {
    }

    public Risk(Integer value, List<String> modules, Long ts, List<Detail> details) {
        this.value = value;
        this.modules = modules;
        this.ts = ts;
        this.details = details;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Risk risk = (Risk) o;
        return Objects.equals(value, risk.value) &&
                Objects.equals(modules, risk.modules) &&
                Objects.equals(ts, risk.ts) &&
                Objects.equals(details, risk.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, modules, ts, details);
    }

    @Override
    public String toString() {
        return "Risk{" +
                "value=" + value +
                ", modules=" + modules +
                ", ts=" + ts +
                ", details=" + details +
                '}';
    }
}

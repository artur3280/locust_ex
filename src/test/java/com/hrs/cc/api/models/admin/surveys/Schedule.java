package com.hrs.cc.api.models.admin.surveys;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "schedule",
        "x",
        "startday"
})
public class Schedule {

    @JsonProperty("type")
    private String type;
    @JsonProperty("schedule")
    private String schedule;
    @JsonProperty("x")
    private String x;
    @JsonProperty("startday")
    private String startday;

    public Schedule() {
    }

    public Schedule(String type, String schedule) {
        this.type = type;
        this.schedule = schedule;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "type='" + type + '\'' +
                ", schedule='" + schedule + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.requests.generation_magic_codes;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class MagiCodeIdentityPacient {
    @JsonProperty("data")
    private Data data;

    public MagiCodeIdentityPacient() {
    }

    public MagiCodeIdentityPacient(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MagiCodeIdentityPacient magiCodeIdentityPacient = (MagiCodeIdentityPacient) o;
        return Objects.equals(data, magiCodeIdentityPacient.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "ClinicianAuthToken{" +
                "data=" + data +
                '}';
    }
}

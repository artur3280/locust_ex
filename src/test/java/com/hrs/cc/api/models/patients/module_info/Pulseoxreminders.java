package com.hrs.cc.api.models.patients.module_info;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status"
})
public class Pulseoxreminders {

    @JsonProperty("status")
    private String status;

    public Pulseoxreminders() {
    }

    public Pulseoxreminders(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Pulseoxreminders{" +
                "status='" + status + '\'' +
                '}';
    }
}

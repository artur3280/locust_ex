package com.hrs.cc.api.models.environment.settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "flag",
        "value"
})
public class Setting <T>{

    @JsonProperty("id")
    private String id;
    @JsonProperty("flag")
    private String flag;
    @JsonProperty("value")
    private T value;

    public Setting() {
    }

    public Setting(String id, String flag, T value) {
        this.id = id;
        this.flag = flag;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Setting{" +
                "id='" + id + '\'' +
                ", flag='" + flag + '\'' +
                ", value=" + value +
                '}';
    }
}

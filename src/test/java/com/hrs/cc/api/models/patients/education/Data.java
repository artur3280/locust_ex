package com.hrs.cc.api.models.patients.education;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "patientcontent",
        "content"
})
public class Data {

    @JsonProperty("patientcontent")
    private List<Patientcontent> patientcontent = null;

    @JsonProperty("content")
    private List<Patientcontent> patientAllContent = null;

    public Data() {
    }

    public Data(List<Patientcontent> patientcontent, List<Patientcontent> patientAllContent) {
        this.patientcontent = patientcontent;
        this.patientAllContent = patientAllContent;
    }

    public List<Patientcontent> getPatientcontent() {
        return patientcontent;
    }

    public void setPatientcontent(List<Patientcontent> patientcontent) {
        this.patientcontent = patientcontent;
    }

    public List<Patientcontent> getPatientAllContent() {
        return patientAllContent;
    }

    public void setPatientAllContent(List<Patientcontent> patientAllContent) {
        this.patientAllContent = patientAllContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return Objects.equals(patientcontent, data.patientcontent) &&
                Objects.equals(patientAllContent, data.patientAllContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(patientcontent, patientAllContent);
    }

    @Override
    public String toString() {
        return "Data{" +
                "patientcontent=" + patientcontent +
                ", patientAllContent=" + patientAllContent +
                '}';
    }
}

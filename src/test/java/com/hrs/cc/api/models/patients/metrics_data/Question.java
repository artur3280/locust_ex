package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "schedule",
        "question",
        "answer",
        "today",
        "id",
        "once",
        "ts",
        "discharge"
})
public class Question {

    @JsonProperty("schedule")
    private Schedule schedule;
    @JsonProperty("question")
    private String question;
    @JsonProperty("answer")
    private String answer;
    @JsonProperty("today")
    private Boolean today;
    @JsonProperty("once")
    private Boolean once;
    @JsonProperty("discharge")
    private Boolean discharge;
    @JsonProperty("id")
    private String id;
    @JsonProperty("ts")
    private Long ts;

    public Question() {
    }

    public Question(Schedule schedule, String question, String answer, Boolean today, Boolean once, Boolean discharge, String id, Long ts) {
        this.schedule = schedule;
        this.question = question;
        this.answer = answer;
        this.today = today;
        this.once = once;
        this.discharge = discharge;
        this.id = id;
        this.ts = ts;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Boolean getToday() {
        return today;
    }

    public void setToday(Boolean today) {
        this.today = today;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Boolean getOnce() {
        return once;
    }

    public void setOnce(Boolean once) {
        this.once = once;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question1 = (Question) o;
        return Objects.equals(schedule, question1.schedule) &&
                Objects.equals(question, question1.question) &&
                Objects.equals(answer, question1.answer) &&
                Objects.equals(today, question1.today) &&
                Objects.equals(once, question1.once) &&
                Objects.equals(id, question1.id) &&
                Objects.equals(ts, question1.ts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(schedule, question, answer, today, once, id, ts);
    }

    @Override
    public String toString() {
        return "Question{" +
                "schedule=" + schedule +
                ", question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", today=" + today +
                ", once=" + once +
                ", id='" + id + '\'' +
                ", ts=" + ts +
                '}';
    }
}

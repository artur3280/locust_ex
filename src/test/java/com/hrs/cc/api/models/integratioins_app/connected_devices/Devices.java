package com.hrs.cc.api.models.integratioins_app.connected_devices;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"})
public class Devices {

    @JsonProperty("data")
    private List<DataDevices> data = null;

    public Devices() {
    }

    public Devices(List<DataDevices> data) {
        this.data = data;
    }

    public List<DataDevices> getData() {
        return data;
    }

    public void setData(List<DataDevices> data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Devices devices = (Devices) o;
        return Objects.equals(data, devices.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "Devices{" +
                "data=" + data +
                '}';
    }
}

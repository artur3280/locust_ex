package com.hrs.cc.api.models.environment.presets;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "resourceType"
})
public class Datum_ {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("resourceType")
    private String resourceType;

    public Datum_() {
    }

    public Datum_(Integer id, String resourceType) {
        this.id = id;
        this.resourceType = resourceType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public String toString() {
        return "Datum_{" +
                "id=" + id +
                ", resourceType='" + resourceType + '\'' +
                '}';
    }
}

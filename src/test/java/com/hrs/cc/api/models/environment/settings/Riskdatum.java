package com.hrs.cc.api.models.environment.settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "alerttype",
        "events",
        "value"
})
public class Riskdatum {

    @JsonProperty("type")
    private String type;
    @JsonProperty("alerttype")
    private String alerttype;
    @JsonProperty("events")
    private List<String> events = null;
    @JsonProperty("value")
    private String value;

    public Riskdatum() {
    }

    public Riskdatum(String type, String alerttype, List<String> events, String value) {
        this.type = type;
        this.alerttype = alerttype;
        this.events = events;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAlerttype() {
        return alerttype;
    }

    public void setAlerttype(String alerttype) {
        this.alerttype = alerttype;
    }

    public List<String> getEvents() {
        return events;
    }

    public void setEvents(List<String> events) {
        this.events = events;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Riskdatum{" +
                "type='" + type + '\'' +
                ", alerttype='" + alerttype + '\'' +
                ", events=" + events +
                ", value='" + value + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.integratioins_app.patient_tasks;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "val",
        "type",
        "selectOnly",
        "text",
        "risk"
})
public class Choice<T> {

    @JsonProperty("id")
    private T id;
    @JsonProperty("val")
    private T val;
    @JsonProperty("type")
    private String type;
    @JsonProperty("selectOnly")
    private Boolean selectOnly;
    @JsonProperty("text")
    private String text;
    @JsonProperty("risk")
    private String risk;

    public Choice() {
    }

    public Choice(T id, T val, String type, Boolean selectOnly, String text, String risk) {
        this.id = id;
        this.val = val;
        this.type = type;
        this.selectOnly = selectOnly;
        this.text = text;
        this.risk = risk;
    }

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }

    public T getVal() {
        return val;
    }

    public void setVal(T val) {
        this.val = val;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getSelectOnly() {
        return selectOnly;
    }

    public void setSelectOnly(Boolean selectOnly) {
        this.selectOnly = selectOnly;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Choice choice = (Choice) o;
        return Objects.equals(id, choice.id) &&
                Objects.equals(val, choice.val) &&
                Objects.equals(type, choice.type) &&
                Objects.equals(selectOnly, choice.selectOnly) &&
                Objects.equals(text, choice.text) &&
                Objects.equals(risk, choice.risk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, val, type, selectOnly, text, risk);
    }

    @Override
    public String toString() {
        return "Choice{" +
                "id=" + id +
                ", val=" + val +
                ", type='" + type + '\'' +
                ", selectOnly=" + selectOnly +
                ", text='" + text + '\'' +
                ", risk='" + risk + '\'' +
                '}';
    }
}

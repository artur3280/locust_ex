package com.hrs.cc.api.models.environment.data;

import com.hrs.cc.api.models.environment.data.metric.Data;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "name",
        "shortname"
})
public class Metric {
    @JsonProperty("data")
    private Data data;
    @JsonProperty("name")
    private String name;
    @JsonProperty("shortname")
    private String shortname;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Metric() {
    }

    public Metric(Data data, String name, String shortname) {
        this.data = data;
        this.name = name;
        this.shortname = shortname;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Metric metric = (Metric) o;
        return Objects.equals(data, metric.data) &&
                Objects.equals(name, metric.name) &&
                Objects.equals(shortname, metric.shortname) &&
                Objects.equals(additionalProperties, metric.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(data, name, shortname, additionalProperties);
    }

    @Override
    public String toString() {
        return "Metric{" +
                "data=" + data +
                ", name='" + name + '\'' +
                ", shortname='" + shortname + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

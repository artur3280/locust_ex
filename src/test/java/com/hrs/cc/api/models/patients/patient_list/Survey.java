package com.hrs.cc.api.models.patients.patient_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "questions",
        "time",
        "window"
})
public class Survey {

    @JsonProperty("questions")
    private List<Question> questions = null;
    @JsonProperty("time")
    private Integer time;
    @JsonProperty("window")
    private Integer window;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Survey() {
    }

    public Survey(List<Question> questions, Integer time, Integer window) {
        this.questions = questions;
        this.time = time;
        this.window = window;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getWindow() {
        return window;
    }

    public void setWindow(Integer window) {
        this.window = window;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Survey survey = (Survey) o;
        return Objects.equals(questions, survey.questions) &&
                Objects.equals(time, survey.time) &&
                Objects.equals(window, survey.window) &&
                Objects.equals(additionalProperties, survey.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(questions, time, window, additionalProperties);
    }

    @Override
    public String toString() {
        return "Survey{" +
                "questions=" + questions +
                ", time=" + time +
                ", window=" + window +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

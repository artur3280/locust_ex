package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "duration",
        "goal",
        "deleted",
        "data",
        "ts"
})
public class Activity {

    @JsonProperty("duration")
    private Integer duration;
    @JsonProperty("goal")
    private Integer goal;
    @JsonProperty("data")
    private List<Datum_> data = null;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("ts")
    private Long ts;

    public Activity() {
    }

    public Activity(Integer duration, Integer goal, List<Datum_> data, Boolean deleted, Long ts) {
        this.duration = duration;
        this.goal = goal;
        this.data = data;
        this.deleted = deleted;
        this.ts = ts;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getGoal() {
        return goal;
    }

    public void setGoal(Integer goal) {
        this.goal = goal;
    }

    public List<Datum_> getData() {
        return data;
    }

    public void setData(List<Datum_> data) {
        this.data = data;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "duration=" + duration +
                ", goal=" + goal +
                ", data=" + data +
                ", deleted=" + deleted +
                ", ts=" + ts +
                '}';
    }
}

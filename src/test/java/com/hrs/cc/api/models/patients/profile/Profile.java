package com.hrs.cc.api.models.patients.profile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.hrs.cc.api.models.patients.patient_list.ByodDevice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "clinician",
        "subgroup",
        "devicehistory",
        "device",
        "extension",
        "gender",
        "readmission",
        "edvisit",
        "testpatient",
        "pid",
        "quicknote",
        "language",
        "hrsid",
        "startdate",
        "lastmoduleupdate",
        "volume",
        "phone",
        "dob",
        "name",
        "envphone",
        "conditions",
        "audioreminders",
        "status",
        "PATIENTINFO_CUSTOMATTRIBUTES",
        "lasthospitalization",
        "alternatetelephone",
        "alternatefirstname",
        "alternatemiddlename",
        "consentFormAvailable",
        "byodDevice",
        "reviewed",
        "alternaterelationship",
        "pcv",
        "timezone"
})

public class Profile {

    @JsonProperty("clinician")
    private Clinician clinician;
    @JsonProperty("devicehistory")
    private List<Devicehistory> devicehistory = null;
    @JsonProperty("extension")
    private String extension;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("subgroup")
    private String subgroup;
    @JsonProperty("readmission")
    private Long readmission;
    @JsonProperty("edvisit")
    private Integer edvisit;
    @JsonProperty("quicknote")
    private Quicknote quicknote = null;
    @JsonProperty("testpatient")
    private Boolean testpatient;
    @JsonProperty("pid")
    private String pid;
    @JsonProperty("timezone")
    private String timezone;
    @JsonProperty("language")
    private String language;
    @JsonProperty("hrsid")
    private String hrsid;
    @JsonProperty("startdate")
    private Long startdate;
    @JsonProperty("lastmoduleupdate")
    private Long lastmoduleupdate;
    @JsonProperty("volume")
    private Integer volume;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("dob")
    private String dob;
    @JsonProperty("alternatetelephone")
    private String alternatetelephone;
    @JsonProperty("alternatefirstname")
    private String alternatefirstname;
    @JsonProperty("alternatemiddlename")
    private String alternatemiddlename;
    @JsonProperty("alternatelastname")
    private String alternatelastname;
    @JsonProperty("alternaterelationship")
    private String alternaterelationship;
    @JsonProperty("name")
    private Name name;
    @JsonProperty("envphone")
    private String envphone;
    @JsonProperty("conditions")
    private List<String> conditions = null;
    @JsonProperty("audioreminders")
    private Boolean audioreminders;
    @JsonProperty("status")
    private String status;
    @JsonProperty("PATIENTINFO_CUSTOMATTRIBUTES")
    private List<PatientInfoCustomAttributes> patientInfoCustomAttributes = null;
    @JsonProperty("lasthospitalization")
    private Long lasthospitalization = null;
    @JsonProperty("device")
    private Device device;
    @JsonProperty("consentFormAvailable")
    private Boolean consentFormAvailable = null;
    @JsonProperty("byodDevice")
    private ByodDevice byodDevice;
    @JsonProperty("reviewed")
    private List<Reviewed> reviewed = null;
    @JsonProperty("pcv")
    private Pcv pcv;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Profile() {
    }

    public Profile(Clinician clinician,
                   List<Devicehistory> devicehistory,
                   String extension,
                   String gender,
                   String subgroup,
                   Long readmission,
                   Integer edvisit,
                   Quicknote quicknote,
                   Boolean testpatient,
                   String pid,
                   String language,
                   String hrsid,
                   Long startdate,
                   Long lastmoduleupdate,
                   Integer volume,
                   String phone,
                   String dob,
                   String alternatetelephone,
                   String alternatefirstname,
                   String alternatemiddlename,
                   String alternatelastname,
                   String alternaterelationship,
                   Name name,
                   String envphone,
                   List<String> conditions,
                   Boolean audioreminders,
                   String status,
                   List<PatientInfoCustomAttributes> patientInfoCustomAttributes,
                   Long lasthospitalization,
                   Device device,
                   Boolean consentFormAvailable,
                   ByodDevice byodDevice,
                   List<Reviewed> reviewed,
                   Pcv pcv,
                   String timezone) {
        this.clinician = clinician;
        this.devicehistory = devicehistory;
        this.extension = extension;
        this.gender = gender;
        this.subgroup = subgroup;
        this.readmission = readmission;
        this.edvisit = edvisit;
        this.quicknote = quicknote;
        this.testpatient = testpatient;
        this.pid = pid;
        this.language = language;
        this.hrsid = hrsid;
        this.startdate = startdate;
        this.lastmoduleupdate = lastmoduleupdate;
        this.volume = volume;
        this.phone = phone;
        this.dob = dob;
        this.alternatetelephone = alternatetelephone;
        this.alternatefirstname = alternatefirstname;
        this.alternatemiddlename = alternatemiddlename;
        this.alternatelastname = alternatelastname;
        this.alternaterelationship = alternaterelationship;
        this.name = name;
        this.envphone = envphone;
        this.conditions = conditions;
        this.audioreminders = audioreminders;
        this.status = status;
        this.patientInfoCustomAttributes = patientInfoCustomAttributes;
        this.lasthospitalization = lasthospitalization;
        this.device = device;
        this.consentFormAvailable = consentFormAvailable;
        this.byodDevice = byodDevice;
        this.reviewed = reviewed;
        this.pcv = pcv;
        this.timezone = timezone;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Clinician getClinician() {
        return clinician;
    }

    public void setClinician(Clinician clinician) {
        this.clinician = clinician;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getReadmission() {
        return readmission;
    }

    public void setReadmission(Long readmission) {
        this.readmission = readmission;
    }

    public Integer getEdvisit() {
        return edvisit;
    }

    public void setEdvisit(Integer edvisit) {
        this.edvisit = edvisit;
    }

    public Boolean getTestpatient() {
        return testpatient;
    }

    public void setTestpatient(Boolean testpatient) {
        this.testpatient = testpatient;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public Long getStartdate() {
        return startdate;
    }

    public void setStartdate(Long startdate) {
        this.startdate = startdate;
    }

    public Long getLastmoduleupdate() {
        return lastmoduleupdate;
    }

    public void setLastmoduleupdate(Long lastmoduleupdate) {
        this.lastmoduleupdate = lastmoduleupdate;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getEnvphone() {
        return envphone;
    }

    public void setEnvphone(String envphone) {
        this.envphone = envphone;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public Boolean getAudioreminders() {
        return audioreminders;
    }

    public void setAudioreminders(Boolean audioreminders) {
        this.audioreminders = audioreminders;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Quicknote getQuicknote() {
        return quicknote;
    }

    public void setQuicknote(Quicknote quicknote) {
        this.quicknote = quicknote;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public List<PatientInfoCustomAttributes> getPatientInfoCustomAttributes() {
        return patientInfoCustomAttributes;
    }

    public void setPatientInfoCustomAttributes(List<PatientInfoCustomAttributes> patientInfoCustomAttributes) {
        this.patientInfoCustomAttributes = patientInfoCustomAttributes;
    }

    public Boolean getConsentFormAvailable() {
        return consentFormAvailable;
    }

    public void setConsentFormAvailable(Boolean consentFormAvailable) {
        this.consentFormAvailable = consentFormAvailable;
    }

    public List<Devicehistory> getDevicehistory() {
        return devicehistory;
    }

    public void setDevicehistory(List<Devicehistory> devicehistory) {
        this.devicehistory = devicehistory;
    }

    public Long getLasthospitalization() {
        return lasthospitalization;
    }

    public void setLasthospitalization(Long lasthospitalization) {
        this.lasthospitalization = lasthospitalization;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public String getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(String subgroup) {
        this.subgroup = subgroup;
    }

    public String getAlternatetelephone() {
        return alternatetelephone;
    }

    public void setAlternatetelephone(String alternatetelephone) {
        this.alternatetelephone = alternatetelephone;
    }

    public String getAlternatefirstname() {
        return alternatefirstname;
    }

    public void setAlternatefirstname(String alternatefirstname) {
        this.alternatefirstname = alternatefirstname;
    }

    public String getAlternatemiddlename() {
        return alternatemiddlename;
    }

    public void setAlternatemiddlename(String alternatemiddlename) {
        this.alternatemiddlename = alternatemiddlename;
    }

    public String getAlternatelastname() {
        return alternatelastname;
    }

    public void setAlternatelastname(String alternatelastname) {
        this.alternatelastname = alternatelastname;
    }

    public String getAlternaterelationship() {
        return alternaterelationship;
    }

    public void setAlternaterelationship(String alternaterelationship) {
        this.alternaterelationship = alternaterelationship;
    }

    public ByodDevice getByodDevice() {
        return byodDevice;
    }

    public void setByodDevice(ByodDevice byodDevice) {
        this.byodDevice = byodDevice;
    }

    public List<Reviewed> getReviewed() {
        return reviewed;
    }

    public void setReviewed(List<Reviewed> reviewed) {
        this.reviewed = reviewed;
    }

    public Pcv getPcv() {
        return pcv;
    }

    public void setPcv(Pcv pcv) {
        this.pcv = pcv;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "clinician=" + clinician +
                ", devicehistory=" + devicehistory +
                ", extension='" + extension + '\'' +
                ", gender='" + gender + '\'' +
                ", subgroup='" + subgroup + '\'' +
                ", readmission=" + readmission +
                ", edvisit=" + edvisit +
                ", quicknote=" + quicknote +
                ", testpatient=" + testpatient +
                ", pid='" + pid + '\'' +
                ", timezone='" + timezone + '\'' +
                ", language='" + language + '\'' +
                ", hrsid='" + hrsid + '\'' +
                ", startdate=" + startdate +
                ", lastmoduleupdate=" + lastmoduleupdate +
                ", volume=" + volume +
                ", phone='" + phone + '\'' +
                ", dob='" + dob + '\'' +
                ", alternatetelephone='" + alternatetelephone + '\'' +
                ", alternatefirstname='" + alternatefirstname + '\'' +
                ", alternatemiddlename='" + alternatemiddlename + '\'' +
                ", alternatelastname='" + alternatelastname + '\'' +
                ", alternaterelationship='" + alternaterelationship + '\'' +
                ", name=" + name +
                ", envphone='" + envphone + '\'' +
                ", conditions=" + conditions +
                ", audioreminders=" + audioreminders +
                ", status='" + status + '\'' +
                ", patientInfoCustomAttributes=" + patientInfoCustomAttributes +
                ", lasthospitalization=" + lasthospitalization +
                ", device=" + device +
                ", consentFormAvailable=" + consentFormAvailable +
                ", byodDevice=" + byodDevice +
                ", reviewed=" + reviewed +
                ", pcv=" + pcv +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

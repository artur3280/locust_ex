package com.hrs.cc.api.models.integratioins_app.patient_tasks;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "question",
        "language"
})
public class QuestionTranslation {

    @JsonProperty("question")
    private String question;
    @JsonProperty("language")
    private String language;

    public QuestionTranslation() {
    }

    public QuestionTranslation(String question, String language) {
        this.question = question;
        this.language = language;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionTranslation that = (QuestionTranslation) o;
        return Objects.equals(question, that.question) &&
                Objects.equals(language, that.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(question, language);
    }

    @Override
    public String toString() {
        return "QuestionTranslation{" +
                "question='" + question + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.patients.notes.history;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "timezone_type",
        "timezone"
})
public class EditTime {

    @JsonProperty("date")
    private String date;
    @JsonProperty("timezone_type")
    private Integer timezoneType;
    @JsonProperty("timezone")
    private String timezone;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public EditTime() {
    }

    public EditTime(String date, Integer timezoneType, String timezone) {
        this.date = date;
        this.timezoneType = timezoneType;
        this.timezone = timezone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTimezoneType() {
        return timezoneType;
    }

    public void setTimezoneType(Integer timezoneType) {
        this.timezoneType = timezoneType;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EditTime editTime = (EditTime) o;
        return Objects.equals(date, editTime.date) &&
                Objects.equals(timezoneType, editTime.timezoneType) &&
                Objects.equals(timezone, editTime.timezone) &&
                Objects.equals(additionalProperties, editTime.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(date, timezoneType, timezone, additionalProperties);
    }

    @Override
    public String toString() {
        return "EditTime{" +
                "date='" + date + '\'' +
                ", timezoneType=" + timezoneType +
                ", timezone='" + timezone + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

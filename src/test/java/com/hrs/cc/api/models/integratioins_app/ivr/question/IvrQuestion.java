package com.hrs.cc.api.models.integratioins_app.ivr.question;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.hrs.cc.api.models.integratioins_app.ivr.questions.Datum;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data"
})
public class IvrQuestion {

    @JsonProperty("data")
    private Datum data = null;

    public IvrQuestion() {
    }

    public IvrQuestion(Datum data) {
        this.data = data;
    }

    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IvrQuestion that = (IvrQuestion) o;
        return Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "IvrQuestionsList{" +
                "data=" + data +
                '}';
    }
}

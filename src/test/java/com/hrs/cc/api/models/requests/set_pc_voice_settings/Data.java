package com.hrs.cc.api.models.requests.set_pc_voice_settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "hrsid",
        "status",
        "callAt",
        "retry",
        "retryCount",
        "entries"
})
public class Data {

    @JsonProperty("id")
    private String id;
    @JsonProperty("hrsid")
    private String hrsid;
    @JsonProperty("status")
    private String status;
    @JsonProperty("callAt")
    private String callAt;
    @JsonProperty("retry")
    private Retry retry;
    @JsonProperty("retryCount")
    private Integer retryCount;
    @JsonProperty("entries")
    private List<Entry> entries = null;

    public Data() {
    }

    public Data(String id, String hrsid, String status, String callAt, Retry retry, Integer retryCount, List<Entry> entries) {
        this.id = id;
        this.hrsid = hrsid;
        this.status = status;
        this.callAt = callAt;
        this.retry = retry;
        this.retryCount = retryCount;
        this.entries = entries;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCallAt() {
        return callAt;
    }

    public void setCallAt(String callAt) {
        this.callAt = callAt;
    }

    public Retry getRetry() {
        return retry;
    }

    public void setRetry(Retry retry) {
        this.retry = retry;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }
}

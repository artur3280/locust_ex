package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "weight",
        "id",
        "type",
        "ts",
        "status",
        "deleted",
        "risk"
})
public class Datum____ {

    @JsonProperty("weight")
    private Integer weight;
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("ts")
    private Long ts;
    @JsonProperty("status")
    private String status;
    @JsonProperty("risk")
    private String risk;
    @JsonProperty("deleted")
    private Boolean deleted;

    public Datum____() {
    }

    public Datum____(Integer weight, String id, String type, Long ts, String status, String risk, Boolean deleted) {
        this.weight = weight;
        this.id = id;
        this.type = type;
        this.ts = ts;
        this.status = status;
        this.risk = risk;
        this.deleted = deleted;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Datum____{" +
                "weight=" + weight +
                ", id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", ts=" + ts +
                ", status='" + status + '\'' +
                ", risk='" + risk + '\'' +
                ", deleted=" + deleted +
                '}';
    }
}

package com.hrs.cc.api.models.patients.patient_notes;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "location",
        "los"
})
public class Edvisit {

    @JsonProperty("date")
    private Object date;
    @JsonProperty("location")
    private Object location;
    @JsonProperty("los")
    private Integer los;

    public Edvisit() {
    }

    public Edvisit(Object date, Object location, Integer los) {
        this.date = date;
        this.location = location;
        this.los = los;
    }

    public Object getDate() {
        return date;
    }

    public void setDate(Object date) {
        this.date = date;
    }

    public Object getLocation() {
        return location;
    }

    public void setLocation(Object location) {
        this.location = location;
    }

    public Integer getLos() {
        return los;
    }

    public void setLos(Integer los) {
        this.los = los;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edvisit edvisit = (Edvisit) o;
        return Objects.equals(date, edvisit.date) &&
                Objects.equals(location, edvisit.location) &&
                Objects.equals(los, edvisit.los);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, location, los);
    }

    @Override
    public String toString() {
        return "Edvisit{" +
                "date=" + date +
                ", location=" + location +
                ", los=" + los +
                '}';
    }
}

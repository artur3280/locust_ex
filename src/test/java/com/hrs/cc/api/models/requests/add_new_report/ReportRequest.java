package com.hrs.cc.api.models.requests.add_new_report;

import java.util.Objects;

public class ReportRequest {
    private String report;
    private String start;
    private String stop;

    public ReportRequest(String report, String start, String stop) {
        this.report = report;
        this.start = start;
        this.stop = stop;
    }

    public ReportRequest() {
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportRequest that = (ReportRequest) o;
        return Objects.equals(report, that.report) &&
                Objects.equals(start, that.start) &&
                Objects.equals(stop, that.stop);
    }

    @Override
    public int hashCode() {
        return Objects.hash(report, start, stop);
    }

    @Override
    public String toString() {
        return "ReportRequest{" +
                "report='" + report + '\'' +
                ", start='" + start + '\'' +
                ", stop='" + stop + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "reminders",
        "quiz",
        "video",
        "tracking",
        "medlist"
})
public class Data {

    @JsonProperty("reminders")
    private Integer reminders;
    @JsonProperty("quiz")
    private Integer quiz;
    @JsonProperty("video")
    private Integer video;
    @JsonProperty("tracking")
    private Integer tracking;
    @JsonProperty("medlist")
    private Integer medlist;

    public Data() {
    }

    public Data(Integer reminders, Integer quiz, Integer video, Integer tracking, Integer medlist) {
        this.reminders = reminders;
        this.quiz = quiz;
        this.video = video;
        this.tracking = tracking;
        this.medlist = medlist;
    }

    public Integer getReminders() {
        return reminders;
    }

    public void setReminders(Integer reminders) {
        this.reminders = reminders;
    }

    public Integer getQuiz() {
        return quiz;
    }

    public void setQuiz(Integer quiz) {
        this.quiz = quiz;
    }

    public Integer getVideo() {
        return video;
    }

    public void setVideo(Integer video) {
        this.video = video;
    }

    public Integer getTracking() {
        return tracking;
    }

    public void setTracking(Integer tracking) {
        this.tracking = tracking;
    }

    public Integer getMedlist() {
        return medlist;
    }

    public void setMedlist(Integer medlist) {
        this.medlist = medlist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return Objects.equals(reminders, data.reminders) &&
                Objects.equals(quiz, data.quiz) &&
                Objects.equals(video, data.video) &&
                Objects.equals(tracking, data.tracking) &&
                Objects.equals(medlist, data.medlist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reminders, quiz, video, tracking, medlist);
    }

    @Override
    public String toString() {
        return "Data{" +
                "reminders=" + reminders +
                ", quiz=" + quiz +
                ", video=" + video +
                ", tracking=" + tracking +
                ", medlist=" + medlist +
                '}';
    }
}

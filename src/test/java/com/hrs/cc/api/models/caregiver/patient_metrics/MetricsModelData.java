package com.hrs.cc.api.models.caregiver.patient_metrics;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "percentComplete",
        "risk"
})
public class MetricsModelData {
    @JsonProperty("percentComplete")
    private Integer percentComplete;
    @JsonProperty("risk")
    private String risk;

    public MetricsModelData() {
    }

    public MetricsModelData(Integer percentComplete, String risk) {
        this.percentComplete = percentComplete;
        this.risk = risk;
    }

    public Integer getPercentComplete() {
        return percentComplete;
    }

    public void setPercentComplete(Integer percentComplete) {
        this.percentComplete = percentComplete;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetricsModelData that = (MetricsModelData) o;
        return Objects.equals(percentComplete, that.percentComplete) &&
                Objects.equals(risk, that.risk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(percentComplete, risk);
    }

    @Override
    public String toString() {
        return "MetricsModelData{" +
                "percentComplete=" + percentComplete +
                ", risk='" + risk + '\'' +
                '}';
    }
}

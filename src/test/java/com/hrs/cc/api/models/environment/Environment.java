package com.hrs.cc.api.models.environment;

import com.hrs.cc.api.models.environment.data.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.hrs.cc.api.models.environment.data.Settings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "settings",
        "environmentSettings",
        "metrics",
        "conditions",
        "PATIENTINFO_CUSTOMATTRIBUTES"
})
public class Environment {
    @JsonProperty("settings")
    private Settings settings;
    @JsonProperty("environmentSettings")
    private List<EnvironmentSetting<Object>> environmentSettings = null;
    @JsonProperty("metrics")
    private List<Metric> metrics = null;
    @JsonProperty("conditions")
    private List<Condition> conditions = null;
    @JsonProperty("PATIENTINFO_CUSTOMATTRIBUTES")
    private List<PatientInfoCustomAttributes> patientInfoCustomAttributes = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Environment() {
    }

    public Environment(Settings settings, List<EnvironmentSetting<Object>> environmentSettings, List<Metric> metrics, List<Condition> conditions, List<PatientInfoCustomAttributes> patientInfoCustomAttributes) {
        this.settings = settings;
        this.environmentSettings = environmentSettings;
        this.metrics = metrics;
        this.conditions = conditions;
        this.patientInfoCustomAttributes = patientInfoCustomAttributes;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public List<EnvironmentSetting<Object>> getEnvironmentSettings() {
        return environmentSettings;
    }

    public void setEnvironmentSettings(List<EnvironmentSetting<Object>> environmentSettings) {
        this.environmentSettings = environmentSettings;
    }

    public List<Metric> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<Metric> metrics) {
        this.metrics = metrics;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public List<PatientInfoCustomAttributes> getPatientInfoCustomAttributes() {
        return patientInfoCustomAttributes;
    }

    public void setPatientInfoCustomAttributes(List<PatientInfoCustomAttributes> patientInfoCustomAttributes) {
        this.patientInfoCustomAttributes = patientInfoCustomAttributes;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Environment that = (Environment) o;
        return Objects.equals(settings, that.settings) &&
                Objects.equals(environmentSettings, that.environmentSettings) &&
                Objects.equals(metrics, that.metrics) &&
                Objects.equals(conditions, that.conditions) &&
                Objects.equals(patientInfoCustomAttributes, that.patientInfoCustomAttributes) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(settings, environmentSettings, metrics, conditions, patientInfoCustomAttributes, additionalProperties);
    }

    @Override
    public String toString() {
        return "Environment{" +
                "settings=" + settings +
                ", environmentSettings=" + environmentSettings +
                ", metrics=" + metrics +
                ", conditions=" + conditions +
                ", patientInfoCustomAttributes=" + patientInfoCustomAttributes +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

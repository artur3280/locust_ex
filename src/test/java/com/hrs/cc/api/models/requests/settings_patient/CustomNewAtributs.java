package com.hrs.cc.api.models.requests.settings_patient;

import java.util.Objects;

public class CustomNewAtributs {
    private Integer id;
    private String name;
    private String type;
    private Boolean required;
    private Boolean patientsetup;
    private String _class;
    private Boolean emrTracked;
    private String lastUpdated;
    private String $$hashKey;
    private String value;

    public CustomNewAtributs() {
    }

    public CustomNewAtributs(Integer id, String name, String type, Boolean required, Boolean patientsetup, String _class, Boolean emrTracked, String lastUpdated, String $$hashKey, String value) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.required = required;
        this.patientsetup = patientsetup;
        this._class = _class;
        this.emrTracked = emrTracked;
        this.lastUpdated = lastUpdated;
        this.$$hashKey = $$hashKey;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Boolean getPatientsetup() {
        return patientsetup;
    }

    public void setPatientsetup(Boolean patientsetup) {
        this.patientsetup = patientsetup;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public Boolean getEmrTracked() {
        return emrTracked;
    }

    public void setEmrTracked(Boolean emrTracked) {
        this.emrTracked = emrTracked;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String get$$hashKey() {
        return $$hashKey;
    }

    public void set$$hashKey(String $$hashKey) {
        this.$$hashKey = $$hashKey;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomNewAtributs that = (CustomNewAtributs) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(type, that.type) &&
                Objects.equals(required, that.required) &&
                Objects.equals(patientsetup, that.patientsetup) &&
                Objects.equals(_class, that._class) &&
                Objects.equals(emrTracked, that.emrTracked) &&
                Objects.equals(lastUpdated, that.lastUpdated) &&
                Objects.equals($$hashKey, that.$$hashKey) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, type, required, patientsetup, _class, emrTracked, lastUpdated, $$hashKey, value);
    }

    @Override
    public String toString() {
        return "CustomAtributs{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", required=" + required +
                ", patientsetup=" + patientsetup +
                ", _class='" + _class + '\'' +
                ", emrTracked=" + emrTracked +
                ", lastUpdated='" + lastUpdated + '\'' +
                ", $$hashKey='" + $$hashKey + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}

package com.hrs.cc.api.models.patients.patient_metric.response_after_add.response;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "expiration",
        "essential",
        "instruction",
        "x",
        "startday",
        "schedule"
})
public class Schedule {

    @JsonProperty("type")
    private String type;
    @JsonProperty("expiration")
    private String expiration;
    @JsonProperty("essential")
    private String essential;
    @JsonProperty("instruction")
    private String instruction;
    @JsonProperty("x")
    private String x;
    @JsonProperty("startday")
    private String startday;
    @JsonProperty("schedule")
    private String schedule;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Schedule() {
    }

    public Schedule(String type, String expiration, String essential, String instruction, String x, String startday, Map<String, Object> additionalProperties) {
        this.type = type;
        this.expiration = expiration;
        this.essential = essential;
        this.instruction = instruction;
        this.x = x;
        this.startday = startday;
        this.additionalProperties = additionalProperties;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getEssential() {
        return essential;
    }

    public void setEssential(String essential) {
        this.essential = essential;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getStartday() {
        return startday;
    }

    public void setStartday(String startday) {
        this.startday = startday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return Objects.equals(type, schedule.type) &&
                Objects.equals(expiration, schedule.expiration) &&
                Objects.equals(essential, schedule.essential) &&
                Objects.equals(instruction, schedule.instruction) &&
                Objects.equals(additionalProperties, schedule.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, expiration, essential, instruction, additionalProperties);
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "type='" + type + '\'' +
                ", expiration='" + expiration + '\'' +
                ", essential='" + essential + '\'' +
                ", instruction='" + instruction + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

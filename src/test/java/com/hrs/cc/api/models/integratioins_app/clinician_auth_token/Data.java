package com.hrs.cc.api.models.integratioins_app.clinician_auth_token;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "token",
        "refresh"
})
public class Data {

    @JsonProperty("token")
    private String token;
    @JsonProperty("refresh")
    private String refresh;

    public Data(String token, String refresh) {
        this.token = token;
        this.refresh = refresh;
    }

    public Data() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return Objects.equals(token, data.token) &&
                Objects.equals(refresh, data.refresh);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, refresh);
    }


}

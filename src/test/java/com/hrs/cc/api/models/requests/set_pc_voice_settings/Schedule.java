package com.hrs.cc.api.models.requests.set_pc_voice_settings;

public class Schedule {

    private String start;
    private Object end;
    private Every every;

    public Schedule() {
    }

    public Schedule(String start, Object end, Every every) {
        this.start = start;
        this.end = end;
        this.every = every;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public Object getEnd() {
        return end;
    }

    public void setEnd(Object end) {
        this.end = end;
    }

    public Every getEvery() {
        return every;
    }

    public void setEvery(Every every) {
        this.every = every;
    }
}

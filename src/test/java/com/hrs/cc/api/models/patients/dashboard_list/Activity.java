package com.hrs.cc.api.models.patients.dashboard_list;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "duration",
        "ts"
})
public class Activity {

    @JsonProperty("active")
    private String active;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("ts")
    private String ts;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Activity() {
    }

    public Activity(String active, String duration, String ts) {
        this.active = active;
        this.duration = duration;
        this.ts = ts;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Activity activity = (Activity) o;
        return Objects.equals(active, activity.active) &&
                Objects.equals(duration, activity.duration) &&
                Objects.equals(ts, activity.ts) &&
                Objects.equals(additionalProperties, activity.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(active, duration, ts, additionalProperties);
    }

    @Override
    public String toString() {
        return "Activity{" +
                "active='" + active + '\'' +
                ", duration='" + duration + '\'' +
                ", ts='" + ts + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

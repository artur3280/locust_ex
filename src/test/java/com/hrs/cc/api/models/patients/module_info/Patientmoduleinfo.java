package com.hrs.cc.api.models.patients.module_info;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "modules",
        "activityreminders",
        "surveyreminders",
        "weightreminders",
        "medicationreminders",
        "glucosereminders",
        "pulseoxreminders",
        "bloodpressurereminders",
        "temperaturereminders"
})
public class Patientmoduleinfo {

    @JsonProperty("modules")
    private List<String> modules = null;
    @JsonProperty("activityreminders")
    private Activityreminders activityreminders;
    @JsonProperty("surveyreminders")
    private Surveyreminders surveyreminders;
    @JsonProperty("weightreminders")
    private Weightreminders weightreminders;
    @JsonProperty("medicationreminders")
    private List<Medicationreminder> medicationreminders = null;
    @JsonProperty("glucosereminders")
    private Glucosereminders glucosereminders;
    @JsonProperty("pulseoxreminders")
    private Pulseoxreminders pulseoxreminders;
    @JsonProperty("bloodpressurereminders")
    private Bloodpressurereminders bloodpressurereminders;
    @JsonProperty("temperaturereminders")
    private Temperaturereminders temperaturereminders;

    public Patientmoduleinfo() {
    }

    public Patientmoduleinfo(List<String> modules, Activityreminders activityreminders, Surveyreminders surveyreminders, Weightreminders weightreminders, List<Medicationreminder> medicationreminders, Glucosereminders glucosereminders, Pulseoxreminders pulseoxreminders, Bloodpressurereminders bloodpressurereminders, Temperaturereminders temperaturereminders) {
        this.modules = modules;
        this.activityreminders = activityreminders;
        this.surveyreminders = surveyreminders;
        this.weightreminders = weightreminders;
        this.medicationreminders = medicationreminders;
        this.glucosereminders = glucosereminders;
        this.pulseoxreminders = pulseoxreminders;
        this.bloodpressurereminders = bloodpressurereminders;
        this.temperaturereminders = temperaturereminders;
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    public Activityreminders getActivityreminders() {
        return activityreminders;
    }

    public void setActivityreminders(Activityreminders activityreminders) {
        this.activityreminders = activityreminders;
    }

    public Surveyreminders getSurveyreminders() {
        return surveyreminders;
    }

    public void setSurveyreminders(Surveyreminders surveyreminders) {
        this.surveyreminders = surveyreminders;
    }

    public Weightreminders getWeightreminders() {
        return weightreminders;
    }

    public void setWeightreminders(Weightreminders weightreminders) {
        this.weightreminders = weightreminders;
    }

    public List<Medicationreminder> getMedicationreminders() {
        return medicationreminders;
    }

    public void setMedicationreminders(List<Medicationreminder> medicationreminders) {
        this.medicationreminders = medicationreminders;
    }

    public Glucosereminders getGlucosereminders() {
        return glucosereminders;
    }

    public void setGlucosereminders(Glucosereminders glucosereminders) {
        this.glucosereminders = glucosereminders;
    }

    public Pulseoxreminders getPulseoxreminders() {
        return pulseoxreminders;
    }

    public void setPulseoxreminders(Pulseoxreminders pulseoxreminders) {
        this.pulseoxreminders = pulseoxreminders;
    }

    public Bloodpressurereminders getBloodpressurereminders() {
        return bloodpressurereminders;
    }

    public void setBloodpressurereminders(Bloodpressurereminders bloodpressurereminders) {
        this.bloodpressurereminders = bloodpressurereminders;
    }

    public Temperaturereminders getTemperaturereminders() {
        return temperaturereminders;
    }

    public void setTemperaturereminders(Temperaturereminders temperaturereminders) {
        this.temperaturereminders = temperaturereminders;
    }

    @Override
    public String toString() {
        return "Patientmoduleinfo{" +
                "modules=" + modules +
                ", activityreminders=" + activityreminders +
                ", surveyreminders=" + surveyreminders +
                ", weightreminders=" + weightreminders +
                ", medicationreminders=" + medicationreminders +
                ", glucosereminders=" + glucosereminders +
                ", pulseoxreminders=" + pulseoxreminders +
                ", bloodpressurereminders=" + bloodpressurereminders +
                ", temperaturereminders=" + temperaturereminders +
                '}';
    }
}

package com.hrs.cc.api.models.patients.metrics_data;

import com.fasterxml.jackson.annotation.*;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "questions",
        "time",
        "window"
})

public class Survey {

    @JsonProperty("questions")
    private List<Question> questions;
    @JsonProperty("time")
    private Integer time;
    @JsonProperty("window")
    private Integer window;

    public Survey() {
    }

    public Survey(List<Question> questions, Integer time, Integer window) {
        this.questions = questions;
        this.time = time;
        this.window = window;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getWindow() {
        return window;
    }

    public void setWindow(Integer window) {
        this.window = window;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Survey survey = (Survey) o;
        return Objects.equals(questions, survey.questions) &&
                Objects.equals(time, survey.time) &&
                Objects.equals(window, survey.window);
    }

    @Override
    public int hashCode() {
        return Objects.hash(questions, time, window);
    }

    @Override
    public String toString() {
        return "Survey{" +
                "questions=" + questions +
                ", time=" + time +
                ", window=" + window +
                '}';
    }
}

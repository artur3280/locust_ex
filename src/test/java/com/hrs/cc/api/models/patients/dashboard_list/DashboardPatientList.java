package com.hrs.cc.api.models.patients.dashboard_list;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "patientlist"
})
public class DashboardPatientList {

    @JsonProperty("patientlist")
    private List<PatientList> patientlist = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public DashboardPatientList(List<PatientList> patientlist) {
        this.patientlist = patientlist;
    }

    public DashboardPatientList() {
    }

    public List<PatientList> getPatientlist() {
        return patientlist;
    }

    public void setPatientlist(List<PatientList> patientlist) {
        this.patientlist = patientlist;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DashboardPatientList that = (DashboardPatientList) o;
        return Objects.equals(patientlist, that.patientlist) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {

        return Objects.hash(patientlist, additionalProperties);
    }

    @Override
    public String toString() {
        return "DashboardPatientList{" +
                "patientlist=" + patientlist +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}

package com.hrs.cc.api.models.patients.module_info;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "minlevel",
        "maxlevel"
})
public class Glucosereminders {

    @JsonProperty("minlevel")
    private String minlevel;
    @JsonProperty("maxlevel")
    private String maxlevel;

    public Glucosereminders() {
    }

    public Glucosereminders(String minlevel, String maxlevel) {
        this.minlevel = minlevel;
        this.maxlevel = maxlevel;
    }

    public String getMinlevel() {
        return minlevel;
    }

    public void setMinlevel(String minlevel) {
        this.minlevel = minlevel;
    }

    public String getMaxlevel() {
        return maxlevel;
    }

    public void setMaxlevel(String maxlevel) {
        this.maxlevel = maxlevel;
    }

    @Override
    public String toString() {
        return "Glucosereminders{" +
                "minlevel='" + minlevel + '\'' +
                ", maxlevel='" + maxlevel + '\'' +
                '}';
    }
}

package com.hrs.cc.api.int_patient;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.EndPoints;
import com.hrs.cc.api.constans.Modules;
import com.hrs.cc.api.constans.Path;
import com.hrs.cc.api.models.patients.Patients;
import com.hrs.cc.api.models.patients.patient_list.PatientList;
import com.hrs.cc.api.models.patients.patient_list.Profile;
import com.hrs.cc.api.models.requests.add_new_patient.CustomAtributs;
import com.hrs.cc.api.models.requests.add_new_patient.Name;
import com.hrs.cc.api.models.requests.add_new_patient.NewPatient;
import com.hrs.cc.api.models.requests.set_patient_metricks.*;
import core.PropertyWorker;
import core.rest.RandomData;
import core.rest.RestUtilities;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MakeNewIntegrationPatient {
    private static RequestSpecification reqSpec;
    private static ResponseSpecification resPec;
    private static RestUtilities restUtilities;
    private static PropertyWorker propertyWorker;

    public static void start() throws IOException {
        System.out.println("Connection to env:" + Path.BASE_URI);
        System.out.println("Checking patient:" + Auth.PATIENT_FIRST_NAME_INTG + " " + Auth.PATIENT_LAST_NAME_INTG);
        try {
            Profile checkPatient = getPatientByName(Auth.PATIENT_FIRST_NAME_INTG, Auth.PATIENT_LAST_NAME_INTG);
            System.out.println("Patient is exists: " + checkPatient.getHrsid());
            propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
            propertyWorker.setProperty("patient_hrs_id", checkPatient.getHrsid());
        } catch (IndexOutOfBoundsException | IOException e) {
            System.out.println("Creating patient:" + Auth.PATIENT_FIRST_NAME_INTG + " " + Auth.PATIENT_LAST_NAME_INTG);
            String newPatient = getAddNewPatient();
            propertyWorker = new PropertyWorker(Connection.AUTH_PROP);
            propertyWorker.setProperty("patient_hrs_id", newPatient);
            System.out.println("done \n");
            System.out.println("Set modules to patient:" + Auth.PATIENT_FIRST_NAME_INTG + " " + Auth.PATIENT_LAST_NAME_INTG);
            setModulesToPatientById(newPatient);
            System.out.println("done \n");
            System.out.println("Set risk to patient:" + Auth.PATIENT_FIRST_NAME_INTG + " " + Auth.PATIENT_LAST_NAME_INTG);
            setRisksToPatientById(newPatient);
            System.out.println("done \n");
        }
    }


    private static Profile getPatientByName(String firstName, String lastName) {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));

        resPec = restUtilities.getResponseSpecification();

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        restUtilities.createQueryParam(reqSpec, "status[]", "Activated");
        restUtilities.createQueryParam(reqSpec, "status[]", "Pre-Activated");
        restUtilities.createQueryParam(reqSpec, "status[]", "Paused");
        restUtilities.createQueryParam(reqSpec, "status[]", "Deactivated");
        restUtilities.createQueryParam(reqSpec, "status[]", "Discharged");
        Response response = restUtilities.getResponse(reqSpec, "get");
        Patients patients = response.as(Patients.class);


        PatientList p = patients.getPatientlist().stream().filter(
                patient -> patient.get(0).getProfile().getName().getFirst().equals(firstName) &&
                        patient.get(0).getProfile().getName().getLast().equals(lastName)).collect(Collectors.toList()).get(0).get(0);
        if (p != null) {
            return p.getProfile();
        }
        return null;
    }

   private static String getAddNewPatient() {
        restUtilities = new RestUtilities();
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities.setBaseUri(Path.BASE_URI);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENT));
        resPec = restUtilities.getResponseSpecification();

        NewPatient patientData = new NewPatient();

        List<CustomAtributs> customAttributes = new ArrayList<>();

        CustomAtributs first = new CustomAtributs();
        first.setId(1);
        first.setName("Test custom attributes");
        first.setType("boolean");
        first.setRequired(true);
        first.setPatientsetup(true);
        first.set_class("col-xs-3");
        first.setEmrTracked(false);
        first.setLastUpdated("2017-08-31T11:39:00+0000");
        first.set$$hashKey("object:2415");
        first.setValue("true");

        CustomAtributs two = new CustomAtributs();
        two.setId(2);
        two.setName("Nickname");
        two.setType("text");
        two.setRequired(false);
        two.setPatientsetup(true);
        two.set_class("col-xs-6");
        two.setEmrTracked(false);
        two.setLastUpdated("2017-09-25T14:22:07+0000");
        two.set$$hashKey("object:2416");
        two.setValue("testnick");

        CustomAtributs three = new CustomAtributs();
        three.setId(4);
        three.setName("Custom Text Field");
        three.setType("text");
        three.setRequired(true);
        three.setPatientsetup(true);
        three.set_class("col-xs-12");
        three.setEmrTracked(false);
        three.setLastUpdated("2017-10-16T14:44:33+0000");
        three.set$$hashKey("object:2417");
        three.setValue("test custom field");

        customAttributes.add(first);
        customAttributes.add(two);
        customAttributes.add(three);
        patientData.setCustomAtributsList(customAttributes);

        Name name = new Name(
                Auth.PATIENT_FIRST_NAME_INTG,
                Auth.PATIENT_MIDDLE_NAME_INTG,
                Auth.PATIENT_LAST_NAME_INTG
        );

        patientData.setName(name);

        patientData.setDob("01/01/1990");
        patientData.setGender("F");
        patientData.setPhone("9082237696");
        patientData.setPid(Auth.PATIENT_PID);
        reqSpec.body(patientData);

        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint();
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        Response response = restUtilities.getResponse(reqSpec, "post");

        JsonPath jsonPath = response.jsonPath();
        return jsonPath.get("hrsid");
    }

    private static void setModulesToPatientById(String patientId) {
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        String date = RandomData.getTimePeriod(25, dateFormat).get("plusDays");

        restUtilities = new RestUtilities();

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));
        resPec = restUtilities.getResponseSpecification();
//        reqSpec.log().all();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.MODULE_INFO));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        NewMetricks newMetrick = new NewMetricks();
        ModuleInfo info = new ModuleInfo();

        List<String> modulesList = new ArrayList<>();
        modulesList.add(Modules.HELTHY);
        modulesList.add(Modules.CHF);
        modulesList.add(Modules.COPD);
        modulesList.add(Modules.DIABETES);
        modulesList.add(Modules.ACTIVITY);
        modulesList.add(Modules.WEIGHT);
        modulesList.add(Modules.TEMPERATURE);
        modulesList.add(Modules.BLOOD_PRESSURE);
        modulesList.add(Modules.MEDICATION);
        modulesList.add(Modules.SURVEY);
        modulesList.add(Modules.PULSE_OX);
        modulesList.add(Modules.GLUCOSE);
        modulesList.add(Modules.WOUND_IMAGING);
        modulesList.add(Modules.PATIENT_CONNECT_VOICE);
        modulesList.add(Modules.STETHOSCOPE);
        modulesList.add(Modules.STEPS);
        info.setModules(modulesList);
        info.setActivityreminder(new Activityreminder("20", "-"));
        info.setSurveyreminder(new Surveyreminder("60", "600"));
        info.setWeightreminder(new Weightreminder("60", "540"));

        List<Medicationreminder> medicationreminderList = new ArrayList<>();

        Medicationreminder SULFAPYRIDINE = new Medicationreminder();
        SULFAPYRIDINE.setMedication("SULFAPYRIDINE");
        SULFAPYRIDINE.setDose("120 mg x1 Oral");
        SULFAPYRIDINE.setSchedule(new Schedule("undefined",
                "everyday",
                date,
                "test special instruction: ".concat(restUtilities.getRandomString())));
        SULFAPYRIDINE.setTimes("PRN");
        SULFAPYRIDINE.setTime("PRN");
        SULFAPYRIDINE.setWindow("30");
        medicationreminderList.add(SULFAPYRIDINE);

        Medicationreminder PERCORTEN = new Medicationreminder();
        PERCORTEN.setMedication("PERCORTEN");
        PERCORTEN.setDose("122 ml x1 Oral");
        PERCORTEN.setSchedule(new Schedule("undefined",
                "everyday",
                date,
                "test special instruction: ".concat(restUtilities.getRandomString())));
        PERCORTEN.setTimes("PRN");
        PERCORTEN.setTime("PRN");
        PERCORTEN.setWindow("30");
        medicationreminderList.add(PERCORTEN);

        Medicationreminder METANDREN = new Medicationreminder();
        METANDREN.setMedication("METANDREN");
        METANDREN.setDose("1 tblsp x2 Oral");
        METANDREN.setSchedule(new Schedule("undefined",
                "everyday",
                date,
                "test special instruction: ".concat(restUtilities.getRandomString())));
        METANDREN.setTimes("PRN");
        METANDREN.setTime("PRN");
        METANDREN.setWindow("30");
        medicationreminderList.add(METANDREN);

        Medicationreminder STILBETIN = new Medicationreminder();
        STILBETIN.setMedication("STILBETIN");
        STILBETIN.setDose("1 mg x1 Oral");
        STILBETIN.setSchedule(new Schedule("false", "everyday", "test special instruction: ".concat(restUtilities.getRandomString())));
        STILBETIN.setTimes("PRN");
        STILBETIN.setTime("PRN");
        STILBETIN.setWindow("30");
        medicationreminderList.add(STILBETIN);

        Medicationreminder everyXdaysReminder = new Medicationreminder();
        everyXdaysReminder.setMedication("LIQUAEMIN SODIUM");
        everyXdaysReminder.setDose("12 mg x1 Oral");
        Schedule scheduleEveryXdaysReminder = new Schedule();
        scheduleEveryXdaysReminder.setEssential("true");
        scheduleEveryXdaysReminder.setType("everyxdays");
        scheduleEveryXdaysReminder.setX(restUtilities.getRandomInt(1, 7).toString());
        scheduleEveryXdaysReminder.setStartday(String.valueOf(restUtilities.getCurrentTimeStamp()));
        scheduleEveryXdaysReminder.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleEveryXdaysReminder.setExpiration(date);
        everyXdaysReminder.setSchedule(scheduleEveryXdaysReminder);
        everyXdaysReminder.setTime("540");
        everyXdaysReminder.setTimes("9:00AM");
        everyXdaysReminder.setWindow("60");
        medicationreminderList.add(everyXdaysReminder);

        Medicationreminder customDaysRemider = new Medicationreminder();
        customDaysRemider.setMedication("ALCOHOL 10% AND DEXTROSE 5%");
        customDaysRemider.setDose("200 mg x2 Oral");
        Schedule scheduleCustomDaysRemider = new Schedule();
        scheduleCustomDaysRemider.setEssential("true");
        scheduleCustomDaysRemider.setType("custom");
        scheduleCustomDaysRemider.setSchedule(Modules.customMedicationsDays);
        scheduleCustomDaysRemider.setInstruction("Test instruction: ".concat(restUtilities.getRandomString()));
        scheduleCustomDaysRemider.setExpiration(date);
        customDaysRemider.setSchedule(scheduleCustomDaysRemider);
        customDaysRemider.setTime("540");
        customDaysRemider.setTimes("9:00AM");
        customDaysRemider.setWindow("60");
        medicationreminderList.add(customDaysRemider);

        info.setMedicationreminders(medicationreminderList);
        newMetrick.setModuleInfo(info);

        reqSpec.body(newMetrick);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
        if (response.statusCode() == 200) {
            System.out.println("Set metrics to patient: " + patientId);
        } else {
            System.out.println("Something wrong! Please check!");
        }
    }

    private static void setRisksToPatientById(String patientId) {
        propertyWorker = new PropertyWorker(Connection.AUTH_PROP);

        restUtilities = new RestUtilities();

        restUtilities.setBaseUri(Path.BASE_URI_APP_INTEGRATION);
        reqSpec = restUtilities.getRequestSpecification();
        reqSpec.basePath(Path.API_V2.concat(Path.PATIENTS));
//        reqSpec.log().all();
        resPec = restUtilities.getResponseSpecification();
        restUtilities.setContentType(ContentType.JSON);
        restUtilities.setEndPoint(EndPoints.ID.concat(EndPoints.RISK));
        reqSpec.header("Authorization", "Bearer ".concat(propertyWorker.getProperty("clinician_intg_token")));

        String bodyReq = "{\n" +
                "    \"settings\": {\n" +
                "        \"riskdata\": [\n" +
                "            {\n" +
                "                \"type\": \"activity_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastolicgreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 100\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_heartrategreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 150\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"glucose_bloodsugargreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 120\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"medication_missedessential\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"pulseox_heartrategreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": 80\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"pulseox_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"survey_checkanswers\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"survey_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"temperature_temperaturegreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 100\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"survey_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_decreasexlbsfromstart\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 120\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_decreasexlbsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 10\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_noreadingsin1day\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"weight_decreasexlbsfromstart\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ],\n" +
                "                \"value\": 90\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastoliclessthan\",\n" +
                "                \"alerttype\": \"highrisk\",\n" +
                "                \"value\": \"40\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_systolicgreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": \"200\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"pulseox_spo2lessthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": \"90\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastolicgreaterthan\",\n" +
                "                \"alerttype\": \"highrisk\",\n" +
                "                \"value\": \"160\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            },\n" +
                "            {\n" +
                "                \"type\": \"bloodpressure_diastolicgreaterthan\",\n" +
                "                \"alerttype\": \"medrisk\",\n" +
                "                \"value\": \"140\",\n" +
                "                \"events\": [\n" +
                "                    \"email\",\n" +
                "                    \"text\",\n" +
                "                    \"patientdevice\"\n" +
                "                ]\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}";

        reqSpec.body(bodyReq);
        Response response = restUtilities.getResponse(
                restUtilities.createPathParam(reqSpec, "id", patientId), "post");
        if (response.statusCode() == 200) {
            System.out.println("Set risks to patient: " + patientId);
        } else {
            System.out.println("Something wrong! Please check!");
        }
    }
}

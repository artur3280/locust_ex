package core;

import com.sun.media.jfxmedia.logging.Logger;

import java.io.*;
import java.util.Properties;

public class PropertyWorker {
    private Properties properties;
    private FileInputStream file = null;
    private FileOutputStream outputStream = null;
    private String path;

    public PropertyWorker(String propertiesPatch) {
        properties = new Properties();
        path = propertiesPatch;
        try {
            if(new File(path).exists()){
                file = new FileInputStream(path);
                properties.load(file);
                file.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Properties getProperties() {
        return properties;
    }

    public String getProperty(String name){
        return getProperties().getProperty(name);
    }

    public void setProperty(String name, String value) throws IOException {
        if (new File(path).exists()) {
            setDataToProp(name, value);
        }else {
            File file = new File(path);
            file.createNewFile();
            setDataToProp(name, value);
        }

    }

    private void setDataToProp(String name, String value) throws IOException {
        outputStream = new FileOutputStream(path);
        getProperties().setProperty(name, value);
        getProperties().store(outputStream, null);
    }

    public void removeAllProperty(){
        File file = new File(path);
        file.deleteOnExit();
        System.out.println("File property is deleted");
    }


}

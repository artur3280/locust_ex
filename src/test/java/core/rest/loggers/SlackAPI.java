package core.rest.loggers;

import com.ullink.slack.simpleslackapi.ChannelHistoryModule;
import com.ullink.slack.simpleslackapi.SlackChannel;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.events.SlackMessagePosted;
import com.ullink.slack.simpleslackapi.impl.ChannelHistoryModuleFactory;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
import core.rest.configs.Configurations;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.TestNG;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SlackAPI {
    private static TestNG testNG;
    private static Logger logger;
    private static SlackSession session;
    private static String authToken = Configurations.getConfigParam("authToken");
    private static String chanelName = Configurations.getConfigParam("chanelName");

//    private static String authToken = "xoxp-271610363575-296557325894-351466871222-4597c046dfc84ae82e70ea5e9f262763";
//    private static String chanelName = "api_tests";

//    private static String authToken = "xoxp-555571100247-553930412833-554827751573-4a80752c41f13accb853de86c96ac574";
//    private static String chanelName = "chanel_for_testing";


//    public static void main(String[] args) throws IOException {
//        SlackAPI slackAPI = new SlackAPI();
//        slackAPI.connection();
//        session.getUsers().forEach(channel->{
//            System.out.println(channel.getId()+" ====== "+channel.getUserName()+ "   " + channel.getRealName().toString());
//        });
//
//        session.getBots().forEach(channel->{
//            System.out.println(channel.getId()+" ====== "+channel.getUserName()+ "   " + channel.getRealName().toString());
//        });
//
//
//        session.sendTyping(getChannel());
//        session.sendFile(getChannel(), "test".getBytes(),"test");
////        slackAPI.deleteAllMessages();
//
//    }

    public SlackAPI(TestNG test, Logger log) {
        testNG = test;
        logger = log;

    }

    public SlackAPI() {

    }

    public void send() throws IOException {
        connection();

        List<ITestContext> context = logger.getTestContexts();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss yyyy/MM/dd");
        Date start = context.get(0).getStartDate();
        Date end = context.get(0).getEndDate();

        List all = context.get(0).getSuite().getAllMethods();
        List skipped = getAllSkipTests();
        List passed = getAllPassTests();
        List failed = getAllFailTests();

        String firstDividingLine = "|===============================================|\n".
                concat("*Test name:* ".concat(context.get(0).getName()));
        String secondDividingLine = "\n |===============================================|";
        String failedTestsReport = logger.newReport("Failed ", getAllFailTests());
        String skippedTestsReport = logger.newReport("Skipped ", getAllSkipTests());
        String overData = "[ _All tests:_ " + all.size()
                + "  _Passed_ = " + passed.size()
                + "  _Failed_ = " + failed.size()
                + "  _Skipped_ = " + skipped.size() + " ] \n\n";
        String startTime = "\n*Start:* ".concat(dateFormat.format(start));
        String endTime = "\n*End:* ".concat(dateFormat.format(end));
        String timeStamp = "\n*TimeStamp:* "
                .concat(String.valueOf((float) (end.getTime() - start.getTime()) / 1000)) + "(s)\n\n";
        String urlSiute = "\n*Test case suit: *" + logger.testRailApi.dataSuit.getUrl();
        String urlReport = "\n*Detail TestRail report: *" + logger.testRailApi.dataRun.getUrl();

        session.sendMessage(getChannel()
                , firstDividingLine +
                        failedTestsReport +
                        skippedTestsReport +
                        startTime +
                        endTime +
                        timeStamp +
                        overData +
                        urlSiute +
                        urlReport +
                        secondDividingLine);
    }

    private List<ITestNGMethod> getAllSkipTests() {
        List<ITestNGMethod> newLis = new ArrayList<>();
        logger.getTestContexts().forEach(iTestContext -> {
            newLis.addAll(iTestContext.getSkippedTests().getAllMethods());
        });
        return newLis;
    }

    private List<ITestNGMethod> getAllPassTests() {
        List<ITestNGMethod> newLis = new ArrayList<>();
        logger.getTestContexts().forEach(iTestContext -> {
            newLis.addAll(iTestContext.getPassedTests().getAllMethods());
        });
        return newLis;
    }

    private List<ITestNGMethod> getAllFailTests() {
        List<ITestNGMethod> newLis = new ArrayList<>();
        logger.getTestContexts().forEach(iTestContext -> {
            newLis.addAll(iTestContext.getFailedTests().getAllMethods());
        });
        return newLis;
    }

    public void sendFile() throws IOException {
        connection();
        SlackChannel channel = getChannel();
        byte[] data;
        try (BufferedInputStream stream =
                     new BufferedInputStream(new FileInputStream("[file path]"))) {
            data = new byte[(char) stream.available()];
        }
        session.sendFile(channel, data, "index.html", "test title", "test comment");
    }

    private void connection() throws IOException {
        session = SlackSessionFactory.createWebSocketSlackSession(authToken);
        session.connect();
    }

    private static SlackChannel getChannel() {
        return session.findChannelByName(chanelName);
    }

    public SlackSession getSession() {
        return session;
    }

    public void deleteAllMessages() {
        ChannelHistoryModule channelHistoryModule = ChannelHistoryModuleFactory.createChannelHistoryModule(session);
        List<SlackMessagePosted> messages = channelHistoryModule.fetchHistoryOfChannel(getChannel().getId());
        messages.forEach((message) -> {
            session.deleteMessage(message.getTimeStamp(), getChannel());
        });
    }
}

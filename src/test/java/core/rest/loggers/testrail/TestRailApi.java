package core.rest.loggers.testrail;

import com.codepine.api.testrail.TestRail;
import com.codepine.api.testrail.model.*;

import com.hrs.cc.api.constans.Auth;
import com.hrs.cc.api.constans.Path;
import core.rest.configs.Configurations;
import me.tongfei.progressbar.ProgressBar;
import org.testng.ISuite;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class TestRailApi {
    final static String HOST = Configurations.getConfigParam("host_tr");
    final static String USER_NAME = Configurations.getConfigParam("user_name_tr");
    final static String PASSWORD = Configurations.getConfigParam("password_tr");
    public Suite dataSuit;
    public Project dataProject;
    public Run dataRun;
    private TestRail testRail;
    private static List<CaseField> customCaseFields;
    private static List<String> testMethods = new ArrayList<>();
    private static Run run;
    private static List<ResultField> customResultFields;
    private static Integer projectId;
    private static Integer suiteId;
    private List<Case> caseList;

//    public static void main(String[] args) {
//        TestRailApi api = new TestRailApi("ClinicianConnect.V2", "AutomationApiTests");
//        List<Section> sections = api.testRail.sections().list(projectId, suiteId).execute();
//
//        System.out.println(api.getCaseList().size());
//        List<String> list = api.getCaseList();
//        list.stream().filter(i -> Collections.frequency(list, i) >1)
//                .collect(Collectors.toSet()).forEach(System.out::println);
//    }


    public TestRailApi(String appName, String suiteName) {
        connectionToApi(appName);
        List<Project> prj = testRail.projects().list().execute();
        Project project = prj.stream().filter(p -> p.getName().equals(appName)).collect(Collectors.toList()).get(0);
        dataProject = project;
        projectId = project.getId();
        System.out.println("Set data to project by id: " + projectId + "(" + project.getName() + ")\n");
        Suite suite = testRail.suites().list(projectId).execute().stream().filter(s -> s.getName().equals(suiteName)).collect(Collectors.toList()).get(0);
        suiteId = suite.getId();
        dataSuit = suite;
    }

    public void connectionToApi(String appName) {
        TestRail.Builder testRailBulder = TestRail.builder(HOST, USER_NAME, PASSWORD);
        testRail = testRailBulder.applicationName(appName).build();
        customCaseFields = testRail.caseFields().list().execute();
        customResultFields = testRail.resultFields().list().execute();
    }

    private void setAllTestMethods(ISuite suite) {
        suite.getAllMethods().forEach(iTestNGMethod -> {
            String pack = iTestNGMethod.getRealClass().getCanonicalName();
            testMethods.add(pack + "." + iTestNGMethod.getMethodName());
        });
//        for (ITestNGMethod o : methods) {
//            String pack = o.getRealClass().getCanonicalName();
//            testMethods.add(pack + "." + o.getMethodName());
//        }
    }

    public void addNewTestCases(ISuite suite) {
        createSection(suite);
        setAllTestMethods(suite);
        List<Section> listAlreadySections = testRail.sections().list(projectId, suiteId).execute();

        System.out.println("\nCreate/update test cases on sections:");
        try (ProgressBar progressBar = new ProgressBar(" \r ",
                listAlreadySections.size())) {
            for (Section section : listAlreadySections) {
                progressBar.step();
                for (String item : Objects.requireNonNull(getDifferentCases())) {
                    if (item.contains(section.getName())) {
                        testRail.cases().add(section.getId(),
                                new Case().setSuiteId(suiteId).setTitle(item), customCaseFields).execute();
                    }
                }
            }
        }
        caseList = testRail.cases().list(projectId, suiteId, customCaseFields).execute();

    }

    public void createSection(ISuite suite) {
        List<Section> listSection = testRail.sections().list(projectId, suiteId).execute();

        for (String className : getClassesName(suite.getAllMethods())) {
            if (!listSection.toString().contains(className)) {
                testRail.sections().add(projectId, new Section().setSuiteId(suiteId)
                        .setName(className)).execute();
            }
        }
    }

    public Set<String> getClassesName(List<ITestNGMethod> methods) {
        Set<String> setData = new HashSet<>();
        for (ITestNGMethod o : methods) {
            setData.add(o.getRealClass().getSimpleName());
        }
        return setData;
    }

    private List<String> getCaseList() {
        List<String> list = new ArrayList<>();
        testRail.cases().list(projectId, suiteId, customCaseFields).execute().forEach(item -> {

            list.add(item.getTitle());
        });
        return list;
    }

    private List<String> getDifferentCases() {
        List<String> list = new ArrayList<>();

        list.addAll(testMethods);
        list.addAll(getCaseList());

        Set<String> set = new HashSet<>();
        List<String> result = new ArrayList<>(list);

        list.stream().filter((i) -> (!set.add(i))).forEachOrdered((i) -> {
            result.removeAll(Collections.singleton(i));
        });
        return result;
    }

//    public static void main(String[] args) {
//        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//        SimpleDateFormat formForFilter = new SimpleDateFormat("dd/MM/yyyy");
//        Date date = new Date();
//
//        System.out.println(formatter.format(date));
//        System.out.println(formForFilter.format(date));
//        TestRailApi api = new TestRailApi("ClinicianConnect.V2", "AutomationApiTests");
//        List<Run> list = api.testRail.runs().list(projectId).execute();
//        List<Run> item = list.stream().filter(l ->
//                l.getName().contains(formForFilter.format(date)) &&
//                l.getName().contains(Path.BASE_URI)
//
//        ).collect(Collectors.toList());
//        item.forEach(i->{
//            System.out.println(i.getName());
//        });
//    }

    public void createRun(String name, String typeTests, String url) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat formForFilter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();

        List<Run> runFiltered = testRail.runs().
                list(projectId).execute().
                stream().filter(
                r -> r.getName().contains(Path.BASE_URI) &&
                        r.getName().contains(formForFilter.format(date)))
                .collect(Collectors.toList());
        if (runFiltered.size() > 0) {
            testRail.runs().delete(runFiltered.get(0).getId()).execute();
        }

        run = testRail.runs().add(projectId, new Run().setSuiteId(suiteId)
                .setDescription("Test environment: ".concat(url)+ "\n Integration patient: " + Auth.PATIENT_FIRST_NAME_INTG + " " +Auth.PATIENT_LAST_NAME_INTG)
                .setName(name.concat(formatter.format(date))))
                .execute();
        dataRun = run;
    }

    /*public void runCases(List<ITestResult> result, Integer status, String statusLine,Map<String, Map<String, StringWriter>> dataDebag) {
        System.out.println("\nSend to TestRail[" + statusLine + "]:");
        try (ProgressBar pb = new ProgressBar("\r", result.size())) {
            for (Case caseT : caseList) {
                for (ITestResult item : result) {
                    String testName = item.getTestClass().getRealClass().getCanonicalName() + "." + item.getName();
                    if (testName.equals(caseT.getTitle())) {

                        Throwable exception = item.getThrowable();
                        if (exception != null) {
                            StringWriter sw = new StringWriter();
                            PrintWriter pw = new PrintWriter(sw);
                            exception.printStackTrace(pw);
                            String exceptionMessage = sw.toString();

                            String commentLine = "requests:\n".toUpperCase()+
                                    dataDebag.get(item.getName()).get("requests")+
                                    "\n\nresponses\n".toUpperCase()+
                                    dataDebag.get(item.getName()).get("responses")+
                                    "\n\nexception Message\n"+
                                    exceptionMessage;
                            Result resul = new Result();

                            resul.setStatusId(status);
                            resul.setComment(commentLine);

                            testRail.results().addForCase(run.getId(), caseT.getId(), resul, customResultFields).execute();
                            pb.step();
                        } else {
                            testRail.results().addForCase(run.getId(), caseT.getId(), new Result().setStatusId(status)
                                            .setComment(""),
                                    customResultFields).
                                    execute();
                            pb.step();
                        }
                    }

                }
            }
        }
    } */


    public void runCases(List<ITestResult> result, Integer status, String statusLine, Map<String, Map<String, StringWriter>> dataDebag) {
        ArrayList<Result> results = new ArrayList<>();
        System.out.println("\nSend to TestRail[" + statusLine + "]:");
        try (ProgressBar pb = new ProgressBar("\r", result.size())) {
            for (Case caseT : caseList) {
                for (ITestResult item : result) {
                    String testName = item.getTestClass().getRealClass().getCanonicalName() + "." + item.getName();
                    if (testName.equals(caseT.getTitle())) {

                        Throwable exception = item.getThrowable();
                        if (exception != null) {
                            StringWriter sw = new StringWriter();
                            PrintWriter pw = new PrintWriter(sw);
                            exception.printStackTrace(pw);
                            String exceptionMessage = sw.toString();

                            String commentLine;
                            try {
                                commentLine = "requests:\n".toUpperCase() +
                                        dataDebag.get(item.getName())
                                                .get("requests") +
                                        "\n\nresponses\n".toUpperCase() +
                                        dataDebag.get(item.getName()).get("responses") +
                                        "\n\nexception Message\n" +
                                        exceptionMessage;
                            } catch (NullPointerException e) {
                                commentLine = "\n\nexception Message\n" + exceptionMessage;
                            }

                            Result resul = new Result();
                            resul.setCaseId(caseT.getId());
                            resul.setStatusId(status);
                            resul.setComment(commentLine);
                            results.add(resul);
                            pb.step();

                        } else {
                            String commentLine;
                            try {
                                commentLine = "requests:\n".toUpperCase() +
                                        dataDebag.get(item.getName())
                                                .get("requests");
                            } catch (NullPointerException e) {
                                commentLine = "Data not found!";
                            }

                            Result resul = new Result();
                            resul.setCaseId(caseT.getId());
                            resul.setStatusId(status);
                            resul.setComment(commentLine);
                            results.add(resul);
                            pb.step();
                        }
                    }

                }
            }
        }
        if (!result.isEmpty()) {
            testRail.results().addForCases(run.getId(), results, customResultFields).execute();
        }
    }

 /*   public void runCases(List<ITestResult> result, Integer status, String statusLine) {
        System.out.println("\nSend to TestRail[" + statusLine + "]:");
        try (ProgressBar pb = new ProgressBar("\r", result.size())) {
            for (Case caseT : caseList) {
                for (ITestResult item : result) {
                    String testName = item.getTestClass().getRealClass().getCanonicalName() + "." + item.getName();
                    if (testName.equals(caseT.getTitle())) {

                        Throwable exception = item.getThrowable();
                        if (exception != null) {
                            StringWriter sw = new StringWriter();
                            PrintWriter pw = new PrintWriter(sw);
                            exception.printStackTrace(pw);

                            Result resul = new Result();
                            resul.setStatusId(status);
                            resul.setComment(sw.toString());

                            testRail.results().addForCase(run.getId(), caseT.getId(), resul, customResultFields).execute();
                            pb.step();
                        } else {
                            testRail.results().addForCase(run.getId(), caseT.getId(), new Result().setStatusId(status)
                                            .setComment(""),
                                    customResultFields).
                                    execute();
                            pb.step();
                        }
                    }

                }
            }
        }
    } */

    public void runCases(List<ITestResult> result, Integer status, String statusLine) {
        ArrayList<Result> results = new ArrayList<>();
        System.out.println("\nSend to TestRail[" + statusLine + "]:");
        try (ProgressBar pb = new ProgressBar("\r", result.size())) {
            for (Case caseT : caseList) {
                for (ITestResult item : result) {
                    String testName = item.getTestClass().getRealClass().getCanonicalName() + "." + item.getName();
                    if (testName.equals(caseT.getTitle())) {

                        Throwable exception = item.getThrowable();
                        if (exception != null) {
                            StringWriter sw = new StringWriter();
                            PrintWriter pw = new PrintWriter(sw);
                            exception.printStackTrace(pw);

                            Result resul = new Result();
                            resul.setCaseId(caseT.getId());
                            resul.setStatusId(status);
                            resul.setComment(sw.toString());
                            results.add(resul);
                            pb.step();

                        } else {

                            Result resul = new Result();
                            resul.setCaseId(caseT.getId());
                            resul.setStatusId(status);
                            resul.setComment("");
                            results.add(resul);
                            pb.step();
                        }
                    }

                }
            }
        }
        if (!result.isEmpty()) {
            testRail.results().addForCases(run.getId(), results, customResultFields).execute();
        }
    }

    public void closeRun() {
        testRail.runs().close(run.getId()).execute();
    }

    public Set<String> searchDuplicatesOfCases(List<String> list) {
        return list.stream().filter(i -> Collections.frequency(list, i) > 1)
                .collect(Collectors.toSet());
    }
}


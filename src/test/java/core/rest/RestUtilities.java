package core.rest;

import core.rest.auth.schemes.AuthScheme;
import core.rest.loggers.Logger;
import io.restassured.RestAssured;
import io.restassured.authentication.FormAuthScheme;
import io.restassured.authentication.OAuth2Scheme;
import io.restassured.authentication.OAuthScheme;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.builder.MultiPartSpecBuilder;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.config.DecoderConfig;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.http.Headers;
import io.restassured.http.Method;
import io.restassured.internal.RequestSpecificationImpl;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.PrintStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static io.restassured.config.EncoderConfig.encoderConfig;
import static io.restassured.config.ParamConfig.UpdateStrategy.MERGE;
import static io.restassured.config.ParamConfig.UpdateStrategy.REPLACE;
import static io.restassured.config.ParamConfig.paramConfig;
import static org.hamcrest.Matchers.lessThan;

public class RestUtilities {
    private String BASE_URI;
    private String ENDPOINT = "";
    private RequestSpecBuilder REQUEST_BUILDER;
    private RequestSpecification REQUEST_SPEC;
    private ResponseSpecBuilder RESPONSE_BUILDER;
    private ResponseSpecification RESPONSE_SPEC;
    private RequestSpecificationImpl requestSpecification;

    public void setEndPoint(String epoint) {
        ENDPOINT = epoint;
    }

    public void setEndPoint() {
        ENDPOINT = "";
    }

    public ResponseSpecBuilder getResponseBuilder() {
        return RESPONSE_BUILDER;
    }

    public void setBaseUri(String baseUri) {
        BASE_URI = baseUri;
    }

    public RequestSpecification getRequestSpecification(String apikey,
                                                        String secretApikey,
                                                        String tokenKey,
                                                        String secretTokenKey) {
        OAuthScheme authenticationScheme = AuthScheme.oAuthScheme(apikey, secretApikey, tokenKey, secretTokenKey);
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
        REQUEST_BUILDER.setAuth(authenticationScheme);
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public RequestSpecification getRequestSpecification(String userName, String password) {
        PreemptiveBasicAuthScheme authBasic = AuthScheme.basicScheme(userName, password);
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
        REQUEST_BUILDER.setAuth(authBasic);
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public RequestSpecification getRequestSpecificationInBody(Map<String, String> queryMap) {
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
        REQUEST_BUILDER.setBody(queryMap);
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public RequestSpecification getRequestSpecificationInBody() {
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public RequestSpecification getRequestSpecificationMultipartForm(String userName, String pass) {
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
        REQUEST_BUILDER.addMultiPart("UserName", userName);
        REQUEST_BUILDER.addMultiPart("Password", pass);

        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public RequestSpecification getRequestSpecificationInBody(String queryMap) {
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
        REQUEST_BUILDER.setBody(queryMap);
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public RequestSpecification getRequestSpecification() {
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
//        REQUEST_BUILDER.setBody(queryMap);
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public RequestSpecification getRequestSpecification(Boolean EncodingEnabled) {
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
//        REQUEST_BUILDER.setBody(queryMap);
        REQUEST_BUILDER.setUrlEncodingEnabled(EncodingEnabled);
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public RequestSpecification getRequestSpecification(RequestSpecBuilder builder) {
        REQUEST_SPEC = builder.build();
        return REQUEST_SPEC;
    }

    public ResponseSpecification getResponseSpecification(ResponseSpecBuilder builder){
        RESPONSE_SPEC = builder.build();
        return RESPONSE_SPEC;
    }

    public RequestSpecBuilder createRequestBuilder(){
        return  new RequestSpecBuilder();
    }

    public ResponseSpecBuilder createResponseBuilder(){
        return  new ResponseSpecBuilder();
    }

    public void addFormData(Map<String, String> formData) {
        for (Map.Entry<String, String> entry : formData.entrySet()) {
            REQUEST_BUILDER.addMultiPart(entry.getKey(), entry.getValue());
        }

    }

    public RequestSpecification getRequestSpecification(String token) {
        OAuth2Scheme authBasic = AuthScheme.oAuth2Scheme(token);

        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
        REQUEST_BUILDER.setAuth(authBasic);
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public RequestSpecification getRequestSpecificationWithForm(String userName, String password, String action, String userNameTag, String passwordTag) {
        FormAuthScheme formAuth = AuthScheme.formAuthScheme(userName, password, action, userNameTag, passwordTag);
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
        REQUEST_BUILDER.setAuth(formAuth);
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public RequestSpecification getRequestSpecificationWithForm(String userName, String password) {
        FormAuthScheme formAuth = AuthScheme.formAuthScheme(userName, password);
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
        REQUEST_BUILDER.setAuth(formAuth);
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public ResponseSpecification getResponseSpecification() {
        RESPONSE_BUILDER = new ResponseSpecBuilder();
        RESPONSE_BUILDER.expectStatusCode(200);
        RESPONSE_BUILDER.expectContentType(ContentType.JSON);
        RESPONSE_BUILDER.expectResponseTime(lessThan(90L), TimeUnit.SECONDS);
        RESPONSE_SPEC = RESPONSE_BUILDER.build();
        return RESPONSE_SPEC;
    }

    public RequestSpecification createQueryParam(RequestSpecification rspec,
                                                 String param, String value) {
        return rspec.queryParam(param, value);
    }

    public RequestSpecification createQueryParams(RequestSpecification rspec,
                                                 String param, Object... value) {
        return rspec.queryParam(param, value);
    }

    public RequestSpecification createQueryParam(RequestSpecification rspec,
                                                 Map<String, String> queryMap) {
        return rspec.queryParams(queryMap);
    }

    public RequestSpecification createPathParam(RequestSpecification rspec,
                                                String param, String value) {
        return rspec.pathParam(param, value);
    }

    public RequestSpecification createPathParam(RequestSpecification rspec,
                                                Map<String, String> queryMap) {
        return rspec.pathParams(queryMap);
    }

    public Response getResponse() {
        if(Logger.isDebug()) REQUEST_SPEC.log().all();
        return given().get(ENDPOINT);
    }

    public Response getResponse(RequestSpecification reqSpec,ResponseSpecification resSpec, String type) {
        if (ENDPOINT == null) {
            setEndPoint("");
        }

        REQUEST_SPEC.spec(reqSpec);
        if(Logger.isDebug()) REQUEST_SPEC.log().all();

        String name = new Exception().getStackTrace()[1].getMethodName();
        StringWriter requestWriter = new StringWriter();
        PrintStream requestCupture = new PrintStream(new WriterOutputStream(requestWriter, Charset.defaultCharset()), true);
        StringWriter responseWriter = new StringWriter();
        PrintStream responseCupture = new PrintStream(new WriterOutputStream(responseWriter, Charset.defaultCharset()), true);

        if(requestCupture != null) {
            REQUEST_SPEC.filters(new RequestLoggingFilter(requestCupture),new ResponseLoggingFilter(responseCupture));
//            REQUEST_SPEC.filter(new ResponseLoggingFilter(Logger.responseCupture));
        }

        Map<String,StringWriter> dataMap = new HashMap<>();
        dataMap.put("requests",requestWriter);
        dataMap.put("responses",responseWriter);
        Logger.setRecSpecMap(name,dataMap);

        Response response = null;
        if (type.equalsIgnoreCase("get")) {
            response = given().spec(REQUEST_SPEC).request(Method.GET, ENDPOINT);
        } else if (type.equalsIgnoreCase("post")) {
            response = given()
                    .spec(REQUEST_SPEC)
                    .request(Method.POST, ENDPOINT);
        } else if (type.equalsIgnoreCase("put")) {
            response = given().spec(REQUEST_SPEC).request(Method.PUT, ENDPOINT);
        } else if (type.equalsIgnoreCase("delete")) {
            response = given().spec(REQUEST_SPEC).request(Method.DELETE, ENDPOINT);
        } else if (type.equalsIgnoreCase("patch")) {
            response = given().spec(REQUEST_SPEC).request(Method.PATCH, ENDPOINT);
        } else {
            System.out.println("Type is not supported");
        }

        if(Logger.isDebug())response.then().log().all();

        response.then().spec(resSpec);
        return response;
    }

    public Response getResponse(RequestSpecification reqSpec, String type) {
        if (ENDPOINT == null) {
            setEndPoint("");
        }
        if(Logger.isDebug()) REQUEST_SPEC.log().all();

        REQUEST_SPEC.spec(reqSpec);
        Response response = null;
        String name = new Exception().getStackTrace()[1].getMethodName();
        StringWriter requestWriter = new StringWriter();
        PrintStream requestCupture = new PrintStream(new WriterOutputStream(requestWriter, Charset.defaultCharset()), true);
        StringWriter responseWriter = new StringWriter();
        PrintStream responseCupture = new PrintStream(new WriterOutputStream(responseWriter, Charset.defaultCharset()), true);

        if(requestCupture != null) {
            REQUEST_SPEC.filters(new RequestLoggingFilter(requestCupture),new ResponseLoggingFilter(responseCupture));
//            REQUEST_SPEC.filter(new ResponseLoggingFilter(Logger.responseCupture));
        }

        Map<String, StringWriter> dataMap = new HashMap<>();
        dataMap.put("requests",requestWriter);
        dataMap.put("responses",responseWriter);
        Logger.setRecSpecMap(name,dataMap);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (type.equalsIgnoreCase("get")) {
            response = given().spec(REQUEST_SPEC).request(Method.GET, ENDPOINT);
        } else if (type.equalsIgnoreCase("post")) {
            response = given()
                    .spec(REQUEST_SPEC)
                    .request(Method.POST, ENDPOINT);
        } else if (type.equalsIgnoreCase("put")) {
            response = given().spec(REQUEST_SPEC).request(Method.PUT, ENDPOINT);
        } else if (type.equalsIgnoreCase("delete")) {
            response = given().spec(REQUEST_SPEC).request(Method.DELETE, ENDPOINT);
        } else if (type.equalsIgnoreCase("patch")) {
            response = given().spec(REQUEST_SPEC).request(Method.PATCH, ENDPOINT);
        } else {
            System.out.println("Type is not supported");
        }

        if(Logger.isDebug())response.then().log().all();

        response.then().spec(RESPONSE_SPEC);
        requestCupture.close();
        responseCupture.close();
        return response;
    }

    public JsonPath getJsonPath(Response res) {
        String path = res.asString();
        return new JsonPath(path);
    }

    public XmlPath getXmlPath(Response res) {
        String path = res.asString();
        return new XmlPath(path);
    }

    public void resetBathPath() {
        RestAssured.basePath = null;
    }

    public RequestSpecification setContentType(ContentType type) {
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
        REQUEST_BUILDER.setContentType(type);
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public RequestSpecification setContentType(String type) {
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
        REQUEST_BUILDER.setContentType(type);
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }

    public void configDecode(RequestSpecification reqSpec, Boolean status) {
        reqSpec.config(
                config().decoderConfig(
                        DecoderConfig
                                .decoderConfig()
                                .useNoWrapForInflateDecoding(status)
                )
        );
    }

    public void configEncode(RequestSpecification reqSpec, Boolean status) {
    }

    public void turnOffContentDecode(RequestSpecification reqSpec) {
        reqSpec.config(config().decoderConfig(DecoderConfig.decoderConfig().noContentDecoders()));
    }

    /**
     * @param reqSpec
     * @wiki https://github.com/rest-assured/rest-assured/wiki/usage#path-parameters
     */
    public void resetQueryPath(RequestSpecification reqSpec) {
        reqSpec.config(config().paramConfig(paramConfig().replaceAllParameters()));
    }


    /**
     * @param reqSpec
     * @wiki https://github.com/rest-assured/rest-assured/wiki/usage#path-parameters
     */
    public void replyQueryPath(RequestSpecification reqSpec) {
        reqSpec.config(config().paramConfig(paramConfig().queryParamsUpdateStrategy(REPLACE)));
    }

    public void margeQueryPath(RequestSpecification reqSpec) {
        reqSpec.config(config().paramConfig(paramConfig().queryParamsUpdateStrategy(MERGE)));
    }

    public void replyQueryPath2(RequestSpecification reqSpec, String name) {
        RequestSpecification spec = new RequestSpecBuilder().removeQueryParam(name).build();
        reqSpec.spec(spec);
    }

    public void removeQueryParam(RequestSpecification reqSpec, Map<String, String> map) {

        map.forEach((key, value) -> {
            requestSpecification = (RequestSpecificationImpl) reqSpec;
            requestSpecification.removeQueryParam(key);
        });
    }

  /*  public RequestSpecification uploadFile() throws IOException {
        REQUEST_BUILDER = new RequestSpecBuilder();
        REQUEST_BUILDER.setBaseUri(BASE_URI);
        REQUEST_BUILDER.addMultiPart(UploadFile.builder("test.txt",
                "test.txt",
                "/Users/qa_master/IdeaProjects/CR_REST_API/src/test/resources/uploads/test.txt",
                "text/plain"));
        REQUEST_SPEC = REQUEST_BUILDER.build();
        return REQUEST_SPEC;
    }*/


    public void uploadFile(RequestSpecification requestSpecification) {
        requestSpecification.config(config().encoderConfig(encoderConfig().encodeContentTypeAs(String.valueOf(ContentType.TEXT), ContentType.JSON)));
        requestSpecification.multiPart(
                new MultiPartSpecBuilder("{\"result\": \"OK\"}".getBytes())
                        .fileName("RoleBasedAccessFeaturePlan.txt")
                        .controlName("reference-data-file")
                        .mimeType("text/plain").build());

    }


    public void removeQueryParam(RequestSpecification reqSpec, String key) {
        requestSpecification = ((RequestSpecificationImpl) reqSpec);
        requestSpecification.removeQueryParam(key);
        requestSpecification.removeParam(key);
        requestSpecification.removeNamedPathParam(key);

    }


    public void removeHeaders(RequestSpecification reqSpec) {
        requestSpecification = (RequestSpecificationImpl) reqSpec;
        requestSpecification.removeHeaders();
    }

    public void removeHeaders(RequestSpecification reqSpec, String headerName) {
        requestSpecification = (RequestSpecificationImpl) reqSpec;
        requestSpecification.removeHeader(headerName);
    }

    public void replaceHeader(RequestSpecification reqSpec, String headerName, String key) {
        requestSpecification = (RequestSpecificationImpl) reqSpec;
        requestSpecification.replaceHeader(headerName,key);
    }
    public Headers getHeaders(RequestSpecification reqSpec) {
        requestSpecification = (RequestSpecificationImpl) reqSpec;
        return requestSpecification.getHeaders();
    }


    public void removePathParams(RequestSpecification reqSpec) {
        Map<String, String> map = requestSpecification.getPathParams();

        requestSpecification = (RequestSpecificationImpl) reqSpec;
        map.forEach((key, value) -> {
            requestSpecification.removePathParam(key);
        });
    }

    public void removePathParam(RequestSpecification reqSpec, String key) {
        requestSpecification = (RequestSpecificationImpl) reqSpec;
        Map<String, String> map = requestSpecification.getPathParams();
        requestSpecification.removePathParam(key);
    }

    public RequestSpecificationImpl getRequestSpecificationImpl(RequestSpecification reqSpec) {
        requestSpecification = (RequestSpecificationImpl) reqSpec;
        return requestSpecification;
    }


    public String getRandomString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }

    public String getRandomString(Integer i) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < i) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }

    public String getRandomChars(Integer s, Integer e){
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString().substring(s,e);
    }

    public Integer getRandomInt(Integer max) {
        return (int) (Math.random() * max);
    }

    public Integer getRandomInt(Integer min, Integer max) {
        Random random = new Random();
        return min + random.nextInt(max - min);
    }

    public Map<String, String> getTimePeriod(int days) {
        LocalDate localDate = LocalDate.now();

        Map<String, String> period = new HashMap<>();
        period.put("current", localDate.toString());
        period.put("plusDays", localDate.plusDays(days - 1).toString());
        period.put("minusDays", localDate.minusDays(days).toString());
        return period;
    }

    public Map<String, String> getTimePeriodTS(int days) {
        LocalTime localTime = LocalTime.now();
        LocalDate localDate = LocalDate.now();

        Map<String, String> period = new HashMap<>();
        period.put("current", localTime.atDate(localDate).withNano(0).withSecond(2).toString());
        period.put("plusDays", localTime.atDate(localDate).withNano(0).plusDays(days-1).withSecond(2).toString());
        period.put("minusDays", localTime.atDate(localDate).withNano(0).minusDays(days).withSecond(2).toString());
        return period;
    }

    public Object getRandomArrayItem(List arr) {
        int id = getRandomInt(0,arr.size()-1);
        return arr.get(id);
    }

    public long getCurrentTimeStamp(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        calendar.getTimeInMillis();

        return calendar.getTimeInMillis();
    }

    /*         RequestConfig.custom()
                .setConnectTimeout(5000)
                .setConnectionRequestTimeout(5000)
                .setSocketTimeout(5000)
                .build();*/

    public RestAssuredConfig setConfigs(RequestConfig requestConfig){

        HttpClientConfig httpClientFactory = HttpClientConfig.httpClientConfig()
                .httpClientFactory(() -> HttpClientBuilder.create()
                        .setDefaultRequestConfig(requestConfig)
                        .build());

       return RestAssured.config().
                httpClient(httpClientFactory);
    }
}

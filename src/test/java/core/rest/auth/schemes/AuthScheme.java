package core.rest.auth.schemes;

import io.restassured.authentication.*;

public class AuthScheme {
    private static PreemptiveBasicAuthScheme basicAuthScheme = new PreemptiveBasicAuthScheme();
    private static OAuthScheme oAuthScheme = new OAuthScheme();
    private static OAuth2Scheme oAuth2Scheme = new OAuth2Scheme();
    private static FormAuthScheme formAuthScheme = new FormAuthScheme();
    private static OAuthSignature signature = OAuthSignature.HEADER;
    private static FormAuthConfig formAuthConfig;

    public static PreemptiveBasicAuthScheme basicScheme(String userName, String password){
        basicAuthScheme.setUserName(userName);
        basicAuthScheme.setPassword(password);
        basicAuthScheme.generateAuthToken();
        return basicAuthScheme;
    }

    public static OAuthScheme oAuthScheme(String apikey,
                                          String secretApikey,
                                          String tokenKey,
                                          String secretTokenKey){
        oAuthScheme.setConsumerKey(apikey);
        oAuthScheme.setConsumerSecret(secretApikey);
        oAuthScheme.setAccessToken(tokenKey);
        oAuthScheme.setSecretToken(secretTokenKey);
        oAuthScheme.setSignature(signature);
        return oAuthScheme;
    }

    public static OAuth2Scheme oAuth2Scheme(String token){
        oAuth2Scheme.setAccessToken(token);
        oAuth2Scheme.setSignature(signature);
        return oAuth2Scheme;
    }

    /**
     * Please watch official documentation
     * {@link}https://github.com/rest-assured/rest-assured/wiki/usage#form-authentication
     * @exception Exception
     * @return FormAuthScheme
     * */
    public static FormAuthScheme formAuthScheme(String userName,String password,String action, String userNameTag, String passwordTag){
        formAuthConfig = new FormAuthConfig(action,userNameTag,passwordTag);
        formAuthScheme.setUserName(userName);
        formAuthScheme.setPassword(password);
        try{
            if (formAuthConfig.hasFormAction()&&formAuthConfig.hasUserInputTagName()&&formAuthConfig.hasPasswordInputTagName()) {
                formAuthScheme.setConfig(formAuthConfig);
            }
        }catch (Exception e){
            System.out.println("Please check form!");
            System.out.println("Status of parameters:");
            System.out.println("Action: "+formAuthConfig.hasFormAction());
            System.out.println("User input: "+formAuthConfig.hasUserInputTagName());
            System.out.println("Pass input: "+formAuthConfig.hasPasswordInputTagName());
        }
        return formAuthScheme;
    }

    /**
     * Please watch official documentation
     * {@link}https://github.com/rest-assured/rest-assured/wiki/usage#form-authentication
     * @exception Exception
     * @return FormAuthScheme
     * */
    public static FormAuthScheme formAuthScheme(String userName,String password){
        formAuthScheme.setUserName(userName);
        formAuthScheme.setPassword(password);
        return formAuthScheme;
    }

}

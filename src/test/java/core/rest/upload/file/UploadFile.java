package core.rest.upload.file;

import io.restassured.builder.MultiPartSpecBuilder;
import io.restassured.specification.MultiPartSpecification;

import java.io.File;
import java.io.IOException;

public class UploadFile {
    private static String fileName;
    private static String filePach;

    public static void setFileName(String name){
        fileName = name;
    }

    public static void setFilePach(String pach){
        filePach = pach;
    }

    private static File getFile(){return new File(filePach);}

    public static MultiPartSpecification builder(String fileName, String controlName, String filePath, String mimeType) throws IOException {
        MultiPartSpecBuilder multiPartSpecBuilder = new MultiPartSpecBuilder(new File(filePath));
        multiPartSpecBuilder.fileName(fileName);
        multiPartSpecBuilder.controlName(controlName);
        multiPartSpecBuilder.mimeType(mimeType);
//        multiPartSpecBuilder.mimeType("application/vnd.custom+txt");
        return multiPartSpecBuilder.build();
    }



}

package core.rest.configs;

import com.hrs.cc.api.connection.Connection;
import com.hrs.cc.api.constans.Auth;
import core.PropertyWorker;
import org.apache.commons.codec.binary.Base64;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SerializeBASE64 {

    private static Base64 base64 = new Base64();
    private static File file = new File(Auth.CONFIGURATION_FILE);


    public static void main(String[] args) throws IOException {
        PropertyWorker propertyWorker = new PropertyWorker("./config.properties");
        generateConfigEncodeFile(propertyWorker.getProperties().toString());
        Connection.ClinicianAuthIntegration();
        Connection.getClinicianToken();
    }


    public static void generateConfigEncodeFile(String string) throws IOException {
        FileOutputStream stream = new FileOutputStream(file);
        stream.write(getEncodeString(string).getBytes());
        stream.close();
    }

    public static String getEncodeString(String str){
        return new String(base64.encode(str.getBytes()));
    }

    public static String getDecodeString(String str){
        return new String(base64.decode(str.getBytes()));
    }

    public static Map<String, String> convertToMap(String str){
        str = str.substring(1, str.length()-1);
        String[] keyValuePairs = str.split(",");
        Map<String,String> map = new HashMap<>();

        for(String pair : keyValuePairs)
        {
            String[] entry = pair.split("=");
            map.put(entry[0].trim(), entry[1].trim());
        }

        return map;
    }
}

package core.rest.configs;

import com.hrs.cc.api.constans.Auth;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.stream.Stream;

public class Configurations {
    private static Map<String, String> configuration = new HashMap<>();
    private static String configPath = Auth.CONFIGURATION_FILE;

//    static {
//        new Configurations();
//    }

    public Configurations() {
        if (Configurations.class.getProtectionDomain().getCodeSource().getLocation().getFile().endsWith(".jar")) {
            InputStream c = Configurations.class.getClassLoader().getResourceAsStream("tests_configs/config.encode");
            Scanner s = new Scanner(c).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            configuration = SerializeBASE64.convertToMap(SerializeBASE64.getDecodeString(result));

        }else {


        Stream<String> stream = null;
        try {
            stream = Files.lines(new File(configPath).toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert stream != null;
        String strEncode = stream.toArray()[0].toString();
        stream.close();
        configuration = SerializeBASE64.convertToMap(SerializeBASE64.getDecodeString(strEncode));
        }
    }


    public static Map<String, String> getProperties() {
//        if (configuration.isEmpty()) {
////            createConfigFile("./config2.properties");
//            String slackApiEx = "Need set slack params:\n authToken=[your token for slack], \n chanellName = [your chanel name].\n\n";
//            String testRailEx = "Need set Testrail params:\n " +
//                    "host_tr=[your host for tr], \n " +
//                    "user_name_tr=[set username],\n" +
//                    "password_tr=[set password].\n\n";
//            String hrsEx = "Need set HRS params:\n " +
//                    "user_name_hrs=[set username], \n " +
//                    "password_hrs=[set password],\n" +
//                    "host_hrs=[set host],\n" +
//                    "host_app_integration_hrs=[set host for integration].\n\n";
//
//            throw new RuntimeException(Auth.CONFIGURATION_FILE + " does not found, please add config file or check current file!\n"
//                    + slackApiEx + testRailEx + hrsEx
//            );
//        }
        return configuration;
    }


    public static String getConfigParam(String name) {
        String valueOfParam = getProperties().get(name);
        if (valueOfParam == null || valueOfParam.isEmpty()) {
            throw new RuntimeException("Param [" + name + "] in " + Auth.CONFIGURATION_FILE +
                    " does not found, please add config file or check current file!\n"
            );
        }
        return valueOfParam;
    }


    public static void createConfigFile(String configPath) {
        try {
            FileOutputStream file = new FileOutputStream(configPath);
            Properties properties = new Properties();
            properties.setProperty("authToken", "");
            properties.setProperty("chanellName", "");
            properties.setProperty("host_tr", "");
            properties.setProperty("user_name_tr", "");
            properties.setProperty("password_tr", "");
            properties.setProperty("user_name_hrs", "");
            properties.setProperty("password_hrs", "");
            properties.setProperty("host_hrs", "");
            properties.setProperty("host_app_integration_hrs", "");
            properties.store(file, null);
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

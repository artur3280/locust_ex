package core;

import com.hrs.cc.api.constans.Modules;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Time;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class ImageCreator {

    public static String getChangedImageBASE64(Date timeStamp, String currentBase64Image) {
        BufferedImage image = base64StringToImg(currentBase64Image);
        Graphics2D g2d = image.createGraphics();
        g2d.setColor(Color.red);
        g2d.setFont(new Font(new JLabel().getFont().getName(), Font.PLAIN, 15));
        g2d.drawString(timeStamp.toString(), 10, 70);
        g2d.dispose();
        return imgToBase64String(image, "jpg");
    }

    private static BufferedImage generationImage(String value, Integer width, Integer height) {

        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = bufferedImage.createGraphics();
        g2d.setColor(Color.white);
        g2d.fillRect(0, 0, width, height);

        g2d.setColor(Color.PINK);
        g2d.setBackground(Color.YELLOW);
        g2d.fillOval(0, 0, width, height);

        g2d.setColor(Color.blue);
        g2d.setFont(new Font(new JLabel().getFont().getName(), Font.PLAIN, 50));
        g2d.drawString(value, 10, height / 2);

        g2d.dispose();
        return bufferedImage;
    }

    private static String imgToBase64String(final RenderedImage img, final String formatName) {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, formatName, Base64.getEncoder().wrap(os));
            return os.toString(StandardCharsets.ISO_8859_1.name());
        } catch (final IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }

    private static BufferedImage base64StringToImg(final String base64String) {
        try {
            return ImageIO.read(new ByteArrayInputStream(Base64.getDecoder().decode(base64String)));
        } catch (final IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }

    public static String getEncodeBase64Image(String value, Integer width, Integer height) {
        return imgToBase64String(generationImage(value, width, height), "jpg");
    }
}

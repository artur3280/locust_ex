package core.deactivation.models.patients;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "hrsid",
        "pid",
        "emrid",
        "ssn",
        "fname",
        "lname",
        "status"
})
public class Patient {

    @JsonProperty("hrsid")
    private String hrsid;
    @JsonProperty("pid")
    private String pid;
    @JsonProperty("emrid")
    private String emrid;
    @JsonProperty("ssn")
    private String ssn;
    @JsonProperty("fname")
    private String fname;
    @JsonProperty("lname")
    private String lname;
    @JsonProperty("status")
    private String status;

    public Patient() {
    }

    public Patient(String hrsid, String pid, String emrid, String ssn, String fname, String lname, String status) {
        this.hrsid = hrsid;
        this.pid = pid;
        this.emrid = emrid;
        this.ssn = ssn;
        this.fname = fname;
        this.lname = lname;
        this.status = status;
    }

    public String getHrsid() {
        return hrsid;
    }

    public void setHrsid(String hrsid) {
        this.hrsid = hrsid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getEmrid() {
        return emrid;
    }

    public void setEmrid(String emrid) {
        this.emrid = emrid;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "hrsid='" + hrsid + '\'' +
                ", pid='" + pid + '\'' +
                ", emrid='" + emrid + '\'' +
                ", ssn='" + ssn + '\'' +
                ", fname='" + fname + '\'' +
                ", lname='" + lname + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}

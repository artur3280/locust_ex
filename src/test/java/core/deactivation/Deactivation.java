package core.deactivation;

import com.fasterxml.jackson.databind.ObjectMapper;
import core.deactivation.models.patients.Patient;
import core.deactivation.models.patients.Patients;
import core.rest.RestUtilities;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Deactivation {
    public static final String HTTPS_ADMIN_HRSALPHA_COM_V_2 = "https://admin.hrsalpha.com/v2";
    public static final String LOGINDATA = "/logindata.php";
    private static RestUtilities restUtilities = new RestUtilities();
    private static RequestSpecification reqSpec;
    private static ResponseSpecification resSpec;
    private static final String AUTH_TOKEN = "Basic YWRtaW46TGFDcm9peA==";
    private static final String AUTH_SESSION = "csb5c7rpq49oo1jvmf5ue2lkp6";
    private static ObjectMapper mapper = new ObjectMapper();

    public static void main(String[] args) throws IOException {
        connectionAdmin();
        List<Patient> patients = getAllPatients().getPatients()
                .stream()
                .filter(p -> p.getFname().startsWith("New first name ") &&
                        p.getLname().startsWith("New last name ") &&
                        p.getStatus().contains("active") &&
                        !p.getFname().contains("UserApp") &&
                        !p.getLname().contains("Integration")
                ).collect(Collectors.toList());

        System.out.println(patients.size());
        for (int i = 0; i < 120; i++) {
            deactivatePatient(patients.get(i).getHrsid());

            System.out.println(i+" => "+patients.get(i).getFname() + "==>" + patients.get(i).getHrsid() +"==>" + patients.get(i).getStatus() + "==> deactivate" );
        }


    }

    private static void connectionAdmin() {
        restUtilities.setBaseUri(HTTPS_ADMIN_HRSALPHA_COM_V_2);
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        reqSpecification.basePath(LOGINDATA);
        Map<String, String> data = new HashMap<>();
        data.put("request", "login");
        data.put("u", "olga_admin");
        data.put("p", "root");
        reqSpecification.header("authorization", AUTH_TOKEN);
        reqSpecification.cookie("PHPSESSID", AUTH_SESSION);
        reqSpecification.formParams(data);
        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        restUtilities.setEndPoint();
        reqSpecification.log().all();
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200).expectContentType(ContentType.fromContentType("text/html")));

        Response response = restUtilities.getResponse(reqSpecification, "post");
        response.prettyPrint();
    }

    private static Patients getAllPatients() throws IOException {
        restUtilities.setBaseUri(HTTPS_ADMIN_HRSALPHA_COM_V_2);
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        reqSpecification.basePath("/data.php");
        Map<String, String> data = new HashMap<>();
        data.put("request", "patientlist");
        data.put("env", "QATestingZone");
        reqSpecification.header("authorization", AUTH_TOKEN);
        reqSpecification.cookie("PHPSESSID", AUTH_SESSION);
        reqSpecification.formParams(data);
        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        restUtilities.setEndPoint();
        reqSpecification.log().all();
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200).expectContentType(ContentType.fromContentType("text/html")));

        Response response = restUtilities.getResponse(reqSpecification, "post");

        Document doc = Jsoup.parse(response.asString());

//        Pattern pattern = Pattern.compile("<html>\n" +
//                 "  <body>(.+?)</body>\n" +
//                 "</html>");
//         Matcher matcher = pattern.matcher(response.asString());


        System.out.println(doc.tagName("body").text());
//            Patients patients = mapper.convertValue(matcher.group(1),Patients.class);
        return mapper.readValue(doc.tagName("body").text(), Patients.class);

    }


    private static JsonPath deactivatePatient(String id) {
        restUtilities.setBaseUri(HTTPS_ADMIN_HRSALPHA_COM_V_2);
        RequestSpecification reqSpecification = restUtilities.getRequestSpecification();
        reqSpecification.basePath("/data.php");
        Map<String, String> data = new HashMap<>();
        data.put("request", "changepatientstatus");
        data.put("action", "disable");
        data.put("patient", id);
        data.put("env", "QATestingZone");
        reqSpecification.header("authorization", AUTH_TOKEN);
        reqSpecification.cookie("PHPSESSID", AUTH_SESSION);
        reqSpecification.formParams(data);
        restUtilities.setContentType(ContentType.fromContentType("application/x-www-form-urlencoded"));
        restUtilities.setEndPoint();
        restUtilities.getResponseSpecification(
                restUtilities.createResponseBuilder()
                        .expectStatusCode(200).expectContentType(ContentType.fromContentType("text/html")));

        Response response = restUtilities.getResponse(reqSpecification, "post");
        JsonPath path = response.jsonPath();
        return path;
    }
}
